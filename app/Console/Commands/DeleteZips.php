<?php

namespace App\Console\Commands;

use Storage;
use Illuminate\Console\Command;

class DeleteZips extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zips:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete zip files older than 2 hours.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::allFiles('/download');
        
        foreach ($files as $file_to_delete) {
            $file_time = intval(substr($file_to_delete, -14, -4));

            if ($file_time < (time() - 7200)) {
                Storage::delete($file_to_delete);
            }
        }
    }
}
