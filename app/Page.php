<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Page extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by_id', 'title', 'image', 'server_id', 'published'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    public function textInCurrentLocale()
    {
        return $this->texts()->where('locale', cur_lan())->first()->text;
    }

    /**
     * Page Text
     */
    public function texts()
    {
        return $this->hasMany('App\PageText');
    }

    /**
     * @return string
     */
    public function titleInCurrentLocale()
    {
        return $this->texts()->where('locale', cur_lan())->first()->title;
    }
}
