<?php

namespace App;

use HttpOz\Roles\Traits\HasRole;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements HasRoleContract
{
    use Notifiable, HasRole;
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id',
        'interests',
        'language',
        'country_id',
        'email',
        'password',
        'name',
        'last_name',
        'phone',
        'web',
        'user_type_id',
        'school',
        'address',
        'departement',
        'position',
        'interest',
        'gps',
        'facebook_id',
        'twitter_id',
        'google_id',
        'linked_id',
        'validated',
        'newsletter',
        'public_info',
        'note',
        'birthday',
        'image'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * TYPE
     */
    public function types()
    {
        return $this->belongsTo('App\UserType', 'user_type_id');
    }


    /**
     * COURSES created
     */
    public function courses_created()
    {
        return $this->hasMany('App\Course', 'created_by');
    }

    /**
     * TOOLS created
     */
    public function tools_created()
    {
        return $this->hasMany('App\Tool', 'created_by');
    }

    /**
     * RESOURCES created
     */
    public function resources_created()
    {
        return $this->hasMany('App\Resource', 'created_by');
    }
    
    /**
     * COMMENTS comments
     */
    public function resource_comments()
    {
        return $this->hasMany('App\ResourceComment', 'created_by');
    }

    /**
     * CASE STUDYS created
     */
    public function study_cases_created()
    {
        return $this->hasMany('App\StudyCase', 'created_by');
    }


    /**
     * COURSES teacher
     */
    public function courses_teached()
    {
        return $this->belongsToMany('App\Course', 'course_teacher');
    }

    

    public function hasGroup($group)
    {
        return ($this->getRoles()->where('group', 'LIKE', $group)->first() ? true : false);
    }

    /**
     * PROJECTS
     */
    public function groups()
    {
        return ($this->getRoles());
    }

    /**
     * Country
    */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }


    /**
     * PROJECTS taught
     */
    public function challenge_group_teached()
    {
        return $this->belongsToMany('App\ChallengeGroup', 'challenge_group_teacher');
    }

    /**
     * CHALLENGES
     */
    public function challenges()
    {
        return $this->hasMany('App\Challenge', 'created_by');
    }

    /**
     * PROJECTS member
     */
    public function challenge_groups()
    {
        return $this->belongsToMany('App\ChallengeGroup', 'challenge_group_user');
    }

    /**
     * Members group
     */
    public function members_groups()
    {
        return $this->belongsToMany('App\MembersGroup');
    }

    /**
     * Members group
     */
    public function members_group_created()
    {
        return $this->belongsTo('App\MembersGroup', 'created_by_id');
    }


    /**
     * Members group
     */
    public function partners_groups()
    {
        return $this->belongsToMany('App\MembersGroup')->where('partner', 1);
    }

    /**
     * Members group
     */
    public function partners_group_created()
    {
        return $this->belongsTo('App\PartnersGroup', 'created_by_id');
    }
    

    /**
     * Full name of the user
     */
    public function fullName()
    {
        return $this->name . " " . $this->last_name;
    }

    public function generate_token()
    {
        $this->login_token = str_random(30);
        $this->save();
    }


    public function getFullNameAttribute($value)
    {
        return ucfirst($this->name) . ' ' . ucfirst($this->last_name);
    }

    public function getImage() {
        if ($this->image) {
            return url($this->image);
        }
        return "";
    }

    //observe this model being deleted and delete the child events
    public static function boot()
    {
        parent::boot();

        self::deleting(function (User $user) {

            // delete courses
            foreach ($user->courses_created as $course) {
                $course->delete();
            }

            // delete tools
            foreach ($user->tools_created as $tool) {
                $tool->delete();
            }

            // delete resources
            foreach ($user->resources_created as $resource) {
                $resource->delete();
            }

            // delete resources
            foreach ($user->study_cases_created as $study_case) {
                $study_case->delete();
            }
        });
    }
}
