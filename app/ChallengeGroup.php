<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class ChallengeGroup extends Model
{
    use RevisionableTrait, SoftDeletes;
    public $badge = "badge-project";
    public $classText = "text-project";
    public $classBck = "bck-project";
    protected $revisionCreationsEnabled = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id',
        'challenge_id',
        'created_by_id',
        'name',
        'project_name',
        'published',
        'archived'
    ];

    /**
     * @return string
     */
    public function getTitle()
    {
        return isset($this->name) ? $this->name : trans('text.no_title');
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/project_big.png";
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    /**
     * Created by
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }


    /**
     * Users
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'challenge_group_user');
    }

    /**
     * Teachers
     */
    public function teachers()
    {
        return $this->belongsToMany('App\User', 'challenge_group_teacher');
    }

    /**
     * Challenge
     */
    public function challenge()
    {
        return $this->belongsTo('App\Challenge');
    }


    /**
     * Get all of the project resources.
     */
    public function resources()
    {
        return $this->morphMany('App\Resource', 'resourcable')->orderBy('order');
    }

    /**
     * Get all of the published project resources.
     */
    public function resources_published()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1);
    }

    public function searchData()
    {
        return [
            'id' => $this->id,
            'title' => $this->project_name,
            'author' => $this->author->fullName(),
            'country' => "",
            'year' => $this->created_at->year,
            'language' => "",
            'type' => 'project',
            'platform' => str_replace('http://', "", url("/")),
            'platform_id' => server_property('server_id'),
            'published' => $this->published,
            'category' => "",
            'description' => $this->description,
            'image' => $this->imageUrl(),
            'created_at' => (array)$this->created_at,
            'created_at_formated' => $this->created_at->timestamp,
            'course' => "",
            'length' => "",
            'state' => "",
            'institution' => "",
            'date' => "",
            'global' => "",
            'producer' => "",
            'criteria' => "",
            'contents' => "",
            'resource_type' => "",
            'designer' => ""
        ];
    }

    public function imageUrl()
    {
        return $this->image ? url($this->image) : "";
    }
}
