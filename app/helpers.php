<?php
    
    function name_to_text($element)
    {
        return trans('text.'.$element);
    }


    /**
     * RETURN CURRENT LANGUAGE
     */
    function cur_lan()
    {
        if (Session::get('locale')) {
            return Session::get('locale');
        }
        if (Auth::check()) {
            return Auth::user()->default_language;
        }
        return 'en';
    }

    /**
     * RETURN CURRENT LANGUAGE NAME
     */
    function cur_lan_name()
    {
        $language = \App\Language::where('locale', 'LIKE', cur_lan())->first();
        if ($language) {
            return $language->name;
        } else {
            return 'No languages';
        }
    }


    /**
     * Curent user cover image
     * @return link to the image
     */
    function user_image($user_id = null)
    {
        if (!$user_id) {
            if (Auth::check()) {
                if (Auth::user()->image) {
                    return Auth::user()->image;
                } else {
                    return '/images/user-profiles/default_logged_in.png';
                }
            } else {
                return '/images/user-profiles/default.png';
            }
        } else {
            $user = \App\User::find($user_id);
            if ($user) {
                if ($user->image) {
                    return $user->image;
                } else {
                    return '/images/user-profiles/default.png';
                }
            } else {
                return '/images/user-profiles/default.png';
            }
        }
    }


    function userAvatar($user_id)
    {
        $image = user_image($user_id);
        $return = '<div class="round_button pull-left" style="background-image: url(' . $image . '); background-size:cover;">&nbsp;</div>';
        return $return;
    }

    /**
     * GET Server property
     * @param  string $name - name of the property
     * @return string 		- the data form the property or false
     */
    function server_property($name)
    {
        $data = \App\ServerProperties::where('name', 'LIKE', $name)->first();
        if ($data->data) {
            return $data->data;
        } else {
            return false;
        }
    }


    function set_server_property($name, $data)
    {
        $server_property = \App\ServerProperties::where('name', 'LIKE', $name)->first();
        $server_property->data = $data;
        $server_property->save();
    }

    /**
     * Social icons
     * @return links to images
     */
    function social_icons()
    {
        
        // get the property from server properties and decode
        $icons = json_decode(server_property('social_icons'));
        $link = "";

        foreach ($icons as $key => $value) {
            // if there is a value, add the icon
            if ($value) {
                $link .= '<a href="' . $value . '" target="_blank"><img src="/images/social_icons/' . $key . '.png" class="social_icons"></a>';
            }
        }

        return $link;
    }
    

    /**
     * Return role by slug
     */
    function role($slug)
    {
        return Role::where('slug', $slug)->first();
    }



    function view_td($title, $text)
    {
        $title = strtoupper(trans('text.' . $title));
        $return_data = "
			<tr>
				<td>
					<!-- type -->
					<span class='title'>
						$title
					</span>
				</td>
				<td class='text-right'>
					$text
				</td>
			</tr>";

        return $return_data;
    }

    function titleAndTextRow($title, $text, $titleWidth = 4)
    {
        // Make the title
        $title = strtoupper(trans($title));
        $text = $text ? $text : "-";
        // Get text width
        if ($titleWidth == 12) {
            $textWidth = 12;
            $textAlign = "text-left";
        } else {
            $textWidth = 12 - $titleWidth;
            $textAlign = "text-right";
        }
        
        // Make the row
        $returnData = "
		<div class='row form-group'>
			<div class='col-md-$titleWidth col-xs-12 title'>
				$title
			</div>
			<div class='col-md-$textWidth col-xs-12 $textAlign'>
				$text
			</div>
		</div>";
        return $returnData;
    }

    /***** LISTS *****/

    // COURSE - schools
    function list_schools()
    {
        // TODO - add other tables
        $return_data = DB::table('courses')->select('school')->distinct()->get();
        return $return_data;
    }
    
    // STUDY CASE - categories
    function list_study_case($filed)
    {
        $return_data = DB::table('study_cases')->select($filed)->distinct()->get();
        return $return_data;
    }

    // DATA FROM CENTRAL SERVER
    function central_server_data($url)
    {
        // make a url
        $central_server_url = server_property("central_server_address") . $url;
        $return_value = json_decode(url_request($central_server_url, "GET"));
        if (!$return_value || !is_array($return_value)) {
            $return_value = [];
        }
        return $return_value;
    }


    function url_request($url, $method = "POST", $parameters = [])
    {
        $url = trim($url);
        try {
            $client = new \GuzzleHttp\Client();
            $return_data = $client->request($method, $url, $parameters);
            $status = $return_data->getStatusCode();
            if ($status == 200) {
                $return_value = (string) $return_data->getBody();
                return $return_value;
            } else {
                logger("No server data $status: " . $return_data);
                $return_value = "";
            }
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            $return_value = "";
            return $return_value;
        }
    }


    function array_of_all_servers()
    {
        $server_list = [];

        if (server_property("central_server") == 1) {
            $serversOnCentral = \App\ServerList::where('enabled', 'LIKE', 1)->get();
            foreach ($serversOnCentral as $serverOnCentral) {
                $serverForArray = new \stdClass();
                $serverForArray->address = $serverOnCentral->address;
                $serverForArray->name = $serverOnCentral->name;
                $serverForArray->id = $serverOnCentral->id;
                array_push($server_list, $serverForArray);
            }
        } else {
            $this_server = (object)[];
            $this_server->address = url('/');
            array_push($server_list, $this_server);
        }
        
        return $server_list;
    }


    /**
     * CHECK IF USER EXISTS ON ANY PLATFORM
     * @param  string $email
     * @return json   server object
     */
    function user_on_system($email)
    {
        $url = "/api/check_user_exists?email=" . $email;
        $user_on_system = async_request_to_all_servers($url, [], "GET");
        return $user_on_system;
    }


    // LIST OF ALL PLATFORMS
    function platforms_list()
    {
        // decode the list from central server
        $platforms_list = central_server_data('/api/server_list');
        $platforms = [];

        // get platform addreses
        if ($platforms_list) {
            foreach ($platforms_list as $platform) {
                array_push($platforms, $platform->address);
            }
        }
        // return the array
        return $platforms;
    }


    /** return if there is index **/
    function echo_index($array, $index)
    {
        if (isset($array[$index])) {
            return $array[$index];
        } else {
            return "";
        }
    }


    /**
     * Server URL from id
     * @param  int 		$server_id 		Id of the server
     * @return string 					Server url
     */
    function server_url($server_id)
    {
        if ($server_id && ($server_id != server_property('server_id'))) {

            // get the server from list
            $server = \App\ServerList::find($server_id);
            
            // if no server found
            if (!$server) {
                flash(trans('text.no_server_found'));
                return redirect('/');
            }

            // RETURN the server address
            return $server->address;
        } else {
            
            // Local view server address
            return url("/");
        }
    }

    /**
     * Question with a tool
     * @param  string $text  test to display in the tooltip
     * @return string
     */
    function tooltip($text, $class = "")
    {
        $return_data = '<i class="fa fa-question-circle-o tooltip_text" aria-hidden="true" title="' . $text . '"></i>';
        return $return_data;
    }


    // GET JSON DATA FORM SERVER_ID AND PARSE IT
    function remote_json_data($url, $server_id, $array = false)
    {
        // Server url  - if local / remote
        if ($server_id == server_property('server_id')) {
            $server_url = url('/');
        } else {
            // get the server id if remote
            $server_url = central_server_data('/api/server_url/' . $server_id);
        }

        // If server exists get the data
        if ($server_url) {
            if ($remote_data = @file_get_contents($server_url . $url)) {
                if ($return_datareturn_data = json_decode($remote_data, $array)) {
                    return $return_data;
                }
            }
        }
        return false;
    }

    // return file extension from path
    function file_exitension($filename)
    {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        if (!$ext) {
            return "";
        } else {
            return $ext;
        }
    }


    // file preview
    function file_preview($filename)
    {
        $images_array = ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'ico'];
        $video_array = ['webm', 'mp4', 'ogg', 'mp3'];
        $audio_array = ['mp3', 'wav'];
        $file_exitension = file_exitension($filename);

        // search if file is video/audio
        if (in_array($file_exitension, $images_array)) {
            return "<img src='$filename' class='image'>";
        }

        // search if file is video_array
        if (in_array($file_exitension, $video_array)) {
            return '
				<video width="100%"  controls>
				  <source src="' . $filename . '" type="video/' . $file_exitension .'">
				Your browser does not support the video tag.
				</video>
			';
        }

        // search if file is audio array
        if (in_array($file_exitension, $audio_array)) {
            return '
				<audio width="100%" controls>
				  <source src="' . $filename . '" type="video/' . $file_exitension .'">
					Your browser does not support the audio element.
				</audio >
			';
        }
    }

    function coverImageEdit($resource)
    {
        return view('common.cover_image_edit', ['resource' => $resource]);
    }


    function displayTitleAndTextNewRow($title, $text)
    {
        $returnData = "
			<div class='row'>
				<div class='col-xs-12'>
					<h5>$title</h5>
				</div>
			</div>
			<div class='row'>
				<div class='col-xs-12'>
					$text
				</div>
			</div>
		";
        return $returnData;
    }


    /**
     * Display a input with a combobox and values
     * @param  string $name
     * @param  string $model
     * @param  int $id
     * @param  array $values
     * @param  string $placeholder
     * @return string
     */
    function inputCombobox($name, $model, $id, $current_value, $values, $placeholder = "")
    {
        $return_data = "<select id='$model-$name-$id-box'>";
        $return_data .= "<option value='$current_value'>$current_value</option>";
        foreach ($values as $value) {
            $return_data .= "<option value='$value'>$value</option>";
        }
        $return_data .= "</select>";
        $return_data .= "<script>$('#$model-$name-$id-box').scombobox({
								invalidAsValue: true, 
								placeholder: '$placeholder',
								data_id: '$id',
								data_model: '$model',
								name: '$name',
								id: '$model-$name-$id',
								removeDuplicates: false,

							});</script>";
        return $return_data;
    }


    function updateTextInput($model, $row, $updateURL, $options)
    {
        return view('bootstrap4.common.text_input', ['model' => $model, 'row' => $row, 'updateURL' => $updateURL, 'options' => $options]);
    }


    function updateInput($row, $form)
    {
        // make label
        $returnData = "<label>";
        $returnData .= trans($row);
        $returnData .= "</label>";
        $returnData .= $form;

        return $returnData;
    }

    function inputErrorRow($row)
    {
        if (isset($errors)) {
            if ($errors->has($row)) {
                $returnData = "<div class='row'><div class='col-xs-12 text-right text-danger'>";
                $returnData .= '<span class="help-block text-danger"><strong>' . $errors->first($row) . '</strong></span></div></div>';
                return $returnData;
            }
        }
    }

    /**
     * Publish button - universal button for publishing resources
     * @param  $resource
     * @param  $additionalData
     * @return string - button
     */
    function publishButton($resource, $additionalData = "")
    {
        $model = strtolower(substr(get_class($resource), 4));
        $titleText =  $resource->published ? trans('text.published_click_to_unpublish') : trans('text.not_published_click_to_publish');
        $returnData = "<button type='button'";
        $returnData .= " class='btn bck-lens publish_object shaddow ";
        $returnData .= $resource->published ? "'" : " transparent' ";
        $returnData .= " data-url='/$model/publish/$resource->id' ";
        $returnData .= " $additionalData ";
        $returnData .= "data-id='$resource->id' title='$titleText'>";
        $returnData .= "<i class='";
        $returnData .= $resource->published ? "far fa-eye" : "far fa-eye-slash";
        $returnData .= "' aria-hidden='true'></i>";
        $returnData .= "</button>";

        return $returnData;
    }

    /**
     * Publish button - universal button for publishing resources
     * @param  $resource
     * @return string - button
     */
    function globalButton($resource)
    {
        $model = strtolower(substr(get_class($resource), 4));
        $titleText =  $resource->global ? trans('text.global_click_to_make_local') : trans('text.local_click_to_make_global');
        $returnData = "<button type='button'";
        $returnData .= " class='btn bck-lens global_object shaddow ";
        $returnData .= $resource->global ? "'" : " transparent' ";
        $returnData .= "data-url='/$model/global/$resource->id'";
        $returnData .= "data-id='$resource->id' title='$titleText'>";
        $returnData .= "<i class='";
        $returnData .= $resource->global ? "fa fa-globe" : "far fa-circle";
        $returnData .= "' aria-hidden='true'></i>";
        $returnData .= "</button>";

        return $returnData;
    }


/**
 * @param $resource
 * @param $imageReplacement
 * @return string
 */
function coverImage($resource, $imageReplacement)
{
    $imageReplacement = "/images/resource/" . $imageReplacement;
    $image = (isset($resource->image) && $resource->image) ? url($resource->image) : $imageReplacement;
    $returnData = "<span class='title resource-info-title'>" . strtoupper(trans('text.cover_image')) . "</span>
						<img src='$image' class='cover-image' >";
    return $returnData;
}


    function backendHeader($class, $model, $text, $image, $createButton = false)
    {
        return view('common.backend_header', ['class' => $class,'model' => $model,'text' => $text,'image' => $image,'createButton' => $createButton]);
    }

    function left_back_button($search = "")
    {
        $back_button =  '<button class="btn bck-lens pull-left back-button rounded-circle text-white" title="' . trans('text.back') . '" data-link="/?res[]=' . $search . '"><i class="fas fa-arrow-left" aria-hidden="true"></i></button>';
        $back_button .= "<script> $(document).ready(function() { 
				$('body').on('click', '.back-button', function() { 
					window.location.href = $(this).data('link'); 
				}); 
			}); 
			</script>";
        return $back_button;
    }
    

    /**
     * Text of the page in current locale
     * @param  string $permalink
     * @return string
     */
    function page_text($permalink)
    {
        $page = \App\Page::where('permalink', 'LIKE', $permalink)->first();
        $text = $page->texts()->where('locale', 'LIKE', cur_lan())->first();

        if ($text) {
            return $text->text;
            // TO DO Change all texts
            return find_email_and_obfuscate($text->text);
        } else {
            return "no text";
        }
    }

    /**
     * Title of the page
     * @param  string $permalink
     * @return string
     */
    function page_title($permalink)
    {
        $page = \App\Page::where('permalink', 'LIKE', $permalink)->first();
        $text = $page->texts()->where('locale', 'LIKE', cur_lan())->first();

        if ($text) {
            return $text->title;
        } else {
            return "no text";
        }
    }

    /**
     * Return type of file by extension
     * @param  string $filePath File path/link
     * @return string           One of: image, pdf, video, document, slideshow, other
     */
    function fileExtensionType($filePath)
    {
        // types of files (add if necessary)
        $fileTypes = [
            'doc' => 'document', 'docx' => 'document', 'xls' => 'document', 'xlsx' => 'document', 'txt' => 'document',
            'jpg' => 'image', 'jpeg' => 'image', 'png' => 'image', 'bmp' => 'image', 'gif' => 'image',
            'avi' => 'video', 'ogg' => 'video', 'mp4' => 'video', 'mpg' => 'video',
            'ppt' => 'slideshow', 'pptx' => 'slideshow',
            'mp3' => 'audio', 'wav' => 'audio',
            'pdf' => 'pdf',
            ];

        // calculate the extension
        $info = pathinfo($filePath);
        $extension = $info['extension'];

        if (!$extension) {
            return "other";
        }

        return isset($fileTypes[$extension]) ? $fileTypes[$extension] : "other";
    }


    /**
     * Display home page video
     * @return string
     */
    function homePageVideo()
    {
        $videoUrl = server_property("home_video");
        $poster = server_property("home_video_poster") ? " poster='" . server_property("home_video_poster") . "?" . time() . "' " : "";
        if ($videoUrl) {
            return "<video width='100%' $poster controls><source src='$videoUrl' type='video/mp4'>Your browser does not support the video tag.</video>";
        } else {
            return "<!-- no home video -->";
        }
    }


    /**
     * Return link or login link if user
     * @param  string $url
     * @param  string $user
     * @return string
     */
    function linkDownloadOrLogin($url, $request = "", $download = "download")
    {
        if (isset($request->user_id) || Auth::user()) {
            return 'href="' . url($url) . '" ' . $download;
        } else {
            return 'href="/login" title="' . trans('text.you_have_to_login_to_download_this_resource') . '"';
        }
    }

    /**
     * Return link or login link if user
     * @param  string $url
     * @param  string $user
     * @return string
     */
    function downloadLinkAndIncrement($url, $request = "")
    {
        if (isset($request->user_id)) {
            return '<span class="round_button text-lens text-center download_resource pull-right" title="' . trans('text.download') . '" data-download-url="' . url("/" . $url) . '" style="background-color: white;">
						<i class="fa fa-download" aria-hidden="true"></i>
					</span>';
        } else {
            return '<span class="round_button text-lens text-center pull-right" style="background-color: white;">
						<a href="' . url("/login") . '" title="' . trans('text.you_have_to_login_to_download_this_resource') . '">
							<i class="fa fa-download" aria-hidden="true"></i>
						</a>
					</span>';
        }
    }


    /**
     * Return link or login link if user
     * @param  string $url
     * @param  string $user
     * @return string
     */
    function downloadLinkAndIncrementBootstrap4($url, $request = "")
    {
        if (isset($request->user_id)) {
            return '<div class="btn bck-lens rounded-circle btn-30 download_resource text-white p-0" title="' . trans('text.download') . '" data-download-url="' . url("/" . $url) . '">
						<i class="fa fa-download" aria-hidden="true"></i>
					</div>';
        } else {
            return '<div class="btn bck-lens rounded-circle btn-30 text-white p-0">
						<a href="' . url("/login") . '" title="' . trans('text.you_have_to_login_to_download_this_resource') . '" class="text-white">
							<i class="fa fa-download" aria-hidden="true"></i>
						</a>
					</div>';
        }
    }

    /**
     * Return link or login link if user (aligne left)
     * @param  string $url
     * @param  string $user
     * @return string
     */
    function downloadLinkAndIncrementAlignLeft($url, $request = "")
    {
        if (isset($request->user_id)) {
            return '<div class="round_button text-lens text-center download_resource pull-left" title="' . trans('text.download') . '" data-download-url="' . url("/" . $url) . '" style="background-color: white;">
						<i class="fa fa-download" aria-hidden="true"></i>
					</div>';
        } else {
            return '<div class="round_button text-lens text-center pull-left" style="background-color: white;">
						<a href="/login" title="' . trans('text.you_have_to_login_to_download_this_resource') . '">
							<i class="fa fa-download" aria-hidden="true"></i>
						</a>
					</div>';
        }
    }
    /**
     * UPDATE COVER IMAGE
     */
    function uploadCoverImage(\Illuminate\Http\Request $request, $resource)
    {
        // Handle image
        if ($request->image) {
            
            // Remove / from class name and make it lowercase
            $class_name = get_class($resource);
            $class_name = explode("\\", $class_name);
            $class = strtolower(end($class_name));

            $folder = "/resources/" . $class  . "/" . $resource->id . "/cover";
            $image_path = $folder . "/" . $resource->id . "_" . time() . '.jpg';
            
            // read image from temporary file
            $img = Image::make($request->image);

            // resize image
            $img->resize(800, 800, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });

            
            // Make folder if not exist
            if (!file_exists(public_path() . $folder)) {
                mkdir(public_path() . $folder, 0777, true);
            }

            // save image
            $img->save(public_path() . $image_path);

            $resource->image = url('/') . $image_path;
            $resource->save();
        }

        // Return OK if all is well
        return "OK";
    }



    function async_request($urls, $parameters, $method = "GET")
    {
        $results = [];
        foreach ($urls as $url) {
            $response = false;
            $client = new \GuzzleHttp\Client();
            
            try {
                $response = $client->request($method, $url, ['json' => $parameters]);
            } catch (\GuzzleHttp\Exception\ConnectException $exception) {
                logger('Guzzle err: ' . $url);
            }

            if ($response) {
                $return_content = (string) $response->getBody()->getContents();
                if ($return_content = json_decode($return_content, true)) {
                    $results = array_merge($results, $return_content);
                }
            }
        }

        return $results;
    }


    function async_request_to_all_servers($url, $parameters, $method = "POST")
    {
        $server_list = array_of_all_servers();
        if (!$server_list) {
            return false;
        }

        // create full url-s
        $full_urls = [];
        foreach ($server_list as $server_url) {
            array_push($full_urls, $server_url->address . $url);
        }

        return async_request($full_urls, $parameters, $method);
    }


    function async_request_to_local_or_all_servers($url, $parameters, $method = "POST")
    {
        $server_list = server_property('central_server') ? array_of_all_servers() : [(object)['address' => url('/')]];
        if (!$server_list) {
            return false;
        }

        // create full url-s
        $full_urls = [];
        foreach ($server_list as $server_url) {
            array_push($full_urls, $server_url->address . $url);
        }

        return async_request($full_urls, $parameters, $method);
    }


    /**
     * Create new vars for each language
     */
    function update_languages()
    {
        set_time_limit('600');
        // Get translations
        $english_translations = \App\LanguageTranslation::where('locale', 'LIKE', 'en')->get();
        // Get Languages
        $languages = \App\Language::where('published', 1)->where('locale', 'NOT LIKE', 'en')->get();
        $i = 0;
        // if there is no translation for language, create one
        foreach ($english_translations as $translation) {
            foreach ($languages as $language) {
                $i++;
                if ($i % 10000 == 0) {
                    echo($i/10000 . "\r\n");
                }
                
                if (!\App\LanguageTranslation::where('locale', 'LIKE', $language->locale)->where('item', 'LIKE', $translation->item)->first()) {
                    DB::table('translator_translations')->insert([
                        'locale' => $language->locale,
                        'namespace' => $translation->namespace,
                        'group' => $translation->group,
                        'item' => $translation->item,
                        'text' => $translation->text,
                        'unstable' => $translation->unstable,
                        'locked' => $translation->locked
                    ]);
                }
            }
        }
    }


    /**
     * Add text to database for all languages
     * @param string $group
     * @param string $item
     * @param string $text
     */
    function add_migration_text($group, $item, $text)
    {
        $languages = \App\Language::where('published', 1)->get();

        foreach ($languages as $language) {
            \App\LanguageTranslation::firstOrCreate(["locale" => $language->locale , "group" => $group, "item" => $item, "text" => $text]);
            // DB::table('translator_translations')->insert([
            //]);
        }

        \Waavi\Translation\Facades\TranslationCache::flushAll();
    }

    /**
     * Increment view counter
     * @param  $model
     * @return integer        View count
     */
    function increment_view_counter($model)
    {
        $model->increment('view_count');
        return $model;
    }

    /**
     * Increment download counter
     * @param  $model
     * @return integer        View count
     */
    function increment_download_counter($model)
    {
        $model->increment('download_count');
        return $model;
    }

    /**
     * Display view and download counters
     * @param  model $model Model to display
     * @return view
     */
    function view_counter_icons($model, $class="pull-right float-right")
    {
        $download_count = isset($model->download_count) ? $model->download_count : 0;
        
        if (get_class($model) == "App\ChallengeGroup" || get_class($model) == "App\Lecture") {
            $download_count = $model->resources->sum('download_count');
        } elseif (get_class($model) == "App\Course") {
            foreach ($model->subjects_published as $subjects_published) {
                foreach ($subjects_published->lectures_published as $lectures_published) {
                    $download_count += $lectures_published->resources->sum('download_count');
                }
            }
        }
                
        return view('common.view_count', ['model' => $model, 'download_count' => $download_count, 'class' => $class])->render();
    }
    
    
    function check_url($file)
    {
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Logo path
     * @param boolean Is the logo background light or dark
     * @return string to
     */
    function logo($dark_background = false)
    {
        $fileName = $dark_background ? 'logo_dark' : 'logo_light';

        if (file_exists(public_path('/images/' . $fileName . '.png'))) {
            return '/images/' . $fileName . '.png';
        } else {
            return '/images/' . $fileName . '_default.png';
        }
    }


    function r_collect($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = r_collect($value);
                $array[$key] = $value;
            }
        }

        return collect($array);
    }

    function odd_even($num)
    {
        return ($num % 2 ? 'even' : 'odd');
    }

    /**
     * Find email in a string and obfuscate it
     * @param  text $text
     * @return text
     */
    function find_email_and_obfuscate($text)
    {
        $pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
        return preg_replace_callback($pattern, function ($matches) {
            return Html::mailto($matches[0]);
        }, $text);
    }
   

    /**
     * Fix text in translation
     * @param  text $oldText
     * @param  text $newText
     */
   function fixText($oldText, $newText)
   {
       $translations = \App\LanguageTranslation::where('text', 'LIKE', $oldText)->get();
       foreach ($translations as $translation) {
           $translation->text = $newText;
           $translation->save();
       }
   }


    function header_menu_link($link, $role, $text, $only_on_central = false)
    {
        $button = "";
        // Check central server
        if (!$only_on_central || ($only_on_central && server_property('central_server') == 1)) {
            // Check role
            if (Auth::user()->hasRole($role)) {
                $active = ("/" . Request::path() == $link) ? "active" : "";
                $button = "<li class='nav-item $active'><a class='nav-link $active' href='$link'>$text</a></li>";
            }
        }

        return $button;
    }


?>

