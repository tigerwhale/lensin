<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Challenge extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'created_by',
        'name',
        'theme_id',
        'course',
        'course_id',
        'year',
        'description',
        'comments_enabled',
        'published',
        'archived',
        'language_id'
    ];


    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }


    public function getCourse()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function getCourseName()
    {
        if ($this->course_id) {
            return $this->getCourse->name;
        }
        return $this->course;
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Users
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'challenge_user');
    }


    /**
     * Theme
     */
    public function theme()
    {
        return $this->belongsTo('App\Theme');
    }


    /**
     * Description
     */
    public function groups()
    {
        return $this->hasMany('App\ChallengeGroup');
    }
}
