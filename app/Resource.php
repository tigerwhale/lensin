<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Resource extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'name', 'category', 'desctiption', 'order', 'filename', 'link', 'published', 'resourcable_id', 'resourcable_type', 'unit', 'resource_type_id'
    ];

    protected $appends = ['serverurl', 'serverid'];


    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Get all of the resourcable models.
     */
    public function resourcable()
    {
        return $this->morphTo();
    }


    /**
     * Licences
     */
    public function licences()
    {
        return $this->belongsToMany('App\Licence');
    }


    /**
     * Resource types
     */
    public function resource_type()
    {
        return $this->belongsTo('App\ResourceType');
    }


    /**
     * Comments
     */
    public function comments()
    {
        return $this->hasMany('App\ResourceComment')->orderBy('created_at', 'DESC');
    }


    public function name()
    {
        if ($this->name) {
            return $this->name;
        } else {
            $no_name = "<span class='no_name'>" . trans('text.no_name') . "</span>";
            return $no_name;
        }
    }

    public function path_or_preview()
    {
        $url = "";
        if ($this->preview) {
            $url = $this->preview;
        } elseif ($this->path) {
            $url = url($this->path);
        }
        return $url;
    }


    public function getServeridAttribute()
    {
        return server_property('server_id');
    }

    public function getServerurlAttribute()
    {
        return url("/");
    }
}
