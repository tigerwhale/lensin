<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class NetworkMember extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'created_by', 'name', 'desctiption', 'email', 'network_id', 'network_member_id'
    ];

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Description
     */
    public function contacts()
    {
        return $this->hasMany('App\NetworkMember');
    }

    /**
     * Description
     */
    public function parent_member()
    {
        return $this->belongsTo('App\NetworkMember', 'network_member_id');
    }

    /**
     * Network
     */
    public function network()
    {
        return $this->belongsTo('App\Network');
    }
}
