<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Language extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'locale', 'published',
    ];

    protected $table = 'translator_languages';

    public static function boot()
    {
        parent::boot();

        self::deleting(function (Language $language) {
            foreach ($language->translations as $translation) {
                $translation->delete();
            }
        });
    }


    //observe this model being deleted and delete the child events

    /**
     * Translations
     */
    public function translations()
    {
        return $this->hasMany('App\LanguageTranslation', 'locale', 'locale');
    }
}
