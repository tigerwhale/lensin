<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Lecture extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by',
        'course_subject_id',
        'name',
        'order',
        'description',
        'image',
        'published',
        'year',
        'author',
        'length',
        'unit',
        'language_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::deleting(function (Lecture $lecture) {
            foreach ($lecture->resources as $resource) {
                $resource->delete();
            }

            foreach ($lecture->contents as $content) {
                $content->delete();
            }
        });
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return isset($this->name) ? $this->name : trans('text.no_title');
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/lecture_big.png";
    }

    /**
     * Course Subject
     */
    public function course_subject()
    {
        return $this->belongsTo('App\CourseSubject');
    }


    public function name()
    {
        return isset($this->name) ? $this->name : "";
    }


    public function getName()
    {
        return isset($this->name) ? $this->name : "";
    }

    /**
     * Course
     */
    public function course()
    {
        return $this->course_subject->course();
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }


    /**
     * Get all of the lectures resources.
     */
    public function resources()
    {
        return $this->morphMany('App\Resource', 'resourcable');
    }

    /**
     * Get all of the lectures resources.
     */
    public function resources_published()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1);
    }

    public function orderedPublishedResources()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1)->orderBy('order');
    }

    public function contentText()
    {
        $contentText = "";
        foreach ($this->contents() as $content) {
            $contentText .= $content->text . "|";
        }
        return $contentText;
    }

    /**
     * Contents
     */
    public function contents()
    {
        return $this->hasMany('App\LectureContent');
    }

    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }

    /**
     * Platform URL
     */
    public function platformURL()
    {
        return url("/");
    }

    public function searchData()
    {
        // Contents of the lecture
        $contents = "";
        foreach ($this->contents as $content) {
            $contents .= $content->text . " | ";
        }

        $image = $this->image ? url($this->image) : "";
        return [
            'id' => $this->id,
            'title' => $this->name,
            'author' => $this->authors(),
            'country' => $this->countryName(),
            'year' => $this->year(),
            'language' => $this->languageName(),
            'type' => 'lecture',
            'platform' => str_replace('http://', "", url("/")),
            'platform_id' => server_property('server_id'),
            'published' => $this->published,
            'category' => "",
            'description' => $this->description,
            'image' => $this->getImage(),
            'created_at' => (array)$this->created_at,
            'created_at_formated' => $this->created_at->timestamp,
            'course' => $this->course_subject->course->name,
            'length' => $this->length,
            'state' => "",
            'institution' => "",
            'date' => $this->created_at->toDateTimeString(),
            'global' => "",
            'producer' => "",
            'criteria' => "",
            'contents' => $contents,
            'resource_type' => $this->resourcesTypes(),
            'designer' => ""
        ];
    }

    /**
     * Authors
     */
    public function authors()
    {
        // add authors
        $author = [];
        if (isset($this->course_subject->course)) {
            return $this->course_subject->course->authors();
        }
        return "";
    }

    /**
     * Country name
     */
    public function countryName()
    {
        return (isset($this->course_subject->course->country->name) ? $this->course_subject->course->country->name : "");
    }

    /**
     * Year
     */
    public function year()
    {
        return (isset($this->course_subject->course->year) ? $this->course_subject->course->year : "");
    }

    /**
     * Language Name
     */
    public function languageName()
    {
        return $this->getLanguagenameAttribute();
    }

    public function getLanguagenameAttribute()
    {
        if ($this->language_id) {
            if ($this->language) {
                return $this->language->name;
            }
        }
        return "";
    }

    /**
     * Image
     */
    public function getImage()
    {
        if ($this->image) {
            $image = url($this->image);
        } else {
            $image = url("/images/resource/lecture_big.png");
        }

        return $image;
    }


    //observe this model being deleted and delete the child events

    public function resourcesTypes()
    {
        $resourcesTypes = [];

        foreach ($this->resources as $resource) {
            array_push($resourcesTypes, $resource->resource_type->name);
        }

        $resourcesTypes = implode("|", $resourcesTypes);
        return $resourcesTypes;
    }
}
