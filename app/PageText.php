<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageText extends Model
{
    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    /**
     * Page Text
     */
    public function page()
    {
        return $this->hasMany('App\Page');
    }

    /**
     * Page Text
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'locale', 'locale');
    }
}
