<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Tutorial extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'created_by_id',
        'title',
        'author',
        'institution',
        'language_id',
        'category',
        'description',
        'image',
        'order',
        'published'
    ];


    protected $appends = ['server', 'serverid', 'languagename'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    public function getAuthorName()
    {
        if ($this->created_by()) {
            return $this->created_by->fullName();
        }
        return "";
    }

    public function content()
    {
        return $this->belongsTo('App\LectureContent');
    }

    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }

    public function name()
    {
        return $this->title;
    }

    public function languageName()
    {
        return $this->getLanguagenameAttribute();
    }

    public function getLanguagenameAttribute()
    {
        if ($this->language_id) {
            if ($this->language) {
                return $this->language->name;
            }
        }
        return "";
    }

    /**
     * Get all of the tutorial resources.
     */
    public function resources()
    {
        return $this->morphMany('App\Resource', 'resourcable');
    }

    /**
     * Year
     */
    public function year()
    {
        return $this->created_at->year;
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/tutorial_big.png";
    }

    public function orderedPublishedResources()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1)->orderBy('order');
    }

    /**
     * Image
     */
    public function getImage()
    {
        if ($this->image) {
            $image = url($this->image);
        } else {
            $image = url("/images/resource/tutorial_big.png");
        }

        return $image;
    }



    /**
     * Get all of the tutorial resources.
     */
    public function resources_published()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1);
    }

    public function tableData()
    {
        $image = $this->image ? url($this->image) : "";
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->getAuthorName(),
            'country' => "",
            'year' => $this->created_at->year,
            'language' => $this->languageName(),
            'type' => 'tutorial',
            'platform' => str_replace('http://', "", url("/")),
            'platform_id' => server_property('server_id'),
            'published' => $this->published,
            'category' => $this->category,
            'description' => $this->description,
            'image' => $image,
            'created_at' => (array)$this->created_at,
            'created_at_formated' => $this->created_at->timestamp,
            'course' => "",
            'length' => "",
            'state' => "",
            'institution' => $this->institution,
            'date' => $this->created_at->toDateTimeString(),
            'global' => "",
            'producer' => "",
            'criteria' => "",
            'contents' => "",
            'resource_type' => "",
            'designer' => ""
        ];
    }
}
