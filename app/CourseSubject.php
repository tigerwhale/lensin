<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class CourseSubject extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'course_id', 'name', 'order', 'published'
    ];

    public static function boot()
    {
        parent::boot();

        self::deleting(function (CourseSubject $course_subject) {
            foreach ($course_subject->lectures as $lecture) {
                $lecture->delete();
            }
        });
    }

    /**
     * Course
     */
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Lectures
     */
    public function lectures()
    {
        return $this->hasMany('App\Lecture')->orderBy('order');
    }


    //observe this model being deleted and delete the child events

    /**
     * Lectures published
     */
    public function lectures_published()
    {
        return $this->hasMany('App\Lecture')->orderBy('order')->where('published', 1);
    }
}
