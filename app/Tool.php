<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tool extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    public $badge = "badge-tool";
    public $classText = "text-tool";
    public $classBck = "bck-tool";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by',
        'title',
        'description',
        'published',
        'order',
        'filename',
        'category',
        'link',
        'image',
        'language_id',
        'author',
        'institution'
    ];

    protected $appends = ['server', 'serverid', 'languagename'];


    /**
     * @return string
     */
    public function getTitle()
    {
        return isset($this->title) ? $this->title : trans('text.no_title');
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/tool_big.png";
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Created By
     */
    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }


    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }

    public function languageName()
    {
        return $this->getLanguagenameAttribute();
    }

    public function getServerAttribute()
    {
        return url("/");
    }

    public function getServeridAttribute()
    {
        return server_property('server_id');
    }
    
    public function getLanguagenameAttribute()
    {
        if ($this->language_id) {
            if ($this->language) {
                return $this->language->name;
            }
        }
        return "";
    }
    
    public function imageUrl()
    {
        return $this->image ? url($this->image) : "";
    }


    public function searchData()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author,
            'country' => "",
            'year' => "",
            'language' => $this->languageName(),
            'type' => 'tool',
            'platform' => str_replace('http://', "", url("/")),
            'platform_id' => server_property('server_id'),
            'published' => $this->published,
            'category' => $this->category,
            'description' => $this->description,
            'image' => $this->imageUrl(),
            'created_at' => (array) $this->created_at,
            'created_at_formated' => $this->created_at->timestamp,
            'course' => "",
            'length' => "",
            'state' => "",
            'institution' => $this->institution,
            'date' => $this->created_at->toDateTimeString(),
            'global' => "",
            'producer' => "",
            'criteria' => "",
            'contents' => "",
            'resource_type'         => "",
            'designer'				=> ""
        ];
    }
}
