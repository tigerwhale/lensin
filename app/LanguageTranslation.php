<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class LanguageTranslation extends Model
{
    use RevisionableTrait;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locale', 'group', 'item', 'text'
    ];

    protected $table = 'translator_translations';

    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'locale', 'locale');
    }
}
