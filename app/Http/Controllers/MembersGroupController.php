<?php

namespace App\Http\Controllers;

use Log;
use App\User;
use Datatables;
use App\MembersGroup;
use Illuminate\Http\Request;
use \HttpOz\Roles\Models\Role;
use App\Mail\MembersGroupCreated;
use Illuminate\Support\Facades\Auth;

class MembersGroupController extends Controller
{
    public function create_member_tab(Request $request)
    {
        return view('users.profile_members_tab');
    }

    public function update_member(Request $request, MembersGroup $member_group)
    {
        $member_group->update($request->all());
        flash(trans('text.saved'));
        return redirect($request->redirect_page);
    }

    public function create(Request $request)
    {
        $membersGroup = new MembersGroup($request->all());
        $membersGroup->created_by_id = Auth::user()->id;
        $membersGroup->country_id = $request->country_id ? $request->country_id : 1;
        $membersGroup->partner = $request->partner ? 1 : null;
        $membersGroup->save();

        if ($request->image) {
            $this->uploadCoverImage($request, $membersGroup);
        }

        $membersGroup->members()->attach(Auth::user()->id);

        // Find administrators
        $adminRole = Role::where('slug', 'manageusers')->first();
        $admins = $adminRole->users;

        // Send emails
        foreach ($admins as $admin) {
            try {
                \Mail::to($admin)->send(new MembersGroupCreated());
            } catch (\Exception $e) {
                // Get error here
            }
        }

        flash(trans('users.member_group_created'))->success()->important();
        return back();
    }

    public function join(Request $request, User $user)
    {
        $this->validate($request, [
            'member_group_id' => 'required',
        ]);

        $user->members_groups()->syncWithoutDetaching([$request->member_group_id]);

        $members_group = MembersGroup::find($request->member_group_id);
        flash(trans('users.you_are_now_a_part_of') . " " . $members_group->title)->success()->important();

        return back();
    }


    public function uploadCoverImage($request, MembersGroup $members_group)
    {
        uploadCoverImage($request, $members_group);
    }

    /**
     * Datatable list
     * @return Collection
     */
    public function datatable_list(Request $request)
    {
        if ($request->partner) {
            $data = MembersGroup::where('partner', 1)->get();
        } else {
            $data = MembersGroup::whereNull('partner')->get();
        }
        return Datatables::of($data)->make(true);
    }

    public function backend(Request $request)
    {
        return view('bootstrap4.backend.members');
    }

    public function edit_modal(Request $request, MembersGroup $member_group)
    {
        return view('bootstrap4.backend.members_edit_modal', ['members_group' => $member_group]);
    }

    /**
     * PUBLISH
     */
    public function publish(Request $request, MembersGroup $members_group)
    {
        $members_group->published = $members_group->published ? null : 1;
        $members_group->save();

        return publishButton($members_group);
    }


    public function delete(Request $request, MembersGroup $members_group)
    {
        $members_group->members()->detach();
        $members_group->delete();
        return "OK";
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $MembersGroup = MembersGroup::find($request->id);
        $MembersGroup->$name = $request->value;
        $MembersGroup->save();

        return 'OK';
    }

    public function leave(Request $request)
    {
        Auth::user()->members_groups()->detach($request->member_id);
        return "OK";
    }


    /**
      * User list for network page
      * @return Datatables json
      */
    public function all_members_api()
    {
        // Get server properties
        $server = ["id" => server_property('server_id'),
                    "name" => server_property('platform_title')];
        // Get all local users
        $data = MembersGroup::where('published', 1)->get();
        $return_data = collect();

        foreach ($data as $item) {
            // skip admin
            if ($item->id != 1) {
                $country_name = isset($item->country) ? $item->country->name : "";
                $type_name = isset($item->types) ? $item->types->name : "";

                $item_to_push = collect([
                        'name' => $item->title,
                        'type' => $item->type,
                        'country' => $country_name,
                        'reference_person' => $item->reference_person,
                        'website' => $item->website,
                        'server_url' => url("/"),
                        'server_id' => $server['id'],
                        'server_name' => $server['name'],
                    ]);
                $return_data->push($item_to_push);
            }
        }

        return $return_data;
        //return Datatables::of($return_data)->make(true);
    }

    public function members_list(Request $request)
    {
        $members = MembersGroup::where('published', 1)->with('members')->get();
        return view('bootstrap4.staticPages.networkPage.membersList', ['members' => $members, 'request' => $request]);
    }

    public function partners_list(Request $request)
    {
        $members = MembersGroup::where('published', 1)->where('partner', 1)->with('members')->get();
        return view('bootstrap4.staticPages.networkPage.membersList', ['members' => $members, 'request' => $request, 'partners' => 1]);
    }

    public function upgrade_member_to_partner(Request $request, MembersGroup $member_group)
    {
        $member_group->partner = 1;
        $member_group->save();
        flash(trans('users.member_upgraded_to_partner'))->success()->important();
        return redirect('/backend/partners');
    }

    public function downgrade_partner_to_member(Request $request, MembersGroup $member_group)
    {
        $member_group->partner = null;
        $member_group->save();
        flash(trans('users.downgraded_partner_to_member'))->success()->important();
        return redirect('/backend/members');
    }
}
