<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Process\Exception\ProcessFailedException;

class UpdateController extends Controller
{

    /**
     * Migrate the database
     */
    public function migrate()
    {
        echo "Migration: " . Artisan::call('migrate', ['--force'=> true]);
    }

    /**
     * Update from git and migrate
     */
    public function update()
    {
        $process = new Process('git init');
        $process -> setWorkingDirectory(base_path());
        $process -> run(function ($type, $buffer) {
            echo $buffer . "<br>";
            echo $type . "<br>";
        });


        $process1 = new Process('git remote add origin https://bitbucket.org/tigerwhale/lensin.git');
        $process1 -> setWorkingDirectory(base_path());
        $process1 -> run(function ($type, $buffer) {
            echo $buffer . "<br>";
            echo $type . "<br>";
        });

        $git_fetch = env('PROXY_SERVER') ? 'git -c "http.proxy=' . env('PROXY_SERVER') . '" fetch --all' : 'git fetch --all';
        $process2 = new Process($git_fetch);
        $process2 -> setWorkingDirectory(base_path());
        $process2 -> setTimeout(3000);
        $process2 -> run(function ($type, $buffer) {
            echo $buffer . "<br>";
            echo $type . "<br>";
        });

        $process3 = new Process('git reset --hard origin/master');
        $process3 -> setWorkingDirectory(base_path());
        $process3 -> run(function ($type, $buffer) {
            echo $buffer . "<br>";
            echo $type . "<br>";
        });

        $this->migrate();
        Artisan::call('optimize');
        Artisan::call('cache:clear');
        //Artisan::call('route:cache');
        Artisan::call('view:clear');
        Artisan::call('config:cache');
        \TranslationCache::flushAll();
    }


    /**
     * Development Update
     */
    public function updateDev()
    {
        $this->update();
        echo "Seed: " . Artisan::call('db:seed');
    }


    public function createTestData()
    {
        \App\User::truncate();

        $user = \App\User::create([
                    "server_id" => "0",
                    "language" => "en",
                    "country_id" => "264",
                    "email" => "administrator@lens-international.org",
                    'password' => bcrypt("administrator"),
                    "password" => '$2y$10$FhLT6ow0YTaH3ALssYUHk.5Q.1EYNh4S2vEpndIWGFxrxnreyt.Pi',
                    "name" => 'Administrator',
                    "last_name" => 'Administrator',
                    "school" => 'Lens',
                    "address" => 'The World',
                    "user_type_id" => 1
                ]);

        $user->attachRole(role('managecourses'));
        $user->attachRole(role('managesite'));
        $user->attachRole(role('manageusers'));
        $user->attachRole(role('managestudycases'));
        $user->attachRole(role('managetools'));
        $user->attachRole(role('manageservers'));
        $user->attachRole(role('manageprojects'));
        $user->attachRole(role('managelanguages'));
        
        echo factory(\App\User::class, 50)->create();

        \App\Course::truncate();
        echo factory(\App\Course::class, 5)->create();

        \App\CourseSubject::truncate();
        echo factory(\App\CourseSubject::class, 10)->create();
        
        \App\Lecture::truncate();
        echo factory(\App\Lecture::class, 30)->create();

        \App\LectureContent::truncate();
        echo factory(\App\LectureContent::class, 100)->create();
        
        \App\Tool::truncate();
        echo factory(\App\Tool::class, 20)->create();
        
        \App\StudyCase::truncate();
        echo factory(\App\StudyCase::class, 20)->create();
        
        \App\Challenge::truncate();
        echo factory(\App\Challenge::class, 10)->create();
        
        \App\ChallengeGroup::truncate();
        echo factory(\App\ChallengeGroup::class, 30)->create();
    }
}
