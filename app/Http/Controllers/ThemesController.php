<?php

namespace App\Http\Controllers;

use Datatables;
use App\Theme;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ThemesController extends Controller
{

    /**
     * CREATE VIEW
     */
    public function create_view()
    {
        $view = view('themes.create');
        return $view;
    }


    /**
     * @param Request $request
     * @return string
     */
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, ['name' => 'required']);

        $server_id = (server_property('server_id') !== null) ? server_property('server_id') : 1;

        // Create the tool
        Theme::create([
            'name' => $request->name,
            'description' => $request->description,
            'created_by' => Auth::user()->id,
            'server_id' => $server_id,
        ]);

        return "OK";
    }

    /**
     * EDIT THEME PAGE
     * @param Request $request
     * @param Theme $theme
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Theme $theme)
    {
        return view('themes.edit', ['theme' => $theme]);
    }


    /**
     * EDIT THEME MODAL
     * @param Request $request
     * @param Theme $theme
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function edit_modal(Request $request, Theme $theme)
    {
        return view('themes.edit_modal', ['theme' => $theme]);
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $theme = Theme::find($request->id);
        $theme->$name = $request->value;
        $theme->save();

        return 'OK';
    }

    /**
     * List of themes to modify
     */
    public function modify()
    {
        return view('themes.modify');
    }


    /**
     * Data table for modify
     */
    public function data_table_modify()
    {
        $data = Theme::all();
        return Datatables::of($data)->make(true);
    }


    /**
     * Delete the theme
     */
    public function delete(Request $request, Theme $theme)
    {
        // delete the theme
        $theme->delete();
        // return back
        return "OK";
    }

    /**
     * Edit page
     * @return view
     */
    public function edit_page()
    {
        return view('themes.modify');
    }
}
