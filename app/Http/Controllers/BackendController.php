<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Image;
use Storage;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    
    /**
     * Front of backend / USERS
     */
    public function backend()
    {
        // If user has one of admin groups enter backend
        if (Auth::user()->hasGroup('admin')) {
            return view('bootstrap4.backend.welcome');
        }

        // Go to login
        flash(trans('text.not_authorised'))->important();
        return redirect(url('/login'));
    }

    // USERS

    /**
     * User list
     */
    public function users_list()
    {
        // TODO remove return view('backend.users');
        return view('bootstrap4.backend.users');
    }

    /**
     * Edit user
     */
    public function user_edit(Request $request, User $user)
    {
        $countries = \App\Country::all()->pluck('name', 'id');
        $user_types = \App\UserType::all()->pluck('name', 'id');
        $user_priviledges = \HttpOz\Roles\Models\Role::all();

        return view('bootstrap4.backend.users_edit', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types, 'user_priviledges' => $user_priviledges]);
        // TODO remove return view('backend.users_edit', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types, 'user_priviledges' => $user_priviledges]);
    }


    /**
     * SERVER PROPERTIES
     */
    public function general()
    {
        $properties = \App\ServerProperties::all();
        return view('bootstrap4.backend.site', ['properties' => $properties]);
    }


    /**
     * UPDATE SITE PROPERTIES
     */
    public function general_update(Request $request)
    {
        $property = \App\ServerProperties::where('name', 'LIKE', $request->name)->first();
        $property->data = $request->data;
        $property->save();

        flash(trans('text.saved'))->important();
        return back();
    }



    public function upload_logo(Request $request, $path)
    {
        // Validate the request
        $this->validate($request, ['image' => 'required']);

        if (!file_exists(public_path() . '/images/server/')) {
            mkdir(public_path() . '/images/server/', 0777, true);
        }

        // read image from temporary file
        $img = Image::make($request->image);

        if ($path == 'logo') {
            $image_path = '/images/logo.png';
        } elseif ($path == 'round_logo') {
            $image_path = '/images/server/logo.png';
        } elseif ($path == 'round_logo_transparent') {
            $image_path = '/images/server/logo_transparent.png';
        } elseif ($path == 'logo_dark') {
            $image_path = '/images/logo_dark.png';
        }

        // save image
        $img->save(public_path() . $image_path);

        flash(trans('text.logo_updated'))->important();
        return back();
    }


    /**
     * Register a platform, return languages
     * @param  Request $request server_address
     * @return json           server_id, languages, translations
     */
    public function register_platform(Request $request)
    {
        set_time_limit(600);
        // validate the request
        $this->validate($request, ['server_address' => 'required']);

        // get the server srom the list if it is registered
        $server_registering = \App\ServerList::where('address', 'LIKE', $request->server_address)->first();
        
        // on error return
        if (!$server_registering) {
            return "no_such_server";
        }
        // put the id in return data
        $return_data = ['server_id' => $server_registering->id];

        return $return_data;
    }



    /**
     * Request for registering the platform on the central server
     */
    public function register_platform_request()
    {
        // make the request url
        $central_server_url = server_property('central_server_address') . "/api/backend/register_platform?server_address=" . url('/');

        // get the data from the central server
        if (!$data = json_decode(file_get_contents($central_server_url))) {
            // return the error
            flash(trans('backend.error_geting_data_from_central_server') . $central_server_url)->important();
            return back();
        }

        // set server id
        $server_id = \App\ServerProperties::where('name', 'LIKE', 'server_id')->first();
        $old_server_id = $server_id->data;
        $server_id->data = $data->server_id;
        $server_id->save();

        // LANGAUGES
        if ($data->languages && $data->translations) {

            // Delete old translations
            $old_translations = \App\LanguageTranslation::where('id', '>', 0)->forceDelete();

            // delete old languages
            $old_languages = \App\Language::where('id', '>', 0)->forceDelete();
            
            // create new languages
            foreach ($data->languages as $language) {
                \App\Language::create([
                        'id' => $language->id,
                        'name' => $language->name,
                        'locale' => $language->locale,
                        'published' => $language->published
                        ]);
            }

            // create new translations
            foreach ($data->translations as $translation) {
                \App\LanguageTranslation::create([
                        'id' => $translation->id,
                        'locale' => $translation->locale,
                        'namespace' => $translation->namespace,
                        'group' => $translation->group,
                        'item' => $translation->item,
                        'text' => $translation->text,
                        'unstable' => $translation->unstable,
                        'locked' => $translation->locked
                        ]);
            }
        }
        
        // clear the cache
        \Artisan::call('cache:clear');

        $affected = DB::table('challenges')->update(array('server_id' => $data->server_id));
        $affected = DB::table('challenge_groups')->update(array('server_id' => $data->server_id));
        $affected = DB::table('courses')->update(array('server_id' => $data->server_id));
        $affected = DB::table('news')->update(array('server_id' => $data->server_id));
        $affected = DB::table('study_cases')->update(array('server_id' => $data->server_id));
        $affected = DB::table('themes')->update(array('server_id' => $data->server_id));
        $affected = DB::table('users')->where('server_id', $old_server_id)->update(array('server_id' => $data->server_id));

        flash(trans('backend.server_connected_translations_pulled'));
        return back();
    }


    /**
     * Sycnhronize languages and translations
     */
    public function synchronize(Request $request)
    {
        // parse data sent
        if (!$translation_data = json_decode($request->translation_data)) {
            // return the error
            return "ERR PARSING DATA!";
        }

        $translations_data = $translation_data->translations;
        $language_data = $translation_data->language;

        // TODO - CHECK IF LANGUAGE EXISTS AND ADD IF NO
        $language = \App\Language::where('locale', 'LIKE', $language_data->locale)->first();
        if ($language) {
            $language->name = $language_data->name;
            $language->locale = $language_data->locale;
            $language->published = $language_data->published;
        } else {
            $language = \App\Language::create([
                'name' => $language_data->name,
                'locale' => $language_data->locale,
                'published' => $language_data->published
                ]);
        }
        $language->save();

        // create new translations
        foreach ($translations_data as $translation) {
            \App\LanguageTranslation::where('locale', 'LIKE', $translation->locale)->where('item', 'LIKE', $translation->item)->where('group', 'LIKE', $translation->group)->forceDelete();

            DB::table('translator_translations')->insert(
                [
                'locale' => $translation->locale,
                'namespace' => $translation->namespace,
                'group' => $translation->group,
                'item' => $translation->item,
                'text' => $translation->text,
                'unstable' => $translation->unstable,
                'locked' => $translation->locked
                ]
            );
        };

        \TranslationCache::flushAll();
        return "OK";
    }

    public function uploadHomeVideo(Request $request)
    {
        // Validate the request
        $this->validate($request, ['home_video' => 'required']);

        // Check for video extension
        $file_extension = explode(".", $request->home_video->getClientOriginalName());

        if (end($file_extension) != "mp4") {
            flash(trans('backend.home_video_must_be_mp4_format'));
            return back();
        }

        // Delete old video if exists
        if (file_exists(public_path() . '/images/home_video.mp4')) {
            unlink(public_path() . '/images/home_video.mp4');
        }

        // Save video
        Storage::disk('public')->putFileAs("/images", $request->home_video, 'home_video.mp4');
        set_server_property('home_video', '/images/home_video.mp4');

        flash(trans('backend.home_video_updated'))->important();
        return back();
    }


    /**
     * Upload Home video poster
     * @param  Request $request
     * @return back
     */
    public function uploadHomeVideoPoster(Request $request)
    {
        $this->validate($request, ['home_video_poster' => 'required']);
        $image_path = public_path() . '/images/home_video_poster.jpg';

        // Delete old image if exists
        if (file_exists($image_path)) {
            unlink($image_path);
        }

        // resize and save image
        $image = Image::make($request->home_video_poster)->fit(1024, 540)->save($image_path);
        set_server_property('home_video_poster', '/images/home_video_poster.jpg');

        flash(trans('text.home_video_poster_uploaded'))->important();
        return back();
    }

    /**
     * Delete home video poster
     * @param  Request $request
     * @return back()
     */
    public function deleteHomeVideoPoster(Request $request)
    {
        set_server_property('home_video_poster', null);
        flash(trans('text.home_video_poster_deleted'))->important();
        return back();
    }

    /**
     * Set default home page video
     * @param  Request $request
     * @return back()
     */
    public function defaut_home_video(Request $request)
    {
        set_server_property('home_video', '/images/home.mp4');
        flash(trans('backend.home_video_set_to_default'))->important();
        return back();
    }

    public function delete_home_video(Request $request)
    {
        set_server_property('home_video', '');
        flash(trans('backend.home_video_deleted'))->important();
        return back();
    }

    /**
     * Register a new network
     * @param  Request $request
     * @return view
     */
    public function register_new_network(Request $request)
    {
    }
}
