<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function homePageOld()
    {
        $news = News::where('published', 1)->get();
        return view('home.index', ['news' => $news, 'home_page' => true]);
    }


    /**
     * New Home Page
     */
    public function newHomePage()
    {
        $languages = \App\Language::where('published', 1)->get();
        $forbiddenNews = $this->getForbiddenNews();

        return view('bootstrap4.homePage.index', ['home_page' => true, 'languages' => $languages, 'page' => 'home', 'forbiddenNews' => $forbiddenNews]);
    }


    /**
     * Forbidden News array
     * @return string
     */
    public function getForbiddenNews()
    {
        $forbiddenNews = "";

        if (server_property('central_server')) {
            $forbiddens = \App\NewsForbidden::all();
            foreach ($forbiddens as $forbidden) {
                $forbiddenNews .= '"' . $forbidden->news_id . "-" . $forbidden->server_id . '",';
            }
        }

        return $forbiddenNews;
    }

    /**
     * Search Page
     */
    public function searchPage()
    {
        $languages = \App\Language::where('published', 1)->get();
        return view('bootstrap4.searchPage.index', ['page' => 'search', 'languages' => $languages]);
    }


    /**
     * Return emtpy page
     */
    public function empty_page()
    {
        return view('empty_page');
    }

    /**
     * view changelog
     */
    public function changelog()
    {
        return nl2br(file_get_contents(public_path('changelog.txt')));
    }
}
