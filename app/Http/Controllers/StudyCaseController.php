<?php

namespace App\Http\Controllers;

use File;
use Image;
use Storage;
use Datatables;
use App\StudyCase;
use App\Resource;
use Chumper\Zipper\Zipper;
use App\StudyCaseGuideline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class StudyCaseController extends Controller
{
    /**
     * DISPLAYS THE CREATE STUDY CASE PAGE
     */
    public function createPage(Request $request)
    {
        // Create the study case
        $study_case = $this->create($request);
        // Return the edit page
        return redirect('/study_cases/edit/' . $study_case->id);
    }


    /**
     * Create study case
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {
        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            'server_id' => server_property('server_id'),
            'country_id' => Auth::user()->country_id,
            'year' => date("Y")
            ];

        // Create the study case
        $study_case = StudyCase::create($request->all() + $additional_data);

        // save she study case
        $study_case->save();

        return $study_case;
    }


    /**
     * MODIFY LIST
     */
    public function modify(Request $request)
    {
        return view('study_cases.modify');
    }

    
    /**
     * All srudy cases list
     */
    public function datatable_list(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        return Datatables::of($data)->make(true);
    }


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // get courses
        if ($published == true) {
            $study_cases = \App\StudyCase::where('published', 1)->get();
        } else {
            $study_cases = \App\StudyCase::all();
        }

        if (!$study_cases->count() > 0) {
            return false;
        }
        
        // create a data array
        $data = $study_cases->map(function ($item) {
            $image = $item->image ? url($item->image) : "";
            return [
                    'id' 					=> $item->id,
                    'title' 				=> $item->name,
                    'author' 				=> $item->author,
                    'country'				=> "", //$item->country->name,
                    'year' 					=> $item->year,
                    'language' 				=> $item->languageName,
                    'type' 					=> 'study_case',
                    'platform' 				=> str_replace('http://', "", url("/")),
                    'platform_id' 			=> server_property('server_id'),
                    'published' 			=> $item->published,
                    'category' 				=> $item->category,
                    'description' 			=> $item->description,
                    'image' 				=> $image,
                    'created_at' 			=> (array) $item->created_at,
                    'created_at_formated' 	=> $item->created_at->timestamp,
                    'course' 				=> "",
                    'length' 				=> "",
                    'state' 				=> $item->state,
                    'institution' 			=> "",
                    'date' 					=> "",
                    'global' 				=> "",
                    'producer' 				=> $item->producer,
                    'criteria' 				=> '',
                    'contents' 				=> "",
                    'resource_type' 		=> "",
                    'designer'				=> $item->designer
                ];
        });

        return $data;
    }


    /**
     * PUBLISH
     */
    public function publish(Request $request, StudyCase $study_case)
    {
        $study_case->published = $study_case->published ? null : 1;
        $study_case->save();

        return publishButton($study_case);
    }

    /**
     * DELETE STUDY CASE
     */
    public function delete(Request $request, StudyCase $study_case)
    {
        $study_case->delete();

        flash(trans('text.study_case_deleted'))->important();
        return back();
    }


    /**
     * DISPLAY THE EDIT STUDY CASE PAGE
     */
    public function edit(Request $request, StudyCase $study_case)
    {
        $countries = \App\Country::all()->pluck('name', 'id');
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        $guidelines = \App\StudyCaseGuideline::orderBy('level', 'order')->where('level', 0)->get();

        return view('study_cases.edit', ['guidelines' => $guidelines, 'study_case' => $study_case, 'countries' => $countries, 'languages' => $languages]);
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $study_case = StudyCase::find($request->id);
        $study_case->$name = $request->value;
        $study_case->save();

        return 'OK';
    }



    /**
     * STUDY CASE VIEW
     */
    public function view(Request $request, $study_case_id)
    {
        $languages = \App\Language::where('published', 1)->get();
        $resourceViewApiUrl = server_url($request->server_id) . '/api/study_cases/view/' . $study_case_id;
        return view(
            'bootstrap4.resources.index',
            [
                'resourceViewApiUrl' => $resourceViewApiUrl,
                'resource_type' => 'study_case',
                'languages' => $languages,
                'page' => 'study_cases_view']
            );
    }

    /**
     * Bootstrap 4 view of study case
     */
    public function viewApi(Request $request, StudyCase $study_case)
    {
        increment_view_counter($study_case);
        return view('bootstrap4.resources.study_case.view_api', ['study_case' => $study_case, 'request' => $request]);
    }


    public function viewCriteriaTab(Request $request)
    {
        $study_case_guidelines = $request->data;

        if ($study_case_guidelines) {
            $study_case_guidelines = json_decode(json_encode($study_case_guidelines), false);
            return view('bootstrap4.searchPage.view_criteria_tab', ['study_case_guidelines' => $study_case_guidelines, 'case_index' => 0, 'server_id' => server_property('server_id')]);
        }
        return trans('criteria.no_cirteria');
    }

    /**
     * OLD
     * API DATA FOR STUDY CASE VIEW
     */
    public function view_api(Request $request, $study_case_id)
    {
        $study_case = StudyCase::where('id', $study_case_id)->with('language', 'guidelines.parent', 'guidelines_level_0', 'guidelines_level_0.children', 'guidelines_level_0.children.children', 'guidelines_level_0.children.children.children', 'resources')->get();
        return $study_case;
    }


    /**
     * UPDATE REPORT
     */
    public function uploadReport(Request $request, StudyCase $study_case)
    {
        $this->validate($request, ['report_file' => 'file|required']);

        // Handle file
        $file_path = '/resources/studycase/' . $study_case->id . '/report';
    
        // Make folder if not exist
        if (!file_exists(public_path() . $file_path)) {
            mkdir(public_path() . $file_path, 0777, true);
        }

        // save the file
        Storage::disk('public')->putFileAs($file_path, $request->report_file, $request->report_file->getClientOriginalName());

        // update the resource
        $study_case->report = $file_path . "/" . $request->report_file->getClientOriginalName();
        $study_case->save();

        return "OK";
    }


    /**
     * Delete the report preview the study case
     */
    public function deleteReportFile(Request $request, StudyCase $study_case)
    {
        $study_case->report = "";
        $study_case->save();
        return "OK";
    }

    /**
     * Upload resource file
     */
    public function upload_file(Request $request, StudyCase $study_case)
    {
        $all_files = $request->resource_file;
        $base_folder = "/resources/studycase/" . $study_case->id . "/";

        // SINGLE FILE
        if (count($request->resource_file) == 1) {
            // set the path
            $full_path = $base_folder . $all_files[0]->getClientOriginalName();
            $filename = $full_path;

            $success = File::cleanDirectory($base_folder);
                
            // Store the file
            Storage::disk('public')->putFileAs($base_folder, $all_files[0], $all_files[0]->getClientOriginalName());

            // Update the file list
            $file_list = Storage::allFiles($base_folder);
            $study_case->filename = $filename;

            $study_case->save();

            return json_encode(['single', $study_case->id ]);
        } else {
            $upload_file_folders = explode(",", $request->paths);
            $file_to_return = [];
            $i = 0;

            foreach ($all_files as $tempFile) {
                $ds = DIRECTORY_SEPARATOR;
                  
                $targetPath = dirname(__FILE__) . $ds . $base_folder . $ds;
                
                $fullPath = $base_folder.rtrim($upload_file_folders[$i], "/.");
                $folder = substr($fullPath, 0, strrpos($fullPath, "/"));

                if (!is_dir($folder)) {
                    $old = umask(0);
                    mkdir($folder, 0777, true);
                    umask($old);
                }
                
                Storage::disk('public')->putFileAs($folder, $tempFile, $tempFile->getClientOriginalName());
                
                array_push($file_to_return, url($folder . "/" . $tempFile->getClientOriginalName()));
                $i++;
            }

            $files_to_zip = glob($base_folder . "/*");
            
            $zip_file = $base_folder . 'lens_study_case_' . $study_case->id .  '.zip';

            // Make a zip
            $zipper = new Zipper();
            $zipper->make($zip_file)->add($files_to_zip);

            $study_case->filename = $zip_file;
            $study_case->save();
            
            // return json array to preview
            return json_encode(['multiple', $study_case->id, implode(',', $file_to_return)]);
        }
    }


    public function fileList(Request $request, StudyCase $study_case)
    {
        return view('study_cases.edit_report_file', ['study_case' => $study_case]);
    }

    
    public function imageListEdit(Request $request, StudyCase $study_case)
    {
        return view('study_cases.edit_image_list', ['study_case' => $study_case]);
    }


    /**
     * Create StudyCase Resource
     * @return int 	course subject id
     */
    public function createResource(Request $request, StudyCase $study_case)
    {
        $orderResource = $study_case->resources()->orderBy('order', 'desc')->first();
        $order = 1;
        if (!$orderResource) {
            $order = 1;
        } else {
            $order = $orderResource->order + 1;
        }

        $resource = Resource::create([
            'created_by' => Auth::user()->id,
            'resourcable_id' => $study_case->id,
            'resourcable_type' => 'App\StudyCase',
            'order' => $order,
            'resource_type_id' => 5
        ]);

        $resource->author = Auth::user()->name . " " . Auth::user()->last_name;
        $resource->year = date('Y');
        $resource->name = $study_case->name;
        $resource->save();

        return $resource;
    }


    public function createResourceWithImage(Request $request, StudyCase $study_case)
    {
        // Handle image
        if ($request->image) {

            // create a resource
            $resource = $this->createResource($request, $study_case);

            $imagePath = '/resources/studycase/' . $study_case->id . '/images';
            $imageFullPath = $imagePath . '/' . $request->image->getClientOriginalName();
            
            // Make folder if not exist
            if (!file_exists(public_path() . $imagePath)) {
                mkdir(public_path() . $imagePath, 0777, true);
            }

            // save image
            Storage::disk('public')->putFileAs($imagePath, $request->image, $request->image->getClientOriginalName());

            // update resource
            $resource->path = $imageFullPath;
            $resource->save();

            return "OK";
        }
        return "err";
    }

    public function imageSlideshow(Request $request, StudyCase $study_case)
    {
        return view('study_cases.image_slideshow', ['study_case' => $study_case]);
    }


    /**
     * Delete image resource
     * @param  Request $request
     * @return OK
     */
    public function deleteImage(Request $request)
    {
        $resource = Resource::find($request->id);
        $resource->delete();
        return "OK";
    }


    public function increment_download(Request $request, StudyCase $study_case)
    {
        increment_download_counter($study_case, $request);
        return (url($study_case->report));
    }

    public function select_by_criteria(Request $request)
    {
        return view('study_cases.select_by_criteria');
    }


    public function select_by_criteria_data()
    {
        $study_case_guidelines = StudyCaseGuideline::where('level', 0)->with(
            'licences',
            'children.children.children',
            'study_cases.resources',
            'children.study_cases.resources',
            'children.children.study_cases.resources',
            'children.children.children.study_cases.resources'
        )->get();
        return $study_case_guidelines;
    }


    public function select_by_criteria_view(Request $request)
    {
        if ($request->data) {
            if ($study_case_guidelines = json_decode($request->data)) {
                return view('study_cases.select_by_criteria_view', ['study_case_guidelines' => $study_case_guidelines, 'case_index' => 0 ]);
            }
        }
        return trans('criteria.no_cirteria');
    }


    public function criteria_tab(Request $request)
    {
        $criteria = StudyCaseGuideline::where('id', $request->data)->with('children', 'children.children', 'children.children.children')->get();
        $criteria = $this->return_children_id($criteria);
        $studyCases = StudyCase::where('published', 1)->with('resources')->whereHas('guidelines', function ($query) use ($criteria) {
            $query->whereIn('study_case_guidelines.id', $criteria);
        })->get();

        return view('bootstrap4.searchPage.select_by_criteria_tab', ['studyCases' => $studyCases]);
    }


    public function return_children_id($criteria, $return_data = [])
    {
        foreach ($criteria as $child) {
            array_push($return_data, $child->id);
            if (isset($child->children)) {
                $return_data = $this->return_children_id($child->children, $return_data);
            }
        }
        return $return_data;
    }

    /**
     * Export data to Json for Study Case offline
     * @return json
     */
    public function exportToJsonForOffline()
    {
        $studyCases = StudyCase::where('published', 1)->get();
        $studyCaseGuidelines = $this->select_by_criteria_data();

        $json = 'var studyCases = ' . $studyCases->toJson() . '; ';
        $json .= "var studyCaseGuidelines = " . $studyCaseGuidelines->toJson() . ";";
        Storage::disk('public')->put('/download/offline/js/data.js', $json);

        return $studyCases;
    }


    public function collectZipFilesForOffline()
    {
        $studyCases = $this->exportToJsonForOffline();
        $files_to_zip = [];

        $zip_filename = "lens_offline_" . time() . ".zip";
        copy('./download/offline/offline.zip', './download/' . $zip_filename);

        $zipper = new \Chumper\Zipper\Zipper;
        foreach ($studyCases as $studyCase) {
            // add cover image
            if ($studyCase->image) {
                $zipper->zip('./download/' . $zip_filename)->folder('resources/studycase/' . $studyCase->id . "/cover")->add("." . $studyCase->image);
            }
            
            // add resource files
            foreach ($studyCase->resources as $resource) {
                $zipper->zip('./download/' . $zip_filename)->folder('resources/studycase/' . $resource->id)->add("." . $resource->path);
            }
        }

        $zipper->zip('./download/' . $zip_filename)->folder('js')->add("./download/offline/js/data.js");
        return url('download/' . $zip_filename);
    }
}
