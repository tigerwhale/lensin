<?php

namespace App\Http\Controllers;

use Image;
use App\Course;
use App\CourseSubject;
use App\Lecture;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\CourseSubjectController;
use App\Http\Controllers\LectureController;

class CourseController extends Controller
{
    public function fix_courses()
    {
        $courses = Course::all();
        foreach ($courses as $course) {
            if (is_numeric($course->language_id)) {
                $course->language_id = "en";
                $course->save();
                echo($course->id);
            }
        }
    }

    /**
     * CREATE COURSE
     */
    public function create(Request $request)
    {
        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            'server_id' => server_property('server_id'),
            'country_id' => Auth::user()->country_id,
            'language_id' => "en",
            'school' => Auth::user()->school,
            'year' => date('Y')
            ];

        // Create Course
        $course = Course::create($request->all() + $additional_data);
    
        // Add a default teacher
        $course->teachers()->attach([ Auth::user()->id ]);

        // Add a subject
        $course_subject = (new CourseSubjectController)->create($request, $course);
        $course_subject->save();

        // Add a Lecture
        $lecture = (new LectureController)->create($request, $course_subject);
        $lecture->save();
        
        return $course;
    }

    /**
     * Create Page
     * @param  Request $request
     * @return Redirect to edit page
     */
    public function createPage(Request $request)
    {
        $course = $this->create($request);
        return redirect('/courses/edit/' . $course->id);
    }


    /**
     * DELETE THE COURSE
     */
    public function delete(Request $request, Course $course)
    {
        // delete the course
        $course->delete();

        // if delete from create mode
        if (isset($_GET['cancel'])) {
            flash(trans('text.course_canceled'))->important();
            // return home
            return redirect("/");
        } else {
            flash(trans('text.deleted'))->important();
            return "OK";
        }
    }

    /**
     * UPDATE FROM INPUT FIELDS
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $course = Course::find($request->id);
        $course->$name = $request->value;
        $course->save();

        return 'OK';
    }


    /**
     * PUBLISH
     */
    public function publish(Request $request, Course $course)
    {
        $course->published = $course->published ? null : 1;
        $course->save();

        return publishButton($course);
    }


    // ---- VIEWS ---- //

   
    /**
     * VIEW THE COURSE
     */
    public function view(Request $request, $course_id)
    {
        $languages = \App\Language::where('published', 1)->get();
        $resourceViewApiUrl = server_url($request->server_id) . '/api/courses/view/' . $course_id;
        return view(
            'bootstrap4.resources.index',
            [
                    'resourceViewApiUrl' => $resourceViewApiUrl,
                    'resource_type' => 'course',
                    'languages' => $languages,
                    'page' => 'course_view']
                );
    }

    /**
     * Course view API
     */
    public function viewApi(Request $request, Course $course)
    {
        if (!$course) {
            return "This resource does not exist";
        }
            
        increment_view_counter($course);
        return view('bootstrap4.resources/course/view_api', ['course' => $course, 'request' => $request]);
    }

    /**
     * COURSES EDIT LIST VIEW
     */
    public function modify(Request $request)
    {
        return view('courses.edit.courses_list');
    }

    /**
     * EDIT COURSE PAGE VIEW
     */
    public function edit(Request $request, Course $course)
    {
        $countries = \App\Country::all()->pluck('name', 'id');
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
            
        return view('courses.edit.course_edit', ['course' => $course, 'countries' => $countries, 'languages' => $languages]);
    }

    /**
     * EDIT COURSE LIST VIEW
     */
    public function editListView(Request $request, Course $course)
    {
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('courses.edit.courseware.edit_list_view', ['course' => $course, 'languages' => $languages]);
    }

    /**
     * EDIT COURSE GRID VIEW
     */
    public function editGridView(Request $request, Course $course)
    {
        return view('courses.edit.courseware.edit_grid_view', ['course' => $course])->render();
    }

    /**
     * COURSEWARE GRID VIEW FRONTEND
     */
    public function coursewareViewGridApi(Request $request, $course_id, $server_id)
    {
        $courseware_data = $this->course_data($request, $course_id);
            
        if (!$courseware_data) {
            return "No such course!";
        }

        // Return view
        return view('bootstrap4.resources.course.courseware_grid', ['course' => $courseware_data[0], 'user_id' => $request->user_id]);
        // return view('courses.view.courseware_grid', ['course' => $courseware_data[0], 'user_id' => $request->user_id]);
    }

    /**
     * COURSEWARE LIST VIEW FRONTEND
     */
    public function coursewareViewListApi(Request $request, $course_id, $server_id)
    {
        $courseware_data = $this->course_data($request, $course_id);

        // If no view return message
        if (!$courseware_data) {
            return "No such course!";
        }

        return view('bootstrap4.resources.course.courseware_list', ['course' => $courseware_data[0], 'user_id' => $request->user_id]);
    }


    // ---- COVER IMAGE ---- //
    
    /**
     * UPDATE COURSE IMAGE
     */
    public function uploadCoverImage(Request $request, Course $course)
    {
        // Handle image
        if ($request->image) {
                
                // read image from temporary file
            $img = Image::make($request->image);

            // resize image
            $img->resize(800, 800, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });
            ;

            $image_path = '/images/courses/' . $course->id . '/cover/' . $course->id . "_" . time() . '.jpg';
                
            // Make folder if not exist
            if (!file_exists(public_path() . '/images/courses/' . $course->id . '/cover')) {
                mkdir(public_path() . '/images/courses/' . $course->id . '/cover', 0777, true);
            }

            // save image
            $img->save(public_path() . $image_path);

            $course->image = $image_path;
            $course->save();
        }

        // Return OK if all is well
        return "OK";
    }

    public function loadCoverImageForEdit(Request $request, Course $course)
    {
        $returnData = "";
        if ($course->image) {
            $returnData = view('courses.edit.course_edit_cover_image', ['course' => $course]);
        }
            
        return $returnData;
    }

    /**
     * Removes the course image
     * @param  Request $request
     * @param  Course  $course
     * @return string "OK"
     */
    public function removeCoverImage(Request $request, Course $course)
    {
        $course->image = "";
        $course->save();
        return "OK";
    }


    // ---- DATA ---- //

    /**
     * Course JSON
     */
    public function course_data(Request $request, $course_id)
    {
        $course = Course::where('id', $course_id)
                ->with('language', 'teachers', 'country', 'subjects_published', 'subjects_published.lectures_published', 'subjects_published.lectures_published.resources_published', 'subjects_published.lectures_published.resources_published.resource_type')
                ->get();

        return $course;
    }


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // get courses
        if ($published == true) {
            $courses = \App\Course::where('published', 1)->get();
        } else {
            $courses = \App\Course::all();
        }

        if (!$courses->count() > 0) {
            return false;
        }

        $data = $this->mapForDatatables($courses);
            
        return $data;
    }


    /**
     * ALL COURSES LIST
     */
    public function datatable_list(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        return Datatables::of($data)->make(true);
    }

    public function mapForDatatables($courses)
    {
        $data = $courses->map(function ($item) {
            $country_name = ($item->country_id) ? $item->country->name : "";
            $language_name = (($item->language_id) ? $item->language->name : "");
            $image = $item->image ? url($item->image) : "";
                
            return [
                        'id' 					=> $item->id,
                        'title'					=> $item->name,
                        'author' 				=> $item->authors(),
                        'country' 				=> $country_name,
                        'year' 					=> $item->year,
                        'language' 				=> $language_name,
                        'type' 					=> 'course',
                        'platform' 				=> str_replace('http://', "", url("/")),
                        'platform_id' 			=> server_property('server_id'),
                        'published' 			=> $item->published,
                        'category' 				=> "",
                        'description' 			=> "",
                        'image' 				=> $image,
                        'created_at' 			=> (array) $item->created_at,
                        'created_at_formated' 	=> $item->created_at->timestamp,
                        'course' 				=> "",
                        'length' 				=> $item->duration,
                        'state' 				=> "",
                        'institution' 			=> $item->school,
                        'date' 					=> $item->created_at->toDateTimeString(),
                        'global' 				=> "",
                        'producer' 				=> "",
                        'criteria' 				=> $item->criteria,
                        'contents' 				=> "",
                        'resource_type' 		=> "",
                        'designer'				=> ""
                    ];
        });

        return $data;
    }

    // ---- AUTHORS ---- //

    /**
     * Add teacher to course
     */
    public function addAuthor(Request $request)
    {
        // Validate the request
        $this->validate(
            $request,
            [
                    'course_id' => 'required',
                    'author_id' => 'required'
                    ]
            );

        // get the models
        $user = \App\User::find($request->author_id);
        $course = Course::find($request->course_id);

        // attach author to course
        if (!$course->teachers->contains($user->id)) {
            $result = $course->teachers()->attach([$user->id]);
            return "OK";
        } else {
            return null;
        }
    }
        
    /**
     * Remove teacher from course
     */
    public function removeAuthor(Request $request)
    {
        // Validate the request
        $this->validate(
            $request,
            [
                    'course_id' => 'required',
                    'author_id' => 'required'
                    ]
            );

        $course = Course::find($request->course_id);

        // attach author to course
        if ($course->teachers->contains($request->author_id)) {
            $result = $course->teachers()->detach([$request->author_id]);
            return 'true';
        } else {
            return 'false';
        }
    }
        
    /**
     * Course Authors list
     */
    public function loadCourseAuthorsForEdit(Request $request, Course $course)
    {
        return view('courses.edit.course_edit_authors', ['course' => $course]);
    }
        
    /**
     * Modal for adding authors
     */
    public function addAuthorModal(Request $request, Course $course)
    {
        return view('courses.edit.modals.authors', ['course' => $course]);
    }
}
