<?php

namespace App\Http\Controllers;

use Session;
use App\Language;
use App\LanguageTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{

    /**
     * LIST OF LANGUAGES
     */
    public function index()
    {
        // get all languages
        $languages = Language::orderBy('published', 'DESC')->orderBy('name', 'ASC')->get();
        $platforms = \App\ServerList::where('enabled', 1)->get();

        // return the view
        return view('bootstrap4.backend.languages_index', ['languages' => $languages, 'platforms' => $platforms]);
    }

    /**
     * CREATE THE LANGUAGE
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, [
                'locale' => 'required',
                'name' => 'required',
            ]);

        if (Language::where('locale', 'LIKE', $request->locale)->orWhere('name', 'LIKE', $request->name)->count() > 0) {
            flash(trans('text.already_exists'))->important();
            return back();
        }

        // Create the language
        $language = Language::create($request->all());

        // copy English translations
        $translations = LanguageTranslation::where('locale', 'LIKE', 'en')->get();

        foreach ($translations as $translation) {
            LanguageTranslation::create([
                    'locale' => $language->locale,
                    'namespace' => $translation->namespace,
                    'group' => $translation->group,
                    'item' => $translation->item,
                    'text' => $translation->text,
                    'unstable' => $translation->unstable,
                    'locked' => $translation->locked
                ]);
        }

        $language->save();

        flash(trans('text.created'))->important();
        return back();
    }


    /**
     * UPDATE THE LANGUAGE
     */
    public function update(Request $request, Language $language)
    {
        $language->published = null;
        $language->update($request->all());

        $this->createFromEnglishIfNoTranslations($request, $language);

        flash(trans('text.updated'))->important();
        return back();
    }


    /**
     * Create transation texts if they dont exist
     */
    public function createFromEnglishIfNoTranslations(Request $request, Language $language)
    {
        if ($request->published == 1) {
            if ($language->translations->count() == 0) {
                $englishTranslations = Language::where('locale', 'LIKE', 'en')->first();

                foreach ($englishTranslations->translations as $translation) {
                    $newTranslation = $translation->replicate();
                    $newTranslation->locale = $language->locale;
                    $newTranslation->save();
                }
            }
        }
        return true;
    }

    /**
     * MANAGE
     */
    public function manage(Request $request, Language $language)
    {
        return view('backend.languages_index', ['translations' => $translations]);
    }


    /**
     * Change Locale
     */
    public function change_locale(Request $request, $locale)
    {
        // Regulate the session
        Session::put('locale', $locale);
        Session::put('waavi.translation.locale', $locale);
        
        // Clear cache
        \Artisan::call('cache:clear');

        // change user default language
        if (Auth::user()) {
            $user = Auth::user();
            $user->language = $locale;
            $user->save();
        }

        return back();
    }


    /**
     * Delete the language
     */
    public function delete(Request $request, Language $language)
    {
        // delete the language
        $language->forceDelete();

        // flash the message to the user
        flash(trans('text.deleted'))->important();

        // clear te cache
        \Artisan::call('cache:clear');

        // return back
        return back();
    }

    public function language_list()
    {
        $languages = Language::where('published', 'LIKE', 1)->select('locale', 'name')->get();
        return $languages;
    }
}
