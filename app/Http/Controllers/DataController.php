<?php

namespace App\Http\Controllers;

use Image;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class DataController extends Controller
{

    /**
     * Display the home grid data
     */
    public function home_grid_view(Request $request)
    {
        // TODO - get only the nessecery resources
        
        // get the data
        if (server_property('central_server') == 1) {
            $grid_data = async_request_to_all_servers('/all_resources', []); //$this->all_resources_all_servers($request);
        } else {
            $grid_data = $this->all_resources_array($request);
            $grid_data = $grid_data->toArray();
        }

        // apply the resource filter
        if (isset($request->resources)) {
            
            // explode the resources list
            $resources_array = $request->resources;
            
            // check each resorce if is not required resource type
            $grid_data = array_filter($grid_data, function ($k) use ($resources_array) {
                return in_array($k['type'], $resources_array) ? true : false;
            });
        }


        // SORT THE DATA
        if (isset($request->sort)) {
            $sort_field = $request->sort;
            
            usort($grid_data, function ($a, $b) use ($sort_field) {
                return strcmp($a[$sort_field], $b[$sort_field]);
            });
        }


        $page = ($request->page ? $request->page : 1); // Get the ?page=1 from the url
        $perPage = 8; // Number of items per page
        $offset = ($page * $perPage) - $perPage;

        $return_data =  new LengthAwarePaginator(
            array_slice($grid_data, $offset, $perPage, true), // Only grab the items we need
            count($grid_data), // Total items
            $perPage, // Items per page
            $page, // Current page
            ['path' => $request->url(), 'query' => $request->query()] // We need this so we can keep all old query parameters from the url
        );

        // return the view
        return view('home_grid_view', [ 'grid_data' => $return_data ]);
    }



    public function all_resources_array(Request $request)
    {
        // get the requests form the user
        $resources = isset($request->resources) ? $request->resources : ['course', 'lecture', 'study_case', 'tool', 'project'];
        $platforms = $request->platforms ? $request->platforms : [];

        //create an empty collection for data
        $data = new \Illuminate\Database\Eloquent\Collection;
        
        // Courses
        $array_course = array_search('course', $resources);
        if ($array_course || $array_course === 0) {
            $courses = app('App\Http\Controllers\CourseController')->table_data(1);
            if ($courses) {
                $data = $courses->merge($data);
            }
        }

        // Lectures
        $array_lectures = array_search('lecture', $resources);
        if ($array_lectures || $array_lectures === 0) {
            $lectures = app('App\Http\Controllers\LectureController')->table_data(1);
            if ($lectures) {
                $data = $lectures->merge($data);
            }
        }

        // Study Cases
        $array_study_cases = array_search('study_case', $resources);
        if ($array_study_cases || $array_study_cases === 0) {
            $study_cases = app('App\Http\Controllers\StudyCaseController')->table_data(1);
            if ($study_cases) {
                $data = $study_cases->merge($data);
            }
        }

        // Tools
        $array_tools = array_search('tool', $resources);
        if ($array_tools || $array_tools === 0) {
            $tools = app('App\Http\Controllers\ToolController')->table_data(1);
            if ($tools) {
                $data = $tools->merge($data);
            }
        }

        // Projects
        $array_projects = array_search('project', $resources);
        if ($array_projects || $array_projects === 0) {
            $projects = app('App\Http\Controllers\ChallengeGroupController')->table_data(1);
            if ($projects) {
                $data = $projects->merge($data);
            }
        }

        /*
        // news
        $array_news = array_search('news', $resources);
        if ($array_news|| $array_news === 0) {
            $news = app('App\Http\Controllers\NewsController')->table_data(1);
            if ($news) {
                $data = $news->merge($data);
            }
        }      */
        return $data;
    }


    /**
     * All Resources in datatable
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function all_resources_datatable(Request $request)
    {
        $data = $this->all_resources_array($request);
        // return the data
        return Datatables::of($data)->make();
    }


    /**
     * Latest resources for home page
     */
    public function home_page_resources_datatable(Request $request)
    {
        $numberOfResources = 3;
        
        $courses = \App\Course::where('published', 'LIKE', 1)->orderBy('created_at', 'DESC')->take($numberOfResources)->get();
        $lectures = \App\Lecture::where('published', 'LIKE', 1)->orderBy('created_at', 'DESC')->take($numberOfResources)->get();
        $study_cases = \App\StudyCase::where('published', 'LIKE', 1)->orderBy('created_at', 'DESC')->take($numberOfResources)->get();
        $tools = \App\Tool::where('published', 'LIKE', 1)->orderBy('created_at', 'DESC')->take($numberOfResources)->get();
        $projects = \App\ChallengeGroup::where('published', 'LIKE', 1)->orderBy('created_at', 'DESC')->take($numberOfResources)->get();
       
        $allResources = [$courses, $lectures, $study_cases, $tools, $projects];
        $data = [];

        foreach ($allResources as $resourceType) {
            foreach ($resourceType as $resource) {
                array_push($data, $resource->searchData());
            }
        }

        usort($data, function ($a, $b) {
            return $a['created_at_formated'] - $b['created_at_formated'];
        });

        $data = array_slice($data, 0, $numberOfResources);
        return $data;
    }



    /**
     * All resources form all servers
     */
    public function all_resources_all_servers(Request $request)
    {
        return async_request_to_all_servers('/api/all_resources', "");
    }

    /**
     * All Resources all servers in datatable
     */
    public function all_resources_all_servers_datatables(Request $request)
    {
        $data = collect($this->all_resources_all_servers($request));
        // return the data
        return Datatables::of($data)->make();
    }


    public function uploadCoverImage(Request $request, $model, $id)
    {
        $fullModelName = 'App\\' . $model;
        $resource = $fullModelName::find($id);

        // Handle image
        if ($request->image && $resource) {
            
            // read image from temporary file
            $img = Image::make($request->image);

            // resize image
            $img->resize(1024, 1024, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });

            $path = '/resources/'. strtolower($model) . '/' . $id . '/cover';
            $image_path = $path . '/' . $id . "_" . time() . '.jpg';
            
            // Make folder if not exist
            if (!file_exists(public_path() . $path)) {
                mkdir(public_path() . $path, 0777, true);
            }

            // save image
            $img->save(public_path() . $image_path);

            $resource->image = $image_path;
            $resource->save();
        }

        if ($model == "User") {
            $this->send_image_to_servers($resource);
        }

        return $image_path;
    }


    public function send_image_to_servers(User $user)
    {
        if ($server_list = array_of_all_servers()) {
            // check all servers
            foreach ($server_list as $server) {
                // get the data and test if return
                $url = $server->address . "/api/update_user_image?image=" . url($user->image) . "&email=" . $user->email;
                if ($server_data = @file_get_contents($url)) {
                }
            }
        }
    }


    public function coverImage(Request $request, $model, $id)
    {
        $fullModelName = 'App\\' . $model;
        $resource = $fullModelName::find($id);
        return view('common.cover_image_edit', ['resource' => $resource]);
    }

    public function removeCoverImage(Request $request, $model, $id)
    {
        $fullModelName = 'App\\' . $model;
        $resource = $fullModelName::find($id);
        $resource->image = "";
        $resource->save();
    }



    /**
     * All users form all servers
     */
    public function data_all_servers(Request $request)
    {
        $data = collect();
        
        // get all servers
        $servers = \App\ServerList::where('enabled', 1)->get();
        $central_server = collect();
        $central_server->address = url("/");

        $servers->push($central_server);
        //for each server get data
        foreach ($servers as $server) {
            // get the data and test if return
            if ($server_data = @file_get_contents($server->address . $request->url)) {
                // test if json and decode
                if ($server_data_array = json_decode($server_data, true)) {
                    // merge the data
                    $data = $data->merge($server_data_array);
                }
            }
        }

        // return collected data
        return Datatables::of($data)->make(true);
    }



    public function network_page()
    {
        return view('network_pages.view', ['central_server_address' => server_property('central_server_address')]);
    }


    public function all_platforms_view(Request $request)
    {
        $platforms = \App\ServerList::where('enabled', 1)->get();
        return view('bootstrap4.staticPages.networkPage.platformsList', ['platforms' => $platforms, 'request' => $request]);
    }
}
