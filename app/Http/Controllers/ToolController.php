<?php

namespace App\Http\Controllers;

use File;
use Image;
use Storage;
use App\Tool;
use Datatables;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{

    /**
     * CREATE TOOL VIEW
     * @return view
     */
    public function createPage(Request $request)
    {
        $tool = $this->create($request);
        return redirect('/tools/edit/' . $tool->id);
    }


    /**
     * CREATE TOOL
     * @return view
     */
    public function create(Request $request)
    {
        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            'author' => Auth::user()->fullName(),
            'institution' => Auth::user()->school,
            'category' => $request->category,
            'language' => "en",
            'server_id' => server_property('server_id'),
            ];

        // Create the tool
        $tool = Tool::create($request->all() + $additional_data);
        $tool->save();

        return $tool;
    }


    /**
     * MODIFY VIEW
     */
    public function modify(Request $request)
    {
        return view('tools.modify');
    }


    /**
     * Tools datatable list
     */
    public function datatable_list(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        
        return Datatables::of($data)->make(true);
    }


    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // get tools
        if ($published == true) {
            $tools = \App\Tool::where('published', 1)->get();
        } else {
            $tools = \App\Tool::all();
        }

        if (!$tools->count() > 0) {
            return false;
        }
        
        // create a data array
        $data = $tools->map(function ($item) {

            // check if author exists
            if ($author = \App\User::find($item->created_by)) {
                $image = $item->image ? url($item->image) : "";
                return [
                    'id' 					=> $item->id,
                    'title' 				=> $item->title,
                    'author' 				=> $author->author,
                    'country' 				=> "",
                    'year' 					=> "",
                    'language' 				=> $item->languageName(),
                    'type' 					=> 'tool',
                    'platform' 				=> str_replace('http://', "", url("/")),
                    'platform_id' 			=> server_property('server_id'),
                    'published' 			=> $item->published,
                    'category' 				=> $item->category,
                    'description' 			=> $item->description,
                    'image' 				=> $image,
                    'created_at' 			=> (array) $item->created_at,
                    'created_at_formated' 	=> $item->created_at->timestamp,
                    'course' 				=> "",
                    'length' 				=> "",
                    'state' 				=> "",
                    'institution' 			=> $item->institution,
                    'date' 					=> $item->created_at->toDateTimeString(),
                    'global' 				=> "",
                    'producer' 				=> "",
                    'criteria' 				=> "",
                    'contents' 				=> "",
                    'resource_type' 		=> "",
                    'designer'				=> ""
                ];
            } else {
                return "";
            }
        });

        return $data;
    }


    /**
     * PUBLISH
     */
    public function publish(Request $request, Tool $tool)
    {
        $tool->published = $tool->published ? null : 1;
        $tool->save();

        return publishButton($tool);
    }


    /**
     * UNPUBLISH
     */
    public function unpublish(Request $request, Tool $tool)
    {
        $tool->published = null;
        $tool->save();
        
        flash(trans('text.unpublished'))->important();
        return back();
    }

    /**
     * EDIT TOOLS PAGE
     */
    public function edit(Request $request, Tool $tool, $create_mode = false)
    {
        $category_list = [];
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('tools.edit', ['tool' => $tool, 'category_list' => $category_list, 'create_mode' => $create_mode, 'languages' => $languages]);
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        $tool = Tool::find($request->id);
        $tool->$name = $request->value;
        $tool->save();

        return 'OK';
    }


    /**
     * UPLOAD FILE
     */
    public function upload_file_old(Request $request, Tool $tool)
    {

        //set the path
        $path = 'resources/tools/' . $tool->id;
        
        // save the file
        Storage::disk('public')->putFileAs($path, $request->filename, $request->filename->getClientOriginalName());

        // update the tool
        $tool->filename = $path . "/" . $request->filename->getClientOriginalName();
        $tool->save();

        return $tool->filename;
    }

    /**
     * Upload resource file
     */
    public function upload_file(Request $request, Tool $tool)
    {
        $all_files = $request->resource_file;
        $base_folder = 'resources/tools/' . $tool->id;

        // SINGLE FILE
        if (count($request->resource_file) == 1) {
            //set the path
            $full_path = $base_folder . "/" . $all_files[0]->getClientOriginalName();
            $filename = $full_path;

            $success = File::cleanDirectory($base_folder);
                
            // Store the file
            Storage::disk('public')->putFileAs($base_folder, $all_files[0], $all_files[0]->getClientOriginalName());

            // Update the file list
            $file_list = Storage::allFiles($base_folder);
            $tool->filename = $filename;

            $tool->save();

            return json_encode(['single', $tool->id ]);
        } else {
            $upload_file_folders = explode(",", $request->paths);
            $file_to_return = [];
            $i = 0;

            foreach ($all_files as $tempFile) {
                $ds = DIRECTORY_SEPARATOR;
                  
                $targetPath = dirname(__FILE__) . $ds . $base_folder . $ds;
                
                $fullPath = $base_folder.rtrim($upload_file_folders[$i], "/.");
                $folder = substr($fullPath, 0, strrpos($fullPath, "/"));

                if (!is_dir($folder)) {
                    $old = umask(0);
                    mkdir($folder, 0777, true);
                    umask($old);
                }
                
                Storage::disk('public')->putFileAs($folder, $tempFile, $tempFile->getClientOriginalName());
                
                array_push($file_to_return, $folder . "/" . $tempFile->getClientOriginalName());
                $i++;
            }

            $files_to_zip = glob($base_folder . "/*");
            
            $zip_file = $base_folder . 'lens_resource_' . $tool->id .  '.zip';

            // Make a zip
            $zipper = new Zipper();
            $zipper->make($zip_file)->add($files_to_zip);

            $tool->filename = $zip_file;
            $tool->save();
            
            // return json array to preview
            return json_encode(['multiple', $tool->id, implode(',', $file_to_return)]);
        }
    }

    public function delete(Request $request, Tool $tool)
    {
        // delete the tool
        $tool->delete();

        // flash the message
        flash(trans('text.deleted'))->important();
        return back();
    }

    /**
     * VIEW THE COURSE
    public function view(Request $request, $tool_id)
    {
        $apiURL = server_url($request->server_id) . "/api/tools/view/" . $tool_id;
        // return courses view
        return view('tools.view', ['tool_id' => $tool_id, 'apiURL' => $apiURL]);
    }
    */

   
    /**
     * VIEW THE TOOL
     */
    public function view(Request $request, $tool_id)
    {
        $languages = \App\Language::where('published', 1)->get();
        $resourceViewApiUrl = server_url($request->server_id) . '/api/tools/view/' . $tool_id;
        return view(
            'bootstrap4.resources.index',
            [
                'resourceViewApiUrl' => $resourceViewApiUrl,
                'resource_type' => 'tool',
                'languages' => $languages,
                'page' => 'tool_view']
            );
    }



    /**
     * API DATA FOR COURSE VIEW
     */
    public function viewApi(Request $request, $tool_id)
    {
        $tool = increment_view_counter(Tool::where('id', $tool_id)->with('created_by', 'language')->first());
        return view('bootstrap4.resources.tool.view_api', ['tool' => $tool, 'request' => $request]);
    }

    
    public function file_list(Request $request, Tool $tool)
    {
        return view('tools.edit_file_list', ['tool' => $tool]);
    }


    public function deleteFile(Request $request, Tool $tool)
    {
        $tool->filename = "";
        $tool->save();
        return "OK";
    }


    public function increment_download(Request $request, Tool $tool)
    {
        increment_download_counter($tool, $request);

        if ($tool->link) {
            return $tool->link;
        } elseif ($tool->filename) {
            return url($tool->filename);
        } else {
            return "";
        }
    }
}
