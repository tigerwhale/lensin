<?php

namespace App\Http\Controllers;

use Log;
use App\User;
use Datatables;
use App\PartnersGroup;
use Illuminate\Http\Request;
use \HttpOz\Roles\Models\Role;
use App\Mail\PartnersGroupCreated;
use Illuminate\Support\Facades\Auth;

class PartnersGroupController extends Controller
{

    /**
     * Create partners group
     * @param  Request $request
     * @return back()
     */
    public function create(Request $request)
    {
        $partnersGroup = new PartnersGroup($request->all());
        $partnersGroup->created_by_id = Auth::user()->id;
        $partnersGroup->country_id = 1;
        $partnersGroup->save();

        if ($request->image) {
            $this->uploadCoverImage($request, $partnersGroup);
        }

        $partnersGroup->members()->attach(Auth::user()->id);

        // Find administrators
        $adminRole = Role::where('slug', 'manageusers')->first();
        $admins = $adminRole->users;

        // if user is not a member create member group
        if (!Auth::user()->members_groups->count()) {
            app('App\Http\Controllers\MembersGroupController')->create($request);
        }

        // Send emails
        foreach ($admins as $admin) {
            try {
                \Mail::to($admin)->send(new PartnersGroupCreated());
            } catch (\Exception $e) {
                // Get error here
            }
        }

        flash(trans('users.partners_group_created'));
        return back();
    }


    /**
     * TAB Create partners
     * @param  Request $request
     * @return view
     */
    public function create_partners_tab(Request $request)
    {
        return view('users.profile_partners_tab');
    }


    /**
     * Join partner group
     * @param  Request $request
     * @param  User    $user
     * @return back
     */
    public function join(Request $request, User $user)
    {
        $this->validate($request, [
            'partner_group_id' => 'required',
        ]);

        if (! $user->partner_groups->contains($request->member_group_id)) {
            $user->partners_groups()->attach($request->member_group_id);
        }

        $members_group = MembersGroup::find($request->member_group_id);
        flash(trans('users.you_are_now_a_part_of') . " " . $members_group->title);

        return back();
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $PartnersGroup = PartnersGroup::find($request->id);
        $PartnersGroup->$name = $request->value;
        $PartnersGroup->save();

        return 'OK';
    }


    /**
     * Backend view
     * @param  Request $request
     * @return view
     */
    public function backend()
    {
        return view('bootstrap4.backend.partners');
    }


    /**
     * Datatable list
     * @return Collection
     */
    public function datatable_list()
    {
        $data = PartnersGroup::all();
        return Datatables::of($data)->make(true);
    }

    public function partners_list(Request $request)
    {
        $partners = PartnersGroup::where('published', 1)->with('members')->get();
        return view('network_pages.partners_list', ['partners' => $partners, 'request' => $request]);
    }
    

    /**
     * PUBLISH
     */
    public function publish(Request $request, PartnersGroup $partnersgroup)
    {
        $partnersgroup->published = $partnersgroup->published ? null : 1;
        $partnersgroup->save();

        return publishButton($partnersgroup);
    }

    /**
     * Delete partner group
     * @param  Request      $request
     * @param  PartnersGroup $partnersgroup
     * @return OK
     */
    public function delete(Request $request, PartnersGroup $partnersgroup)
    {
        $partnersgroup->members()->detach();
        $partnersgroup->delete();
        return "OK";
    }

    /**
     * Upload cover image
     * @param  request       $request
     * @param  PartnersGroup $members_group
     */
    public function uploadCoverImage($request, PartnersGroup $partnersgroup)
    {
        uploadCoverImage($request, $partnersgroup);
    }

    /**
     * Edit Modal
     * @param  Request      $request
     * @param  PartnersGroup $partnersgroup
     * @return
     */
    public function edit_modal(Request $request, PartnersGroup $partnersgroup)
    {
        return view('bootstrap4.backend.partners_edit_modal', ['partnersgroup' => $partnersgroup]);
    }

    public function leave(Request $request)
    {
        Auth::user()->partners_groups()->detach($request->partner_id);
        return "OK";
    }
}
