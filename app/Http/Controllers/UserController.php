<?php

namespace App\Http\Controllers;

use Log;
use Image;
use App\User;
use Datatables;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Datatable list
     * @return Collection
     */
    public function datatable_list()
    {
        $data = User::all();
        return Datatables::of($data)->make(true);
    }

    /**
     * User list for network page
     * @return Datatables json
     */
    public function all_users_api()
    {
        // Get server properties
        $server = ["id" => server_property('server_id'),
                    "name" => server_property('platform_title')];
        // Get all local users
        $data = User::with('types', 'country')->where('server_id', $server['id'])->get();
        $return_data = collect();

        foreach ($data as $item) {
            // skip admin
            if ($item->id != 1) {
                $country_name = isset($item->country) ? $item->country->name : "";
                $type_name = isset($item->types) ? $item->types->name : "";

                $item_to_push = collect([
                        'name' => $item->name,
                        'last_name' => $item->last_name,
                        'type' => $type_name,
                        'country' => $country_name,
                        'departement' => $item->departement,
                        'school' => $item->school,
                        'server_url' => url("/"),
                        'server_id' => $server['id'],
                        'server_name' => $server['name'],
                    ]);
                $return_data->push($item_to_push);
            }
        }

        return $return_data;
        //return Datatables::of($return_data)->make(true);
    }


    /**
     * Update from input fields
     */
    public function update_data_backend(Request $request)
    {
        $name = $request->name;
        
        // Update data
        $user = User::find($request->id);
        $user->$name = $request->value;
        $user->save();
        
        $this->update_user_data_on_servers($user);
        
        return 'OK';
    }

    /**
     * Update user data on all servers
     * @param  User $user
     * @return
     */
    public function update_user_data_on_servers($user)
    {
        // get central server address
        $central_server_address = server_property('central_server_address');
        // get list of servers
        $server_list = central_server_data('/api/server_list');
        // get user data
        $user_data_to_send = $user->makeVisible('password')->toArray();

        // send data to central server
        if (server_property('central_server') != 1) {
            $url = $central_server_address . "/api/update_user_data";
            async_request([$url], $user_data_to_send, "POST");
        }
        
        $urls = [];
        // send data to child servers
        foreach ($server_list as $child_server) {
            if ($child_server->id != server_property('server_id')) {
                array_push($urls, $child_server->address . "/api/update_user_data");
            }
        }
        async_request($urls, $user_data_to_send, "POST");
    }

    /**
     * Api method for updating user data
     * @param  Request $request
     * @return string
     */
    public function update_user_data_from_server(Request $request)
    {
        // Find the user
        if ($request->email) {
            $user = User::where('email', 'LIKE', $request->email)->first();
            if ($user) {
                // UPDATE USER
                $user->update($request->toArray());
                return response()->json([
                    'success' => 'user_updated',
                ]);
            } else {
                // CREATE USER
                $validator = Validator::make($request->all(), [
                    'language_id' => 'required|string',
                    'email' => 'required',
                    'country_id' => 'required',
                    'password' => 'required|string',
                    'name' => 'required|string',
                    'last_name' => 'required|string',
                    'user_type_id' => 'required|integer',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'error' => 'not_valid_data',
                    ]);
                }

                $request->id = "";
                User::create($request->all());
                return response()->json([
                    'success' => 'user_created',
                ]);
            }
        }

        return response()->json([
            'error' => 'no_email_sent',
        ]);
    }


    /**
     * @param Request $request
     * @param User $user
     * @return Request
     */
    public function update_roles(Request $request, User $user)
    {
        if ($request->value == 1) {
            $user->attachRole($request->id);
        } else {
            $user->detachRole($request->id);
        }
        
        return $request;
    }

    /**
     * User profile page
     */
    public function profile(Request $request)
    {
        $user = Auth::user();
        $countries = \App\Country::all()->pluck('name', 'id');
        $user_types = \App\UserType::all()->pluck('name', 'id');
        $editableProfile = ($user->server_id == server_property('server_id')) ? "" : "disabled";

        // return (view('users.profile', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types, 'editableProfile' => $editableProfile]));
        return (view('auth.profile', ['user' => $user, 'countries' => $countries, 'user_types' => $user_types, 'editableProfile' => $editableProfile]));
    }


    /**
     * Upload user image
     */
    public function upload_image(Request $request, User $user)
    {
        $this->validate($request, ['image' => 'required|image']);

        // read image from temporary file
        $image = Image::make($request->image);

        $image->resize(800, 800, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        });
        ;

        $image_path = '/images/users/' . $user->id . '/' . $user->id . "_" . time() . '.jpg';

        if (!file_exists(public_path() . '/images/users/' . $user->id)) {
            mkdir(public_path() . '/images/users/' . $user->id, 0777, true);
        }

        $image->save(public_path() . $image_path);

        $user->image = url($image_path);
        $user->save();

        $returnData = view('courses.course_cover_image', ['course' => $user]);
        return $returnData;
    }


    public function update_user_image(Request $request)
    {
        $user = User::where('email', 'LIKE', $request->email)->first();
        $img = Image::make($request->image);
        $path = '/resources/user/' . $user->id . '/cover';
        
        // Make folder if not exist
        if (!file_exists(public_path() . $path)) {
            mkdir(public_path() . $path, 0777, true);
        }

        // save image
        $image_name = $user->id . "_" . time() . '.jpg';
        $img->save(public_path() . $path . "/" . $image_name);

        $user->image = $path . "/" . $image_name;
        $user->save();
        return "OK";
    }

    /**
     * Remove user profile image
     * @return string
     */
    public function remove_image(Request $request, User $user)
    {
        $user->image = "";
        $user->save();
        return 'OK';
    }


    /**
     * Check if email user exists on this server
     * @param  Request $request email of the user
     * @return string
     */
    public function check_user_exists(Request $request)
    {
        if ($request->email) {
            // get the user
            $user = User::where('email', 'LIKE', $request->email)->first();
            if ($user) {
                return ["server_address" => url("/")];
            }
        }
        return "";
    }


    /**
     * User details for transfering between servers
     * @param  Request $request
     * @param  User    $user
     * @return Json
     */
    public function user_details(Request $request, $email)
    {
        $user = User::where('email', $email)->first();
        if ($user) {
            if (isset($request->header()['serverurl'])) {
                if ($request->header()['serverurl'][0] == url("/")) {
                    // RETURN tHE USER WITH THE PASS
                    return json_encode($user->makeVisible('password'));
                }
            }
        }
        return "error";
    }


    public function user_on_system_api(Request $request, $email)
    {
        return json_encode(user_on_system($email));
    }


    public function suspend_user(Request $request, User $user)
    {
        $user->delete();

        flash(trans('users.user_banned'));
        return back();
    }

    public function delete_user(Request $request, User $user)
    {
        $user->forceDelete();

        flash(trans('users.user_deleted'));
        return back();
    }

    public function groupsList(Request $request, User $user)
    {
        return view('bootstrap4.homePage.project_list_modal', ['user' => $user])->render();
    }
    


    public function update_from_central(Request $request)
    {
        # code...
    }

    public function confirm_login(Request $request, $token, $email)
    {
        $user = User::where('email', $email)->where('login_token', 'LIKE', $token)->first();
        if ($user) {
            return ['confirm_login' => 'OK', 'user' => $user];
        }
        return ['confirm_login' => 'no_such_user'];
    }
}
