<?php

namespace App\Http\Controllers;

use Log;
use App\ServerList;
use Illuminate\Http\Request;

class ServerListController extends Controller
{

    /**
     * List Of Servers Backend
     * @return view
     */
    public function index()
    {
        $server_list = ServerList::orderBy('enabled', 'DESC')->get();
        return view('bootstrap4.backend.server_list', ['server_list' => $server_list]);
    }


    /**
     * CREATE A NEW SERVER
     * @return back()
     */
    public function create(Request $request)
    {
        // Validation of the request
        $this->validate($request, [
                'name' => 'required',
                'address' => 'required',
                'administrator_contact' => 'required'
                ]);

        // Check if server address is this one
        if ($request->address == url('/')) {
            flash(trans('text.central_server_can_not_access_itself'))->important();
            return back();
        }

        // Create the server in the list
        $server = ServerList::create([
                'name' => $request->name,
                'address' => $request->address,
                'enabled'  => $request->enabled,
                'administrator_contact' => $request->administrator_contact,
                'notes'  => $request->notes
            ]);

        //
        flash(trans('text.created'))->important();
        return back();
    }


    /**
     * UPDATE SERVER
     * @return  back
     */
    public function update(Request $request, ServerList $server_list)
    {
        $this->validate($request, [
                'name' => 'required',
                'address' => 'required',
                'administrator_contact' => 'required'
            ]);

        $server_list->name = $request->name;
        $server_list->address = $request->address;
        $server_list->enabled = $request->enabled ? 1 : 0;
        $server_list->administrator_contact = $request->administrator_contact;
        $server_list->server_description = $request->server_description;
        $server_list->notes = $request->notes;
        $server_list->save();


        flash(trans('text.saved'));
        return back();
    }


    /**
     * LIST OF SERVERS
     * @return json
     */
    public function server_list(Request $request)
    {
        if ($request->requestServerId) {
            return  ServerList::where('enabled', 1)->where('id', '!=', $request->requestServerId)->get(['id', 'name', 'address', 'server_description']);
        }
        return ServerList::where('enabled', 1)->get(['id', 'name', 'address', 'server_description']);
    }


    /**
     * DELETE SERVER FORM LIST
     * @param  ServerList $servel_list [description]
     * @return back()
     */
    public function delete(Request $request, ServerList $server_list)
    {
        // delete the server from list
        $server_list->delete();

        // flash the text to the user
        flash(trans('text.deleted'))->important();

        // return back()
        return back();
    }


    public function server_url_api(Request $request, $server_id)
    {
        if ($server_id) {

            // get the server from list
            $server = \App\ServerList::find($server_id);
            
            // if no server found
            if (!$server) {
                return "no_server";
            }

            // RETURN the server address
            return $server->address;
        } elseif ($server_id == 0) {
            return url('/');
        } else {
            return "no_server";
        }
    }

    public function platform_list_dropdown(Request $request)
    {
        $platform_id = $request->platform_id ? $request->platform_id : 0;
        $server_list = ServerList::orderBy('name', 'DESC')->where('enabled', 1)->get();
        return view('auth.platform_list_dropdown', ['server_list' => $server_list, 'platform_id' => $platform_id, 'request' => $request]);
    }

    public function profile_platform_list_dropdown(Request $request)
    {
        $platform_id = $request->platform_id ? $request->platform_id : 0;
        $server_list = ServerList::orderBy('name', 'DESC')->where('enabled', 1)->get();
        return view('users.profile_platform_list_dropdown', ['server_list' => $server_list, 'platform_id' => $platform_id, 'request' => $request]);
    }
}
