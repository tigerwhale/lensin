<?php

namespace App\Http\Controllers;

use Storage;
use App\User;
use Datatables;
use App\NewNetwork;
use Illuminate\Http\Request;
use \HttpOz\Roles\Models\Role;
use App\Mail\NewNetworkCreated;
use Illuminate\Support\Facades\Auth;

class NewNetworkController extends Controller
{
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'reference_person' => 'required',
            'reference_person_email' => 'required',
            'country_id' => 'required'
            ]);

        // Add the aditional settings
        $additional_data = [
            'created_by_id' => Auth::user()->id,
        ];

        // Create the tool
        $newNetwork = NewNetwork::create($request->all() + $additional_data);
        $newNetwork->save();

        // Set the path
        $base_folder = 'resources/new_networks/' . $newNetwork->id . "/";
        if ($request->document) {
            $full_path = $base_folder . $request->document->getClientOriginalName();
            $filename = url($full_path);
            // Store the file
            Storage::disk('public')->putFileAs($base_folder, $request->document, $request->document->getClientOriginalName());
            $newNetwork->document = $filename;
        }

        $newNetwork->save();

        // Find administrators
        $adminRole = Role::where('slug', 'manageusers')->first();
        $admins = $adminRole->users;

        // Send emails
        foreach ($admins as $admin) {
            try {
                \Mail::to($admin)->send(new NewNetworkCreated());
            } catch (\Exception $e) {
                // Get error here
            }
        }

        flash(trans('text.new_network_request_created'));
        return back();
    }

    /**
     * Datatable list
     * @return Collection
     */
    public function datatable_list()
    {
        $data = NewNetwork::orderBy('resolved', 'ASC')->get();
        return Datatables::of($data)->make(true);
    }

    /**
     * PUBLISH
     */
    public function resolve(Request $request, NewNetwork $newnetwork)
    {
        $newnetwork->resolved = $newnetwork->resolved ? null : 1;
        $newnetwork->save();

        return "ok";
    }


    public function delete(Request $request, NewNetwork $newnetwork)
    {
        $newnetwork->delete();
        return "OK";
    }

    public function backend(Request $request)
    {
        return view('bootstrap4.backend.new_network_list');
    }

    public function view_modal(Request $request, NewNetwork $newnetwork)
    {
        return view('bootstrap4.backend.new_network_view_modal', ['newnetwork' => $newnetwork]);
    }
}
