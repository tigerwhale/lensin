<?php

namespace App\Http\Controllers;

use Datatables;
use App\Fakedata;
use Illuminate\Http\Request;

class FakedataController extends Controller
{
    public function data(Request $request)
    {
        $data = Fakedata::select('id', 'title', 'author', 'country', 'year', 'language', 'type', 'platform');

        $resources = $request->resources;
        $platforms = $request->platforms;

        if ($resources) {
            $data = $data->where(function ($query) use ($resources) {
                foreach ($resources as $resource) {
                    $query->orWhere('type', 'LIKE', $resource);
                }
            });
        }

        if ($platforms) {
            $data = $data->where(function ($query) use ($platforms) {
                foreach ($platforms as $platform) {
                    $query->orWhere('platform', 'LIKE', $platform);
                }
            });
        }


        $data->get();

        return Datatables::of($data)->make();
    }
}
