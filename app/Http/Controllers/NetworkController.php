<?php

namespace App\Http\Controllers;

use App\Network;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;

class NetworkController extends Controller
{
    public function backend_view()
    {
        $networks = Network::all();
        return view('backend.networks', [ 'networks' => $networks ]);
    }

    /**
     * CREATE COURSE
     */
    public function create(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required'
        ]);

        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            ];

        $network = Network::create($request->all() + $additional_data);
        flash(trans('text.created'));

        return Redirect::to(URL::previous() . "#network_" . $network->id);
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $PartnersGroup = Network::find($request->id);
        $PartnersGroup->$name = $request->value;
        $PartnersGroup->save();

        return 'OK';
    }
    
    public function view()
    {
        /*
        if (server_property('central_server') == 1) {
            $networks = Network::all();
            return view('bootstrap4.staticPages.networkPageCentralServer', [ 'networks' => $networks ]);
        }
        */
        // Old network page
        $networks = Network::where('published', true)->get();
        return view('bootstrap4.staticPages.networkPageCentralServer', [ 'networks' => $networks ]);

        // New network page
        //return view('bootstrap4.staticPages.networkPageLocalServer', ['central_server_address' => server_property('central_server_address')]);
    }

    public function new_page_view()
    {
        return view('bootstrap4.staticPages.networkPageLocalServer', ['central_server_address' => server_property('central_server_address')]);
    }

    public function backend_networks_page()
    {
        $networks = Network::all();
        return view('backend.networks', [ 'networks' => $networks ]);
    }


    public function networks_edit_page()
    {
        return view('backend.networks_edit');
    }


    /**
     * List of all news
     */
    public function datatableList(Request $request)
    {
        return Datatables::of(Network::all())->make(true);
    }

    public function edit_network_modal(Request $request, Network $network)
    {
        return view('backend.network_edit_modal', ['network' => $network]);
    }


    /**
     * PUBLISH
     */
    public function publish(Request $request, Network $network)
    {
        $network->published = $network->published ? null : 1;
        $network->save();

        return publishButton($network);
    }
}
