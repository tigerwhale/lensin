<?php

namespace App\Http\Controllers;

use App\Language;
use App\News;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
 
    /**
     * View news
     * @param  Request $request
     * @param  News    $news
     * @return view
     */
    public function view(Request $request, News $news)
    {
        $languages = Language::where('published', 1)->orderBy('name')->get();
        return view('bootstrap4.news.view', ['news' => $news, 'page' => 'news', 'languages' => $languages]);
    }

    /**
     * View news
     * @param  Request $request
     * @param  News    $news
     * @return view
     */
    public function view_old(Request $request, News $news)
    {
        return view('news.view', ['news' => $news]);
    }

    /**
     * Create News
     * @param  Request $request [description]
     * @return News
     */
    public function create(Request $request)
    {
        // Add the aditional settings
        $additional_data = [
            'created_by_id' => Auth::user()->id,
            'server_id' => server_property('server_id')
            ];

        // Create the news
        $news = News::create($request->all() + $additional_data);
        $news->save();
        return $news;
    }


    public function createPage(Request $request)
    {
        $news = $this->create($request);
        return redirect('/news/edit/'. $news->id);
    }


    public function edit(Request $request, News $news)
    {
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'id');
        return view('news.edit.edit', ['news' => $news, 'languages' => $languages]);
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        $news = News::find($request->id);
        $news->$name = $request->value;
        $news->save();

        return 'OK';
    }


    /**
     * NEWS EDIT LIST VIEW
     */
    public function modify(Request $request)
    {
        return view('news.edit.modify');
    }


    /**
     * List of all news
     */
    public function datatableList(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        return Datatables::of($data)->make(true);
    }

    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // Get news
        if ($published == true) {
            $news = \App\News::where('published', 1)->get();
        } else {
            $news = \App\News::all();
        }

        if (!$news->count() > 0) {
            return false;
        }

        // create a data array
        $data = $news->map(function ($item) {
            $image = $item->image ? url($item->image) : "";
            return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'author' => $item->created_by ? $item->created_by->fullName() : "",
                    'country' => "",
                    'year' => "",
                    'language' => "",
                    'type' => 'news',
                    'platform' => str_replace('http://', "", url("/")),
                    'platform_id' => server_property('server_id'),
                    'published' => $item->published,
                    'category' => "",
                    'description' => "",
                    'image' => $image,
                    'created_at' => (array) $item->created_at,
                    'created_at_formated' => $item->created_at->timestamp,
                    'course' => "",
                    'length' => "",
                    'state' => "",
                    'institution' => $item->school,
                    'date' => $item->created_at->toDateTimeString(),
                    'global' => $item->global,
                    'producer' => ""
                ];
        });

        return $data;
    }


    /**
     * PUBLISH
     */
    public function publish(Request $request, News $news)
    {
        $news->published = $news->published ? null : 1;
        $news->save();

        return publishButton($news);
    }

    /**
     * GLOBAL
     */
    public function global_change(Request $request, News $news)
    {
        $news->global = $news->global ? null : 1;
        $news->save();

        return globalButton($news);
    }
    /**
     * Delete news
     * @param  Request $request
     * @param  News    $news
     * @return OK
     */
    public function delete(Request $request, News $news)
    {
        $news->delete();
        return "OK";
    }

    /**
     * Backend index page
     * @param  Request $request
     * @return View
     */
    public function backend_index(Request $request)
    {
        $news = News::all();
        $server_list = [];
        if (server_property("central_server") == 1) {
            $server_list = \App\ServerList::where('enabled', 1)->get();
        }
        $newsForbidden = \App\NewsForbidden::all();

        return view('bootstrap4.backend.news_index', ['news' => $news, 'server_list' => $server_list, 'newsForbidden' => $newsForbidden]);
        // return view('backend.news_index', ['news' => $news, 'server_list' => $server_list]);
    }


    public function forbidden_news_list(Request $request)
    {
        $newsForbidden = \App\NewsForbidden::all();
        $returnNews = [];
        foreach ($newsForbidden as $news) {
            array_push($returnNews, $news->news_id . "_" . $news->server_id);
        }
        return $returnNews;
    }

    /**
     * Backend API list
     * @return view
     */
    public function api_backend_news_list()
    {
        $news = \App\News::where('published', 1)->where('global', 1)->get();
        $newsForbidden = \App\NewsForbidden::all();

        return view('backend.news_edit_local_list', [ 'news' => $news, 'newsForbidden' => $newsForbidden ]);
    }

    public function api_publish_news_central(Request $request, $news_id, $server_id)
    {
        $forbid = \App\NewsForbidden::where('news_id', $news_id)->where('server_id', $server_id)->first();
        if ($forbid) {
            $forbid->delete();
        } else {
            $forbid = new \App\NewsForbidden;
            $forbid->news_id = $news_id;
            $forbid->server_id = $server_id;
            $forbid->save();
        }
    }


    public function edit_modal(Request $request, News $news)
    {
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'id');
        return view('backend.news_edit', [ 'news' => $news, 'languages' => $languages ]);
    }


    public function create_modal(Request $request)
    {
        $news = $this->create($request);
        return $this->edit_modal($request, $news);
    }


    public function latest_api(Request $request)
    {
        $news = \App\News::where('published', 1)->where('global', 1)->get();
        if (!$news->count() > 0) {
            return "{}";
        }

        // create a data array
        $data = $news->map(function ($item) {
            $image = $item->image ? url($item->image) : "";
            return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'author' => $item->created_by ? $item->created_by->fullName() : "",
                    'language' => $item->language_id ? $item->language : "",
                    'platform' => str_replace('http://', "", url("/")),
                    'platform_id' => server_property('server_id'),
                    'news_link' => url("/news/" . $item->id),
                    'image' => $image,
                    'created_at' => $item->created_at->toDateTimeString(),
                    'text' => $item->text,
                ];
        });
        return $data;
    }

    public function home_page_view(Request $request)
    {
        return view('home.news_view', [ 'news_json' => $request->data ]);
    }
}
