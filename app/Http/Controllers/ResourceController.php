<?php

namespace App\Http\Controllers;

use File;
use Storage;
use App\Lecture;
use App\Tutorial;
use App\Resource;
use App\ChallengeGroup;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ResourceController extends Controller
{
    /**
     * Create Lecture Resource
     * @return int 	course subject id
     */
    public function create_lecture(Request $request, $resource_model)
    {
        $orderResource = $resource_model->resources()->orderBy('order', 'desc')->first();
        $order = 1;
        if (!$orderResource) {
            $order = 1;
        } else {
            $order = $orderResource->order + 1;
        }

        $resource = Resource::create([
            'created_by' => Auth::user()->id,
            'resourcable_id' => $resource_model->id,
            'resourcable_type' => get_class($resource_model),
            'order' => $order,
            'resource_type_id' => 6
        ]);

        $resource->author = isset($request->author) ? $request->author : Auth::user()->fullName();
        $resource->year = date('Y');
        $resource->name = $resource_model->name();
        $resource->save();

        $resource->licences()->attach([1, 2]);

        return $resource;
    }


    /**
     * Create a lecture and add a link
     * @param  Request $request
     * @param  $resource_model
     * @return Resource
     */
    public function createWithPreview(Request $request, $resource_model)
    {
        if (gettype($resource_model) == 'string') {
            $resource_model = Lecture::find($resource_model);
        }
        $resource = $this->create_lecture($request, $resource_model);
        $resource->preview = $request->preview;
        $resource->save();

        return $resource;
    }

    /**
     * UPDATE THE SORTING
     * @param  Request $request | token and array
     */
    public function update_sort(Request $request)
    {
        $this->validate($request, ['resources' => 'required']);

        $i = 1;
        foreach ($request->resources as $resource) {
            $resource_object = Resource::find($resource);
            $resource_object->order = $i;
            $resource_object->save();
            $i++;
        }

        return "OK";
    }


    /**
     * Create Resource for PROJECT
     * @return int 	course subject id
     */
    public function create_project_resource(Request $request, ChallengeGroup $project)
    {
        $orderResource = $project->resources()->orderBy('order', 'desc')->first();
        $order = 1;
        if (!$orderResource) {
            $order = 1;
        } else {
            $order = $orderResource->order + 1;
        }

        $resource = Resource::create([
            'created_by' => Auth::user()->id,
            'resourcable_id' => $project->id,
            'resourcable_type' => 'App\ChallengeGroup',
            'order' => $order,
        ]);
        // Set default author, year
        $resource->author = isset($request->author) ? $request->author : Auth::user()->fullName();
        $resource->year = date('Y');
        
        // Get the path if set
        $resource->path = isset($request->preview_link) ? $request->preview_link : "";
        $resource->save();
        
        // Add default licences
        $resource->licences()->attach([1, 2]);

        // Add the file if uploaded
        if ($request->resource_file) {
            $this->upload_file($request, $resource);
        }

        return $resource->id;
    }


    /**
     * LOAD project resources
     * @param  Request  $request
     * @param  ChallangeGroup
     * @return view
     */
    public function load_project_resources(Request $request, ChallengeGroup $project, $edit = null)
    {
        return view('bootstrap4.resources.project.resource_list', ['project' => $project, 'edit' => $edit, 'request' => $request]);
    }
    

    public function preview(Request $request, Resource $resource)
    {
        $all_resources = Resource::where('resourcable_type', $resource->resourcable_type)->where('resourcable_id', $resource->resourcable_id)->get();
        return view('bootstrap4.resources.project.resource_preview', ['resource' => $resource, 'all_resources' => $all_resources, 'request' => $request]);
    }

    /**
     * EDIT project resource
     * @param  Request  $request
     * @param  Resource $resource
     * @return view
     */
    public function edit_project_resource(Request $request, Resource $resource)
    {
        return view('projects.resource_edit_modal', ['resource' => $resource]);
    }

    /**
     * Upload resource file
     */
    public function upload_file(Request $request, Resource $resource)
    {
        $all_files = $request->resource_file;
        $base_folder = 'resources/' . substr($resource->resourcable_type, 4) . "/" . $resource->id . "/";

        // SINGLE FILE
        if (count($all_files) == 1) {
            //set the path
            $file_name = $all_files[0]->getClientOriginalName();
            $file_full_path = $base_folder . $file_name;

            $success = File::cleanDirectory($base_folder);
            Storage::disk('public')->putFileAs($base_folder, $all_files[0], $file_name);

            // Update the file list
            $file_list = Storage::allFiles($base_folder);
            $resource->path = $file_full_path;
            $resource->save();

            $file_type = fileExtensionType($file_name);
            switch ($file_type) {
                case 'image':
                    $resource->resource_type_id = 8;
                    break;
                case 'pdf':
                    $resource->resource_type_id = 3;
                    break;
                case 'document':
                    $resource->resource_type_id = 3;
                    break;
                case 'slideshow':
                    $resource->resource_type_id = 1;
                    break;
                case 'video':
                    $resource->resource_type_id = 2;
                    break;
                case 'audio':
                    $resource->resource_type_id = 4;
                    break;
                default:
                    $resource->resource_type_id = 6;
                    break;
            }

            if ($request->zip) {
                $zipper = new Zipper();
                $zipper->make($file_full_path)->extractTo($base_folder);
                $file_list = $zipper->listFiles();
                $file_to_return = [];

                foreach ($file_list as $file_name) {
                    array_push($file_to_return, [ 'text' => $file_name, 'value' => url($base_folder . $file_name) ]);
                }
                return json_encode([ 'type' => 'multiple', 'resource_id' => $resource->id, 'file_list' => json_encode($file_to_return) ]);
            } else {
                return json_encode(['type'=>'single', 'resource_id' => $resource->id ]);
            }
        } else {
            // MULTIPLE FILES
            //upload_file_folders
            $uploaded_files = explode(",", $request->file_paths);

            $file_to_return = [];
            $i = 0;

            foreach ($all_files as $tempFile) {
                $ds = DIRECTORY_SEPARATOR;
                $targetPath = dirname(__FILE__) . $ds . $base_folder . $ds;
                
                $fullPath = $base_folder . rtrim($uploaded_files[$i], "/.");
                $folder = substr($fullPath, 0, strrpos($fullPath, "/"));

                if ($folder) {
                    if (!is_dir(public_path($folder))) {
                        $old = umask(0);
                        mkdir($folder, 0777, true);
                        umask($old);
                    }
                }
                
                Storage::disk('public')->putFileAs($folder, $tempFile, $tempFile->getClientOriginalName());
                array_push($file_to_return, [ 'text' => $uploaded_files[$i], 'value' => url($fullPath) ]);
                $i++;
            }

            $files_to_zip = glob($base_folder . "/*");
            
            $zip_file = $base_folder . 'lens_resource_' . $resource->id .  '.zip';

            // Make a zip
            $zipper = new Zipper();
            $zipper->make($zip_file)->add($files_to_zip);

            $resource->path = $zip_file;
            $resource->save();
            
            // return json array to preview
            return json_encode([ 'type' => 'multiple', 'resource_id' => $resource->id, 'file_list' => json_encode($file_to_return) ]);
        }
    }



    public function courseEditModal(Request $request, Resource $resource)
    {
        return view('courses.edit.modals.resource_edit_modal', ['resource' => $resource]);
    }

    public function courseEditModalFileList(Request $request, Resource $resource)
    {
        $view = view('courses.edit.modals.resource_edit_modal_file_list', ['resource' => $resource]);
        return ($view);
    }


    /**
     * CREATE A RESOUCE AND UPLOAD A FILE
     * @param  Request $request
     * @param  Lecture $resource_model
     * @return view
     */
    public function createAndUploadFile(Request $request, $resource_model_id)
    {
        $model_name = $request->model_name ? "App\\" . $request->model_name : 'App\Lecture';
        $model = new $model_name;
        $resource_object = $model->find($resource_model_id);

        // Create the resource
        $resource = $this->create_lecture($request, $resource_object);
        // Upload the file
        $return = $this->upload_file($request, $resource);
        // Return the ID
        return $return;
    }


    public function addPreview(Request $request, Resource $resource)
    {
        $resource->path = "";
        $resource->preview = $request->preview;
        $resource->save();
        return "OK";
    }
    

    /**
     * Delete resource
     * @return [text]
     */
    public function delete(Request $request)
    {
        // get the resource
        $resource = Resource::find($request->id);

        // if exists, delete it
        if ($resource) {
            $resource->delete();
            return trans('text.resource_deleted');
        } else {
            return trans('text.resource_does_not_exist');
        }
    }


    

    public function deleteMultiple(Request $request)
    {
        $resourcable_array = [];

        foreach ($request->resources as $resouce_id) {
            $resource = Resource::find($resouce_id);
            $resource->delete();
            array_push($resourcable_array, $resource->resourcable->id);
        }

        return $resourcable_array;
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        // find the Resource
        $resource = Resource::find($request->id);

        if (!$resource) {
            return trans('text.resource_doesnt_exist');
        }

        // Update the Resource
        $resource->$name = $request->value;
        $resource->save();

        return 'OK';
    }



    /**
     * LICENCE update
     */
    public function licence_update(Request $request)
    {
        $resource = Resource::find($request->id);
        //return $request->id_belongs_to;
        if ($request->value) {
            if (!$resource->licences->contains($request->id_belongs_to)) {
                $resource->licences()->attach($request->id_belongs_to);
            }
            return "attach";
        } else {
            $resource->licences()->detach($request->id_belongs_to);
            return "detach";
        }
    }


    /**
     * Publish / unpublish resource
     * @return text		icon for the button
     */
    public function publish(Request $request, Resource $resource)
    {
        $resource->published = $resource->published ? null : 1;
        $resource->save();
        return publishButton($resource);
    }


    /**
     * Publish multiple resources
     * @param  Request $request
     * @return string OK
     */
    public function publishMultiple(Request $request)
    {
        $resourcable_array = [];

        foreach ($request->resources as $resouce_id) {
            $resource = Resource::find($resouce_id);
            $resource->published = $request->publish_state;
            $resource->save();
            array_push($resourcable_array, $resource->resourcable->id);
        }

        return $resourcable_array;
    }


    /**
     * Display modal view of the resource
     * TO REMOVE
     */
    public function view_modal(Request $request, $resource_id, $lecture_id)
    {
        return "aa";
        $resource = Resource::find($resource_id);
        $lecture = Lecture::find($lecture_id);
        $resources = Resource::where('resourcable_id', $lecture_id)->where('resourcable_type', 'App\Lecture')->with('resourcable', 'resource_type', 'licences')->get();

        return view('courses.view.resource_modal', ['opened_resource_id' => $resource_id, 'request' => $request, 'resources' => $resources, 'lecture' => $lecture_id]);
    }


    /**
     * Display bootstrap4 modal view of the resource
     */
    public function resourceModal(Request $request, $resource_id, $lecture_id)
    {
        $resourceClicked = Resource::find($resource_id);
        $lecture = Lecture::find($lecture_id);
        $resources = Resource::where('resourcable_id', $lecture_id)->where('resourcable_type', 'App\Lecture')->with('resourcable', 'resource_type', 'licences')->get();

        return view('bootstrap4.resources.course.resource_modal', ['resourceClicked' => $resourceClicked, 'request' => $request, 'resources' => $resources, 'lecture' => $lecture]);
    }


    /**
     * Display bootstrap4 modal view of the resource
     */
    public function tutorialResourceModal(Request $request, $resource_id, $tutorial_id)
    {
        $resourceClicked = Resource::find($resource_id);
        $tutorial = Tutorial::find($tutorial_id);
        $resources = Resource::where('resourcable_id', $tutorial_id)->where('resourcable_type', 'App\Tutorial')->with('resourcable', 'resource_type', 'licences')->get();

        return view('bootstrap4.resources.course.resource_modal', ['resourceClicked' => $resourceClicked, 'request' => $request, 'resources' => $resources, 'lecture' => $tutorial]);
    }
    


    /**
     * API DATA FOR RESOURCE VIEW
     */
    public function view_modal_api_lecture(Request $request, $resource_type_id, $lecutre_id)
    {
        $resources = Resource::where('resource_type_id', $resource_type_id)->where('resourcable_id', $lecutre_id)->where('resourcable_type', 'App\Lecture')->with('resourcable', 'resource_type', 'licences')->get();
        return $resources;
    }


    /**
     * Remove resource file
     */
    public function delete_file(Request $request, Resource $resource)
    {
        // save the new file list
        $resource->path = "";
        $resource->preview = "";
        $resource->save();

        return "OK";
    }


    /**
     * Zip the files and send download link
     */
    public function zip_files(Request $request)
    {
        $files_array = [];

        // if there are some files seleted...
        if ($request->file_list) {
            foreach ($request->file_list as $file_item) {
                $reg = '/resources([^%]*)/';
                preg_match($reg, $file_item[0], $file_path);

                if ($file_path) {
                    array_push($files_array, public_path() . "/resources" . $file_path[1]);
                    // TODO add exception if not found
                    increment_download_counter(Resource::find($file_item[1]));
                }
            }
            
            // make a zip file
            $zip_file_path = 'storage/download/lens_files_'. time() . '.zip';
            $zipper = new Zipper();
            $zipper->make($zip_file_path)->add($files_array)->close();
        }

        return url($zip_file_path);
    }

    public function filePreviewList(Request $request, Resource $resource)
    {
        return view('courses.edit.modals.resource_edit_modal_file_list', ['resource' => $resource]);
    }



    public function increment_download(Request $request, Resource $resource)
    {
        increment_download_counter($resource, $request);

        if ($resource->link) {
            return $resource->link;
        } elseif ($resource->preview && !$resource->path) {
            return $resource->preview;
        } elseif ($resource->path) {
            return url($resource->path);
        } else {
            return "";
        }
    }
}
