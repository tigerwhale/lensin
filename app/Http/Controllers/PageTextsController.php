<?php

namespace App\Http\Controllers;

use App\PageText;
use Illuminate\Http\Request;

class PageTextsController extends Controller
{
    /**
     * Update from input fields
     */
    public function update(Request $request)
    {
        $name = $request->name;
        
        // Update the Page Text
        $page = PageText::find($request->id);
        $page->$name = $request->value;
        $page->save();

        return 'OK';
    }
}
