<?php

namespace App\Http\Controllers;

use Datatables;
use App\Theme;
use App\Course;
use App\User;
use App\Challenge;
use App\ChallengeGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ChallengesController extends Controller
{
    
    /**
     * CREATE VIEW
     */
    public function create_modal(Request $request)
    {
        $themes = Theme::get()->pluck('name', 'id');
        $view = view('challenges.create', ['themes' => $themes]);
        return $view;
    }


    /**
     * CREATE THEME
     */
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, ['name' => 'required']);

        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            'server_id' => server_property('server_id')
            ];

        // Create the tool
        $challenge = Challenge::create($request->all() + $additional_data);

        return "OK";
    }


    /**
     * List of challenges to modify
     */
    public function modify()
    {
        return view('challenges.modify');
    }

    /**
     * Data table for modify
     */
    public function data_table_modify()
    {
        $challenges = Challenge::all();

        // create a data array
        $data = $challenges->map(function ($item) {
            return [
                    'id' => $item->id,
                    'theme' => $item->theme->name,
                    'description' => $item->description,
                    'name' => $item->name,
                    'course' => $item->course,
                    'author' => $item->created_by,
                    // 'author' => $item->created_by->name . " " . $item->created_by->last_name,
                    'year' => $item->year,
                    'published' => $item->published
                ];
        });

        return Datatables::of($data)->make(true);
    }


    /**
     * DELETE CHALLENGE
     */
    public function delete(Request $request, Challenge $challenge)
    {
        // delete the challenge
        $challenge->delete();
        // send the text
        flash(trans('text.deleted'))->important();
        // return back
        return back();
    }


    /**
     * EDIT CHALLANGE
     */
    public function edit(Request $request, Challenge $challenge)
    {
        $themes = Theme::get()->pluck('name', 'id');
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'id');

        return view('challenges.edit', ['challenge' => $challenge, 'themes' => $themes, 'languages' => $languages]);
    }

    /**
     * EDIT CHALLANGE MODAL
     */
    public function edit_modal(Request $request, Challenge $challenge)
    {
        $themes = Theme::get()->pluck('name', 'id');
        $courses = Course::get()->pluck('name', 'id');
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'id');

        return view('challenges.edit_modal', ['challenge' => $challenge, 'themes' => $themes, 'courses' => $courses, 'languages' => $languages]);
    }

    /**
     * ASSIGN GROUP TO CHALLANGE BOX
     */
    public function load_assign_group_to_challenge(Request $request)
    {
        $groups = ChallengeGroup::whereNull('challenge_id')->get()->pluck('name', 'id');
        return view('challenges.edit.assign_group_to_challenge', ['groups' => $groups]);
    }

    /**
     * CREATE NEW GROUP MODAL
     */
    public function create_new_group_modal(Request $request, Challenge $challenge)
    {
        $challenges = Challenge::get()->pluck('name', 'id');
        return view('challenges.edit.create_new_group_modal', ['challenges' => $challenges]);
    }


    public function groups_assigned_to_challenge(Request $request, Challenge $challenge)
    {
        return view('challenges.edit.groups_assigned_to_challenge', ['challenge' => $challenge]);
    }


    /**
     * UPDATE DATA
     * @param  Request $request
     * @return OK
     */
    public function update_data(Request $request)
    {
        $variable_name = $request->name;

        $challenge = Challenge::find($request->id);
        $challenge->$variable_name = $request->value;
        $challenge->save();

        return 'OK';
    }

    /**
    * CHALLENGE FRONT
    * @param Request $request
    * @return view
    */

    public function main_view(Request $request, $create = null)
    {
        return view('projects.main', ['create' => $create]);
    }
}
