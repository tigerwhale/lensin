<?php

class LoginController extends Controller
{
    public function authenticated($request, $user)
    {
        return redirect(session()->pull('from', $this->redirectTo));
    }
}
