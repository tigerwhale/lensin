<?php

namespace App\Http\Controllers\Auth;

use Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        flash(trans('text.logout_text'), 'success')->important();

        return redirect('/');
    }


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function user_exists($email)
    {
        // check if user exists on this system
        $user = \App\User::where('email', $email)->withTrashed()->first();

        // if there is a user authenticate
        if ($user) {
            // if user is banned
            if ($user->deleted_at) {
                flash(trans('users.user_banned_from_server'))->important();
                return back();
            }

            // if user is local, run the auth
            if ($user->server_id == server_property('server_id')) {
                return true;
            } else {

                // IF USER IS REMOTE, FETCH HIS DATA FROM THE MOTHER SERVER
                // get user server url
                $user_server_url = @file_get_contents(server_property('central_server_address') . "/api/server_url/" . $user->server_id);
                
                if ($user_server_url) {
                    // create the header
                    $opts = array(
                        'http'=>array(
                            'method'=>"GET",
                            'header'=>"ServerUrl: " . $user_server_url . "\r\n"
                                      
                        )
                    );
                    $context = stream_context_create($opts);

                    // get the data for the user
                    $remote_user = json_decode(@file_get_contents($user_server_url . '/api/user_details/' . $email, false, $context), true);

                    $this->update_user_image($user_server_url, $remote_user, $user);

                    // if there is a user
                    if ($remote_user) {
                        unset($remote_user->id);
                        unset($remote_user->created_at);
                        unset($remote_user->updated_at);

                        $user->update($remote_user);
                    }
                }
                return true;
            }
        } else {
            
            // If there is no user, try to get it from other servers
            // if user is on some server get his data
            if ($users_server = user_on_system($email)) {
                $users_server = $users_server['server_address'];
                $user_info_url = $users_server . '/api/user_details/' . $email;
                $remote_user = url_request($user_info_url, "GET", ['headers' => [ 'serverurl' => $users_server ]]);
                
                // if the data is ok, create a local user
                if ($remote_user = json_decode($remote_user, true)) {
                    $remote_user['id'] = "";
                    $remote_user['created_at'] = "";
                    $remote_user['updated_at'] = "";
                    
                    $user = \App\User::create($remote_user);

                    $this->update_user_image($users_server, $remote_user, $user);

                    return true;
                } else {

                    // if data is not ok...
                    return false;
                }
            }
        }
        flash(trans('passwords.user'))->important();
        return false;
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $user->generate_token();
        //$this->generateLoginToken($user);
    }

    public function login(Request $request)
    {
        $this->redirectTo = session('from') ? $request->session()->pull('from') : "/";
        $this->validateLogin($request);
        

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        if (!$this->user_exists($request->email)) {
            flash(trans('ERR'))->important();
            return back();
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        session()->put('from', url()->previous());
        return view('auth.login');
    }


    public function update_user_image($user_server_url, $remote_user, $user)
    {
        if ($remote_user['image']) {
            $user_image_url = $user_server_url . "/". $remote_user['image'];
            if (check_url($user_image_url)) {
                $img = Image::make($user_image_url);
                $path = '/resources/user/' . $user->id . '/cover';
                // Make folder if not exist
                if (!file_exists(public_path() . $path)) {
                    mkdir(public_path() . $path, 0777, true);
                }

                // save image
                $img->save(public_path() . $path . "/" . basename($remote_user['image']));
                $user->image = $path . "/" . basename($remote_user['image']);
                $user->save();
            }
            return $user;
        } else {
            return $user;
        }
    }
}
