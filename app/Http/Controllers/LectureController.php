<?php

namespace App\Http\Controllers;

use Image;
use Datatables;
use App\Lecture;
use App\CourseSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LectureController extends Controller
{
    /**
     * Create lecture
     * @return int 	course subject id
     */
    public function create(Request $request, CourseSubject $course_subject)
    {
        $orderLecture = Lecture::where('course_subject_id', $course_subject->id)->orderBy('order', 'desc')->first();
        if (!$orderLecture) {
            $order = 1;
        } else {
            $order = $orderLecture->order + 1;
        }

        $lecture = Lecture::create([
            'created_by' => Auth::user()->id,
            'course_subject_id' => $course_subject->id,
            'order' => $order,
            'year' => date("Y"),
            'author' => Auth::user()->fullName()
            ]);

        return $lecture;
    }

    /**
     * Create lecture form grid view
     * @param  Request       $request
     * @param  CourseSubject $course_subject
     * @return view
     */
    public function createFromGridView(Request $request, CourseSubject $course_subject)
    {
        $lecture = $this->create($request, $course_subject);
        return view('courses.edit.courseware.lecture_grid_view', ['lecture' => $lecture]);
    }

    /**
     * Create lecture from leist view
     * @param  Request       $request
     * @param  CourseSubject $course_subject
     * @return view
     */
    public function createFromListView(Request $request, CourseSubject $course_subject)
    {
        $lecture = $this->create($request, $course_subject);
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('courses.edit.courseware.lecture_list_view', ['lecture' => $lecture, 'languages' => $languages]);
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        // find the Lecture
        $lecture = Lecture::find($request->id);

        if (!$lecture) {
            return trans('text.lecture_doesnt_exist');
        }

        // Update the Lecture
        $lecture->$name = $request->value;
        $lecture->save();

        return 'OK';
    }

    /**
     * Delete lecture
     * @return [text]
     */
    public function delete(Request $request)
    {
        $lecture = Lecture::find($request->id);
        if ($lecture) {
            $lecture->delete();
            return trans('text.lecture_deleted');
        } else {
            return trans('text.lecture_doesnt_exist');
        }
    }


    /**
     * Upload cover image
     */
    public function upload_cover_image(Request $request, Lecture $lecture)
    {
        // Handle image
        if ($request->image) {
            $imagePath = '/resources/lecture/' . $lecture->id . '/images';
            $imageFullPath = $imagePath . '/' . $request->image->getClientOriginalName();
            
            // Make folder if not exist
            if (!file_exists(public_path() . $imagePath)) {
                mkdir(public_path() . $imagePath, 0777, true);
            }

            // save image
            Storage::disk('public')->putFileAs($imagePath, $request->image, $request->image->getClientOriginalName());

            // update resource
            $resource->filename = url($imageFullPath);
            $resource->save();

            return "OK";
        }
        
        return "err";
    }


    /**
     * View lecture
     */
    public function view(Request $request, $lecture_id)
    {
        $languages = \App\Language::where('published', 1)->get();
        $resourceViewApiUrl = server_url($request->server_id) . '/api/lectures/view/' . $lecture_id;
        return view(
            'bootstrap4.resources.index',
            [
                'resourceViewApiUrl' => $resourceViewApiUrl,
                'resource_type' => 'lecture',
                'languages' => $languages,
                'page' => 'lecture_view']
            );
    }

    /**
     * API DATA FOR LECTURE VIEW
     */
    public function viewApi(Request $request, $lecture_id)
    {
        $lecture = increment_view_counter(Lecture::where('id', $lecture_id)->with('created_by', 'language')->first());
        return view('bootstrap4.resources.lecture.view_api', ['lecture' => $lecture, 'request' => $request]);
    }



    /**
     * Publish / unpublish lecture
     * @return text           icon for the button
     */
    public function publish(Request $request, Lecture $lecture)
    {
        $lecture->published = $lecture->published ? null : 1;
        $lecture->save();
        return publishButton($lecture, "data-return-function='toggleTransparentAndEyeClass' data-return-function-arguments='lecture_" . $lecture->id . "'");
    }

    public function setPublishedStatus(Request $request, Lecture $lecture)
    {
        $lecture->published = $request->status;
        $lecture->save();
        return "OK";
    }

    public function update_sort(Request $request)
    {
        $this->validate($request, ['lectures' => 'required']);

        $i = 1;
        foreach ($request->lectures as $lecture) {
            $lecture_object = Lecture::find($lecture);
            $lecture_object->order = $i;
            $lecture_object->save();
            $i++;
        }

        return "OK";
    }

    // Modify view
    public function modify(Request $request)
    {
        return view('lectures.modify');
    }

    // Datatable list
    public function datatableList(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        
        return Datatables::of($data)->make(true);
    }


    // Datatable data
    public function table_data($published = false)
    {
        // Get Lectures
        if ($published == true) {
            $lectures = \App\Lecture::where('published', 1)
                ->whereHas('course_subject', function ($query) {
                    $query->where('published', 1)
                        ->whereHas('course', function ($queryCourse) {
                            $queryCourse->where('published', 1);
                        });
                })->get();
        } else {
            $lectures = \App\Lecture::all();
        }

        if (!$lectures->count() > 0) {
            return false;
        }

        return $this->mapForDatatable($lectures);
    }



    public function mapForDatatable($lectures)
    {
        // create a data array
        $data = $lectures->map(function ($item) {
            
            // Contents of the lecture
            $contents = "";
            foreach ($item->contents as $content) {
                $contents .= $content->text . " | ";
            }
            
            $image = $item->image ? url($item->image) : "";
            return [
                    'id' 					=> $item->id,
                    'title' 				=> $item->name,
                    'author' 				=> $item->authors(),
                    'country' 				=> (isset($item->course_subject->course->country->name) ? $item->course_subject->course->country->name : ""),
                    'year' 					=> (isset($item->course_subject->course->year) ? $item->course_subject->course->year : ""),
                    'language' 				=> (isset($item->course_subject->course->language->name) ? $item->course_subject->course->language->name : ""),
                    'type' 					=> 'lecture',
                    'platform' 				=> str_replace('http://', "", url("/")),
                    'platform_id' 			=> server_property('server_id'),
                    'published' 			=> $item->published,
                    'category' 				=> "",
                    'description' 			=> $item->description,
                    'image' 				=> $image,
                    'created_at' 			=> (array) $item->created_at,
                    'created_at_formated' 	=> $item->created_at->timestamp,
                    'course' 				=> $item->course_subject->course->name,
                    'length'			 	=> $item->length,
                    'state' 				=> "",
                    'institution' 			=> "",
                    'date' 					=> $item->created_at->toDateTimeString(),
                    'global' 				=> "",
                    'producer' 				=> "",
                    'criteria' 				=> "",
                    'contents' 				=> $contents,
                    'resource_type'         => $item->resourcesTypes(),
                    'designer'				=> ""
                ];
        });

        return $data;
    }

    /**
     * Lecture Edit Modal
     * @param  Request $request
     * @param  Lecture $lecture
     * @return view
     */
    public function edit_modal(Request $request, Lecture $lecture)
    {
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('courses.edit.modals.lecture_modal_grid_view', ['lecture' => $lecture, 'languages' => $languages]);
    }


    /**
     * Lecture grid edit view
     */
    public function grid_view_edit(Request $request, Lecture $lecture)
    {
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('courses.edit.courseware.lecture_grid_view', ['lecture' => $lecture, 'languages' => $languages]);
    }


    /**
     * Download resources modal
     */
    public function download_resources_modal(Request $request, Lecture $lecture)
    {
        return view('courses.view.lecture_download_resources_modal', ['modal_data' => $lecture, 'server_url' => $request->server_url]);
    }


    /**
     * Load Cover image for edit
     * @param  Request $request [description]
     * @param  Course  $course  [description]
     * @return view
     */
    public function loadCoverImageForEdit(Request $request, Lecture $lecture)
    {
        return view('courses.edit.lecture_edit_cover_image', ['lecture' => $lecture]);
    }

    /**
     * Remove Cover image
     * @param  Request $request
     * @param  Lecture $lecture
     * @return string OK
     */
    public function removeCoverImage(Request $request, Lecture $lecture)
    {
        $lecture->image = "";
        $lecture->save();
        return "OK";
    }
}
