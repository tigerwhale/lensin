<?php

namespace App\Http\Controllers;

use App\Page;
use Datatables;
use App\Network;
use App\PageText;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{

    /**
     * View news
     * @param  Request $request
     * @param  News    $news
     * @return view
     */
    public function view(Request $request, $permalink)
    {
        $page = Page::where('permalink', $permalink)->first();
        return view('bootstrap4.staticPages.index', ['page' => $page]);
        //return view('pages.view', ['page' => $page]);
    }


    public function modify(Request $request, Page $page)
    {
        $networks = Network::all();
        // TODO remove return view('backend.pages_modify', ['networks' => $networks]);
        return view('bootstrap4.backend.pages_modify', ['networks' => $networks]);
    }


    public function edit(Request $request, Page $page)
    {
        $languages = Language::where('published', 1)->orderBy('name')->get();
        // Check if texts for all languages exist. If no - create one
        foreach ($languages as $language) {
            if (!$page->texts->where('locale', 'LIKE', $language->locale)->first()) {
                $pageText = new PageText;
                $pageText->locale = $language->locale;
                $pageText->page_id = $page->id;
                $pageText->created_by_id = Auth::user()->id;
                $pageText->save();
            }
        }
        $page = $page->fresh();
        return view('bootstrap4.backend.page_single_edit', ['page' => $page, 'languages' => $languages, 'tinymce' => true]);
        return view('backend.pages_edit', ['page' => $page, 'languages' => $languages, 'tinymce' => true]);
    }


    /**
     * List of all news
     */
    public function datatableList(Request $request)
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }
        return Datatables::of($data)->make(true);
    }



    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // Get news
        if ($published == true) {
            $page = \App\Page::where('published', 1)->get();
        } else {
            $page = \App\Page::all();
        }

        if (!$page->count() > 0) {
            return false;
        }

        // create a data array
        $data = $page->map(function ($item) {
            $image = $item->image ? url($item->image) : "";
            return [
                'id' => $item->id,
                'title' => $item->name,
                'author' => $item->created_by ? $item->created_by->fullName() : "",
                'country' => "",
                'year' => "",
                'language' => "",
                'type' => 'page',
                'platform' => str_replace('http://', "", url("/")),
                'platform_id' => server_property('server_id'),
                'published' => $item->published,
                'permalink' => $item->permalink,
                'image' => $image,
                'created_at' => (array) $item->created_at,
                'created_at_formated' => $item->created_at ? $item->created_at->timestamp : ""
            ];
        });

        return $data;
    }



    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $page = Page::find($request->id);
        $page->$name = $request->value;
        $page->save();

        return 'OK';
    }
}
