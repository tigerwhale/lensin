<?php

namespace App\Http\Controllers;

use App\Tutorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class TutorialController extends Controller
{

    public function view(Request $request, Tutorial $tutorial)
    {
        $user = Auth::user();
        $languages = \App\Language::where('published', 1)->get();
        increment_view_counter($tutorial);
        return view('bootstrap4.tutorials.view_single', ['tutorial' => $tutorial, 'request' => $request, 'languages' => $languages, 'user' => $user]);
    }

    public function viewPage()
    {
        $tutorials = Tutorial::where('published', 1)->orderBy('category')->get();
        $languages = \App\Language::where('published', 1)->get();
        return view('bootstrap4.tutorials.view', ['languages' => $languages, 'tutorials' => $tutorials]);
    }

    /**
     * Backend Create Page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createPage()
    {
        $tutorial = $this->create();
        return redirect('/tutorials/edit/' . $tutorial->id);
    }

    /**
     * @param Request $request
     * @param Tutorial $tutorial
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modifyPage(Request $request, Tutorial $tutorial)
    {
        $uniqueCategories = DB::table('tutorials')
            ->select('category')
            ->groupBy('category')
            ->pluck('category')
            ->toArray();
        $languages = \App\Language::orderBy('name')->get()->pluck('name', 'locale');
        return view('tutorial.edit', ['tutorial' => $tutorial, 'languages' => $languages, 'uniqueCategories' => $uniqueCategories]);
    }

    /**
     * Backend Tutorial List
     */
    public function backendView()
    {
        return view('tutorial.modify');
    }

    /**
     * Create Tutorial
     * @return Tutorial
     */
    public function create()
    {
        $additional_data = [
            'created_by_id' => Auth::user()->id,
            'author' => Auth::user()->fullName(),
            'institution' => Auth::user()->school,
            'language' => "en",
            'server_id' => server_property('server_id'),
        ];

        $tutorial = Tutorial::create($additional_data);
        $tutorial->save();

        return $tutorial;
    }

    public function delete(Request $request, Tutorial $tutorial)
    {
        $tutorial->delete();
        flash(trans('text.deleted'))->important();
        return back();
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        $tutorial = Tutorial::find($request->id);
        $tutorial->$name = $request->value;
        $tutorial->save();

        return 'OK';
    }

    /**
     * PUBLISH
     */
    public function publish(Request $request, Tutorial $tutorial)
    {
        $tutorial->published = $tutorial->published ? null : 1;
        $tutorial->save();

        return publishButton($tutorial);
    }


    /**
     * UNPUBLISH
     */
    public function unpublish(Request $request, Tutorial $tutorial)
    {
        $tutorial->published = null;
        $tutorial->save();

        flash(trans('text.unpublished'))->important();
        return back();
    }


    /**
     * Tools datatable list
     */
    public function datatable_list()
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }

        return Datatables::of($data)->make(true);
    }

    /**
     * @return mixed
     */
    public function datatable_all()
    {
        $data = $this->table_data();
        if (!$data) {
            $data = [];
        }

        return Datatables::of($data)->make(true);
    }


    private function table_data($published = false)
    {
        // get tools
        if ($published == true) {
            $tutorials = Tutorial::where('published', 1)->get();
        } else {
            $tutorials = Tutorial::all();
        }

        if (!$tutorials->count() > 0) {
            return false;
        }

        // create a data array
        $data = $tutorials->map(function ($item) {
            return $item->tableData();
        });

        return $data;
    }


}
