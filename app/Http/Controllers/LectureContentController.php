<?php

namespace App\Http\Controllers;

use App\Lecture;
use App\LectureContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LectureContentController extends Controller
{

    /**
     * Load edit part for the resources
     */
    public function listForLectureModal(Request $request, Lecture $lecture)
    {
        return view('courses.edit.modals.lecture_content_grid_view_list', ['lecture' => $lecture]);
    }

    /**
     * Create a new lecture content
     */
    public function create(Request $request, Lecture $lecture)
    {
        $lectureContent = LectureContent::create([
            'created_by' => Auth::user()->id,
            'lecture_id' => $lecture->id,
            'text' => "",
            ]);

        return "OK";
    }


    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        // find the Lecture
        $lectureContent = LectureContent::find($request->id);

        if (!$lectureContent) {
            return trans('text.lecture_doesnt_exist');
        }

        // Update the Lecture
        $lectureContent->$name = $request->value;
        $lectureContent->save();

        return 'OK';
    }

    /**
     * Delete lecture content
     */
    public function delete(Request $request, LectureContent $lectureContent)
    {
        $lectureContent->delete();
        return 'OK';
    }

    /**
     * Load resources list
     * @param  Request $request
     * @param  Lecture $lecture
     * @return view
     */
    public function resourcesListForLectureModal(Request $request)
    {
        $model_name = $request['model_name'] ? "App\\" . $request['model_name'] : 'App\Lecture';
        $model_id = $request['model_id'];

        $model = new $model_name;
        $model = $model->find($model_id);

        return view('courses.edit.modals.resource_list_for_lecture_modal',  ['lecture' => $model]);
    }


    public function backendResourceList(Request $request)
    {
        $model_name = $request->model_name ? "App\\" . $request->model_name : 'App\Lecture';
        $model_id = $request->model_id;

        $model = new $model_name;
        $resource_object = $model->find($model_id);

        return view('courses.edit.modals.resource_list_for_lecture_modal',  ['lecture' => $resource_object]);
    }
}
