<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Image;
use App\User;
use Datatables;
use App\Challenge;
use App\ChallengeGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChallengeGroupController extends Controller
{
    /**
     * CREATE PROJECT
     */
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, ['name' => 'required', 'project_name' => 'required']);

        // Add the aditional settings
        $additional_data = [
            'created_by_id' => Auth::user()->id,
            'challenge_id' => $request->challenge_id,
            'server_id' => server_property('server_id'),
            ];

        // Create the tool
        $group = ChallengeGroup::create($request->all() + $additional_data);

        return "OK";
    }


    /**
     * DELETE PROJECT
     * @param  ChallengeGroup $group
     * @return string OK
     */
    public function delete(Request $request, ChallengeGroup $group)
    {
        $group->delete();
        return "OK";
    }


    /**
     * FRONT PAGE VIEW
     * @param  Request $request
     * @param  integer  $project_id
     * @return view
     */
    public function viewOLD(Request $request, $project_id)
    {
        $apiURL = server_url($request->server_id) . "/api/projects/view/" . $project_id;
        // return courses view
        return view('bootstrap4.resources.projects.view', ['project_id' => $project_id, 'apiURL' => $apiURL, 'request' => $request]);
    }

    /**
     * VIEW THE PROJECT
     * @param Request $request
     * @param $tool_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request, $project_id)
    {
        $languages = \App\Language::where('published', 1)->get();
        $resourceViewApiUrl = server_url($request->server_id) . '/api/projects/view/' . $project_id;
        return view(
            'bootstrap4.resources.index',
            [
                'resourceViewApiUrl' => $resourceViewApiUrl,
                'resource_type' => 'project',
                'languages' => $languages,
                'page' => 'project_view']
        );
    }


    /**
     * @param Request $request
     * @param ChallengeGroup $challenge_group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewApi(Request $request, ChallengeGroup $challenge_group)
    {
        // set default editable var
        $editable = 0;

        // check if editable (url and user are the same)
        if ($request->request_server_id == $challenge_group->server_id && isset($challenge_group->users) && Auth::user()) {
            foreach ($challenge_group->users as $user) {
                if ($user->id == Auth::user()->id) {
                    $editable = 1;
                }
            }
        }
        increment_view_counter($challenge_group);
        
        return view('bootstrap4.resources.project.view_api', [
            'challenge_group' => $challenge_group,
            'users' => $challenge_group->users()->orderBy('name')->get(),
            'request' => $request,
            'editable' => $editable
        ]);
    }



    /**
     * API DATA FOR LECTURE VIEW
     */
    public function view_api(Request $request, $project_id)
    {
        $project = ChallengeGroup::where('id', $project_id)->with('users', 'resources')->get();
        increment_view_counter($project[0]);
        return $project;
    }

    /**
     * Data table for modify PROJECTS
     */
    public function data_table_modify()
    {
        $groups = ChallengeGroup::all();

        // create a data array
        $data = $groups->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->project_name,
                'challenge' => $item->challenge->name,
                'course' => $item->challenge->course,
                'published' => $item->published,
                'author' => $item->created_by->name . " " . $item->created_by->last_name,
                'description' => $item->description
                //'course' => "",
            ];
        });

        return Datatables::of($data)->make(true);
    }


    /**
     * List of groups to modify
     */
    public function modify()
    {
        return view('projects.modify');
    }

    /**
     * Edit modal view
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @return view group_edit_modal
     */
    public function edit_modal(Request $request, ChallengeGroup $challenge_group)
    {
        $users = \App\User::select(
            DB::raw("CONCAT(name,' ',last_name) AS name"),
            'id'
        )
            ->orderBy('name')
            ->pluck('name', 'id');
            
        return view('challenges.edit.group_edit_modal', ['challenge_group' => $challenge_group, 'users' => $users]);
    }

    /**
     * Load group teachers
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @return view
     */
    public function load_group_teachers(Request $request, ChallengeGroup $challenge_group)
    {
        return view('challenges.edit.group_teachers_list', ['challenge_group' => $challenge_group]);
    }

    /**
     * Assign teacher to the group
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @param  User           $teacher
     */
    public function assign_teacher(Request $request, ChallengeGroup $challenge_group, User $teacher)
    {
        // check if teacher is already assigned
        if (! $challenge_group->teachers->contains($teacher->id)) {
            // attach the teacher to the group
            $challenge_group->teachers()->attach($teacher->id);
            return "OK";
        } else {
            return trans('challenges.teacher_already_assigned');
        }
    }

    /**
     * Detach teacher from the group
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @param  User           $teacher
     */
    public function detach_teacher(Request $request, ChallengeGroup $challenge_group, User $teacher)
    {
        $challenge_group->teachers()->detach($teacher->id);
        return "OK";
    }


    /**
     * Load group users
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @return view
     */
    public function load_group_users(Request $request, ChallengeGroup $challenge_group)
    {
        return view('challenges.edit.group_users_list', ['challenge_group' => $challenge_group]);
    }


    /**
     * Assign user to the group
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @param  User           $user
     */
    public function assign_user(Request $request, ChallengeGroup $challenge_group, User $user)
    {
        // check if teacher is already assigned
        if (! $challenge_group->users->contains($user->id)) {

            // attach the teacher to the group
            $challenge_group->users()->attach($user->id);
            return "OK";
        } else {
            return trans('challenges.user_already_assigned');
        }
    }

    /**
     * Detach user from the group
     * @param  Request        $request
     * @param  ChallengeGroup $challenge_group
     * @param  User           $user
     */
    public function detach_user(Request $request, ChallengeGroup $challenge_group, User $user)
    {
        $challenge_group->users()->detach($user->id);
        return "OK";
    }



    /**
     * Update data
     * @param  Request $request [description]
     * @return string OK
     */
    public function update_data(Request $request)
    {
        $variable_name = $request->name;

        $challenge_group = ChallengeGroup::find($request->id);
        $challenge_group->$variable_name = $request->value;
        $challenge_group->save();

        return 'OK';
    }



    /**
     * DATA FOR THE DATATABLE
     */
    public function table_data($published = false)
    {
        // get courses
        if ($published == true) {
            $groups = \App\ChallengeGroup::with('created_by')->where('published', 1)->get();
        } else {
            $groups = \App\ChallengeGroup::with('created_by')->get();
        }

        if (!$groups->count() > 0) {
            return false;
        }


        // create a data array
        $data = $groups->map(function ($item) {
            $image = $item->image ? url($item->image) : "";
            return [
                    'id' 					=> $item->id,
                    'title' 				=> $item->project_name,
                    'author' 				=> $item->created_by->name . " " . $item->created_by->last_name,
                    'country' 				=> "",
                    'year' 					=> "",
                    'language' 				=> "",
                    'type' 					=> 'project',
                    'platform' 				=> str_replace('http://', "", url("/")),
                    'platform_id' 			=> server_property('server_id'),
                    'published' 			=> $item->published,
                    'category' 				=> "",
                    'description' 			=> $item->description,
                    'image' 				=> $image,
                    'created_at' 			=> (array) $item->created_at,
                    'created_at_formated' 	=> $item->created_at->timestamp,
                    'course' 				=> "",
                    'length' 				=> "",
                    'state' 				=> "",
                    'institution' 			=> "",
                    'date' 					=> "",
                    'global' 				=> "",
                    'producer' 				=> "",
                    'criteria' 				=> "",
                    'contents' 				=> "",
                    'resource_type' 		=> "",
                    'designer'				=> ""
                ];
        });

        return $data;
    }


    /**
     * CHANGE THE PUBLISH STATE
     * @param  Request $request [id of the group]
     * @return string           publish button
     */
    public function publish(Request $request, ChallengeGroup $group)
    {
        $group->published = $group->published ? null : 1;
        $group->save();

        return publishButton($group);
    }


    /**
     * Resource upload modal
     * @param  Request $request
     * @return view
     */
    public function resource_upload_modal(Request $request, ChallengeGroup $group)
    {
        return view('projects.resource_upload_modal', ['group' => $group]);
    }


    /**
     * Load project image
     * @param  Request        $request
     * @param  ChallengeGroup $project
     * @return view
     */
    public function loadProjectImage(Request $request, ChallengeGroup $project)
    {
        if ($project->image) {
            $view = view('challenges.edit.project_image', ['project' => $project, 'editable' => $request->editable ]);
        } else {
            $view = "";
        }
        return $view;
    }
}
