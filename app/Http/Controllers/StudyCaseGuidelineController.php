<?php

namespace App\Http\Controllers;

use App\Licence;
use App\StudyCase;
use App\StudyCaseGuideline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudyCaseGuidelineController extends Controller
{


    /**
     * LICENCE update
     */
    public function licence_update(Request $request)
    {
        $study_case = StudyCaseGuideline::find($request->id);

        if ($request->value) {
            if (!$study_case->licences->contains($request->id_belongs_to)) {
                $study_case->licences()->attach($request->id_belongs_to);
            }
            return "attach";
        } else {
            $study_case->licences()->detach($request->id_belongs_to);
            return "detach";
        }
    }


    
    /**
     * Load criteria
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function load_guidelines(Request $request)
    {
        $languages = \App\Language::orderBy('name')->pluck('name', 'locale');
        $guidelines = StudyCaseGuideline::where('level', 0)->orderBy('level', 'order')->get();
        $licences = Licence::all();
        $data = '';
        $level_texts = ['dimension', 'criteria', 'guideline', 'subguideline'];
        foreach ($guidelines as $guideline) {
            $data .= view('study_cases_guidelines.edit', [
                'licences' => $licences,
                'languages' => $languages,
                'guideline' => $guideline,
                'level' => 0,
                'parent' => 0,
                'level_texts' => $level_texts
            ]);
        }
        return $data;
    }


    public function create(Request $request)
    {
        $level_texts = ['dimension', 'criteria', 'guideline', 'subguideline'];
        $orderGuideline = StudyCaseGuideline::where('study_case_guideline_id', $request->parrent_id)->orderBy('order', 'desc')->first();
        $languages = \App\Language::orderBy('name')->pluck('name', 'locale');
        if (!$orderGuideline) {
            $order = 1;
        } else {
            $order = $orderGuideline->order + 1;
        }

        $guideline = new StudyCaseGuideline;
        $guideline->name = 'Insert ' . trans('text.' . $level_texts[$request->level + 1]) . ' title';
        $guideline->order = $order;
        $guideline->study_case_guideline_id = $request->parrent_id;
        $guideline->level = $request->level + 1;
        $guideline->created_by = Auth::user()->id;
        $guideline->save();
        
        $guideline->licences()->attach([1, 2]);
        $guideline->author = Auth::user()->fullName;
        $guideline->save();

        $licences = Licence::all();


        return view('study_cases_guidelines.edit', [
            'level' => $request->level + 1,
            'guideline' => $guideline,
            'languages' => $languages,
            'level_texts' => $level_texts,
            'licences' => $licences
            ]);
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $guideline = StudyCaseGuideline::find($request->id);
        $guideline->$name = $request->value;
        $guideline->save();

        return 'OK';
    }


    /**
     * DELETE STUDY CASE GUIDELINE
     */
    public function delete(Request $request, StudyCaseGuideline $study_case_guideline)
    {
        $study_case_guideline->delete();

        flash(trans('text.study_case_deleted'))->important();
        return back();
    }

    /**
     * DELETE STUDY CASE GUIDELINE
     */
    public function assign(Request $request, StudyCase $study_case)
    {
        // sync the guidelines to the study case
        $study_case->guidelines()->detach();
        $study_case->guidelines()->attach($request->study_case_id);

        return "OK";
    }
}
