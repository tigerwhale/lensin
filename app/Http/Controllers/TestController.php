<?php

namespace App\Http\Controllers;

use App\Mail\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TestController extends Controller
{
    public function test_email(Request $request, $email)
    {
        try {
            \Mail::to($email)->send(new Test());
            logger("TEST email SENT to " . $email);
        } catch (\Exception $e) {
            logger("TEST email NOT sent to " . $email . " " . $e);
        }
    }

    public function clear_cache()
    {
        Artisan::call('config:clear');
        return "OK";
    }
}
