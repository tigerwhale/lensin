<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ResourceComment;
use App\Resource;

class ResourceCommentsController extends Controller
{
    public function comments_list(Request $request, Resource $resource)
    {
        $editable = isset($request->editable) ? $request->editable : "";
        $teacher = "";

        if ($resource->resourcable_type = "App\ChallengeGroup" && Auth::user()) {
            /*if ( $resource->resourcable->teachers->where('id', Auth::user()->id)->count() )
            {
                $teacher = 1;
            }
            */
        }
        return view('projects.comments_list', [ 'resource'=>$resource, 'editable'=>$editable, 'teacher'=>$teacher ]);
    }


    public function create(Request $request)
    {
        $comment = new ResourceComment;
        $comment->resource_id = $request->resource_id;
        $comment->created_by = $request->user_id;
        $comment->text = $request->text;
        $comment->save();

        return "OK";
    }


    public function delete(Request $request, ResourceComment $comment)
    {
        $comment->delete();
        return "OK";
    }
}
