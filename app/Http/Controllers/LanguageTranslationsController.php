<?php

namespace App\Http\Controllers;

use App\Language;
use App\LanguageTranslation;
use Illuminate\Http\Request;

class LanguageTranslationsController extends Controller
{
    /**
     * LIST OF TRANSLATIONS
     */
    public function index(Request $request, Language $language)
    {
        // get all translations
        $translations = LanguageTranslation::where('locale', 'LIKE', $language->locale)->orderBy('group')->get();
        
        if ($translations->count() == 0) {
            $this->createTranslationsFromEnglish($language);
            $translations = LanguageTranslation::where('locale', 'LIKE', $language->locale)->orderBy('group')->get();
        }

        // return the view
        return view('backend.language_translations_index', ['language' => $language, 'translations' => $translations]);
    }

    /**
     * Create default Translations from English as a template for translating
     * @param  Language $language
     * @return translations
     */
    public function createTranslationsFromEnglish(Language $language)
    {
        $englishTranslations = LanguageTranslation::where('locale', 'LIKE', 'en')->get();
        foreach ($englishTranslations as $englishTranslation) {
            $newTranslation = LanguageTranslation::create([
                                    'locale' => $language->locale,
                                    'group' => $englishTranslation->group,
                                    'namespace' => $englishTranslation->namespace,
                                    'item' => $englishTranslation->item,
                                    'text'	=> $englishTranslation->text,
                                    'unstable' => $englishTranslation->unstable,
                                    'locked' => $englishTranslation->locked,
                            ]);
            $newTranslation->save();
        }
        return true;
    }


    /**
     * UPDATE THE LANGUAGE
     */
    public function update(Request $request, LanguageTranslation $translation)
    {
        // update the translation
        $translation->update($request->all());

        // clear the cache
        \Artisan::call('cache:clear');
        \Waavi\Translation\Facades\TranslationCache::flushAll();

        // flash the message and return back
        return "OK";
    }


    public function translation_array(Request $request, $locale)
    {
        $translations = LanguageTranslation::where('locale', 'LIKE', $locale)->get();
        $language = Language::where('locale', 'LIKE', $locale)->first();
        $data_to_send = ['translations' => $translations, 'language' => $language];
        return json_encode($data_to_send);
    }
}
