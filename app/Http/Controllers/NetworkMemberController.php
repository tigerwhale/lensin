<?php

namespace App\Http\Controllers;

use App\NetworkMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class NetworkMemberController extends Controller
{

    /**
     * Create Network memeber
     */
    public function create(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
        ]);

        // Add the aditional settings
        $additional_data = [
            'created_by' => Auth::user()->id,
            ];

        $network_member = NetworkMember::create($request->all() + $additional_data);
        flash(trans('text.created'));

        $hash_tag = $request->network_id ? $request->network_id : $network_member->parent_member->network->id;
        return Redirect::to(URL::previous() . "#network_" . $hash_tag);
    }


    public function delete(Request $request, NetworkMember $member)
    {
        $hash_tag = $member->network_id ? $member->network_id : $member->parent_member->network->id;
        $member->delete();
        flash(trans('text.deleted'));

        return Redirect::to(URL::previous() . "#network_" . $hash_tag);
    }

    /**
     * Update from input fields
     */
    public function update_data(Request $request)
    {
        $name = $request->name;
        
        // Update the Course
        $member = NetworkMember::find($request->id);
        $member->$name = $request->value;
        $member->save();

        return 'OK';
    }
}
