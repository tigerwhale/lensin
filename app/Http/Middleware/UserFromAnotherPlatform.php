<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserFromAnotherPlatform
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->token) && isset($request->server_id) && isset($request->email)) {
            $origin_server_url = server_property('central_server_address') . '/api/server_url/' . $request->server_id;
            $server_url = url_request($origin_server_url, "GET");
            $url = $server_url . "/api/confirm_login/" . $request->token . "/" . $request->email;

            $confirmation = url_request($url, "GET");
            $confirmation = json_decode($confirmation, true);

            if ($confirmation["confirm_login"] == "OK") {
                $user = \App\User::where("email", "LIKE", $request->email)->first();
                if ($user) {
                    Auth::login($user);
                    $user->generate_token();
                } elseif ($confirmation["user"]) {
                    $user = \App\User::create($confirmation["user"]);
                    Auth::login($user);
                    $user->generate_token();
                }
            }
        }
        
        return $next($request);
    }
}
