<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App;
use Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Session::get('locale');

        // Set the language if user is logged in
        if (Auth::user()) {
            $locale = Auth::user()->language;
        }

        // if locale set via url
        if (isset($_GET['locale'])) {
            if ($_GET['locale']) {
                $locale = $_GET['locale'];
            }
        }


        Session::put('locale', $locale);
        App::setLocale($locale);

        return $next($request);
    }
}
