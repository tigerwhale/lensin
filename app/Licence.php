<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Licence extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Resource
     */
    public function resources()
    {
        return $this->belongsToMany('App\Resource');
    }

    /**
     * Resource
     */
    public function guidelines()
    {
        return $this->belongsToMany('App\StudyCaseGuideline');
    }
}
