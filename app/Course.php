<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Course extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'server_id', 'country_id', 'language_id', 'name', 'image', 'school', 'duration', 'type', 'description', 'year'
    ];

    public static function boot()
    {
        parent::boot();

        self::deleting(function (Course $course) {
            foreach ($course->subjects as $subject) {
                $subject->delete();
            }
        });
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return isset($this->name) ? $this->name : trans('text.no_title');
    }

    /**
     * @return App\Challenge
     */
    public function challenges()
    {
        return $this->hasMany('App\Challenge');
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/course_big.png";
    }

    /**
     * Country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * Country name
     */
    public function countryName()
    {
        return $this->country_id ? $this->country->name : "";
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Created by
     */
    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Teachers
     */
    public function teachers()
    {
        return $this->belongsToMany('App\User', 'course_teacher');
    }

    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }


    /**
     * Language name
     */
    public function languageName()
    {
        return $this->language_id ? $this->language->name : "";
    }


    /**
     * Course Subjects
     */
    public function subjects()
    {
        return $this->hasMany('App\CourseSubject')->orderBy('order');
    }


    /**
     * Course Subjects published
     */
    public function subjects_published()
    {
        return $this->hasMany('App\CourseSubject')->orderBy('order')->where('published', 1);
    }

    //observe this model being deleted and delete the child events

    public function searchData()
    {
        $country_name = ($this->country_id) ? $this->country->name : "";
        $language_name = (($this->language_id) ? $this->language->name : "");
        $image = $this->image ? url($this->image) : "";

        $data = [
            'id' => $this->id,
            'title' => $this->name,
            'author' => $this->authors(),
            'country' => $country_name,
            'year' => $this->year,
            'language' => $language_name,
            'type' => 'course',
            'platform' => str_replace('http://', "", url("/")),
            'platform_id' => server_property('server_id'),
            'published' => $this->published,
            'category' => "",
            'description' => "",
            'image' => $image,
            'created_at' => (array)$this->created_at,
            'created_at_formated' => $this->created_at->timestamp,
            'course' => "",
            'length' => $this->duration,
            'state' => "",
            'institution' => $this->school,
            'date' => $this->created_at->toDateTimeString(),
            'global' => "",
            'producer' => "",
            'criteria' => $this->criteria,
            'contents' => "",
            'resource_type' => "",
            'designer' => ""

        ];

        return $data;
    }

    /**
     * All Teachers Names
     * @return string
     */
    public function authors()
    {
        $author = [];
        foreach ($this->teachers as $teacher) {
            array_push($author, $teacher->fullName());
        }

        $authors = implode(", ", $author);

        if (!$authors) {
            $authors = $this->createdBy->fullName();
        }

        return $authors;
    }
}
