<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyCase extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id',
        'created_by',
        'name',
        'year',
        'category',
        'country_id',
        'source',
        'producer',
        'author',
        'designer',
        'location',
        'main_contact',
        'email',
        'website',
        'status',
        'start_year',
        'description',
        'benefits',
        'published',
        'gps',
        'other_notes',
        'language_id',
        'institution'
    ];

    protected $appends = ['server', 'serverid', 'languagename'];


    /**
     * @return string
     */
    public function getTitle()
    {
        return isset($this->name) ? $this->name : trans('text.no_title');
    }

    /**
     * @return string
     */
    public function getCoverImage()
    {
        return isset($this->image) ? $this->image : "/images/resource/project_big.png";
    }

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Guidelines
     */
    public function guidelines()
    {
        return $this->belongsToMany('App\StudyCaseGuideline')->orderBy('level', 'order');
    }


    /**
     * Guidelines level 0
     */
    public function guidelines_level_0()
    {
        return $this->belongsToMany('App\StudyCaseGuideline')->where('level', 0)->orderBy('order');
    }

    public function getCountryName() {
        if ($this->country()) {
            return $this->country->name;
        }
        return "";
    }

    /**
     * Country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * Get all of the lectures resources.
     */
    public function resources()
    {
        return $this->morphMany('App\Resource', 'resourcable')->orderBy('order');
    }

    /**
     * Get all of the lectures resources.
     */
    public function resources_published()
    {
        return $this->morphMany('App\Resource', 'resourcable')->where('published', 1)->orderBy('order');
    }

    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }


    public function getServerAttribute()
    {
        return url("/");
    }

    public function getServeridAttribute()
    {
        return server_property('server_id') ? server_property('server_id') : 0;
    }

    public function getLanguagenameAttribute()
    {
        if ($this->language_id && isset($this->language)) {
            return $this->language->name;
        }
        return "";
    }
        
    public function imageUrl()
    {
        return $this->image ? url($this->image) : "";
    }


    public function authorName()
    {
        return $this->author;
    }

    public function searchData()
    {
        // create a data array
        return [
            'id'                    => $this->id,
            'title'                 => $this->name,
            'author'                => $this->authorName(),
            'country'               => $this->getCountryName(), //$item->country->name,
            'year'                  => $this->year,
            'language'              => $this->languageName,
            'type'                  => 'study_case',
            'platform'              => str_replace('http://', "", url("/")),
            'platform_id'           => server_property('server_id'),
            'published'             => $this->published,
            'category'              => $this->category,
            'description'           => $this->description,
            'image'                 => $this->imageUrl(),
            'created_at'            => (array) $this->created_at,
            'created_at_formated'   => $this->created_at->timestamp,
            'course'                => "",
            'length'                => "",
            'state'                 => $this->state,
            'institution'           => $this->institution,
            'date'                  => "",
            'global'                => "",
            'producer'              => $this->producer,
            'criteria'              => "",
            'contents'              => "",
            'resource_type'         => "",
            'designer'				=> $this->designer

        ];
    }
}
