<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerProperties extends Model
{
    protected $appends = ['address'];

    public function getAddressAttribute()
    {
        return url("/");
    }
}
