<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class NewNetwork extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by_id',
        'document',
        'resolved',
        'name',
        'description',
        'reference_person',
        'reference_person_email',
        'country_id',
    ];


    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id');
    }

    /**
     * Country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}
