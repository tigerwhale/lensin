<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Network extends Model
{
    use RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'created_by', 'name', 'description'
    ];

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * All Members
     */
    public function members()
    {
        return $this->hasMany('App\NetworkMember');
    }


    /**
     * First Members
     */
    public function top_members()
    {
        return $this->hasMany('App\NetworkMember')->whereNull('network_member_id');
    }
}
