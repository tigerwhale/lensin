<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyCaseGuideline extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait, SoftDeletes;
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'study_case_id', 'study_case_guideline_id', 'created_by', 'title', 'order', 'level', 'reference', 'published'
    ];

    protected $appends = ['serverurl', 'serverid', 'languagename'];
    

    /**
     * Created by
     */
    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }


    /**
     * Study case
     */
    public function study_cases()
    {
        return $this->belongsToMany('App\StudyCase');
    }

    /**
     * Guideline parent
     */
    public function parent()
    {
        return $this->belongsTo('App\StudyCaseGuideline', 'study_case_guideline_id');
    }

    /**
     * Guideline children
     */
    public function children()
    {
        return $this->hasMany('App\StudyCaseGuideline');
    }

    /**
     * Language
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id', 'locale');
    }

    /**
     * Licences
     */
    public function licences()
    {
        return $this->belongsToMany('App\Licence');
    }


    /**
     * @return int|string
     */
    public function getServeridAttribute()
    {
        return server_property('server_id') ? server_property('server_id') : 0;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getServerurlAttribute()
    {
        return url("/");
    }

    /**
     * @return string
     */
    public function getLanguagenameAttribute()
    {
        if ($this->language_id) {
            if ($this->language) {
                return $this->language->name;
            }
        }
        return "";
    }
}
