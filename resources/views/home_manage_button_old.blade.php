<!-- Removed for central - && server_property('central_server') != 1 -->
    <div class="round_button bck-lens" style="display: inline-block;" id="manage_button">
        <i class="fas fa-pencil-alt" aria-hidden="true"></i>

        <div class="resource_options" >
            <table style="width:100%">
            <tr>
                <!-- MANAGE COURSES -->
                @if (Auth::user()->isRole('managecourses'))
                    <td style="padding-right: 10px;">
                        <a href="/courses/modify" onclick="toggleMenu();">
                          <div style="margin: 0 auto;" class="btn-course-small btn-smaller shadow-small" id="edit_courses" title="{{ strtoupper(trans('text.courses')) }}"></div>
                        </a>
                    </td>
                @endif

                <!-- MANAGE LECTURES -->
                @if (Auth::user()->isRole('managestudycases'))
                    <td style="padding-right: 10px;">
                        <a href="/lectures/modify" onclick="toggleMenu();">
                            <div style="margin: 0 auto;" class="btn-lecture-small btn-smaller shadow-small" id="edit_lectures" title="{{ strtoupper(trans('text.lectures')) }}"></div>
                        </a>
                    </td>
                @endif

                <!-- MANAGE CASE STUDIES -->
                @if (Auth::user()->isRole('managestudycases'))
                    <td style="padding-right: 10px;">
                        <a href="/study_cases/modify" onclick="toggleMenu();">
                            <div style="margin: 0 auto;" class="btn-study_case-small btn-smaller shadow-small" id="edit_courses" title="{{ strtoupper(trans('text.study_cases')) }}"></div>
                        </a>
                    </td>
                @endif

                <!-- MANAGE TOOLS -->
                @if (Auth::user()->isRole('managetools'))
                    <td style="padding-right: 10px;">
                        <a href="/tools/modify" onclick="toggleMenu();">
                            <div style="margin: 0 auto;" class="btn-tool-small btn-smaller shadow-small" id="edit_tools" title="{{ strtoupper(trans('text.tools')) }}"></div>
                        </a>
                    </td>
                @endif

                <!-- MANAGE CHALLANGES -->
                @if (Auth::user()->isRole('manageprojects'))
                    <td style="padding-right: 10px;">
                        <a href="/projects" onclick="toggleMenu();">
                            <div title="{{ strtoupper(trans('text.projects')) }}" style="margin: 0 auto;" class="btn-project-small btn-smaller shadow-small" id="edit_challenges"></div>
                        </a>                 
                    </td>
                @endif

                <!-- MANAGE NEWS -->
                @if (Auth::user()->isRole('managenews'))
                    <td>
                        <a href="/news/modify" onclick="toggleMenu();">
                            <div title="{{ strtoupper(trans('news.news')) }}" style="margin: 0 auto;" class="btn-news-small btn-smaller shadow-small" id="edit_challenges"></div>
                        </a>                 
                    </td>
                @endif
            </tr>
            </table>
        </div>
    </div>

    <script>
        $('#manage_button').click(function(){ 
            $('.resource_options').animate({width:'toggle'},350);;
        })
    </script>

    <script>
        function toggleMenu() {
            $('.resource_options').animate({width:'toggle'},350);;
        }
    </script>

