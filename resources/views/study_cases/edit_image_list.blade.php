@if ($study_case->resources->count() > 0)
	<div class="row">
		<div class="col-xs-12">
			<ul class="sortable_ul study_case_images_ul">
				<!-- Upload images -->
				<li class="col-xs-3 study_case_image_li">
					<div style="width: 100%; height: 100px; padding: 5px;">
						<div class="dropzone_div col-xs-3" id="study_case_upload_images">
							{{ trans('text.click_to_select_file') }}
						</div>
					</div>					
				</li>

				@foreach ($study_case->resources as $resource)
					<li class="sortable_li study_case_image_li col-xs-3" data-resource-id="{!! $resource->id !!}">
						<div class="image-frame ">
							<div class="image_container_inner">
								<img src="{{ $resource->path }}" style="max-width:100%; max-height: 140px;">
								<!-- bottom popup -->
								<div class="image_change_bottom_cover bck-studycase">
									<!-- remove image button -->
									<button class="btn btn-danger delete_studycase_image" data-id="{!! $resource->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
								</div>			
							</div>	
						</div>
						<span class="movable_arrow_study_case_image "><i class="fa fa-arrows shadow-text-small" aria-hidden="true"></i></span>
					</li>		
				@endforeach
				
			</ul>
		</div>
	</div>

@else 

	<div class="row">
		<div class="dropzone_div col-xs-3" id="study_case_upload_images">
			{{ trans('text.click_to_select_file') }}
		</div>
	</div>

@endif
<script type="text/javascript">


	// DROPZONE - upload image and refresh imagelist
	$("div#study_case_upload_images").dropzone({ 
		url: "/study_cases/upload_image/{{ $study_case->id }}",
		paramName: "image",
		previewsContainer: false,
		uploadMultiple: false,
		parallelUploads: 1,
		acceptedFiles: "image/*",

		sending: function(files, xhr, formData){
			formData.append( "_token", $("meta[name='csrf-token']").attr('content'));
		},
		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#study_case_upload").html(parseInt(progress) + "%");
		},
		success: function(file, responce) {
			// Update image list
			imageListEdit();
		},
		// error return function
		error: function (file, responce){
			bootbox.alert ("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
		},

		init: function (){
			myDropzone = this;
		}
});
</script>