<?php 
	$images = 0; 
?>
<div class="select_by_criteria_tab">
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item active">
			<a class="nav-link light-gray-bck" data-toggle="tab" href="#images" role="tab">{{ trans('text.images') }}</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link light-gray-bck" data-toggle="tab" href="#record" role="tab">{{ trans('text.records') }}</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<!-- IMAGES -->
		<div class="tab-pane active select_by_criteria_tab_pane" id="images" role="tabpanel">
	  		@foreach($studyCases as $studyCase)
	  			@if (isset($studyCase->resources))
	  				@foreach ($studyCase->resources as $resource)
	  					<?php $images = $images + 1; ?>

	  					@if ($loop->first)
	  						<a href="{!! $resource->path_or_preview() !!}" data-lightbox="slideshow_{!! $studyCase->id !!}" data-title="{!! $resource->name !!}">
	  							{!! $studyCase->name !!}
	  						</a>
	  						@if (!$loop->parent->last)
	  							<hr class="margin-small">
	  						@endif
	  					@else 
	  						<a href="{!! $resource->path_or_preview() !!}" data-lightbox="slideshow_{!! $studyCase->id !!}" style="display: none;" data-title="{!! $resource->name !!}">
	  					@endif

	  				@endforeach
	  			@endif 

	  		@endforeach

			@if ($images == 0)
		  		{{ trans('criteria.no_images') }}
		  	@endif
		</div>
		<!-- RECORDS -->
		<div class="tab-pane select_by_criteria_tab_pane" id="record" role="tabpanel" style="padding: 10px;">
			@if ($studyCases->count() > 0)
		  		@foreach($studyCases as $studyCase)
		  			<a style="margin-bottom: 5px;" href="{{ $studyCase->server }}/study_cases/view/{{ $studyCase->id }}" target="_blank">
		  				{{ $studyCase->name }}
		  			</a>
		  			@if (!$loop->last)
		  				<hr class="margin-small">
		  			@endif
		  		@endforeach
		  	@else 
		  		{{ trans('criteria.no_records') }}
		  	@endif
		</div>
	</div>
</div>