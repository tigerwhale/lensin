<div class="row">
	<div class="col-xs-12" >
		<ul id="guideline_ul_0" class="sortable_ul">
		</ul>
		<div class="btn round_button btn-success create_guideline" data-guideline-id = '0' data-level="-1"><i class="fa fa-plus" aria-hidden="true"></i></div>
	</div>
</div>

<script type="text/javascript">
	
	load_criteria();

	function load_criteria(){
		$.ajax({
			url: '/study_case_guideline/load_guidelines' ,
			type: 'GET',
			success: function(data, status) {
				$('#guideline_ul_0').html(data);

			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}


	// ADD GUIDELINE
    $('body').on('click', '.create_guideline', function(){
    	var guideline_id =  $(this).data('guideline-id');
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/study_case_guideline/create' ,
			type: 'POST',
			data: {
				'_token': token,
				'level': $(this).data('level'),
				'parrent_id': $(this).data('guideline-id'),
			},
			success: function(data, status) {
				$('#guideline_ul_' + guideline_id).append(data);
				//sort_courses();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});


	// DELETE GUIDELINE
    $('body').on('click', '.delete_guideline', function(){
    	var guideline_id =  $(this).data('guideline-id');
		var token = $("meta[name='csrf-token']").attr("content"); 
		
		$.ajax({
			url: '/study_case_guideline/delete/' + guideline_id ,
			type: 'POST',
			data: {
				'_token': token,
				'guideline_id': $(this).data('guideline-id'),
			},
			success: function(data, status) {
				$('#guideline_li_' + guideline_id).remove();
				//sort_courses();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});

    })
</script>