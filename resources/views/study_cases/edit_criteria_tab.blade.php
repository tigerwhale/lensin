@foreach($guidelines as $dimension)
	<div class="row sortable_div_0" style="margin-bottom: 10px;">
		<!-- LEVEL 0 -->
		<div class="col-xs-3">
			<?php if ($study_case->guidelines()->find($dimension->id)) { $checked = "checked";} else { $checked= ""; } ?>
			<input {!! $checked !!} type="checkbox" data-study-case-id="{!! $study_case->id !!}" data-id="{!! $dimension->id !!}" data-parrent="0" id="guideline_{{ $dimension->id }}" class="guideline_update_checkbox">
			<label for="guideline_{!! $dimension->id !!}">{{ $dimension->name }}</label>
		</div>

		<!-- LEVEL 1 -->
		@if ($dimension->children->count() > 0)
			<div class="col-xs-9 sortable_div_1">

			 	@foreach($dimension->children as $criteria)

			 		<div class="row">
			 			<div class="col-xs-4">
			 				<?php if ($study_case->guidelines()->find($criteria->id)) { $checked = "checked";} else { $checked= ""; } ?>
			 				<input {!! $checked !!} type="checkbox" data-study-case-id="{!! $study_case->id !!}" data-id="{!! $criteria->id !!}" data-parrent="{!! $dimension->id !!}" id="guideline_{{ $criteria->id }}" class="guideline_update_checkbox">
			 				<label for="guideline_{{ $criteria->id }}">{{ $criteria->name }}</label>
			 			</div>

		 				<!-- level 2 -->
		 				@if ($criteria->children->count() > 0)
		 					<div class="col-xs-8 sortable_div_2">

				 			@foreach($criteria->children as $guideline)
				 				<div class="row">
				 					<div class="col-xs-5">
				 						<?php if ($study_case->guidelines()->find($guideline->id)) { $checked = "checked";} else { $checked= ""; } ?>
						 				<input {!! $checked !!} type="checkbox" data-study-case-id="{!! $study_case->id !!}" data-id="{!! $guideline->id !!}" data-parrent="{!! $criteria->id !!}" id="guideline_{{ $guideline->id }}" class="guideline_update_checkbox">
										<label for="guideline_{{ $guideline->id }}">{{ $guideline->name }}</label>
				 					</div>

									<!-- level 3 -->
					 				@if ($guideline->children->count() > 0)
					 					<div class="col-xs-7 sortable_div_3">

							 			@foreach($guideline->children as $subguideline)
							 				<div class="row">
							 					<div class="col-xs-12">
							 						<?php if ($study_case->guidelines()->find($subguideline->id)) { $checked = "checked";} else { $checked= ""; } ?>
							 						<input {!! $checked !!} type="checkbox" data-study-case-id="{!! $study_case->id !!}" data-id="{!! $subguideline->id !!}" data-parrent="{!! $guideline->id !!}" id="guideline_{{ $subguideline->id }}" class="guideline_update_checkbox">
													<label for="guideline_{{ $subguideline->id }}">{{ $subguideline->name }}</label>
							 					</div>
											</div>
							 			@endforeach

							 			</div>
							 		@endif
								</div>
				 			@endforeach

				 			</div>
				 		@endif

			 		</div>

				@endforeach

			</div>
		@endif

	</div>
@endforeach


<script type="text/javascript">
	  // UPDATE CHECKBOX
    $('body').on('click', '.guideline_update_checkbox', function(){

        var object = $(this);
        var id = $(this).data('id');
        var study_case_id = $(this).data('study-case-id');
        var parrent = $(this).data('parrent');
        var update_url = "/study_case_guideline/assign/" + {!! $study_case->id !!};
        var token = $("meta[name='csrf-token']").attr("content");

        // If user selects checkbox
        if ($(this).is(":checked")) {
            var value = 1;
            $('#guideline_' + parrent).prop('checked', true).each(function() {
            	$(this).trigger("change");
            	var parrent = $(this).data('parrent');
            	$('#guideline_' + parrent).prop('checked', true).each(function() {
            		$(this).trigger("change");
            		var parrent = $(this).data('parrent');
            		$('#guideline_' + parrent).prop('checked', true).each(function() {
            			$(this).trigger("change");
            		});
            	});
            });

        } else {
            var value = 0;
            $("[data-parrent=" + id + "]").prop('checked', false).each(function (){
            	var id = $(this).data('id');
            	$(this).trigger('change');
            	$("[data-parrent=" + id + "]").prop('checked', false).each(function (){
	            	var id = $(this).data('id');
	            	$(this).trigger('change');
	            	$("[data-parrent=" + id + "]").prop('checked', false).each(function (){
		            	var id = $(this).data('id');
		            	$(this).trigger('change');
            		});
            	});
            });

        }

        var ids = [];
		$(".guideline_update_checkbox:checked").each(function(){
			ids.push($(this).data('id'));
		});

        $.ajax({
            data: {
                'study_case_id': ids,
                'value': value,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(response) {
                //alert(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    });

</script>