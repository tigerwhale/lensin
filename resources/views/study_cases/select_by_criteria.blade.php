@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row" style="position: relative;">
		<div class="col-xs-12" id="select_by_criteria">
			<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
		</div>
		<div class="btn" id="criteria_tab"></div>
	</div>
</div>
<script type="text/javascript">
	var select_by_criteria = function(data){
		ajax_post_to_div("/study_cases/select_by_criteria_view", "select_by_criteria", data);
	}

	$(document).ready(function(){
		data_from_all_servers("/api/study_cases/select_by_criteria_data", select_by_criteria);
	});
</script>

@endsection

