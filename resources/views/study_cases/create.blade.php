@extends('layouts.resource')

@section('content')

	<div class="container">
	
		<form method="POST" action="/study_cases/create" enctype="multipart/form-data">
			{{ csrf_field() }}

			<div class="resource_header bck-study-case">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						<td style="padding: 5px;">
							<button class="btn btn-success" type="submit"><i class="fa fa-check" aria-hidden="true"></i></button>
							<br>
							{{ trans('text.confirm') }}
						</td>
						<td  style="padding: 5px;">
							<button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							<br>
							{{ trans('text.cancel') }}
						</td>
					</tr>
				</table>			
			</div>

			<!-- Ctudy case name -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-3">
					<img src="/images/resource/case.png" class="table_view_image"> {{ strtoupper(trans('study_cases.study_case_title')) }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="name" class="form-control" placeholder="{{ strtoupper(trans('text.insert_title')) }}" required>
				</div>
			</div>

			<div class="row" style="margin-top: 20px">
				
				<div class="col-xs-4 text-center">
					<!-- IMAGE -->
					{{ strtoupper(trans('text.cover_image')) }}
					<input type="file" name="image" class="form-control">

					<!-- CATEGORY -->
					{{ trans('text.category') }}
					<input name="category" id="category" list="categories" class="form-control" required>
					<datalist id="categories">
						@foreach (list_study_case('category') as $category) 
							<option value="{{ $category->category }}">
						@endforeach
					</datalist>

					<!-- PRODUCER -->
					{{ trans('text.producer') }}
					<input name="producer" id="producer" list="producers" class="form-control" required>
					<datalist id="producers">
						@foreach (list_study_case('producer') as $producer) 
							<option value="{{ $producer->producer }}">
						@endforeach
					</datalist>

					<!-- DESIGNER -->
					{{ trans('text.designer') }}
					<input name="designer" id="designer" list="designers" class="form-control" required>
					<datalist id="designers">
						@foreach (list_study_case('designer') as $designer) 
							<option value="{{ $designer->designer }}">
						@endforeach
					</datalist>

					<!-- COUNTRY -->
					{{ trans('text.country') }}
					{!! Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'required' => 'required']) !!}

					<!-- LOCATION -->
					{{ trans('text.location') }}
					<input name="location" id="location" list="locations" class="form-control" required>
					<datalist id="locations">
						@foreach (list_study_case('location') as $location) 
							<option value="{{ $location->location }}">
						@endforeach
					</datalist>

					<!-- YEAR -->
					{{ trans('text.year') }}
					<input name="year" id="year" list="years" class="form-control" value="{!! date('Y') !!}" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year) 
							<option value="{{ $year }}">
						@endforeach
					</datalist>

					<!-- email -->
					{{ trans('text.email') }}
					<input name="email" id="email"  class="form-control" >

					<!-- website -->
					{{ trans('text.website') }}
					<input name="website" id="website"  class="form-control" >


				</div>

				<!-- DESCRIPTION -->
				<div class="col-xs-8">
					{{ strtoupper(trans('text.description')) }}<br>
					<textarea name="description" class="form-control" placeholder="{{ trans('text.insert_description') }}" style="height: 300px;"></textarea>
	
					<!-- SUSTAINABLE BENEFITS -->
					{{ strtoupper(trans('text.sustainable_benefits')) }}<br>
					<input name="benefits" id="benefits" class="form-control" >
				</div>

			</div>
		</div>

	</form>


<!-- Modal -->
<div id="authorsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
			</div>
			<div class="modal-body">
				<table class="display" id="datatable" style="width: 100%">
					<thead>
						<th>{{ trans('text.name') }}</th>
						<th>{{ trans('text.last_name') }}</th>
						<th>{{ trans('text.school') }}</th>
						<th></th>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	
	// DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache": true,
            "url": '/users/list',
            "rowId": 'id',
        },
        "columns": [
            { "data": "name" },
            { "data": "last_name" },
            { "data": "school" },
            { "data": "id" },

        ],
		"aoColumnDefs": [
        {
            "mRender": function ( data, type, row ) {
                return "<button class='btn btn-success' type='button' id='add_teacher'><i class='fa fa-plus' aria-hidden='true'></i></button>";
            },
            "aTargets": [ 3 ]
        },
     	]
    });

    // ALTER TEACHERS
	$('#datatable tbody').on( 'click', '#add_teacher', function () {

		var row_data = table.row( $(this).parents('tr') ).data();

		var author_id = row_data['id'];
		var author_full_name = row_data['name'] + " " + row_data["last_name"];

		var author_input_field = '<div id="author_' + author_id + '">';
		author_input_field += '<input type="hidden" name="authors[]" value="' + author_id + '">';
		author_input_field += author_full_name;
		author_input_field += '<button class="btn btn-danger remove_teacher_button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>';
		author_input_field += '</div>';

		$("#authors_list").append(author_input_field);
		/*
	    table
	        .row( $(this).parents('tr') )
	        .remove()
	        .draw();
		*/
	});


    $('.back-button').click(function(){
        parent.history.back();
        return false;
    }); 


</script>

@endsection
