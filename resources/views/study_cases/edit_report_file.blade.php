@if ($study_case->report)
	<!-- FILE -->
	<div class="row" style="padding-bottom: 10px;" id="file_div">
		<div class="col-xs-10">
			<div id="report" style="width:100%; word-wrap: break-word">
				{!! $study_case->report ? url($study_case->report) : trans('text.no_file') !!}
			</div>
		</div>
		<div class="col-xs-2 text-right">
			<button type="button" class="btn round_button btn-danger delete_report_file" data-study-case-id="{!! $study_case->id !!}">
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
		</div>
	</div>	

@else  
	<!-- REPORT UPLOAD -->
	<div style="background-color: #EEE; padding: 5px;">
		<!-- FILE UPLOAD -->
		<div class="row" id="upload_div">
			<div class="col-xs-12">
				<!-- DROPZONE -->
				<div class="dropzeon dropzone_div" id="study_case_upload">{{ trans('text.click_to_select_file') }}</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">

		var token = $("meta[name='csrf-token']").attr("content"); 
	  	var progressBar_study_case_{{ $study_case->id }} = document.getElementById('progressBar_study_case_{{ $study_case->id }}');
	  	var progressOuter_study_case_{{ $study_case->id }} = document.getElementById('progressOuter_study_case_{{ $study_case->id }}');

		// DROPZONE - upload resouce and open study_case modal
		$("div#study_case_upload").dropzone({ 
			url: "/study_cases/upload_report/{{ $study_case->id }}",
			paramName: "report_file",
			previewsContainer: false,
			uploadMultiple: false,
			parallelUploads: 1,

			sending: function(files, xhr, formData){
				formData.append( "_token", $("meta[name='csrf-token']").attr('content'));
			},
			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#study_case_upload").html(parseInt(progress) + "%");
			},
			success: function(file, responce) {
				// Upload file list
				studyCaseFileList({!! $study_case->id !!});
			},
			// error return function
			error: function (file, responce){
				bootbox.alert ("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			},

			init: function (){
				myDropzone = this;
			}
		});

		
	</script>
@endif