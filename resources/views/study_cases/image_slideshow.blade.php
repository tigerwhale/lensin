@if (count($study_case->resources) > 0)
	<div class="row">
		<div class="col-xs-12">
			@foreach ($study_case->resources as $resource)
				@if ($resource->path)
					<a href="{!! url($resource->path) !!}" data-lightbox="slideshow"><img class="image-slideshow" src="{!! url($resource->path) !!}"></a>
				@endif 
			@endforeach

			<button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
			<button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button>

			<div class="w3-center w3-display-bottommiddle" style="width:100%">
				@foreach ($study_case->resources as $resource)
					<i class="fa fa-circle dots" aria-hidden="true" onclick="currentDiv({!! $loop->index + 1 !!})"></i>
				@endforeach
			</div>
		</div>
	</div>
@endif

<script type="text/javascript">
	
	var slideIndex = 1;
	showDivs(slideIndex);

	function plusDivs(n) {
	    showDivs(slideIndex += n);
	}

	function currentDiv(n) {
		showDivs(slideIndex = n);
	}

	function showDivs(n) {
		var i;
		var x = document.getElementsByClassName("image-slideshow");
		var dots = document.getElementsByClassName("dots");
		if (n > x.length) {slideIndex = 1}    
		if (n < 1) {slideIndex = x.length}
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";  
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" fa-circle ", " fa-circle-o ");
		}
		x[slideIndex-1].style.display = "block";  
		dots[slideIndex-1].className += " fa-circle ";
	}

</script>

