@extends('layouts.resource')

@section('content')

<div class="container">
    
    {!! backendHeader('studycase', 'study_cases', strtoupper(trans('text.study_cases')), 'study_case', true)  !!}

    <!-- HEADER BUTTONS -->
    <div class="row">
        <div class="col-xs-12 text-center">
            <ul id="content_tabs">
                <li class="btn nav-study-case active"><a data-toggle="tab" href="#study_cases_tab">{{ strtoupper(trans('text.study_cases')) }}</a></li>
                <li class="btn nav-study-case"><a data-toggle="tab" href="#criteria_and_guidelines_tab">{{ strtoupper(trans('text.criteria_and_guidelines')) }}</a></li>
            </ul>
            <script>
                $('#content_tabs a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })
            </script>       
        </div>
    </div>

    <div class="row tab-content">
    
        <!-- STUDY CASES TAB -->
        <div role="tabpanel" class="tab-pane active" id="study_cases_tab">

        	<div class="row">

        		<div class="col-xs-12 text-center">
        	        <!-- DATATABLE SEARCH -->
        		    <table class="display" id="datatable">
        		        <thead>
        		            <tr>
        		                <th></th>
                                <th>{{ trans('text.category') }}</th>
        		                <th>{{ trans('text.title') }}</th>
        		                <th>{{ trans('text.description') }}</th>
                                <th>{{ trans('text.author') }}</th>
        		                <th>{{ trans('text.country') }}</th>
        		                <th>{{ trans('text.state') }}</th>
        		                <th>{{ trans('text.language') }}</th>
        		                <th>{{ trans('text.year') }}</th>
        		                <th></th>
        		            </tr>
        		        </thead>
        		    </table>
        		</div>
            </div>
        </div>


        <!-- CRITERIA_AND_GUIDELINES TAB -->
        <div role="tabpanel" class="tab-pane" id="criteria_and_guidelines_tab">
            <div class="row">
                <div class="col-xs-12">
                    @include('study_cases.modify_criteria_tab')
                </div>
            </div>
        </div>
        

	</div>
</div>


<script type="text/javascript">

$( document ).ready(function(){
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "order": [[ 3, "asc" ]],
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/study_cases/datatable_list',
            rowId: 'id',
        },
		
        "columns": [
            { "data": "id" },
            { "data": "category" },
            { "data": "title" },
            { "data": "description",
                "render": function(data){
                    if(data){
                        return (data.length > 300)?data.substring(0, 300)+'...':data;
                    } else {
                        return '';
                    }
                },
            },
            { "data": "author" },
            { "data": "country" },
            { "data": "state" },
            { "data": "language" },
            { "data": "year" },
            { "data": "published" }
        ], 

		"aoColumnDefs": [
            {
                "mRender": function (data, type, row) {
                    return "<div class='text-center' style='width:100%'><img src='/images/resource/" + row['type'] + "_off.png' class='table_view_image bck-study-case'><br><img src='{!! logo() !!}' class='table_view_image platform'></div>";
                },
                'orderable': false,
                "aTargets": [0],
            }, {
                'orderable': false,
                "mRender": function ( data, type, row ) {
                    var modify = '<a title="{{ trans('text.modify') }}" href="/study_cases/edit/' + row['id'] + '"><div class="btn bck-lens"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                    var publish = publishButton(row['published'], 'studycase', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}");
                    var delete_button = '<div title="{{ trans('text.delete') }}" class="btn btn-danger delete_study_case" data-id="' + row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';
                    return modify + publish + delete_button;
                },
                "aTargets": [ -1 ]
            }
        ]
    });


    $("table tbody tr").hover(function(event) {
          $(".drawer").show().appendTo($(this).find("td:first"));
        }, function() {
          $(this).find(".drawer").hide();
    });


   // DELETE RESOURCE 
    $('body').on('click', '.delete_study_case', function(){

        var update_url = '/' + $(this).data('model') + '/delete';
        var id = $(this).data('id');
        var text =  "{{ trans('text.study_case_delete_text') }}";
        
        bootbox.confirm(text, function(result){ 
            // if OK, delete the model
            if (result) {
                window.location.href = '/study_cases/delete/' + id;
            } 
        });
    });

    
});
</script>



@endsection