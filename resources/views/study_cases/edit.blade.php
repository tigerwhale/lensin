@extends('layouts.resource')

@section('content')

<div class="container">
	{!! backendHeader('studycase', 'study_cases', strtoupper(trans('text.study_cases')), 'study_case')  !!}

	<!-- TAB BUTTONS -->
	<div class="row">
		<div class="col-xs-12 text-center">
			<ul id="content_tabs">
				<li class="btn nav-study-case active"><a data-toggle="tab" href="#study_case_studies_tab">{{ strtoupper(trans('text.studies')) }}</a></li>
			    <li class="btn nav-study-case"><a data-toggle="tab" href="#study_case_criteria_tab">{{ strtoupper(trans('text.criteria')) }}</a></li>
			</ul>
		</div>
	</div>

	<!-- STUDY CASE TITLE  -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-3">
			<span class="btn-study_case-small pull-left" style="margin-right: 10px;"></span> <span style="line-height: 30px;">{{ strtoupper(trans('text.study_case_title')) }}</span>
		</div>
		<div class="col-xs-9">
			<input type="text" name="name" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
		</div>
	</div>

	<!-- TAB STUDY CASES -->
	<div class="row tab-content">
	
		<!-- STUDY CASE INFO -->
		<div role="tabpanel" class="tab-pane active" id="study_case_studies_tab">

			<div class="row" style="margin-top: 20px;">
				
				<!-- LEFT -->
				<div class="col-xs-4 text-center">

					<!-- IMAGES -->
					{{ strtoupper(trans('text.cover_image')) }}
					<div class="row">
						<div class="col-xs-12" id="cover_image_container_{!! $study_case->id !!}">
							{!! coverImageEdit($study_case) !!}
						</div>
					</div>
					<!-- CATEGORY -->
					{{ trans('text.category') }}
					<input list="categories" type="text" id="category" name="category" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->category }}" class="form-control update_input" required>
					<datalist id="categories">
						@foreach (list_study_case('category') as $category) 
							<option value="{{ $category->category }}">
						@endforeach
					</datalist>


					<!-- PRODUCER -->
					{{ trans('text.producer') }}
					<input list="producers" type="text" id="producer" name="producer" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->producer }}" class="form-control update_input" required>
					<datalist id="categories">
						@foreach (list_study_case('producer') as $list_data) 
							<option value="{{ $list_data->producer }}">
						@endforeach
					</datalist>

					<!-- DESIGNER -->
					{{ trans('text.designer') }}
					<input list="designers" type="text" id="designer" name="designer" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->designer }}" class="form-control update_input" required>
					<datalist id="designers">
						@foreach (list_study_case('designer') as $list_data) 
							<option value="{{ $list_data->designer }}">
						@endforeach
					</datalist>

					<!-- COUNTRY -->
					{{ trans('text.country') }}
					{!! Form::select('country_id', $countries, $study_case->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'study_cases', 'data-id'=> "$study_case->id" ]) !!}

					<!-- LOCATION -->
					{{ trans('text.location') }}
					<input list="locations" type="text" id="location" name="location" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->location }}" class="form-control update_input" required>
					<datalist id="locations">
						@foreach (list_study_case('location') as $list_data) 
							<option value="{{ $list_data->location }}">
						@endforeach
					</datalist>

					<!-- LANGUAGE -->
					{{ trans('text.language') }}
					{!! Form::select('language_id', $languages, $study_case->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'study_cases', 'data-id'=> "$study_case->id", 'placeholder' => trans('text.select_language')  ]) !!}

					<!-- YEAR -->
					{{ trans('text.year') }}
					<input type="text" id="year" name="year" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->year }}" class="form-control update_input" required maxlength="4">
					
					<!-- EMAIL -->
					{{ trans('text.email') }}
					<input type="email" id="location" name="email" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->email }}" class="form-control update_input" required>
					
					<!-- WEBSITE -->
					{{ trans('text.website') }}
					<input list="website" type="text" id="website" name="website" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->website }}" class="form-control update_input" required>
					
					<!-- INSTITUTION -->
					{{ trans('text.institution') }}
					<input list="institution" type="text" id="institution" name="institution" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->institution }}" class="form-control update_input" required>
					<!-- AUTHOR -->
					{{ trans('text.author') }}
					<input  type="text" id="author" name="author" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->author }}" class="form-control update_input" required>

					<!-- UPLOAD REPORT --> 
					{{ strtoupper(trans('text.report')) }}
					<div class="row">

						<div class="col-xs-12" id="file_list">
							@include('study_cases.edit_report_file', ['study_case' => $study_case])
						</div>
					</div>
					
				</div>

				<!-- RIGHT -->
				<div class="col-xs-8">

					<!-- Image slideshow -->
					{{ strtoupper(trans('study_cases.case_study')) }}<br>
					<div id="studyCaseImageSlideshow"></div>
					<!-- Image list -->
					<div class="row">
						<div class="col-xs-12">
							<div class="study_case_images"></div>
						</div>
					</div>
					
					<!-- DESCRIPTION -->
					{{ strtoupper(trans('text.description')) }}<br>
					<textarea maxlength="300" name="description" class="form-control update_input" data-model='study_cases' data-id={{ $study_case->id }} placeholder="{{ trans('text.insert_description') }}" style="height: 150px;">{{ $study_case->description }}</textarea>
					<br>

					<!-- BENEFITS -->
					{{ strtoupper(trans('text.sustainable_benefits')) }}<br>
					<!-- <input name="benefits" type="text" id="benefits" data-model='study_cases' data-id={{ $study_case->id }} value="{{ $study_case->benefits }}" class="form-control update_input" required> -->
					<textarea maxlength="300" name="benefits" type="text" id="benefits" data-model='study_cases' data-id={{ $study_case->id }} placeholder="{{ trans('text.sustainable_benefits') }}" style="height: 150px;" class="form-control update_input">{{ $study_case->benefits }}</textarea>
					<br>
					
				</div>
			</div>
		</div>

		<!-- TAB CRITERIA  -->
		<div role="tabpanel" class="tab-pane" id="study_case_criteria_tab">
			@include('study_cases.edit_criteria_tab', ['guidelines' => $guidelines, 'study_case' => $study_case])
		</div>				

	</div>
</div>


<!-- Authors Modal -->
<div id="authorsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
			</div>
			<div class="modal-body">
				<table class="display" id="datatable" style="width: 100%">
					<thead>
						<th>{{ trans('text.name') }}</th>
						<th>{{ trans('text.last_name') }}</th>
						<th>{{ trans('text.school') }}</th>
						<th></th>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>


<!-- Resource Modal -->
<div id="resourceModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ strtoupper(trans('text.add_resource_information')) }}</h4>
			</div>
			<div class="modal-body" id="resource_modal_div">
				
			</div>
		</div>

	</div>
</div>


<script>

// DELETE THE REPORT LINK
function report_delete(data) {
	$('#report_container').html("");
}

$( document ).ready(function() {

	imageListEdit()

	// REMOVE COURSE IMAGE
	$('body').on('click', '.delete_studycase_image', function(){
		var token = $("meta[name='csrf-token']").attr("content"); 
		var id = $(this).data('id');
		$.ajax({
			url: '/study_cases/delete_image' ,
			type: 'POST',
			data: {
				'_token': token,
				'id': id
		    },
			success: function(data, status) {
				imageListEdit();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	})

});


	function imageListEdit(){
		$(".study_case_images").html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>');
		var token = $("meta[name='csrf-token']").attr("content"); 
		$.ajax({
			url: '/study_cases/edit_image_list/{!! $study_case->id !!}' ,
			type: 'POST',
			data: {
				'_token': token,
		    },
			success: function(data, status) {
				$(".study_case_images").html(data);
				study_case_images_sortable();
				studyCaseImageSlideshow({!! $study_case->id !!});
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}

	function studyCaseFileList(study_case_id)
	{
		var token = $("meta[name='csrf-token']").attr("content"); 
		$.ajax({
			url: '/study_cases/file_list/' + study_case_id,
			type: 'POST',
			data: {
				'_token': token,
		    },
			success: function(data, status) {
				$("#file_list").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}


	$('body').on('click', '.delete_report_file', function()
	{
		var token = $("meta[name='csrf-token']").attr("content"); 
		$.ajax({
			url: '/study_cases/delete_report_file/{!! $study_case->id !!}',
			type: 'POST',
			data: {
				'_token': token,
		    },
			success: function(data, status) {
				studyCaseFileList({!! $study_case->id !!});
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});

	// Make images sortable
	function study_case_images_sortable() {
		$(".sortable_ul").sortable({
			handle: ">.movable_arrow_study_case_image",
			stop: function( event, ui ) {
				// get the token
				var token = $("meta[name='csrf-token']").attr("content"); 

				// make subject id-s		
				var resources = [];

				$(this).children('.sortable_li').each(function(i){
					resources.push($(this).data('resource-id'));
				});

				// send to server
				$.post({
					url: '/resources/update_sort',
					data: {
						'_token': token,
						'resources': resources,
					},
					success: function(data, status) {
						studyCaseImageSlideshow({!! $study_case->id !!});
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
		  	}
		});
	}


</script>

@endsection