<!-- content header -->
<div class="row" style="padding-bottom: 20px; margin-left: -40px; margin-right: -40px;">
    <div class="col-xs-12 text-center">
        <ul id="content_tabs" class="nav nav-tabs">
            {!! left_back_button('study_cases') !!}
		  	<li class="active">
		  		<a data-toggle="tab" href="#study_case_tab">
		  			{{ strtoupper(trans('text.cases')) }}
		  		</a>
		  	</li>
		  	<li>
				<a data-toggle="tab" href="#criteria_tab_content" id="open_courseware">
					{{ strtoupper(trans('text.criteria')) }}
				</a>
			</li>
            <li style="position: absolute; right: 15px;">
                {!! view_counter_icons($study_case) !!}
            </li>
		</ul>
	</div>
</div>

<div class="row tab-content">

	<!-- STUDY CASE INFO -->
	<div role="tabpanel" class="tab-pane active" id="study_case_tab">

		<!-- title -->
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-xs-12">
				<span class="title">{{ strtoupper(trans('text.title')) }}</span>
				<table>
					<tr>
						<td style="padding-right: 10px;">
							<div class="btn-study_case-small pull-left">&nbsp;</div>
						</td>
						<td>
							<div class="resource-title text-study-case">{{ $study_case->name }}</div>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<!-- CONTENT --> 
		<div class="row">
			
			<!-- LEFT -->
			<div class="col-md-4 col-xs-12">

				<!-- COVER IMAGE --> 
				{!! coverImage($study_case, 'study_case_big.png') !!}

	        	<!-- Download report --> 
	        	@if ($study_case->report)
	        		<span class="title" style="line-height: 45px;">{{ strtoupper(trans('text.report')) }}:</span>
	        		{!! downloadLinkAndIncrement("api/study_cases/increment_download/" . $study_case->id, $request) !!}
	        	@endif

				<!-- DATA --> 
				<table style="width: 100%;">
					{!! (isset($study_case->category)) ? view_td('category', $study_case->category) : "" !!}
					{!! (isset($study_case->producer)) ? view_td('producer', $study_case->producer) : "" !!}
					{!! (isset($study_case->designer)) ? view_td('designer', $study_case->designer) : "" !!}
					{!! (isset($study_case->location)) ? view_td('email', $study_case->location) : "" !!}
					{!! (isset($study_case->language_id)) ? view_td('language', $study_case->language->name) : "" !!}
					{!! (isset($study_case->year)) ? view_td('year', $study_case->year) : "" !!}
					{!! (isset($study_case->web)) ? view_td('web', $study_case->web) : "" !!}
					{!! (isset($study_case->author)) ? view_td('author', $study_case->author) : "" !!}
					{!! (isset($study_case->institution)) ? view_td('institution', $study_case->institution) : "" !!}
				</table>

			</div>

			<!-- MIDDLE CONTENT -->
			<div class="col-md-8 col-xs-12">

				<!-- CASE STUDY IMAGES -->
				@if (isset($study_case->resources))
					{{ strtoupper(trans('study_cases.case_study')) }}
					<div id="studyCaseImageSlideshow">
						@include('study_cases.image_slideshow', ['study_case' => $study_case])
					</div>
				@endif 

				<!-- DESCRIPTION -->
				{!! isset($study_case->description) ? displayTitleAndTextNewRow( strtoupper(trans('text.description')), $study_case->description ) : "" !!}

				<!-- BENEFITS -->
				{!! isset($study_case->benefits) ? displayTitleAndTextNewRow( strtoupper(trans('text.sustainable_benefits')), $study_case->benefits ) : "" !!}

				<hr>
				<div class="row">
					<div class="col-xs-12">
						<h5>{{ strtoupper(trans('text.criteria')) }}</h5>
					</div>
				</div>					

				<!-- guidelines -->
				<div class="row text-study-case">

					<div class="col-xs-2">
						{{ strtoupper(trans('text.dimension')) }}
					</div>
					<div class="col-xs-2">
						{{ strtoupper(trans('text.criteria')) }}
					</div>
					<div class="col-xs-2">
						{{ strtoupper(trans('text.guideline')) }}
					</div>
					<div class="col-xs-2">
						{{ strtoupper(trans('text.subguideline')) }}
					</div>
				</div>

				@if (isset($study_case->guidelines))
					@foreach($study_case->guidelines as $guidelineLvl0)
						@if ($guidelineLvl0->level == 0)
							<!-- dimension -->
							<div class="row">
								<div class="col-xs-6">
									{{ $guidelineLvl0->name }}
								</div>
							</div>

							@foreach($study_case->guidelines as $guidelineLvl1)
								@if ($guidelineLvl1->level == 1 && $guidelineLvl1->parent->id == $guidelineLvl0->id)
									<div class="row">
										<div class="col-xs-2 text-right">
											<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
										</div>
										<div class="col-xs-6">
											{{ $guidelineLvl1->name }}
										</div>
									</div>

									@foreach($study_case->guidelines as $guidelineLvl2)
										@if ($guidelineLvl2->level == 2 && $guidelineLvl2->parent->id == $guidelineLvl1->id)
											<div class="row">
												<div class="col-xs-4 text-right">
													<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
												</div>
												<div class="col-xs-6">
													{{ $guidelineLvl2->name }}
												</div>
											</div>

											@foreach($study_case->guidelines as $guidelineLvl3)
												@if ($guidelineLvl3->level == 3 && $guidelineLvl3->parent->id == $guidelineLvl2->id)
													<div class="row">
														<div class="col-xs-6 text-right">
															<i class="fa fa-long-arrow-right text-study-case " aria-hidden="true"></i>
														</div>
														<div class="col-xs-6">
															{{ $guidelineLvl3->name }}
														</div>
													</div>
												@endif
											@endforeach
										
										@endif
									@endforeach

								@endif
							@endforeach

						@endif
					@endforeach

				@endif
			</div>
		</div>
	</div>

	<!-- TAB CRITERIA  -->
	<div role="tabpanel" class="tab-pane" id="criteria_tab_content">
        <!-- CRITERIA VIEW CONTENT -->
        <div class="row" style="position: relative;">
            <!-- criteria header -->
            <div id="criteria_header" class="row text-study-case" style="margin-top: 12px; margin-left: 6px;">
                {{ strtoupper(trans('text.dimension')) }} | {{ strtoupper(trans('text.criteria')) }} | {{ strtoupper(trans('text.guideline')) }} | {{ strtoupper(trans('text.subguideline')) }}
            </div>
            <!-- criteria body -->
            <div id="criteria_view_content">
				<div class="criteria_loader">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
            <div id="criteria_tab"></div>
        </div>
	</div>
</div>		

<script type="text/javascript">
	loadCriteriaTabView();

	function loadCriteriaTabView(){
		ajax_post_to_div("/study_cases/select_by_criteria_view", "criteria_view_content", "");
    };
</script>

