@extends('layouts.app')

@section('content')

	<!-- STUDY CASES -->
	<div class="container">
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['active_switch' => "study_cases"])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-8" style="margin-top: 20px;" id="study_case_data">
				<div class="text-center" style="width: 100%;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
			</div>

			<!-- MENU RIGHT -->
	        <div class="col-xs-2">
	            @include('right_menu_filter', ['home_page' => 1])
	        </div>
	        
		</div>
	</div>

	<!-- STUDY CASES AJAX -->
	<script type="text/javascript">
		var token = $("meta[name='csrf-token']").attr("content"); 
		
        $.ajax({
            url: "{!! $server_url !!}/api/study_cases/view/{!! $study_case_id !!}",
            type: 'get',
            data: {
            	'user_id': "{!! Auth::user() ? Auth::user()->id : '' !!}",
            	'_token': token,
            },
            success: function(data, status) {
                $('#study_case_data').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
	</script>

@endsection
