<div class="row"> 
    <div class="col-xs-12" id="select_by_criteria_div">

    @foreach($study_case_guidelines as $study_case_guideline)
		@if ($loop->first && !isset($guideline_parent))
			<?php 
				$server_ids = (array) $study_case_guideline;
			?>
		@else 
			@if ( in_array($study_case_guideline->serverid, $server_ids) )
			
				<?php 
					$display_criteria = true;
					$case_index = "_" . $study_case_guideline->serverid . "_" . $study_case_guideline->id; 
					$row_index = isset($row_index) ? ($row_index + 1) : 1;
					$info = $study_case_guideline->languagename ? (trans('text.language') . ": " . $study_case_guideline->languagename . "<br>") : "";
					$info .= isset($study_case_guideline->author) ? (trans('text.author') . ": " . $study_case_guideline->author . " <br>" ) : "";
					$info .= isset($study_case_guideline->year) ? (trans('text.year') . ": " . $study_case_guideline->year . " <br> " ) : "";
					$info .= isset($study_case_guideline->institution) ? (trans('text.institution') . ": " . $study_case_guideline->institution . " <br> " ) : "";
					if (isset($study_case_guideline->licences)) {
						if (count($study_case_guideline->licences) > 0) {
							$counter = 1;
							$count = count($study_case_guideline->licences);
							$info .= trans('text.licence') . ": ";
							foreach ($study_case_guideline->licences as $licence) {
								$info .= $licence->icon;
								if ($counter == $count) continue; 
								$info .= " ";
			    				$counter++;
							}
							$info .= " <br>";
						}
					}
					if (server_property('central_server')) {
						$info .= isset($study_case_guideline->serverurl) ? (trans('text.platform') . ": " . $study_case_guideline->serverurl) : "";	
					}
				
					$info = $info ? $info : trans('text.no_info');
				?>
				
				<div class="panel-group criteria-group" id="accordion{!! $case_index !!}">
					<div class="panel panel-default criteria-panel" style="box-shadow: none;">
						<!-- subject heading -->
						<div class="panel-heading criteria-header {!! $row_index !!} {!! odd_even($row_index) !!}" style="background-color: transparent;">
							<table style="width: 100%; min-height: 55px;">
								<tr style="width: 100%; min-height: 55px;" id="criteria{!! $case_index !!}"  title="{{ $info }}"
									@if ( isset($guideline_parent) )
										data-parent="criteria_{{ $guideline_parent->serverid }}_{{ $guideline_parent->id }}" 
									@endif 
									class="criteria-name" data-criteria-id="{{ $study_case_guideline->id }}" data-server-url="{{ $study_case_guideline->serverurl }}">
									<!-- study case server icon-->
									@if ($study_case_guideline->level == 0)
										<td style="width: 30px; vertical-align: middle; padding-top: 4px;">
											<div class="round_button btn-smallest" style="display: inline-block; background-size: 100%; background-image: url({!! $study_case_guideline->serverurl . '/images/server/logo.png' !!})"></div>
										</td>
									@endif
									<!-- study case name-->
									<td style="vertical-align: middle; width: 100%; min-height: 55px;">
										<span>
											{{ $study_case_guideline->name }} 
										</span>
									</td>
									<!-- study case icon-->
									@if (isset($study_case_guideline->study_cases))
										@foreach($study_case_guideline->study_cases as $study_case)
											@if (isset($study_case->resources) && $loop->first)
												<td style="width: 40px; vertical-align: top;">
													<div class="round_button bck-study-case lens criteria-name" style="display: inline-block;">g</div>
												</td>
											@endif
										@endforeach
									@endif 

									<!-- collapse header icon-->	
									@if (isset($study_case_guideline->children))
										@if ($study_case_guideline->children)
											<td style="width: 20px; vertical-align: middle; text-align: center;">
												<a class="collapse_arrow" data-toggle="collapse" data-parent="#accordion{!! $case_index !!}" href="#collapse{!! $case_index !!}">
													<i class="fa fa-caret-down fa-lg arrow" aria-hidden="true"></i>
												</a>
											</td>
										@endif
									@endif							
								</tr>
							</table>

							<!-- info icon	
							@if ($study_case_guideline->level == 0)
								<div class="round_button bck-study-case btn-smallest"  style="display: inline-block;">i</div>
							@endif
							-->
						</div>

						<div id="collapse{!! $case_index !!}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{!! $case_index !!}">
						    <div class="panel-body criteria-body">
						    	@if(isset($study_case_guideline->children))
									@if ($study_case_guideline->children)
										@include('study_cases.select_by_criteria_view', ['study_case_guidelines' => $study_case_guideline->children, 'guideline_parent' => $study_case_guideline])
									@endif
								@endif
						    </div>
						</div>
					</div>
				</div>
			
			@endif

		@endif
	@endforeach

	@if (!isset($display_criteria)) 
		<div class="text-center" style="width:100%; margin-left: 125px;">
			<div class="no_cases_badge resource_badge"></div>
			<br>
			{{ trans('text.no_criteria') }}
		</div>
	@endif 

    </div> 
</div>   