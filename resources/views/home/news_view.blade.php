    <!-- NEWS --> 
    @if($news_json)
        @foreach($news_json as $news_story) 
            <div class="image-slideshow" style="display:none;"> 
                <div class="row">
                    <div class="col-xs-12">
                        <div class="home_news_image" style="background-image: url({!! isset($news_story["image"]) ? $news_story["image"] : "/images/resource/news_big.png"  !!}); "></div>
                    </div>
                </div>
                <div class="row home_news_text "> 
                    <div class="col-xs-12 text-left"> 
                        <div class="bck-news" style="width: 100%; padding: 15px;">
                            <a href="{!! $news_story["news_link"] !!}"> 
                                <h2>{{ $news_story["title"] }}</h2> 
                                {!! substr($news_story["text"], 0, 50) !!}...  
                            </a> 
                        </div>
                    </div> 

                </div> 
            </div> 
        @endforeach 

        <!-- Dots --> 
        <div class="w3-center text-center" style="width:100%"> 
            @foreach($news_json as $news_story) 
                <i class="fa fa-circle dots" aria-hidden="true" onclick="currentDiv({!! $loop->index + 1 !!}); clearInterval(startInterval);"></i>
            @endforeach  
        </div> 
         
        <!-- News butons left/right --> 
        <button class="w3-button w3-display-left" onclick="plusDivs(-1); clearInterval(startInterval);">&#10094;</button> 
        <button class="w3-button w3-display-right" onclick="plusDivs(+1); clearInterval(startInterval);">&#10095;</button> 
        
        <script type="text/javascript">
            var slideIndex = 1;
            showDivs(slideIndex);

            // show other news
            var startInterval = setInterval(function() { plusDivs(+1); }, 3000);
        </script>

    @else
        {{ trans('news.no_news') }}
    @endif
