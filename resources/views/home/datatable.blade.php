<script type="text/javascript">
    $(document).ready(function() {
        $("#loaderContainer").show();
    });
</script>

<!-- Critera tabs -->
<div class="row" id="criteria_view_selector" style="display: none; padding-bottom: 20px;">
    <div class="col-xs-12 text-center">
        <ul id="content_tabs" class="nav nav-tabs">
            <li id="nav-study-case-tab" class="active"><a data-toggle="tab" href="#study_case_tab" id="study_case_tab_load">{{ strtoupper(trans('text.cases')) }}</a></li>
            <li id="nav-criteria-tab"><a data-toggle="tab" href="#criteria_tab" id="criteria_tab_load">{{ strtoupper(trans('text.criteria')) }}</a></li>
        @if (Auth::user())
            <li id="nav-criteria-tab" style="position: absolute; right: 0px;"><a href="study_cases/exportToJsonForOffline" target="_blank" id="criteria_tab_load">{{ strtoupper(trans('text.offline_cases')) }}</a></li>
        @endif
        </ul>
    </div>
</div>

<div class="row content-header" id="select_by_criteria" style="display:none;">
    <div  class="row text-study-case" style="display: none; margin-top: 12px; margin-left: 6px;">
        {{ strtoupper(trans('text.dimension')) }} | {{ strtoupper(trans('text.criteria')) }} | {{ strtoupper(trans('text.guideline')) }} | {{ strtoupper(trans('text.subguideline')) }}
    </div>

    <!-- criteria body -->
    <div id="criteria_view_content">
        <div class="criteria_loader">
            <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
        </div>
    </div>
    <div id="criteria_tab"></div>
</div>

<div class="row" id="resources_view">
    <div class="col-xs-12">
        <!-- selector -->
        <div class="row">
            <div class="col-xs-12 text-center">
                <i id="grid_view_selector" class="fa fa-th-large resource_view_selector text-lens"></i>
                <i id="list_view_selector" class="fa fa-bars resource_view_selector"></i>

                <span class="text-lens" style="font-size: 10pt; font-weight: bold; margin-left:10px;"></span>

                <div class="btn-group" style="margin-top: -9px;">
                    <button class="btn btn-active dropdown-toggle text-lens" id="dropdown-button" type="button" data-toggle="dropdown" style="padding: 0px 10px; background-color: #E5E5E5">
                        {{ strtoupper(trans('text.sort_by')) }}: {{ strtoupper(trans('text.year')) }} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li class="text-center sort_selector hover" data-sort="4">{{ strtoupper(trans('text.year')) }}</li>
                        <li class="text-center sort_selector hover" data-sort="1">{{ strtoupper(trans('text.title')) }}</li>
                        <li class="text-center sort_selector hover" data-sort="2">{{ strtoupper(trans('text.author')) }}</li>
                        <li class="text-center sort_selector hover" data-sort="3">{{ strtoupper(trans('text.country')) }}</li>
                        <li class="text-center sort_selector hover" data-sort="5">{{ strtoupper(trans('text.language')) }}</li>
                        <li class="text-center sort_selector hover" data-sort="6">{{ strtoupper(trans('text.type')) }}</li>
                    </ul>
                </div>

            </div>
        </div>  
        
        <!-- DATATABLE -->
        <div class="row" id="home_data_table" style="display: none;">
            <div class="col-xs-12">
                <table class="display home_datatable" id="datatable" style="width:100%;">
                   
                </table>
            </div>
        </div>
    </div>
</div>



