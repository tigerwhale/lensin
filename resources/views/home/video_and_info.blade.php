<!-- VIDEO and INFO -->
<div class="row" id="home_info" style="display: none;">
    
    <!-- COOKIE -->
    <div class="col-xs-12" style="text-align:center;">
        <div id="close-me" style="display: none;">
            <h2>{{ page_title('home') }}</h2>
            {!! page_text('home') !!}
            
            <!-- VIDEO -->
            <div style="width: 100%; text-align: center;">
                {!! homePageVideo() !!}
                <!-- COOKIE -->
                <div class="close-div">
                    <a><em class="close-button">&times;</em>{{ trans('text.hide_introduction_video_and_text') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

