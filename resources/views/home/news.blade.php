<div class="row" id="news" style="margin-bottom: 20px;">
    <div class="col-xs-12 text-center">
        <h2>{{ trans('text.news') }}</h2> 
        <hr class="margin-small">
    </div>

    <div class="col-xs-12 text-center" id="news_div">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="margin-top: 20px; margin-bottom: 20px;"></i>
    </div>
</div>


<script type="text/javascript">

    var populate_news_function = function(news_json){
        $.post({
            url: '/news/home_page_view',
            data: {
                'data': news_json,
                '_token': token,
            },
            success: function(data, status) {
                $("#news_div").html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })
    }

    $(document).ready(function() {
        @if (server_property("central_server") == 1)
            data_from_all_servers("/api/news/latest", populate_news_function);
        @else
            populate_div_with_request('/api/news/latest', '/news/home_page_view', "#news_div");
        @endif
    });
</script>