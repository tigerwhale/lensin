<?php 
	$advanced_reasearch_buttons = [ 2 => 'author', 3 => 'country', 4 => 'year', 15 => 'course', 16 => 'length', 18 => 'institution', 21 => 'producer', 10 => 'category', 17 => 'contents' ];
?>
<div class="row" id="advanced_research" style="display: none;">
	<div class="col-xs-12 text-center">
		<h2>{{ trans('text.search') }}</h2>
	</div>
	<div class="col-xs-12">
		@foreach ($advanced_reasearch_buttons as $advanced_reasearch_button)
			<select id="advanced_research_{!! $advanced_reasearch_button !!}" class="advanced_research_select datatable_search_{!! $advanced_reasearch_button !!}" multiple="multiple" data-buttontext="{{ trans('advanced_research.any_' . $advanced_reasearch_button) }}" data-buttontype="{{ trans('text.' . $advanced_reasearch_button) }}">
			</select> 
		@endforeach

		<div class="multiselect round_button bck-lens clear_advanced_search_filters">X</div>
	</div>
</div>
