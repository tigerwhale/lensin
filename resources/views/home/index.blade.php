@extends('layouts.app')

@section('content')
	
	<div class="container"> 
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-xs-2" style="min-height: 600px;">
				@include('left_menu_filter', ['home_page' => 1])
			</div>

			<!-- MIDDLE CONTENT -->
			<div class="col-xs-8" style="margin-top:20px;">
                
                <!-- VIDEO AND INFO -->
                @include('home.video_and_info')
                
                <!-- NEWS -->
                @include('home.news')

                <!-- SHOW INTRODUCTION -->
                <div class="row" id="show-video" style="display: none;">
                    <div class="col-xs-12 text-center">
                        <div class="show-video-button">
                            <a><em>&times;</em> {{ trans('text.show_introduction') }}</a>
                        </div>
                    </div>
                </div>
                
                <!-- ADVANCED RESEARCH-->
                @include('home.advanced_research')

                <!-- DATA TABLE -->
                @include('home.datatable')

            </div>

            <!-- MENU RIGHT -->
            <div class="col-xs-2">
                @include('right_menu_filter', ['home_page' => 1])
            </div>

		</div>
	</div>

    <script>
        var home_page = true;
        $(document).ready(function() {
            $("#loaderContainer").show();
            setTimeout(function() {
                loadSearchData();
                loadCriteriaData();
            }, 1);

        });

        $('body').on('click', '.datatable_grid_div', function() {
            window.location.href = $(this).data('link');
        });

        $('body').on('click', '.datatable_row', function () {
            var data = datatable.row( this ).data();
            if (server_id != data.platform_id) {
                location.href = '/' + data.type + 's/view/' + data.id + '?server_id=' + data.platform_id;    
            } else {
                location.href = '/' + data.type + 's/view/' + data.id;    
            }
        });


        $('#grid_view_selector').click(function() {
            showGridView();
        });

        $('#list_view_selector').click(function() {
            showListView()
        });

        $('#criteria_tab_load').click(function() {
            showCriteriaView();
        });
     
        $('#study_case_tab_load').click(function() {
            showResources();
        });     


        function showResources() {
            $('#resources_view').show();
            hideCriteria();
        }

        function hideCriteria(){
            // hide criteria selector
            $('#criteria_header').hide(); 
            // hide criteria view
            $('#select_by_criteria').hide();
        }

        function showGridView() {
            $(".noResourcesDiv").show();
            // change color of selector
            $('#grid_view_selector').addClass("text-lens");
            $('#list_view_selector').removeClass("text-lens");
            // show datatable
            $('#grid_datatable').show();
            // hide list view
            $('#datatable').hide();

            hideCriteria();
            showResources();
        }

        function showListView() {
            $(".noResourcesDiv").hide();
            // change color of selector
            $('#grid_view_selector').removeClass("text-lens");
            $('#list_view_selector').addClass("text-lens");
            // show list view
            $('#datatable').show();
            // hide datatable
            $('#grid_datatable').hide();

            hideCriteria();
            showResources();
        }

        function showCriteriaView() {
            // show criteria selector
            $('#criteria_header').show(); 
            // show criteria view
            $('#select_by_criteria').show();  
            // hide resources view
            $('#resources_view').hide();
        }

        // sort
        $('.sort_selector').click(function() {
            $('#dropdown-button').html( "{{ strtoupper(trans('text.sort_by')) }}: " +  $(this).html() + ' <span class="caret"></span>');
            datatable.order( [ $(this).data('sort'), 'asc' ] ).draw();
        });

        // criteria
        var select_by_criteria = function(data) {
            ajax_post_to_div("/study_cases/select_by_criteria_view", "criteria_view_content", data);
        }


        // IF CENTRAL SERVER, OPEN API VIEWS
        @if (server_property('central_server') == 1)
            // Open course on click
            $('#datatable tbody').on('click', 'tr', function () {
                var data = table.row( this ).data();
                location.href = '/' + data[6] + 's/view/' + data[0] + '?server_id=' + data[8];
            });
        @else 
            // Open course on click
            $('#datatable tbody').on('click', 'tr', function () {
                var data = table.row( this ).data();
                location.href = '/' + data[6] + 's/view/' + data[0];
            });
        @endif

        $('body').on('click', '.datatable_grid_div', function() {
            window.location.href = $(this).data('link');
        });        

        // NEWS
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function currentDiv(n) {
            showDivs(slideIndex = n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("image-slideshow");
            var dots = document.getElementsByClassName("dots");
            if (n > x.length) {slideIndex = 1}    
            if (n < 1) {slideIndex = x.length}
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" fa-circle ", " fa-circle-o ");
            }
            x[slideIndex-1].style.display = "block";  
            dots[slideIndex-1].className += " fa-circle ";
        }
    </script>

@endsection