@extends('layouts.resource')

@section('content')

<div class="container">

    {!! backendHeader('lecture', 'lectures', strtoupper(trans('text.lectures')), 'lecture')  !!}

	<div class="row">
		<div class="col-xs-12 text-center">		
	        <!-- DATATABLE SEARCH -->
		    <table class="display" id="datatable">
		        <thead>
		            <tr>
		                <th class="col-xs-1"></th>
		                <th class="col-xs-2">{{ trans('text.title') }}</th>
		                <th class="col-xs-2">{{ trans('text.course') }}</th>
                        <th class="col-xs-5">{{ trans('text.authors') }}</th>
		                <th class="col-xs-5">{{ trans('text.year') }}</th>
		                <th class="col-xs-2"></th>
		            </tr>
		        </thead>
		    </table>
		</div>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 
    var reloadTable = function reloadTable() { table.ajax.reload(); };
    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/lectures/datatable_list',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "course" },
            { "data": "author" },
            { "data": "year" }            
        ], 

		"aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {
                
                var modify = '<button type="button" title="{{ trans('text.modify') }}" class="btn bck-lens shadow modal_url" data-close-function="reloadTable" data-url="/lectures/edit_modal/' +  row['id'] + '"><i class="fas fa-pencil-alt" aria-hidden="true"></i></button> ';
                var publish = publishButton(row['published'], 'lecture', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
            	var delete_button = ' <div class="btn btn-danger delete_lecture" title="{{ trans('text.delete') }}" data-lecture-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + publish + delete_button;
            },
            "aTargets": [ 5 ]
        }]

    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    $('body').on('click', '.delete_lecture', function() {

        var lecture_id = $(this).data('lecture-id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_lecture') }}", function(result){ 
            // if OK, delete the model
            if (result) {
               $.ajax({
                    method:"POST",
                    url:"/lectures/delete",
                    success: function(responce){
                        table.ajax.reload();
                    },
                    data: {
                        '_token': token,
                        'id' : lecture_id
                    },
                }); 

            }
        });
    });

    // DELETE LECTURE CONTENT
    $('body').on('click', '.delete_lecture_content', function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var content_id = $(this).data('content-id');
        var lecture_id = $(this).data('lecture-id');

        // ASK THE QUESTION 
        bootbox.confirm({
            message: "{{ trans('courses.delete_content_question') }}",
            buttons: {
                confirm: {
                    label: '{{ trans('text.delete') }}',
                    className: 'btn-success'
                },
                cancel: {
                    label: '{{ trans('text.cancel') }}',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {

                    // If OK, delete the content
                    $.post({
                        url: '/lectures/content/delete/' + content_id,
                            data: {
                                '_token': token,
                            },
                            success: function(data, status) {
                                lecture_content_load(lecture_id);
                            },
                            error: function(xhr, desc, err) {
                                console.log(xhr);
                                console.log("Details: " + desc + "\nError:" + err);
                            }
                    });
                }
            }
        });
    });


</script>


@endsection