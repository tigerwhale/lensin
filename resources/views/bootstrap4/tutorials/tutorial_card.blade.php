<script type="text/javascript">
    var lecture_files_{!! $tutorial['id'] !!} = [];
</script>

<div class="col-6 col-sm-4 col-md-4 col-lg-3">
    <!-- lecture card -->
    <div class="card border mb-3 lecture_courseware_card resource-card">
        <!-- lecture image -->
        <div class="card-img-top datatable-image-grid" style="background-image: url('{!! $tutorial->getImage() !!}')">
        </div>
        <!-- lecture resources -->
        <div class="tutorial-courseware-card-resources">
            @if(!$tutorial->orderedPublishedResources->count() > 0)
                <div class="pt-5 text-white">
                    {{ trans('resources.no_resources') }}
                </div>
            @else
                <div class="row px-3 pt-5">
                    @foreach ($tutorial->orderedPublishedResources as $resource)
                        <div class="col px-1">
                            <!-- check if user logged in -->
                            @if ($user_id)
                                <div class="round_button bck-course modal_url lens mx-auto" data-user-id="{!! $user_id !!}" data-url="{!! url('/api/resources/tutorialModal/' . $resource->id . '/' . $tutorial->id) !!}" data-data="" title="{!! trans('resource_types.' . $resource->resource_type->name ) !!}">
                                    {!! $resource->resource_type->icon !!}
                                </div>
                            @else
                                <div class="round_button bck-course lens on-click-show-toast" title="{!! trans('resource_types.' . $resource->resource_type->name) !!}. {{ trans('resources.you_have_to_login_to_download_this_resource') }}" style="margin-left: auto; margin-right: auto;" >
                                    {!! $resource->resource_type->icon !!}
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        <!-- tutorial title-->
        <div class="card-text px-2 pt-2">
            @if (!$user_id)
                {{ $tutorial->title }}
            @else
                <a href="{!! url('/tutorial/' . $tutorial->id ) !!}">{{ $tutorial->title }}</a>
            @endif
        </div>
        <!-- tutorial footer -->
        <div class="position-absolute pb-2 px-2 fixed-bottom text-center">
            {{ $tutorial->year() }}
            <div class="bck-tutorial-img btn-30 border float-left"></div>
            <div class="btn-20 float-right border mt-1 bck-lens" style="background-image: url({!! url("/images/server/logo.png") !!}"></div>
        </div>
    </div>
    <!-- end tutorial card -->
</div>
