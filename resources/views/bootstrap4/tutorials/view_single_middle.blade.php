<!-- content header -->
<ul class="nav nav-tabs mb-3" id="courseTabHeader" role="tablist">
    @include ('bootstrap4.resources.resources_back_button', ['search' => ''])
    <h3 class="ml-auto mr-auto">{{ trans('text.tutorial') }}</h3>
    {!! view_counter_icons($tutorial) !!}
</ul>

<!-- title -->
<div class="row mb-2">
    <div class="col-12">
        <table class="mt-1">
            <tr>
                <td style="padding-right: 10px;">
                    <div class="resource-badge-30 badge-tutorial float-left">&nbsp;</div>
                </td>
                <td>
                    <div class="resource-title text-tutorial">{{ $tutorial->title }}</div>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- end title -->


<!-- info -->
<div class="row">

    <!-- left info -->
    <div class="col-12 col-md-4">

        <!-- image -->
        {!! coverImage($tutorial, "tutorial_big.png") !!}

    <!-- Resource view links -->
    @if ($tutorial->resources_published)
        <div class="row mb-3">
            <div class="col col-md-6 col-lg-4 resource-info-title">
                {{ strtoupper(trans('text.resources')) }}
            </div>
            <div class="col-12 col-md-6 col-lg-8">
                @foreach ($tutorial->resources_published as $resource)
                    @if ($resource->published)
                        @if($user)
                            <div class="btn-20 float-left float-md-right modal_url ml-1 bck-lens text-light pt-1"
                                 style="font-family: lens; font-size:7pt; cursor: pointer;"
                                 data-user-id={!! $user->id !!}
                                         data-url="{!! url( '/api/resources/tutorialModal/' . $resource->id . '/' . $tutorial->id ) !!}
                                         title="{{ $tutorial->title }}"
                            ">
                            {!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
                            </div>
                        @else
                            <div class="btn-20 float-left float-md-right ml-1 bck-lens text-light pt-1"
                                 style="font-family: lens; font-size:7pt; cursor: pointer;"
                                 title="{{ trans('text.you_have_to_login_to_download_this_resource') }}">
                                {!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    @endif
    <!-- END Resource view links -->

    <!-- language -->
    @if ($tutorial->languageName)
        @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.language')), 'text' => $tutorial->languageName] )
    @endif

    <!-- author -->
    @if (isset($tutorial->author) )
        @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.author')), 'text' => $tutorial->author] )
    @endif

<!-- author -->
    @if (isset($tutorial->institution) )
        @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.institution')), 'text' => $tutorial->institution] )
    @endif

    </div>

    <!-- right info -->
    <div class="col-12 col-md-8">
        <!-- description -->
        @if (isset($tutorial->description) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.description')), 'text' => $tutorial->description] )
        @endif
    </div>

</div>