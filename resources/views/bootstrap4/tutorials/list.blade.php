<?php
    $previousCategory = "";
    $user_id = Auth::user();
    ?>

@if ($tutorials->count())
    @foreach ($tutorials as $tutorial)
        <!-- CATEGORY -->
        @if ($previousCategory != $tutorial->category)

            @if (!$loop->first)
                            <!--  END PREVIOUS CATEGORY IF STARTED -->
                                </div>
                            </div>
                        </div>
                    <!-- END CATEGORY -->
                    </div>
                </div>
            @endif

            <?php $previousCategory = $tutorial->category; ?>

                <div class="accordion mb-4" id="accordion{!! $loop->index !!}"  style="width:100%">
                    <div class="card border">
                        <!-- subject heading -->
                        <div class="card-header" id="subjectHeading{!! $loop->index !!}" data-toggle="collapse" data-target="#collapse{!! $loop->index !!}" aria-expanded="true" aria-controls="collapse{!! $loop->index !!}">
                            {{ $tutorial->category }}
                        </div>

                        <!-- TUTORIALS -->
                        <div id="collapse{!! $loop->index !!}" class="collapse show" aria-labelledby="subjectHeading{!! $loop->index !!}" data-parent="#accordion{!! $loop->index !!}">
                            <div class="card-body">
                                <div class="row">
        @endif
                                <!-- TUTORIAL -->
                                @include('bootstrap4.tutorials.tutorial_card', ['tutorial' => $tutorial, 'user_id' => $user_id])

                @if ($loop->last)
                    <!--  END CATEGORY IF LAST -->
                                    </div>
                                </div>
                            </div>
                            <!-- END CATEGORY -->
                        </div>
                    </div>
                @endif
    @endforeach

@else
    <div class="text-center">
        {!! __('No Tutorials yet.') !!}
    </div>
@endif