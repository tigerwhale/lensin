@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">

    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

	<div class="container mt-3">
        <!-- TITLE -->
		<div class="row">
            <div class="col-12 text-center mb-3">
                <h2>{{ trans('text.network') }}</h2>
            </div>
        </div>
		<!-- CONTENT -->
	    <div class="row">
	        <div class="col-12 text-center mt-3">
	            <!-- TAB MENU -->
	            <ul class="nav nav-tabs nav-fill lens-nav">
	                <li class="nav-item" id="partners_tab" >
	                    <a class="nav-link active"  data-toggle="tab" href="#partnersTab">
	                        {{ trans('network_page.network_partners') }}
	                    </a>
	                </li>
	                <li class="nav-item" id="participants_tab">
	                    <a class="nav-link" data-toggle="tab"  href="#participantsTab">
	                        {{ trans('network_page.participants') }}
	                    </a>
	                </li>
	                <li class="nav-item" id="members_tab">
	                    <a class="nav-link"  data-toggle="tab" href="#membersTab">
	                        {{ trans('network_page.members') }}
	                    </a>
	                </li>
	            </ul>

	            <div class="tab-content text-left">
	                <!-- PARTNERS -->
	                <div id="partnersTab" class="tab-pane active fade show">
						@if (!Auth::user())
							<div class="my-3">
								<a href="/register">{{ trans('network_page.register_as_partner_text') }}</a>
							</div>
	                	@endif	                	
	                	<div id="partners">
	                		<i class="fa fa-spinner fa-pulse fa-2x fa-fw mt-4"></i>
	                	</div>
	                </div>
	                <!-- END PARTNERS -->

	                <!-- PARTICIPANTS -->
	                <div id="participantsTab" class="tab-pane fade">
	                	@if (!Auth::user())
	                		<div class="my-3">
	                			<a href="/register">{{ trans('network_page.register_as_participant_text') }}</a>
	                		</div>
	                	@endif
	                	<table class="display row-links mt-2" id="datatable_participants"  style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{ trans('text.platform') }}</th>
                                    <th>{{ trans('text.name') }}</th>
                                    <th>{{ trans('text.last_name') }}</th>
                                    <th>{{ trans('text.type') }}</th>
                                    <th>{{ trans('text.country') }}</th>
                                    <th>{{ trans('text.institution') }}</th>
                                </tr>
                            </thead>
                        </table>
	                </div>
	                <!-- END PARTICIPANTS -->

	                <!-- MEMBERS -->
	                <div id="membersTab" class="tab-pane fade">
						@if (!Auth::user())
							<div class="my-3">
	                			<a href="/register">{{ trans('network_page.register_as_member_text') }}</a>
	                		</div>
	                	@endif
	                	<div id="members">             	
	                		<i class="fa fa-spinner fa-pulse fa-2x fa-fw mt-4"></i>
	                	</div>
	                </div>
	                <!-- END MEMBERS -->


	            </div>
            </div>
        </div>



	<script type="text/javascript">
		$(document).ready(function(){
	    
	        var datatable_participants = $('#datatable_participants').DataTable({ 
	            "processing": true, 
	            "stateSave": true,  
	            "dom": '<"top"lf>rt<"bottom"ip><"clear">', 
	            "ajax": { 
	                "cache":true, 
	                "url" : '{!! $central_server_address !!}/api/data_all_servers', 
	                "rowId": 'id', 
	                "data": { 
	                    "url":"/api/all_users" 
	                }, 
	            }, 
	             
	            "columns": [ 
	                { "data": "server_name" }, 
	                { "data": "name" }, 
	                { "data": "last_name" }, 
	                { "data": "type" }, 
	                { "data": "country" }, 
	                { "data": "school" }, 
	            ], 
	        }); 
	 
	        var data = {page: 'partners'};
    		populateDivFromApiCall("{!! $central_server_address !!}/api/all_platforms_view",
    			"#partners", 
    			"GET",
    			data);
    		
    		var data = {page: 'members'};
    		populateDivFromApiCall("{!! $central_server_address !!}/api/all_platforms_view",
    			"#members", 
    			"GET",
    			data);
	    });

	</script>
@endsection
