@if ($members->count() > 0)
	@foreach($members as $member)
		<div class="row member-list">
			<div class="col-12 col-md-3 col-lg-2">
				@if($member->image)
					<img src="{!! url($member->image) !!}" class="image" style="max-width:250px; max-height:250px;">
				@else 
					<div class="member-default-icon">
						<i class="fa fa-users fa-5x" aria-hidden="true"></i>
					</div>
				@endif
			</div>
			<div class="col-12 col-md-9 col-lg-10">
				<!-- Title -->
				<h4>{{ $member->title }}</h4>
				<!-- Address -->
				{{ $member->country->name }}, {{ $member->address }}
				<br>
				<!-- Description -->
				@if($member->description)
					{{ $member->description }}
					<br>
				@endif
				<!-- Website -->
				@if($member->website)
					<a href="{{ $member->website }}">{{ $member->website }}</a><br>
				@endif
				<!-- Email -->
				@if($member->email)
					<a href="{{ $member->email }}">{{ $member->email }}</a><br>
				@endif
				@if($member->reference_person)
					{{ trans('text.reference_person') }}: {{ $member->reference_person }}, <a href="{{ $member->reference_person_email }}">{{ $member->reference_person_email }}</a>
				@endif

				@if ($member->members)
					<h5>{{ trans('text.participants') }}</h5>
					@foreach($member->members as $user)
						{{ $user->fullName() }} {{ $user->email }}
						<br>
					@endforeach
				@endif
			</div>
		</div>
		<hr>
	@endforeach 

@else 
	@if (isset($partners))
		{{ trans('users.no_partners') }}
	@else
		{{ trans('users.no_members') }}
	@endif
@endif