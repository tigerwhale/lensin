<?php 
    $memberGroups = \App\MembersGroup::where('published', 1)->where('partner', 1)->get()->pluck('title', 'id');
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
?>

<!-- MEMEBER GROUP LIST-->
    @if (count($user->members_groups))
        <br>
        <div class="panel-group">
            <div class="panel panel-default">
                <a data-toggle="collapse" href="#member_group_list" class="" aria-expanded="true">
                    <div class="panel-heading"> 
                        <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                        <div class="panel-title">
                            <h4>{{ trans('users.current_partners') }}</h4>
                        </div>
                    </div>
                </a>
                <div id="member_group_list" class="panel-collapse collapse">
                    <div class="panel-body">
                        {{ trans('users.you_are_a_part_of') }}:<br>
                        @foreach ($user->members_groups as $member_group)
                            <div class="row hover padding-small" id="member_row_{!! $member_group->id !!}">
                                <div class="col-xs-10" style="padding-top: 7px;">
                                    {!! $member_group->title !!}
                                    @if(!$member_group->published)
                                        <span class="text-danger small"> - {{ trans('users.pending_administrator_approval') }}</span><br>
                                    @endif 
                                </div>
                                <div class="col-xs-2 text-right">
                                    <div class="btn btn-danger leave_member" title="{{ trans('users.leave_member') }}" data-member-id="{!! $member_group->id !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    @endif

<!-- JOIN MEMEBER GROUP -->

    <div class="panel-group">
        <div class="panel panel-default">
            <a data-toggle="collapse" href="#member_group_existing" class="" aria-expanded="true">
                <div class="panel-heading"> 
                    <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                    <div class="panel-title">
                        <h4>{{ trans('users.join_existing_partner_group') }}</h4>
                    </div>
                </div>
            </a>
            <div id="member_group_existing" class="panel-collapse collapse">
                <div class="panel-body">
                    {{ trans('users.join_member_text') }}
                    @if (count($memberGroups) > 0)

                        <form method="POST" action="/member_group/join/{!! Auth::user()->id !!}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-10">
                                    {{ Form::select('member_group_id', $memberGroups , null, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-xs-2 text-right">
                                    <button class="btn btn-success" type="submit">{{ trans('text.join') }}</button>
                                </div>
                            </div>
                        </form>

                    @else
                        {{ trans('users.no_member_groups') }}
                    @endif 
                </div>
            </div>
        </div>
    </div>

<!-- CREATE MEMEBER GROUP -->
   <div class="panel-group">
        <div class="panel panel-default">
            <a data-toggle="collapse" href="#member_group_create" class="" aria-expanded="true">
                <div class="panel-heading"> 
                    <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                    <div class="panel-title">
                        <h4>{{ trans('users.create_a_new_member') }}</h4>
                    </div>
                </div>
            </a>
            <div id="member_group_create" class="panel-collapse collapse">
                <div class="panel-body">
                    {!! trans('users.register_member_text') !!}

                    <form method="POST" action="/member_group/create" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <!-- LEFT -->
                        <div class="col-md-8">
                            <div class="row form-group platform_dropdown">
                                <i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>
                            </div>
                            <!-- Type --> 
                            {!! updateInput('text.type', Form::select('type', $memberGroupTypes , null, ['class' => 'form-control', 'id'=> 'member_type']) ) !!}
                            <!-- Name --> 
                            {!! updateInput('text.name', Form::text('title', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Description --> 
                            {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'required' => "", 'style' => 'height: 300px;']) ) !!}
                            <!-- Reference Person --> 
                            {!! updateInput('text.reference_person', Form::text('reference_person', Auth::user()->fullName(), ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Reference Person Email --> 
                            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', Auth::user()->email, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Country --> 
                            {!! updateInput('text.country', Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'id'=> 'country_id', 'placeholder' => trans('text.select_country')]) ) !!}
                            <!-- Address --> 
                            {!! updateInput('text.address', Form::text('address', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Website --> 
                            {!! updateInput('text.web_site', Form::text('web_site', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Email --> 
                            {!! updateInput('text.email', Form::email('email', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Submit button --> 
                            <button class="btn btn-success" id="create_member_group">{{ trans('text.register') }}</button>
                        </div>

                        <!-- RIGHT -->
                        <div class="col-md-4">
                            <div class="row top-padding-small">
                                {{ trans('users.members_logo') }}
                            </div>
                            <div class="row">
                                <label for="member_cover_image" class="dropzone_div" id="member_cover_image_label">{{ trans('text.click_to_select_image') }}</label>
                                <input type="file" name="image" id="member_cover_image" style="display: none">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
