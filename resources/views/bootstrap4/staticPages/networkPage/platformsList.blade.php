@foreach($platforms as $platform)

	<div class="accordion mt-3" id="platform_{!! $request->page !!}_accrdion">
	    <div class="card mb-3">

	    	<!-- PLATFORM TITLE -->
	        <div class="card-header collapsed"
				 data-toggle="collapse"
				 data-target="#platform_{!! $request->page !!}_tab_{!! $platform->id !!}"
				 aria-expanded="true"
				 aria-controls="platform_{!! $request->page !!}_tab_{!! $platform->id !!}"
				 style="cursor: pointer">
	            <h6 class="mb-0 float-left">
					<div class="text-left colapse_arrow">
						{{ $platform->name }}
					</div>
	            </h6>
	        </div>
	        <!-- END PLATFORM TITLE -->

	        <!-- PLATFORM CONTENT -->
	        <div id="platform_{!! $request->page !!}_tab_{!! $platform->id !!}" 
	        	class="collapse" 
	        	aria-labelledby="platform_{!! $request->page !!}_card_{!! $platform->id !!}" 
	        	data-parent="#platform_{!! $request->page !!}_accrdion">

	            <div class="card-body" id="partners_{!! $request->page !!}_{{ $platform->id }}">
               		<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
	            </div>
	        </div>
	        <!-- END PLATFORM CONTENT -->
	    </div>
	</div>

	<script>
		var data = {token : $("meta[name='csrf-token']").attr("content") }; 
   		populateDivFromApiCall(
   			"{{ $platform->address }}/api/{!! $request->page !!}_list",
   			"#partners_{!! $request->page !!}_{{ $platform->id }}", 
   			"GET",
   			data);
	</script>
@endforeach

