@extends('bootstrap4.layouts.frontend')

@section('head')
    <script src="/js/jquery.cookie.min.js"></script>
@endsection

@section('content')
   
	<div class="container mt-3">
        @if ($page)
            <!-- TITLE -->
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 text-center">
                    <h2 class="text-center">
                        {{ page_title($page->permalink) }}
                    </h2>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="row mt-3">
                <div class="col-12 col-md-8 offset-md-2">
                    {!! page_text($page->permalink) !!}
                </div>
            </div>
        @else
            <!-- TITLE -->
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 text-center mt-5">
                    <h2 class="text-center">
                        NO SUCH PAGE :(
                    </h2>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="row mt-5">
                <div class="col-12 col-md-8 offset-md-2 text-center">
                    <strong>OOPS... THE PAGE YOU ARE LOOKING FOR DOES NOT EXIST.</strong>
                    <br><br>
                    Either something went wrong, or this page does not exist. 
                    <br>
                    Please try to refresh, or go to <a href="/">LeNS home page</a>.
                    <br><br><br><br><br>
                </div>
            </div>
        @endif
    </div>

@endsection
