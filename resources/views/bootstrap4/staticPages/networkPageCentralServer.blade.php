@extends('bootstrap4.layouts.frontend')

@section('content')
    
	<div class="container mt-3">
        <!-- TITLE -->
		<div class="row">
            <div class="col-12 text-center mb-3">
                <h2>{{ trans('text.network') }}</h2>
            </div>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                    
                <!-- NETWORKS LIST -->
                @foreach ($networks as $network)
                    <div class="accordion" id="networkList_{{ $network->id }}">

                        <div class="card mb-3">
                            <div class="card-header collapsed" data-toggle="collapse"
                                 data-target="#network_panel_collapse_{{ $network->id }}"
                                 aria-expanded="false"
                                 aria-controls="network_panel_collapse_{{ $network->id }}"
                                 id="network_panel_heading_{{ $network->id }}">
                                <h6 class="mb-0 float-left">
                                    <div class="text-left colapse_arrow"
                                         >
                                        {{ $network->name }}
                                    </div>
                                </h6>

                            </div>

                            <div id="network_panel_collapse_{{ $network->id }}"
                                 class="collapse"
                                 aria-labelledby="network_panel_heading_{{ $network->id }}"
                                 data-parent="#networkList_{{ $network->id }}">

                                <div class="card-body">
                                    @if ($network->description)
                                        {{ $network->description }}
                                        <br><br>
                                    @endif

                                    <!-- MEMBERS LIST -->
                                    @if ($network->top_members->count())
                                        <strong>{{ strtoupper(trans('text.members_and_contacts')) }}</strong>
                                        <br><br>
                                        @foreach ($network->top_members as $member)
                                            <strong>{{ $member->name }}</strong>
                                            <br>
                                            @if ($member->contacts->count())
                                                @foreach($member->contacts as $contact)
                                                    {{ $contact->name }} | {{ Html::mailto($contact->email) }} <br>
                                                @endforeach
                                            @endif
                                            <br>
                                        @endforeach
                                    @endif 

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        <!-- END CONTENT -->

    </div>
@endsection
