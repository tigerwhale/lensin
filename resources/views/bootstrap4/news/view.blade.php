@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/left_menu_filter.css" rel="stylesheet">
    <link href="/css/lens/right_menu_filter.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container">
        <div class="row">

            <!-- MENU LEFT -->
            <div class="col-2 d-md-block d-none" style="min-height: 600px;">
                @include('bootstrap4.layouts.left_menu_filter', ['home_page' => false, 'page' => 'news'])
            </div>

            <!-- MIDDLE CONTENT -->
            <div class="col-12 col-md-8" style="margin-top:20px;" id="article">

                <!-- content header -->
                <ul class="nav nav-tabs mb-3" id="lectureTabHeader" role="tablist">
                    <button class="btn bck-lens pull-left back-button rounded-circle text-white"
                            title="{!! trans('text.back') !!}"
                            data-link="/">
                        <i class="fas fa-arrow-left" aria-hidden="true"></i>
                    </button>

                    <h3 class="ml-auto mr-auto">
                        {{ trans('text.news') }}
                    </h3>
                </ul>

                <!-- title -->
                <div class="row mb-2">
                    <div class="col-12">
                        <table class="mt-1">
                            <tr>
                                <td style="padding-right: 10px;">
                                    <div class="resource-badge-30 badge-news float-left">&nbsp;</div>
                                </td>
                                <td>
                                    <div class="resource-title text-news">{{ $news->title }}</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- end title -->


                <!-- info -->
                <div class="row">

                    <!-- COVER IMAGE -->
                    <div class="col-12">
                        @if (isset($news->image))
                            <img src="{{ $news->image }}" class="image my-3">
                        @endif
                    </div>
                    <div class="col-12">
                        {{ $news->text }}
                    </div>
                </div>


            </div>

            <!-- MENU RIGHT -->
            <div class="col-2 d-md-block d-none text-center" id="right_menu_filter">
                @include('bootstrap4.layouts.right_menu_filter', ['home_page' => false, 'languages' => $languages])
            </div>

        </div>
    </div>

@endsection
