<script>
    function showIntroduction() 
    {
        if ($.cookie('hide-introduction') == "false") 
        {
            $("#introduction").show();
            $("#show-introduction").hide();
        } else {
            $("#introduction").hide();
            $("#show-introduction").show();
        }
    }

    $(document).ready(function() {
       
        showIntroduction();

        $("#hide-introduction-btn").click(function() {
            $("#introduction").hide();
            $("#show-introduction").show();
            $.cookie('hide-introduction', true);
        });

        $("#show-introduction-btn").click(function() {
            $("#introduction").show();
            $("#show-introduction").hide();
            $.cookie('hide-introduction', false);
        });
    });
</script>
