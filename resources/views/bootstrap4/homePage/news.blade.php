<!-- NEWS -->
<div class="row mb-4" id="news">
    <div class="col-12 text-center">
        <h2>{{ trans('frontend_home_page.news') }}</h2> 
        <hr class="margin-small">
    </div>

    <div class="col-12 text-center" id="news_content">
        <div id="newsCarousel" class="carousel slide" data-ride="carousel">
            <i id="newsSpinner" class="my-4 fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <ol class="carousel-indicators"></ol>
            <div class="carousel-inner">
            </div>
            <a class="carousel-control-prev" href="#newsCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">{{ trans('frontend_home_page.news_previous') }}</span>
            </a>
            <a class="carousel-control-next" href="#newsCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">{{ trans('frontend_home_page.next') }}</span>
            </a>
        </div>        
    </div>
</div>

<!-- SHOW INTRODUCTION -->
<div class="row" style="display:none" id="show-introduction">
    <div class="col-12 text-center">
        <a href="#" id="show-introduction-btn">
            <em>&times;</em> {{ trans('frontend_home_page.show_introduction') }}
        </a>
    </div>
</div>

@include('bootstrap4.homePage.news_js')
