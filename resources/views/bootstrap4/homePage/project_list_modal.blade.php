<div class="modal" tabindex="-1" role="dialog" id="apiModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title js-modal-title">
					{{ trans('users.my_projects') }}
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				@foreach($user->challenge_groups as $group)
					<div class="row hover">
						<div class="col-12">
							<a href="/projects/view/{!! $group->id !!}">{{ $group->name }}</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>

<script>
	$('#apiModal').on('hide.bs.modal', function () {
		console.log('closed');
		$(this).remove();
	});
</script>