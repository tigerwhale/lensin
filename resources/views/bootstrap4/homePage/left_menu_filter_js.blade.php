<script>

	$(function() {
		$('.resources').click(function() {
   			window.location.href = "/search?" + filterOptionsSelectedForGetRequest();
        });

		$('.resource-mobile-switch').click(function() {
			var resourceName = $(this).data('resource');
			console.log(resourceName);
			$('#' + resourceName + '_switch').click();
		});
    });

</script>