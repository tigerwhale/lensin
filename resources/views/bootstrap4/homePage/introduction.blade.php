<!-- VIDEO and INFO -->
<div class="row mb-4" style="display:none" id="introduction">
    <div class="col-12 text-center">
        <!-- HOME TEXT -->
        <h2>{{ page_title('home') }}</h2>
        {!! page_text('home') !!}
        
        <!-- VIDEO -->
        <div class="w-100 text-center">
            {!! homePageVideo() !!}
        </div>

        <!-- HIDE INTRODUCTION -->
        <a href="#" id="hide-introduction-btn">
            <em class="close-button">&times;</em> {{ trans('frontend_home_page.hide_introduction_video_and_text') }}
        </a>
    </div>
</div>

@include('bootstrap4.homePage.introduction_js')