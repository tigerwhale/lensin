<script type="text/javascript">
    var forbiddenNews = [{!! $forbiddenNews !!}];
    $(document).ready(function() {
        dataFromAllServers("/api/news/latest", 10, populateNews);
    });

    var populateNews = function(news)
    {
        var isThereNews = false;
        var forbiddenNews = [{!! $forbiddenNews !!}];
        var displayedNewsCounter = 0;

        $.each(news, function(i, newsData)
        {
            var newsIdServer = newsData.id + "-" + newsData.platform_id;


            if (forbiddenNews.indexOf(newsIdServer) == -1) {

                if (newsData.title || newsData.text) 
                {
                    // INDICATOR
                    var indicator = document.createElement('li')
                    indicator.setAttribute("data-target", "#newsCarousel");
                    indicator.setAttribute("data-slide-to", displayedNewsCounter);
                    displayedNewsCounter += 1;

                    if (!isThereNews)
                    {
                        indicator.classList.add("active");
                    }
                    $("#newsCarousel .carousel-indicators").append(indicator);

                    // CONTENT
                    var newsContent = document.createElement('div');
                    newsContent.classList.add("carousel-item");
                    if (!isThereNews)
                    {
                        newsContent.classList.add("active");
                    }

                    // IMAGE
                    var newsContentImage = document.createElement('img');
                    if (newsData.image) {
                        newsContentImage.src = newsData.image;
                    } else {
                        newsContentImage.src = '/images/resource/news_big.png';
                    }
                    newsContentImage.classList.add('d-block');
                    newsContentImage.classList.add('w-100');
                    $(newsContent).append(newsContentImage);

                    // TEXT BLOCK
                    var newsContentText = document.createElement('div');
                        newsContentText.classList.add("carousel-caption");
                        newsContentText.classList.add("bck-news");
                        newsContentText.classList.add("px-3");
                        //newsContentText.classList.add("d-none");
                        //newsContentText.classList.add("d-md-block");
                        
                        // LINK
                        var newsContentLink = document.createElement('a');
                            newsContentLink.href = newsData.news_link;
                            newsContentLink.classList.add('news-link');
                        $(newsContentText).append(newsContentLink);
                            
                            // TITLE AND TEXT
                            newsData.text = truncate(newsData.text, 200);
                            $(newsContentLink).append("<h5>" + newsData.title + "</h5>");
                            $(newsContentLink).append("<p>" + newsData.text + "</p>");

                    $(newsContent).append(newsContentText);

                    $("#newsCarousel .carousel-inner").append(newsContent);
                    isThereNews = true;
                }

            }
        });

        $("#newsSpinner").hide();

        if (!isThereNews) 
        {
            $("#newsCarousel").append("<h5>{{ trans('frontend_home_page.no_news') }}</h5>");
            console.log('no news');
        }
    }

</script>
