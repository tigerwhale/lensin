<?php
    App::setLocale(Session::get('locale'));
    if (!isset($page)) {
        $page = "";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{!! server_property('platform_title') !!}</title>

        <!-- JS -->
        <script src="/js/jquery-331/jquery-3.3.1.min.js"></script>
        <script src="/js/bootstrap4/popper.min.js"></script>
        <script src="/js/bootstrap4/lens.js"></script>
        <script src="/js/bootstrap4/bootstrap.min.js"></script>
        <script src="/js/toastr.min.js"></script>
        <script src="/js/bootbox.min.js"></script>
        <script src="/fontawesome/js/all.min.js"></script>


        <!-- CSS -->
        <link href="/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
        <!-- Font awsome -->
        <link rel="stylesheet" type="text/css" href="/fontawesome/css/all.min.css"/>
{{--        <link rel="stylesheet" type="text/css" href="/fontawsome/css/font-awesome.min.css"/>--}}

        <link href="/css/toastr.min.css" rel="stylesheet">
        <link href="/css/lens/lens-fonts.css" rel="stylesheet">
        <link href="/css/lens/left_menu_filter.css" rel="stylesheet">
        <link href="/css/lens/right_menu_filter.css" rel="stylesheet">
        <link href="/css/lens/lens.css" rel="stylesheet">
        <!-- Ubuntu font --> 
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,500,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">

        <!-- ADITIONAL HEAD -->
        @yield('head')
    </head>

    <body>
        <!-- MODAL -->
        <div id="modalDiv"></div>

        <!-- HEADER -->
        @include('bootstrap4.layouts.frontend_header')

        <!-- CONTENT -->
        <article>
            @yield('content')
        </article>
        
        <!-- FOOTER --> 
        @include('bootstrap4.layouts.frontend_footer')

    </body>

    @include('bootstrap4.layouts.frontend_js')
</html>