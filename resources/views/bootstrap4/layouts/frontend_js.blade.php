<script>
    /** DEFINE VARIABLES **/
    var locale = "{!! Session::get('locale') !!}";

    var token = $("meta[name='csrf-token']").attr("content");    
    var central_server = {!! server_property('central_server') ? 1 : 0 !!};
    var central_server_url = "{!! server_property('central_server_address') !!}";
    var server_id = {!! server_property('server_id') ? server_property('server_id') : 0 !!};
    var page = "{!! $page !!}";
    var login_send = "";



    @if (Auth::user()) 
      login_send = "&token={!! Auth::user()->login_token !!}&email={!! Auth::user()->email !!}&server_id={!! server_property('server_id') ?: 0 !!}"; 
    @endif 


    async function dataFromMultipleServers(urls, method = "GET", return_function = "", jsonLength) {
        var return_json = [];
        try {
            var data = await Promise.all(
                urls.map(
                    url =>
                        fetch(url, {method: method})
                        .then((response) => response.json())
                        .then( function (data) {
                            return_json = checkJsonLength(return_json, data, jsonLength, url);
                        })                            
                        .catch(function(err) {
                            console.log('Fetch Error :-S ' + url, err);
                        })
                )
            );

            if (return_function) {
                return_function(return_json);
            } else {
                return return_json;
            }
        } catch (error) {
            console.log(error);
            return return_json;
        }
        
    }

    
    function checkJsonLength(return_json, data, jsonLength, url) 
    {
        var isJsonOk = true;
 
        if (jsonLength != "") 
        {
            jQuery.each(data, function(){ 
                if (Object.keys(this).length != jsonLength) {
                    console.log("JSON Error on " + url + ". Lenght: " + Object.keys(this).length);
                    isJsonOk = false;
                    return false;
                }
            });
        }

        if (isJsonOk) {
            return_json = return_json.concat(data);
        }

        return return_json;
    }



    function dataFromAllServers(url_path, jsonLength, return_function, method = "GET") {
        var allServersJson = {!! json_encode(array_of_all_servers()) !!};
        var allServersArray = [];
        allServersJson.map(
            function (serverUrl) {
                allServersArray.push(serverUrl.address + url_path);
            }
        );

        data = dataFromMultipleServers(allServersArray, method, return_function, jsonLength);
        return data;
    }


    function createElement(type = "div", classes = "", attributes = [])
    {
        var div = document.createElement(type);
        
        if (classes != "") {
            classes = classes.split(" ");
            classes.forEach(element => {
                div.classList.add(element);
            });
        }
        
        if (attributes) {
            for (var attribute in attributes){
                div.setAttribute(attribute, attributes[attribute]);
            }
        }
        return div;
    }

    function truncate(truncateString, n){
        if (truncateString.length <= n) { return truncateString; }
        var subString = truncateString.substr(0, n-1);
        return ( subString.substr(0, subString.lastIndexOf(' ')) + "&hellip;" );
    };



    function apiCall(url, return_function, method = "GET", data = []) {
        data._token = $("meta[name='csrf-token']").attr("content"); 
        // Ajax Request
        $.ajax({
            url: url,
            method: method,
            data: data,
        }).done(function(returnData) {
            return_function(returnData);
        }).fail(function(xhr, desc, err) {
            console.log("Details: " + desc + "\nError:" + err);
        });
    }


    function populateDivFromApiCall(url, divClass, method="GET", data = [], callbackFunction) {
        data['_token'] = $("meta[name='csrf-token']").attr("content");
        var updateDiv = function(returnData) {
            $(divClass).html(returnData);
            if (callbackFunction) {
                callbackFunction();    
            }            
        }
        
        apiCall(url, updateDiv, method, data);
    }


    $(document).ready(function() {

        $('.tooltip_text').tooltip();

        /* ----- TOASTS ----- */
        toastr.options.closeButton = true;
        toastr.options.timeOut = 100000000;
        toastr.options.closeDuration = 100;


        $(document).on('click', '#toast-container .toast', function() {
            $(this).fadeOut(function(){
                $(this).remove();
            });
        });    

        @foreach (session('flash_notification', collect())->toArray() as $message)
            toastr.success('{{ $message['message'] }}');
        @endforeach

        @if ( Auth::user() )
            @if ( Auth::user()->hasRole('manageusers') )
                @if ( \App\User::where('email', 'LIKE', 'administrator@lens-international.org')->first() )
                    toastr.warning('{{ trans('text.please_remove_default_user') }}');
                @endif
            @endif
        @endif

    });


</script>
