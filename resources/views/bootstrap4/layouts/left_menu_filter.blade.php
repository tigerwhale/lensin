<div class="left_filter_menu" style="margin-top: 26px; padding-top: 21px;">

	<!-- RESOURCE SWITCHES -->
	<h4 class="resources_title">{{ strtoupper(trans('left_menu_filter.resources')) }}</h4>
	<h4 class="resources_title d-none">{{ strtoupper(trans('left_menu_filter.edit')) }}</h4>

	@if (Auth::check() && Auth::user()->isOne('managecourses|managestudycases|managetools|manageprojects|managenews'))
		<!-- manage button -->
		<div class="pb-4">
			@include('bootstrap4.layouts.left_menu_filter_manage_button')	
		</div>
	@endif
	
	<div class="edit_resource_buttons">

	<?php
        $switches = ['courses', 'lectures', 'tools', 'study_cases', 'projects'];
        $switchesChecked = (isset($_GET['switch']) ? $_GET['switch'] : []);
        $active_switch = (isset($active_switch)) ? $active_switch : "";
        
        foreach ($switches as $switch) {
            $checked = "";
            
            // Test is the switch checked
            if (($page == "search" && $switchesChecked && in_array(substr($switch, 0, -1), $switchesChecked)) || $active_switch == $switch) {
                $checked = "checked";
            } ?>

			<label class="switch type mb-1">
				<input type="checkbox" id="{!! $switch !!}_switch" class="resources type datatableFilter" value="{!! ($switch != 'news') ? substr($switch, 0, -1) : 'news' !!}" {!! $checked !!}>
				<div class="slider round {!! $switch !!}_slider"></div>
			</label>
			<div class="resource_name" id="{!! $switch !!}_title">{{ strtoupper(trans('left_menu_filter.' . $switch)) }}</div>
	<?php
        }
    ?>

	</div>
</div>

@if ($page == "search")
	@include("bootstrap4.searchPage.left_menu_filter_js")
@else 
	@include("bootstrap4.homePage.left_menu_filter_js")
@endif