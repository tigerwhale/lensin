<script>
    $(document).ready(function()
    {
        $("#header-advanced-search").click(function() {
            $("#advanced-research").toggle("fast");
            changeAdvancedResearchCaret();
            resourceSwitchesEnableAllIfDisabled();
            $("#search-box-header").toggleClass('advanced-research-background');
            advancedResearchSelectedResourcesDisablesFilters();
        });

        $("#multiselectRemoveAllFilters").click(function () {
            deselectAllAdvancedResearchFilters();
        });

        $('body').on('click', '.deselect-column', function()
        {
            let columnName = $(this).closest('div').prev().data('column');
            let columnId = $(this).closest('div').prev().data('column-id');
            
            $("#" + columnName + "_filter").multiselect('clearSelection');

            datatableTable.columns([columnId]).search("", true, false);
            datatableTable.draw();
            advancedResearchSelectedResourcesDisablesFilters();

            event.preventDefault();
            event.stopPropagation();

        });
        

    });

    function deselectAllAdvancedResearchFilters() 
    {
        var fieldIds = [];
        $(advancedResearchFields).each(function(key, field) 
        {
            let filedObject = $("#" + field + "_filter");
            if (filedObject.data('column-id') != null) {
                fieldIds.push( filedObject.data('column-id') );    
            }
            filedObject.multiselect('clearSelection');
        });

        datatableTable.columns(fieldIds).search("", true, false);
        datatableTable.draw();
        advancedResearchSelectedResourcesDisablesFilters();
    }
    

    /**
     * Change filter selection according to URI parameters
     */
    function initFiltersFromUrl() 
    {
        selectedOptions =  getParams();
        $(advancedResearchFields).each(function(key, field) {
            if ( selectedOptions[field] ) {
                $(selectedOptions[field]).each(function(key, value) {
                    $('.' + field + ' :checkbox[value="' + value + '"]').click();
                });
            }
        });
    }

    /**
     * Change Advanced Research Caret rotation
     */
    function changeAdvancedResearchCaret() 
    {
        $('#advanced-research-caret').toggleClass('fa-caret-down');
        $('#advanced-research-caret').toggleClass('fa-caret-up');
    }

	/**
	 *	Enable Resourse Switches if all are disabled
	 */
	function resourceSwitchesEnableAllIfDisabled() 
	{
		var checkedSwitches = document.querySelectorAll('input.resources:checked');

		if (checkedSwitches.length === 0) 
		{
			var allResourceSwitches = document.querySelectorAll('input.resources');
			for( var i=0; allResourceSwitches.length > i; i++)
			{
				document.querySelectorAll('input.resources')[i].click();
			}
		} 
	}

    $('#header_search').keyup(function(){
        datatableTable.search($(this).val()).draw() ;
    });


/*! DataTables MultiSelect integration
 * 
 */

/**
 * DataTables integration for Bootstrap 3. This requires Bootstrap 3 and
 * DataTables 1.10 or newer.
 * for further information.  
 
 */
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
	    define(['jquery', 'datatables.net', 'vendors/bootstrap-multiselect'], function ($) {
			return factory( $, window, document );
		} );
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
    'use strict';
    var DataTable = $.fn.dataTable;

    var DTMultiSelectColumnApi = function (instance, column, index, opts) {
        this.instance = instance;
        this.index = index;
        this.column = column;
    	this.options = $.extend(DTMultiSelectApi.defaults.defaultMultiSelectOptions, opts);
        this.isChanging = false;
        this.seachList = [""];
        this._constructor();
        this.search = function () {
            var val = this._getSearchText();
    		if(val == "" || val == null){
    			DTMultiSelectColumnApi.last = null;
    		}else{
    			DTMultiSelectColumnApi.last = this;
    		}
    		
            this.column.search(val, true, false).draw();
            
            return val;
        };
        this.update = function () {
            this._update();
        };
    };


    DTMultiSelectColumnApi.last = null;
    DTMultiSelectColumnApi.prototype = { 
    	
        _constructor: function () {
            var self = this; 
            this.instance.multiselect(this.options);
            this.instance.on('change', function () {
                    advancedResearchSelectedResourcesDisablesFilters();
                    this.search();
                }.bind(this) 
            );

        }, 
    	// Build a regular expresion from multiple selection in order to filter multiple rows.
        _getSearchText: function () {
            var arr = this._getSearchList();
            var val = "";
            if (arr != null) {
                
                for (var i = 0; i < arr.length; i++) {
                    arr[i] = $.fn.dataTable.util.escapeRegex(
                        arr[i]
                    );
                    arr[i] = arr[i] != "" ? "(" + arr[i] + ")" : "";
                }
                val = arr.join("|");
            } else {
            }
            return val;
        },
        _getSearchList: function(){
            return this.instance.val();
        },
        _update: function () {
            var self = this;
    		// Do not update the component if it is the last one modified.
            if(DTMultiSelectColumnApi.last == this){return;}
            
            var component = this.instance;
            component.empty();

    		// Build an array from the search text applied to the column.
    		//
            var selected = this.column.search().split("|").map(function (value) {
    			// Remove \ symbol added by escape.
                var _re_escape_regex = /\\/gi;
                value = value.replace(_re_escape_regex, '');
                return value.replace(/\((.*)\)/g, "$1");
            });

            // For each unique value we create an option for the filter component.
            var dataName = $(component).data('column');
            var result = this.column.rows({ filter: 'applied' })
                                    .data()
                                    .flatten()
                                    .map(a => a[dataName])
                                    .unique()
                                    .map(function (a) {
                                        if (typeof a === "string") {
                                            return a.split("|");
                                        } else {
                                            return a;
                                        }                                    
                                    })
                                    .flatten()
                                    .unique()
                                    .sort();

            result.each(function (value) 
            {
                if (value == null || value == "") { return ; }
                var index = selected.indexOf(value.toString());
                var sel = index != -1 ? "selected" : "";
                component.append('<option value="' + value + '" ' + sel + '>' + value + '</option>');
                if (index != -1) {
                    selected.splice(index, 1);
                }
            });

            // In case of the filtered values are not in the column data, we should add those options to able to the user unselect it.
            $.each(selected, function (i, value) {
                if (value != "") {
                    component.append('<option value="' + value + '" selected>' + value + '</option>');
                }

            });

            component.multiselect('rebuild');
        }
    };

    var DTMultiSelectApi = function (settings, opts) {
        // Sanity check that we are using DataTables 1.10 or newer
        if (!DataTable.versionCheck || !DataTable.versionCheck('1.10.1')) {
            throw 'DataTables MutiSelect requires DataTables 1.10.1 or newer';
        }
    	this.options = $.extend(DTMultiSelectApi.defaults, opts);
        this.s = {
            dt: new $.fn.dataTable.Api(settings),
            columns: []
        };

        settings.multiSelectApi = this;
        this._constructor();
    };
    DTMultiSelectApi.prototype = {

        _constructor: function () {
            var self = this;
    		// This is the Id of the TABLE. We need to identify it.
    		self.sInstance = self.s.dt.context[0].sInstance;
    		// MultiSelect components.
            self.components = {};
    		
            self._init();
       
            this.s.dt.on('draw.dt', function (e, settings) {
                // Detect changes in datasource for redraw.
                if (self.s.recordsDisplay != settings.fnRecordsDisplay()) {
                    self._draw();
                }
                self.s.recordsDisplay = settings.fnRecordsDisplay();
            });
    		self._draw();  
        },
        _init: function () {
            var self = this;
            this.s.recordsDisplay = 0;
            this.s.dt.columns().eq(0).each(function (item, index) {
    			var selector = self._getMultiSelectOption(index, "selector");
                var dataName = self._getMultiSelectOption(index, "dataName");
                var dataColumnNumber = self._getMultiSelectOption(index, "col");
                var opts = self._getMultiSelectOption(index);
                
                var comp = selector ? $(selector) : $('#'+self.sInstance+'-quick-filter-' + index);
                $(comp).data("dataName", dataName);
                $(comp).data("dataColumnNumber", dataColumnNumber);
                if (comp.length > 0) {
                    self.components[index] = new DTMultiSelectColumnApi(comp,
                        self.s.dt.column(index, { "filter": "applied" }),
                        index, opts);
                }
            });
        },
        _draw: function (){
            this._updateComponents();
        },
        _updateComponents:function (){
            for (var key in this.components) {
    			this.components[key].update();
            }
        },
    	_getOption: function (name, def) {
    		if (this.options == null || this.options[name] == null) {
    			return def;
    		}
    		return this.options[name];
    	},
    	_getMultiSelectOption: function(index, prop){
            var multiSelectOpts = this._getOption("multiSelectOptions");
    		var ret = null;
    		if(!multiSelectOpts){return null;}
    		for(var i = 0; i<multiSelectOpts.length; i++){
                if(multiSelectOpts[i] 
    				&& multiSelectOpts[i].col != null
    				&& multiSelectOpts[i].col == index
    				){
    				ret = multiSelectOpts[i];
    			}
    		}
    		if(prop){
    			if(ret && ret[prop]){
    				return ret[prop];
    			}else{
    				return null;
    			}
            }
    		return ret;
    	},
    };


    DTMultiSelectApi.defaults = {
    	multiSelectOptions: {},
    	defaultMultiSelectOptions:{ 
            maxHeight: 400,
            enableHTML: true,
            disableIfEmpty: true,
            includeSelectAllOption: true,
    		onDropdownShow: function () {
    		}, 
    		onDropdownHidden: function () { 
            },
            buttonText: function (options, select) {
                if (options.length === 0) {
                    return $(select).attr('placeholder');
                }
                else if (options.length < 3) {
                    var labels = [];
                    options.each(function () {
                        if ($(this).attr('label') !== undefined) {
                            labels.push($(this).attr('label'));
                        }
                        else {
                            labels.push($(this).html());
                        }
                    });
                    return $(select).attr('placeholder') + ': </span><span class="multiselect-button-values-selected">' + labels.join(', ') + '';
                } else {
                    return $(select).attr('placeholder') + ': ' + '</span><span class="multiselect-button-values-selected">' + options.length + ' {{ trans('frontend_datatable.multiple_selected') }}' ;
                }
            },
    	}
    	
    }

    $(document).on('init.dt.dtr', function (e, settings, json) {
        new DTMultiSelectApi(settings, settings.oInit.multiSelect); 
    });

return DTMultiSelectApi;
}));




</script>