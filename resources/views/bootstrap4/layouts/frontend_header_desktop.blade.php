<div class="container d-md-block d-none">
    <div class="row mt-4">

        <!-- LOGO -->
        <div class="col-md-3 col-lg-2 px-3 text-center">
            <a title="{{ server_property('server_name') }}" href="/">
                <img src="{!! logo() !!}" class="header-logo" alt="{{ server_property('server_name') }}">
            </a>
            <div class="header-logo-title">{{ server_property('slogan') }}</div>
        </div>

        <!-- SEARCH -->
        <div id="search-box-header" class="col-md-5 col-lg-3 px-3">

            @if ($page == 'search')
                <input  name="header_search" 
                        type="text" 
                        id="header_search" 
                        class="form-control header-search" 
                        placeholder="{{ trans('frontend_header.what_are_you_looking_for') }}" 
                        value="{{ isset($_GET['header_search']) ? $_GET['header_search'] : '' }}"    
                    />
            @else 
                <form action="/search" method="get">
                    <input name="header_search" type="text" id="header_search" class="form-control header-search" placeholder="{{ trans('frontend_header.what_are_you_looking_for') }}" />
                </form>           
            @endif

            <div id="header-advanced-search" class="text-left">
                <a href="{!! ($page != "search") ? '/search?advanced=true' : '#' !!}" class="text-lens" >
                    {{ trans('frontend_header.advanced_research') }}
                    <i class="fa fa-caret-down" id="advanced-research-caret"></i>
                </a>
            </div>
        </div>

        <!-- MENU -->
        <div class="col-md-2 col-lg-5" style="text-align: right;">

            <!-- menu icons -->
            <div class="header-menu-icons d-none d-lg-block">
                <a href="/contact" title="{{ trans('frontend_header.contact') }}">
                    <img src="/images/header-menu/contact.png" alt="{{ trans('frontend_header.contact') }}">
                    <br>{{ strtoupper(trans('frontend_header.contact')) }}
                </a>
            </div>
            <div class="header-menu-icons d-none d-lg-block">
                <a href="/tutorials" title="{{ trans('frontend_header.tutorial') }}">
                    <img src="/images/header-menu/tutorials.png" alt="{{ trans('frontend_header.tutorial') }}">
                    <br>{{ strtoupper(trans('frontend_header.tutorial')) }}
                </a>
            </div>
            <div class="header-menu-icons d-none d-lg-block">
                <a href="/network" title="{{ trans('frontend_header.network') }}">
                    <img src="/images/header-menu/network.png" alt="{{ trans('frontend_header.network') }}">
                    <br>{{ strtoupper(trans('frontend_header.network')) }}
                </a>
            </div>
            <div class="header-menu-icons d-none d-lg-block">
                <a href="/labs" title="{{ trans('frontend_header.labs') }}">
                    <img src="/images/header-menu/labs.png" alt="{{ trans('frontend_header.labs') }}">
                    <br>{{ strtoupper(trans('frontend_header.labs')) }}
                </a>
            </div>
            <div class="header-menu-icons d-none d-lg-block">
                <a href="/about" title="{{ trans('frontend_header.about') }}">
                    <img src="/images/header-menu/about.png" alt="{{ trans('frontend_header.about') }}">
                    <br>{{ strtoupper(trans('frontend_header.about')) }}
                </a>
            </div>  

            <!-- mobile menu -->
            <div class="dropdown d-none d-md-block d-lg-none">
                <div class="header-menu-icons float-left dropdown-toggle" id="dropdownHeaderMenuMD" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img src="/images/header-menu/menu.png"  alt="{{ trans('frontend_header.menu') }}">
                </div> 
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownHeaderMenuMD">
                    
                    <a class="dropdown-item" href="/contact" title="{{ trans('frontend_header.contact') }}">
                        <img src="/images/header-menu/contact.png" alt="{{ trans('frontend_header.contact') }}">
                            {{ strtoupper(trans('frontend_header.contact')) }}
                    </a>
                    <a class="dropdown-item" href="/tutorials" title="{{ trans('frontend_header.tutorial') }}">
                        <img src="/images/header-menu/tutorials.png" alt="{{ trans('frontend_header.tutorial') }}">
                            {{ strtoupper(trans('frontend_header.tutorial')) }}
                    </a>
                    <a class="dropdown-item" href="/network" title="{{ trans('frontend_header.network') }}">
                        <img src="/images/header-menu/network.png" alt="{{ trans('frontend_header.network') }}">
                            {{ strtoupper(trans('frontend_header.network')) }}
                    </a>                                
                    <a class="dropdown-item" href="/ntwor" title="{{ trans('frontend_header.labs') }}">
                        <img src="/images/header-menu/labs.png" alt="{{ trans('frontend_header.labs') }}">
                            {{ strtoupper(trans('frontend_header.labs')) }}
                    </a> 
                    <a class="dropdown-item" href="/about" title="{{ trans('frontend_header.about') }}">
                        <img src="/images/header-menu/about.png" alt="{{ trans('frontend_header.about') }}">
                            {{ strtoupper(trans('frontend_header.about')) }}
                    </a>                                                               
                </div>

            </div>
        </div>


        <!-- PROFILE -->
        <div class="col-md-2 px-3 text-center">

            @if (Auth::check())
                <!-- menu if user logged in -->
                <div class="dropdown">

                    <!-- user image -->
                    <div class="header-menu-profile" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background-image: url('{{ user_image(Auth::user()->id) }}');"></div>

                    <!-- user menu -->
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUserMenu">
                        <!-- backend if admin -->
                        @if (Auth::user()->hasGroup('admin'))
                            <a class="dropdown-item" href="/backend">{{ trans('frontend_header.admin') }}</a>
                        @endif
                        <!-- profile -->
                        @if (Auth::user()) 
                            <a class="dropdown-item" href="/profile">{{ trans('frontend_header.profile') }}</a>
                        @endif 
                        <!-- Projects modal list -->
                        <a href="#" class="modal_url dropdown-item" data-url="/profile/groups_list/{{ Auth::user()->id }}">{{ trans('frontend_header.my_projects') }}</a></li>
                        <hr>

                        <!-- logout -->
                        <a class="dropdown-item" href="/logout">{{ trans('frontend_header.logout') }}</a>
                    </div>

                    <div class="header-logo-title">{{ Auth::user()->name }}</div>
                    
                </div>
                
            @else
                <!-- menu if NOT logged in -->
                <a href="/login">
                    <div class="header-menu-profile" style="background-image: url('{{ user_image() }}');"></div>
                </a>
                <div class="header-logo-title">{{ trans('frontend_header.login') }}</div>
            @endif

        </div>

        
    </div>                
</div>