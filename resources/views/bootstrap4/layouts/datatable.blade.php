<div id="home-page-datatable">
    <!-- datatable sort dropdown -->
    <div class="row mt-3 mb-3 pb-2 border-bottom" id="datatable-sort-dropdown">
        <div class="col-12 text-center">
            <i id="datatable-grid-view-switch" class="fa fa-th-large text-lens mr-1 hover"></i>
            <i id="datatable-list-view-switch" class="fa fa-bars mr-3 hover"></i>

            <a class="dropdown-toggle text-lens datatable-sort-dropdown-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ strtoupper(trans('frontend_home_page.sort_by')) }}: {{ strtoupper(trans('resources.year')) }} <span class="caret"></span>    
            </a>
            <div class="dropdown-menu text-lens">
                <div class="text-left dropdown-item px-3" data-sort="4">{{ strtoupper(trans('resources.year')) }}</div>
                <div class="text-left dropdown-item px-3" data-sort="1">{{ strtoupper(trans('resources.title')) }}</div>
                <div class="text-left dropdown-item px-3" data-sort="2">{{ strtoupper(trans('resources.author')) }}</div>
                <div class="text-left dropdown-item px-3" data-sort="3">{{ strtoupper(trans('resources.country')) }}</div>
                <div class="text-left dropdown-item px-3" data-sort="5">{{ strtoupper(trans('resources.language')) }}</div>
                <div class="text-left dropdown-item px-3" data-sort="6">{{ strtoupper(trans('resources.type')) }}</div>
            </div>
        </div>
    </div>

    <!-- datatable -->
    <div class="row" id="home_data_table">
        <div class="col-12">
            <table class="display home_datatable" id="datatable" style="display:none; width:100%;">
                <thead>
                    <tr>
                        <th></th>
                        <th style="width: 100%">{{ strtoupper(trans('text.title')) }}</th>
                        <th>{{ strtoupper(trans('text.author')) }}</th>
                        <th class="hidden-md hidden-sm hidden-xs">{{ strtoupper(trans('text.country')) }}</th>
                        <th>{{ strtoupper(trans('text.year')) }}</th>
                        <th>{{ strtoupper(trans('text.language')) }}</th>
                        <th>type</th>
                        <th>platform</th>
                        <th>server_id</th>
                        <th>created_at_formated</th>
                        <th>category</th>
                        <th>length</th>
                        <th>course</th>
                        <th>criteria</th>
                        <th class="hidden-md hidden-sm hidden-xs">{{ strtoupper(trans('text.category')) }}</th>
                        <th>institution</th>
                        <th>producer</th>
                        <th>contents</th>
                        <th>resource_type</th>
                        <th>designer</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="datatable-loader" class="text-center">
        <i class="fa fa-spinner fa-pulse fa-2x fa-fw mt-4"></i>
    </div>

    @if ($page == "home")
        <div id="datatable-load-more" class="row text-center mt-3" style="display:none">
            <a href="/search" class="btn bck-lens text-light">{{ strtoupper(trans('frontend_home_page.load_more')) }}</a>
        </div>
    @endif

    @include('bootstrap4.layouts.datatable_js')

</div>
