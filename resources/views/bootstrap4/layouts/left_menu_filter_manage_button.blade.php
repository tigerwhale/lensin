<!-- Removed for central - && server_property('central_server') != 1 -->
    <div class="btn bck-lens text-light rounded-circle btn-30" id="manage_button">
        <i class="fas fa-pencil-alt"></i>
    </div>

    <div class="edit_resource_buttons" style="display: none; margin-top: 20px;">
        <!-- MANAGE COURSES -->
        @if (Auth::user()->isRole('managecourses'))
            {!! createLeftFilterButton("/courses/modify", "bck-course-img", "edit_courses", strtoupper(trans('left_menu_filter.courses')) ) !!}
        @endif

        <!-- MANAGE LECTURES -->
        @if (Auth::user()->isRole('managestudycases'))
            {!! createLeftFilterButton("/lectures/modify", "bck-lecture-img", "edit_lectures", strtoupper(trans('left_menu_filter.lectures')) ) !!}
        @endif

        <!-- MANAGE TOOLS -->
        @if (Auth::user()->isRole('managetools'))
            {!! createLeftFilterButton("/tools/modify", "bck-tool-img", "edit_tools", strtoupper(trans('left_menu_filter.tools')) ) !!}
        @endif

        <!-- MANAGE CASE STUDIES -->
        @if (Auth::user()->isRole('managestudycases'))
            {!! createLeftFilterButton("/study_cases/modify", "bck-study-case-img", "edit_study_cases", strtoupper(trans('left_menu_filter.study_cases')) ) !!}
        @endif

        <!-- MANAGE CHALLANGES -->
        @if (Auth::user()->isRole('manageprojects'))
            {!! createLeftFilterButton("/projects", "bck-project-img", "edit_challenges", strtoupper(trans('left_menu_filter.projects')) ) !!}
        @endif

        <!-- MANAGE CHALLANGES -->
        @if (Auth::user()->isRole('managenews'))
            {!! createLeftFilterButton("/news/modify", "bck-news-img", "", strtoupper(trans('left_menu_filter.news')) ) !!}
        @endif

        <!-- MANAGE TUTORIALS -->
        @if (Auth::user()->isRole('managetutorials'))
            {!! createLeftFilterButton("/tutorials/modify", "bck-tutorial-img", "", strtoupper(trans('left_menu_filter.tutorials')) ) !!}
        @endif

    </div>

<script>
    $('#manage_button').click(function(){ 
        $('.edit_resource_buttons').each(function(i, obj) {
            $(obj).toggle();
        });
        $('.left_filter_menu').toggleClass('edit-mode');
    })
</script>


<?php 
    function createLeftFilterButton($href, $class, $id, $text) 
    {
        $returnHTML = '<div class="row">';
            $returnHTML .= '<div class="col-12 pb-2">';
                $returnHTML .= '<a href="' . $href . '">';
                    $returnHTML .= '<div class="' . $class . ' btn btn-35 shadow-small mx-auto" id="' . $id . '" ></div>';
                $returnHTML .= '</a>';
            $returnHTML .= '</div>';
            $returnHTML .= '<div class="col-12 resource_name">';
                $returnHTML .= $text;
            $returnHTML .= '</div>';
        $returnHTML .= '</div>';
        echo $returnHTML;
    }
?>
