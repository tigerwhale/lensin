<div class="container d-md-none bck-lens py-2">
    <div class="row no-gutters">
      
        <div class="col-1 {!! $page != "search" ? 'd-none':"" !!}" id='mobile-header-search-back'>
            <a href="/">
                <img src="/images/header-menu/mobile-header-back.png" class="header-logo-mobile" alt="{{ server_property('server_name') }}">
            </a>
        </div>
        <div class="col-5 col-sm-5 {!! $page != "search" ? 'd-none':"" !!}">
            <input type="text" class="form-control header-search-mobile mt-2 inline" id="mobile-header-search-input"/>
        </div>    

        <div class="col-6 col-sm-6 col-md-8 {!! $page == "search" ? 'd-none':"" !!}">
            <a title="{{ server_property('server_name') }}" href="/">
                <img src="{!! logo(true) !!}" class="header-logo-mobile" alt="{{ server_property('server_name') }}">
            </a>
        </div>

        <div class="col-6 col-sm-6 text-right">
            <!-- search button -->
            @if ($page != "search")
                <img src="/images/header-menu/mobile-header-search.png" class="header-buttons opacity-5 mr-2" id="header-search-button">
            @endif
            
            <!-- menu button -->
            @if ($page != "search")
                <img src="/images/header-menu/mobile-header-menu-on.png" class="header-buttons mr-2 opacity-5" id="header-menu-button">
            @endif
            
            <!-- toggle advanced filters button -->
            @if ($page == "search")
                <img src="/images/header-menu/mobile-header-advanced-research.png" 
                        class="header-buttons mr-2 {{ isset( $_GET['advanced'] ) ? "opacity-10" : "opacity-5" }}" 
                        id="header-advanced-research-button">
            @endif

            <!-- toggle resources/platforms button -->
            <img src="/images/header-menu/mobile-switches-header-10.png" class="header-buttons" id="header-resources-platforms-button" data-visible-filters=2>

            <!-- toggle user login -->
            <div class="mobile-login-button" style="height: 35px;display: inline-block;">
                <i class="far fa-user text-white ml-2 opacity-5" style="font-size:1.4em;" id="mobile-login-button"></i>
            </div>
        </div>

    </div>
</div>

<!-- Login -->
<div class="container mt-3 d-md-none pb-3" id="header-mobile-login" style="display:none;">
    <div class="row no-gutters text-center">
        @if(Auth::user()) 
            <div class="col-12 mb-3">
                {{ Auth::user()->fullName }}
            </div>
                <a href="/logout" class="col-6"><button type="submit" class="btn btn-primary form-control  mb-2">{{ trans('text.logout') }}</button></a>
                <a href="/profile" class="col-6"><button type="submit" class="btn btn-primary form-control mb-2">{{ trans('text.profile') }}</button></a>
        @else 
            <form role="form" class="form-inline" method="POST" action="/login">
                {{ csrf_field() }}
                <input id="email" type="email" class="form-control mb-2 col-12 col-sm-5 mr-2" placeholder="{{ trans('text.email') }}" name="email" value="" required="" autofocus="">
                <input id="password" type="password" class="form-control col-12 col-sm-5 mb-2 mr-2"  placeholder="{{ trans('text.password') }}" name="password" required="">
                <button type="submit" class="btn btn-primary form-control mb-2">{{ trans('text.login') }}</button>
            </form>       
            <div class="col-6 text-left">
                 <a href="{{ url('/register') }}">
                    {{ trans('text.not_registered') }}? {{ trans('text.join_as_participant') }}.
                </a>
            </div>
            <div class="col-6 text-right">
                <a href="{{ url('/password/reset') }}">
                    {{ trans('text.forgot_password') }}
               </a>
            </div>
        @endif       
        
    </div>
</div>


<!-- pages menu -->
<div class="container mt-2 d-md-none" id="header-mobile-menu" style="display:none;">
    <div class="row no-gutters">
        <div class="col text-center">
            <a href="/about" title="{{ trans('frontend_header.about') }}">
                <img src="/images/header-menu/about.png" alt="{{ trans('frontend_header.about') }}">
                <br>{{ strtoupper(trans('frontend_header.about')) }}
            </a>
        </div>       
        <div class="col text-center">
            <a href="/labs" title="{{ trans('frontend_header.labs') }}">
                <img src="/images/header-menu/labs.png" alt="{{ trans('frontend_header.labs') }}">
                <br>{{ strtoupper(trans('frontend_header.labs')) }}
            </a>
        </div>            
        <div class="col text-center">
            <a href="/network" title="{{ trans('frontend_header.network') }}">
                <img src="/images/header-menu/network.png" alt="{{ trans('frontend_header.network') }}">
                <br>{{ strtoupper(trans('frontend_header.network')) }}
            </a>
        </div>
        <div class="col text-center">
            <a href="/tutorials" title="{{ trans('frontend_header.tutorial') }}">
                <img src="/images/header-menu/tutorials.png" alt="{{ trans('frontend_header.tutorial') }}">
                <br>{{ strtoupper(trans('frontend_header.tutorial')) }}
            </a>
        </div>
        <div class="col text-center">
            <a href="/contact" title="{{ trans('frontend_header.contact') }}">
                <img src="/images/header-menu/contact.png" alt="{{ trans('frontend_header.contact') }}">
                <br>{{ strtoupper(trans('frontend_header.contact')) }}
            </a>
        </div>
    </div>
</div>

<!-- resources menu -->
<div class="container mt-3 d-md-none" id="header-mobile-resources" style="display:block;">
    <div class="row no-gutters">

        @foreach(['courses', 'lectures', 'tools', 'study_cases', 'projects'] as $resource)
            <div class="col text-center">
                <label class="switch type mb-1" for="{!! $resource !!}_switch_mobile">
                    <input type="checkbox" id="{{ $resource }}_switch_mobile" class="resource-mobile-switch" data-resource="{{ $resource }}">
                    <div class="slider round {{ $resource }}_slider"></div>
                </label>
                <div class="resource_name">{{ strtoupper(trans('text.' . $resource)) }}</div>
            </div>
        @endforeach

    </div>
</div>

<!-- platforms mobile menu -->
<div class="container mb-3 d-md-none" id="header-mobile-platforms-menu" style="display:none;">
    <div class="row">
        <div class="col-12" >
            <div style="width:100%; overflow-y: hidden; white-space:nowrap; overflow: scroll;">
                <table>
                    <tr id="header-mobile-platforms">
                        <td style="width:70px; display:inline-block; padding-top: 7px; vertical-align: top;">
                            <!-- ON/OFF SWITCH -->
                            <label class="switch_platform" style="margin-left:18px;">
                                <input type="checkbox" class="platforms_mobile" id="all_platforms_switch_mobile" value="all_platforms" data-server-id="100000">
                                <div class="slider_platform round all_platforms_switch"></div>
                            </label>
                            <div class="platform_name" style="text-align:center; white-space: normal; margin-bottom:0px; ">{{ strtoupper(trans('frontend_right_menu_filter.all_off_on')) }}</div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
