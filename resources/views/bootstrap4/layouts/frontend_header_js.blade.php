<!-- HEADER JS -->
<script type="text/javascript">

	var typingTimer;
	var doneTypingInterval = 1000;

	$(document).ready(function() {

		/**
		 * Mobile header search input
		 * In keyup change the value for header search
		 */
		$("#mobile-header-search-input").keyup ( function() {
			$('#header_search').val( $(this).val() ).change();
		});

		/**
		 * Header search input box
		 * On input change the mobile input value and set the typing timer 
		 * so that we dont search on each keypress
		 */
		$('#header_search').change( function() {
			$("#mobile-header-search-input").val( $(this).val() );
			clearTimeout(typingTimer);
			typingTimer = setTimeout(doneTyping, doneTypingInterval);
		});


		/**
		 * Header menu toggle
		 */
		$("#header-menu-button").click( function() {
			$("#header-mobile-menu").slideToggle('fast');
			$(this).toggleClass('opacity-10');
			$(this).toggleClass('opacity-5');
		});

		/**
		 * Login menu toggle
		 */
        $('body').on('click', '#mobile-login-button', function(){
			$("#header-mobile-login").slideToggle('fast');
			$(this).toggleClass('opacity-5');
		});


		/**
		 * Mobile resources menu toggle
		 */
		$('#header-resources-platforms-button').click( function() {
			// 1- nothing visible; 2- resources switches visible; 3- all visible
			var visibleFilters = $(this).data('visible-filters');
			if (visibleFilters == 1) 
			{
				$(this).data('visible-filters', 2);
				$(this).attr('src', "./images/header-menu/mobile-switches-header-10.png");
				
				$("#header-mobile-resources").slideToggle('fast');

			} else if(visibleFilters == 2) 
			{
				$(this).data('visible-filters', 3);
				$(this).attr('src', "./images/header-menu/mobile-switches-header-11.png");
				
				$("#header-mobile-platforms-menu").slideToggle('fast');

			} else if(visibleFilters == 3) 
			{
				$(this).data('visible-filters', 1);
				$(this).attr('src', "./images/header-menu/mobile-switches-header-00.png");
				$("#header-mobile-resources").slideToggle('fast');
				$("#header-mobile-platforms-menu").slideToggle('fast');
			}
		
		});
	});            


	@if($page != 'search')

		/**
		 * Redirect to the search page 
		 * when user finishes typing
		 */
		function doneTyping () {
			var textToSend = $('#header_search').val().replace(" ", "+");
			window.location.href = '/search?header_search=' + textToSend;
		}

		/**
		 * Search button 
		 * redirects to search page
		 */
		$('#header-search-button').click( function() {
			window.location.href = '/search?mobile-search=on&=mobile-resources-filter=on&mobile-platforms-filter=on';
		});

	@endif

	
	@if($page == 'search')
		
		// If this is the search page, shorten the timer
		var doneTypingInterval = 300;

		$(document).ready(function() {
			$('#header-menu-button').click( function() {
				$("#header-mobile-menu").slideToggle('fast');
				$(this).toggleClass('opacity-10');
				$(this).toggleClass('opacity-5');
			});

			$('#header-advanced-research-button').click( function() {
				$("#header-advanced-search").click();
				$(this).toggleClass('opacity-10');
				$(this).toggleClass('opacity-5');
			});


		});


		/**
		 * Focus on search input when redirected by search button
		 */
		@if (isset($_GET['mobile-search'])) 
			$(document).ready(function() {
				document.getElementById("mobile-header-search-input").focus();
			});
		@endif

		/**
		 * Redirect to the search page 
		 * when user finishes typing
		 */
		function doneTyping () {
			datatableTable.search( $('#header_search').val() ).draw() ;
		}

	@endif		

</script>
