<!-- LANGUAGES -->
<div class="dropdown text-center text-lens">
    <div class="dropdown-toggle dropdown_selector" id="dropdownMenuLanguageLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"  style="cursor: pointer;">
        {{ trans('frontend_right_menu_filter.languages') }}
        <span class="caret"></span>
    </div>
    
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLanguageLeft">
        <div class="dropdown text-left" style="padding-left: 15px; padding-top: 7px;">
            
            <!-- Resource language
            <span class="text-gray">{{ trans('frontend_right_menu_filter.resources_languages') }}:</span>
            <div id="resourcesLanguageSelect" style="margin-left: 20px;">
                <i class="fa fa-spinner fa-pulse fa-fw"></i>
            </div>

            <hr>-->

            <!-- Interface language -->
            <p class="text-gray">{{ trans('frontend_right_menu_filter.interface_language') }}:</p>
            
            <?php 
                $cur_lan_name = cur_lan_name();
            ?>
            
            @foreach ($languages as $language)
                <div class = "language-link text-lens" data-language="{!! $language->locale !!}">
                    <a href="/locale/{!! $language->locale !!}">{!! $language->name !!}</a>
                </div>
            @endforeach
        </div>	
    </div>
</div>

<div class="right-menu-filter">

    <!-- PLATFORMS -->
    <h4 class="resources_title">{{ strtoupper(trans('frontend_right_menu_filter.platforms')) }}</h4>

    <div id="right_switches">

        <!-- ON/OFF SWITCH -->
        <label class="switch_platform">
            <input type="checkbox" class="platforms" id="all_platforms_switch" data-server-id="100000">
            <div class="slider_platform round all_platforms_switch"></div>
        </label>
        <div class="platform_name">{{ strtoupper(trans('frontend_right_menu_filter.all_off_on')) }}</div>
        <i id="platform_switches_spinner" class="fa fa-spinner fa-pulse fa-2x fa-fw mt-4"></i>
    </div>

</div>
@include('bootstrap4.layouts.right_menu_filter_js')