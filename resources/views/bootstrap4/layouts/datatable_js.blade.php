<script src="/js/jquery.shave.min.js"></script>
<script>
    var datatableTable = "";
    var datatableData = "";
    var datatableDoNotLoad = true;
    var lastCheckedFilter = "";    

    
    var advancedResearchFieldsNoDataText = "{{ strtoupper(trans('frontend_datatable.no_data')) }}";


    $(document).ready(function()
    {
        loadDatatable(page);
        @if ($page != 'home')
            populateDatatableRowsWhenNearPageBottom();
        @endif
        
        /**
         *  Grid and list buttons
         */
        $(document).on('click', "#datatable-grid-view-switch, #datatable-list-view-switch", function() {
            $('#datatable-grid-view-switch').toggleClass("text-lens");
            $("#datatable-list-view-switch").toggleClass("text-lens");

            $('#datatable').toggle();
            $('#datatable-grid').toggle();
        });


        /**
         * Datatable grid link
         */
        $('body').on('click', '.datatable_grid_div, .datatable_row', function()
        {
            window.location.href = $(this).data('link');
        });


        /**
         *  Sort datatable dropdown
         */
        $('#datatable-sort-dropdown .dropdown-item').click(function() 
        {
            $('#home-page-datatable .datatable-sort-dropdown-button').html( "{{ strtoupper(trans('frontend_home_page.sort_by')) }}: " +  $(this).html() + ' <span class="caret"></span>');
            datatableTable.order( [ $(this).data('sort'), 'asc' ] ).draw();
        });  


        /** 
         * Load more resources button
         */
        $('#datatable-load-more').click(function() 
        {
            addRowsToDatatable(12);
        });
    });



    /**
     * Load datatable rows when view is near page bottom
     */
    function populateDatatableRowsWhenNearPageBottom() 
    {
        window.didScroll = false;

        $(window).scroll(function() {
            didScroll = true;
        });  

        setInterval(function() {
            if ( didScroll && typeof(datatableTable) == "object") 
            {
                didScroll = false;

                var isNearBottom = ($(window).scrollTop() >= $(document).height() - $(window).height() - 600);
                var hasMoreDataToShow = (datatableTable.rows( { search:'applied' } )[0].length > datatableTable.page.len());
                
                if ( isNearBottom && hasMoreDataToShow) 
                {
                    addRowsToDatatable(12);
                }                
            }
        }, 500); 
    }




    /**
     * Load the Datatable :D
     */
    function loadDatatable(page) 
    {
        $.fn.dataTable.ext.errMode = 'throw';

        dataUrl = "/api/all_resources";
        if (page == "home")
        {
            dataUrl = "/api/home_page_resources_datatable";
        }

        dataFromAllServers(dataUrl, 26).then( function(resourcesData) {
            datatableData = resourcesData;
            datatableTable = $('#datatable').DataTable({
                data: datatableData,
                processing: true,
                responsive: true,
                stateSave: false, 
                dom: '<"top">rt<"bottom"><"clear">',
                order: [[ 9, "desc" ]],
                lengthMenu: [[12, 24, 48, 96, -1], [12, 24, 48, 96, "All"]],
                fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    return createGridElement( nRow, aData, iDisplayIndex, iDisplayIndexFull );
                },
                fnPreDrawCallback: function (oSettings) {
                    emptyGridDiv();
                    $("#datatable-loader").show();
                },    
                fnDrawCallback: function( oSettings,aData ) {
                    // Hide loading container
                    $("#datatable-loader").hide();
                    setTimeout(function() {
                        areThereResourcesInDatatable();
                        },10)    
                    // Limit text in grid
                    $("#datatable-grid .card-text").shave(105);
                },   

                fnInitComplete: function (settings, json) {
                    var datatableLoadMoreDiv = document.getElementById("datatable-load-more");
                    if (datatableLoadMoreDiv)
                    {
                        datatableLoadMoreDiv.style = "display:block";
                    }
                   
                },
                aoColumnDefs: [
                    {
                        "mRender": function ( data, type, row ) {
                            return "<span style='display:none'>" + row.type + "</span><div class='text-center'><div class='bck-" +  row.type + "-img btn-30 shadow-sm mb-1'></div><img src='http://" +  row.platform + "/images/server/logo.png' class='table_view_image platform'></div>";
                        },
                        "sWidth": "20px",
                        "aTargets": [ 0 ]
                    },
                    {
                        "mRender": function ( data, type, row ) {
                            return row.title;
                        },
                        "sWidth": "50%",
                        "aTargets": [ 1 ]
                    },
                    {
                        "mRender": function ( data, type, row ) {
                            return row.author;
                        },
                        "className": "hidden-xs",
                        "aTargets": [ 2 ]
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.country; },
                        "className": "hidden-md hidden-sm hidden-xs",
                        "aTargets": [ 3 ]
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.year; },
                        "className": "hidden-xs",
                        "aTargets": [ 4 ]
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.language; },
                        "className": "hidden-xs",
                        "aTargets": [ 5 ]
                    }, 
                    {
                        "mRender": function ( data, type, row ) { return row.type; },
                        "aTargets": [ 6 ],
                        "visible": false
                    }, 
                    {
                        "mRender": function ( data, type, row ) { return row.platform; },
                        "aTargets": [ 7 ],
                        "visible": false
                    }, 
                    {
                        "mRender": function ( data, type, row ) { return row.platform_id; },
                        "aTargets": [ 8 ],
                        "visible": false
                    }, 
                    {
                        "mRender": function ( data, type, row ) { return row.created_at_formated; },
                        "aTargets": [ 9 ],
                        "visible": false
                    },   
                    {
                        "mRender": function ( data, type, row ) { return row.category; },
                        "aTargets": [ 10 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.length; },
                        "aTargets": [ 11 ],
                        "visible": false
                    },  
                    {
                        "mRender": function ( data, type, row ) { return row.course; },
                        "aTargets": [ 12 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.criteria; },
                        "aTargets": [ 13 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.category; },
                        "className": "hidden-md hidden-sm hidden-xs",
                        "aTargets": [ 14 ],
                        "visible": true
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.institution; },
                        "aTargets": [ 15 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.producer; },
                        "aTargets": [ 16 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.contents; },
                        "aTargets": [ 17 ],
                        "visible": false
                    },
                    {
                        "mRender": function ( data, type, row ) { return row.resource_type; },
                        "aTargets": [ 18 ],
                        "visible": false
                    },                                                          
                    {
                        "mRender": function ( data, type, row ) { return row.designer; },
                        "aTargets": [ 19 ],
                        "visible": false
                    },
                ],
                multiSelect:{
                    multiSelectOptions:[
                        {selector:"#author_filter", col: 2, dataName: "author"},
                        {selector:"#country_filter", col: 3, dataName: "country"},
                        {selector:"#year_filter", col: 4, dataName: "year"},
                        {selector:"#length_filter", col: 11, dataName: "length"},
                        {selector:"#institution_filter", col: 15, dataName: "institution"},
                        {selector:"#category_filter", col: 14, dataName: "category"},
                        {selector:"#contents_filter", col: 17, dataName: "contents"},
                        {selector:"#course_filter", col: 12, dataName: "course"},
                        {selector:"#producer_filter", col: 16, dataName: "producer"},
                        {selector:"#resource_type_filter", col: 18, dataName: "resource_type"},
                        {selector:"#designer_filter", col: 19, dataName: "designer"},
                        {selector:"#language_filter", col: 5, dataName: "language"}
                    ]                    
	        	}                        
            });
            // end datatable init
            
            if (page == "search") 
            {
                if ($('#header_search').val()) {
                    datatableTable.search( $('#header_search').val() ).draw();
                }

                checkDatatableFilters();
                
                @if ( isset($_GET['advanced']) )
                    changeAdvancedResearchCaret();
                    resourceSwitchesEnableAllIfDisabled();
                    $("#search-box-header").toggleClass('advanced-research-background');
                    $("#multiselectRemoveAllFilters").show();
                @endif 
                
                advancedResearchSelectedResourcesDisablesFilters();

            }

        });

    }

    /**
     * Check datatable grid div exists and empty it
     */
    function emptyGridDiv() {
        
        if ($("#datatable-grid").length != 0) {
            $("#datatable-grid").html("");
        } else {
            // create a new grid div
            var datatable_grid = $("<div>", {
                "id": "datatable-grid",
                class: "row datatable-grid"
            });    
            // add it after list table in datatable
            $("#datatable").after(datatable_grid);
        }  
    }


    /**
     * Create datatable grid rows 
     */
    function createGridElement(nRow, aData, iDisplayIndex, iDisplayIndexFull) 
    {
        var data_link = createResourceLink(aData);

        nRow.classList.add("datatable_row");
        nRow.setAttribute("data-link", data_link);

        if (aData.image) {
            var imageSrc = "background-image: url(" + aData.image + ")";
        } else {
            var imageSrc = "background-image: url(/images/resource/" + aData.type + "_big.png)";
        }


        var resourceDiv = createElement('div', 'col-6 col-sm-4 col-md-4 col-lg-3 datatable_grid_div', {"data-link" : data_link});
        document.getElementById("datatable-grid").append(resourceDiv);

        var cardDiv = createElement('div', 'card mb-3 resource-card ' + aData.type + "_card" );
            resourceDiv.appendChild(cardDiv);

                // IMAGE
                if (aData.image) {
                    var imageSrc = "background-image: url(" + aData.image + ")";
                } else {
                    var imageSrc = "background-image: url(/images/resource/" + aData.type + "_big.png)";
                }
                var cardImage = createElement('div', 'card-img-top datatable-image-grid', { "style" : imageSrc });
                cardDiv.appendChild(cardImage);

                // TEXT
                var cardText = createElement('div', "card-text px-2 pt-2");
                cardText.innerHTML = aData.title;
                cardDiv.appendChild(cardText);

                // FOOTER
                var cardFooter = createElement("div", "position-absolute pb-2 px-2 fixed-bottom text-center");
                cardFooter.innerHTML = "<span class='footer-year'>" + aData.year + "</span>";
                cardDiv.appendChild(cardFooter);

                    var footerResourceType = createElement("div", "bck-" + aData.type + "-img btn-30 border float-left");
                    cardFooter.appendChild(footerResourceType);

                    var footerPlatformLogo = createElement("div", "btn-20 float-right border mt-1 bck-lens", { "style": "background-image: url(http://" + aData.platform + "/images/server/logo.png)" });
                    cardFooter.appendChild(footerPlatformLogo);

        return nRow;
    }


    /**
     * Create resource link from row data
     */
    function createResourceLink(aData) 
    {
        var data_link = aData.type + "s/view/" + aData.id;
        if (server_id != aData.platform_id) {
            data_link = data_link + "?server_id=" + aData.platform_id;
        }

        return data_link;
    }
   

    /**
     * Add number of rows to datatable (list and grid)
     */
    function addRowsToDatatable(rowsToAdd = 12) 
    {
        document.getElementById("datatable-loader").style = "display:inline";
        datatableTable.page.len( datatableTable.page.len() + rowsToAdd ).draw();
    }


    /**
     * Filter the datatable 
     */
    function checkDatatableFilters()
    {
        advancedResearchSelectedResourcesDisablesFilters();
        datatableSearchPlatforms();
        datatableSearchResourceType();

        datatableTable.draw();
        showOrHideCriteriaView();
    }


    function datatableSearchAdvancedFilter() 
    {
        return true;
    }


    /**
     * If only criteria selected show the criteria view
     */
    function showOrHideCriteriaView() 
    {
        if ($('.resources:checkbox:checked').length == 1 && $('.resources:checkbox:checked:first').val() == "study_case") 
        {
            $('#searchPageTab').show();
            $('#datatable-sort-dropdown').addClass("border-0");
        } else {
            $('#searchPageTab').hide();
            $('#datatableTab').tab('show');
            $('#datatable-sort-dropdown').removeClass("border-0");
        }
    }


    function showResources(){
        
    }

    /**
     * Hide criteria view
     */
    function hideCriteria()
    {
        $('#criteria_header').hide(); 
        $('#select_by_criteria').hide();
    }


    /**
     *  Show checkboxes from filters
     */ 
    function filterAdvancedResearchOptions() 
    {
        filteredDataToSearch = datatableTable.rows( { filter : 'applied'} ).data();

        $.each(advancedResearchFields, function(key, fieldName) {
            if (fieldName != lastCheckedFilter) {
                $(".multiselect-container > li." + fieldName + " > a > label").hide();
            }
            eval(fieldName + 'Data = []') ;

            $.each(filteredDataToSearch, function(key, singleEntery) {
                if (singleEntery[fieldName]) {
                    $('input[value="' +  singleEntery[fieldName] + '"]').parent().show();
                }
            });
        });

        lastCheckedFilter = "";
        checkIfFiltersAreEmpty();
    }


    function checkIfFiltersAreEmpty() 
    {
        $(".multiselect-native-select").each(function()
        {
            var toShow = 0;
            $(this).find('label.checkbox').each(function() 
            {
                if ( !$(this).parent().hasClass('multiselect-all') && $(this).css("display") != "none" ) {
                    toShow = 1;
                }
            });

            if (toShow) {
                $(this).show();
            } else {
                $(this).hide();
            } 
        });
    }

    function datatableSearchPlatforms() 
    {
        // PLATFORMS
        var platformsSelected = [];
        $('.platforms:checked').each(function() {
            platformsSelected.push("\\b" + $(this).data('server-id') + "\\b");
        });

        var platformFilter = "";
        platformFilter = platformsSelected.join("|");

        // search the table
        datatableTable.column( 8 ).search(platformFilter, true, false, true);

        return true;
    }


    function datatableSearchResourceType() 
    {
        // PLATFORMS
        var resourceTypesSelected = [];
        $('.resources:checked').each(function() {
            resourceTypesSelected.push($(this).val());
        });

        var resourceFilter = "";
        resourceFilter = resourceTypesSelected.join("|");

        // search the table
        datatableTable.column( 6 ).search(resourceFilter, true, false, true);

        return true;
    }


    function advancedResearchSelectedResourcesDisablesFilters() 
    {
        setTimeout(function() {
            enableAllAdvancedButtons();
            var courses_switch = ['category', 'designer', 'producer', 'resource_type', 'state', 'theme', 'contents', 'course'];
            var lectures_switch = ['designer', 'category', 'producer', 'state', 'theme'];
            var tools_switch = ['course', 'contents', 'length', 'designer', 'theme', 'resource_type'];
            var study_cases_switch = ['course', 'length', 'resource_type', 'theme', 'contents'];
            var projects_switch = ['designer', 'category', 'length', 'producer', 'state', 'contents', 'resource_type'];

            var resourceButtons = ['courses_switch', 'lectures_switch', 'tools_switch', 'study_cases_switch', 'projects_switch'];

            resourceButtons.forEach(function(resourceName) 
            {
                if (document.getElementById(resourceName).checked) 
                {
                    var buttonsToDisable = eval(resourceName);
                    buttonsToDisable.forEach( function(buttonToDisable) {
                        var button = document.getElementById(buttonToDisable + '_filter');
                        //$("#length_filter").multiselect('disable');
                        
                        if ( button ) 
                        {
                            $("#" + buttonToDisable + "_filter").multiselect('enable');
                            $("#" + buttonToDisable + "_filter").multiselect('deselectAll', false);
                            $("#" + buttonToDisable + "_filter").multiselect('refresh');
                            $("#" + buttonToDisable + "_filter").multiselect('disable');
                            
                            datatableTable.columns( $("#" + buttonToDisable + "_filter").data('column-id') ).search("");
                        }
                    });
                }
            });

            datatableTable.draw();
        }, 200);
    }	

    function enableAllAdvancedButtons() {
        advancedResearchFields.forEach( function(buttonId) {
            var button = document.getElementById(buttonId + '_filter');
            if (button) 
            {
                $("#" + buttonId + "_filter").multiselect('enable');
                //button.nextSibling.firstChild.disabled = false;    
            }     
        });

    }



    /**
     * If no rows in Datatable, add "No resources found" text
     */
    function areThereResourcesInDatatable() {
        if (datatableTable){
            if ( datatableTable.rows({ search:'applied' })[0].length == 0 ) {
                var noResourcesDiv = '<div class="noResourcesDiv text-center" style="width:100%"><br>';
                noResourcesDiv += '<div class="no_courses_badge resource_badge"></div> ';
                noResourcesDiv += '<div class="no_lectures_badge resource_badge"></div> ';
                noResourcesDiv += '<div class="no_tools_badge resource_badge"></div> ';
                noResourcesDiv += '<div class="no_cases_badge resource_badge"></div> ';
                noResourcesDiv += '<div class="no_projects_badge resource_badge"></div>';
                noResourcesDiv += '<br>{{ trans('text.no_resources_found') }}</div>';
            
                $("#spinnerDiv").hide();
                $(".noResourcesDiv").remove();
                $("#datatable_wrapper").append(noResourcesDiv);
            } else {
                $(".noResourcesDiv").remove();
            }
        }
    }


</script>