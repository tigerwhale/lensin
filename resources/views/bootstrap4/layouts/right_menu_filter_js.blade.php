<!-- RIGHT MENU JS -->
<script type="text/javascript">

	$(function() {
		
		loadPlatformSwitchesFromCentralServer();

		$('.dropdown-submenu').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
		});

		// PLATFORMS SWITCH ON/OFF
		$('#all_platforms_switch').click(function() 
		{
			if ($(this).is(':checked')) {
				$('.platforms').prop('checked', true);
				$('.platforms_mobile').prop('checked', true);
			} else {
				$('.platforms').prop('checked', false);
				$('.platforms_mobile').prop('checked', false);
			}
		});


		// Mobile Platforms Fliter
		$("#header-mobile-platforms").on("click", ".platforms_mobile", function() {
            var buttonID = "#" + $(this).val() + "_switch";

		    if ($(buttonID).prop( "checked" )) {
                $(buttonID).prop( "checked", true );
            }
		});

		// FILTER PLATFORMS
		$("#right_menu_filter").on("click", ".platforms", function()
		{
			areAllPlatformSwitchesChecked();
			checkSinglePlatformSelected();
			
			// click the mobile switch			
			var platformName = $(this).val();
            clickButton('#' + platformName + '_switch_mobile');
			//$('#' + platformName + '_switch_mobile').ß;

			if (!central_server) 
			{
				if ( $('.platforms:checkbox:checked').length > 1 ) 
				{
					var filterOptionsSelected = filterOptionsSelectedForGetRequest();
					location.href = "{!! server_property('central_server_address') !!}/search?" + "&locale=" + locale + login_send + filterOptionsSelected;
				} 
			}
			
			checkDatatableFilters();
			checkCriteriaPlatforms();
		});

		$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
			if (!$(this).next().hasClass('show')) {
				$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
			}
			var $subMenu = $(this).next(".dropdown-menu");
			$subMenu.toggleClass('show');

			$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
				$('.dropdown-submenu .show').removeClass("show");
			});

			return false;
		});


	});

	function clickButton(buttonID) {
        if ($(buttonID).prop( "checked" )) {
            $(buttonID).prop( "checked", true );
        } else {
            $(buttonID).prop( "checked", false );
        }
    }

	/**
	 * Check what platforms are checked and show adequite Criteria
	 */
	function checkCriteriaPlatforms() {
		if (central_server)
		{
			$(".platforms:checkbox:not(:checked)").each( function() {
				let server_id = $(this).data('server-id');
				$('.accordion_server_id_' + server_id).hide('fast');
			});
			$(".platforms:checkbox:checked").each( function() {
				let server_id = $(this).data('server-id');
				$('.accordion_server_id_' + server_id).show('fast');
			});
		}
	}

	/**
	 * Load Platform Switches From Central Server
	 * then create switches, and filter options from URL
	 */
	function loadPlatformSwitchesFromCentralServer() 
	{
		$.post({
            url: '{!! server_property('central_server_address') !!}/api/server_list',
            data: {
                'requestServerId': {!! server_property('server_id') ? server_property('server_id') : 0 !!},
            },
            success: function(data, status) {
                createPlatformSwitches(eval(data));
                areAllPlatformSwitchesChecked();

                // filterOptionsSelectedForGetRequest();
                $('#platform_switches_spinner').remove();
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })
	}
	
	/**
	 * Create Platform Switches in .right_switches 
	 * @param  {array} server_list 
	 */
	function createPlatformSwitches(server_list) {
		
		var platformSwitches = "";
		var platformSwitchesMobile = "";

		if (central_server) { 
			var checked = "checked"; 
		} else {
			var checked = ""; 
			server_list.unshift({
				address:"{{ Request::root() }}",
				id:"{{ server_property('server_id') }}",
				name:"{{ server_property('platform_title') }}", 
				server_description:"{{ server_property('slogan') }}"
			});			
		}
		
		$.each(server_list, function (i, platform) 
		{
			// Title for hover
			if (platform.server_description == 'null') 
			{
				var server_description = platform.server_description;
			} else {
				var server_description = platform.name;
			}
			
			// checked if in URL or first switch on local server
			checked = isPlatformSwitchOn("platform_" + platform.id);
			if (!central_server && i == 0) 
			{
				checked = "checked";
			}

			// HTML
			platformSwitches += '<label class="switch_platform">';
				platformSwitches += '<input type="checkbox" class="platforms server_id" id="platform_' + platform.id + '_switch" value="platform_' + platform.id + '" data-url="' + platform.address + '" data-server-id="' + platform.id + '" ' + checked + '>';
				platformSwitches += '<div class="slider_platform round platform_' + platform.id + '_slider" title="' + server_description + '"></div>';
			platformSwitches += '</label>';
			platformSwitches +=  '<div class="platform_name" id="' + platform.id + '_platform">' + platform.name + '</div>';

			// HTML Mobile
			platformSwitchesMobile += '<td class="" style="width:70px; display:inline-block; padding-top: 7px; vertical-align: top;">';
				platformSwitchesMobile += '<label class="switch_platform" style="margin-left:18px;">';
					platformSwitchesMobile += '<input type="checkbox" class="platforms_mobile server_id" id="platform_' + platform.id + '_switch_mobile" value="platform_' + platform.id + '" data-url="' + platform.address + '" data-server-id="' + platform.id + '" ' + checked + '>';
					platformSwitchesMobile += '<div class="slider_platform round platform_' + platform.id + '_slider" title="' + server_description + '"></div>';
				platformSwitchesMobile += '</label>';
				platformSwitchesMobile +=  '<div class="platform_name" id="' + platform.id + '_platform" style="margin-bottom:0px; text-align:center; white-space: normal;">' + platform.name + '</div>';
			platformSwitchesMobile += '</td>';

			// CSS
			document.styleSheets[0].insertRule('input:checked + .slider_platform.round.platform_' + platform.id + '_slider:before { background-image: url("' + platform.address + '/images/server/logo.png"); }');
			document.styleSheets[0].insertRule('.slider_platform.round.platform_' + platform.id + '_slider:before { background-image: url("' + platform.address + '/images/server/logo_transparent.png"); }');

		});

		$("#right_switches").append(platformSwitches);
		$("#header-mobile-platforms").append(platformSwitchesMobile);
		checkCriteriaPlatforms();
	}


    function isPlatformSwitchOn(platform_id) {
    	var checkedReturnValue = "";
    	if (central_server) 
    	{
	    	var getParameters = getParams();
			if (getParameters['switch_platform']) 
			{
                $(getParameters['switch_platform']).each(function(key, value) 
                {
                	if (value == platform_id) 
                	{
                		checkedReturnValue = "checked";
                	}
                });
            } else {
            	checkedReturnValue = "checked";
            }
    	}
        
        return checkedReturnValue;
    }


	/**
	 * Checks if single platform is selected and redirects to that platform
	 */
	function checkSinglePlatformSelected()
	{
		if ( $('.platforms:checkbox:checked').length == 1 && $('.platforms:checkbox:checked').data('server-id') != server_id) 
		{
			let filterOptionsSelected = filterOptionsSelectedForGetRequest();
			let redirectUrl = $('.platforms:checkbox:checked').data('url') + "/search?locale=" + locale + login_send + filterOptionsSelected;
			window.location.href = redirectUrl;
		} else if (page != 'search') {
			let filterOptionsSelected = filterOptionsSelectedForGetRequest();
			let redirectUrl = "/search?locale=" + locale + login_send + filterOptionsSelected;
			window.location.href = redirectUrl;
		}		
	}


	/**
	 * Check if all right menu filter buttons are checked
	 * and check the all_platforms_switch accordingly
	 */
	function areAllPlatformSwitchesChecked() 
	{
		var allPlatformSwitchesLength = $(".platforms").not("#all_platforms_switch").length;
		var checkedPlatformSwitchesLength = $(".platforms:checked").not("#all_platforms_switch").length;

		if (allPlatformSwitchesLength == checkedPlatformSwitchesLength) 
		{
			$("#all_platforms_switch").prop('checked', true);
			$("#all_platforms_switch_mobile").prop('checked', true);
		} else 	{
			$("#all_platforms_switch").prop('checked', false);
			$("#all_platforms_switch_mobile").prop('checked', false);
		} 
	}


	/**
	 * Decode URI parameters
	 */
	function getParams() {
		
		url = decodeURIComponent(window.location.href);

		var regex = /([^=&?]+)=([^&#]*)/g, params = {}, parts, key, value;

		while((parts = regex.exec(url)) != null) {

			key = parts[1], value = parts[2];
			var isArray = /\[\]$/.test(key);

			if(isArray) {
				params[key.slice(0,-2)] = params[key.slice(0,-2)] || [];
				params[key.slice(0,-2)].push(value);
			}
			else {
				params[key] = value;
			}
		}
		return params;
	}


    /**
     * Filter selected options 
     * @return {string}    string formated for url
     */
    function filterOptionsSelectedForGetRequest() 
    {
        var optionsSelected = [];
        var advancedResearchFieldsWithPlatforms = [];
        advancedResearchFieldsWithPlatforms = advancedResearchFieldsWithPlatforms.concat(advancedResearchFields);
        advancedResearchFieldsWithPlatforms.push('switch_platform');

        $.each(advancedResearchFieldsWithPlatforms, function(key, fieldName) {

            $('.' + fieldName + ' input:checkbox:checked').each(function() {
                if ( $(this).val() ) {
                    data = $(this).val().toString();
                    data = data.replace(/&/g, "");
                    optionsSelected =  optionsSelected + "&" + fieldName + '[]=' + data;
                }
            });
        });

        return optionsSelected;
    }

</script>
