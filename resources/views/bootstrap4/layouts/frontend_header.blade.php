<header>
    <!-- MOBILE -->
    @include('bootstrap4.layouts.frontend_header_mobile')

    <!-- DESKTOP -->
    @include('bootstrap4.layouts.frontend_header_desktop')

    <!-- JavaScript -->
    @include('bootstrap4.layouts.frontend_header_js')
</header>

