<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 text-center text-md-left pb-3">
                <img src="/images/erasmus.png"  style="height: 40px">
            </div>
            <div class="col-12 col-md-6 text-center small pb-3">
                {!! social_icons() !!}
                <br>
                {{ trans('text.platform_version') }} 0.96
            </div>
            <div class="col-12 col-md-3 text-center text-md-right text-light">
                <span class="footer-creative-commons-icons">
                    <i class="fab fa-creative-commons"></i> <i class="fab fa-creative-commons-by"></i>
                </span>
                <p class="footer-creative-commons-text">
                    {{ trans('frontend_footer.creative_commons_licence') }}
                </p>
            </div>
        </div>
    </div>
</footer>
<div  class="modal fade bd-example-modal-lg js-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content js-modal-content">

        </div>
    </div>
</div>
<div  class="modal fade bd-example-modal-lg js-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content js-modal-content">

        </div>
    </div>
</div>
<div id="modalDiv"></div>