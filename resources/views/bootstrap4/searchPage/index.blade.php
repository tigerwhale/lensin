@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/left_menu_filter.css" rel="stylesheet">
    <link href="/css/lens/right_menu_filter.css" rel="stylesheet">
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/chosen.min.css" rel="stylesheet">
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    
    <script src="/js/chosen.jquery.min.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/lightbox.js"></script>

    <script src="/js/dataTables.multiselect2.js"></script>
    <script src="/js/bootstrap-multiselect.js"></script>
    <link href="/css/bootstrap-multiselect.css" rel="stylesheet">
    
@endsection


@section('content')

    <div class="container"> 
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-2 d-md-block d-none" style="min-height: 600px;">
				@include('bootstrap4.layouts.left_menu_filter', ['page' => $page])
			</div>

			<!-- MIDDLE CONTENT -->
            <div class="col-12 col-md-8" style="margin-top:20px;" id="article">
                @include('bootstrap4.layouts.advanced_research')

                <ul class="nav nav-tabs justify-content-center" style="display: none;" id="searchPageTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="datatables-tab" data-toggle="tab" href="#datatableTab" role="tab" aria-controls="datatableTab" aria-selected="true">{{ strtoupper(trans('resources.case_studies')) }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="criteria-tab" data-toggle="tab" href="#criteriaTab" role="tab" aria-controls="criteria" aria-selected="false">{{ strtoupper(trans('resources.criteria')) }}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="datatableTab" role="tabpanel" aria-labelledby="datatables-tab">
                        @include('bootstrap4.layouts.datatable')
                    </div>
                    <div class="tab-pane fade" role="tabpanel" id="criteriaTab" aria-labelledby="criteria-tab">
                        <div class="row mt-2">
                            <div class="col" id="criteriaTabContent">
                                <i id="criteriaSpinner" class="my-4 fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            </div>
                            <div class="col" id="criteriaTabFloat">
                            </div>
                        </div>
                    </div>
                </div>

                
			</div>

            <!-- MENU RIGHT -->
            <div class="col-2 d-md-block d-none text-center" id="right_menu_filter">
                @include('bootstrap4.layouts.right_menu_filter', ['page' => $page, 'languages' => $languages])
            </div>			

        </div>
    </div>

    @include('bootstrap4.searchPage.index_js')
       
        
@endsection
