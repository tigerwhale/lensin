<?php 
	$images = 0; 
?>
<div class="select_by_criteria_tab">
	<ul class="nav nav-tabs nav-justified" role="tablist">
		<li class="nav-item">
			<a class="nav-link active light-gray-bck" data-toggle="tab" href="#images" role="tab">{{ trans('text.images') }}</a>
		</li>
		<li class="nav-item">
			<a class="nav-link light-gray-bck" data-toggle="tab" href="#record" role="tab">{{ trans('text.records') }}</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<!-- IMAGES -->
		<div class="tab-pane fade active show select_by_criteria_tab_pane p-2" id="images" role="tabpanel">
	  		@foreach($studyCases as $studyCase)
	  			@if (isset($studyCase->resources))
	  				@foreach ($studyCase->resources as $resource)
	  					<?php $images = $images + 1; ?>

	  					@if ($loop->first)
	  						<a href="{!! $resource->path_or_preview() !!}" data-lightbox="slideshow_{!! $studyCase->id !!}" data-title="{!! $resource->name !!}">
	  							{!! $studyCase->name !!}
	  						</a>
	  						@if (!$loop->parent->last)
	  							<hr class="my-1">
	  						@endif
	  					@else 
	  						<a href="{!! $resource->path_or_preview() !!}" data-lightbox="slideshow_{!! $studyCase->id !!}" style="display: none;" data-title="{!! $resource->name !!}">
	  					@endif

	  				@endforeach
	  			@endif 

	  		@endforeach

			@if ($images == 0)
		  		{{ trans('criteria.no_images') }}
		  	@endif
		</div>
		<!-- RECORDS -->
		<div class="tab-pane fade select_by_criteria_tab_pane p-2" id="record" role="tabpanel" >
			@if ($studyCases->count() > 0)
		  		@foreach($studyCases as $studyCase)
		  			<a style="margin-bottom: 5px;" href="{{ $studyCase->server }}/study_cases/view/{{ $studyCase->id }}" target="_blank">
		  				{{ $studyCase->name }}
		  			</a>
		  			@if (!$loop->last)
		  				<hr class="my-1">
		  			@endif
		  		@endforeach
		  	@else 
		  		{{ trans('criteria.no_records') }}
		  	@endif
		</div>
	</div>
</div>