<script>

    $(document).ready(function()
    {
        dataFromAllServers("/api/study_cases/select_by_criteria_data", "", select_by_criteria);
    });


    var select_by_criteria = function(data)
    {
        createCriteriaAccordion(data, '#criteriaTabContent');
        $('#criteriaSpinner').hide();
        checkCriteriaPlatforms();
    };

    function createCriteriaAccordion(criteriaData, divId, guidelineParent = "") {
        var row1 = $('<div/>', { class: 'row' }).appendTo(divId);
            var col1 = $('<div/>', { class: 'col-12', style: "width:100%" }).appendTo(row1);

                // Accordion
                jQuery.each(criteriaData, function(key, study_case_guideline) 
                {
                    var case_index = "_" + study_case_guideline.serverid + "_" + study_case_guideline.id;
                    var info = "";
                    var accordionDiv = $('<div/>', { id: 'accordion_' + case_index, class: 'accordion mt-1 accordion_server_id_' + study_case_guideline.serverid }).appendTo(col1);
                        var cardDiv = $('<div/>', { class: 'card border-bottom', style: 'background-color: transparent;' }).appendTo(accordionDiv);
                            // Card Header
                            var cardHeaderDiv = $('<div/>', { class: ' p-0 m-0', id:"card_header_" + case_index }).appendTo(cardDiv); //card-header
                                var cardHeaderTable = $('<table/>', { class: 'width-100', style:"min-height: 55px;" }).appendTo(cardHeaderDiv);
                                    
                                    var cardHeaderTableRow = $('<tr/>', {   class: 'width-100 criteria-name', 
                                                                            style:"min-height: 55px;", 
                                                                            id: 'criteria' + case_index,
                                                                            title: info,
                                                                        }).data('criteria-id', study_case_guideline.id)
                                                                        .data('server-url', study_case_guideline.serverurl);
                                                                        
                                    if (guidelineParent != "") {
                                        console.log(guidelineParent);
                                        cardHeaderTableRow.data('parent', guidelineParent);
                                    }
                                    cardHeaderTableRow.appendTo(cardHeaderTable);
                                    
                                        // Server icon
                                        if (study_case_guideline.level == 0) {
                                            var cardHeaderTableRowServerIcon = $('<td/>', { class: 'align-middle pt-1', style:"width: 30px;" }).appendTo(cardHeaderTableRow);
                                            var cardHeaderTableRowServerIconDiv = $('<div/>', { class: 'round_button btn-smallest', 
                                                                                                style: 'display: inline-block; background-size: 100%; background-image: url(' + study_case_guideline.serverurl + '/images/server/logo.png)' })
                                                                                            .appendTo(cardHeaderTableRowServerIcon);
                                        }
                                        // Study case name 
                                        var cardHeaderStudyCaseName = $('<td/>', { class: 'pl-2 align-middle width-100 text-left', style: "min-height: 55px;"})
                                                                .html( study_case_guideline.name )
                                                                .appendTo(cardHeaderTableRow);

                                        // study case icon
                                        if (study_case_guideline.study_cases != "") 
                                        {
                                            jQuery.each(study_case_guideline.study_cases, function(key, study_case)
                                            {
                                                if (study_case.resources && key == 0)
                                                {
                                                    var studyCaseIcon = $('<td/>', { class: 'align-top pt-2 pr-2', style: "width: 40px;"})
                                                                .appendTo(cardHeaderTableRow);
                                                        var studyCaseIconDiv = $('<div/>', { class: 'round_button bck-study-case lens criteria-name', style: "display: inline-block;"})
                                                                .html("g")
                                                                .appendTo(studyCaseIcon);
                                                }
                                            });
                                        }
                                        // end study case icon
                                        
                                        // collapse header icon
                                        if (study_case_guideline.children != "") {
                                            var studyCaseColapseHeaderIcon = $('<td/>', { class: 'align-middle pr-2 text-center', style: "width: 20px;"})
                                                                                .appendTo(cardHeaderTableRow);
                                                var studyCaseColapseHeaderIconA = $('<a/>', {   class: 'collapse_arrow', 
                                                                                                style: "width: 20px;", 
                                                                                                //href: "#collapse" + case_index,
                                                                                                id : 'a' + case_index
                                                                                            })
                                                                                    .data('toggle', 'collapse')
                                                                                    .data('target', "#collapse" + case_index)
                                                                                    .attr("aria-controls","collapse" + case_index)
                                                                                    .attr("aria-expanded","true")
                                                                                    .data('parent', '#card_header_' + case_index)
                                                                                    .html('<i class="fa fa-caret-down fa-lg arrow" aria-hidden="true"></i>')
                                                                                    .appendTo(studyCaseColapseHeaderIcon);
                                        }
                                        // end collapse header icon
                        
                        // Card Collapse
                        var cardCollapse = $('<div/>', { id: 'collapse' + case_index,
                                                         class: 'collapse',
                                                        })
                                                    .attr("aria-labelledby","card_header_" + case_index)
                                                    .data("parent", "#accordion_" + case_index)
                                                    .appendTo(cardDiv);
                        // END Card Collapse

                            // Card body
                            var cardCollapseBody = $('<div/>', { class: 'card-body p-0 pl-3 m-0'}).appendTo(cardCollapse);
                                // Card case study div
                                var cardCollapseChildren = $('<div/>', { id: 'case_studies_' + case_index}).appendTo(cardCollapseBody);
                                
                                // CHILD CRITERIA
                                if (study_case_guideline.children) {
                                    createCriteriaAccordion(study_case_guideline.children, "#" + cardCollapseChildren.attr('id'), 'criteria' + case_index);
                                }
                });
                // END FOREACH
   
    }

    function odd_even(num) { 
        if ( (num % 2) == 1 ) {
            return "even";
        } else {
            return "odd";
        }
    }

    $('body').on('click', '.collapse_arrow', function(){
        $( $(this).data('target') ).collapse('toggle');
    });

    $('body').on('click', '.criteria-name', function(){
        $(".criteria-name").removeClass("criteria-selected");
        $(".criteria-name").removeClass("criteria-parent-selected");
        $(this).addClass("criteria-selected");
        var criteria_tab_url = $(this).data("server-url") + "/api/study_cases/criteria_tab?data=" + $(this).data('criteria-id');
        var data = $(this).data('criteria-id');
        selectCriteriaParents( $(this).data('parent') );
        populateDivFromApiCall(criteria_tab_url, '#criteriaTabFloat', "GET", data, realign_load);
        //ajax_post_to_div(criteria_tab_url, 'criteria_tab', data, realign_load);
    }); 

    function selectCriteriaParents(parent_id) {
        var parent = $('#' + parent_id);
        parent.addClass("criteria-parent-selected");
        if (parent.data('parent')) {
            selectCriteriaParents(parent.data('parent'));
        }
    }


    var realign_load = function() {

        var top_position = $(".criteria-selected:first").offset().top - 217;
        $(".select_by_criteria_tab").css({ top: top_position + 'px' });

        // Set the height of the main div
        var tab_pane_height = $(".select_by_criteria_tab_pane").first().height();
        if ( tab_pane_height > $("#select_by_criteria").height() ) 
        {
            $("#select_by_criteria").height(tab_pane_height + 150);
        }
    }   

</script> 