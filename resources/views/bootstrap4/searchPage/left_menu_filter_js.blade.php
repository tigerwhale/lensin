<script>
	$(function() {

		/**
		* Check if each resource switch is checked and click the mobile switch
		*/
		$('.resources:checked').each( function (){
			var resourceName = $(this).attr('id');
			$('#' + resourceName + '_mobile').click(); 
		});

		/**
		* Desktop resource switch clicked
		*/
        $('body').on('click', '.resources', function() {
            var resourceName = $(this).attr('id');
            var isChecked = this.checked;

            checkDatatableFilters();
            advancedResearchSelectedResourcesDisablesFilters();

            $('#' + resourceName + '_mobile').prop('checked', isChecked);
            setTimeout(function() {
				datatableTable.draw();
			}, 100);
        });
		
		/**
		* Mobile resource switch click
		* trigger the click on the desktop switch
		*/
		$('.resource-mobile-switch').click(function() {
			var resourceName = $(this).data('resource');
			$('#' + resourceName + '_switch').click();
		});
	});
</script>
