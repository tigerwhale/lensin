<?php 
	$row_index = 1;
	print_r($study_case_guidelines);
	die();
?>
<div class="row"> 
    <div class="col-12" id="select_by_criteria_div" style="width:100%">

	    @foreach($study_case_guidelines as $study_case_guideline)
			<?php 
				$display_criteria = true;
				$case_index = "_" . $study_case_guideline->serverid . "_" . $study_case_guideline->id; 
				$row_index = $row_index + 1;
				$info = $study_case_guideline->languagename ? (trans('text.language') . ": " . $study_case_guideline->languagename . "<br>") : "";
				$info .= isset($study_case_guideline->author) ? (trans('text.author') . ": " . $study_case_guideline->author . " <br>" ) : "";
				$info .= isset($study_case_guideline->year) ? (trans('text.year') . ": " . $study_case_guideline->year . " <br> " ) : "";
				$info .= isset($study_case_guideline->institution) ? (trans('text.institution') . ": " . $study_case_guideline->institution . " <br> " ) : "";
				if (isset($study_case_guideline->licences)) {
					if (count($study_case_guideline->licences) > 0) {
						$counter = 1;
						$count = count($study_case_guideline->licences);
						$info .= trans('text.licence') . ": ";
						foreach ($study_case_guideline->licences as $licence) {
							$info .= $licence->icon;
							if ($counter == $count) continue; 
							$info .= " ";
		    				$counter++;
						}
						$info .= " <br>";
					}
				}
			
				$info = $info ? $info : trans('text.no_info');
			?>
			<div id="accordion_{!! $case_index !!}">				
				<div class="card border-0" style="background-color: transparent;">
					<div class="card-header p-0 m-0" id="card_header_{!! $case_index !!}" style="">

						<table class="width-100 {!! odd_even($row_index) !!}" style="min-height: 55px;">
							<tr style="width: 100%; min-height: 55px;" id="criteria{!! $case_index !!}"  title="{{ $info }}"
								@if ( isset($guideline_parent) )
									data-parent="criteria_{{ $study_case_guideline->serverid }}_{{ $guideline_parent->id }}" 
								@endif 
								class="criteria-name" data-criteria-id="{{ $study_case_guideline->id }}" data-server-url="{{ $study_case_guideline->serverurl }}"
								>
								<!-- study case server icon-->
								@if ($study_case_guideline->level == 0)
									<td style="width: 30px; vertical-align: middle; padding-top: 4px;">
										<div class="round_button btn-smallest" style="display: inline-block; background-size: 100%; background-image: url({!! $study_case_guideline->serverurl . '/images/server/logo.png' !!})"></div>
									</td>
								@endif
								<!-- study case name-->
								<td class="pl-2 align-middle width-100 text-left" style="min-height: 55px;">
									{{ $study_case_guideline->name }} 
								</td>
								<!-- study case icon-->
								@if (isset($study_case_guideline->study_cases))
									@foreach($study_case_guideline->study_cases as $study_case)
										@if (isset($study_case->resources) && $loop->first)
											<td class="align-top pr-2" style="width: 40px;">
												<div class="round_button bck-study-case lens criteria-name" style="display: inline-block;">g</div>
											</td>
										@endif
									@endforeach
								@endif 

								<!-- collapse header icon-->	
								@if (isset($study_case_guideline->children))
									@if ($study_case_guideline->children)
										<td class="align-middle pr-2 text-center" style="width: 20px;">
											<a class="collapse_arrow" data-toggle="collapse" 
											data-parent="#card_header_{!! $case_index !!}" href="#collapse{!! $case_index !!}">
												<i class="fa fa-caret-down fa-lg arrow" aria-hidden="true"></i>
											</a>
										</td>
									@endif
								@endif							
							</tr>
						</table>	
					</div>

					<div id="collapse{!! $case_index !!}" class="collapse" aria-labelledby="card_header_{!! $case_index !!}" data-parent="#accordion_{!! $case_index !!}">
						<div class="card-body p-0 pl-5 m-0">
							<div id="case_studies_{!! $case_index !!}"></div>
							@if(isset($study_case_guideline->children))
								@if ($study_case_guideline->children)
									@include('bootstrap4.searchPage.view_criteria_tab', ['study_case_guidelines' => $study_case_guideline->children, 'guideline_parent' => $study_case_guideline])
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>		
		@endforeach

		@if (!isset($display_criteria)) 
			<div class="text-center" style="width:100%; margin-left: 125px;">
				<div class="no_cases_badge resource_badge"></div>
				<br>
				{{ trans('text.no_criteria') }}
			</div>
		@endif 

    </div> 
</div>   