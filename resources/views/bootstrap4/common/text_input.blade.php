<?php
/**
 * $model, $row, $updateURL, $options
 */
?>

<div class='row form-group'>
    <div class="col-12">
        <label for="name">{{ trans('text.' . $row) }}</label>
        <input  type="text"
                name="{!! $row !!}"
                data-id="{!! $model->id !!}"
                value="{!! $model->$row !!}"
                @if ($updateURL)
                    data-model="{!! $updateURL !!}" class="form-control update_input"
                @else
                    class="form-control"
                @endif

                @if (!isset($options['type']))
                    type='text'
                @endif

                @if (!isset($options['placeholder']))
                    placeholder='{!! trans('text.insert_' . $row) !!}'
                @endif

                @foreach ($options as $key => $value)
                    {!! $key !!}="{!!  $value !!}"
                @endforeach
        >
    </div>
</div>
