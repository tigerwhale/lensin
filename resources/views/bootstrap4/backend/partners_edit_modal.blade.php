<?php 
    $countries = \App\Country::all()->pluck('name', 'id');
    $partnersGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
?>
<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header" id='main_modal_header'>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>{{ trans('text.modify') }}</h4>
		</div>
		<div class="modal-body" id="main_modal_body">
			<div class="row">

				<!-- LEFT PART - IMAGE -->
				<div class="col-3">
					
					<!-- cover image -->
					<div class="row padding-small text-center">
						{{ strtoupper(trans('text.cover_image')) }}
						<div class="col-12" id="cover_image_container_{!! $partnersgroup->id !!}">
							{!! coverImageEdit($partnersgroup) !!}
						</div>
					</div>	

				</div>


				<!-- RIGHT PART - CONTENT -->
				<div class="col-9">

					<!-- NAME -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.title')) }}
						</div>
						<div class="col-12">
							<input type="text" name="title" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- description -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.description')) }}
						</div>
						<div class="col-12">
							<input type="text" name="description" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->description }}" placeholder="{{ strtoupper(trans('text.description')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- reference_person -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.reference_person')) }}
						</div>
						<div class="col-12">
							<input type="text" name="reference_person" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->reference_person }}" placeholder="{{ strtoupper(trans('text.reference_person')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- reference_person_email -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.reference_person_email')) }}
						</div>
						<div class="col-12">
							<input type="text" name="reference_person_email" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->reference_person_email }}" placeholder="{{ strtoupper(trans('text.reference_person_email')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- address -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.address')) }}
						</div>
						<div class="col-12">
							<input type="text" name="address" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->address }}" placeholder="{{ strtoupper(trans('text.address')) }}" class="form-control update_input" required>
						</div>
					</div>
					
					<!-- web_site -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.web_site')) }}
						</div>
						<div class="col-12">
							<input type="text" name="web_site" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->web_site }}" placeholder="{{ strtoupper(trans('text.web_site')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- web_site -->
					<div class="row" style="margin-top: 20px">
						<div class="col-12">
							{{ strtoupper(trans('text.email')) }}
						</div>
						<div class="col-12">
							<input type="text" name="email" data-model="partnersgroup" data-id="{!! $partnersgroup->id !!}" value="{{ $partnersgroup->email }}" placeholder="{{ strtoupper(trans('text.email')) }}" class="form-control update_input" required>
						</div>
					</div>

			        <!-- Country --> 
			        <div class="row top-padding-small">
			            <div class="col-12">
			                {{ trans('text.country') }}
			            </div>
			            <div class="col-12">
			                {{ Form::select('country_id', $countries , $partnersgroup->country_id, ['class' => 'form-control update_input', 'data-model' => "partnersgroup", 'data-id' => $partnersgroup->id, 'placeholder' => trans('text.select_country')]) }}
			            </div>
			        </div>

			        <!-- type --> 
			        <div class="row top-padding-small">
			            <div class="col-12">
			                {{ trans('text.type') }}
			            </div>
			            <div class="col-12">
			                {{ Form::select('type', $partnersGroupTypes , $partnersgroup->type, ['class' => 'form-control update_input', 'data-model' => "partnersgroup", 'data-id' => $partnersgroup->id, 'id'=> 'partnersgroup']) }}
			            </div>
			        </div>
				</div>

			</div>
		</div>
	</div>
</div>

