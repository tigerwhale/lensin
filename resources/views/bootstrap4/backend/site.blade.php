@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

    <!-- TITLE -->
        <h4 class="text-center mt-3">{{ trans('backend.platform_properties') }}</h4>

        <div class="row list-section">

            <!-- TITLE -->
            <div class="col-md-6 col-12">
                <?php $platform_title = server_property('platform_title'); ?>
                <form action="/backend/general" method="POST">
                    {{ csrf_field() }}
                    <label for="platform_title">{{ trans('text.platform_title') }}</label>
                    <div class="input-group">
                        <input type="text" name="data" class="form-control" value="{{ $platform_title }}">
                        <input type="hidden" name="name" value="platform_title">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                        </div>
                    </div>
                </form>
            </div>

            <!-- TEXT UNDER THE LOGO -->
            <div class="col-md-6 col-12">
                <?php $slogan = server_property('slogan'); ?>
                <form action="/backend/general" method="POST">
                    {{ csrf_field() }}

                    <label for="platform_title">{{ trans('text.slogan') }}</label>
                    <div class="input-group">
                        <input type="text" name="data" class="form-control" value="{{ $slogan }}">
                        <input type="hidden" name="name" value="slogan">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                        </div>
                    </div>
                </form>
            </div>

            <!-- SERVER LOGO -->
            <form action="/backend/upload/logo" class="mt-4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row py-2">
                    <div class="col-12 col-md-2 offset-md-2">
                        {{ trans('text.server_logo') }} {!! tooltip(trans('text.tooltip_server_logo')) !!}
                    </div>
                    <div class="col-12 col-md-2">
                        <img src="{!! logo() !!}?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                    </div>
                    <div class="col-8 col-md-2">
                        <input type="file" name="image">
                    </div>
                    <div class="col-4 col-md-2 text-right">
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                </div>
            </form>

            <!-- SERVER DARK LOGO -->
            <form action="/backend/upload/logo_dark" class="mt-4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="type" value="dark">
                <div class="row py-2">
                    <div class="col-12 col-md-2 offset-md-2">
                        {{ trans('text.mobile_server_logo') }} {!! tooltip(trans('text.tooltip_mobile_dark_logo')) !!}
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="p-2 bck-lens">
                            <img src="{!! logo(true) !!}" style="max-height: 40px; max-width: 200px;">
                        </div>
                    </div>
                    <div class="col-8 col-md-2">
                        <input type="file" name="image">
                    </div>
                    <div class="col-4 col-md-2 text-right">
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                </div>
            </form>

            <!-- SERVER ROUND LOGO -->
            <form action="/backend/upload/round_logo" class="mt-4" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row pt-2">
                    <div class="col-12 col-md-2 offset-md-2">
                        {{ trans('text.server_round_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo')) !!}
                    </div>
                    <div class="col-12 col-md-2">
                        <img src="/images/server/logo.png?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                    </div>
                    <div class="col-8 col-md-2">
                        <input type="file" name="image">
                    </div>
                    <div class="col-4 col-md-2 text-right">
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                </div>
            </form>

            <!-- SERVER ROUND LOGO TRANSPARENT -->
            <form action="/backend/upload/round_logo_transparent" class="mt-4" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row pt-2">
                    <div class="col-12 col-md-2 offset-md-2">
                        {{ trans('text.server_round_transparent_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo_transparent')) !!}
                    </div>
                    <div class="col-12 col-md-2">
                        <img style="background: #CCC; max-height: 40px; max-width: 200px;"
                             src="/images/server/logo_transparent.png?{!! time() !!}">
                    </div>
                    <div class="col-8 col-md-2">
                        <input type="file" name="image">
                    </div>
                    <div class="col-4 col-md-2 text-right">
                        <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                    </div>
                </div>
            </form>
        </div>


        <!-- HOME PAGE VIDEO -->
        <h4>{{ trans('text.home_page_video') }}</h4>


        <!-- preview -->
        <div class="row list-section">
            <div class="col-12">
                {!! homePageVideo() !!}
            </div>

            <!-- video upload -->
            <div class="col-md-6 col-12">
                <form class="form-inline" action="/backend/upload_home_video" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="file" name="home_video" class="inline">
                    <button type="submit" class="btn btn-success pull-right">{{ trans('text.upload') }}</button>
                </form>
            </div>
            <div class="col-2"></div>
            <div class="col-md-3 col-12">
                <form action="/backend/defaut_home_video" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <button type="submit"
                            class="btn btn-info pull-right">{{ trans('text.return_to_default') }}</button>
                </form>
            </div>
            <div class="col-md-1 col-12">
                <form action="/backend/delete_home_video" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger pull-right">{{ trans('text.delete') }}</button>
                </form>
            </div>

            <!-- video poster -->
            <div class="row text-center mt-5">
                <div class="col-12">
                    <h4>{{ trans('text.home_page_video_thumbnail') }}
                </div>

                <!-- show video poster -->
                @if (server_property('home_video_poster'))
                    <div class="col-md-8 offset-md-2 col-12 text-center padding-small">
                        <img src="{!! server_property('home_video_poster') !!}" class="image">
                    </div>
                @else
                    <div class="col-12">
                        {{ trans('text.no_video_poster') }}
                    </div>
            @endif

            <!-- upload video poster -->
                <div class="col-md-6 col-12 text-left">
                    <form action="/backend/upload_home_video_poster" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" name="home_video_poster" class="inline">
                        <button type="submit" class="btn btn-success pull-right">{{ trans('text.upload') }}</button>
                    </form>
                </div>

                <div class="col-md-6 col-4">
                    <form action="/backend/delete_home_video_poster" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger pull-right">{{ trans('text.delete') }}</button>
                    </form>
                </div>
            </div>
        </div>

        <!-- CENTRAL SERVER PROPERTIES -->
        <h4 class="mt-5">{{ trans('backend.central_server_properties') }}</h4>

        <!-- CENTRAL SERVER ADDRESS -->
        <form action="/backend/general" method="POST" class="list-section">
            {{ csrf_field() }}

            <label for="data">{{ trans('text.central_server_address') }}</label>
            <div class="input-group">
                <input type="text" name="data" class="form-control" value="{{ server_property('central_server_address') }}">
                <input type="hidden" name="name" value="central_server_address">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>

        <!-- REGISTRER ON CENTRAL SERVER -->
        @if (!server_property('central_server'))
            <div class="row">
                <div class="col-8">
                    @if (server_property('server_id'))
                        {{ trans('text.server_id') }}: {!! server_property('server_id') !!}
                    @else
                        {{ trans('backend.you_need_to_register_your_platform_on_central_server') }}
                    @endif
                </div>
                <div class="col-4 text-right">
                    <a href="/backend/register_platform_request">
                        <button type="button" class="btn btn-success">{{ trans('backend.register_platform_on_central_server') }}</button>
                    </a>
                </div>
            </div>
        @endif

        <!-- CHANGE LOG -->
        <div class="row mt-5">
            <div class="col-12 text-center">
                <h4>{{ trans('backend.changelog') }}</h4>
            </div>
        </div>

        <div class="row padding-small">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="changelog">
                    {!! nl2br(file_get_contents(public_path("changelog.txt"))) !!}
                </div>
            </div>
        </div>

    </div>

@endsection
