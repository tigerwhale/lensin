@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('text.new_network_requests') }}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center mt-4">
                <!-- DATATABLE USERS -->
			    <table class="display row-links" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.name') }}</th>
			                <th>{{ trans('text.reference_person') }}</th>
                            <th>{{ trans('text.description') }}</th>
                            <th>{{ trans('text.date') }}</th>
                            <th>{{ trans('text.resolved') }}</th>
			                <th class="text-right"></th>
			            </tr>
			        </thead>
			    </table>
			</div>
		</div>
	</div>

    <script type="text/javascript">

    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/newnetwork/datatable_list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "name" },
                { "data": "reference_person" },
                { "data": "description" },
                { "data": "created_at" },
                { "data": "resolved" },
            ],
             "createdRow": function( row, data, dataIndex){
                if( data['resolved'] == 1){
                    $(row).addClass('resolved');
                } else {
                    $(row).addClass('unresolved');
                }
            },
            "aaSorting": [],
            "orderClasses": false,
            "aoColumnDefs": [
                {
                "mRender": function ( data, type, row ) {

                    var view_request = '<div class="btn bck-lens modal_url" data-url="/backend/newnetwork/view_modal/' + row['id'] + '" data-close-function="reloadTable"><i class="fa fa-search" aria-hidden="true"></i></div></a> ';
                    //var publish = publishButton(row['resolved'], 'newnetwork', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                    var delete_button = '<div class="btn btn-danger delete_request" title="{{ trans('text.delete') }}" data-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                    return view_request + delete_button;
                },
                "aTargets": [ 5 ]
            }],   
        });

        // Reload datatable
        function reloadTable() {
            $("#datatable").DataTable().ajax.reload();
        }

        
        $('body').on('click', '.close_and_mark_as_resolved', function(){
            var newnetwork_id = $(this).data('id');
            var token = $("meta[name='csrf-token']").attr("content"); 
            $('.modal').modal('hide');
            
            $.post({
                url: '/newnetworks/resolve/' + newnetwork_id,
                data: {
                    '_token': token,
                },
                success: function(data, status) {
                    reloadTable();
                },
                error: function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            })
        });



        // Delete new network
        $('body').on('click', '.delete_request', function(){

            var token = $("meta[name='csrf-token']").attr("content"); 
            var newnetwork_id = $(this).data('id');
            var text = "{{ trans('text.delete_question') }}";

            bootbox.confirm(text, function(result){ 
                // if OK, delete the model
                if (result) {
                    $.post({
                        url: '/newnetworks/delete/' + newnetwork_id,
                        data: {
                            '_token': token,
                        },
                        success: function(data, status) {
                            reloadTable();
                        },
                        error: function(xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    })
                }
            });
        });

    </script>    
@endsection
