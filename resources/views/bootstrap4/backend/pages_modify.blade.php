@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('backend.pages_list') }}</h4>
            </div>
        </div>

        <!-- PAGES LIST -->
        <div class="row">
            <div class="col-12 text-center mt-4">
                <!-- DATATABLE SEARCH -->
                <table class="display" id="datatable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{ trans('text.title') }}</th>
                            <th>{{ trans('pages.page_name') }}</th>
                            <th>{{ trans('text.author') }}</th>
                            <th style="min-width: 120px;"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

{{--        <!-- NETWORKS -->--}}
{{--        <div class="row">--}}
{{--            <div class="col-12 text-center mt-5">--}}
{{--                <h3>{{ trans('text.network') }}</h3>--}}
{{--                <div style="width: 100%;" id="networks_edit">--}}
{{--                    @include('bootstrap4.backend.networks')--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>  --}}

	</div>

<script type="text/javascript">
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/pages/datatable_list',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "permalink" },
            { "data": "author" }
        ], 

        "aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {

                var modify = '<a title="{{ trans('text.modify') }}" href="/backend/pages/edit/' + row['id'] + '"><div class="btn bck-lens"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                var publish = publishButton(row['published'], 'page', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                var delete_button = '<div class="btn btn-danger delete_page" title="{{ trans('text.delete') }}" data-page-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                return modify + publish + delete_button;
            },
            "aTargets": [ 4 ]
        }]

    });

</script>

@endsection
