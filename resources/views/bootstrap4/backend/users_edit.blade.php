@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('text.users') }}</h4>
            </div>
        </div>

        <div class="row mt-4">
            <!-- USER INFORMATION -->
            <div class="col-12 col-md-6 mb-4">

                <h4>{{ trans('text.general_information') }}</h4>
                <div class="row">
                    <div class="col-12">

                        <label for="name">{{ trans('text.name') }}</label>
                        <input  type="text" name="name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->name }}" placeholder="{{ trans('text.insert_name') }}" class="form-control update_input" required="">

                        <label for="last_name">{{ trans('text.last_name') }}</label>
                        <input  type="text" name="last_name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->last_name }}" placeholder="{{ trans('text.insert_last_name') }}" class="form-control update_input" required="">

                        <label for="last_name">{{ trans('text.user_type') }}</label>
                        {!! Form::select('user_type_id', $user_types, $user->user_type_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}

                        <label for="school">{{ trans('text.school') }}</label>
                        <input  type="text" name="school" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->school }}" placeholder="{{ trans('text.insert_school') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="departement">{{ trans('text.departement') }}</label>
                        <input  type="text" name="departement" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->departement }}" placeholder="{{ trans('text.insert_departement') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="position">{{ trans('text.position') }}</label>
                        <input  type="text" name="position" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->position }}" placeholder="{{ trans('text.insert_position') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="address">{{ trans('text.address') }}</label>
                        <input  type="text" name="address" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->address }}" placeholder="{{ trans('text.insert_address') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="country">{{ trans('text.country') }}</label>
                        {!! Form::select('country_id', $countries, $user->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}

                        <label for="interest">{{ trans('text.interest') }}</label>
                        <input  type="text" name="interest" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->interest }}" placeholder="{{ trans('text.insert_interest') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="phone">{{ trans('text.phone') }}</label>
                        <input  type="text" name="phone" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->phone }}" placeholder="{{ trans('text.insert_phone') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="web">{{ trans('text.web') }}</label>
                        <input  type="text" name="web" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->web }}" placeholder="{{ trans('text.insert_web') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                        <label for="email">{{ trans('text.email') }}</label>
                        <input  type="email" name="email" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->email }}" placeholder="{{ trans('text.insert_email') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">

                    </div>
                </div>
            </div>

            <!-- USER PRIVILEDGES -->
            <div class="col-12 col-md-3">
                <h4>{{ trans('text.user_priviledges') }}</h4>
                <div class="row">
                    <div class="col-12">
                        @foreach ($user_priviledges as $user_priviledge)
                            @if (server_property('central_server') != 1 && $user_priviledge->slug == "manageservers")
                            @else 
                                <div class="row">
                                    <div class="col-12">
                                        <label>
                                            <input type="checkbox" name="user_roles" data-url="/backend/user_roles/update/{{ $user->id }}" data-id="{{ $user_priviledge->id }}" value="" placeholder="{{ trans('text.insert_name') }}" class="update_checkbox" required="" style="background-color: rgb(255, 255, 255);" {!! $user->hasRole($user_priviledge->slug) ? 'checked' : '' !!} >
                                            {{ trans('text.' . $user_priviledge->slug) }}
                                        </label>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <!-- USER IMAGE -->
            <div class="col-12 col-md-3" style="padding-top: 20px;">
                <h4>User Image</h4>
                {!! coverImageEdit($user) !!}
            </div>
        </div>
	</div>

    <script type="text/javascript">
    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/users/list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "name" },
                { "data": "last_name" },
                { "data": "email" },
            ]
        });

        // Open course on click     
        $('#datatable tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            location.href = '/backend/users/edit/' + data['id'];
        });
    </script>    
@endsection
