@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('text.users') }}</h4>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-12 text-center">
                <!-- DATATABLE USERS -->
			    <table class="display" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.name') }}</th>
			                <th>{{ trans('text.last_name') }}</th>
                            <th>{{ trans('text.email') }}</th>
			                <th></th>
			            </tr>
			        </thead>
			    </table>
            </div>
		</div>
	</div>

    <script type="text/javascript">
    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/users/list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "name" },
                { "data": "last_name" },
                { "data": "email" },
            ],
            "aoColumnDefs": [
                {
                    "mRender": function ( data, type, row ) {
                        var edit_user_button = "<a href='/backend/users/edit/" + row['id'] + "'><button class='btn bck-lens' type='button' title='{{ trans('text.edit') }}'><i class='fas fa-pencil-alt' aria-hidden='true'></i></button></a> ";
                        var suspend_user_button = "<a href='/backend/users/suspend/" + row['id'] + "'><button class='btn btn-warning' type='button' title='{{ trans('users.suspend_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a> ";
                        var delete_user_button = "<a href='/backend/users/delete/" + row['id'] + "'> <button class='btn btn-danger' type='button' title='{{ trans('users.delete_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a>";
                        return edit_user_button + " " + suspend_user_button + " " + delete_user_button;
                    },  
                    "aTargets": [ 3 ]
                },
            ]        
        });

    </script>    
@endsection
