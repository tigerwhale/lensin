@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')
    {{ csrf_field() }}
    <div class="container mt-3">
        @include('bootstrap4.backend.nav_bar')

        <div class="row">
            <div class="col-2 mt-3"></div>
            <div class="col-8 text-center">
                <h4>News list</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center mt-3">
                <!-- DATATABLE USERS -->
                <table class="display text-left" id="datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('text.title') }}</th>
                        <th>{{ trans('text.author') }}</th>
                        <th class="text-right" style="min-width:140px;"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <!-- LOCAL SERVER NEWS ON CENTRAL -->
        @if(server_property("central_server") == 1)
            <div class="row mt-3">
                <div class="col-2" style="padding-top: 20px;"></div>
                <div class="col-8 text-center">
                    <h4>News from local servers</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-2" style="padding-top: 20px;"></div>
                <div class="col-8 text-center">
                    @foreach ($server_list as $local_server)
                        <button class="btn bck-lens load-local-news mt-3 transparent" type="button"
                                data-server-url="{{ $local_server->address }}">{{ $local_server->name }}</button>
                    @endforeach
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12 text-center" id="local_server_news">
                </div>
            </div>
        @endif

    </div>


    <script type="text/javascript">

        var forbiddenNews = [];

        var getForbiddenNews = function getForbiddenNews() {
            $.get({
                url: '/api/news/forbidden_news_list',
                success: function (data, status) {
                    forbiddenNews = data;
                    changeForbiddenNewsButtons(forbiddenNews);
                },
                error: function (xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                    var errorDebug = eval("(" + xhr.responseText + ")");
                },
            });
        }

        function changeForbiddenNewsButtons(forbiddenNews) {
            forbiddenNews.forEach(function (element_id) {
                $("body #" + element_id).addClass('transparent');
                $("body #" + element_id + " i").addClass('fa-eye-slash').removeClass('fa-eye');
            });
        }

        $('body').on('click', '.load-local-news', function () {
            $this = $(this);
            var url = $this.data('server-url') + '/api/backend/news_list';
            $('.load-local-news').addClass('transparent');
            $this.removeClass('transparent');
            populateDivFromApiCall(url, "#local_server_news", "GET", [], getForbiddenNews);
        });

        $('body').on('click', '.publish_news_central', function () {
            $this = $(this);
            var url_path = '/api/backend/publish_news_central/' + $this.data('news-id') + "/" + $this.data('server-id');

            $.post({
                url: url_path,
                success: function (data, status) {
                    var url = $this.data('server-url') + '/api/backend/news_list';
                    ajax_post_to_div(url, 'local_server_news', null, getForbiddenNews);
                },
                error: function (xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                    var errorDebug = eval("(" + xhr.responseText + ")");
                },
            });
        });

        $('body').on('click', '.js-publish-news', function () {
            $("#datatable").DataTable().ajax.reload();
        });

        var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true,
            "dom": '<"top"lf><"toolbar">rt<"bottom"ip><"clear">',
            "ajax": {
                "cache": true,
                "url": '/news/datatable_list',
                "rowId": 'id',
            },
            "columns": [
                {"data": "title"},
                {"data": "author"},
                {"data": "published"},
            ],
            "aoColumnDefs": [
                {
                    "mRender": function (data, type, row) {
                        var modify_button = '<div class="btn bck-lens modal_url" data-url="/backend/news/edit_modal/' + row['id'] + '" data-close-function="reloadTable"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div> ';
                        var publish_button = publishButton(row['published'], 'news', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}");
                        var global_button = globalButton(row['global'], 'news', row['id'], "{{ trans('text.global_click_to_make_local') }}", "{{ trans('text.local_click_to_make_global') }}");
                        //var delete_user_button = "<a href='/backend/users/delete/" + row['id'] + "'> <button class='btn btn-danger' type='button' title='{{ trans('users.delete_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a>";
                        return modify_button + " " + publish_button + " " + global_button;
                    },
                    "aTargets": [-1]
                },
            ]
        });

        $("div.toolbar").html('<button type="button" class="btn btn-success modal_url" data-url="/backend/news/create_modal" title="{{ trans('text.create') }}" data-close-function="reloadTable"><i class="fa fa-plus" aria-hidden="true"></i></button>');

        function reloadTable() {
            $('.modal').modal('hide');
            $("#datatable").DataTable().ajax.reload();
        };

    </script>
@endsection
