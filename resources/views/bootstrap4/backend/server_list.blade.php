@extends('bootstrap4.layouts.frontend')

@section('head')
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

    <!-- ADD A NEW SERVER -->
        <div class="row mt-4 pb-4 border-bottom">
            <div class="col-12">
                <h4>{{ (trans('text.add_new_server')) }} </h4>

                <section class="list-section-flex">
                    <form action="/backend/servers/create" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">{{ trans('text.name') }}</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label for="address">{{ trans('text.address') }}</label>
                                <input type="text" name="address" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label for="administrator_contact">{{ trans('text.administrator_contact') }}</label>
                                <input type="text" name="administrator_contact" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label for="notes">{{ trans('text.notes') }}</label>
                                <input type="text" name="notes" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <label for="enabled">{{ trans('text.enabled') }}</label>
                                <br>
                                <input type="checkbox" name="enabled" value="1">
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button type="submit" class="btn btn-success align-self-end">{{ trans('text.create') }}</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>

        <!-- LIST SERVERS -->
        <div class="row mt-4">
            <div class="col-12">
                <h4 class="text-center">{{ (trans('text.server_list')) }}</h4>

                @foreach($server_list as $server)
                    <section class="list-section-flex">
                        <form action="/backend/servers/update/{!! $server->id !!}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">{{ trans('text.name') }}</label>
                                    <input type="text" name="name" value="{{ $server->name }}" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="address">{{ trans('text.address') }}</label>
                                    <input type="text" name="address" value="{{ $server->address }}" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="administrator_contact">{{ trans('text.administrator_contact') }}</label>
                                    <input type="text" name="administrator_contact"
                                           value="{{ $server->administrator_contact }}" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="notes">{{ trans('text.notes') }}</label>
                                    <input type="text" name="notes" value="{{ $server->notes }}" class="form-control">
                                </div>

                            </div>
                            <div class="row mt-4">
                                <div class="col-6  text-left">
                                    <label for="enabled">{{ trans('text.enabled') }} / ID</label>
                                    <br>
                                    <input type="checkbox" name="enabled" {{ $server->enabled ? "checked" : ""}}>
                                    {!! $server->id !!}
                                </div>
                                <div class="col-6 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success align-self-end">{{ trans('text.save') }}</button>
                                    <a href="/backend/servers/delete/{!! $server->id !!}" class="btn btn-danger align-self-end ml-2">
                                        {{ trans('text.delete') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </section>
                @endforeach

            </div>
        </div>


    </div>


@endsection

