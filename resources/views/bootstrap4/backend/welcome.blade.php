@extends('bootstrap4.layouts.frontend')

@section('head')
@endsection

@section('content')

<div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

    <div class="row">
        <!-- CONTENT -->
        <div class="col-12 text-center">
            <br>
            {{ trans('text.backend_welcome_text') }}
        </div>
    </div>
</div>

@endsection
