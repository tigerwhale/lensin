@extends('bootstrap4.layouts.frontend')

@section('head')
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('text.languages') }}</h4>
            </div>
        </div>

        <!-- LANGUAGE LIST -->
        <div class="row mt-4">
            <div class="col-12">

                <div id="accordion">
                    <div class="card">
                        <div class="card-header colapse_arrow" id="headingOne">
                            <button class="btn btn-link"  style="width: 100%;" data-toggle="collapse" data-target="#languageList" aria-expanded="true" aria-controls="languageList">
                                <h5 class="mb-0 text-lens text-left pull-left">{{ strtoupper(trans('languages.language_list')) }}</h5>
                                <i class="fa pull-right fa-lg fa-caret-down text-lens mt-2" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div id="languageList" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><small>{{ trans('text.published') }}</small></th>
                                        <th><small>{{ trans('languages.locale') }}</small></th>
                                        <th><small>{{ trans('languages.name') }}</small></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($languages as $language)
                                        <form action="/backend/languages/update/{!! $language->id !!}" method="POST">
                                            {{ csrf_field() }}
                                            <tr>
                                                <td class="text-center" style="width:80px;">
                                                    <input type="checkbox" name="published" {{ $language->published ? "checked" : ""}} value="1">
                                                </td>
                                                <td style="width: 100px;">
                                                    <input type="text" name="locale" value="{{ $language->locale }}" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" name="name" value="{{ $language->name }}" class="form-control">
                                                </td>
                                                <td style="text-align: right">
                                                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                                                    <a href="/backend/languages/delete/{!! $language->id !!}"><button type="button" class="btn btn-danger">{{ trans('text.delete') }}</button></a>
                                                    <a href="/backend/translations/{!! $language->id !!}"><button type="button" class="btn btn-info">{{ trans('text.manage') }}</button></a>
                                                    <button type="button" class="btn btn-info distribute" data-locale="{!! $language->locale !!}">{{ trans('text.distribute') }}</button>
                                                </td>
                                            </tr>
                                        </form>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <!-- ADD A NEW LANGUAGE -->
        <div class="row">
            <div class="col-12">

                <div id="accordion2">
                    <div class="card">
                        <div class="card-header colapse_arrow" id="headingOne">
                            <button class="btn btn-link"  style="width: 100%;" data-toggle="collapse" data-target="#newLanguage" aria-expanded="true" aria-controls="newLanguage">
                                <h5 class="mb-0 text-lens text-left pull-left">{{ strtoupper(trans('languages.add_new_language')) }}</h5>
                                <i class="fa pull-right fa-lg fa-caret-down text-lens mt-2" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div id="newLanguage" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion2">
                            <div class="card-body">
                                {{ trans('languages.add_new_language_directions') }}
                                <form action="/backend/languages/create" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <!-- locale -->
                                        <div class="col-2">
                                            <small>{{ trans('languages.locale') }}</small><br>
                                            <input type="text" name="locale"  class="form-control" required maxlength="2">
                                        </div>
                                        <!-- name -->
                                        <div class="col-8">
                                            <small>{{ trans('languages.name') }}</small><br>
                                            <input type="text" name="name" class="form-control" required>
                                        </div>
                                        <!-- create -->
                                        <div class="col-2">
                                            <br>
                                            <button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END ADD A NEW LANGUAGE -->

	</div>

<script type="text/javascript">

    // distribute languages across platforms
    $('.distribute').click(function (){
        
        // prepare the results array
        var results = "";
        var button = $(this);
        var locale = $(this).data('locale');
        var token = $("meta[name='csrf-token']").attr("content"); 
        //disable the button
        button.attr("disabled", true);
        button.html("<i class='fa fa-spinner fa-pulse fa-fw'></i> {{ trans('text.please_wait') }} ");

        // Get translations for this location
        $.post({
            url: '/backend/langauge/translation_array/' + locale,
            data: {
                '_token': token,
            },
            success: function(translation_data, status) {
                    
                // trigger pull on servers
                $.when(

                    @foreach($platforms as $platform)

                        // Send data to servers
                        $.post({
                            url: '{{ $platform->address }}/api/backend/synchronize',
                            data: {
                                '_token': token,
                                'locale': locale,
                                'translation_data': translation_data,
                            },
                            success: function(data, status) {
                                results += ('{{ $platform->address }} ' + data + " || ");
                            },
                            error: function(xhr, desc, err) {
                                results += ('{{ $platform->address }} ' + err + " || ");  
                                console.log(xhr);
                                console.log("Details: " + desc + "\nError:" + err);
                            }
                        })

                        @if (!$loop->last)
                            ,
                        @endif


                    @endforeach

                ).then(function() {
                    //display the results
                    button.html("{{ trans('text.distribute') }}");
                    button.removeAttr("disabled");
                    bootbox.alert(results);
                });
            }
        });
    });
</script>

@endsection
