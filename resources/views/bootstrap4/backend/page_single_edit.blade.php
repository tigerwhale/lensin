@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/tinymce/tinymce.min.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')
    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('backend.edit_page') }}</h4>
            </div>
        </div>

        <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center">
                <!-- Title -->
                <div class="row mb-2">
                    <div class="col-2 text-left">
                        {{ trans('text.title') }}
                    </div>
                    <div class="col-10">
                        <input type="text" name="name" data-model='pages' data-id={{ $page->id }} value="{{ $page->name }}" placeholder="Permalink" class="form-control update_input" required>
                    </div>
                </div>

                <!-- Permalink -->
                <div class="row">
                    <div class="col-2 text-left">
                        {{ trans('pages.page_name') }}
                    </div>
                    <div class="col-10">
                        <input type="text" name="permalink" data-model='pages' data-id={{ $page->id }} value="{{ $page->permalink }}" placeholder="{{ strtoupper(trans('pages.permalink')) }}" class="form-control update_input" required>
                    </div>
                </div>

                <!-- Texts -->
                @foreach($page->texts as $text)
                <form>
                    <div class="panel-group text-left" style="margin-top: 20px;">
                        <div class="panel panel-default">
                            <a data-toggle="collapse" href="#locale_{!! $text->locale !!}">
                                <div class="panel-heading colapse_arrow">
                                    <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" ></i>
                                    <h4 class="panel-title">
                                        {!! $text->language->name !!}
                                    </h4>
                                </div>
                            </a>

                            <div id="locale_{!! $text->locale !!}" class="panel-collapse collapse"  style="padding: 0px 15px 15px 15px;">
                                <h4>{{ trans('text.title') }}</h4>
                                <input type="text" id="edit_title_{!! $text->id !!}" name="title" data-model='page_texts' data-id={{ $text->id }} value="{{ $text->title }}" placeholder="{{ strtoupper(trans('pages.title')) }}" class="form-control" required>

                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-6">
                                        <h4>{{ trans('text.text') }}</h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div style="display: none;" id="saved">Saved...</div>
                                        <button class="btn btn-success save_text" data-id="{!! $text->id !!}" >{{ trans('text.save') }}</button>
                                    </div>
                                </div>

                                <textarea id="edit_text_{!! $text->id !!}" name="text" class="form-control" placeholder="{{ strtoupper(trans('text.text')) }}" data-model='page_texts' data-id={{ $text->id }}>{{ $text->text }}</textarea>
                                <br>

                            </div>
                        </div>
                    </div>
                </form>

                <script type="text/javascript">
                    tinymce.init({
                        selector: '#edit_text_{!! $text->id !!}',
                        menu: {},
                        forced_root_block : "",
                        branding: false,
                        plugins: "code",
                        toolbar: "bold italic underline  strikethrough | justifyleft justifycenter justifyright justifyfull | bullist numlist | outdent indent | cut copy paste | undo redo | link unlink | image cleanup removeformat formatselect fontsizeselect  styleselect | sub sup | charmap  blockquote code"
                    });
                </script>

                @endforeach
            </div>
        </div>


    </div>

    <script type="text/javascript">
        $('body').on('click', '.save_text', function(){

            var token = $("meta[name='csrf-token']").attr("content");
            var pageTextId = $(this).data('id');
            var title = $('#edit_title_' + pageTextId).val();
            var text = tinyMCE.get('edit_text_' + pageTextId).getContent();
            var button = $(this);
            button.prop("disabled", true);

            $.ajax({
                method: "POST",
                url: "/page_texts/update_data",
                data: {
                    '_token': token,
                    'id': pageTextId,
                    'name': "title",
                    'value': title
                },
                success: function(data, status) {

                },
                error: function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });

            $.ajax({
                method: "POST",
                url: "/page_texts/update_data",
                data: {
                    '_token': token,
                    'id': pageTextId,
                    'name': "text",
                    'value': text
                },
                success: function(data, status) {
                    button.prop("disabled", false);
                    $('#saved').css('display', 'inline');
                    $('#saved').delay(1000).fadeOut(500);
                },
                error: function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        });


        $('form').dirtyForms({
            message: '{{ trans('text.are_you_sure_you_want_to_leave_this_page') }}'
        });

    </script>

@endsection
