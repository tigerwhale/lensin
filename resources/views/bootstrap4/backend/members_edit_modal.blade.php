<?php 
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
    $updateURL = '';
    $redirectURL = $members_group->partner ? '/backend/partners' : '/backend/members';
?>
<div class="modal" tabindex="-1" role="dialog" id="apiModal">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">

			<div class="modal-header" id='main_modal_header'>
				<h4>{{ trans('text.modify') }}</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body" id="main_modal_body">
				<div class="row">
					<!-- LEFT PART - COVER IMAGE -->
					<div class="col-3">
						{{ strtoupper(trans('text.cover_image')) }}
						<div id="cover_image_container_{!! $members_group->id !!}">
							{!! coverImageEdit($members_group) !!}
						</div>
					</div>

					<!-- RIGHT PART - CONTENT -->
					<div class="col-9">
						<form method="post" action="/backend/members/update/{!! $members_group->id !!}">
							{!! csrf_field() !!}
							<input type="hidden" value="{!! $redirectURL !!}" name="redirect_page">
							{!! updateTextInput($members_group, 'title', $updateURL, []) !!}
							<div class='row form-group'>
								<div class="col-12">
									<label for="description">{{ trans('text.description') }}</label>
									<textarea name="description" class="form-control" rows="8">{{ $members_group->description }}</textarea>
								</div>
							</div>
							{!! updateTextInput($members_group, 'reference_person', $updateURL, []) !!}
							{!! updateTextInput($members_group, 'reference_person_email', $updateURL, []) !!}
							{!! updateTextInput($members_group, 'address', $updateURL, []) !!}
							{!! updateTextInput($members_group, 'web_site', $updateURL, []) !!}
							{!! updateTextInput($members_group, 'email', $updateURL, []) !!}

							<!-- Country -->
							<div class="row form-group">
								<div class="col-12">
									<label>{!! trans('text.country') !!}</label>
									{!! Form::select('country_id', $countries , $members_group->country_id, ['class' => 'form-control update_input',  'placeholder' => trans('text.select_country')]) !!}
								</div>
							</div>

							<!-- type -->
							<div class="row form-group">
								<div class="col-12">
									<label>{{ trans('text.type') }}</label>
									{{ Form::select('type', $memberGroupTypes , $members_group->type, ['class' => 'form-control update_input', 'id'=> 'member_type']) }}
								</div>
							</div>

						<!-- upgrade to partner -->
						<div class="row form-group">
							<div class="col-6 text-left">
									<button class="btn btn-success inline" type="submit">{{ trans('text.save') }}</button>
								</form>
							</div>

							<div class="col-6 text-right">
								@if (!$members_group->partner)
									<form action="/backend/upgrade_member_to_partner/{!! $members_group->id !!}">
										<button type="submit" class="btn btn-success">{{ trans('users.upgrade_member_to_partner') }}</button>
									</form>
								@else
									<form action="/backend/downgrade_partner_to_member/{!! $members_group->id !!}">
										<button type="submit" class="btn btn-secondary">{{ trans('users.downgrade_partner_to_member') }}</button>
									</form>
								@endif
							</div>
						</div>

					</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
