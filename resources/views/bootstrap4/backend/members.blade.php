@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

    <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('text.members') }}</h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 mt-4 text-center">
                <!-- DATATABLE USERS -->
			    <table class="display row-links" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.name') }}</th>
			                <th>{{ trans('text.reference_person') }}</th>
                            <th>{{ trans('text.description') }}</th>
			                <th></th>
			            </tr>
			        </thead>
			    </table>
			</div>
		</div>
	</div>

    <script type="text/javascript">

    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/member_group/datatable_list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "title" },
                { "data": "reference_person" },
                { "data": "description" },
                { "data": "published" },
            ],
            "aoColumnDefs": [
                {
                "mRender": function ( data, type, row ) {

                    var modify = '<div class="btn bck-lens modal_url" data-url="/backend/members/edit_modal/' + row['id'] + '" data-close-function="reloadTable"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                    var publish = publishButton(row['published'], 'membersgroup', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                    var delete_button = '<div class="btn btn-danger delete_member" title="{{ trans('text.delete') }}" data-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                    return modify + publish + delete_button;
                },
                "aTargets": [ -1 ]
            }],   
        });

        function reloadTable() {
            $("#datatable").DataTable().ajax.reload();
        }

        $('body').on('click', '.delete_member', function(){

            var token = $("meta[name='csrf-token']").attr("content"); 
            var member_id = $(this).data('id');
            var text = "{{ trans('text.delete_question') }}"
            bootbox.confirm(text, function(result){ 
                // if OK, delete the model
                if (result) {
                    $.post({
                        url: '/member_group/delete/' + member_id,
                        data: {
                            '_token': token,
                        },
                        success: function(data, status) {
                            reloadTable();
                        },
                        error: function(xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    })
                }
            });
        });

    </script>    
@endsection
