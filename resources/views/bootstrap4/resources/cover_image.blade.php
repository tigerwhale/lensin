<div class="pb-3">
    <span class='title resource-info-title'>{{ strtoupper(trans('text.cover_image')) }}</span>
    <img src='{!! $resource->getCoverImage() !!}' class='cover-image'>
</div>