<?php 
	$user_id = Auth::check() ? Auth::user()->id : "";
?>
<script>
    var resource_type = "{!! $resource_type !!}";

    $(document).ready(function() {
		$('body').on('click', '.back-button', function() { 
			window.location.href = $(this).data('link'); 
		}); 
		
        populateDivFromApiCall( "{!! $resourceViewApiUrl !!}" , 
    							"#article", 
    							"POST", 
    							{
    								user_id: '{!! $user_id !!}',
									request_server_id: '{!! server_property('server_id') !!}'
    							});

    });
</script>
