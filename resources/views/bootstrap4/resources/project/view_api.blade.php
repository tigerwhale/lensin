<!-- content header -->
<ul class="nav nav-tabs mb-3" id="toolTabHeader" role="tablist">
    @include ('bootstrap4.resources.resources_back_button', ['search' => 'project'])
    <h3 class="ml-auto mr-auto">
        {{ trans('text.project') }}
    </h3>
    {!! view_counter_icons($challenge_group) !!}
</ul>
<!-- end content header -->

<!-- TITLE -->
@include ('bootstrap4.resources.resources_title', ['resource' => $challenge_group])

<!-- info -->
<div class="row">
    <!-- left info -->
    <div class="col-12 col-md-4">

        <!-- cover image -->
        @if ($editable)
            <div id="cover_image"></div>
        @else
            @include("bootstrap4.resources.cover_image", ["resource" => $challenge_group])
        @endif

        @if ($challenge_group->challenge->getCourseName())
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('text.course')), 'text' => $challenge_group->challenge->getCourseName()] )
        @endif
        <!-- List of students -->
        @include('bootstrap4.resources.project.list_of_students', ['resource' => $challenge_group])

        <!-- Attachments -->
        @include('bootstrap4.resources.project.attachments', ['resource' => $challenge_group, 'editable' => $editable])


    </div>

    <div class="col-12 col-md-8">
        <!-- FILE PREVIEW -->
        <div class="row mb-4">
            <div class="col-12" id="file_preview"></div>
        </div>
        <!-- FILE COMMENTS -->
        <div class="row">
            <div class="col-12" id="resource_comments"></div>
        </div>
    </div>

</div>