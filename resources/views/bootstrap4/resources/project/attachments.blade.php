<!-- LIST OF FILES -->
<div class="resource-info-title">{{ strtoupper(trans('challenges.attachments')) }}</div>

<div id="project_resources">
    <i class="fa fa-spinner fa-pulse fa-fw"></i>
</div>

<!-- upload new files if editable -->
@if ($editable)
    <div class="mt-2 p-2 w-100" style="background-color: #EEE; height: 135px;">
        <div class="row">
            <div class="col-12">
                <!-- DROPZONE -->
                <div class="dropzeon dropzone_div py-2" id="resource_upload_{{ $challenge_group->id }}" >{{ trans('text.click_to_select_file') }}</div>
                <span id="tmp-path"></span>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-12">
                    <input type="text" id="add_resource_preview_{!! $challenge_group->id !!}" name="resource_link" class="form-control inline_for_button" placeholder="{{ trans('text.or_insert_link') }} ">
                    <button type="button" class="btn btn-success add_resource" data-project-id="{!! $challenge_group->id !!}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

@endif
    <!-- PROJECTS AJAX-->
    <script type="text/javascript">
        var first_load = 1;
        var resource_list = [];

        $(document).ready(function(){
            load_project_resources();
            load_project_image({!! $challenge_group->id !!});
        });


        // LOAD PROJECT COVER IMAGE
        function load_project_image(project_id) {

            var token = $("meta[name='csrf-token']").attr("content");

            $.ajax({
                data: {
                    '_token': token,
                    'editable': {!! $editable ? 1 : 0 !!}
                },
                url: '{!! server_url($challenge_group->server_id) !!}/api/projects/challenges/load_project_image/' + project_id,
                type: 'POST',
                success: function(response) {
                    $('#cover_image').html(response);
                },
                error: function(response){
                    bootbox.alert (response);
                }
            });
        }

        // LOAD PROJECT RESOURCES
        function load_project_resources(project_edit = 0) {

            // show the loader
            $('#project_resources').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

            // load the resources
            $.ajax({
                url: '{!! server_url($challenge_group->server_id) !!}/api/resources/load_project_resources/{!! $challenge_group->id !!}{!! $editable ? "/edit" : "" !!}',
                type: 'POST',
                data: {
                    'user_id' : {!! isset($request->user_id) ? $request->user_id : "''" !!}
                },
                success: function(data, status) {

                    $('#project_resources').html(data);
                    if (project_edit) {
                        $("#project_resource" + project_edit).click();
                    };
                    sort_files();
                },
                error: function(xhr, desc, err) {
                    bootbox.alert (desc);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        }

        // LOAD RESOURCE COMMENTS
        function load_resource_comments(url) {
            // show the loader
            $('#resource_comments').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

            // load the resources
            $.ajax({
                url: url,
                type: 'GET',
                @if ($editable)
                data: {
                    'editable' : "{!! Auth::user() ? Auth::user()->id : '' !!}"
                },
                @endif
                success: function(data, status) {
                    // load the data
                    $('#resource_comments').html(data);

                    // TODO ADD COMMENT

                },
                error: function(xhr, desc, err) {
                    bootbox.alert (desc);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        }

        // SHOW RESOURCE PREVIEW BUTTON
        $('body').on('click', '.show_resource_preview', function(){
            var url = $(this).data('url');
            var id = $(this).data('id');
            show_resource_preview(url, id);
            first_load = 0;
        });

        function show_resource_preview(url, id) {

            $(".edit_project_resource").removeClass("active");
            $(".edit_project_resource[data-id='" + id +"']").closest( "li" ).addClass("active");

            load_project_resource_preview(url + "/api/resources/preview/" + id);
            load_resource_comments(url + "/api/resources/comments/list/" + id);
        }

        // LOAD RESOURCE PREVIEW
        function load_project_resource_preview(url) {

            // show the loader
            $('#file_preview').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

            // load the preview
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    'user_id' : {!! isset($request->user_id) ? $request->user_id : "''" !!}
                },
                success: function(data, status) {
                    $('#file_preview').html(data);
                },
                error: function(xhr, desc, err) {
                    bootbox.alert (desc);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        }



        // SORT ATTACHMENTS
        function sort_files() {
            $( ".sortable_ul" ).sortable({
                //handle: ".handle",
                stop: function( event, ui ) {
                    // get the token
                    var token = $("meta[name='csrf-token']").attr("content");

                    // make resources id-s
                    var resources = [];

                    $(this).children('li').each(function(i){
                        resources.push($(this).data('id'));
                    });

                    // send to server
                    $.post({
                        url: '/resources/update_sort',
                        data: {
                            '_token': token,
                            'resources': resources,
                        },
                        success: function(data, status) {
                            //alert(data);
                        },
                        error: function(xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    });
                }
            });
        }

        @if ($editable)

        // ADD COMMENT
        $('body').on('click', '#add_resource_comment', function(){

            var resource_id = $(this).data('id');
            var text = $('#resource_comment_text').val();
            var token = $("meta[name='csrf-token']").attr("content");

            if (!text) {
                bootbox.alert ('{{ trans('resources.you_must_enter_comment_text') }}');
            } else {

                // show the loader
                $('#resource_comments').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

                // load the preview
                $.ajax({
                    url: '/api/resources/comments/create',
                    type: 'POST',
                    data: {
                        'resource_id': resource_id,
                        'text': text,
                        'token': token,
                        'user_id': {{ Auth::user()->id }}
                    },
                    success: function(data, status) {
                        load_resource_comments("{{ url("/") }}/api/resources/comments/list/" + resource_id);
                    },
                    error: function(xhr, desc, err) {
                        bootbox.alert (desc);
                        console.log("Details: " + desc + "\nError:" + err);
                    }
                });

            }
        });

        // DELETE COMMENT
        $('body').on('click', '.delete_comment', function(){

            var comment_id = $(this).data('comment-id');
            var resource_id = $(this).data('resource-id');
            var text = "{{ trans('resources.question_delete_resource_comment') }}";
            var token = $("meta[name='csrf-token']").attr("content");

            bootbox.confirm(text, function(result){

                $.ajax({
                    url: '/resources/comments/delete/' + comment_id,
                    type: 'GET',
                    data: {
                        'token': token
                    },
                    success: function(data, status) {
                        load_resource_comments("/api/resources/comments/list/" + resource_id);
                    },
                    error: function(xhr, desc, err) {
                        bootbox.alert (desc);
                        console.log("Details: " + desc + "\nError:" + err);
                    }
                });

            });
        });

        // ADD A RESOURCE TO PROJECT
        $('body').on('click', '.add_project_resource', function(){
            var project_id = $(this).data('project-id');

            $.ajax({
                url: '/resources/create_project_resource/' + project_id ,
                type: 'GET',
                success: function(data, status) {

                },
                error: function(xhr, desc, err) {
                    bootbox.alert (desc);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        });

        // Create a new resource
        $('body').on('click', '.add_resource', function(){

            var project_id = $(this).data('project-id');
            var token = $("meta[name='csrf-token']").attr("content");
            var preview_link = $("#add_resource_preview_" + project_id).val();
            $.ajax({
                method:"POST",
                url:"/resources/create_project_resource/" + project_id,
                success: function(responce){
                    openModalFromUrl('/resources/edit_project_resource/' + responce, 'load_project_resources');
                    load_project_resources();
                    $("#add_resource_preview_" + project_id).val("");
                    sort_files();
                },
                data: {
                    '_token': token,
                    'preview_link': preview_link,
                },
            });
        });

        // delete resource
        $('body').on('click', '.delete_resource', function(){

            var resource_id = $(this).data('id');
            var text = "{{ trans('text.question_delete_resource') }}";

            bootbox.confirm(text, function(result){
                if (result) {
                    $.ajax({
                        url: '/resources/delete',
                        type: 'GET',
                        data: {
                            'id' : resource_id
                        },
                        success: function(data, status) {
                            load_project_resources();
                        },
                        error: function(xhr, desc, err) {
                            bootbox.alert (desc);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    });
                }

            });
        });



        // DROPZONE - upload resouce and open resource modal
        $("div#resource_upload_{{ $challenge_group->id }}").dropzone({
            url: "/resources/create_project_resource/{{ $challenge_group->id }}",
            paramName: "resource_file",
            previewsContainer: false,
            uploadMultiple: true,
            parallelUploads: 1,

            sending: function(files, xhr, formData){
                formData.append( "_token", $("meta[name='csrf-token']").attr('content'));
                formData.append( "author", "{!! $challenge_group->name !!}");
            },

            uploadprogress: function(file, progress, bytesSent) {
                // upload progress percentage
                $("#resource_upload_{{ $challenge_group->id }}").html(parseInt(progress) + "%");
            },

            success: function(file, responce) {
                // Reload resources
                load_project_resources();
                // Open resource modal
                openModalFromUrl('/resources/edit_project_resource/' + responce, 'load_project_resources');
                // Return text to "click here..."
                $("#resource_upload_{{ $challenge_group->id }}").html("{{ trans('text.click_to_select_file') }}");
                sort_files();
            },

            // error return function
            error: function (file, responce){
                bootbox.alert ("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
            },

            init: function (){
                myDropzone = this;
            }
        });

        @endif
    </script>
