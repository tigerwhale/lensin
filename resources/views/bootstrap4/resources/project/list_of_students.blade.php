<?php
$users = $challenge_group->users()->orderBy('name')->get();
?>

<div class="pb-3 mt-3">
    <div class="resource-info-title">{{ strtoupper(trans('text.students')) }}</div>
    @if (isset($users))

        @if (count($users) == 0)
            {{ trans('challenges.no_students_assigned') }}
        @else
            @foreach ($users as $student)
                <div class="hover small">{{ $student->fullName() }}</div>
            @endforeach
        @endif

    @endif
</div>