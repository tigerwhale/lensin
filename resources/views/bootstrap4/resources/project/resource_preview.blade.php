@if ($resource->path)

    <!-- content -->
    <div class="row">
        <div class="col-12">
            @if (isset($resource->name))
                @include('bootstrap4.resources.descriptionTitleAndText', [
                    'title' => strtoupper(trans('text.name')),
                    'text' => $resource->name
                    ])
            @endif

            @if (isset($resource->author))
                @include('bootstrap4.resources.descriptionTitleAndText', [
                    'title' => strtoupper(trans('text.author')),
                    'text' => $resource->author
                    ])
            @endif

            @if (isset($resource->year))
                @include('bootstrap4.resources.descriptionTitleAndText', [
                    'title' => strtoupper(trans('text.year')),
                    'text' => $resource->year
                    ])
            @endif

            @if (isset($resource->licences) && is_array($resource->licences))
                <?php
                    $licenceImages = "";
                    foreach($resource->licences as $licence) {
                        $licenceImages .= '<img src="/images/licences/' . $licence->id . '.png" style="height: 20px;">';
                    };
                ?>
                @include('bootstrap4.resources.descriptionTitleAndText', [
                    'title' => strtoupper(trans('text.licence')),
                    'text' => $licenceImages
                    ]);
            @endif

            @if (isset($resource->length))
                @include('bootstrap4.resources.descriptionTitleAndText', [
                    'title' => strtoupper(trans('text.length')),
                    'text' => $resource->length
                    ])
            @endif

        </div>

    </div>

    <div class="row">
        <!-- file preview -->
        <div class="col-12">
        @if ($resource->resource_type_id)
            @if ($resource->resource_type->name == "video")
                <!-- VIDEO -->
                    <video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
                        <source src="{{ url($resource->path) }}?{{ time() }}" type='video/mp4'>
                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                        </p>
                    </video>

            @elseif($resource->resource_type->name == "image")
                <!-- IMAGE -->
                    @if ($all_resources->count() > 1)
                        @foreach($all_resources as $single_resource)
                            @if ($single_resource->resource_type)
                                @if ($single_resource->resource_type->name == "image")
                                    <?php $visible = ($single_resource->id != $resource->id) ? " display:none; " : "" ?>

                                    <a href="{!! url($single_resource->path) !!}" data-lightbox="slideshow"
                                       style="{!! $visible !!}">
                                        <img class="image-slideshow"
                                             src="{!! url($single_resource->path) !!}?{{ time() }}"
                                             style="max-width: 100%;">
                                    </a>

                                @endif
                            @endif
                        @endforeach
                    @else
                        <a href="{!! url($resource->path) !!}" data-lightbox="slideshow">
                            <img class="image-slideshow" src="{!! url($resource->path) !!}?{{ time() }}"
                                 style="max-width: 100%;">
                        </a>
                @endif

            @endif
        @endif
        <!-- file name and download -->
            <div class="no-preview">
                <div class="row">
                    <div class="col-10 pt-3">
                        {{ basename($resource->path) }}
                    </div>
                    <div class="col-2">
                        {!! downloadLinkAndIncrement("api/resources/increment_download/" . $resource->id, $request) !!}
                    </div>
                </div>

            </div>
        </div>

    </div>


@else
    {{ trans('resources.no_files') }}
@endif 