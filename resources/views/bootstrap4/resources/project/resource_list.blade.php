<ul class="sortable_ul ui-sortable pl-0">
	
	@foreach ($project->resources as $resource) 

		<script type="text/javascript">
			resource_list.push({!! $resource->id !!});	
			@if($loop->first)
				if (first_load){
					show_resource_preview('{!! url('/') !!}', {!! $resource->id !!});
				}
			@endif 
		</script>

		<li class="edit_project_resource hover show_resource_preview"
			data-url="{{ url('/') }}"
			data-id="{!! $resource->id !!}" s
			tyle="list-style-type: none; cursor: pointer">

			<!-- name -->
			@if ($edit)
				<div class="align-self-center pl-1">
					<i class="fas fa-arrows-alt-v" aria-hidden="true"></i>
				</div>
			@endif
			<div class="align-self-center">
				<label class="round_button bck-project icon">{!! ($resource->resource_type) ? $resource->resource_type->icon : '<i class="fa fa-ellipsis-h" aria-hidden="true"></i>' !!}</label>
			</div>
			<div class="flex-grow-1 align-self-center pl-1"
				 data-url="{{ url('/') }}"
				 data-id="{!! $resource->id !!}"
				 style="cursor: pointer; word-break:break-all;">
				{{ $resource->name }}
			</div>
			<!-- sort and select -->
			@if ($edit)
				<div class="text-right align-self-center">
					<button title="{{ trans('text.modify') }}"
							class="btn btn-xs bck-project modal_url round_button pb-0 d-block"
							data-url="/resources/edit_project_resource/{!! $resource->id !!}"
							data-close-function="load_project_resources" >
						<i class="fas fa-pencil-alt" aria-hidden="true"></i>
					</button>

					<button title="{{ trans('text.delete') }}"
							class="btn btn-xs btn-danger delete_resource round_button d-block"
							data-project-id="{!! $project->id !!}"
							data-id="{!! $resource->id !!}">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>
			@endif			
		</li>

	@endforeach


</ul>