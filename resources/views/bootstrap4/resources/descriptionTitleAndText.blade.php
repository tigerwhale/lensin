<?php 
    $alignClass = isset($align) ? $align : "right";
?>

<div class="row mb-3">
    <div class="col col-md-6 col-lg-4 resource-info-title {!! isset($titleClass) ? $titleClass : "" !!}">
        {{ $title }}
    </div>
    <div class="col-12 col-md-6 col-lg-8 text-md-{!! $alignClass !!} {!! isset($textClass) ? $textClass : "" !!}">
        {!! $text !!}
    </div>
</div>
