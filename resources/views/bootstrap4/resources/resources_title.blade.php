<!-- title -->
<div class="row mb-2">
    <div class="col-12">
        <table class="mt-1">
            <tr>
                <td style="padding-right: 10px;">
                    <div class="resource-badge-30 {{ $resource->badge }} float-left">&nbsp;</div>
                </td>
                <td>
                    <div class="resource-title {{ $resource->classText }}">{{ $resource->getTitle() }}</div>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- end title -->