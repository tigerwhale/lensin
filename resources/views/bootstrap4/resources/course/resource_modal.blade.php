<?php $resource_count = 0; ?>
<!-- resource modal -->
<div class="modal" tabindex="-1" role="dialog" id="apiModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
            <!-- modal header / lecture name -->
            <div class="modal-header">
                <div class="round_button bck-course modal_url lens" style="display: inline-block;">
                    {!! ($resourceClicked->resource_type) ? $resourceClicked->resource_type->icon : "" !!}
                </div>
                <span class="text-lecture mt-2">
                    {{ $lecture->name() }}
                </span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- end modal header / lecture name -->

            <div class="modal-body px-0">
                <!-- resources carousel -->
                <div id="resourcesCarousel" class="carousel carousel-fade" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                        @foreach($resources as $resource)
                            @if ($resource->published)
                                <?php $resource_count++ ?>
                                <!-- resource -->
                                <div class="row carousel-item mx-0 align-items-start {!! ($resource->id == $resourceClicked->id) ? 'active' : '' !!}">
                                    <!-- resource left side -->
                                    <div class="col-12 col-md-5">
                                        <table style="width: 100%" class="table table-borderless">
                                            <!-- name -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.name') }}</td>
                                                <td>{{ $resource->name }}</td>
                                            </tr>
                                            <!-- author -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.author') }}</td>
                                                <td>{{ $resource->author }}</td>
                                            </tr>
                                            <!-- year -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.year') }}</td>
                                                <td>{{ $resource->year }}</td>
                                            </tr>
                                            <!-- licence -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.licence') }}</td>
                                                <td>
                                                    @if ($resource->licences)
                                                        @foreach($resource->licences as $licence)
                                                            <img src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
                                                        @endforeach
                                                    @endif
                                                </td>
                                            </tr>
                                            <!-- length -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.length') }}</td>
                                                <td>{{ $resource->length }} {{ isset($resource->unit) ? trans('text.' . $resource->unit) : ""}}</td>
                                            </tr>
                                            <!-- download -->
                                            <tr>
                                                <td class="text-lecture">{{ trans('text.download') }}</td>
                                                <td>{!! downloadLinkAndIncrementAlignLeft("api/resources/increment_download/" . $resource->id, $request) !!}</td>
                                            </tr>
                                            <!-- downloads -->
                                            <tr>
                                                <td colspan="2" class=""> {!! view_counter_icons($resource, 'float-left') !!}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- end resource left side -->

                                    <!-- resource right side -->
                                    <div class="col-12 col-md-7">
                                        @if ($resource->path && !$resource->preview)
                                            <!-- SINGLE FILE -->
                                            <div class="row">
                                                <div class="col-12">

                                                    @if ($resource->resource_type->name == "video")
                                                    <!-- VIDEO -->
                                                        <video id="my-video" class="video-js" style="width:100%" controls preload="none" data-setup="{}">
                                                            <source src="{{ url($resource->path) }}?{{ time() }}" type='video/mp4'>
                                                            <p class="vjs-no-js">
                                                                To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                                <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                            </p>
                                                        </video>

                                                    @elseif ($resource->resource_type->name == "image")
                                                    <!-- IMAGE -->
                                                        <!-- icon and name -->
                                                        <a href="{!! url($resource->path) !!}?{{ time() }}" target="_blank">
                                                            <div class="row">
                                                                <div class="col-12 text-center">
                                                                    <img src="{!! url($resource->path) !!}" style="max-width:100%">
                                                                </div>
                                                            </div>
                                                        </a>

                                                    @else
                                                        <div class="row" style="height: 300px; padding-top: 20px;">
                                                            <div class="col-12 text-center">
                                                                <!-- resource file type -->
                                                                <div class="resource_grid_icon lens" style="font-size:150px; color: #CCC">
                                                                    @if ($resource->resource_type)
                                                                        {!! $resource->resource_type->icon !!}
                                                                    @else
                                                                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                                    @endif
                                                                </div>
                                                                <!-- resource name
                                                                <div class="resource_grid_name line-limit" style="margin-top: 20px;">
                                                                    {!! $resource->name() !!}
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>

                                        @else ($resource->preview && $resource->path)
                                            <!-- FILE AND PREVIEW (CAMTASIA) -->
                                            @if( pathinfo($resource->preview, PATHINFO_EXTENSION) == "mp4" )
                                                <!-- mp4 -->
                                                <video id="my-video" class="video-js" style="width:100%" controls preload="none" data-setup="{}">
                                                    <source src="{{ url($resource->preview) }}?{{ time() }}" type='video/mp4'>
                                                    <p class="vjs-no-js">
                                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                    </p>
                                                </video>
                                            @else
                                                <!-- IFRAME -->
                                                <iframe id="preview_iframe_{!! $resource->id !!}" class="iframeVideo" src="{!! $resource->preview !!}?autostart=0" style="width:100%; height:100%; max-height: 260px; border: 1px #CCC solid;" allowfullscreen></iframe>
                                                <div class="btn btn-info full_screen_btn" data-iframe-id="preview_iframe_{!! $resource->id !!}">
                                                    <i class="fa fa-television" aria-hidden="true"></i> {{ trans('text.fullscreen') }}
                                                </div>
                                            @endif
                                        @endif

                                    </div>
                                    <!-- end resource right side -->

                                </div>
                                <!-- end resource -->
                            @endif
                        @endforeach
                    </div>

                    <!-- previous/next buttons -->
                    @if ($resource_count > 1)
                        <script>
                            console.log('<?php  echo $resource_count; ?>');
                        </script>

                        <a class="carousel-control-prev" href="#resourcesCarousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">{{ trans('text.previous') }}</span>
                        </a>
                        <a class="carousel-control-next" href="#resourcesCarousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">{{ trans('text.next') }}</span>
                        </a>
                    @endif
                    <!-- end previous/next buttons -->

                </div>
                <!-- end resource carousel -->
            </div>

        </div>
    </div>
</div>

<script>
    $('#apiModal').on('hide.bs.modal', function () {
        console.log('closed');
        $(this).remove();
    });
</script>

    <!-- end resource modal -->