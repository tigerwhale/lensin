<?php 
	$lecture_grid_image_class = $lecture['image'] ? 'lecture_grid_image' : 'lecture_grid_no_image';
?>
<script type="text/javascript">
	var lecture_files_{!! $lecture['id'] !!} = [];
</script>

<!-- Lecture row -->
<div class="row hover-lecture py-1">
	<!-- lecture icon -->
	<div class="col-2 pt-1">
		<div class="bck-lecture-img btn-30 border float-left pt-1"></div>
	</div>
	<div class="col-5 col-md-6 col-lg-7">
		{{ $lecture->name }}
	</div>
	<div class="col-5 col-md-4 col-lg-3">
		@foreach ($lecture->orderedPublishedResources as $resource)
			<!-- check if user logged in -->
			@if ($user_id)
				<div class="round_button bck-course modal_url lens float-right ml-2 mr-0" data-user-id="{!! $user_id !!}" data-url="{!! $lecture->platformURL() !!}/api/resources/modal/{!! $resource->id !!}/{!! $lecture->id !!}" data-data="" title="{!! trans('resource_types.' . $resource->resource_type->name ) !!}">
					{!! $resource->resource_type->icon !!}
				</div>
			@else
				<div class="round_button bck-course lens on-click-show-toast float-right ml-2 mr-0" title="{!! trans('resource_types.' . $resource->resource_type->name) !!}. {{ trans('resources.you_have_to_login_to_download_this_resource') }}" style="margin-left: auto; margin-right: auto;" >
					{!! $resource->resource_type->icon !!}
				</div>
			@endif

		@endforeach
	</div>

</div>

