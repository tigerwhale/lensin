@if ($resource->path)
	<!-- FILE -->
	<div class="row pb-2" id="file_div">
		<div class="col-10">
			<div id="filename" style="width:100%; word-wrap: break-word">
				{!! $resource->path ? $resource->path : trans('text.no_file') !!}
			</div>
		</div>
		<div class="col-2 text-right">
			<button type="button" class="btn round_button btn-danger delete_resource_file" data-resource-id="{!! $resource->id !!}" title="{{ trans('text.delete_file') }}">
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
		</div>
	</div>	
@endif 

@if ($resource->preview)
	<!-- PREVIEW -->
	<div class="row" style="padding-bottom: 10px;" id="file_div">
		<div class="col-12">
			<div class="input-group">
				<input type="text" id="preview" name="preview" data-model='resources' data-id={!! $resource->id !!} value="{{ $resource->preview }}" class="form-control update_input" >
				<span class="input-group-btn">
					<button type="button" class="btn btn-danger delete_resource_file" data-resource-id="{!! $resource->id !!}">
	            		<i class="fa fa-times" aria-hidden="true"></i>
	        		</button>
				</span>
			</div>
		</div>
	</div>	
@endif 


@if (!$resource->path && !$resource->preview)
	<div style="background-color: #EEE; padding: 5px;">
		<!-- FILE UPLOAD -->
		<div class="row" id="upload_div">
			<div class="col-12">
				<!-- DROPZONE -->
				<div class="dropzeon dropzone_div" id="resource_upload_{{ $resource->id }}">{{ trans('text.click_to_select_file') }}</div>
			</div>
		</div>
		<!-- PREVIEW -->
		<div class="row" id="preview_div" style="margin-top: 10px;">
			<div class="col-12">
				<div class="input-group">
					<input type="text" name="resource_preview" data-id={!! $resource->id !!} class="form-control resource_preview" placeholder="{{ trans('text.or_insert_link') }}">
					<span class="input-group-btn">
						<button type="button" class="btn btn-success resource_add_preview" data-resource-id="{!! $resource->id !!}">
		            		<i class="fa fa-plus" aria-hidden="true"></i>
		        		</button>
					</span>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">

		var token = $("meta[name='csrf-token']").attr("content"); 
	  	var progressBar_resource_{{ $resource->id }} = document.getElementById('progressBar_resource_{{ $resource->id }}');
	  	var progressOuter_resource_{{ $resource->id }} = document.getElementById('progressOuter_resource_{{ $resource->id }}');
	  	var file_paths = [];
	  	var lecture_id = {{ $resource->resourcable->id }};

		// DROPZONE - upload resouce and refresh file list
		$("div#resource_upload_{{ $resource->id }}").dropzone({ 

			url: "/resources/upload_file/{{ $resource->id }}",
			paramName: "resource_file",
			previewsContainer: false,
			uploadMultiple: true,
			parallelUploads: 100,
			autoProcessQueue: false, 

			drop: function()
			{
				setTimeout(function() { 
					var myDropzone = Dropzone.forElement("div#resource_upload_{{ $resource->id }}");
					myDropzone.processQueue();
				}, 1000);
			},
			addedfile: function()
			{
				setTimeout(function() { 
					var myDropzone = Dropzone.forElement("div#resource_upload_{{ $resource->id }}");
					myDropzone.processQueue();
				}, 1000);
			},
			processing: function(file, xhr, formData) 
			{
				if (file.fullPath) {
					file_paths.push(file.fullPath);
				} else {
					file_paths.push(file.name);
				}
			},
			sending: function(file, xhr, formData) 
			{
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') );
		        formData.append("file_paths", file_paths);

				// if extension is zip, ask if the upload is camtasia
				if (file.name.substring(file.name.length -3) == "zip") 
				{
					if (confirm( "{{ trans('resources.question_is_this_camtasia') }}" )) {
			    		formData.append("zip", "1");
			    	}
				}
			},
			uploadprogress: function(file, progress, bytesSent) 
			{
			    $("#resource_upload_{{ $resource->id }}").html(parseInt(progress) + "%");
			},
			successmultiple: function(file, responce) 
			{
				// add the row and open the modal
				if (responce != "upload_error") 
				{
					console.log(responce);
					resourceModalFilePreviewUpdate({{ $resource->id }});
					var responce_array = JSON.parse(responce);
					if (responce_array['type'] == "multiple") {
						select_resource_preview_file(responce_array['resource_id'], responce_array['file_list']);	
					}
				} else 
				{
					alert ("{{ trans('text.upload_error') }}");
				}

				// Return text to "click here..."
				$("#resource_upload_{{ $resource->id }}").html("{{ trans('text.click_to_select_file') }}");
				
			},
			// error return function
			error: function (file, responce){
				$("#resource_upload_{{ $resource->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			},

			init: function (){
				var file_paths = [];
				myDropzone = this;
			},
		});

	</script>
@endif