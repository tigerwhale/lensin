<!-- VIEW SELECTORS grid/list -->
<div class="row">
	<div class="col-12 text-center">
		<i id="grid_view_selector" class="fa fa-th-large resource_view_selector text-course" data-id="{!! $course->id !!}" aria-hidden="true"></i>
		&nbsp;
		<i id="list_view_selector" class="fa fa-bars resource_view_selector"  data-id="{!! $course->id !!}"  aria-hidden="true"></i>
	</div>
</div>

<!-- title -->
<div class="row mb-2">
	<div class="col-12">
		<table class="mt-1">
			<tr>
				<td style="padding-right: 10px;">
					<div class="resource-badge-30 badge-course float-left">&nbsp;</div>
				</td>
				<td>
					<div class="resource-title text-course">{{ $course->name }}</div>
				</td>
			</tr>
		</table>
	</div>
</div>
<!-- end title -->

<!-- COURSEWARE -->
<div class="row">

	<!-- SUBJECTS -->
	@if (isset($course->subjects_published))
		<!-- if no published subjects -->
		@if (count($course->subjects_published) == 0)
			<div class="col-12 text-center">
				{{ trans('text.no_subjects') }}
			</div>
		@endif
		
		@foreach ($course->subjects_published as $subject)
		
			<div class="accordion mb-4" id="accordion{!! $loop->index !!}"  style="width:100%">
				<div class="card border">
					<!-- subject heading -->
					<div class="card-header" id="subjectHeading{!! $loop->index !!}" data-toggle="collapse" data-target="#collapse{!! $loop->index !!}" aria-expanded="true" aria-controls="collapse{!! $loop->index !!}">
						<a href="#">
							{{ $subject->name }}
							<!--<i class="fa fa-caret-up float-right fa-lg mt-1" aria-hidden="true"></i>-->
						</a>
					</div>

					<!-- LECTURES -->
					<div id="collapse{!! $loop->index !!}" class="collapse show" aria-labelledby="subjectHeading{!! $loop->index !!}" data-parent="#accordion{!! $loop->index !!}">
						<div class="card-body">

							@if (isset($subject->lectures_published))
											
								<!-- if no published lectures -->
								@if (count($subject->lectures_published) == 0)
									<div class="col-12 text-center">
										{{ trans('text.no_lectures') }}
									</div>
								@else 
									<div class="row">
										@foreach ($subject->lectures_published as $lecture)
											@include('bootstrap4.resources.course.courseware_grid_lecture', ['lecture' => $lecture, 'user_id' => $user_id])
										@endforeach
									</div>
								@endif 
							@else 
								<div class="col-12 text-center">
									{{ trans('text.no_lectures') }}					      		
								</div>
							@endif 
						</div>
					</div>
					<!-- END LECTURES -->
				</div>
			</div>
		@endforeach 
	
	@else 
		<div class="col-12 text-center">
			{{ trans('text.no_subjects') }}
		</div>
	@endif
	<!-- END SUBJECTS -->

</div>

<script type="text/javascript">
	$(document).ready(function(){
		lineLimit();
	});
</script>