<!-- COURSE -->
<!-- content header -->
<ul class="nav nav-tabs mb-3" id="courseTabHeader" role="tablist">
	@include ('bootstrap4.resources.resources_back_button', ['search' => 'course'])
	<li class="nav-item ml-auto">
		<a class="nav-link" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="flase">
			{{ strtoupper(trans('resources.info')) }}
		</a>
	</li>
	<li class="nav-item mr-auto">
		<a class="nav-link active" id="courseware-tab" data-toggle="tab" href="#courseware" role="tab" aria-controls="courseware" aria-selected="true">
			{{ strtoupper(trans('resources.courseware')) }}
		</a>
	</li>
	{!! view_counter_icons($course) !!}
</ul>
<!-- end content header -->

<!-- content -->
<div class="tab-content" id="courseTabContent">
	<!-- course info -->
	<div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
		<!-- title -->
        <div class="row mb-2">
        	<div class="col-12">
        		<table class="mt-1">
        			<tr>
        				<td style="padding-right: 10px;">
        					<div class="resource-badge-30 badge-course float-left">&nbsp;</div>
        				</td>
        				<td>
        					<div class="resource-title text-course">{{ $course->name }}</div>
        				</td>
        			</tr>
        		</table>
        	</div>
        </div>
		<!-- end title -->

		<!-- info -->
        <div class="row">
			<!-- left info -->
        	<div class="col-12 col-md-4">

        		<!-- image --> 
        		{!! coverImage($course, "course_big.png") !!}

				<!-- description -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.description')) }}
					</div>
				</div>
				
				<!-- school -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.school')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->school }}
					</div>
				</div>	

				<!-- year -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.year')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->year }}
					</div>
				</div>	

				<!-- country -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.country')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->countryName() }}
					</div>
				</div>		

				<!-- type -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.type')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->type }}
					</div>
				</div>	
				
				<!-- language -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.language')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->languageName() }}
					</div>
				</div>	
				
				<!-- duration -->
				<div class="row mb-3">
					<div class="col col-md-6 resource-info-title">
						{{ strtoupper(trans('resources.duration')) }}
					</div>
					<div class="col-12 col-md-6 text-md-right">
						{{ $course->duration }}
					</div>
				</div>					
			
			</div>
			<!-- end left info -->

			<!-- central info -->
			<div class="col-12 col-md-6 pt-4 pt-md-0">
				<!-- syllabus -->
				<div class="row">
					<div class="col-12 resource-info-title">
						{{ strtoupper(trans('resources.syllabus')) }}
					</div>
					<div class="col-12 text-left">
						{{ $course->description }}
					</div>
				</div>	
			</div>
			<!-- end central info -->

			<!-- teachers -->
			<div class="col-12 col-md-2 pt-4 pt-md-0">
				<div class="row">
					<div class="col-12 resource-info-title">
						{{ strtoupper(trans('resources.teachers')) }}
					</div>
					<div class="col-12 text-left">
						@if(isset($course->teachers))
							@foreach($course->teachers as $teacher)
								<div class="text-course resource-info-title">
									<img src="{!! $teacher->getImage() !!}" class="image">
									{{ $teacher->name }} {{ $teacher->last_name }}
								</div>
							@endforeach
						@endif
					</div>
				</div>					
			</div>
			<!-- end teachers -->

		</div>
		<!-- end info -->

	</div>
	<!-- end course info -->

	<!-- courseware -->
	<div class="tab-pane fade show active" id="courseware" role="tabpanel" aria-labelledby="courseware-tab">
		<!-- Populated through ajax -->
        <div class="text-center" style="width: 100%;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
	</div>
	<!-- end courseware -->

</div>
<!-- end content -->


@include('bootstrap4.resources.course.view_api_js')
