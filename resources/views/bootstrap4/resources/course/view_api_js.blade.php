<script type="text/javascript">
	
	$(document).ready(function() {

        coursewareView('grid');
        $("#open_courseware").tab('show');

		// GRID VIEW LOADER
		$('body').on('click', '#open_courseware', function() {
			coursewareView('grid');
		});

		// GRID VIEW LOADER
		$('body').on('click', '#grid_view_selector', function() {
			coursewareView('grid');
		});

		// LIST VIEW LOADER
		$('body').on('click', '#list_view_selector', function() {
			coursewareView('list');
		});

		// PREVENT DEFAULT ON TABS
		$('#content_tabs a').click(function (e) {
		  	e.preventDefault();
		  	$(this).tab('show');
		});

	});

	// LOAD COURSEWARE VIEW
	function coursewareView(view_type, obj) {

		var token = $("meta[name='csrf-token']").attr("content"); 
		var view_url = '{!! url("/") !!}/api/courses/view/courseware/' + view_type + '/{!! $course->id !!}/{!! $course->server_id !!}';

		$.get({
			url: view_url,
			data: {
				'_token': token,
				'user_id': "{!! $request->user_id !!}",
		      },
			success: function(data, status) {
				$("#courseware").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});	
	}

</script>

