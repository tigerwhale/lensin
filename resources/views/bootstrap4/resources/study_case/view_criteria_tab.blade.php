<div class="row"> 
    <div class="col-12 m-1" id="select_by_criteria_div" style="width:100%">

	    @foreach($study_case_guidelines as $study_case_guideline)
			<?php 
				$display_criteria = true;
				$case_index = "_" . $study_case_guideline->serverid . "_" . $study_case_guideline->id; 
				$row_index = isset($row_index) ? ($row_index + 1) : 1;
				$info = $study_case_guideline->languagename ? (trans('text.language') . ": " . $study_case_guideline->languagename . "<br>") : "";
				$info .= isset($study_case_guideline->author) ? (trans('text.author') . ": " . $study_case_guideline->author . " <br>" ) : "";
				$info .= isset($study_case_guideline->year) ? (trans('text.year') . ": " . $study_case_guideline->year . " <br> " ) : "";
				$info .= isset($study_case_guideline->institution) ? (trans('text.institution') . ": " . $study_case_guideline->institution . " <br> " ) : "";
				if (isset($study_case_guideline->licences)) {
					if (count($study_case_guideline->licences) > 0) {
						$counter = 1;
						$count = count($study_case_guideline->licences);
						$info .= trans('text.licence') . ": ";
						foreach ($study_case_guideline->licences as $licence) {
							$info .= $licence->icon;
							if ($counter == $count) continue; 
							$info .= " ";
		    				$counter++;
						}
						$info .= " <br>";
					}
				}
			
				$info = $info ? $info : trans('text.no_info');
			?>
			<div id="accordion_{!! $case_index !!}" class="mb-1">				
				<div class="card border-0" >
					<div class="card-header border-0 p-0 pl-2 pr-3" id="card_header_{!! $case_index !!}" style="background-color: #EEE;">
						<h5 class="mb-0">
							<table style="width: 100%; min-height: 55px;">
								<tr style="width: 100%; min-height: 55px;" id="criteria{!! $case_index !!}" title="{{ $info }}"
									@if ( isset($guideline_parent) )
										data-parent="criteria_{{ $study_case_guideline->serverurl }}_{{ $guideline_parent->id }}" 
									@endif 
									class="criteria-name" data-criteria-id="{{ $study_case_guideline->id }}" data-server-url="{{ $study_case_guideline->serverurl }}">
									<!-- study case server icon-->
									@if ($study_case_guideline->level == 0)
										<td style="width: 30px; vertical-align: middle; padding-top: 4px;">
											<div class="round_button btn-smallest" style="display: inline-block; background-size: 100%; background-image: url({!! $study_case_guideline->serverurl . '/images/server/logo.png' !!})"></div>
										</td>
									@endif
									<!-- study case name-->
									<td style="vertical-align: middle; width: 100%; min-height: 55px; text-align: left">
										<span>
											{{ $study_case_guideline->name }} 
										</span>
									</td>
									<!-- study case icon-->
									@if (isset($study_case_guideline->study_cases))
										@foreach($study_case_guideline->study_cases as $study_case)
											@if (isset($study_case->resources) && $loop->first)
												<td style="width: 40px; vertical-align: top;">
													<div class="round_button bck-study-case lens criteria-name" style="display: inline-block;">g</div>
												</td>
											@endif
										@endforeach
									@endif 

									<!-- collapse header icon-->	
									@if (isset($study_case_guideline->children))
										@if ($study_case_guideline->children)
											<td style="width: 20px; vertical-align: middle; text-align: center;">
												<a class="collapse_arrow" data-toggle="collapse" data-parent="#card_header_{!! $case_index !!}" href="#collapse{!! $case_index !!}">
													<i class="fa fa-caret-down fa-lg arrow" aria-hidden="true"></i>
												</a>
											</td>
										@endif
									@endif							
								</tr>
							</table>	
						</h5>
					</div>

					<div id="collapse{!! $case_index !!}" class="collapse" aria-labelledby="card_header_{!! $case_index !!}" data-parent="#accordion_{!! $case_index !!}" style="border: 10px #eee solid; border-top: 0px none;">
						<div class="card-body p-1 pr-3">
							<div id="case_studies_{!! $case_index !!}"></div>
							@if(isset($study_case_guideline->children))
								@if ($study_case_guideline->children)
									@include('bootstrap4.resources.study_case.view_criteria_tab', ['study_case_guidelines' => $study_case_guideline->children, 'guideline_parent' => $study_case_guideline])
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>		
		@endforeach

		@if (!isset($display_criteria)) 
			<div class="text-center" style="width:100%; margin-left: 125px;">
				<div class="no_cases_badge resource_badge"></div>
				<br>
				{{ trans('text.no_criteria') }}
			</div>
		@endif 

    </div> 
</div>   