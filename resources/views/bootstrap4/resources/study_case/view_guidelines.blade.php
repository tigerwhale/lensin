@if (isset($study_case->guidelines))
    <!-- title -->
    <div class="row">
        <div class="col-12 resource-info-title">
            {{ strtoupper(trans('resources.criteria')) }}
        </div>
    </div>

    <!-- guidelines -->
    <div class="row text-studycase">

        <div class="col-3">
            {{ strtoupper(trans('resources.dimension')) }}
        </div>
        <div class="col-3">
            {{ strtoupper(trans('resources.criteria')) }}
        </div>
        <div class="col-3">
            {{ strtoupper(trans('resources.guideline')) }}
        </div>
        <div class="col-3">
            {{ strtoupper(trans('resources.subguideline')) }}
        </div>
    </div>

    @foreach($study_case->guidelines as $guidelineLvl0)
        @if ($guidelineLvl0->level == 0)
            <!-- dimension -->
            <div class="row">
                <div class="col-6">
                    {{ $guidelineLvl0->name }}
                </div>
            </div>

            @foreach($study_case->guidelines as $guidelineLvl1)
                @if ($guidelineLvl1->level == 1 && $guidelineLvl1->parent->id == $guidelineLvl0->id)
                    <div class="row">
                        <div class="col-3 text-right">
                            <i class="fa fa-long-arrow-alt-right text-studycase " aria-hidden="true"></i>
                        </div>
                        <div class="col-6">
                            {{ $guidelineLvl1->name }}
                        </div>
                    </div>

                    @foreach($study_case->guidelines as $guidelineLvl2)
                        @if ($guidelineLvl2->level == 2 && $guidelineLvl2->parent->id == $guidelineLvl1->id)
                            <div class="row">
                                <div class="col-6 text-right">
                                    <i class="fa fa-long-arrow-alt-right text-studycase " aria-hidden="true"></i>
                                </div>
                                <div class="col-6">
                                    {{ $guidelineLvl2->name }}
                                </div>
                            </div>

                            @foreach($study_case->guidelines as $guidelineLvl3)
                                @if ($guidelineLvl3->level == 3 && $guidelineLvl3->parent->id == $guidelineLvl2->id)
                                    <div class="row">
                                        <div class="col-8 text-right">
                                            <i class="fa fa-long-arrow-alt-right text-studycase " aria-hidden="true"></i>
                                        </div>
                                        <div class="col-4">
                                            {{ $guidelineLvl3->name }}
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        
                        @endif
                    @endforeach

                @endif
            @endforeach

        @endif
    @endforeach

@endif