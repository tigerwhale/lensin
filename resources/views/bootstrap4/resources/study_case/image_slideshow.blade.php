@if (count($study_case->resources) > 0)
    <?php $resource_count = 0; ?>
    <div id="studyCaseSlideshow" class="carousel slide mb-4" data-ride="carousel">
        <!-- indicators -->
        <ol class="carousel-indicators">
             @foreach ($study_case->resources as $resource)
                @if ($resource->path)
                    <?php $resource_count++; ?>
                    <li data-target="#studyCaseSlideshow" data-slide-to="{!! $loop->index !!}" {!! $loop->first ? 'class="active"' : '' !!}></li>
                @endif 
            @endforeach
        </ol>
        
        <!-- images -->
        <div class="carousel-inner">
            @foreach ($study_case->resources as $resource)
                @if ($resource->path)
                    <div class="carousel-item {!! $loop->first ? 'active' : '' !!}">
                        <a href="{!! url($resource->path) !!}" data-lightbox="slideshow"><img class="d-block w-100" src="{!! url($resource->path) !!}"></a>
                    </div>
                @endif 
            @endforeach
        </div>

        <!-- previous/next buttons -->
        @if ($resource_count > 1)
            <a class="carousel-control-prev" href="#studyCaseSlideshow" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#studyCaseSlideshow" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        @endif
    </div>

@endif