<!-- COURSE -->
<!-- content header -->
<ul class="nav nav-tabs mb-3" id="lectureTabHeader" role="tablist">
    @include ('bootstrap4.resources.resources_back_button', ['search' => 'study_case'])
	<h3 class="ml-auto mr-auto">
        {{ trans('resources.case_study') }}
    </h3>
	{!! view_counter_icons($study_case) !!}
</ul>
<!-- end content header -->

<!-- content -->
<div class="tab-content" id="courseTabContent">
	<!-- study cases tab -->
	<div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">

		<!-- title -->
		<div class="row mb-2">
			<div class="col-12">
				<table class="mt-1">
					<tr>
						<td style="padding-right: 10px;">
							<div class="resource-badge-30 badge-study-case float-left">&nbsp;</div>
						</td>
						<td>
							<div class="resource-title text-studycase">{{ $study_case->name }}</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<!-- end title -->

		<!-- info -->
		<div class="row">

			<!-- left info -->
			<div class="col-12 col-md-4">
				
				<!-- image --> 
				{!! coverImage($study_case, "study_case_big.png") !!}
				
				<!-- category -->
				@if (isset($study_case->category) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.category')), 'text' => $study_case->category] )
				@endif
				<!-- producer -->
				@if (isset($study_case->producer) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.producer')), 'text' => $study_case->producer] )
				@endif
				<!-- language -->
				@if (isset($study_case->language_id) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.language')), 'text' => $study_case->languageName] )
				@endif
				<!-- year -->
				@if (isset($study_case->year) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.year')), 'text' => $study_case->year] )
				@endif
				<!-- author -->
				@if (isset($study_case->author) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.author')), 'text' => $study_case->authorName() ] )
				@endif
				<!-- institution -->
				@if (isset($study_case->institution) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.institution')), 'text' => $study_case->institution ] )
				@endif
			</div>
			
			<!-- right info -->
			<div class="col-12 col-md-8">
				<!-- CASE STUDY IMAGES -->
				@if (isset($study_case->resources))
					{{ strtoupper(trans('resources.case_study')) }}
					<div id="studyCaseImageSlideshow">
						@include('bootstrap4.resources.study_case.image_slideshow', ['study_case' => $study_case])
					</div>
				@endif 	

				<!-- description -->
				@if (isset($study_case->description) )
					@include('bootstrap4.resources.descriptionTitleAndText', 
						['title' => strtoupper(trans('resources.description')), 'text' => $study_case->description, 'align' => 'left'] )
				@endif	

				<!-- guidelines -->
				@include('bootstrap4.resources.study_case.view_guidelines', ['study_case' => $study_case])
					
			</div>
		</div>

	</div>
	<!-- end study cases tab -->

	<!-- criteria tab -->
	<div class="tab-pane fade" id="criteriaTab" role="tabpanel" aria-labelledby="criteria-tab">
		@include('bootstrap4.resources.study_case.view_criteria_tab', ['study_case_guidelines' => $study_case->guidelines_level_0])
	</div>
	<!-- end criteria tab -->

</div>

<script>

	var select_by_criteria = function(data)
	{
		populateDivFromApiCall(
				"/api/study_cases/criteria_tab_view", 
				"#criteriaTab", 
				"POST", 
				{data: data});
	}

	$(document).ready(function(){
		//dataFromAllServers("/api/study_cases/select_by_criteria_data", "", select_by_criteria);
	});

</script>