<button class="btn bck-lens pull-left back-button rounded-circle text-white" 
		title="{!! trans('text.back') !!}"  
		data-link="/search?switch[]={!! $search !!}">
	<i class="fas fa-arrow-left" aria-hidden="true"></i>
</button>
