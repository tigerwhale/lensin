@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/left_menu_filter.css" rel="stylesheet">
    <link href="/css/lens/right_menu_filter.css" rel="stylesheet">
    <link href="/css/lens/datatables.css" rel="stylesheet">

    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/dropzone.js"></script>
@endsection


@section('content')

    <div class="container">
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-2 d-md-block d-none" style="min-height: 600px;">
				@include('bootstrap4.layouts.left_menu_filter', ['home_page' => false])
			</div>

			<!-- MIDDLE CONTENT -->
            <div class="col-12 col-md-8" style="margin-top:20px;" id="article">
            </div>

            <!-- MENU RIGHT -->
            <div class="col-2 d-md-block d-none text-center" id="right_menu_filter">
                @include('bootstrap4.layouts.right_menu_filter', ['home_page' => false, 'languages' => $languages])
            </div>

        </div>
    </div>

    @include('bootstrap4.resources.index_js', [ 'resourceViewApiUrl' => $resourceViewApiUrl ])

@endsection
