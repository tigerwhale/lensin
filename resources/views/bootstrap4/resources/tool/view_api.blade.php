<!-- TOOL VIEW -->
<!-- content header -->
<ul class="nav nav-tabs mb-3" id="toolTabHeader" role="tablist">
    @include ('bootstrap4.resources.resources_back_button', ['search' => 'tool'])
	<h3 class="ml-auto mr-auto">
        {{ trans('resources.tool') }}
    </h3>
	{!! view_counter_icons($tool) !!}
</ul>
<!-- end content header -->

@include ('bootstrap4.resources.resources_title', ['resource' => $tool])

<!-- info -->
<div class="row">

    <!-- left info -->
    <div class="col-12 col-md-4">
        
        <!-- image --> 
        {!! coverImage($tool, "tool_big.png") !!}
        
        <!-- Download link --> 
        @if ($tool->filename || $tool->link)
            <div class="row mb-3">
                <div class="col col-md-6 resource-info-title">
                    {{ strtoupper(trans('text.download')) }}
                </div>
                <div class="col-12 col-md-6 text-md-right">
                    {!! downloadLinkAndIncrementBootstrap4("api/tools/increment_download/" . $tool->id, $request) !!}
                </div>
            </div>	
        @endif        
        <!-- end download link --> 

        <!-- language -->
        @if (isset($tool->languageName) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.language')), 'text' => $tool->languageName] )
        @endif
        <!-- author -->
        @if (isset($tool->author) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.author')), 'text' => $tool->author] )
        @endif
        <!-- institution -->
        @if (isset($tool->institution) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.institution')), 'text' => $tool->institution] )
        @endif

    </div>
     <!-- end left info -->

     <!-- right info -->
    <div class="col-12 col-md-8">

        <!-- file preview-->
        @if ($tool->filename && (isset($request->user_id) || Auth::check() ))
            @include('common.file_link_view', ['filename' => url($tool->filename), 'noLink' => 1])
        @endif        

        <!-- category -->
        @if (isset($tool->category) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.category')), 'text' => $tool->category] )
        @endif    

        <!-- description -->
        @if (isset($tool->description) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.description')), 'text' => $tool->description] )
        @endif              
    </div>
     <!-- end right info -->
</div>
