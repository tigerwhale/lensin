<!-- LECTURE VIEW -->
<!-- content header -->
<ul class="nav nav-tabs mb-3" id="lectureTabHeader" role="tablist">
    @include ('bootstrap4.resources.resources_back_button', ['search' => 'lecture'])
	<h3 class="ml-auto mr-auto">
        {{ trans('resources.lecture') }}
    </h3>
	{!! view_counter_icons($lecture) !!}
</ul>
<!-- end content header -->

<!-- title -->
<div class="row mb-2">
    <div class="col-12">
        <table class="mt-1">
            <tr>
                <td style="padding-right: 10px;">
                    <div class="resource-badge-30 badge-lecture float-left">&nbsp;</div>
                </td>
                <td>
                    <div class="resource-title text-lecture">{{ $lecture->name }}</div>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- end title -->

<!-- info -->
<div class="row">

    <!-- left info -->
    <div class="col-12 col-md-4">
        
        <!-- image --> 
        {!! coverImage($lecture, "lecture_big.png") !!}

        <!-- Resource view links --> 
        @if ($lecture->resources_published)
            <div class="row mb-3">
                <div class="col col-md-6 col-lg-4 resource-info-title">
                    {{ strtoupper(trans('text.resources')) }}
                </div>
                <div class="col-12 col-md-6 col-lg-8">
                    @foreach ($lecture->resources_published as $resource)
                        @if ($resource->published)
                            @if($request->user_id)
                                <div class="btn-20 float-left float-md-right modal_url ml-1 bck-lens text-light pt-1" 
                                    style="font-family: lens; font-size:7pt; cursor: pointer;" 
                                    data-user-id={!! isset($request->user_id) ? $request->user_id : '""' !!} 
                                    data-url="{!! url( '/api/resources/modal/' . $resource->id . '/' . $lecture->id ) !!} 
                                    title="{{ $lecture->name }}"
                                    ">
                                    {!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
                                </div>
                            @else 
                                <div class="btn-20 float-left float-md-right ml-1 bck-lens text-light pt-1" 
                                    style="font-family: lens; font-size:7pt; cursor: pointer;"  
                                    title="{{ trans('text.you_have_to_login_to_download_this_resource') }}">
                                    {!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
                                </div>
                            @endif
                        @endif
                    @endforeach
                </div>
            </div>  
        @endif
        <!-- END Resource view links --> 

        <!-- language -->
        @if (isset($lecture->languageName) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.language')), 'text' => $lecture->languageName] )
        @endif
        <!-- author -->
        @if (isset($lecture->author) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.author')), 'text' => $lecture->author] )
        @endif

    </div>
     <!-- end left info -->

     <!-- right info -->
    <div class="col-12 col-md-8">
        
        <!-- course -->
        @if (isset($lecture->course) )
            @include('bootstrap4.resources.descriptionTitleAndText', 
                        [ 'title' => strtoupper(trans('resources.course')), 
                        'text' => '<a href="/courses/view/' . $lecture->course->id  . '?server_id=' . $lecture->course->server_id  . '">' . $lecture->course->name . '</a>' ])
        @endif         
 
        <!-- course subject -->
        @if (isset($lecture->course_subject) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('text.course_subject')), 'text' => $lecture->course_subject->name] )
        @endif      

        <!-- description -->
        @if (isset($lecture->description) )
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('resources.description')), 'text' => $lecture->description] )
        @endif      

        <!-- lecture content -->
        @if (isset($lecture->contents) )
            <?php 
                $lecture_content_text = "";
                foreach ($lecture->contents as $content) {
                    $lecture_content_text .= $content->text . "<br>";
                }
            ?>
            @include('bootstrap4.resources.descriptionTitleAndText', ['title' => strtoupper(trans('text.contents')), 'text' => $lecture_content_text] )
        @endif  



    </div>
     <!-- end right info -->
</div>
