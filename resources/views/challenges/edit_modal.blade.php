<!-- EDIT CHALLENGE MODAL BACKEND -->

<div class="modal-dialog modal-lg">
	<div class="modal-content">

		<!-- HEADER -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('text.edit_challenge') }}</h4>
		</div>

		<!-- CONTENT -->
		<div class="modal-body">
			<div class="row">

				<!-- LEFT -->

					<!-- CHALLENGE EDIT -->
					<div class="col-xs-6">

						<!-- title -->
						<div>
							<h4>{{ trans('text.info') }}</h4>
						</div>

						<!-- name -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.challenge')) }}
							</div>
							<div class="col-xs-8">
								<input type="text" name="name" data-model='projects/challenges' data-id={{ $challenge->id }} value="{{ $challenge->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
							</div>
						</div>

						<!-- theme --> 
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.theme')) }}
							</div>
							<div class="col-xs-8">
								{!! Form::select('theme_id', $themes, $challenge->theme_id, ['class' => 'form-control update_input', 'data-model' => 'projects/challenges', 'data-id' => $challenge->id ]) !!}
							</div>
						</div>

						<!-- select course -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('challenges.select_course')) }}
							</div>
							<div class="col-xs-8">
								{!! Form::select('course_id',
										$courses,
										$challenge->course_id,
										[	'placeholder' => trans('challenges.select_a_course'),
											'class' => 'form-control update_input',
											'data-model' => 'projects/challenges',
											'id' => 'course_id',
											'data-callback' => 'isCourseLinkedOrCustom',
											'data-id' => $challenge->id ]) !!}
							</div>
						</div>

						<!-- course -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('challenges.or_insert_course_name')) }}
							</div>
							<div class="col-xs-8">
								<input type="text"
									   name="course"
									   id="course"
									   data-callback="isCourseLinkedOrCustom"
									   data-model='projects/challenges'
									   data-id='{{ $challenge->id }}'
									   value="{{ $challenge->course }}"
									   placeholder="{{ strtoupper(trans('text.course')) }}"
									   class="form-control update_input" required>
							</div>
						</div>

						<!-- language -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.language')) }}
							</div>
							<div class="col-xs-8">
								{!! Form::select('language_id', $languages, $challenge->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'challenge', 'data-id'=> "$challenge->id", 'placeholder' => trans('text.select_language')]) !!}
							</div>
						</div>
						
						<!-- year -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.year')) }}
							</div>
							<div class="col-xs-8">
								<input type="text" name="year" id="year" list="years" data-model='projects/challenges' data-id={{ $challenge->id }} class="form-control update_input" value="{{ $challenge->year }}" placeholder="{{ strtoupper(trans('text.insert_course_name')) }}" required>
								<datalist id="years">
									@foreach (range(date('Y'), 1970) as $year) 
										<option value="{{ $year }}">
									@endforeach
								</datalist>
							</div>
						</div>

						<!-- description -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.description')) }}
							</div>
							<div class="col-xs-8">
								<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/challenges' data-id={{ $challenge->id }}>{{ $challenge->description }}</textarea>
							</div>
						</div>
								
						<!-- comments -->
						<div class="row" style="margin-top: 20px">
							<div class="col-xs-4">
								{{ strtoupper(trans('text.allow_user_comments')) }}
							</div>
							<div class="col-xs-8">
								{{ Form::checkbox('comments_enabled', '1', $challenge->comments_enabled, [ 'title' => trans('text.enable_users_comments'), 'data-id' => $challenge->id, 'data-url' => '/projects/challenges/update_data', 'class' => 'update_checkbox' ]) }}
							</div>
						</div>

					</div>

				<!-- RIGHT -->

					<!-- GROUPS EDIT -->
					<div class="col-xs-6">

						<!-- title --> 
						<div class="row">
							<div class="col-xs-12 text-left">
								<h4>{{ trans('text.projects') }}</h4>
							</div>
						</div>

						<!-- LIST OF ASSIGNED GROUPS -->
						<div id='groups_assigned_to_challenge' style="margin-bottom: 20px;">
							<i class="fa fa-spinner fa-pulse fa-fw"></i>
						</div>

				</div>
			</div>


		</div>


	</div>
</div>

<script type="text/javascript">
	
    $(document).ready(function() {
		// load groups assigned to the challenge
		groups_assigned_to_challenge();

		isCourseLinkedOrCustom();
    });


    function isCourseLinkedOrCustom() {
		$("#course").prop('disabled', false);
		$("#course_id").prop('disabled', false);

		if ($("#course_id").val()) {
			$("#course").val("");
			$("#course").prop('disabled', true);
		}
		if ($("#course").val()) {
			$("#course_id").val("");
			$("#course_id").prop('disabled', true);
		}
	}


    // load groups assigned to challange
    function groups_assigned_to_challenge(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/edit/{{ $challenge->id }}/groups_assigned_to_challenge',
            type: 'POST',
            success: function(response) {

                $('#groups_assigned_to_challenge').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });    	
    }





</script>