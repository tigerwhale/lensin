@foreach($challenge_group->teachers as $teacher)
	
	<div class="row padding-small">
		<div class="col-xs-12 hover">

			<div class="row ">
				<div class="col-xs-9"  style="line-height: 38px;">
					{{ $teacher->name }} {{ $teacher->last_name }}
				</div>
				<div class="col-xs-3 text-right">
					<button class="btn btn-danger detach_teacher" data-teacher-id="{{ $teacher->id }}" data-group-id="{{ $challenge_group->id }}" title="{{ trans('challenges.detach_teacher_from_group') }}">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
				</div>
			</div>

		</div>
	</div>
	

@endforeach