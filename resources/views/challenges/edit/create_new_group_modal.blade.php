<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content" style="font-size: 13pt;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('challenges.create_new_group') }}</h4>
		</div>
		<div class="modal-body">
			
			<!-- CHALLENGE -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('challenges.challenge') }}
				</div>
				<div class="col-xs-8">
					{!! Form::select('group_challange_id', $challenges, null, ['class' => 'form-control', 'id' => 'group_challange_id']) !!}
				</div>
			</div>

			<!-- GROUP NAME -->
			<div class="row" style="padding-top: 20px;">
				<div class="col-xs-4">
					{{ trans('challenges.group_name') }}
				</div>
				<div class="col-xs-8">
					<input type="text" id="create_group_name" name="group_name" class="form-control">
				</div>
			</div>

			<!-- PROJECT NAME -->
			<div class="row" style="padding-top: 20px;">
				<div class="col-xs-4">
					{{ trans('challenges.project_name') }}
				</div>
				<div class="col-xs-8">
					<input type="text" id="create_group_project_name" name="project_name" class="form-control">
				</div>
			</div>

			<!-- BUTTONS -->
			<div class="row" style="padding-top: 20px;">
				<div class="col-xs-12 text-right">
					<button class="btn btn-success" id="create_a_group">
						<i class="fa fa-check" aria-hidden="true"></i>
					</button>
					<button class="btn btn-danger" class="close" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>
			</div>

		</div>
	</div>

</div>