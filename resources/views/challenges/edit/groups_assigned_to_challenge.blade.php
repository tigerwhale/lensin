@if ($challenge->groups->count())

	<!-- groups assigned to challenge -->
	@foreach ($challenge->groups as $group)
		<div class="row hover">
			<div class="col-xs-5">
				{{ $group->name }}
			</div>
			<div class="col-xs-4">
				@foreach( $group->teachers as $teacher )
					{{ $teacher->name }} {{ $teacher->last_name }} <br>
				@endforeach
			</div>
			<div class="col-xs-3 text-right">
				<!-- edit group -->
				<button type="button" class="btn btn-info modal_url" data-url="/projects/challenges/groups/edit/{{ $group->id }}" data-id="{{ $group->id }}" title="{{ trans('challenges.edit_group') }}">
					<i class="fas fa-pencil-alt" aria-hidden="true"></i>
				</button>
				<!-- delete group -->
				<button type="button" class="btn btn-danger delete_group" data-id="{{ $group->id }}" title="{{ trans('challenges.delete_group') }}">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>				
			</div>
		</div>
	@endforeach

@else 

	<!-- title --> 
	<div class="row">
		<div class="col-xs-12 text-center">
			<h4>{{ trans('challenges.no_groups_assigned_to_challenge') }}</h4>
		</div>
	</div>
	

@endif