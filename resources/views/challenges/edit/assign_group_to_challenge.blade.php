<!-- title --> 
<div class="row">
	<div class="col-xs-12 text-center">
		<h3>{{ trans('challenges.assign_group_to_challenge') }}</h3>
	</div>
</div>

<div class="row">

	@if ($groups->count())
		<!-- Select of groups to assign -->	
		<div class="col-xs-8">
			{!! Form::select('challenge_group_id', $groups, null, ['class' => 'form-control']) !!}
		</div>
	
	@else

		<!-- If no groups exits -->	
		<div class="col-xs-8">
			<h4>{{ trans('challenges.no_groups') }}</h4>	
		</div>
	@endif

	<div class="col-xs-4 text-right">
		<!-- Assign group to challenge button-->
		@if ($groups->count())
			<button type="button" id="assign_group_to_challenge_btn" class="btn btn-info" title="{{ trans('challenges.assign_group_to_challenge')}}">
				<i class="fa fa-sign-in" aria-hidden="true"></i>
			</button>
		@endif

		<!-- Create a new group -->
		<button type="button" id="create_new_group_btn" class="btn btn-success modal_url" data-url="/projects/challenges/edit/create_new_group_modal" title="{{ trans('challenges.create_new_group')}}">
			<i class="fa fa-plus" aria-hidden="true"></i>
		</button>
	</div>


</div>