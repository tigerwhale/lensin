@if ($editable)
	{{ strtoupper(trans('text.cover_image')) }}
	<div class="row">
		<div class="col-12" id="cover_image_container_{!! $project->id !!}">
			{!! coverImageEdit($project) !!}
		</div>
	</div>
@else 
	<div class="row">
		<div class="col-xs-12">
			<img src="{!! $project->image !!}" style="max-height: 300px; max-width: 100%;">
		</div>
	</div>
@endif