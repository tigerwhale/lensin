@foreach($challenge_group->users as $user)
	
	<div class="row padding-small">
		<div class="col-xs-12 hover">

			<div class="row ">
				<div class="col-xs-9" style="line-height: 38px;">
					{{ $user->name }} {{ $user->last_name }}
				</div>
				<div class="col-xs-3 text-right">
					<button class="btn btn-danger detach_user" data-user-id="{{ $user->id }}" data-group-id="{{ $challenge_group->id }}" title="{{ trans('challenges.detach_user_from_group') }}">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
				</div>
			</div>

		</div>
	</div>

@endforeach