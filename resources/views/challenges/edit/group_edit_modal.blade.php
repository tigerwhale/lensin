<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="text-project modal-title">{{ trans('challenges.edit_group') }}</h4>
		</div>

		<div class="modal-body">

			<div class="row">
				<!-- LEFT SIDE -->
				<div class="col-xs-6">

					<!-- COVER IMAGE -->
					{{ strtoupper(trans('text.cover_image')) }}
					<div class="row">
						<div class="col-xs-12" id="cover_image_container_{!! $challenge_group->id !!}">
							{!! coverImageEdit($challenge_group) !!}
						</div>
					</div>

					<!-- group name -->
					<div class="row">
						<div class="col-xs-3">
							
						</div>
						<div class="col-xs-12">
							<span class="text-project">{{ trans('challenges.group_name') }}</span><br>
							<input type="text" name="name" data-model='projects/challenges/groups' data-id={{ $challenge_group->id }} value="{{ $challenge_group->name }}" placeholder="{{ strtoupper(trans('challenges.group_name')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- project name -->
					<div class="row">
						<div class="col-xs-12">
							<span class="text-project">{{ trans('challenges.project_name') }}</span><br>
							<input type="text" name="project_name" data-model='projects/challenges/groups' data-id={{ $challenge_group->id }} value="{{ $challenge_group->project_name }}" placeholder="{{ strtoupper(trans('challenges.project_name')) }}" class="form-control update_input" required>
						</div>
					</div>

					<hr>

					<!-- GROUP TEACHERS -->
					<div class="row">
						<div class="col-xs-12">

							<!-- assign group teachers -->
							<div class="row">
								<div class="col-xs-12">
									<h4 class="text-project"> {{ trans('challenges.group_teachers') }}</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-10">
									{!! Form::select('teacher_id', $users, null, ['class' => 'form-control', 'id' => 'teacher_id', 'data-group-id' => $challenge_group->id ]) !!}
								</div>
								<div class="col-xs-2 text-right">
									<button class="btn btn-success" id="assign_teacher" title="{{ trans('challenges.assign_teacher') }}">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<br>

							<!-- group teachers list-->
							<div id="group_teachers_list">
								<i class="fa fa-spinner fa-pulse fa-fw"></i>
							</div>

						</div>
					</div>

					<hr>

					<!-- GROUP USERS -->
					<div class="row">

						<div class="col-xs-12">

							<!-- assgin user to group -->
							<div class="row">
								<div class="col-xs-12">
									<h4 class="text-project">{{ trans('challenges.users_assigned_to_group') }}</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-10">
									{!! Form::select('user_id', $users, null, ['class' => 'form-control', 'id' => 'user_id', 'data-group-id' => $challenge_group->id ]) !!}
								</div>
								<div class="col-xs-2 text-right">
									<button class="btn btn-success" id="assign_user" title="{{ trans('challenges.assign_user') }}">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</div>

							<br><br>
							
							<!-- group users list-->
							<div id="group_users_list">
								<i class="fa fa-spinner fa-pulse fa-fw"></i>
							</div>

						</div>
					</div>
				</div>

				<!-- RIGHT SIDE -->
				<div class="col-xs-6" style="border-left: 1px #CCC solid;">

					<!-- files assigned to group -->
					<h4 class="text-project">{{ trans('challenges.attachments') }}</h4>
					<div id="project_resources"></div>

				</div>
			</div>

		</div>
	</div>

</div>

<script type="text/javascript">

	// load teachers assigned to group
	load_group_teachers({{ $challenge_group->id }});
	load_group_users({{ $challenge_group->id }});
	load_project_resources({{ $challenge_group->id }});
	load_project_image({{ $challenge_group->id }});

    // LOAD PROJECT RESOURCES
    function load_project_resources() {
        
        // show the loader
        $('#project_resources').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        
        // load the resources
        $.ajax({
            url: '{!! server_url($challenge_group->server_id) !!}/api/resources/load_project_resources/{!! $challenge_group->id !!}/edit',
            type: 'GET',
            success: function(data, status) {
                
                $('#project_resources').html(data);
                $("#project_resource" + {!! $challenge_group->id !!}).click();
                sort_files();
                
            },
            error: function(xhr, desc, err) {
                bootbox.alert (desc);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    }

</script>  
