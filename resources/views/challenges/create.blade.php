<!-- CREATE CHALLENGES BACKEND -->

<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content" style="font-size: 13pt;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('challenges.create_new_challenge') }}</h4>
		</div>

		<div class="modal-body">

			<!-- CHALLENGE NAME -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('challenges.challenge') }}
				</div>
				<div class="col-xs-10">
					<input type="text"
						   name="name"
						   class="form-control"
						   placeholder="{{ (trans('text.insert_name')) }}"
						   required>
				</div>
			</div>

			<!-- AUTHOR -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.author')) }}
				</div>
				<div class="col-xs-10">
					<input type="text"
						   name="author"
						   class="form-control"
						   placeholder="{{ (trans('text.insert_name')) }}"
						   value="{{ Auth::user()->fullName() }}"
						   required>
				</div>
			</div>

			<!-- THEME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.select_theme')) }}
				</div>
				<div class="col-xs-10">	
					{!! Form::select('theme_id', $themes, null, ['class' => 'form-control', 'required']) !!}
				</div>

			</div>

			<!-- COURSE -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.course')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="course" class="form-control" placeholder="{{ (trans('text.insert_course_name')) }}" required>
				</div>
			</div>

			<!-- YEAR -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.year')) }}
				</div>
				<div class="col-xs-10">
					<input name="year" id="year" list="years" class="form-control" value="{!! date('Y') !!}" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year) 
							<option value="{{ $year }}">
						@endforeach
					</datalist>
				</div>
			</div>

			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.description')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control" placeholder="{{ (trans('text.description')) }}"></textarea>
				</div>
			</div>

			<!-- COMMENTS -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ (trans('text.allow_user_comments')) }}
				</div>
				<div class="col-xs-10">
					<input type="checkbox" name="comments_enabled" value="1" checked title="{{ trans('text.enable_users_comments') }}">
				</div>
			</div>

			<!-- BUTTONS -->
			<div class="row" style="padding-top: 20px;">
				<div class="col-xs-12 text-right">
					<button class="btn btn-success" id="create_challenge_btn">
						<i class="fa fa-check" aria-hidden="true"></i>
					</button>
					<button class="btn btn-danger" class="close" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>
			</div>	


		</div>
	</div>
</div>

