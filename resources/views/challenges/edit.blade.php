@extends('layouts.resource')

@section('content')

{!! backendHeader('project', 'projects', strtoupper(trans('text.project')), 'project')  !!}
 
<div class="container">
	<div class="row">
		<!-- CHALLENGE EDIT -->
		<div class="col-xs-6">

			<!-- title --> 
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2>{{ trans('text.edit_challenge') }}</h2>
				</div>
			</div>	

			<!-- theme --> 
			<div class="row">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.theme')) }}
				</div>
				<div class="col-xs-10">
					{!! Form::select('theme_id', $themes, $challenge->theme_id, ['class' => 'form-control update_input', 'data-model' => 'projects/challenge', 'data-id' => $challenge->id ]) !!}
				</div>
			</div>

			<!-- name -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="name" data-model='projects/challenge' data-id={{ $challenge->id }} value="{{ $challenge->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- course -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.course')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="course" data-model='projects/challenge' data-id={{ $challenge->id }} value="{{ $challenge->course }}" placeholder="{{ strtoupper(trans('text.course')) }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- year -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.year')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="year" id="year" list="years" class="form-control" value="{{ $challenge->year }}" placeholder="{{ strtoupper(trans('text.insert_course_name')) }}" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year) 
							<option value="{{ $year }}">
						@endforeach
					</datalist>
				</div>
			</div>

			<!-- language -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.language')) }}
				</div>
				<div class="col-xs-10">
					{!! Form::select('language_id', $languages, $challenge->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'challenge', 'data-id'=> "$challenge->id", 'placeholder' => trans('text.select_language')]) !!}
				</div>
			</div>

			<!-- description -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('challenges.challenge_title')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/challenges' data-id={{ $challenge->id }}>{{ $challenge->description }}</textarea>
				</div>
			</div>
					
					
			<!-- comments -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.comments')) }}
				</div>
				<div class="col-xs-10">
					{{ Form::checkbox('comments_enabled', '1', $challenge->comments_enabled, [ 'title' => trans('text.enable_users_comments'), 'data-id' => $challenge->id, 'data-url' => '/projects/challenges/update_data', 'class' => 'update_checkbox' ]) }}
				</div>
			</div>

		</div>

		<!-- GROUPS EDIT -->
		<div class="col-xs-6">

			<!-- title --> 
			<div class="row">
				<div class="col-xs-12 text-left" style="margin-top: 25px;">
					<h4>{{ trans('challenges.groups_assigned_to_challenge') }}</h4>
				</div>
			</div>

			<!-- LIST OF ASSIGNED GROUPS -->
			<div id='groups_assigned_to_challenge' style="margin-bottom: 20px;">
				<i class="fa fa-spinner fa-pulse fa-fw"></i>
			</div>

			<!-- Create a new group -->
			<button type="button" id="create_new_group_btn" class="btn btn-success modal_url" data-url="/projects/challenges/edit/create_new_group_modal" title="{{ trans('challenges.create_new_group')}}">
				<i class="fa fa-plus" aria-hidden="true"></i> {{ trans('challenges.create_new_group') }}
			</button>
			
		</div>
	</div>

</div>


<script type="text/javascript">

    $(document).ready(function(){
		// load groups assigned to the challenge
		groups_assigned_to_challenge();
    });


    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/projects/challenges/modify';
        return false;
    }); 

   
   	// DETACH TEACHER FROM GROUP
    $('body').on('click', '.detach_teacher', function(){
		
		var token = $("meta[name='csrf-token']").attr("content");
		var teacher_id = $(this).data('teacher-id');
		var group_id = $(this).data('group-id');

        var text = "{{ trans('challenges.detach_teacher_from_group_question') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

				$.ajax({
					data: { 
						'_token': token,
		            },
		            url: '/projects/challenges/groups/detach_teacher/' + group_id + '/' + teacher_id,
		            type: 'POST',
		            success: function(response) {
		            	if (response == "OK") {
							load_group_teachers(group_id);
		            	};

		            	groups_assigned_to_challenge();
		            },
		            error: function(response){
		                bootbox.alert (response);
		            }
				});    
			}
		});

    });


   	// ATTACH TEACHER TO GROUP
    $('body').on('click', '#assign_teacher', function(){
		
		var token = $("meta[name='csrf-token']").attr("content");
		var teacher_id = $("#teacher_id").val();
		var group_id = $("#teacher_id").data('group-id');

		$.ajax({
			data: { 
				'_token': token,
            },
            url: '/projects/challenges/groups/assign_teacher/' + group_id + '/' + teacher_id,
            type: 'POST',
            success: function(response) {
            	if (response == "OK") {
					load_group_teachers(group_id);
            	} else {
            		bootbox.alert (response);
            	}

            	groups_assigned_to_challenge();
            },
            error: function(response){
                bootbox.alert (response);
            }
		});    
    });

	// LOAD GROUP ASSIGNED TEACHERS
	function load_group_teachers(challenge_group_id){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/load_group_teachers/' + challenge_group_id,
            type: 'POST',
            success: function(response) {

                $('#group_teachers_list').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });    	
    }


   	// ATTACH TEACHER TO GROUP
    $('body').on('click', '#assign_user', function(){
		
		var token = $("meta[name='csrf-token']").attr("content");
		var user_id = $("#user_id").val();
		var group_id = $("#user_id").data('group-id');

		$.ajax({
			data: { 
				'_token': token,
            },
            url: '/projects/challenges/groups/assign_user/' + group_id + '/' + user_id,
            type: 'POST',
            success: function(response) {
            	if (response == "OK") {
					load_group_users(group_id);
            	} else {
            		bootbox.alert (response);
            	}

            	groups_assigned_to_challenge();
            },
            error: function(response){
                bootbox.alert (response);
            }
		});    
    });


	// LOAD GROUP USERS
	function load_group_users(challenge_group_id){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/load_group_users/' + challenge_group_id,
            type: 'POST',
            success: function(response) {

                $('#group_users_list').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });    	
    }

   
   	// DETACH TEACHER FROM GROUP
    $('body').on('click', '.detach_user', function(){
		
		var token = $("meta[name='csrf-token']").attr("content");
		var user_id = $(this).data('user-id');
		var group_id = $(this).data('group-id');

        var text = "{{ trans('challenges.detach_user_from_group_question') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

				$.ajax({
					data: { 
						'_token': token,
		            },
		            url: '/projects/challenges/groups/detach_user/' + group_id + '/' + user_id,
		            type: 'POST',
		            success: function(response) {
		            	if (response == "OK") {
							load_group_users(group_id);
		            	};

		            	groups_assigned_to_challenge();
		            },
		            error: function(response){
		                bootbox.alert (response);
		            }
				});    
			}
		});
    });


    function load_assign_group_to_challenge(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/edit/load_assign_group_to_challenge',
            type: 'POST',
            success: function(response) {

                $('#assign_group_to_challenge').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    }


    // load groups assigned to challange
    function groups_assigned_to_challenge(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/edit/{{ $challenge->id }}/groups_assigned_to_challenge',
            type: 'POST',
            success: function(response) {

                $('#groups_assigned_to_challenge').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });      
    }





</script>

@endsection
