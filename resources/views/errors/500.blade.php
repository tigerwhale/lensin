@extends('layouts.app')

@section('content')

	<div class="container text-center" style="padding-top: 100px; padding-bottom: 200px;">
		<h1>Error on page. </h1>
		Please go back and refresh your request.
	</div>

@endsection
