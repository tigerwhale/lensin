@extends('layouts.app')

@section('content')

	<div class="container">
        
        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.news') }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8 text-center">
                <h4>News list</h4>  
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8 text-center" style="padding-top: 20px;">
                <!-- DATATABLE USERS -->
			    <table class="display text-left" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.title') }}</th>
                            <th>{{ trans('text.author') }}</th>
			                <th class="text-right"></th>
			            </tr>
			        </thead>
			    </table>
            </div>
		</div>

        <!-- LOCAL SERVER NEWS ON CENTRAL -->
        @if(server_property("central_server") == 1)
            <div class="row">
                <div class="col-xs-2" style="padding-top: 20px;"></div>
                <div class="col-xs-8 text-center">
                    <h4>News from local servers</h4>  
                </div>
            </div>

            <div class="row">
                <div class="col-xs-2" style="padding-top: 20px;"></div>  
                <div class="col-xs-8 text-center">
                    @foreach ($server_list as $local_server)
                        <button class="btn bck-lens load-local-news" type="button" data-server-url="{{ $local_server->address }}">{{ $local_server->name }}</button>
                    @endforeach
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-xs-2" style="padding-top: 20px;"></div>  
                <div class="col-xs-8 text-center" id="local_server_news">
                </div>
            </div>
        @endif

	</div>

    <script type="text/javascript">

        // display list of servers
        // get the list of disabled news from each server
        // on click get news from each server
        // ajax to disable news from the list


        $('body').on('click', '.load-local-news', function(){ 
            $this = $(this);
            console.log( $this.data('server-url') );
            var url = $this.data('server-url') + '/api/backend/news_list';
            ajax_post_to_div(url, 'local_server_news');

        });

        //ajax_post_to_div('url_path', div_id, data_to_send, callback)


    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf><"toolbar">rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/news/datatable_list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "title" },
                { "data": "author" },
                { "data": "published" },
            ],
            "aoColumnDefs": [
                {
                    "mRender": function ( data, type, row ) {
                        var modify_button = '<div class="btn bck-lens modal_url" data-url="/backend/news/edit_modal/' + row['id'] + '" data-close-function="reloadTable"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div> ';
                        var publish_button = publishButton(row['published'], 'news', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}");
                        var global_button = globalButton(row['global'], 'news', row['id'], "{{ trans('text.global_click_to_make_local') }}", "{{ trans('text.local_click_to_make_global') }}");
                        //var delete_user_button = "<a href='/backend/users/delete/" + row['id'] + "'> <button class='btn btn-danger' type='button' title='{{ trans('users.delete_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a>";
                        return modify_button + " " + publish_button + " " + global_button;
                    },  
                    "aTargets": [ -1 ]
                },
            ]        
        });

        $("div.toolbar").html('<button type="button" class="btn btn-success modal_url" data-url="/backend/news/create_modal" title="{{ trans('text.create') }}" data-close-function="reloadTable"><i class="fa fa-plus" aria-hidden="true"></i></button>');

        function reloadTable() {
            $("#datatable").DataTable().ajax.reload();
        }

    </script>    
@endsection
