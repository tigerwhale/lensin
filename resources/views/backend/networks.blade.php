{{ csrf_field() }}
<!-- CREATE NEW NETWORK -->
<h5>{{ trans('network.create_new_network') }}</h5>
<form action="/backend/networks/create" method="POST">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<small>{{ trans('text.name') }}</small><br>
			<input type="text" name="name" class="form-control" required>			
		</div>
		<div class="col-md-6 col-xs-12">
			<small>{{ trans('text.description') }}</small><br>
			<input type="text" name="description" class="form-control">
		</div>
		<div class="col-md-2 col-xs-12 text-right">
			<br>
			<button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
		</div>
	</div>
</form>
<br>

<!-- NETWORKS LIST -->
@foreach ($networks as $network)

	<div class="panel-group" >
		<div class="panel panel-default" id="network_panel_{{ $network->id }}">

		<div class="panel-heading">
			<div class="row">
				<div class="col-md-5 col-xs-11">
					<small>{{ trans('text.name') }}</small><br>
					<input type="text" name="name" data-model='networks' data-id={{ $network->id }} value="{{ $network->name }}" class="form-control update_input">
				</div>
				<div class="col-md-6 col-xs-11">
					<small>{{ trans('text.description') }}</small><br>
					<input type="text" name="description" data-model='networks' data-id={{ $network->id }} value="{{ $network->description }}" class="form-control update_input">
				</div>
				<div class="col-xs-1">
					<a data-toggle="collapse" href="#network_{{ $network->id }}" class="colapse_arrow collapsed" aria-expanded="false">
						<i class="fa fa-caret-down pull-right fa-lg" aria-hidden="true" style="margin-top: 14px;"></i>	
					</a>
				</div>
			</div>
		</div>

		<div id="network_{{ $network->id }}" class="panel-collapse collapse">
			<div class="panel-body">
				<!-- MEMBERS LIST -->
				@if ($network->top_members->count())

					@foreach ($network->top_members as $member)
						<div class="row">
							<div class="col-md-5 col-xs-12">
								<small>{{ trans('network.member_name') }}</small><br>
								<input type="text" name="name" data-model='network_member' data-id={{ $member->id }} value="{{ $member->name }}" class="form-control update_input">
							</div>
							<div class="col-md-7 col-xs-12">
								<small>{{ trans('text.description') }}</small><br>
								<input type="text" name="description" data-model='network_member' data-id={{ $member->id }} value="{{ $member->description }}" class="form-control update_input">
							</div>
						</div>

						@if ($member->contacts->count())
							<div class="row">
								<div class="col-sm-offset-1 col-xs-11">
									<br>
									{{ trans('network.contacts') }}
									<hr class="margin-small">
									
									@foreach($member->contacts as $contact)
										<div class="row">
											<div class="col-md-5 col-xs-12">
												<small>{{ trans('network.member_name') }}</small><br>
												<input type="text" name="name" data-model='network_member' data-id={{ $contact->id }} value="{{ $contact->name }}" class="form-control update_input">
											</div>
											<div class="col-md-5 col-xs-12">
												<small>{{ trans('text.email') }}</small><br>
												<input type="text" name="email" data-model='network_member' data-id={{ $contact->id }} value="{{ $contact->email }}" class="form-control update_input">
											</div>
											<div class="col-md-2 col-xs-12">
												<form action="/network_member/delete/{{ $contact->id }}" method="POST">
													{{ csrf_field() }}
													<br>
													<button type="submit" class="btn btn-danger">{{ trans('text.delete') }}</button>
												</form>
											</div>
										</div>
									@endforeach
								</div>
							</div>
						@endif

						<!-- CREATE NEW CONTACT -->
						<div class="row">
							<div class="col-sm-offset-1 col-xs-11">						
								<h6>{{ trans('network.create_new_contact') }}</h6>
								<form action="/network_member/create" method="POST">
									<input type="hidden" name="network_member_id" value="{{ $member->id }}">
									{{ csrf_field() }}
									<div class="row">
										<div class="col-md-4 col-xs-12">
											<small>{{ trans('text.name') }}</small><br>
											<input type="text" name="name" class="form-control" required>			
										</div>
										<div class="col-md-6 col-xs-12">
											<small>{{ trans('text.email') }}</small><br>
											<input type="text" name="email" class="form-control">
										</div>
										<div class="col-md-2 col-xs-12 text-right">
											<br>
											<button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
										</div>
									</div>
								</form>
								<br>
							</div>
						</div>

						<hr>
					@endforeach

				@endif 
				
				<!-- CREATE NEW MEMBER -->
				<h6>{{ trans('network.create_new_member') }}</h6>
				<form action="/network_member/create" method="POST">
					<input type="hidden" name="network_id" value="{{ $network->id }}">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-4 col-xs-12">
							<small>{{ trans('text.name') }}</small><br>
							<input type="text" name="name" class="form-control" required>			
						</div>
						<div class="col-md-6 col-xs-12">
							<small>{{ trans('text.description') }}</small><br>
							<input type="text" name="description" class="form-control">
						</div>
						<div class="col-md-2 col-xs-12 text-right">
							<br>
							<button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
						</div>
					</div>
				</form>
				<br>

			</div>
		</div>
	</div>
	<br>

@endforeach