<div class="col-xs-12">
	<ul id="content_tabs" class="nav nav-tabs ">
				
		{!! header_menu_link("/backend/servers", "manageservers", trans('text.servers'), true ) !!}
		{!! header_menu_link("/backend/news", "managenews", trans('text.news'), true ) !!}
		{!! header_menu_link("/backend/general", "managesite", trans('text.general') ) !!}
		{!! header_menu_link("/backend/pages", "managesite", trans('text.site') ) !!}

		{!! header_menu_link("/backend/users", "manageusers", trans('text.users') ) !!}
		{!! header_menu_link("/backend/members", "manageusers", trans('text.members') ) !!} 
		{!! header_menu_link("/backend/partners", "manageusers", trans('text.partners') ) !!}
		{!! header_menu_link("/backend/newnetworks", "manageusers", trans('text.new_networks') ) !!}

		{!! header_menu_link("/backend/languages", "managelanguages", trans('text.languages'), true ) !!}

	</ul>
</div>

