<div class="list-section-flex">
    <h4>{{server_property('server_name')}}</h4>
    <br>

    <table style="width:100%">
        <thead>
        <tr class="bck-lens" style="color: #FFF;">
            <th class="text-left">Title</th>
            <th class="text-left">News text</th>
            <th class="text-center">Published <br>on central</th>
        </tr>
        </thead>

        @foreach ($news as $single_news)
            <tr class="hover">
                <td class="text-left">
                    {{ $single_news->title }}
                </td>
                <td class="text-left">
                    {{ mb_strimwidth($single_news->text, 0, 60, "...")  }}
                </td>
                <td class="text-center" style="min-width: 120px;">
                    <button type="button" class="btn bck-lens publish_news_central shaddow"
                            id="{!! $single_news->id !!}_{!! server_property('server_id') !!}"
                            data-url="/news/publish-central" data-news-id="{{ $single_news->id }}"
                            data-server-url="{{ url("/") }}" data-server-id="{{ server_property('server_id') }}"
                            title="text.click_to_publish"><i class="far fa-eye" aria-hidden="true"></i></button>
                </td>
            </tr>
        @endforeach

    </table>
</div>