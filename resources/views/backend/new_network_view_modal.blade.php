<?php
	$downloadButton = '<span class="round_button text-tools text-center pull-right" style="background-color: white;">';
	$downloadButton .= "<a href='$newnetwork->document' target='_blank' download><i class='fa fa-download' aria-hidden='true'></i></a>";
	$downloadButton .= "</span>";
?>
<div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header" id='main_modal_header'>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>{{ trans('text.new_network') }}</h4>
		</div>
		<div class="modal-body" id="main_modal_body">
			{!! titleAndTextRow('text.name', $newnetwork->name, 5) !!}
			{!! titleAndTextRow('text.description', $newnetwork->description, 5) !!}
			{!! titleAndTextRow('text.reference_person', $newnetwork->reference_person, 5) !!}
			{!! titleAndTextRow('text.reference_person_email', $newnetwork->reference_person_email, 5) !!}
			{!! titleAndTextRow('text.country', $newnetwork->country->name, 5) !!}
			{!! titleAndTextRow('text.document', $downloadButton, 5) !!}
			
			<div class="row">
				<div class="col-xs-6">
					<button class="btn btn-success close_and_mark_as_resolved" data-id="{!! $newnetwork->id !!}" type="button">{{  $newnetwork->resolved ? trans('text.close_and_mark_as_unresolved') : trans('text.close_and_mark_as_resolved') }}</button>
				</div>
				<div class="col-xs-6 text-right">
					<button class="btn btn-danger" data-dismiss="modal" type="button">{{ trans('text.close') }}</button>
				</div>
			</div>
				
		</div>
	</div>
</div>