@extends('layouts.app')

@section('content')

	<div class="container">

        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.languages') }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

      
        <!-- LANGUAGE LIST -->
        <div class="row" style="padding-top: 20px;">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <a data-toggle="collapse" href="#language_list" class="" aria-expanded="true">
                            <div class="panel-heading colapse_arrow"> 
                                <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                                <div class="panel-title">
                                    <h4>{{ strtoupper(trans('languages.language_list')) }}</h4>
                                </div>
                            </div>
                        </a>
                        <div id="language_list" class="panel-collapse collapse">
                            <div class="panel-body">
               
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><small>{{ trans('text.published') }}</small></th>
                                            <th><small>{{ trans('languages.locale') }}</small></th>
                                            <th><small>{{ trans('languages.name') }}</small></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($languages as $language) 
                                            <form action="/backend/languages/update/{!! $language->id !!}" method="POST">
                                            {{ csrf_field() }}
                                            <tr>
                                                <td class="col-xs-1">
                                                    <input type="checkbox" name="published" {{ $language->published ? "checked" : ""}} value="1">
                                                </td>
                                                <td class="col-xs-1">
                                                    <input type="text" name="locale" value="{{ $language->locale }}" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" name="name" value="{{ $language->name }}" class="form-control">
                                                </td>
                                                <td style="text-align: right">
                                                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button> 
                                                    <a href="/backend/languages/delete/{!! $language->id !!}"><button type="button" class="btn btn-danger">{{ trans('text.delete') }}</button></a> 
                                                    <a href="/backend/translations/{!! $language->id !!}"><button type="button" class="btn btn-info">{{ trans('text.manage') }}</button></a> 
                                                    <button type="button" class="btn btn-info distribute" data-locale="{!! $language->locale !!}">{{ trans('text.distribute') }}</button>
                                                </td>
                                            </tr>
                                            </form>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ADD A NEW LANGUAGE -->
        <div class="row">
            <div class="col-xs-2" ></div>

            <div class="col-xs-8">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <a data-toggle="collapse" href="#add_language" class="" aria-expanded="true">
                            <div class="panel-heading colapse_arrow"> 
                                <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                                <div class="panel-title">
                                    <h4>{{ strtoupper(trans('languages.add_new_language')) }}</h4>
                                </div>
                            </div>
                        </a>
                        <div id="add_language" class="panel-collapse collapse">
                            <div class="panel-body">
                                {{ trans('languages.add_new_language_directions') }}
                                <form action="/backend/languages/create" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <!-- locale -->
                                        <div class="col-xs-2">
                                            <small>{{ trans('languages.locale') }}</small><br>
                                            <input type="text" name="locale"  class="form-control" required maxlength="2">
                                        </div>
                                        <!-- name -->
                                        <div class="col-xs-8">
                                            <small>{{ trans('languages.name') }}</small><br>
                                            <input type="text" name="name" class="form-control" required>
                                        </div>
                                        <!-- create -->
                                        <div class="col-xs-2">
                                            <br>
                                            <button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 
        <br>

        DISTRIBUTE TRANSLATIONS TO LOCAL PLATFORMS
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success" id="distribute">{{ trans('backend.distribute_translations_to_local_platforms') }}</button>
                </div>
            </div> -->
        
	</div>

<script type="text/javascript">

    // distribute languages across platforms
    $('.distribute').click(function (){
        
        // prepare the results array
        var results = "";
        var button = $(this);
        var locale = $(this).data('locale');
        var token = $("meta[name='csrf-token']").attr("content"); 
        //disable the button
        button.attr("disabled", true);
        button.html("<i class='fa fa-spinner fa-pulse fa-fw'></i> {{ trans('text.please_wait') }} ");

        // Get translations for this location
        $.post({
            url: '/backend/langauge/translation_array/' + locale,
            data: {
                '_token': token,
            },
            success: function(translation_data, status) {
                    
                // trigger pull on servers
                $.when(

                    @foreach($platforms as $platform)

                        // Send data to servers
                        $.post({
                            url: '{{ $platform->address }}/api/backend/synchronize',
                            data: {
                                '_token': token,
                                'locale': locale,
                                'translation_data': translation_data,
                            },
                            success: function(data, status) {
                                results += ('{{ $platform->address }} ' + data + " || ");
                            },
                            error: function(xhr, desc, err) {
                                results += ('{{ $platform->address }} ' + err + " || ");  
                                console.log(xhr);
                                console.log("Details: " + desc + "\nError:" + err);
                            }
                        })

                        @if (!$loop->last)
                            ,
                        @endif


                    @endforeach

                ).then(function() {
                    //display the results
                    button.html("{{ trans('text.distribute') }}");
                    button.removeAttr("disabled");
                    bootbox.alert(results);

                });
            }
        });
    });
    
        
    
</script>

@endsection
