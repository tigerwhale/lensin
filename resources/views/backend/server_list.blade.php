@extends('layouts.app')

@section('content')

	<div class="container">
       
        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.servers') }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

        <!-- ADD A NEW SERVER -->
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 text-center" style="padding-top: 20px;">
                <h3>{{ strtoupper(trans('text.add_new_server')) }} </h3>
                <form action="/backend/servers/create" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-4">
                            <small>{{ trans('text.name') }}</small><br>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="col-xs-6">
                            <small>{{ trans('text.address') }}</small><br>
                            <input type="text" name="address"  class="form-control" required>
                        </div>
                        <div class="col-xs-2 text-center">
                            <small>{{ trans('text.enabled') }}</small><br>
                            <input type="checkbox" name="enabled" value="1" >
                        </div>

                        <div class="col-xs-4">
                            <small>{{ trans('text.administrator_contact') }}</small><br>
                            <input type="text" name="administrator_contact" class="form-control" required>
                        </div>
                        <div class="col-xs-6">
                            <small>{{ trans('text.notes') }}</small><br>
                            <input type="text" name="notes"  class="form-control">
                        </div>   
                        <div class="col-xs-2 text-right">
                            <br>
                            <button type="submit" class="btn btn-success">{{ trans('text.create') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- LIST SERVERS -->
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 text-center" style="padding-top: 20px;">
                <h3>{{ strtoupper(trans('text.server_list')) }}</h3>
                
                @foreach($server_list as $server) 
                    <form action="/backend/servers/update/{!! $server->id !!}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
             
                            <div class="col-xs-4">
                                <small>{{ trans('text.name') }}</small><br>
                                <input type="text" name="name" value="{{ $server->name }}" class="form-control">
                            </div>
                            <div class="col-xs-6">
                                <small>{{ trans('text.address') }}</small><br>
                                <input type="text" name="address" value="{{ $server->address }}" class="form-control">
                            </div>
                            <div class="col-xs-2 text-center">
                                <small>{{ trans('text.enabled') }} / ID</small><br>
                                <input type="checkbox" name="enabled" {{ $server->enabled ? "checked" : ""}}>
                                {!! $server->id !!} 
                            </div>                               
                            <div class="col-xs-4">
                                <small>{{ trans('text.administrator_contact') }}</small><br>
                                <input type="text" name="administrator_contact" value="{{ $server->administrator_contact }}" class="form-control">
                            </div>    
                            <div class="col-xs-6">
                                <small>{{ trans('text.notes') }}</small><br>
                                <input type="text" name="notes" value="{{ $server->notes }}" class="form-control">
                            </div>   
                            <div class="col-xs-1">
                                <br>
                                <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                            </div>
                            <div class="col-xs-1">
                                <br>
                                <a href="/backend/servers/delete/{!! $server->id !!}"><button type="button" class="btn btn-danger">{{ trans('text.delete') }}</button></a>
                            </div>

                        </div>
                        <hr>
                    </form>
                @endforeach

            </div>
        </div>
        

	</div>

@endsection
