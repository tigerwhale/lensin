@extends('bootstrap4.layouts.frontend')

@section('head')
    <link href="/css/lens/datatables.css" rel="stylesheet">
    <link href="/css/lens/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/dropzone.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <script src="/js/jquery.cookie.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
@endsection

@section('content')

    <div class="container mt-3">
    @include('bootstrap4.backend.nav_bar')

    <!-- TITLE -->
        <div class="row">
            <div class="col-12 text-center mt-2">
                <h4>{{ trans('frontend_header.network') }}</h4>
            </div>
        </div>

        <!-- NETWORKS LIST -->
        <div class="row">
            <div class="col-12 text-center mt-4">
                <!-- DATATABLE SEARCH -->
                <table class="display" id="datatable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('text.title') }}</th>
                        <th>{{ trans('text.description') }}</th>
                        <th style="min-width: 120px;" class="text-right"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");

        // DATATABLE
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true,
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                cache:true,
                data: {
                    '_token': token
                },
                url : '/backend/networks_datatableList',
                rowId: 'id',
            },

            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "description" },
            ],

            "aoColumnDefs": [
                {
                    "mRender": function ( data, type, row ) {
                        var modify = '<div class="btn bck-lens modal_url" data-url="/backend/edit_network_modal/' + row['id'] + '" data-close-function="reloadTable"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div> ';

                        var publish = publishButton(row['published'], 'network', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                        //var delete_button = '<div class="btn btn-danger delete_page" title="{{ trans('text.delete') }}" data-page-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                        return modify + publish;
                        //return modify + publish + delete_button;
                    },
                    "aTargets": [ 3 ]
                }]

        });

    </script>

@endsection
