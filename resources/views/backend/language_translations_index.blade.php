@extends('layouts.app')

@section('content')

	<div class="container">
		
        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.languages') }} - {{ $language->name }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>


        <!-- LIST VARIABLES -->        
        <div class="row">
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8" style="padding-top: 20px;">
                <?php $group = ""; ?>
                @foreach($translations as $translation)
                    @if ($group != $translation->group)
                        <?php $group = $translation->group; ?>

                        @if (!$loop->first) 
                        <!-- CLOSE PANEL IF NOT FIRST -->    
                                        </div>
                                    </div>
                                </div>                                
                            </div>                        
                        @endif

                        <!-- OPEN PANEL -->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <a data-toggle="collapse" href="#panel_{{ $translation->group }}" class="" aria-expanded="true">
                                    <div class="panel-heading colapse_arrow"> 
                                        <i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
                                        <div class="panel-title">
                                            <h5>{{ strtoupper($translation->group) }}</h5>
                                        </div>
                                    </div>
                                </a>
                                <div id="panel_{{ $translation->group }}" class="panel-collapse collapse">
                                    <div class="panel-body">
                    @endif
                                        <div class="row padding-small">
                                            <div class="col-md-5 col-xs-12" style="word-break: break-all;">
                                                {{ $translation->item }}        
                                            </div>
                                            <div class="col-md-5 col-xs-12">
                                                <input class="form-control" style="width:100%;" type="text" name="text" value="{{ $translation->text }}" id="text_{{ $translation->id }}">
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-success update_translation" data-translation-id="{{ $translation->id }}">{{ trans('text.save') }}</button>
                                            </div>
                                        </div>
                                        
                @endforeach
                                    </div>
                                </div>
                            </div>                                
                        </div>  
            </div>
        </div>

	</div>

<script type="text/javascript">

$(document).ready(function() {
    
    var i = 0;
    
    $('.table thead th').each(function (index) {
        if (index == 0) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width:100%;" />' );
        }
        if (index == 1) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 100%;" />' );
        }        
    });
  

    var table = $('.table').DataTable({
        stateSave: true,
        "bSort": false,
        "autoWidth": false,
        "aoColumnDefs": [
            {
                "mRender": function ( data, type, row ) {
                    return row[1] + '<span style="display:none">' + row[3] + '</span> <input style="width:100%;" type="text" name="text" value="' + row[3] + '" id="text_' + row[4] + '">';
                },
                "sWidth": "20px",
                "aTargets": [ 1 ]
            },
           {
                "aTargets": [ 3, 4 ],
                "visible": false
            },
        ], 

    });

    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $('body').on('click', '.update_translation', function(){
        var token = $("meta[name='csrf-token']").attr("content"); 
        var translation_id = $(this).data('translation-id');
        var object = $("#text_" + translation_id);
        var text = $("#text_" + translation_id).val();

        $.post({
            url: '/backend/translations/update/' + translation_id,
            data: {
                '_token': token,
                'text' : text
            },
            success: function(data, status) {
                if (data.trim() === "OK") {
                    object.animate({
                        backgroundColor: "#90EE90"
                    }, 300);
                    object.animate({
                        backgroundColor: "white"
                    }, 400).delay(400);
                } else {
                }            
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })        

    });

} );

</script>

@endsection
