@extends('layouts.app')

@section('content')

	<div class="container">

        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.general') }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

        <!-- TITLE -->
        <div class="row">
            <div class="col-md-offset-2 col-md-8 col-xs-12 text-center" style="padding-top: 20px;">
                <h2>{{ trans('backend.platform_properties') }}</h2>
            </div>
        </div>

        <!-- SERVER LOGO --> 
        <form action="/backend/upload/logo" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.server_logo') }} {!! tooltip(trans('text.tooltip_server_logo')) !!}
                </div>
                <div class="col-xs-12 col-md-2">
                    <img src="{!! logo() !!}?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                </div>
                <div class="col-xs-8 col-md-2">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        


        <!-- SERVER ROUND LOGO --> 
        <form action="/backend/upload/round_logo" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.server_round_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo')) !!}
                </div>
                <div class="col-xs-12 col-md-2">
                    <img src="/images/server/logo.png?{!! time() !!}" style="max-height: 40px; max-width: 200px;">
                </div>
                <div class="col-xs-8 col-md-2">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        

        <!-- SERVER ROUND LOGO TRANSPARENT --> 
        <form action="/backend/upload/round_logo_transparent" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.server_round_transparent_logo') }} {!! tooltip(trans('text.tooltip_server_round_logo_transparent')) !!}
                </div>
                <div class="col-xs-12 col-md-2">
                    <img style="background: #CCC; max-height: 40px; max-width: 200px;" src="/images/server/logo_transparent.png?{!! time() !!}">
                </div>
                <div class="col-xs-8 col-md-2">
                    <input type="file" name="image">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        


        <!-- TITLE --> 
        <?php $platform_title = server_property('platform_title'); ?>
        <form action="/backend/general" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.platform_title') }}
                </div>
                <div class="col-xs-8 col-md-4">
                    <input type="text" name="data" class="form-control" value="{{ $platform_title }}">
                    <input type="hidden" name="name" value="platform_title">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>     

 
        <!-- TEXT UNDER THE LOGO --> 
        <?php $slogan = server_property('slogan'); ?>
        <form action="/backend/general" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.slogan') }}
                </div>
                <div class="col-xs-8 col-md-4">
                    <input type="text" name="data" class="form-control" value="{{ $slogan }}">
                    <input type="hidden" name="name" value="slogan">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>      

        <hr>

        <!-- HOME PAGE VIDEO -->
        <div class="row">
            <div class="col-xs-12 text-center">
                <h4>{{ trans('text.home_page_video') }}</h4>
            </div>
        </div>

        <!-- preview -->
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                {!! homePageVideo() !!}
            </div>
        </div>  

        <!-- video upload -->
        <div class="row text-center">
            <form class="form-inline" action="/backend/upload_home_video" method="POST" enctype="multipart/form-data">  
                {{ csrf_field() }}
                <div class="col-md-4 col-md-offset-2 col-xs-12">
                    <input type="file" name="home_video" class="inline">
                </div>
                <div class="col-md-1 col-xs-12">
                    <button type="submit" class="btn btn-success pull-right">{{ trans('text.upload') }}</button>
                </div>
            </form>  

            <div class="col-md-2 col-xs-12">
                <form action="/backend/defaut_home_video" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}     
                    <button type="submit" class="btn btn-info pull-right">{{ trans('text.return_to_default') }}</button>
                </form> 
            </div>
            <div class="col-md-1 col-xs-12">
                <form action="/backend/delete_home_video" method="POST" enctype="multipart/form-data">        
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger pull-right">{{ trans('text.delete') }}</button>
                </form>
            </div>
        </div>
        
        <!-- video poster -->
        <div class="row text-center">
            <div class="col-xs-12">
                <h4>{{ trans('text.home_page_video_thumbnail') }}
            </div>

            <!-- show video poster -->
            @if (server_property('home_video_poster'))
                <div class="col-md-8 col-md-offset-2 col-xs-12 text-center padding-small">
                    <img src="{!! server_property('home_video_poster') !!}" class="image">
                </div>
            @else 
                {{ trans('text.no_video_poster') }}
            @endif
            
            <!-- upload video poster -->
            <form action="/backend/upload_home_video_poster" method="POST" enctype="multipart/form-data">  
                {{ csrf_field() }}
                <div class="col-md-6 col-md-offset-2 col-xs-12">
                    <input type="file" name="home_video_poster" class="inline">
                </div>
                <div class="col-md-1 col-xs-8">
                    <button type="submit" class="btn btn-success pull-right">{{ trans('text.upload') }}</button>
                </div>  
             </form>              
            <div class="col-md-1 col-xs-4">
                <form action="/backend/delete_home_video_poster" method="POST">  
                    {{ csrf_field() }}      
                    <button type="submit" class="btn btn-danger pull-right">{{ trans('text.delete') }}</button>
                </form>
            </div>                
        </div>

        <hr>

        <!-- CENTRAL SERVER PROPERTIES --> 
        <div class="row padding-small hover">
            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
                <h4>{{ trans('backend.central_server_properties') }}</h4>
            </div>
        </div>

        <!-- CENTRAL SERVER ADDRESS --> 
        <form action="/backend/general" method="POST">
            {{ csrf_field() }}
            <div class="row padding-small hover">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    {{ trans('text.central_server_address') }}
                </div>
                <div class="col-xs-8 col-md-4">
                    <input type="text" name="data" class="form-control" value="{{ server_property('central_server_address') }}">
                    <input type="hidden" name="name" value="central_server_address">
                </div>
                <div class="col-xs-4 col-md-2 text-right">
                    <button type="submit" class="btn btn-success">{{ trans('text.save') }}</button>
                </div>
            </div>
        </form>        

        <!-- REGISTRER ON CENTRAL SERVER -->
        @if (!server_property('central_server'))
            <div class="row padding-small hover">
                <div class="col-xs-2"></div>
                <div class="col-xs-4">
                    @if (server_property('server_id'))
                        {{ trans('text.server_id') }}: {!! server_property('server_id') !!}
                    @else 
                        {{ trans('backend.you_need_to_register_your_platform_on_central_server') }}
                    @endif
                </div>
                <div class="col-xs-4 text-right">
                    <a href="/backend/register_platform_request"><button type="button" class="btn btn-success">{{ trans('backend.register_platform_on_central_server') }}</button></a>
                </div>
            </div>
        @endif

        <hr>

        <!-- CHANGE LOG -->
        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-8 text-center">
                <h4>{{ trans('backend.changelog') }}</h4>
            </div>
        </div>

        <div class="row padding-small">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="changelog">
                    {!! nl2br(file_get_contents(public_path("changelog.txt"))) !!}
                </div>
            </div>        
        </div>
        
	</div>

@endsection
