@extends('layouts.app')

@section('content')

	<div class="container">
        <!-- HEADER -->
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>{{ trans('text.users') }}</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8 text-center" style="padding-top: 20px;">
                <!-- DATATABLE USERS -->
			    <table class="display" id="datatable">
			        <thead>
			            <tr>
			                <th>{{ trans('text.name') }}</th>
			                <th>{{ trans('text.last_name') }}</th>
                            <th>{{ trans('text.email') }}</th>
			                <th></th>
			            </tr>
			        </thead>
			    </table>
            </div>
		</div>
	</div>

    <script type="text/javascript">

    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/users/list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "name" },
                { "data": "last_name" },
                { "data": "email" },
            ],
            "aoColumnDefs": [
                {
                    "mRender": function ( data, type, row ) {
                        var edit_user_button = "<a href='/backend/users/edit/" + row['id'] + "'><button class='btn bck-lens' type='button' title='{{ trans('text.edit') }}'><i class='fas fa-pencil-alt' aria-hidden='true'></i></button></a> ";
                        var suspend_user_button = "<a href='/backend/users/suspend/" + row['id'] + "'><button class='btn btn-warning' type='button' title='{{ trans('users.suspend_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a> ";
                        var delete_user_button = "<a href='/backend/users/delete/" + row['id'] + "'> <button class='btn btn-danger' type='button' title='{{ trans('users.delete_user') }}'><i class='fa fa-times' aria-hidden='true'></i></button></a>";
                        return edit_user_button + " " + suspend_user_button + " " + delete_user_button;
                    },  
                    "aTargets": [ 3 ]
                },
            ]        
        });
        /*
            // Open course on click     
            $('#datatable tbody').on('click', 'tr', function () {
                var data = table.row( this ).data();
                location.href = '/backend/users/edit/' + data['id'];
            });
        */
    </script>    
@endsection
