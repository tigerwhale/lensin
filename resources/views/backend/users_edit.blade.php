@extends('layouts.app')

@section('content')

	<div class="container">
        <div class="row">

            <!-- CONTENT -->
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8" style="padding-top: 20px;">
                <h2 style="text-align: center;">User Details</h2>
                <!-- content header -->
                <div class="row content-header">
                    <!-- middle -->
                    <div class="col-xs-12 text-center">
                        @include('backend.header_menu')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <!-- USER INFORMATION -->
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-4" style="padding-top: 20px;">

                <h4>{{ trans('text.general_information') }}</h4>
                <div class="row">
                    <div class="col-xs-12">
                        
                        <!-- name --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.name') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->name }}" placeholder="{{ trans('text.insert_name') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- last_name --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.last_name') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="last_name" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->last_name }}" placeholder="{{ trans('text.insert_last_name') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- user_type --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.user_type') }}
                            </div>
                            <div class="col-xs-9">
                                {!! Form::select('user_type_id', $user_types, $user->user_type_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}
                            </div>
                        </div>                
                        <!-- school --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.school') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="school" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->school }}" placeholder="{{ trans('text.insert_school') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- departement --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.departement') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="departement" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->departement }}" placeholder="{{ trans('text.insert_departement') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- position --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.position') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="position" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->position }}" placeholder="{{ trans('text.insert_position') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- address --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.address') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="address" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->address }}" placeholder="{{ trans('text.insert_address') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- country --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.country') }}
                            </div>
                            <div class="col-xs-9">
                                {!! Form::select('country_id', $countries, $user->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) !!}
                            </div>
                        </div>
                        <!-- interrest --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.interest') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="interest" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->interest }}" placeholder="{{ trans('text.insert_interest') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- phone --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.phone') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="phone" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->phone }}" placeholder="{{ trans('text.insert_phone') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>
                        <!-- web --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.web') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="text" name="web" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->web }}" placeholder="{{ trans('text.insert_web') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>  
                        <!-- email --> 
                        <div class="row">
                            <div class="col-xs-3">
                                {{ trans('text.email') }}
                            </div>
                            <div class="col-xs-9">
                                <input  type="email" name="email" data-model="backend/users" data-id="{{ $user->id }}" value="{{ $user->email }}" placeholder="{{ trans('text.insert_email') }}" class="form-control update_input" required="" style="background-color: rgb(255, 255, 255);">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- USER PRIVILEDGES -->
            <div class="col-xs-2" style="padding-top: 20px;">
                <h4>{{ trans('text.user_priviledges') }}</h4>
                <div class="row">
                    <div class="col-xs-12">
                        @foreach ($user_priviledges as $user_priviledge)
                            @if (server_property('central_server') != 1 && $user_priviledge->slug == "manageservers")
                            @else 
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>
                                            <input type="checkbox" name="user_roles" data-url="/backend/user_roles/update/{{ $user->id }}" data-id="{{ $user_priviledge->id }}" value="" placeholder="{{ trans('text.insert_name') }}" class="update_checkbox" required="" style="background-color: rgb(255, 255, 255);" {!! $user->hasRole($user_priviledge->slug) ? 'checked' : '' !!} >
                                            {{ trans('text.' . $user_priviledge->slug) }}
                                        </label>
                                        
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <!-- USER IMAGE -->
            <div class="col-xs-2" style="padding-top: 20px;">
                <h4>User Image</h4>
                <!--{!! coverImageEdit($user) !!}-->
            </div>

        </div>




	</div>


    <script type="text/javascript">

    	var values = [];
        var table = $('#datatable').DataTable({
            "processing": true,
            "stateSave": true, 
            "dom": '<"top"lf>rt<"bottom"ip><"clear">',
            "ajax": {
                "cache":true,
                "url" : '/users/list',
                "rowId": 'id',
            },
            "columns": [
                { "data": "name" },
                { "data": "last_name" },
                { "data": "email" },
            ]
        });

        // Open course on click     
        $('#datatable tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
            location.href = '/backend/users/edit/' + data['id'];
        });

    </script>    
@endsection
