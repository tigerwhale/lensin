@extends('layouts.app')

@section('content')

	<div class="container">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2>Admin</h2>
            </div>
            <div class="col-xs-12 text-center">
                @include('backend.header_menu', ['no_back_button' => true])
            </div>
        </div>

		<div class="row">
            <!-- CONTENT -->
            <div class="col-xs-8 col-xs-offset-2 text-center">
                <br>
                {{ trans('text.backend_welcome_text') }}
			</div>
		</div>
	</div>
 
@endsection
