<?php 
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
?>
<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header" id='main_modal_header'>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>{{ trans('text.modify') }}</h4>
		</div>
		<div class="modal-body" id="main_modal_body">
			<div class="row">

				<!-- LEFT PART - IMAGE -->
				<div class="col-xs-3">
					<!-- cover image -->
					<div class="row padding-small text-center">
						{{ strtoupper(trans('text.cover_image')) }}
						<div class="col-xs-12" id="cover_image_container_{!! $members_group->id !!}">
							{!! coverImageEdit($members_group) !!}
						</div>
					</div>
				</div>

				<!-- RIGHT PART - CONTENT -->
				<div class="col-xs-9">
					<!-- NAME -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.title')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="title" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- description -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.description')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="description" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->description }}" placeholder="{{ strtoupper(trans('text.description')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- reference_person -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.reference_person')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="reference_person" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->reference_person }}" placeholder="{{ strtoupper(trans('text.reference_person')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- reference_person_email -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.reference_person_email')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="reference_person_email" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->reference_person_email }}" placeholder="{{ strtoupper(trans('text.reference_person_email')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- address -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.address')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="address" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->address }}" placeholder="{{ strtoupper(trans('text.address')) }}" class="form-control update_input" required>
						</div>
					</div>
					
					<!-- web_site -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.web_site')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="web_site" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->web_site }}" placeholder="{{ strtoupper(trans('text.web_site')) }}" class="form-control update_input" required>
						</div>
					</div>

					<!-- web_site -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							{{ strtoupper(trans('text.email')) }}
						</div>
						<div class="col-xs-12">
							<input type="text" name="email" data-model="member_group" data-id="{!! $members_group->id !!}" value="{{ $members_group->email }}" placeholder="{{ strtoupper(trans('text.email')) }}" class="form-control update_input" required>
						</div>
					</div>

			        <!-- Country --> 
			        <div class="row top-padding-small">
			            <div class="col-xs-12">
			                {{ trans('text.country') }}
			            </div>
			            <div class="col-xs-12">
			                {{ Form::select('country_id', $countries , $members_group->country_id, ['class' => 'form-control update_input', 'data-model' => "member_group", 'data-id' => $members_group->id, 'placeholder' => trans('text.select_country')]) }}
			            </div>
			        </div>

			        <!-- type --> 
			        <div class="row top-padding-small">
			            <div class="col-xs-12">
			                {{ trans('text.type') }}
			            </div>
			            <div class="col-xs-12">
			                {{ Form::select('type', $memberGroupTypes , $members_group->type, ['class' => 'form-control update_input', 'data-model' => "member_group", 'data-id' => $members_group->id, 'id'=> 'member_type']) }}
			            </div>
			        </div>

			        <!-- upgrade to partner --> 
			        @if (!$members_group->partner)
				        <div class="row top-padding-small">
				            <div class="col-xs-12">
				            	<form action="/backend/upgrade_member_to_partner/{!! $members_group->id !!}">
				            		<button type="submit" class="btn btn-success">{{ trans('users.upgrade_member_to_partner') }}</button>
				            	</form>
				            </div>
				        </div>
				    @else 
				    	<div class="row top-padding-small">
				            <div class="col-xs-12">
				            	<form action="/backend/downgrade_partner_to_member/{!! $members_group->id !!}">
				            		<button type="submit" class="btn btn-success">{{ trans('users.downgrade_partner_to_member') }}</button>
				            	</form>
				            </div>
				        </div>
				    @endif
				</div>
			</div>
		</div>
	</div>
</div>

