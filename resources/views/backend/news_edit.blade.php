<div class="modal" tabindex="-1" role="dialog" id="apiModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" id='main_modal_header'>
				<h4>{{ trans('text.modify') }}</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="main_modal_body">
				<div class="row">

					<!-- LEFT PART - IMAGE -->
					<div class="col-3">
						<!-- cover image -->
						{{ strtoupper(trans('text.cover_image')) }}
						<div id="cover_image_container_{!! $news->id !!}">
							{!! coverImageEdit($news) !!}
						</div>
					</div>

					<!-- RIGHT PART - CONTENT -->
					<div class="col-9">
						<!-- NAME -->
						<div class="form-group mt-3">
							<label for="news_title">{{ strtoupper(trans('text.title')) }}</label>
							<input type="text"
								   id="news_title"
								   name="title"
								   data-model="news"
								   data-id="{!! $news->id !!}"
								   value="{{ $news->title }}"
								   placeholder="{{ strtoupper(trans('text.insert_title')) }}"
								   class="form-control update_input" required>
						</div>
						<!-- TEXT -->
						<div class="form-group mt-3">
							<label for="news_text">{{ strtoupper(trans('text.text')) }}</label>
							<textarea id="news_title"
									  name="text"
									  id="news_text"
									  data-model="news"
									  data-id="{!! $news->id !!}"
									  value="{{ $news->text }}"
									  placeholder="{{ strtoupper(trans('text.text')) }}"
									  rows="8"
									  class="form-control update_input" required>{{ trim($news->text) }}</textarea>
						</div>

						<!-- Language -->
						<div class="form-group mt-3">
							<label for="news_text">{{ strtoupper(trans('text.language')) }}</label>
							{{ Form::select('language_id', $languages, $news->language_id, ['class' => 'form-control update_input', 'data-model' => "news", 'data-id' => $news->id, 'placeholder' => trans('text.select_language')]) }}
						</div>

						<!-- Publish -->
						<div class="form-group mt-3">
							<button class="btn btn-success js-publish-news">{!! trans('text.publish') !!}</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>