<!-- =============
	 LEFT MENU PHP -->

<?php 
	$languages = \App\Language::where('published', 1)->get();
	$switches = ['courses', 'lectures', 'tools', 'study_cases', 'projects'];
	$platforms_list = central_server_data('/api/server_list');
	$platforms = platforms_list();
	$this_server_id = server_property('server_id');
	$central_server = server_property('central_server');
	$active_switch = (isset($active_switch)) ? $active_switch : "";
	$resource_switch = (isset($_GET['res']) ? $_GET['res'] : []);
?>


<!-- ==============
	 LEFT MENU HTML -->

	<div class="left_filter_menu left_edit_mode" style="margin-top: 26px; padding-top: 21px;">

		<!-- RESOURCE SWITCHES -->
		<h4  class="resources_title edit_resource_buttons">{{ strtoupper(trans('text.resources')) }}</h4>
		<h4 style="display: none;" class="resources_title edit_resource_buttons">{{ strtoupper(trans('text.edit')) }}</h4>

		<!-- Removed for central - && server_property('central_server') != 1 -->
		@if (Auth::check() && Auth::user()->isOne('managecourses|managestudycases|managetools|manageprojects|managenews'))

			<!-- manage button -->
			<div style="margin-bottom: 20px;">
				@include('home_manage_button')	
			</div>

		@endif
		
		<div class="edit_resource_buttons">
		@foreach ($switches as $switch)
			<?php 
				$checked = "";
				// if home page
				if (isset($home_page)) {
					$checked = "";
					// if any checked
					if ($resource_switch) {
						$checked = "";
						// if this switch is checked
						if (in_array($switch, $resource_switch)) {
							$checked = "checked";
						}
					}
				} else {
					if ($active_switch == $switch) {
						$checked = "checked";
					}
				}
			?>
		
			<label class="switch type">
			  <input type="checkbox" id="{!! $switch !!}_switch" class="resources type" value="{!! ($switch != 'news') ? substr($switch, 0, -1) : 'news' !!}" {!! $checked !!}>
			  <div class="slider round {!! $switch !!}_slider"></div>
			</label>
			<div class="resource_name" id="{!! $switch !!}_title">{{ strtoupper(trans('text.' . $switch)) }}</div>
		@endforeach
		</div>

		<br>
		{!! social_icons() !!}
		<img src="/images/erasmus.png" class="left_menu_logo" >
		<img src="/images/by.png">
		<br>
	</div>


<!-- ============
	 LEFT MENU JS -->

	<script type="text/javascript">

		// HIDE HOME INFO
		$(document).ready(function(){
			hideHomeInfo()
		});

		function hideHomeInfo(){
			if($('.resources:checkbox:checked').length > 0) {
				$('#home_info').hide();
				$('#news').hide();
			} else {
				$('#home_info').show();
				$('#news').show();
			}
		}

		// FILTER DATATABLE RESOURCES
		$('.resources').click(function() {
			$("#loaderContainer").show();
			setTimeout(function(){
				var resources = "";
				// Get checked values
		        $('.resources:checkbox:checked').each(function() {
		            resources += $(this).val() + "|";
		        });
			
				// if the page is front page 
				@if (isset($home_page)) 
					hideHomeInfo();
					checkDatatableFilters();
				// ELSE IF OTHER PAGES
				@else  
					resources = resources.replace(/\|/g, "s&res[]=");
					window.location = "{!! url('/') !!}?res[]=" + resources;
				@endif 
			}, 100);
		});

		// ANIMATE NEWS
		$(function() {
			$( "#slider" ).slider({
				value : 0,
				min : 0,
				max : 1,
				step : 1,
				animate : true,
				slide : function( event, ui ) {
					$( "#amount" ).val( "$" + ui.value );
				}
			});
			$( "#amount" ).val( "$" + $( "#slider" ).slider( "value" ) );
		  });


	  	// INIT DATATABLES
		function init_datatable_parameters(table) {

			var resources = "";

			$('.resources:checkbox:checked').each(function() {
				resources += $(this).val() + "|";
			});

			// remove the last "|"
			resources = resources.substring(0, resources.length - 1);

			// search the data and redraw table
			table.column( 6 ).search(resources, true).draw();

		}  
	</script>