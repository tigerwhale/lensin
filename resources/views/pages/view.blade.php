@extends('layouts.app')

@section('content')
    @if ($page)

	<div class="container" style="margin-top: 20px;">
		<div class="row">

            <!-- CONTENT -->
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8" style="padding-top: 20px;"">
                <h2 style="text-align: center;">{{ page_title($page->permalink) }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">
            </div>
            <div class="col-xs-8">
                {!! page_text($page->permalink) !!}
            </div>
        </div>
    </div>

    @else 
        No page
    @endif

@endsection
