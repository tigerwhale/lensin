@extends('layouts.app')

@section('content')
    
	<div class="container" style="margin-top: 20px;">
		<div class="row">
            <!-- CONTENT -->
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8" style="padding-top: 20px;"">
                <h2 style="text-align: center;">{{ trans('text.network') }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2">
            </div>
            <div class="col-xs-8">

                <!-- NETWORKS LIST -->
                @foreach ($networks as $network)

                    <div class="panel-group" >
                        <div class="panel panel-default" id="network_panel_{{ $network->id }}">

                        <div class="panel-heading">
                            <a data-toggle="collapse" href="#network_{{ $network->id }}" class="collapsed" aria-expanded="false">
                                <div class="row">
                                    <div class="col-xs-11">
                                        <h4>{{ $network->name }}</h4><i class="fa float-right"></i>
                                    </div>
                                    <div class="col-xs-1">
                                        <i class="fa fa-caret-down pull-right fa-lg" aria-hidden="true" style="margin-top: 14px;"></i>  
                                    </div>
                                </div>
                            </a>                            
                        </div>

                        <div id="network_{{ $network->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                @if ($network->description)
                                    {{ $network->description }}
                                    <br><br>
                                @endif
                                <!-- MEMBERS LIST -->
                                @if ($network->top_members->count())
                                    <strong>{{ strtoupper(trans('text.members_and_contacts')) }}</strong>
                                    <br><br>
                                    @foreach ($network->top_members as $member)
                                        <strong>{{ $member->name }}</strong>
                                        <br>
                                        @if ($member->contacts->count())
                                            @foreach($member->contacts as $contact)
                                                {{ $contact->name }} | {{ Html::mailto($contact->email) }} <br>
                                            @endforeach
                                        @endif
                                        <br>
                                    @endforeach
                                @endif 
                            </div>
                        </div>
                    </div>
                    <br>

                @endforeach




            </div>
        </div>
    </div>
    
@endsection
