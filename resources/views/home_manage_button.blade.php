<!-- Removed for central - && server_property('central_server') != 1 -->
    <div class="round_button bck-lens" style="display: inline-block;" id="manage_button">
        <i class="fas fa-pencil" aria-hidden="true"></i>
    </div>

    <div class="edit_resource_buttons" style="display: none; margin-top: 20px;">
        <!-- MANAGE COURSES -->
        @if (Auth::user()->isRole('managecourses'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/courses/modify" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-course-small btn_35 shadow-small" id="edit_courses" ></div>
                    </a>
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.courses')) }}
                </div>
            </div>
        @endif

        <!-- MANAGE LECTURES -->
        @if (Auth::user()->isRole('managestudycases'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/lectures/modify" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-lecture-small btn_35 shadow-small" id="edit_lectures" ></div>
                    </a>
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.lectures')) }}
                </div>
            </div>
        @endif

        <!-- MANAGE TOOLS -->
        @if (Auth::user()->isRole('managetools'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/tools/modify" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-tool-small btn_35 shadow-small" id="edit_tools" ></div>
                    </a>
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.tools')) }}
                </div>
            </div>
        @endif

        <!-- MANAGE CASE STUDIES -->
        @if (Auth::user()->isRole('managestudycases'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/study_cases/modify" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-study_case-small btn_35 shadow-small" id="edit_courses" ></div>
                    </a>
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.study_cases')) }}
                </div>
            </div>
        @endif

        <!-- MANAGE CHALLANGES -->
        @if (Auth::user()->isRole('manageprojects'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/projects" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-project-small btn_35 shadow-small" id="edit_challenges"></div>
                    </a>
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.projects')) }}
                </div>
            </div>
        @endif

        <!-- MANAGE CHALLANGES -->
        @if (Auth::user()->isRole('managenews'))
            <div class="row">
                <div class="col-xs-12" style="padding-bottom: 6px;">
                    <a href="/news/modify" onclick="toggleMenu();">
                        <div style="margin: 0 auto;" class="btn-news-small btn_35 shadow-small" id="edit_challenges"></div>
                    </a> 
                </div>
                <div class="col-xs-12 resource_name">
                    {{ strtoupper(trans('text.news')) }}
                </div>
            </div>
        @endif


    </div>

<script>
    $('#manage_button').click(function(){ 
        $('.edit_resource_buttons').each(function(i, obj) {
            $(obj).toggle();
        });
        $('.left_edit_mode').toggleClass('left_menu_edit');
    })
</script>
