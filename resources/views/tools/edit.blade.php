@extends('layouts.resource')

@section('content')

	<div class="container">

    {!! backendHeader('tools', 'tools', strtoupper(trans('text.tools')), 'tool')  !!}

	<div class="row">

		<!-- LEFT PART - IMAGE -->
		<div class="col-xs-3">
			
			<!-- cover image -->
			<div class="row padding-small text-center">
				{{ strtoupper(trans('text.cover_image')) }}
				<div class="col-xs-12" id="cover_image_container_{!! $tool->id !!}">
					{!! coverImageEdit($tool) !!}
				</div>
			</div>	

		</div>


		<!-- RIGHT PART - CONTENT -->
		<div class="col-xs-9">

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('tools.tool_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="title" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- Author -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.author')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="author" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->author }}" placeholder="{{ strtoupper(trans('text.author')) }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- Institution -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.institution')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="institution" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->institution }}" placeholder="{{ strtoupper(trans('text.institution')) }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- LANGUAGE -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.language')) }}
				</div>
				<div class="col-xs-10">
					{!! Form::select('language_id', $languages, $tool->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'tools', 'data-id'=> "$tool->id", 'placeholder' => trans('text.select_language') ]) !!}
				</div>
			</div>		

			<!-- CATEGORY -->
			<div class="row"  style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.category')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="category" list="categories" data-model='tools' data-id={{ $tool->id }} value="{{ $tool->category }}" placeholder="{{ strtoupper(trans('text.insert_category')) }}" class="form-control update_input" required>
					<datalist id="categories">
						@foreach ($category_list as $category) 
							<option value="{{ $category }}">
						@endforeach
					</datalist>
				</div>
			</div>

			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.description')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='tools' data-id={{ $tool->id }}>{{ $tool->description }}</textarea>
				</div>
			</div>

			<!-- FILE -->
			<div class="row" style="margin-top: 20px" >
				<div class="col-xs-2">
					{{ strtoupper(trans('text.tools')) }}
				</div>
				<div class="col-xs-10" id="file_list">
					@include('tools.edit_file_list', ['tool' => $tool])
				</div>
			</div>
		</div>
	</div>



<script type="text/javascript">
	// DROPZONE COVER IMAGE
	$("#tool_cover_image_upload").dropzone({ 
		url: "/tools/upload_cover_image/{{ $tool->id }}",
		paramName: "image",
		previewsContainer: false,
		uploadMultiple: false,
		autoDiscover: false,

		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $("#tool_cover_image_text").html(parseInt(progress) + "%");
		},

		success: function(file, responce) {

			// change the image on upload
			if (responce != "upload_error") {
				
				$('#tool_cover_image_upload').css("background-image", "url(" + responce + ")");  
				$('#tool_cover_image_text').html("");  

			} else {

				alert ("{{ trans('text.upload_error') }}");
			}

		},

		// error return function
		error: function (file, responce){
			$("#tool_upload").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
		},

		init: function (){
			
			myDropzone = this;
		},
	});

	// Remove cover image
	$('.remove_tool_image_button').click( function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var id = $(this).data('id');
	    $.ajax({
            data: { 
                '_token': token,
            },
            url: "/tools/remove_cover_image/" + id,
            type: 'POST',

            success: function(response) {
                if (response) {

					$('#tool_cover_image_upload').css("background-image", "none");  
					$('#tool_cover_image_text').html("{{ trans('text.drop_or_click_to_select_image') }}");  

                } else {
					bootbox.alert (response);
                }
                
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
	});
	
	// Back to tols list page
    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/tools/modify';
        return false;
    }); 

    // Load file/preview list
	function toolFileList(tool_id) {
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/tools/file_list/' + tool_id,
			type: 'POST',
			data: {
				'_token': token,
		    },
			success: function(data, status) {
				$("#file_list").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});		
	}

	// Delete tool file
	$('body').on('click', '.delete_tool_file', function() {
 		
 		var token = $("meta[name='csrf-token']").attr("content"); 
        var id = $(this).data('tool-id');
	    $.ajax({
            data: { 
                '_token': token,
            },
            url: "/tools/delete_file/" + id,
            type: 'POST',

            success: function(response) {
				toolFileList(id);
            },
            error: function(response){
                bootbox.alert(response);
            }
       });		
	});

	// Add tool preview
	$('body').on('click', '.tool_add_preview', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var tool_id = $(this).data('tool-id');
		var link = $(".tool_link").val();

		$.ajax({
			url: '/tools/update_data',
			type: 'POST',
			data: {
				'_token': token,
				'value': link,
				'id': tool_id,
				'name': 'link',
		      },
			success: function(data, status) {
				toolFileList(tool_id);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});	
	});


	// Add tool preview
	$('body').on('click', '.delete_tool_preview', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var tool_id = $(this).data('tool-id');

		$.ajax({
			url: '/tools/update_data',
			type: 'POST',
			data: {
				'_token': token,
				'value': "",
				'id': tool_id,
				'name': 'link',
		      },
			success: function(data, status) {
				toolFileList(tool_id);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});	
	});
		
</script>

@endsection
