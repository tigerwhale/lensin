@if ($tool->filename)
	<!-- FILE -->
	<div class="row" style="padding-bottom: 10px;" id="file_div">
		<div class="col-xs-10">
			<div id="filename" style="width:100%; word-wrap: break-word">
				{!! $tool->filename ? $tool->filename : trans('text.no_file') !!}
			</div>
		</div>
		<div class="col-xs-2 text-right">
			<button type="button" class="btn round_button btn-danger delete_tool_file" data-tool-id="{!! $tool->id !!}">
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
		</div>
	</div>	
@endif 

@if ($tool->link)
	<!-- PREVIEW -->
	<div class="row" style="padding-bottom: 10px;" id="file_div">
		<div class="col-xs-12">
			<div class="input-group">
				<input type="text" id="link" name="link" data-model='tools' data-id={!! $tool->id !!} value="{{ $tool->link }}" class="form-control update_input" >
				<span class="input-group-btn">
					<button type="button" class="btn btn-danger delete_tool_preview" data-tool-id="{!! $tool->id !!}">
	            		<i class="fa fa-times" aria-hidden="true"></i>
	        		</button>
				</span>
			</div>
		</div>
	</div>	
@endif 


@if (!$tool->filename && !$tool->link)
	<div style="background-color: #EEE; padding: 5px;">
		<!-- FILE UPLOAD -->
		<div class="row" id="upload_div">
			<div class="col-xs-12">
				<!-- DROPZONE -->
				<div class="dropzeon dropzone_div" id="tool_upload">{{ trans('text.click_to_select_file') }}</div>
			</div>
		</div>
		<!-- PREVIEW -->
		<div class="row" id="preview_div" style="margin-top: 10px;">
			<div class="col-xs-12">
				<div class="input-group">
					<input type="text" name="tool_link" data-id={!! $tool->id !!} class="form-control tool_link" placeholder="{{ trans('text.or_insert_link') }}">
					<span class="input-group-btn">
						<button type="button" class="btn btn-success tool_add_preview" data-tool-id="{!! $tool->id !!}">
		            		<i class="fa fa-plus" aria-hidden="true"></i>
		        		</button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">

		var token = $("meta[name='csrf-token']").attr("content"); 
	  	var progressBar_tool_{{ $tool->id }} = document.getElementById('progressBar_tool_{{ $tool->id }}');
	  	var progressOuter_tool_{{ $tool->id }} = document.getElementById('progressOuter_tool_{{ $tool->id }}');

		// DROPZONE - upload resouce and open tool modal
		$("div#tool_upload").dropzone({ 
			url: "/tools/upload_file/{{ $tool->id }}",
			paramName: "resource_file",
			previewsContainer: false,
			uploadMultiple: true,
			parallelUploads: 100,

			sendingmultiple: function(files, xhr, formData){
			
				var paths = [];
				files.forEach( function(item, index){
					paths.push(item.fullPath);
				})
				console.log('----' + paths);
				formData.append( "paths", paths);
				formData.append( "_token", $("meta[name='csrf-token']").attr('content'));
				
			},

			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#tool_upload").html(parseInt(progress) + "%");
			},

			successmultiple: function(file, responce) {

				var responce_json = JSON.parse(responce);

				// If multiple
				if (responce_json[0] == "multiple") {

					var files = responce_json[2].split(",");
					var file_prompt = [];
					files.forEach( function(item, index){
						file_prompt.push({text: item, value: item, className: "select_preview_file"});
					})
					// open file choise
					bootbox.prompt({
					    title: "Select file for preview:",
					    inputType: 'select',
					    inputOptions: file_prompt,
					    closeButton: false,
					    callback: function (result) {
			        		$.ajax({
								data: { 
								    '_token': $("meta[name='csrf-token']").attr("content"),
								    'preview': result,
								    'id' : responce_json[1],
								    'name': 'link',
								    'value': result
								},
								url: '/tools/update_data',
								type: 'POST',
								success: function(response) {
									toolFileList({!! $tool->id !!});
								},
								error: function(response){
								    bootbox.alert (response);
								}
							});
					    }
					});

				} 
				// If single
				else if (responce_json[0] == "single")			
				{
					toolFileList({!! $tool->id !!});
				} else 
				{
					bootbox.alert ("{{ trans('text.upload_error') }}");
					toolFileList({!! $tool->id !!});
				}	

				// Return text to "click here..."
				$("#tool_upload").html("{{ trans('text.click_to_select_file') }}");
				
			},

			// error return function
			error: function (file, responce){
				bootbox.alert ("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			},

			init: function (){
				myDropzone = this;
			}
		});
	</script>
@endif