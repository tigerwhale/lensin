@extends('layouts.resource')

@section('content')

<div class="container">

    {!! backendHeader('tools', 'tools', strtoupper(trans('text.tools')), 'tool', true)  !!}

	<div class="row">
		<div class="col-xs-12 text-center">		
	        <!-- DATATABLE SEARCH -->
		    <table class="display" id="datatable">
		        <thead>
		            <tr>
		                <th class="col-xs-1"></th>
		                <th class="col-xs-2">{{ trans('text.category') }}</th>
		                <th class="col-xs-2">{{ trans('text.title') }}</th>
		                <th class="col-xs-5">{{ trans('text.description') }}</th>
		                <th class="col-xs-2"></th>
		            </tr>
		        </thead>
		    </table>
		</div>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/tools/datatable_list',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "category" },
            { "data": "title" },
            { "data": "description" },
            { "data": "published" }
        ], 

		"aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {

            	var modify = '<a title="{{ trans('text.modify') }}" href="/tools/edit/' + row['id'] + '"><div class="btn bck-lens"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                var publish = publishButton(row['published'], 'tool', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
            	var delete_button = '<div class="btn btn-danger delete_tool" title="{{ trans('text.delete') }}" data-tool-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + publish + delete_button;
            },
            "aTargets": [ 4 ]
        }]

    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    $('body').on('click', '.delete_tool', function() {

        var tool_id = $(this).data('tool-id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_tool') }}", function(result){ 
            // if OK, delete the model
            if (result) {
                window.location.href = '/tools/delete/' + tool_id;
            }

        });

    });


</script>


@endsection