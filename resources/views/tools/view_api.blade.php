<!-- TOOL HEADER -->
    <div class="row content-header">
        
        <!-- left button -->
        <div class="col-xs-3">
    		{!! left_back_button('tools') !!}
        </div>
        
        <!-- middle -->
        <div class="col-xs-6 text-center">
        	<h2 style="margin-bottom: 0px;">{{ trans('text.tool') }}</h2>
    	</div>
        
        <!-- right -->
        <div class="col-xs-3">
            {!! view_counter_icons($tool) !!}
        </div>

    </div>

<!-- TOOL TITLE -->
    <div class="row">
        <!-- title -->
    	<div class="col-xs-12">
            <div class="row" style="margin-bottom: 10px;">
            	<div class="col-xs-12">
            		<span class="title">{{ strtoupper(trans('text.title')) }}</span>
    				<table style="margin-top: 5px; max-width: 100%;">
    					<tr>
    						<td style="padding-right: 10px;">
    							<div class="btn-tool-small pull-left">&nbsp;</div>
    						</td>
    						<td class="resource-title text-tools">
    							{{ $tool->title }}
    						</td>
    					</tr>
    				</table>
            	</div>

                <!-- LEFT -->
                <div class="col-xs-12 col-md-4"> 
                   	<!-- Cover image--> 
                	{!! coverImage($tool, 'tool_big.png') !!}
            		
                	<!-- Download link --> 
                	@if ($tool->filename || $tool->link)
                		<span style="line-height: 45px;">{{ strtoupper(trans('text.download')) }}</span>
    					{!! downloadLinkAndIncrement("api/tools/increment_download/" . $tool->id, $request) !!}
                	@endif

                	<!-- Language -->
                	{!! isset($tool->languageName) ? titleAndTextRow('text.language', $tool->languageName) : "" !!}
                	<!-- Author -->
                	{!! isset($tool->author) ? titleAndTextRow('text.author', $tool->author) : "" !!}
                	<!-- Institution -->
                	{!! isset($tool->institution) ? titleAndTextRow('text.institution', $tool->institution) : "" !!}
    			</div>

    			<!-- RIGHT -->		
                <div class="col-xs-12 col-md-8"> 
    				<!-- file preview-->
    				@if ($tool->filename && isset($request->user_id))
    					@include('common.file_link_view', ['filename' => $tool->filename, 'noLink' => 1])
    				@endif

                    <!-- category -->
                    @if (isset($tool->category))
                        <div class="row padding-small">
                            <div class="col-xs-12 col-md-4">
                                {{ strtoupper(trans('text.category')) }}
                            </div>
                            <div class="col-xs-12 col-md-8">
                                {{ $tool->category }}
                            </div>
                        </div>
                    @endif

                    <!-- description -->
                    @if (isset($tool->category))
                        <div class="row padding-small">
                            <div class="col-xs-12 col-md-4">
                                {{ strtoupper(trans('text.description')) }}
                            </div>
                            <div class="col-xs-12 col-md-8">
                                {{ $tool->description }}
                            </div>
                        </div>
                    @endif

    			</div>
            </div>
    	</div>
    </div>