@extends('layouts.app')

@section('content')

    <!-- TOOLS -->
	<div class="container">
        <div class="row">

    		<!-- MENU LEFT -->
    		<div class="col-xs-2">
    			@include('left_menu_filter', ['active_switch' => "tools"])
    		</div>

    		<!-- CONTENT -->
    		<div class="col-xs-8" id="tool_content" style="margin-top: 20px;">
    			<div class="text-center" style="width: 100%;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
    		</div>

    		<!-- MENU RIGHT -->
            <div class="col-xs-2">
                @include('right_menu_filter', ['home_page' => 1])
            </div>
            
        </div>
	</div>

    <!-- TOOLS AJAX -->
	<script type="text/javascript">
		var token = $("meta[name='csrf-token']").attr("content"); 

        $.ajax({
            url: "{!! $apiURL !!}",
            type: 'post',
            data: {
                '_token': token,
                'user_id': '{!! Auth::user() ? Auth::user()->id : "" !!}',
              },
            success: function(data, status) {
                $('#tool_content').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
	</script>
@endsection
