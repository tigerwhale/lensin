@extends('layouts.resource')

@section('content')

	<div class="container">

		<form method="POST" action="/tools/create" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="row bck-tools resource_header">
				<!-- left text -->
				<div class="col-xs-4 text-left resource_header_title">
					<table>
						<tr>
							<td class="edit-mode-header-tr">
								+
							</td>
							<td style="vertical-align: middle; padding-left: 10px;">
								{{ strtoupper(trans('text.create_mode')) }}
							</td>
						</tr>
					</table>
				</div>

				<!-- middle buttons -->
				<div class="col-xs-4">
					<table style="margin-left: auto; margin-right: auto; margin-top: 5px;">
						<tr>
							<td style="padding: 5px;">
								<button class="btn btn-success" type="submit">
									<i class="fa fa-check" aria-hidden="true"></i> 
									{{ strtoupper(trans('text.confirm')) }}
								</button>
							</td>
							<td style="padding: 5px;">
								<button class="btn btn-danger back-button" type="button">
									<i class="fa fa-times" aria-hidden="true"></i>
									{{ strtoupper(trans('text.cancel')) }}
								</button>
							</td>
						</tr>
					</table>			
				</div>
				
				<!-- right text -->
				<div class="col-xs-4 text-right resource_header_title">
					<table style="float: right;">
						<tr>
							<td style="vertical-align: middle; padding-right: 10px;">
								{{ strtoupper(trans('text.tools')) }}
							</td>
							<td class="edit-mode-header-tr">
								<i class="fa fa-wrench" aria-hidden="true"></i>
							</td>
						</tr>
					</table>
				</div>			
			</div>
			
				<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('tools.tool_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="title" class="form-control" placeholder="{{ strtoupper(trans('text.insert_title')) }}" required>
				</div>
			</div>

			<!-- CATEGORY -->
			<div class="row"  style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.category')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="category" class="form-control" placeholder="{{ strtoupper(trans('text.category')) }}" required>
				</div>
			</div>

			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.description')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control" placeholder="{{ strtoupper(trans('text.description')) }}"></textarea>
				</div>
			</div>


			<!-- FILE -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.file')) }}
				</div>
				<div class="col-xs-10">
					<input type="file" name="file">
				</div>
			</div>
				
		</div>

	</form>



<script type="text/javascript">
	
    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}';
        return false;
    }); 

</script>

@endsection
