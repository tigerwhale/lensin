<ul class="sortable_ul ui-sortable">
	
	@foreach ($project->resources as $resource) 

		<script type="text/javascript">
			resource_list.push({!! $resource->id !!});	
			@if($loop->first)
				if (first_load){
					show_resource_preview('{!! url('/') !!}', {!! $resource->id !!});
				}
			@endif 
		</script>

		<li class="row edit_project_resource hover" data-id="{!! $resource->id !!}" style="list-style-type: none;">
			<!-- name -->
			<div class="col-xs-{!! $edit ? 7 : 12 !!} show_resource_preview" data-url="{{ url('/') }}" data-id="{!! $resource->id !!}" style="cursor: pointer;">
				@if ($edit)
					<i class="fas fa-arrows-alt-v" aria-hidden="true"></i>
				@endif
				<label class="round_button bck-project icon">{!! ($resource->resource_type) ? $resource->resource_type->icon : '<i class="fa fa-ellipsis-h" aria-hidden="true"></i>' !!}</label> {{ $resource->name }}
			</div>
			<!-- sort and select -->
			@if ($edit)
				<div class="col-xs-5 text-right" style="padding-top: 9px;">
					<button title="{{ trans('text.modify') }}" class="btn btn-xs bck-project modal_url" data-url="/resources/edit_project_resource/{!! $resource->id !!}" data-close-function="load_project_resources" ><i class="fas fa-pencil-alt" aria-hidden="true"></i></button>
					<button title="{{ trans('text.delete') }}" class="btn btn-xs btn-danger delete_resource" data-project-id="{!! $project->id !!}" data-id="{!! $resource->id !!}"><i class="fa fa-times" aria-hidden="true"></i></button>
				</div>
			@endif			
		</li>
		
	@endforeach


</ul>