@extends('layouts.resource')

@section('content')

<div class="container">

    <div class="row bck-project resource_header">
        <!-- left text -->
        <div class="col-xs-4 text-left resource_header_title">
            <table>
                <tr>
                    <td class="edit-mode-header-tr">
                        <i class="fas fa-pencil-alt" aria-hidden="true">
                    </td>
                    <td style="vertical-align: middle; padding-left: 10px;">
                        {{ strtoupper(trans('text.modify_mode')) }}
                    </td>
                </tr>
            </table>        
        </div>

        <!-- middle buttons -->
        <div class="col-xs-4">
            <table style="margin-left: auto; margin-right: auto; margin-top:5px;">
                <tr>
                    <td style="padding: 5px;">
                        <!-- Close button -->
                        <a href="/"><button class="btn btn-danger" type="button" title="{{ trans('text.close') }}"><i class="fas fa-arrow-left" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
            </table>          
        </div>

        <!-- right text -->
        <div class="col-xs-4 text-right resource_header_title">
            <table style="float: right;">
                <tr>
                    <td style="vertical-align: middle; padding-right: 10px;">
                        {{ strtoupper(trans('text.projects')) }}
                    </td>
                    <td class="edit-mode-header-tr">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    </td>
                </tr>
            </table>
        </div>          
    </div>


    <div class="row">
        <div class="col-xs-12">

            <ul class="nav nav-tabs" style="border-bottom: 1px solid #ddd;">
                <li id="themes_tab"><a data-toggle="tab" href="#themes">{{ trans('text.themes') }}</a></li>
                <li id="challenges_tab"><a data-toggle="tab" href="#challenges">{{ trans('text.challenges') }}</a></li>
                <li id="projects_tab" class="active"><a data-toggle="tab" href="#projects">{{ trans('text.projects') }}</a></li>
            </ul>

            <div class="tab-content">
                <div id="content" class="tab-pane fade in active">
                    <div style="width: 100%; padding-top: 30px; text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>    
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        // load the default tab
        load_projects();
    });


/******* THEMES **********/

    // DELETE A THEME
    $('body').on('click', '.delete_theme', function(){

        var token = $("meta[name='csrf-token']").attr("content");
        var theme_id = $(this).data('id');      
        var text = "{{ trans('challenges.question_delete_theme') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

                $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/projects/themes/delete/' + theme_id,
                    type: 'POST',
                    success: function(response) {
                        load_themes();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
                });    
            }
        });

    });

    var loader = '<div style="width: 100%; padding-top: 30px; text-align: center;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>';

    // LOAD THE THEMES TAB ON CLICK
    $('body').on('click', '#themes_tab', function() {
        load_themes();
    });

    // LOAD THEMES TAB
    function load_themes() {

        var token = $("meta[name='csrf-token']").attr("content");

        $('#content').html(loader);

        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/themes/edit_page',
            type: 'POST',
            success: function(response) {
                $('#content').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
        });
    }

    // RELOAD IF MODAL CLOSES
    $('body').on('hidden.bs.modal', '#main_modal', function() {
        
        if ($('.themes_datatable').length > 0) {
            load_themes();        
        }

    });    

    // CREATE A NEW THEME
    $('body').on('click', '#create_theme_btn', function() {
        
        var token = $("meta[name='csrf-token']").attr("content");

        var name = $("[name='name']").val();
        var description = $("[name='description']").val();

        // check
        if (name) {

            // create new group and close modal
            $.ajax({
                data: { 
                    '_token': token,
                    'name': name,
                    'description': description,
                },
                url: '/projects/themes/create',
                type: 'POST',
                success: function(response) {
                    console.log(response);
                    if (response.trim() == "OK") {
                        load_themes();
                        $('#create_theme_btn').closest('.modal').modal('hide');
                    } 
                },
                error: function(response){
                    bootbox.alert(response);
                }
            });    

        } else {

            bootbox.alert("{{ trans('challenges.enter_group_and_project_name') }}");
        }
    });

/****** GRPUPS / PROJECTS *******/

    // DELETE A GROUP
    $('body').on('click', '.delete_group', function(){

        var token = $("meta[name='csrf-token']").attr("content");
        var group_id = $(this).data('id');      
        var text = "{{ trans('challenges.delete_group_question') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

                $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/projects/challenges/groups/delete/' + group_id,
                    type: 'POST',
                    success: function(response) {
                        groups_assigned_to_challenge();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
                });    
            }
        });

    });

    // DETACH TEACHER FROM GROUP
    $('body').on('click', '.detach_teacher', function(){
        
        var token = $("meta[name='csrf-token']").attr("content");
        var teacher_id = $(this).data('teacher-id');
        var group_id = $(this).data('group-id');

        var text = "{{ trans('challenges.detach_teacher_from_group_question') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

                $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/projects/challenges/groups/detach_teacher/' + group_id + '/' + teacher_id,
                    type: 'POST',
                    success: function(response) {
                        if (response.trim() == "OK") {
                            load_group_teachers(group_id);
                        };

                        groups_assigned_to_challenge();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
                });    
            }
        });
    });


    // ATTACH TEACHER TO GROUP
    $('body').on('click', '#assign_teacher', function(){
        
        var token = $("meta[name='csrf-token']").attr("content");
        var teacher_id = $("#teacher_id").val();
        var group_id = $("#teacher_id").data('group-id');

        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/assign_teacher/' + group_id + '/' + teacher_id,
            type: 'POST',
            success: function(response) {
                if (response.trim() == "OK") {
                    load_group_teachers(group_id);
                } else {
                    bootbox.alert (response);
                }

                groups_assigned_to_challenge();
            },
            error: function(response){
                bootbox.alert (response);
            }
        });    
    });


    // LOAD GROUP ASSIGNED TEACHERS
    function load_group_teachers(challenge_group_id){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/load_group_teachers/' + challenge_group_id,
            type: 'POST',
            success: function(response) {

                $('#group_teachers_list').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });      
    }


    // ATTACH USER TO GROUP
    $('body').on('click', '#assign_user', function(){
        
        var token = $("meta[name='csrf-token']").attr("content");
        var user_id = $("#user_id").val();
        var group_id = $("#user_id").data('group-id');

        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/assign_user/' + group_id + '/' + user_id,
            type: 'POST',
            success: function(response) {
                if (response.trim() == "OK") {
                    load_group_users(group_id);
                } else {
                    bootbox.alert (response);
                }

                groups_assigned_to_challenge();
            },
            error: function(response){
                bootbox.alert (response);
            }
        });    
    });


    // LOAD GROUP USERS
    function load_group_users(challenge_group_id){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/load_group_users/' + challenge_group_id,
            type: 'POST',
            success: function(response) {

                $('#group_users_list').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });      
    }

   
    // DETACH TEACHER FROM GROUP
    $('body').on('click', '.detach_user', function(){
        
        var token = $("meta[name='csrf-token']").attr("content");
        var user_id = $(this).data('user-id');
        var group_id = $(this).data('group-id');

        var text = "{{ trans('challenges.detach_user_from_group_question') }}";
        
        bootbox.confirm(text, function(result){ 
            if (result) {

                $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/projects/challenges/groups/detach_user/' + group_id + '/' + user_id,
                    type: 'POST',
                    success: function(response) {
                        if (response.trim() == "OK") {
                            load_group_users(group_id);
                        };

                        groups_assigned_to_challenge();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
                });    
            }
        });
    });


    function load_assign_group_to_challenge(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/edit/load_assign_group_to_challenge',
            type: 'POST',
            success: function(response) {

                $('#assign_group_to_challenge').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    }

    // Create a new group button
    $('body').on('click', '#create_a_group', function(){
        
        var token = $("meta[name='csrf-token']").attr("content");
        var group_name = $("#create_group_name").val();
        var group_project_name = $("#create_group_project_name").val();
        var challenge_id = $("#group_challange_id").val();

        // check if group and project name entered
        if (group_name && group_project_name) {

            // create new group and close modal
            $.ajax({
                data: { 
                    '_token': token,
                    'name': $("#create_group_name").val(),
                    'project_name': $("#create_group_project_name").val(),
                    'challenge_id': challenge_id
                },
                url: '/projects/challenges/groups/create',
                type: 'POST',
                success: function(response) {
                  
                    if (response.trim() == "OK") {
                        load_projects();
                        $('#create_a_group').closest('.modal').modal('hide');
                    } 
                },
                error: function(response){
                    bootbox.alert(response);
                }
            });    

        } else {

            bootbox.alert("{{ trans('challenges.enter_group_and_project_name') }}");
        }
    });

    var create_var = 0;

    // LOAD PROJECTS TAB
    function load_projects() {

        var token = $("meta[name='csrf-token']").attr("content");

        $('#content').html(loader);

        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/groups/modify',
            type: 'POST',
            success: function(response) {
                $('#content').html(response);

                // if crate button is pressed, open the modal
                @if ($create)
                    if (!create_var){
                        $('#create_project_btn').click();
                        create_var = 1;
                    }
                @endif   

            },
            error: function(response){
                bootbox.alert (response);
            }
        });
    }

    /** DELETE PROJECT */
    $('body').on('click', '.delete_project', function() {

        var id = $(this).data('id');

        // confirm the delete
        bootbox.confirm("{{ trans('text.delete_project_question') }}", function(result){ 

            // delete group and reload 
            $.ajax({
                data: { 
                    '_token': token,
                },
                url: '/projects/challenges/groups/delete/' + id,
                type: 'POST',
                success: function(response) {
                    
                    if (response.trim() == "OK") {
                        load_projects();
                    } 
                },
                error: function(response){

                    bootbox.alert(response);
                }
            });    
            
        });
    });

    // LOAD THE PROJECTS TAB ON CLICK
    $('body').on('click', '#projects_tab', function() {
        load_projects();
    });



/****** CHALLENGES ******/
    
    // LOAD THE CHALLENGES TAB ON CLICK
    $('body').on('click', '#challenges_tab', function() {
        load_challanges();
    });


    // LOAD CHALLENGES TAB
    function load_challanges() {

        var token = $("meta[name='csrf-token']").attr("content");

        $('#content').html(loader);

        $.ajax({
            data: { 
                '_token': token,
            },
            url: '/projects/challenges/modify',
            type: 'POST',
            success: function(response) {
                $('#content').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
        });
    }


    // CREATE A NEW CHALLENGE
    $('body').on('click', '#create_challenge_btn', function() {
        
        var token = $("meta[name='csrf-token']").attr("content");

        var theme_id = $("[name='theme_id']").val();
        var name = $("[name='name']").val();
        var course = $("[name='course']").val();
        var year = $("[name='year']").val();
        var description = $("[name='description']").val();

        var comments_enabled = $("[name='comments_enabled']")[0].checked;
        if (comments_enabled) {
            comments_enabled = 1;
        } else {
            comments_enabled = 0;
        }

        // check
        if (name && course && year) {

            // create new group and close modal
            $.ajax({
                data: { 
                    '_token': token,
                    'theme_id': theme_id,
                    'name': name,
                    'course': course,
                    'year': year,
                    'description': description,
                    'comments_enabled': comments_enabled,
                },
                url: '/projects/challenges/create',
                type: 'POST',
                success: function(response) {
                  
                    if (response.trim() == "OK") {
                        load_challanges();
                        $('#create_challenge_btn').closest('.modal').modal('hide');
                    } 
                },
                error: function(response){
                    bootbox.alert(response);
                }
            });    

        } else {

            bootbox.alert("{{ trans('challenges.enter_group_and_project_name') }}");
        }
    });


  

    // SORT ATTACHMENTS
    function sort_files() {
        $( ".sortable_ul" ).sortable({
            //handle: ".handle", 
            stop: function( event, ui ) {
                // get the token
                var token = $("meta[name='csrf-token']").attr("content"); 

                // make resources id-s      
                var resources = [];

                $(this).children('li').each(function(i){
                    resources.push($(this).data('id'));
                });

                // send to server
                $.post({
                    url: '/resources/update_sort',
                    data: {
                        '_token': token,
                        'resources': resources,
                    },
                    success: function(data, status) {
                        //alert(data);
                    },
                    error: function(xhr, desc, err) {
                        console.log(xhr);
                        console.log("Details: " + desc + "\nError:" + err);
                    }
                });
            }
        });
    }

    // delete resource
    $('body').on('click', '.delete_resource', function(){   
        var resource_id = $(this).data('id');
        var project_id = $(this).data('project-id');
        var text = "{{ trans('text.question_delete_resource') }}";

        bootbox.confirm(text, function(result){ 

            $.ajax({
                url: '/resources/delete',
                type: 'GET',
                data: {
                    'id' : resource_id
                },
                success: function(data, status) {
                    load_project_resources(project_id);
                },
                error: function(xhr, desc, err) {
                    bootbox.alert (desc);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });

        });
    });
    

    // LOAD PROJECT COVER IMAGE
    function load_project_image(project_id) {

        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            data: { 
                '_token': token,
                'editable': 1
            },
            url: '/api/projects/challenges/load_project_image/' + project_id,
            type: 'POST',
            success: function(response) {
                $('#cover_image').html(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
        });
    }

</script>
@endsection
