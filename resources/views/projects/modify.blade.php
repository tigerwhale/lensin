<!-- EDIT PROJECTS BACKEND -->

<!-- CREATE NEW PROJECT -->
<div class="row">
	<div class="col-xs-12 text-center" style="padding-top: 20px;">
		<button type="button" class="btn bck-project modal_url" id="create_project_btn" data-url="/projects/challenges/edit/create_new_group_modal" title="{{ trans('challenges.create_new_project') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </button>
	</div>
</div>

<!-- LIST OF PROJECTS -->
<div class="row">
	<div class="col-xs-12 text-center">		
        <!-- DATATABLE SEARCH -->
	    <table class="display" id="datatable">
	        <thead>
	            <tr>
	                <th class="col-xs-1">{{ trans('text.id') }}</th>
                    <th class="col-xs-2">{{ trans('text.project') }}</th>
	                <th class="col-xs-2">{{ trans('challenges.challenge') }}</th>
                    <th class="col-xs-2">{{ trans('text.course') }}</th>
                    <th class="col-xs-2">{{ trans('text.description') }}</th>
                    <th class="col-xs-2">{{ trans('text.author') }}</th>
	                <th class="col-xs-1">{{ trans('text.edit') }}</th>
	            </tr>
	        </thead>
	    </table>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/projects/data_table_modify',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "challenge" },
            { "data": "course" },
            { "data": "description" },
            { "data": "author" },
            { "data": "published" }
        ], 

		"aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {

            	var modify = '<div  title="{{ trans('text.modify') }}" class="btn bck-lens modal_url" data-url="/projects/challenges/groups/edit/' + row['id'] + '"  title="{{ trans('challenges.modify_project') }}"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div> ';
                var publish = publishButton(row['published'], 'challengegroup', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
            	var delete_button = '<div class="btn btn-danger delete_project" title="{{ trans('text.delete') }}" data-id="' + row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + publish + delete_button;
            },
            "aTargets": [ 6 ]
        }]

    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });

</script>