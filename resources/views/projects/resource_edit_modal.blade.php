<?php
/**
* @var $resource
*/
?>
<!-- Modal header-->
<div class="modal-header">
    <h5 class="modal-title js-modal-title">
        {{ strtoupper(trans('text.add_resource_information')) }}
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<!-- Modal body-->
<div class="modal-body" id="resource_modal_div">

    <input type="hidden" id="resource_modal_id" value="{{ $resource->id }}">
    <!-- NAME -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.name') }}
        </div>
        <div class="col-8">
            <input id="resource_modal_name" type="text" name="name" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->name }}" class="form-control update_input" required>
        </div>
    </div>

    <!-- author -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.author') }}
        </div>
        <div class="col-8">
            <input id="resource_modal_author" type="text" name="author" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->author }}" class="form-control update_input" required>
        </div>
    </div>

    <!-- YEAR -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.year') }}
        </div>
        <div class="col-8">
            <input list="years" type="text" id="resource_modal_year" name="year" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->year }}" class="form-control update_input" required>
            <datalist id="years">
                @foreach (range(date('Y'), 1970) as $year)
                    <option value="{{ $year }}">
                @endforeach
            </datalist>
        </div>
    </div>

    <!-- LICENCES -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.licence') }}
        </div>
        <div class="col-8">
            @foreach (\App\Licence::all() as $licence)
                <?php
                    $checked = $resource->licences->find($licence->id) ? "checked" : "";
                ?>
                <input type="checkbox"
                       id="licence_{!! $licence->id !!}"
                       name="licence"
                       data-url='/resources/licence_update'
                       data-id="{!! $resource->id !!}"
                       data-id-belongs-to="{!! $licence->id !!}"
                       value="{!! $licence->id !!}"
                       class="update_checkbox radio_course"
                       style="display: none"
                        {!! $checked !!} >

                <label class="round_button" for="licence_{{ $licence->id }}">
                    <img id="licence_image_{{ $licence->id }}" src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
                </label>
            @endforeach
        </div>
    </div>

    <!-- FILE TYPE -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.file_type') }}
        </div>
        <div class="col-8">
            <?php
                if ($resource->resourcable_type == "App\ChallengeGroup") {
                    $resource_types = \App\ResourceType::orderBy('order')->where('name', 'LIKE', 'image')->get();
                } else {
                    $resource_types = \App\ResourceType::orderBy('order')->get();
                }
            ?>
            @foreach ($resource_types as $resource_type)
                <?php $checked = ($resource->resource_type_id == $resource_type->id) ? "checked" : "";?>
                <input {!! $checked !!} type="radio" value="{{ $resource_type->id }}" list="years" type="text" id="resource_type_{{ $resource_type->id }}" name="resource_type_id" data-model='resources' data-id={{ $resource->id }} class="radio_course update_input resource_type" style="display: none;">
                <label class="round_button icon" for="resource_type_{{ $resource_type->id }}">{!! $resource_type->icon !!}</label>
            @endforeach
        </div>
    </div>

    <!-- LENGTH -->
    <div class="row">
        <div class="col-4">
            {{ trans('text.length') }}
        </div>
        <div class="col-8">
            <input type="text" name="length" data-model='resources' data-id={!! $resource->id !!} value="{{ $resource->length }}" class="form-control update_input" required>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-4"></div>
        <div class="col-8" id="resource_file_and_preview">
            @include('bootstrap4.resources.course.modals.resource_edit_modal_file_list', ['resource' => $resource])
        </div>
    </div>

    <script type="text/javascript">
        var token = $("meta[name='csrf-token']").attr("content");
        var progressBar_resource_{{ $resource->id }} = document.getElementById('progressBar_resource_{{ $resource->id }}');
        var progressOuter_resource_{{ $resource->id }} = document.getElementById('progressOuter_resource_{{ $resource->id }}');
    </script>

</div>

