@if ($resource->comments)

	<h4>{{ strtoupper(trans('text.comments') )}}</h4>
	<!-- ADD A COMMENT -->
	@if ($editable)
		<table style="width:100%;">
			<tr>
				<td style="width: 100%;">
					<textarea class="form-control" id="resource_comment_text" placeholder="{{ trans('resources.write_a_comment') }}"></textarea>	
				</td>
				<td style="padding:5px;">
					<button type="button" class="btn btn-success" data-id="{!! $resource->id !!}" id="add_resource_comment">
						<i class="fa fa-plus" aria-hidden="true"></i> 
					</button>
				</td>
			</tr>
		</table>
		<!-- 
		<div class="dropzeon dropzone_div" style="padding-top: 5px; padding-bottom: 5px;" id="comment_files" >{{ trans('text.click_to_select_file') }}</div>
		-->
	@endif 


	@foreach($resource->comments as $comment)
		<table style="margin-top: 20px;">
			<tr class="hover">
				<td style="vertical-align: top;">
					{!! userAvatar($comment->creator->id) !!}
				</td>
				<td style="width: 100%;">
					 <span class="small"  style="color: #CCC;">
					 	{{ $comment->creator->fullName() }} - {{ $comment->created_at->format('d.m.Y. h:i') }} 
					 </span>
					 <br>
					 {{ $comment->text }}
				</td>
				@if ($editable == $comment->creator->id || $teacher)
					<td style="padding:5px;">
						<button class="btn btn-xs btn-danger delete_comment" data-comment-id="{!! $comment->id !!}" data-resource-id={!! $resource->id !!}>
							<i class="fa fa-times" aria-hidden="true"></i>
						</button>
					</td>
				@endif
			</tr>
		</table>
	@endforeach

@endif