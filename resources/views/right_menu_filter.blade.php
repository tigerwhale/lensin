<?php 
	$languages = \App\Language::where('published', 1)->get();
	$thisServerId = server_property('server_id');
?>

	<div class="left_filter_menu">
		
		<!-- LANGUAGES -->
		<div class="dropdown text-center text-lens" style="cursor: pointer;">
			<div class="dropdown-toggle dropdown_selector" id="dropdownMenuLanguageLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				{{ trans('text.languages') }}
				<span class="caret"></span>
			</div>
			
			<div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuLanguageLeft">
				<div class="dropdown text-left text-lens" style="cursor: pointer; padding-left: 15px; padding-top: 7px;">
					
					<!-- Resource language-->
					<span class="text-gray">{{ trans('text.resources_languages') }}</span>
					<div id="resourcesLanguageSelect" style="margin-left: 20px;">
						<i class="fa fa-spinner fa-pulse fa-fw"></i>
					</div>

					<!-- Interface language -->
					<span class="text-gray">{{ trans('text.interface_language') }}</span>
					<div class="dropdown-submenu dropdown_selector" id="dropdownInterfaceLanguageLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						{!! cur_lan_name() !!}
						<span class="caret"></span>
					</div>
					<ul class="dropdown-menu pull-right" aria-labelledby="dropdownInterfaceLanguageLeft">
						@foreach ($languages as $language)
							<li><a href="/locale/{!! $language->locale !!}">{!! $language->name !!}</a></li>
						@endforeach
					</ul>
				</div>	
			</div>
		</div>


		<!-- PLATFORMS -->
		<h4 class="resources_title" style="margin-top: 32px;">{{ strtoupper(trans('text.right_menu_title_platforms')) }}</h4>

		<div id="right_switches">

			<!-- ON/OFF SWITCH -->
			<label class="switch_platform">
				<input type="checkbox" class="platforms" id="all_platforms_switch" data-server-id="100000">
			    <div class="slider_platform round all_platforms_switch"></div>
			</label>
			<div class="platform_name">All On/Off</div>

			<i id="platform_switches_spinner" class="fa fa-spinner fa-pulse fa-2x fa-fw" style="margin-top: 10px;"></i>

		</div>
	</div>

	@include('right_menu_filter_js')