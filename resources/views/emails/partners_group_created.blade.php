<!DOCTYPE html>
<html>
<head>
	<title>Partners group created</title>
</head>
<body>
	<h1>Partners group created</h1>
	A user has created a partner group on {!! server_property('platform_title') !!}. You can see and manage partners <a href="{{ url('backend/partners') }}">here.</a>
</body>
</html>