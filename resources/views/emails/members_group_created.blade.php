<!DOCTYPE html>
<html>
<head>
	<title>Member group created</title>
</head>
<body>
	<h1>Member group created</h1>
	A user has created a member group on {!! server_property('platform_title') !!}. You can see and manage members <a href="{{ url('backend/members') }}">here.</a>
</body>
</html>