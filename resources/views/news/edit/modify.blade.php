@extends('layouts.resource')

@section('content')

    <div class="container">

        {!! backendHeader('news', 'news', strtoupper(trans('news.news')), 'news', true)  !!}            

        <div class="row">
            <div class="col-xs-12 text-center">     
                <!-- DATATABLE SEARCH -->
                <table class="display" id="datatable">
                    <thead>
                        <tr>
                            <th>{{ trans('text.title') }}</th>
                            <th>{{ trans('text.author') }}</th>
                            <th>{{ trans('text.date') }}</th>
                            <th style="min-width: 120px;"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('news.edit.modify_js')

@endsection