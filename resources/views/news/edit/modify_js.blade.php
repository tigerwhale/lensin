<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/news/datatable_list',
            rowId: 'id',
        },
        
        "columns": [
            { "data": "title" },
            { "data": "author" },
            { "data": "date" },
            { "data": "id" },
        ], 
        "aoColumnDefs": [
        {

            "mRender": function ( data, type, row ) {
                
                var modify = '<a title="{{ trans('text.modify') }}" href="/news/edit/' + row['id'] + '"><div class="btn bck-lens"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                var publish = publishButton(row['published'], 'news', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                var delete_button = '<div class="btn btn-danger delete_news" title="{{ trans('text.delete') }}" data-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                return modify + publish + delete_button;
            },
            "aTargets": [ -1 ]
        }]
    });


    // ROW HOVER
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    // DELETE COURSE BUTTON
    $('body').on('click', '.delete_news', function() {
        var id = $(this).data('id');
        // confirm the delete
        bootbox.confirm("{{ trans('news.question_delete_news') }}", function(result){ 
            // if OK, delete the course
            if (result) {

                 $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/news/delete/' + id,
                    type: 'GET',
                    success: function(response) {
                        table.ajax.reload();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
               });
            }
        });
    });
</script>