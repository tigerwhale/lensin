@extends('layouts.resource')

@section('content')

<div class="container">

    {!! backendHeader('news', 'news', strtoupper(trans('news.news')), 'news')  !!}

	<div class="row">
		<div class="col-xs-3 text-center">
			<!-- Cover image -->
			<div class="row padding-small">
                {{ strtoupper(trans('text.cover_image')) }}
				<div class="col-xs-12" id="cover_image_container_{!! $news->id !!}">
					{!! coverImageEdit($news) !!}
				</div>
			</div>		

			<!-- Title  -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-12">
					<span style="line-height: 30px;">{{ strtoupper(trans('text.title')) }}</span>
					<input type="text" name="title" data-model='news' data-id={{ $news->id }} value="{{ $news->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
				</div>
			</div>		

			<!-- Language -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-12">
					<span style="line-height: 30px;">{{ strtoupper(trans('text.language')) }}</span>
					{!! Form::select('language_id', $languages, $news->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'tools', 'data-id'=> "$news->id", 'placeholder' => trans('text.select_language') ]) !!}
				</div>
			</div>		
		</div>


		<div class="col-xs-9">
			<!-- Text  -->
			<div class="row">
				<div class="col-xs-12">
					<span style="line-height: 30px;">{{ strtoupper(trans('text.text')) }}</span><br>
					<textarea style="height: 400px;" type="text" name="text" data-model='news' data-id={{ $news->id }} placeholder="{{ strtoupper(trans('text.text')) }}" class="form-control update_input">{!! $news->text !!}</textarea>
				</div>
			</div>
		</div>
	</div>
	

</div>


@endsection