@extends('layouts.app')

@section('content')

	<div class="container" style="margin-top: 20px;">

		<!-- MENU LEFT -->
		<div class="col-xs-2">
			@include('left_menu_filter')
		</div>

		<div class="col-xs-8" style="margin-top:20px;">

			<!-- content header -->
	        <div class="row content-header">
	            
	            <!-- left button -->
	            <div class="col-xs-2">
	            	{!! left_back_button() !!}
	            </div>
	            
	            <!-- middle -->
	            <div class="col-xs-8 text-center">
	            	<h2 style="margin-bottom: 0px;">{{ trans('text.news') }}</h2>
				</div>
	            
	            <!-- right -->
	            <div class="col-xs-2">
	                
	            </div>

	        </div>

			<!-- title -->
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-xs-12">
					<span class="btn-news-small pull-left" style="margin-right: 10px;"></span> <span class="text-news" style="line-height: 30px;">{{ $news->title }}</span>
				</div>
			</div>

			<div class="row tab-content">
	
				<!-- STUDY CASE INFO -->
				<div role="tabpanel" class="tab-pane active">

					<!-- CONTENT --> 
					<div class="row">

						<!-- COVER IMAGE --> 
						<div class="col-xs-12">
							@if (isset($news->image))
								<img src="{{ $news->image }}" class="image" style="margin-top: 10px; margin-bottom: 10px;">
							@endif
						</div>
						<div class="col-xs-12">
							{{ $news->text }}
						</div>
					</div>

				</div>		
			</div>
		</div>
		<!-- MENU RIGHT -->
	    <div class="col-xs-2">
	    	@include('right_menu_filter', ['home_page' => 1])
	    </div>
	</div>

@endsection
