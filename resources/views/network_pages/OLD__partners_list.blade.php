@foreach($partners as $partner)
	<div class="row">
		@if($partner->image)
			<div class="col-xs-2">
				<img src="{{ url('/') . $partner->image }}" class="image">
			</div>
		@endif
		<div class="col-xs-10">
			<!-- Title -->
			<h4>{{ $partner->title }}</h4>
			<!-- Address -->
			{{ $partner->country->name }}, {{ $partner->address }}
			<br>
			<!-- Description -->
			@if($partner->description)
				{{ $partner->description }}
				<br>
			@endif
			<!-- Website -->
			@if($partner->website)
				<a href="{{ $partner->website }}">{{ $partner->website }}</a><br>
			@endif
			<!-- Email -->
			@if($partner->email)
				<a href="{{ $partner->email }}">{{ $partner->email }}</a><br>
			@endif
			@if($partner->reference_person)
				{{ trans('text.reference_person') }}: {{ $partner->reference_person }}, <a href="{{ $partner->reference_person_email }}">{{ $partner->reference_person_email }}</a>
			@endif

			@if ($partner->members)
				<h5>{{ trans('text.' . $request->page) }}</h5>
				@foreach($partner->members as $member)
					{{ $member->fullName() }} {{ $member->email }}
					<br>
				@endforeach
			@endif
		</div>
	</div>
@endforeach 