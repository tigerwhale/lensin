<br>
@foreach($platforms as $platform)
	<div class="panel-group">
		<div class="panel panel-default">
			<a data-toggle="collapse" href="#platform_{!! $request->page !!}_tab_{!! $platform->id !!}" class="" aria-expanded="true">
				<div class="panel-heading"> 
					<i class="fa pull-right fa-lg fa-caret-down" aria-hidden="true" style="margin-top: 15px;"></i>
					<div class="panel-title">
						<h4><span class="platform_icon btn-smallest pull-left" style="background-image: url({{ $platform->address }}/images/server/logo.png); margin-right: 10px;"></span> {{ $platform->name }}</h4>
					</div>
				</div>
			</a>
			<div id="platform_{!! $request->page !!}_tab_{!! $platform->id !!}" class="panel-collapse collapse">
				<div class="panel-body">
					<h4>{{ trans('text.' . $request->page) }}</h4>

					<div id="partners_{!! $request->page !!}_{{ $platform->id }}">
						<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
					</div>

					<script type="text/javascript">
						var token = $("meta[name='csrf-token']").attr("content"); 	
		                $.get({
		                    url: '{{ $platform->address }}/api/{!! $request->page !!}_list',
		                    data: {
		                        '_token': token,
		                    },
		                    success: function(data, status) {
		                        $('#partners_{!! $request->page !!}_{{ $platform->id }}').html(data);
		                    },
		                    error: function(xhr, desc, err) {
		                    	$('#partners_{!! $request->page !!}_{{ $platform->id }}').html("{{ trans('text.error_getting_data') }}");
		                        console.log(xhr);
		                        console.log("Details: " + desc + "\nError:" + err);
		                    }
		                })
					</script>
				</div>
			</div>
		</div>
	</div>	
@endforeach

