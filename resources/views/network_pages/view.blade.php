@extends('layouts.app')

@section('content')

	<div class="container" style="margin-top: 20px;">
		<div class="row">

            <!-- CONTENT -->
            <div class="col-xs-2" style="padding-top: 20px;"></div>
            <div class="col-xs-8" id="tool_content" style="padding-top: 20px;">
                <h2 style="text-align: center;">Network</h2>

                <ul class="nav nav-tabs" style="border-bottom: 1px solid #ddd;">
                    <li id="users" ><a data-toggle="tab" href="#users_tab">{{ trans('text.participants') }}</a></li>
                    <li id="members"><a data-toggle="tab" href="#members_tab">{{ trans('text.members') }}</a></li>
                    <li id="platforms" class="active"><a data-toggle="tab" href="#platforms_tab">{{ trans('text.partners') }}</a></li>
                </ul>

                <div class="tab-content">
                    <!-- USERS -->
                    <div id="users_tab" class="tab-pane fade in ">
                        <br>
                        <table class="display row-links" id="datatable_participants"  style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{ trans('text.platform') }}</th>
                                    <th>{{ trans('text.name') }}</th>
                                    <th>{{ trans('text.last_name') }}</th>
                                    <th>{{ trans('text.type') }}</th>
                                    <th>{{ trans('text.country') }}</th>
                                    <th>{{ trans('text.institution') }}</th>
                                </tr>
                            </thead>
                        </table>
                        
                    </div>

                    <!-- MEMBERS -->
                    <div id="members_tab" class="tab-pane fade in">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                    </div>

                    <!-- PLATFORMS -->
                    <div id="platforms_tab" class="tab-pane fade in active">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                    </div>
                    
    			</div>  
    		</div>
    	</div>
    </div>

	<script type="text/javascript">
    
        var datatable_participants = $('#datatable_participants').DataTable({ 
            "processing": true, 
            "stateSave": true,  
            "dom": '<"top"lf>rt<"bottom"ip><"clear">', 
            "ajax": { 
                "cache":true, 
                "url" : '{!! $central_server_address !!}/api/data_all_servers', 
                "rowId": 'id', 
                "data": { 
                    "url":"/api/all_users" 
                }, 
            }, 
             
            "columns": [ 
                { "data": "server_name" }, 
                { "data": "name" }, 
                { "data": "last_name" }, 
                { "data": "type" }, 
                { "data": "country" }, 
                { "data": "school" }, 
            ], 
        }); 
 
        var datatable_members = $('#datatable_members').DataTable({ 
            "processing": true, 
            "stateSave": true,  
            "dom": '<"top"lf>rt<"bottom"ip><"clear">', 
            "ajax": { 
                "cache":true, 
                "url" : '{!! $central_server_address !!}/api/data_all_servers', 
                "rowId": 'id', 
                "data": { 
                    "url":"/api/all_members" 
                },   
            }, 
                       
            "columns": [ 
                { "data": "server_name" }, 
                { "data": "name" }, 
                { "data": "type" }, 
                { "data": "country" }, 
                { "data": "reference_person" }, 
                { "data": "website" }, 
            ], 
        }); 
 


        $.get({
            url: "{!! $central_server_address !!}/api/all_platforms_view",
            success: function(data, status) {
                $("#platforms_tab").html(data);
            },
            data: {
                page: 'partners'
            },
            error: function(xhr, desc, err) {
                $("#platforms_tab").html("{{ trans('users.no_partners') }}");
                
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });

        $.get({
            url: "{!! $central_server_address !!}/api/all_platforms_view",
            success: function(data, status) {
                $("#members_tab").html(data);
            },
            data: {
                page: 'members'
            },
            error: function(xhr, desc, err) {
                $("#partners_tab").html("{{ trans('users.no_members') }}");
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });

	</script>
@endsection
