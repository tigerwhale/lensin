<!-- RIGHT MENU JS -->

<script type="text/javascript">


	/**
	 * Load Platform Switches From Central Server
	 * then create switches, and filter options from URL
	 */
	function loadPlatformSwitchesFromCentralServer() 
	{
		$.post({
            url: '{!! server_property('central_server_address') !!}/api/server_list',
            data: {
                'requestServerId': {!! server_property('server_id') ? server_property('server_id') : 0 !!},
            },
            success: function(data, status) {
                createPlatformSwitches(eval(data));
                areAllPlatformSwitchesChecked();
                filterOptionsSelectedForGetRequest();
                $('#platform_switches_spinner').remove();
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })
	}
	
	/**
	 * Create Platform Switches in .right_switches 
	 * @param  {array} server_list 
	 */
	function createPlatformSwitches(server_list) {
		
		var platformSwitches = "";
		if (central_server) { 
			var checked = "checked"; 
		} else {
			var checked = ""; 
			server_list.unshift({
				address:"{{ Request::root() }}",
				id:"{{ server_property('server_id') }}",
				name:"{{ server_property('platform_title') }}", 
				server_description:"{{ server_property('slogan') }}"
			});			
		}
		
		$.each(server_list, function (i, platform) 
		{
			// Title for hover
			if (platform.server_description == 'null') 
			{
				var server_description = platform.server_description;
			} else {
				var server_description = platform.name;
			}
			
			// checked if in URL or first switch on local server
			checked = isPlatformSwitchOn("platform_" + platform.id);
			if (!central_server && i == 0) 
			{
				checked = "checked";
			}

			// HTML
			platformSwitches += '<label class="switch_platform">';
				platformSwitches += '<input type="checkbox" class="platforms server_id" id="platform_' + platform.id + '_switch" value="platform_' + platform.id + '" data-url="' + platform.address + '" data-server-id="' + platform.id + '" ' + checked + '>';
				platformSwitches += '<div class="slider_platform round platform_' + platform.id + '_slider" title="' + server_description + '"></div>';
			platformSwitches += '</label>';
			platformSwitches +=  '<div class="platform_name" id="' + platform.id + '_platform">' + platform.name + '</div>';
			// CSS
			document.styleSheets[0].insertRule('input:checked + .slider_platform.round.platform_' + platform.id + '_slider:before { background-image: url("' + platform.address + '/images/server/logo.png"); }');
			document.styleSheets[0].insertRule('.slider_platform.round.platform_' + platform.id + '_slider:before { background-image: url("' + platform.address + '/images/server/logo_transparent.png"); }');

		});

		$("#right_switches").append(platformSwitches);
	}


    function isPlatformSwitchOn(platform_id) {
    	var checkedReturnValue = "";
    	if (central_server) 
    	{
	    	var getParameters = getParams();
			if (getParameters['switch_platform']) 
			{
                $(getParameters['switch_platform']).each(function(key, value) 
                {
                	if (value == platform_id) 
                	{
                		checkedReturnValue = "checked";
                	}
                });
            } else {
            	checkedReturnValue = "checked";
            }
    	}
        
        return checkedReturnValue;
    }

	function checkSinglePlatformSelected()
	{
		if ( $('.platforms:checkbox:checked').length == 1 && $('.platforms:checkbox:checked').data('server-id') != server_id) 
		{
			var filterOptionsSelected = filterOptionsSelectedForGetRequest();
			window.location.href = $('.platforms:checkbox:checked').data('url') + "?locale=" + locale + login_send + filterOptionsSelected;

		} else if (typeof home_page == 'undefined') {
			var filterOptionsSelected = filterOptionsSelectedForGetRequest();
			window.location.href = "/?locale=" + locale + login_send + filterOptionsSelected;
		} 
	}

	/**
	 * Check if all right menu filter buttons are checked
	 * and check the all_platforms_switch accordingly
	 */
	function areAllPlatformSwitchesChecked() 
	{
		var allPlatformSwitchesLength = $(".platforms").not("#all_platforms_switch").length;
		var checkedPlatformSwitchesLength = $(".platforms:checked").not("#all_platforms_switch").length;

		if (allPlatformSwitchesLength == checkedPlatformSwitchesLength) 
		{
			$("#all_platforms_switch").prop('checked', true);
		} else 	{
			$("#all_platforms_switch").prop('checked', false);
		} 
	}


	$(function() {
		
		loadPlatformSwitchesFromCentralServer();

		$('.dropdown-submenu').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
		});

		// PLATFORMS SWITCH ON/OFF
		$('#all_platforms_switch').click(function() 
		{
			if ($(this).is(':checked')) {
				$('.platforms').prop('checked', true);
				$('.platforms_mobile').prop('checked', true);
			} else {
				$('.platforms').prop('checked', false);
				$('.platforms_mobile').prop('checked', false);
			}	
		});


		// FILTER PLATFORMS
		$(".left_filter_menu").on("click", ".platforms", function()
		{
			$('#loaderContainer').show();

			setTimeout(function() {
				areAllPlatformSwitchesChecked();
				
				@if (server_property('central_server') == 1)
					checkSinglePlatformSelected();
					checkDatatableFilters();
				@else
					var filterOptionsSelected = filterOptionsSelectedForGetRequest();
					if ($('.platforms:checkbox:checked').length > 1) 
					{
						location.href = "{!! server_property('central_server_address') !!}?" + "&locale=" + locale + login_send + filterOptionsSelected;
					} else {
						checkSinglePlatformSelected();
					}
				@endif

				if (typeof loadGrid == 'function') { 
				  loadGrid(); 
				}
				
				loadCriteriaTab();
				$('#loaderContainer').hide();	

			}, 1);
		});


	    $( "#slider" ).slider({
			value:0,
			min: 0,
			max: 1,
			step: 1,
			animate:true,
			slide: function( event, ui ) {
			$( "#amount" ).val( "$" + ui.value );
			}
	    });

	    $( "#amount" ).val( "$" + $( "#slider" ).slider( "value" ) );

	});


	

	/*
	function init_datatable_parameters(table) {

		var resources = "";

		$('.resources:checkbox:checked').each(function() {
			resources += $(this).val() + "|";
		});

		// remove the last "|"
		resources = resources.substring(0, resources.length - 1);

		// search the data and redraw table
		table.column( 6 ).search(resources, true).draw();

	}  
	*/

	/*
		// FILTER DATATABLE PLATFORMS
		$('.platforms').click(function() {

			// Local platforms
			@if (server_property('central_server') != 1)
				var platforms = "";

				$('.platforms:checkbox:checked').each(function() {
					platforms += '&platforms[]=' + $(this).data('server-id');
				});
				var filterOptionsSelected = filterOptionsSelectedForGetRequest();
				location.href = "{!! server_property('central_server_address') !!}?" + platforms + "&locale=" + locale + login_send + filterOptionsSelected;

			@else 
				// Central server
				var platforms = "";
				setTimeout(function() { 
					console.log('aaa');
					if ($('.platforms:checkbox:checked').length == 1) {
						var filterOptionsSelected = filterOptionsSelectedForGetRequest();
						window.location.href = $('.platforms:checkbox:checked').data('url') + "?locale=" + locale + login_send + filterOptionsSelected;
					} else if( typeof home_page == 'undefined' ) {
						var filterOptionsSelected = filterOptionsSelectedForGetRequest();
						window.location.href = "/?locale=" + locale + filterOptionsSelected;
					}
				}, 200);

				checkDatatableFilters()
				
			@endif

			if (typeof loadGrid == 'function') { 
			  loadGrid(); 
			}
			
			loadCriteriaTab();
		});	
	*/


</script>