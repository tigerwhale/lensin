<?php 
    $memberGroups = \App\MembersGroup::where('published', 1)->where('partner', '=', 1)->get()->pluck('title', 'id');
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
?>
<!-- PROFILE GROUP LIST-->
    @if (count($user->partners_groups))
        <div id="accordion_partner_groups" class="mt-3 accordion">
            <div class="card">
                <div class="card-header collapsed"
                     id="headingThree"
                     data-toggle="collapse"
                     data-target="#partner_group_member"
                     aria-expanded="false"
                     aria-controls="partner_group_member">
                    <span class="btn-link collapsed" style="width: 100%;">
                        <h5 class="mb-0 text-lens text-left pull-left">{{ trans('users.current_partners') }}</h5>
                    </span>
                </div>

                <div id="partner_group_member" class="collapse" aria-labelledby="headingThree" data-parent="#accordion_partner_groups">
                    <div class="card-body pt-3">
                        {{ trans('users.you_are_a_part_of') }}
                        @foreach ($user->partners_groups as $member_group)
                            <div class="row hover padding-small" id="member_row_{!! $member_group->id !!}">
                                <div class="col-10" style="padding-top: 7px;">
                                    {!! $member_group->title !!}
                                    @if(!$member_group->published)
                                        <span class="text-danger small"> - {{ trans('users.pending_administrator_approval') }}</span><br>
                                    @endif
                                </div>
                                <div class="col-2 text-right">
                                    <div class="btn btn-danger leave_member" title="{{ trans('users.leave_member') }}" data-member-id="{!! $member_group->id !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

<!-- JOIN MEMEBER GROUP -->
<div id="accordion_partner" class="mt-3 accordion">
    <div class="card">
        <div class="card-header collapsed"
             id="headingOne"
             data-toggle="collapse"
             data-target="#partner_group"
             aria-expanded="false"
             aria-controls="partner_group">
            <span class="btn-link collapsed" style="width: 100%;">
                <h5 class="mb-0 text-lens text-left pull-left">{{ trans('users.join_existing_partner_group') }}</h5>
            </span>
        </div>

        <div id="partner_group" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_partner">
            <div class="card-body pt-3">
                {{ trans('users.join_partners_text') }}
                <br>
                <br>
                @if (count($memberGroups) > 0)

                    <form method="POST" action="/member_group/join/{!! Auth::user()->id !!}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-10">
                                {{ Form::select('member_group_id', $memberGroups , null, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-2 text-right">
                                <button class="btn btn-success" type="submit">{{ trans('text.join') }}</button>
                            </div>
                        </div>
                    </form>
                @else
                    {{ trans('users.no_partners_groups') }}
                @endif
            </div>
        </div>
    </div>
</div>



<!-- CREATE MEMEBER GROUP -->
<div id="accordion_create_partner" class="mt-3 accordion">
    <div class="card">
        <div class="card-header collapsed"
             id="headingTwo"
             data-toggle="collapse"
             data-target="#partner_create_group"
             aria-expanded="false"
             aria-controls="partner_create_group">
            <span class="btn-link collapsed" style="width: 100%;">
                <h5 class="mb-0 text-lens text-left pull-left">{{ trans('users.create_a_new_partner') }}</h5>
            </span>
        </div>

        <div id="partner_create_group" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion_create_partner">
            <div class="card-body pt-3">

                {!! trans('users.register_member_text') !!}
                <br>
                <br>
                <form method="POST" action="/member_group/create" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <input type="hidden" name="partner" value="1">
                            <!-- platform -->
                            <label>{{ trans('text.platform') }}</label>
                            <select name="platform_select" class="form-control platform_select">
                                <option value="{{ url('/') }}">{{ server_property('platform_title') }}</option>
                            </select>

                            <!-- Type -->
                            {!! updateInput('text.type', Form::select('type', $memberGroupTypes , null, ['class' => 'form-control', 'id'=> 'member_type']) ) !!}
                            <!-- Name -->
                            {!! updateInput('text.name', Form::text('title', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Reference Person -->
                            {!! updateInput('text.reference_person', Form::text('reference_person', Auth::user()->fullName(), ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Reference Person Email -->
                            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', Auth::user()->email, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Country -->
                            {!! updateInput('text.country', Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'id'=> 'country_id', 'placeholder' => trans('text.select_country')]) ) !!}
                            <!-- Address -->
                            {!! updateInput('text.address', Form::text('address', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Website -->
                            {!! updateInput('text.web_site', Form::text('web_site', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                            <!-- Email -->
                            {!! updateInput('text.email', Form::email('email', null, ['class' => 'form-control', 'required'=>""]) ) !!}
                        </div>

                        <div class="col-12 col-md-6">
                            <label>
                                {{ trans('users.partners_logo') }}<br>
                                <small>{{ trans('text.logo_jpg_disclaimer') }}</small>
                            </label>
                            <label for="partner_cover_image" class="dropzone_div" id="partner_cover_image_label">{{ trans('text.click_to_select_image') }}</label>
                            <input type="file" name="image" id="partner_cover_image" style="display: none">
                            <img class="js-partner-image image">

                            <!-- Description -->
                            {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'required' => "", 'style' => 'height: 300px;']) ) !!}

                        </div>
                        <div class="col-12 mt-4">
                            <!-- Submit button -->
                            <button class="btn btn-success" id="create_member_group">{{ trans('text.register') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#partner_cover_image_label').on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
        })
            .on('drop', function(e) {
                droppedFiles = e.originalEvent.dataTransfer.files;
                $('#partner_cover_image_label').html( e.originalEvent.dataTransfer.files[0].name);
                $("#partner_cover_image").prop("files", e.originalEvent.dataTransfer.files);
            });

        $('#partner_cover_image').change(function(e) {
            $('#partner_cover_image_label').html( e.target.files[0].name );
            if (e.target.files && e.target.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.js-partner-image').attr('src', e.target.result);
                }

                reader.readAsDataURL(e.target.files[0]);
            }
        });

    });
</script>