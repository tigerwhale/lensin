@extends('bootstrap4.layouts.frontend')

@section('head')
    <script src="/js/dropzone.js"></script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 text-left mt-3">
                <!-- TAB MENU -->
                <ul class="nav nav-tabs nav-fill lens-nav">
                    <li class="nav-item" id="profile_tab">
                        <a data-toggle="tab" class="nav-link active" href="#profile">
                            {{ trans('register_page.join_as_participant') }}
                        </a>
                    </li>
                    <li class="nav-item" id="members_tab">
                        <a data-toggle="tab" href="#members" class="nav-link">
                            {{ trans('register_page.join_as_memeber') }}
                        </a>
                    </li>
                    <li class="nav-item" id="partners_tab" >
                        <a data-toggle="tab" href="#partners" class="nav-link">
                            {{ trans('register_page.join_as_partner') }}
                        </a>
                    </li>
                    <li class="nav-item" id="new_network_tab" >
                        <a data-toggle="tab" href="#new_network" class="nav-link">
                            {{ trans('register_page.register_a_new_network') }}
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="profile" class="tab-pane fade show active">
                        @include('auth.profile_tabs.profile', ['user' => $user, 'editableProfile' => $editableProfile])
                    </div>

                    <div id="members" class="tab-pane fade">
                        @include('auth.profile_tabs.tab_profile_members', ['user' => $user])
                    </div>

                    <div id="partners" class="tab-pane fade">
                        @include('auth.profile_tabs.tab_profile_partners', ['user' => $user])
                    </div>

                   <div id="new_network" class="tab-pane fade">
                        @include('auth.profile_tabs.tab_new_network', ['user' => $user])
                    </div>
                </div>

            </div>
        </div>
    </div>
@include('users.profile_js')

@endsection
