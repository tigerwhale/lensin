<?php
/** @var $platform_id
 ** @var $server_list
 */

$platform_id = isset($request->platform_id) ? $request->platform_id : "";
?>

@foreach ($server_list as $server)
	@if ($server->id == $request->platform_id)
		<option value="{!! $server->address !!}" selected>{{ $server->name }}</option>
	@endif
@endforeach

@foreach ($server_list as $server)
	@if ($server->id != $request->platform_id)
    	<option value="{!! $server->address !!}">{{ $server->name }}</option>
    @endif
@endforeach
