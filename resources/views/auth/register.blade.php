@extends('bootstrap4.layouts.frontend')

@section('content')

<?php
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university' => trans('users.university_type'),
        'school' => trans('users.school_type'),
        'company' => trans('users.company_type'),
        'ngo' => trans('users.ngo_type'),
        'goverment_institution' => trans('users.goverment_institution_type'),
        'others' => trans('users.others_type')
    ];
?>

<div class="container">
    <div class="row">
        <div class="col-12 text-center mt-3">
            <!-- TAB MENU -->
            <ul class="nav nav-tabs nav-fill lens-nav">
                <li class="nav-item" id="profile_tab">
                    <a data-toggle="tab" class="nav-link active" href="#profile">
                        {{ trans('register_page.join_as_participant') }}
                    </a>
                </li>
                <li class="nav-item" id="members_tab">
                    <a data-toggle="tab" href="#members" class="nav-link">
                        {{ trans('register_page.join_as_memeber') }}
                    </a>
                </li>
                <li class="nav-item" id="partners_tab" >
                    <a data-toggle="tab" href="#partners" class="nav-link">
                        {{ trans('register_page.join_as_partner') }}
                    </a>
                </li>
                <li class="nav-item" id="new_network_tab" >
                    <a data-toggle="tab" href="#new_network" class="nav-link">
                        {{ trans('register_page.register_a_new_network') }}
                    </a>
                </li>
            </ul>

            <div class="tab-content text-left">
                <!-- REGISTER AS PARTICIPANT -->
                <div id="profile" class="tab-pane fade show active">
                    <br>
                    {!! trans('users.register_participant_text') !!}
                    <br><br>

                    <form class="form-horizontal" role="form" method="POST" id="register_form" action="{{ url('register') }}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8">
                                {{ csrf_field() }}
                                <!-- platform -->
                                <label>{{ trans('text.platform') }}</label>
                                    <i class="fa fa-spinner fa-pulse fa-lg fa-fw platform_spinner"></i>
                                <select name="platform_select" class="form-control platform_select">
                                    <option value="{{ url('/') }}">{{ server_property('platform_title') }}</option>
                                </select>

                                <!-- name -->
                                {!! updateInput('text.name', Form::text('name', old('name'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('name') !!}
                                <!-- last_name -->
                                {!! updateInput('text.last_name', Form::text('last_name', old('last_name'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('last_name') !!}
                                <!-- user_type -->
                                {!! updateInput('text.user_type', Form::select('user_type_id', $user_types, old('user_type_id'), ['required' => 'required', 'class' => 'form-control']) ) !!}
                                {!! inputErrorRow('user_type_id') !!}
                                <!-- email -->
                                {!! updateInput('text.email', Form::email('email', old('phone'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('email') !!}
                                <!-- password -->
                                {!! updateInput('text.password', Form::password('password', ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('password') !!}
                                <!-- password confirm -->
                                {!! updateInput('text.confirm_password', Form::password('password_confirmation', ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('password_confirmation') !!}
                                <!-- school -->
                                {!! updateInput('text.univeristy_institution_company', Form::text('school', old('school'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('school') !!}
                                <!-- departement -->
                                {!! updateInput('text.departement', Form::text('departement', old('departement'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('departement') !!}
                                <!-- position -->
                                {!! updateInput('text.position', Form::text('position', old('position'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('position') !!}
                                <!-- country -->
                                {!! updateInput('text.country', Form::select('country_id', $countries, old('country_id'), ['required' => 'required', 'class' => 'form-control', 'placeholder' => trans('text.select_country')] )) !!}
                                {!! inputErrorRow('country_id') !!}
                                <!-- position -->
                                {!! updateInput('text.address', Form::text('address', old('address'), ['class' => 'form-control', 'required'=>""]) ) !!}
                                {!! inputErrorRow('address') !!}
                                <!-- position -->
                                {!! updateInput('text.interest', Form::text('interest', old('interest'), ['class' => 'form-control']) ) !!}
                                {!! inputErrorRow('interest') !!}
                                <!-- phone -->
                                {!! updateInput('text.phone', Form::text('phone', old('phone'), ['class' => 'form-control']) ) !!}
                                {!! inputErrorRow('phone') !!}
                                <!-- phone -->
                                {!! updateInput('text.web', Form::text('web', old('web'), ['class' => 'form-control']) ) !!}
                                {!! inputErrorRow('web') !!}

                            </div>

                            <div class="col-md-4 ">
                                {{ trans('auth.profile_photo') }}<br>
                                <small>{{ trans('text.logo_jpg_disclaimer') }}</small>
                                <label for="member_cover_image" class="dropzone_div" id="member_cover_image_label">{{ trans('text.click_to_select_image') }}</label>
                                <input type="file" name="image" id="member_cover_image" style="display: none">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-success">{{ trans('text.register') }}</button>
                            </div>
                        </div>

                    </form>
                </div>

                <!-- REGISTER AS MEMBER -->
                <div id="members" class="tab-pane fade in">
                    <br>
                    <b>{{ trans('users.to_become_a_member_please_register_as_participant') }}</b>
                    <br><br>
                    {!! trans('users.register_member_text') !!}
                    <br><br>

                    <div class="row form-group" style="margin-top: 20px;">

                        <!-- LEFT -->
                        <div class="col-md-8">
                            <!-- platform -->
                            <label>{{ trans('text.platform') }}</label>
                            <i  class="fa fa-spinner fa-pulse fa-lg fa-fw platform_spinner"></i>
                            <select name="platform_select" class="form-control" disabled>
                                <option value="{{ url('/') }}">{{ server_property('platform_title') }}</option>
                            </select>

                            <!-- Type -->
                            {!! updateInput('text.type', Form::select('member_type', $memberGroupTypes , null, ['class' => 'form-control', 'disabled'=>'']) ) !!}
                            <!-- Name -->
                            {!! updateInput('text.name', Form::text('member_title', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Description -->
                            {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'disabled' => "", 'style' => 'height: 300px;']) ) !!}
                            <!-- Reference Person -->
                            {!! updateInput('text.reference_person', Form::text('reference_person', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Reference Person Email -->
                            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Country -->
                            {!! updateInput('text.country', Form::select('country_id', $countries, "", ['class' => 'form-control', 'id'=> 'country_id', 'disabled'=>"", 'placeholder' => trans('text.select_country')]) ) !!}
                            <!-- Address -->
                            {!! updateInput('text.address', Form::text('address', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Website -->
                            {!! updateInput('text.web_site', Form::text('web_site', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Email -->
                            {!! updateInput('text.email', Form::email('email', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}

                        </div>

                        <!-- RIGHT -->
                        <div class="col-md-4">
                            {{ trans('text.logo') }}<br>
                            <small>{{ trans('text.logo_jpg_disclaimer') }}</small>
                            <label class="dropzone_div" style="background-color: #EEE">{{ trans('text.click_to_select_image') }}</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <!-- Submit button -->
                            <button class="btn btn-success" id="create_member_group" disabled>{{ trans('text.register') }}</button>
                        </div>
                    </div>
                </div>

                <!-- REGISTER AS PARTNER -->
                <div id="partners" class="tab-pane fade in">
                    <br>
                    <b>{{ trans('users.to_become_a_partner_please_register_as_participant') }}</b>
                    <br><br>
                    {!! trans('users.register_partner_text') !!}
                    <br><br>

                    <div class="row form-group">

                        <!-- LEFT -->
                        <!-- platform -->
                        <div class="col-md-8">
                            <!-- platform -->
                            <label>{{ trans('text.platform') }}</label>
                            <i  class="fa fa-spinner fa-pulse fa-lg fa-fw platform_spinner"></i>
                            <select name="platform_select" class="form-control" disabled>
                                <option value="{{ url('/') }}">{{ server_property('platform_title') }}</option>
                            </select>

                            <!-- Type -->
                            {!! updateInput('text.type', Form::select('member_type', $memberGroupTypes , null, ['class' => 'form-control', 'disabled'=>'']) ) !!}
                            <!-- Name -->
                            {!! updateInput('text.name', Form::text('member_title', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Description -->
                            {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'disabled' => "", 'style' => 'height: 300px;']) ) !!}
                            <!-- Reference Person -->
                            {!! updateInput('text.reference_person', Form::text('reference_person', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Reference Person Email -->
                            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Country -->
                            {!! updateInput('text.country', Form::select('country_id', $countries, "", ['class' => 'form-control', 'id'=> 'country_id', 'disabled'=>"", 'placeholder' => trans('text.select_country')]) ) !!}
                            <!-- Address -->
                            {!! updateInput('text.address', Form::text('address', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Website -->
                            {!! updateInput('text.web_site', Form::text('web_site', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                            <!-- Email -->
                            {!! updateInput('text.email', Form::email('email', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                        </div>

                        <!-- RIGHT -->
                        <div class="col-md-4">
                                {{ trans('text.logo') }}
                                <br>
                                <small>{{ trans('text.logo_jpg_disclaimer') }}</small>
                                <label class="dropzone_div" style="background-color: #EEE">{{ trans('text.click_to_select_image') }}</label>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col">
                            <!-- Submit button -->
                            <button class="btn btn-success" id="create_member_group" disabled>{{ trans('text.register') }}</button>
                        </div>
                    </div>
                </div>

                <!-- REGISTER NEW NETWORK -->
                <div id="new_network" class="tab-pane fade in">
                    <br>
                    <b>{{ trans('users.to_regiter_a_new_network_please_register_as_participant') }}</b>
                    <br><br>

                    <h4>{{ trans('text.launch_a_new_lens_platform') }}</h4>
                    {!! trans('text.launch_a_new_lens_platform_text') !!}
                    <br><br>

                    <div class="row">
                        <div class="col-md-4">
                            {{ strtoupper(trans('text.download')) }}:
                        </div>
                        <div class="col-md-8">
                            <a href="/download_files/LeNS_PLATFORM_letter_of_intent.docx" class="btn-30 border float-left" style="background-color: white;"><i class="fa fa-download" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <br><br>
                    <!-- Name -->
                    {!! updateInput('text.name', Form::text('name', null, ['class' => 'form-control', 'disabled'=>""]) ) !!}
                    <!-- Description -->
                    {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'disabled' => "", 'style' => 'height: 300px;']) ) !!}
                    <!-- reference_person -->
                    {!! updateInput('text.reference_person', Form::text('reference_person', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                    <!-- reference_person_email -->
                    {!! updateInput('text.reference_person_email', Form::email('reference_person_email', "", ['class' => 'form-control', 'disabled'=>""]) ) !!}
                    <!-- Country -->
                    {!! updateInput('text.country', Form::select('country_id', $countries, "", ['class' => 'form-control', 'disabled'=>'', 'placeholder' => trans('text.select_country')]) ) !!}
                    <div class="row form-group">
                        <div class="col-md-4">
                            {{ trans('text.upload_filled_document') }}
                        </div>
                        <div class="col-md-8">
                            <label for="document_upload" class="dropzone_div" style="background-color: #EEE" id="document_upload_label" disabled>{{ trans('text.click_to_select_document') }}</label>
                        </div>
                    </div>
                    <br>
                    <button class="btn btn-success" disabled>{{ trans('text.register') }}</button>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
    $(document).ready(function(){

        $.get({
            url: '{!! server_property('central_server_address') !!}/api/platform_list_dropdown',
            data: {
                "platform_id": "{!! server_property('server_id') !!}",
            },
            success: function(data, status) {
                $('.platform_select').append(data);
                $('.platform_spinner').toggle();
            },
            error: function(xhr, desc, err) {
                $('.platform_spinner').toggle();
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });


        $('#member_cover_image_label').on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            })
        .on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            $('#member_cover_image_label').html( e.originalEvent.dataTransfer.files[0].name);
            $("#member_cover_image").prop("files", e.originalEvent.dataTransfer.files)
        });

        $('#member_cover_image').change(function(e) {
            $('#member_cover_image_label').html( e.target.files[0].name );
            console.log('ss');
        });
    });

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })

    $('body').on('change', '.platform_select', function(){
        var url = document.location.toString();
        var address = $(this).val() + "/register";
        if (url.match('#')) {
            address = address + "#" +url.split('#')[1];
        }
        window.location.href = address;
    });

</script>
@endsection
