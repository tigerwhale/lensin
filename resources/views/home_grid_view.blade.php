<!--
'id' => $item->id, 
'title' => $item->name, 
'author' => $author, 
'country' => $item->country->name, 
'year' => $item->year, 
'language' => $language_name, 
'type' => 'course', 
'platform' => str_replace('http://', "", url("/")),
'platform_id' => server_property('server_id'),
'published' => $item->published,
'image' => $item->image,
'created_at' => $item->created_at,
-->

<div class="row">
	@foreach($grid_data->getCollection()->all() as $grid_item)

		<li class="col-xs-3 datatable_grid_div" style="margin-top: 0px; cursor: pointer;" data-link="{!! ($grid_item['type'] == 'news') ? 'new' : $grid_item['type'] !!}s/view/{!! $grid_item['id'] !!}?server_id={!! $grid_item['platform_id'] !!}">

			<!-- IMAGE DIV -->
			@if (isset($grid_item['image']) && $grid_item['image'])
				<div class="lecture_grid_image hover_below" style="background-image: url({!! $grid_item['image'] !!}); background-size: cover;"></div>
			@else
				<div class="lecture_grid_image hover_below">
					<img src="/images/resource/{!! $grid_item['type'] !!}_big.png" />
				</div>
			@endif

			<div class="row">
				<div class="col-xs-12 lecture_grid">

					<!-- TEXT DIV -->
					<div class="home_grid_text {!! $grid_item['type'] !!}_hover text-left">
						<div class="row">
							<!-- name -->
							<div class="col-xs-12 line-limit" style="height: 105px;">
								{{ mb_strimwidth($grid_item['title'], 0, 35, "..."); }}
							</div>
						</div>


						<div class="row">
							<!-- icon -->
							<div class="col-xs-3 text-left">
								<div class="btn-{!! $grid_item['type'] !!}-small btn-smaller shadow-small"></div>
							</div>
							
							<!-- date -->
							<div class="col-xs-6 text-center grid_date">
								{!! strtoupper(Carbon\Carbon::parse($grid_item['created_at']['date'])->format('M y')) !!}
							</div>
							<!-- platform icon -->
							<div class="col-xs-3" style="padding-top: 5px;">
								<div class="platform_icon btn-smallest pull-right" style="background-image: url(http://{!! $grid_item['platform'] !!}/images/server/logo.png); "></div>
							</div>
						</div>
					</div>

				</div>
			</div>															

		</li>
	@endforeach
</div>


<!-- pagination if needed -->
@if ($grid_data->lastPage() > 1) 
	
	<div id="pagination"></div>
	
	<script type="text/javascript">

		var options = {
				currentPage: {!! $grid_data->currentPage() !!},
	            totalPages: {!! $grid_data->lastPage() !!},
	            onPageClicked: function(e,originalEvent,type,page){
                	loadGrid(page);
            	}
	        }

		$('#pagination').bootstrapPaginator(options);


	</script>
@endif

<script type="text/javascript">
	$(document).ready(function(){
		lineLimit();
	});
</script>