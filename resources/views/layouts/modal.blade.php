<div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header" id='main_modal_header'>
			<h4>{{ $header }}</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body" id="main_modal_body">
			{{ $body }}
		</div>
	</div>
</div>

