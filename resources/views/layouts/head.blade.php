    <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

    <!-- JAVASCRIPT -->
        <!-- Jquery, Bootstrap, Vue, Sizzle, Datatalbes-->
        <script src="/js/app.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/jquery.dataTables.min.js"></script>
        <script src="/js/jquery.dataTables.yadcf.js"></script>
        <script src="/js/jquery.easing.min.js"></script>
        <script src="/js/jquery.scombobox.min.js"></script>
        <script src="/js/bootbox.min.js"></script>
        <script src="/js/lens.js"></script>
        <script src="/js/video-js/video.js"></script>
        <script src="/js/bootstrap-paginator.js"></script>
        <script src="/js/lightbox.js"></script>
        <script src="/js/jquery.sticky.js"></script>
        <script src="/js/jquery.cookie.min.js"></script>
        <script src="/js/jquery.colorbox-min.js"></script>
        <script src="/js/jquery.dirtyforms.min.js"></script>
        <script src="/js/bootstrap-multiselect.js"></script>
        <script src="/fontawesome/js/all.min.js"></script>

    @if (isset($tinymce))
            <script src="/js/tinymce/tinymce.min.js"></script>
        @endif
        

    <!-- CSS -->
        <!-- Font awsome -->
{{--        <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>--}}
        <link rel="stylesheet" type="text/css" href="/css/fontawesome/css/all.min.css"/>


    <!-- Bootstrap -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- Datatables --> 
        <link href="/css/jquery.dataTables.min.css" rel="stylesheet">

        <!-- Simle combo box -->
        <link href="/css/jquery.scombobox.min.css" rel="stylesheet">
        
        <!-- Multiselect button -->
        <link href="/css/bootstrap-multiselect.css" rel="stylesheet">
        
        <!-- Custom Lens CSS -->
        <link href="/css/lens.css" rel="stylesheet">
        <link href="/css/lens-fonts.css" rel="stylesheet">

        <!-- DROPZONE -->
        <link href="/css/dropzone.css" rel="stylesheet">

        <!-- Ubuntu font --> 
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,500,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">

        <!-- Video js -->
        <link href="/js/video-js/video-js.css" rel="stylesheet">
        <link href="/css/lightbox.css" rel="stylesheet">
        <link href="/css/colorbox.css" rel="stylesheet">


        <script type="text/javascript">
            var user_id = '{!! Auth::user() ? Auth::user()->id : "" !!}';
            var locale = '{!! Config::get('locale') !!}';
            var searchData = "";
            var criteriaTabData = "";
        </script>