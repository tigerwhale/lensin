<script type="text/javascript">

    @if (session()->has('message'))
        alert('{{ session('message') }}');
    @endif  


    // VARS
    jQuery.support.cors = true;
    var advanced_research_multiselect; 
    var userScrolling = false;
    var locale = "{!! Session::get('locale') !!}";
    var token = $("meta[name='csrf-token']").attr("content");    
    var central_server = {!! server_property('central_server') ? 1 : 0 !!};
    var server_id = {!! server_property('server_id') ? server_property('server_id') : 0 !!};
    var lastCheckedFilter = "";
    var datatable = "";
    var advancedResearchFields = [
        'author', 
        'country', 
        'year', 
        'institution', 
        'length', 
        'switch', 
        'category', 
        'course', 
        'criteria', 
        'contents', 
        'language',
        'producer', 
        'contents',
        'resource_type'        
        ];
    
    var login_send = ""; 

    @if (Auth::user()) 
      login_send = "&token={!! Auth::user()->login_token !!}&email={!! Auth::user()->email !!}&server_id={!! server_property('server_id') ?: 0 !!}"; 
    @endif 



    // USER SCROLL
    $(document).scroll(function(){
        // If window is 400px from bottom AND there is more data in tables, add rows
        if ($(window).scrollTop() + $(window).height() + 400 > $(document).height() && datatable.rows( { search:'applied' } )[0].length > datatable.page.len() ) 
        {
            $("#spinnerDiv").show();
            userScrolling = true;
            var rowsToAdd = 12;
            var pageLength = datatable.page.len();

            datatable.page.len( pageLength + rowsToAdd ).draw();
            userScrolling = false;

            // add rows to grid
            userScrolling = false;
            var rowsToAddIndex = datatable.rows({ search:'applied' });
            rowsToAddIndex = rowsToAddIndex[0].slice(pageLength, pageLength + rowsToAdd);

            rowsToAddIndex.forEach(function(element) {
                addRowToDatatable(null, datatable.data()[element]);
            });
            lineLimit();
        } 
        
        if (datatable.rows( { search:'applied' } )[0].length < datatable.page.len())
        {
            $("#spinnerDiv").hide();
        }
    })


    // DOCUMENT READY
    $(document).ready(function() {
        // Make sticky sidebars
        $(".left_filter_menu").sticky({
            topSpacing: 0,
            bottomSpacing: 300
        });

        // Open/close advanced search
        $('body').on('click', '.clear_advanced_search_filters', function()
        {
            $("#advanced_research :checkbox:checked").click();
        });

        // Video cookie
        if ($.cookie('hide-div')) {
            $("#close-me").hide();
            $("#show-video").show();
        } else {
            $("#close-me").show();
            $("#show-video").hide();
        }

        $(".close-div").click(function() {
            $("#close-me").hide();
            $("#show-video").show();
            $.cookie('hide-div', true);
        });

        $(".show-video-button").click(function() {
            $("#close-me").show();
            $("#show-video").hide();
            $.cookie('hide-div', false);
        });        

        $(".show-div").click(function() {
            $("#close-me").hide();
            $.cookie('hide-div', true);
        });


        $('body').on('click', '.close-icon', function(){
            $(".header-search").val('');
            $(".header-search").click();
        });

        // DELETE RESOURCE FILE
        $('body').on('click', '.delete_resource_file', function(){

            var token = $("meta[name='csrf-token']").attr("content"); 
            var resource_id = $(this).data('resource-id');

            bootbox.confirm("{{ trans('courses.delete_content_question') }}", function(result){ 
                if (result) {
                    $.ajax({
                        url: '/courses/resource/delete_file/' + resource_id,
                        type: 'POST',
                        data: {
                            '_token': token,
                            //'file_index': file_index
                          },
                        success: function(data, status) {
                            resourceModalFilePreviewUpdate(resource_id);
                        },
                        error: function(xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    });
                }
            });
        });


        // Open/close advanced search
        $('body').on('click', '.header-advanced-search', function()
        {
            $("#advanced_research").toggle();

            if( $('#advanced_research').is(":visible") ) {
                $('#home_info').hide();
                $('#news').hide();
            } else {
                $('#home_info').show();
                $('#news').show();
            }
        });
    });


    /**
     * Get data from all servers
     */
    function data_from_all_servers(url_path, return_function) {
        var return_json = [];
        $.when(
            <?php $array_of_all_servers = array_of_all_servers(); ?>
            @if (isset($array_of_all_servers))
                @foreach( $array_of_all_servers as $server )
                    $.post({
                        beforeSend: function () {
                        },
                        url: "{!! $server->address !!}" + url_path,
                        timeout: 12000, // MR from 5000
                        success: function(returned_data, status) 
                        {
                            if (returned_data) {
                                return_json = return_json.concat(returned_data);
                            };
                        },
                        global: false,
                        error: function(xhr, desc, err) {
                            console.log( 'err: {!! $server->address !!}' + url_path);
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                        }
                    })
                    @if (!$loop->last)
                        ,
                    @endif
                @endforeach
            @endif

        ).always(function() {
            return_function(return_json);
        });
    }  


    /**
     * Put POST request to div
     * @param  {string}   url_path
     * @param  {string}   div_id       #id of the target div
     * @param  {json}     data_to_send
     * @param  {Function} callback     callback function
     */
    function ajax_post_to_div(url_path, div_id, data_to_send, callback) {

        if (data_to_send == undefined) { data_to_send = ""; }
        if (callback == undefined) { callback = ""; }

        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.post({
            url: url_path,
            data: {
                '_token': token,
                'data': JSON.stringify(data_to_send),
              },
            success: function(data, status) {
                $("#" + div_id).html(data);
                if (callback){
                    callback();
                }
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
                var errorDebug = eval("(" + xhr.responseText + ")");
            },
        });
    }


    /**
     * Load Search data to global variable
     */
    function loadSearchData() {
        // Set the url
        var url = "/api/all_resources";

        // Function to run for each server
        var save_search_data = function save_search_data(search_data) 
        {
            searchData = search_data;

            // sort data
            searchData = searchData.sort(function(a,b){
              return (a.created_at_formated - b.created_at_formated);
            });

            loadIndexGridTable();
            updateAdvancedResearchFields();
        }

        // Run the load 
        data_from_all_servers(url, save_search_data);
    }

    var loadCriteriaTabFn; 

    /**
     * Load criteria tab data
     */
    function loadCriteriaData() {

        // Set the url
        var url = "/api/study_cases/select_by_criteria_data";

        // Function to run for each server
        var save_criteria_data = function save_criteria_data(criteria_data) {
            criteriaTabData = criteria_data;
        }
        
        // Load the data from all servers
        data_from_all_servers(url, save_criteria_data);

        // Wait for data to load
        loadCriteriaTabFn = window.setTimeout( function(){loadCriteriaTab()}, 8000);    // MR FROM 5100
    }


    /**
     * Display criteria tab
     * @return {view}
     */
    function loadCriteriaTab() 
    {
        // Clear criteria tab 
        $("#criteria_tab").html("");
        $("#criteria_view_content").html("");

        // Clead div and set loader
        loaderToDiv("select_by_criteria");

        // Clear timeout
        clearTimeout(loadCriteriaTabFn);

        // Get selected platforms
        var serverids = [];
        $('.platforms:checked').not("#all_platforms_switch").each(function() {
            serverids.push( $(this).data("server-id") );
        });

        // Add selected platforms to data to be sent
        var loadCriteriaTabData = new Array(); 
        loadCriteriaTabData.push(serverids);

        // Add criteria data to data to send
        loadCriteriaTabData = loadCriteriaTabData.concat(criteriaTabData);

        // Load criteria tab view
        ajax_post_to_div("/study_cases/select_by_criteria_view", "criteria_view_content", loadCriteriaTabData, removeLoader);
    };


    /**
     * Place loader in div
     */
    function loaderToDiv(div_id) 
    {
        removeLoader();
        $("#" + div_id).append('<div class="loader text-center" style="width:100%"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
    }
    
    var removeLoader = function removeLoader() {
        $(".loader").remove();
    }


    /**
     * Filter selected options 
     * @return {string}    string formated for url
     */
    function filterOptionsSelectedForGetRequest() {
        //var fields = ['author', 'country', 'year', 'language', 'institution', 'length', 'switch'];
        var optionsSelected = [];
        var advancedResearchFieldsWithPlatforms = [];
        advancedResearchFieldsWithPlatforms = advancedResearchFieldsWithPlatforms.concat(advancedResearchFields);
        advancedResearchFieldsWithPlatforms.push('switch_platform');

        $.each(advancedResearchFieldsWithPlatforms, function(key, fieldName) {

            $('.' + fieldName + ' input:checkbox:checked').each(function() {
                if ( $(this).val() ) {
                    data = $(this).val().toString();
                    data = data.replace(/&/g, "");
                    optionsSelected =  optionsSelected + "&" + fieldName + '[]=' + data;
                }
            });
        });

        return optionsSelected;
    }

    // SHOW CHECKBOXES FROM FILTERS
    function filterAdvancedResearchOptions() 
    {
        filteredDataToSearch = datatable.rows( { filter : 'applied'} ).data();

        $.each(advancedResearchFields, function(key, fieldName) {
            if (fieldName != lastCheckedFilter) {
                $(".multiselect-container > li." + fieldName + " > a > label").hide();
            }
            eval(fieldName + 'Data = []') ;

            $.each(filteredDataToSearch, function(key, singleEntery) {
                if (singleEntery[fieldName]) {
                    $('input[value="' +  singleEntery[fieldName] + '"]').parent().show();
                }
            });
        });

        lastCheckedFilter = "";
        checkIfFiltersAreEmpty();
    }

    function updateAdvancedResearchFields() {
        // var fields = ['author', 'country', 'year', 'language', 'institution', 'length'];
        // Check each field
        $.each(advancedResearchFields, function(key, fieldName) {
            // Make an empty array
            eval(fieldName + 'Data = []') ;
            // Get data and fill array
            $.each(searchData, function(key, singleEntery) {
                if (singleEntery[fieldName]) {
                    data = singleEntery[fieldName].toString();
                    data = data.replace(/"/g, "'");
                    // data = data.replace('\u2028', '');  // MR
                    data = data.replace(/\u2028/g, '');
                    eval(fieldName + 'Data.push("' + data + '")');
                }
            });

            // Sort data
            eval(fieldName + "Data = " + fieldName + "Data.filter( onlyUnique ).sort()");
            // Add data to select
            $.each(eval(fieldName + 'Data') , function(key, value) {
                // For contents split the search fields
                if (fieldName == "contents") {
                    var valuesArray  = value.split(" | ");
                    $.each(valuesArray, function(key, contentValue) {
                        if(contentValue) {
                            $('#advanced_research_' + fieldName).append($("<option/>", {
                                value: contentValue,
                                text: contentValue,
                                class: fieldName
                            })); 
                        }
                    })
                } else {
                    $('#advanced_research_' + fieldName).append($("<option/>", {
                        value: value,
                        text: value,
                        class: fieldName
                    }));
                }
                
                
            });
        });     

        // Make an empty array
        languageData = [];
        // Get data and fill array
        $.each(searchData, function(key, singleEntery) {
            if (singleEntery['language']) {
                data = singleEntery['language'].toString();
                data = data.replace(/"/g, "'");
                languageData.push(data);
            }
        });
        // sort data
        languageData = languageData.filter( onlyUnique ).sort();

        $('#resourcesLanguageSelect').html('');
        $.each(languageData, function(key, value) {
            var label = $('<label />', { class: 'checkbox'}).appendTo('#resourcesLanguageSelect');
            $('<input />', { type: 'checkbox', value: value, class: "language", onChange: 'checkDatatableFilters()' }).appendTo(label);
            label.append(value);
        });

        initFiltersMultiselect();
        initFiltersFromUrl();
        checkIfFiltersAreEmpty();
    }

    function checkIfFiltersAreEmpty() 
    {
        $(".multiselect-native-select").each(function()
        {
            var toShow = 0;
            $(this).find('label.checkbox').each(function() 
            {
                if ( !$(this).parent().hasClass('multiselect-all') && $(this).css("display") != "none" ) {
                    toShow = 1;
                }
            });

            if (toShow) {
                $(this).show();
            } else {
                $(this).hide();
            } 
        });
    }

    function initFiltersFromUrl() {
        // var fields = ['author', 'country','year', 'language', 'institution', 'length', 'switch'];
        selectedOptions =  getParams();
        $(advancedResearchFields).each(function(key, field) {
            if ( selectedOptions[field] ) {
                $(selectedOptions[field]).each(function(key, value) {
                    $('.' + field + ' :checkbox[value="' + value + '"]').click();
                    // $('#advanced_research').show();
                });
            }
        });
    }


    function initFiltersMultiselect()
    {
        advanced_research_multiselect = $('.advanced_research_select').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            selectAllText: '{{ trans('advanced_research.select_all') }}',
            filterPlaceholder: '{{ trans('text.search') }}...',
            enableCaseInsensitiveFiltering: true,
            buttonClass: "btn advanced_research_button", 
            onChange: function(option, checked, select) {
                lastCheckedFilter = $(option).attr('class');
                checkDatatableFilters();
            },
            enableHTML: true,
            inheritClass: true,
            buttonText: function(options, select) {
                if (options.length === 0) {
                    return $(select).data('buttontext');
                }
                else if (options.length < 3){
                    var labels = [];
                    options.each(function() {
                        if ($(this).attr('label') !== undefined) {
                            labels.push($(this).attr('label'));
                        }
                        else {
                            labels.push($(this).html());
                        }
                    });
                    return $(select).data('buttontype') + ': </span><span class="advanced_research_button_selected">' + labels.join(', ') + '';
                } else {
                    return $(select).data('buttontype') + ': ' + '</span><span class="advanced_research_button_selected">{{ trans('advanced_research.multiple_selected') }}';
                }
            },
            maxHeight: 400,
            templates: {
                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-danger" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></span>',
            }
        });
    }

    function onlyUnique(value, index, self) { 
        if (value) {
            return self.indexOf(value) === index;    
        }
    }


    function loadIndexGridTable() {
        $("#loaderContainer").show();
        datatable = $('#datatable').DataTable({
            processing: true,
            responsive: true,
            stateSave: false, 
            data: searchData,
            dom: '<"top">rt<"bottom"><"clear">',
            "order": [[ 9, "desc" ]],
            "lengthMenu": [[12, 24, 48, 96, -1], [12, 24, 48, 96, "All"]],
            
            initComplete: function () {
                $("#datatable").hide();

                @if (isset($_GET['platforms']))
                    @foreach ($_GET['platforms'] as $platform)
                        $( "#platform_{!! $platform !!}" ).trigger( "click" );
                    @endforeach
                @endif
                
                if (!document.getElementById("spinnerDiv")) 
                {
                    var spinnerDiv = '<div id="spinnerDiv" class="text-center" style="width:100%; display:none"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>';
                    $("#datatable_wrapper").append(spinnerDiv);
                }
                return true;                
            }, 
            "fnDrawCallback": function( oSettings ) {
                // Limit text in grid
                lineLimit();
                // Set user scrolling var
                userScrolling = false;
                // Hide loading container
                $("#loaderContainer").hide();
                // If no rows, add "No resources found" text
                setTimeout(function() {
                    areThereResourcesInDatatable();
                },1000)
            },
            "fnPreDrawCallback": function (oSettings) {
                if (!userScrolling) {
                    // check if div exists and delete it
                    if ( $("#grid_datatable").length != 0) {
                        $("#grid_datatable").html("");
                    } else {
                        // create a new grid div
                        var grid_datatable = $("<div>", {
                            "id": "grid_datatable",
                            class: "row"
                        });    
                        // add it after list table in datatable
                        $("#datatable").after(grid_datatable);
                    }  
                }

            },
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                return addRowToDatatable( nRow, aData );
            },
            aoColumnDefs: [
                {
                    "mRender": function ( data, type, row ) {
                        return "<span style='display:none'>" + row.type + "</span><div class='text-center'><div class='btn-" +  row.type + "-small shadow-small' style='margin-bottom:4px;'></div><img src='http://" +  row.platform + "/images/server/logo.png' class='table_view_image platform'></div>";
                    },
                    "sWidth": "20px",
                    "aTargets": [ 0 ]
                },
                {
                    "mRender": function ( data, type, row ) {
                        return row.title;
                    },
                    "sWidth": "50%",
                    "aTargets": [ 1 ]
                },
                {
                    "mRender": function ( data, type, row ) {
                        return row.author;
                    },
                    "className": "hidden-xs",
                    "aTargets": [ 2 ]
                },
                {
                    "mRender": function ( data, type, row ) { return row.country; },
                    "className": "hidden-md hidden-sm hidden-xs",
                    "aTargets": [ 3 ]
                },
                {
                    "mRender": function ( data, type, row ) { return row.year; },
                    "className": "hidden-xs",
                    "aTargets": [ 4 ]
                },
                {
                    "mRender": function ( data, type, row ) { return row.language; },
                    "className": "hidden-xs",
                    "aTargets": [ 5 ]
                }, 
                {
                    "mRender": function ( data, type, row ) { return row.type; },
                    "aTargets": [ 6 ],
                    "visible": false
                }, 
                {
                    "mRender": function ( data, type, row ) { return row.platform; },
                    "aTargets": [ 7 ],
                    "visible": false
                }, 
                {
                    "mRender": function ( data, type, row ) { return row.platform_id; },
                    "aTargets": [ 8 ],
                    "visible": false
                }, 
                {
                    "mRender": function ( data, type, row ) { return row.created_at_formated; },
                    "aTargets": [ 9 ],
                    "visible": false
                },   
                {
                    "mRender": function ( data, type, row ) { return row.category; },
                    "aTargets": [ 10 ],
                    "visible": false
                },
                {
                    "mRender": function ( data, type, row ) { return row.length; },
                    "aTargets": [ 11 ],
                    "visible": false
                },  
                {
                    "mRender": function ( data, type, row ) { return row.course; },
                    "aTargets": [ 12 ],
                    "visible": false
                },
                {
                    "mRender": function ( data, type, row ) { return row.criteria; },
                    "aTargets": [ 13 ],
                    "visible": false
                },
                {
                    "mRender": function ( data, type, row ) { return row.category; },
                    "className": "hidden-md hidden-sm hidden-xs",
                    "aTargets": [ 14 ],
                    "visible": true
                },
                {
                    "mRender": function ( data, type, row ) { return row.institution; },
                    "aTargets": [ 15 ],
                    "visible": false
                },
                {
                    "mRender": function ( data, type, row ) { return row.producer; },
                    "aTargets": [ 16 ],
                    "visible": false
                },
                {
                    "mRender": function ( data, type, row ) { return row.contents; },
                    "aTargets": [ 17 ],
                    "visible": false
                }                                   
            ],
        });
        
        checkDatatableFilters();
        $('#home_data_table').show();
        lineLimit();
    }


    /**
     * If no rows in Datatable, add "No resources found" text
     */
    function areThereResourcesInDatatable() {
        if (datatable){
            var noResourcesDiv = '<div class="noResourcesDiv text-center" style="width:100%"><br>';
            noResourcesDiv += '<div class="no_courses_badge resource_badge"></div> ';
            noResourcesDiv += '<div class="no_lectures_badge resource_badge"></div> ';
            noResourcesDiv += '<div class="no_tools_badge resource_badge"></div> ';
            noResourcesDiv += '<div class="no_cases_badge resource_badge"></div> ';
            noResourcesDiv += '<div class="no_projects_badge resource_badge"></div>';
            noResourcesDiv += '<br>{{ trans('text.no_resources_found') }}</div>';
            if ( datatable.rows({ search:'applied' })[0].length == 0 ) {
                $("#spinnerDiv").hide();
                $(".noResourcesDiv").remove();
                $("#datatable_wrapper").append(noResourcesDiv);
            } else {
                $(".noResourcesDiv").remove();
            }    
        }
    }



    function addRowToDatatable(nRow, aData, datatableDiv = "#grid_datatable") {
        // if the user is not scrolling, add rows
        if (userScrolling == false) {
            $(nRow).addClass("datatable_row");
            
            var data_link = aData.type + "s/view/" + aData.id;
            if (server_id != aData.platform_id) {
                data_link = data_link + "?server_id=" + aData.platform_id;
            }

            // Add main div
            var div = $("<div>", {
                "class": "col-xs-12 col-sm-6 col-md-4 col-lg-3 datatable_grid_div",
                "style": "margin-top: 0px; cursor: pointer;",
                "data-link": data_link
            });

            // Add main div
            var datatable_grid_width_div = $("<div>", {
                "class": "datatable_grid_width_div",
            });

            // Image container
            var image_div_container = $("<div>", {
                "class": "datatable_grid_image_container",
                "data-link": data_link
            });

            // image
            if (aData.image) {
                var image = aData.image;
            } else {
                var image = "/images/resource/" + aData.type + "_big.png";
            }
            
            var image_div = $("<div>", {
                "class": "datatable_grid_image",
                "style": "background-image: url(" + image + ")",
                "data-link": data_link
            });

            image_div_container.append(image_div);
            datatable_grid_width_div.append(image_div_container);
            div.append(datatable_grid_width_div);

            // row
            var row = $("<div>", { class: "row" }); 
            datatable_grid_width_div.append(row);

                // xs
                var xs_div = $("<div>", { class: "col-xs-12 grid_xs"}); 
                row.append(xs_div);
                    // text div
                    var home_grid_text = $("<div>", { class: "home_grid_text " + aData.type + "_hover text-left"});
                    xs_div.append(home_grid_text);
                        // row 2 
                        var row2 = $("<div>", { class: "row"}); 
                        home_grid_text.append(row2);
                            // text div
                            var text_div = $("<div>", { class: "col-xs-12 line-limit", style: "height: 105px;", html: aData.title}); 
                            row2.append(text_div);
                        // row 3
                        var row3 = $("<div>", { class: "row"}); 
                        home_grid_text.append(row3);
                            // icon div
                            var icon_div = $("<div>", { class: "col-xs-3 text-left"});
                            row3.append(icon_div);
                                // icon
                                var icon = $("<div>", { class: "btn-" + aData.type + "-small btn-smaller shadow-small"});
                                icon_div.append(icon);
                            // date
                            var date = $("<div>", { class: "col-xs-6 text-center grid_date", html: aData.year });
                            row3.append(date);
                            // platform icon div
                            var platform_icon_div = $("<div>", { class: "col-xs-3", style: "padding-top: 5px;"});
                            row3.append(platform_icon_div);
                                // platform icon
                                var platform_icon = $("<div>", { 
                                    class: "platform_icon btn-smallest pull-right", 
                                    style: "background-image: url(http://" + aData.platform + "/images/server/logo.png"});
                                platform_icon_div.append(platform_icon);
                // append to grid view
                $(datatableDiv).append(div);
            }

        return nRow;
    }


    function checkDatatableFilters(){

        console.log('checkDatatableFilters');
        var choosedFilter = "";
        
        var searchBySearchField = datatableSearchBySearchField();
        var searchPlatforms = datatableSearchPlatforms();
        var searchAdvancedFilter = datatableSearchAdvancedFilter();

        // hide news and introducery video if there is search
        if (searchBySearchField || searchAdvancedFilter) 
        {
            $('#home_info').hide();
            $('#news').hide();
            // $("#advanced_research").show();
        } else {
            $('#home_info').show();
            $('#news').show();
            // $("#advanced_research").hide();
        }
        datatable.draw();
        showOrHideCriteriaViewSelector();
        filterAdvancedResearchOptions();
    }

    function showOrHideCriteriaViewSelector() {
        if ($('.resources:checkbox:checked').length == 1 && $('.resources:checkbox:checked:first').val() == "study_case") 
        {
            $('#criteria_view_selector').show();
            $('#criteria_view_selector_resource_header').addClass("no_border");
        } else {
            $('#criteria_view_selector').hide();
            $('#criteria_view_selector_resource_header').removeClass("no_border");
            showResources();
            hideCriteria();
        }
    }


    /**
     * Hide criteria view
     */
    function hideCriteria(){
        // hide criteria selector
        $('#criteria_header').hide(); 
        // hide criteria view
        $('#select_by_criteria').hide();
    }

    function datatableSearchAdvancedFilter() 
    {
        // CHECKBOXES
        var checkboxes = [
            '', // icons
            '', // title
            'author', 
            'country', 
            'year', 
            'language', 
            'type', 
            'platform', 
            '', // server id
            '', // date
            'category', 
            'length', 
            'course', 
            'criteria',
            '', // category
            'institution', 
            'producer',
            'contents'
            ];
        var search = false;

        $(checkboxes).each(function(index, value) {
            if (value) {
                choosenFilter = "";
                // get the values
                $('.' + value + ':checked').each(function() {
                    choosenFilter = choosenFilter + $(this).val() + "|";
                    search = true;
                });
                // remove the last |
                choosenFilter = choosenFilter.slice(0,-1);
                // search the table
                datatable.column( index ).search(choosenFilter, true, false);
            }
        });
        datatable.draw();

        return search;
    }


    function datatableSearchBySearchField() 
    {
        datatable.search( $(".header-search").val() );
        datatable.draw();
        if ( $(".header-search").val() != "" ) {
            return true;
        }
        return false;
    }

    function datatableSearchPlatforms() 
    {
        // PLATFORMS
        var platformsSelected = [];
        $('.platforms:checked').each(function() {
            platformsSelected.push("\\b" + $(this).data('server-id') + "\\b");
        });

        platformFilter = "";
        platformFilter = platformsSelected.join("|");

        if (platformFilter == "")
        {
            platformFilter = "no server";
            return false;  
        } 
        
        // search the table
        datatable.column( 8 ).search(platformFilter, true, false, true);
        datatable.draw();

        return true;
    }


    $('.header-search').on( 'keyup click', function () {
        if ($(this).val() == "") 
        {
            $(".close-icon").hide();
        } else {
            $(".close-icon").show();
        }        
        checkDatatableFilters();
    });
   
</script>