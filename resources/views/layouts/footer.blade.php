<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-2 text-center padding-small">
                <img src="/images/erasmus.png"  style="height: 40px">
            </div>
            <div class="col-xs-8 text-center padding-small">
                {!! social_icons() !!}
            </div>
            <div class="col-xs-2 text-center padding-small">
                <img src="/images/by.png">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 platform-version text-center">
                {{ trans('text.platform_version') }}
            </div>
        </div>

    </div>
</footer>

<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
</script>