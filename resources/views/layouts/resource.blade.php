<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! server_property('platform_title') !!}</title>

    @include('layouts.head')
    <script src="/js/dropzone.js"></script>
    
</head>

<body>

    <!-- MODAL -->
    <div id="main_modal" class="modal fade" role="dialog">
    </div>

    <div id="wrapper">
    
        <!-- HEADER --> 
        <header>
            @include('flash::message')
        </header>
            
        <!-- CONTENT -->
        <article>
            @yield('content')
        </article>
        
        <!-- FOOTER 
        @include('layouts.footer')
        --> 
    </div><!-- #wrapper -->

    <div id="modalDiv"></div>


    @include('layouts.app_js')

</body>
</html>
