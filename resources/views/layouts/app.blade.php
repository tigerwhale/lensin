<?php
App::setLocale(Session::get('locale'));
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! server_property('platform_title') !!}</title>
    @include('layouts.head')
    <script src="/js/dropzone.js"></script>
</head>

<body>

<!-- HEADER -->
<header>
    @include('flash::message')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    @if ( Auth::user() )
        @if ( Auth::user()->hasRole('manageusers') )
            @if ( \App\User::where('email', 'LIKE', 'administrator@lens-international.org')->first() )
                <div class="alert alert-danger">
                    {{ trans('text.please_remove_default_user') }}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>

            @endif
        @endif
    @endif
</header>

<div id="wrapper" style="padding-top: 20px;">

    <!-- HEADER -->
    <header>

        <!-- SMALL SCREENS -->
        <div class="container visible-xs-block">
            <div class="navbar-header">
                <!-- NAV BAR -->
                <div class="col-xs-3">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- LOGO -->
                <div class="col-xs-6">
                    <a href='/'><img src="{!! logo() !!}" class="header-logo"></a><br>
                </div>
            </div>
        </div>

        <!-- LARGE SCREEN -->
        <div class="container hidden-xs">
            <div class="row">

                <!-- LOGO -->
                <div class="col-xs-2 text-center">
                    <a href="/"><img src="{!! logo() !!}" class="header-logo"></a>
                    <div class="header-logo-title">{{ server_property('slogan') }}</div>
                </div>

                <!-- SEARCH -->
                <div class="col-xs-4" style="padding-top: 3px;">
                    <div>
                        <input type="text" class="form-control header-search"
                               placeholder="&#xF002; {{ trans('text.search') }}..."/>
                        <button class="close-icon" type="reset">X</button>
                    </div>
                    <div class="dropdown">
                        <span class="header-advanced-search text-lens">
                            {{ trans('text.advanced_research') }}
                            <span class="caret"></span>
                        </span>
                    </div>
                </div>

                <!-- MENU -->
                <div class="col-xs-4" style="text-align: right;">
                    <!-- menu icons -->
                    <div class="header-menu-icons">
                        <a href="/contact">
                            <img src="/images/header-menu/contact.png">
                            <br>{{ strtoupper(trans('text.contact')) }}
                        </a>
                    </div>
                    <div class="header-menu-icons">
                        <a href="/tutorials">
                            <img src="/images/header-menu/tutorials.png">
                            <br>{{ strtoupper(trans('text.tutorial')) }}
                        </a>
                    </div>
                    <div class="header-menu-icons">
                        <a href="/network">
                            <img src="/images/header-menu/network.png">
                            <br>{{ strtoupper(trans('text.network')) }}
                        </a>
                    </div>
                    <div class="header-menu-icons">
                        <a href="/labs">
                            <img src="/images/header-menu/labs.png">
                            <br>{{ strtoupper(trans('text.labs')) }}
                        </a>
                    </div>
                    <div class="header-menu-icons">
                        <a href="/about">
                            <img src="/images/header-menu/about.png">
                            <br>{{ strtoupper(trans('text.about')) }}
                        </a>
                    </div>
                </div>

                <!-- PROFILE -->
                <div class="col-xs-2">
                    <!-- menu if logged -->
                    @if (Auth::check())

                        <div class="dropdown text-lens">

                            <!-- user image -->
                            <div class="header-menu-profile dropdown-toggle" id="dropdownUser" data-toggle="dropdown"
                                 aria-haspopup="true" aria-expanded="true"
                                 style="background-image: url('{{ user_image(Auth::user()->id) }}');"></div>

                            <!-- user menu -->
                            <ul class="dropdown-menu pull-right profile_links" aria-labelledby="dropdownUser">
                                <!-- backend if admin -->
                                @if (Auth::user()->hasGroup('admin'))
                                    <li><a href="/backend">{{ trans('text.admin') }}</a></li>
                                @endif

                            <!-- profile -->
                                @if (Auth::user())
                                    <li>
                                        <a href="{!! Auth::user()->server_url !!}/profile?token={!! Auth::user()->login_token !!}&email={!! Auth::user()->email !!}&server_id={!! Auth::user()->server_id ?: 0 !!}">{{ trans('text.profile') }}</a>
                                    </li>
                            @endif

                            <!-- Projects modal list -->
                                <li><a href="#" class="modal_url"
                                       data-url="/profile/groups_list/{{ Auth::user()->id }}">{{ trans('users.my_projects') }}</a>
                                </li>
                                <li>
                                    <hr>
                                </li>

                                <!-- logout -->
                                <li><a href="/logout">{{ trans('text.logout') }}</a></li>
                            </ul>
                        </div>
                        <div class="header-logo-title">{{ Auth::user()->name }}</div>
                    @else
                    <!-- menu if NOT logged -->
                        <a href="/login">
                            <div class="header-menu-profile" style="background-image: url('{{ user_image() }}');"></div>
                        </a>
                        <div class="header-logo-title">{{ trans('text.login') }}</div>

                    @endif
                </div>

            </div>
        </div>

    </header>

    <!-- CONTENT -->
    <article>
        @yield('content')
    </article>

    <!-- FOOTER -->
    @include('layouts.footer')


</div><!-- #wrapper -->

<!-- MODAL -->
<div id="main_modal" class="modal fade" role="dialog">
</div>

@include('layouts.app_js')

<div id="loaderContainer">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
</div>
</body>
</html>



@if (isset($errors))
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <script>
                    // alert('{{ $error }}');
                </script>
            @endforeach
        </div>
    @endif
@endif
