<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content">

		<!-- TITLE -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ (isset($modal_data->name) ? $modal_data->name : "") }}</h4>
		</div>

		<!-- FILES -->
		<div class="modal-body">
			
			@foreach($modal_data->resources_published as $resource)

				<div class="row">
					<div class="col-xs-12">
						<h4 class="text-course">{{ $resource->name }}</h4>
					</div>
				</div>
				
				<?php 
					$resource_files = json_decode($resource->path);
					$i = 0;
				?>

				@if ($resource_files)
					@foreach ($resource_files as $resource_file)
						<div class="row hover">
							<div class="col-xs-10">
								<input type="checkbox" data-resource-id="{!! $resource->id !!}" class="download_file" name="download_file_{!! $resource->id !!}_{!! $i !!}" id="download_file_{!! $resource->id !!}_{!! $i !!}" value="{{ $resource_file }}"> <label for="download_file_{!! $resource->id !!}_{!! $i !!}">{{ basename($resource_file) }}</label>
							</div>
							<div class="col-xs-2 text-right">
								<a href="{{ $resource_file }}" class="btn round_button bck-resource" download="" title=""><i class="fa fa-download" aria-hidden="true"></i></a>
							</div>
						</div>
						<?php $i++; ?>
					@endforeach
				@endif 

			@endforeach

			<button type="button" class="btn btn-success" id="download_selected_files">{{ trans('courses.download_selected_files') }}</button>

		</div>
	</div>
</div>

<script type="text/javascript">
	
	$( document ).ready(function() {

		$("#download_selected_files").click( function(){

			var url = "{{ $server_url }}" + "/api/resources/zip_files";

			// get the files that are checked
			var file_list = [];
			$('.download_file:checkbox:checked').each(function(){
				var file_list_item = [$(this).val(), $(this).data('recource-id')];
				file_list.push( file_list_item );
			});

			console.log(url);

			$.ajax({
				type: "POST",
				url: url,
				data: {
					"file_list": file_list
				},

				success: function(responce) {
					console.log(responce);
					top.location.href = responce;
				},

			});

		});

	});


</script>