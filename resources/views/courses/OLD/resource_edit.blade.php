<div class="row" style="background-color: white">
	<div class="col-xs-12">
		

	@foreach ($lecture->resources as $resource)

		<div class="row resource-div hover" id="resource_row_{{ $resource->id }}">
			
			<!-- resource file type -->
			<div class="col-xs-1" id="resource_{{ $resource->id }}_file_type" style="font-size: 14pt; padding-top: 4px;">
				<input type="checkbox" name="resource_{!! $resource->id !!}" class="resource_checkbox resource_checkbox_{!! $lecture->id !!}" data-id="{!! $resource->id !!}">
				@if ($resource->resource_type)
					{!! $resource->resource_type->icon !!}
				@endif
			</div>
			
			<!-- resource name -->
			<div class="col-xs-4 top-padding-small" id="resource_{{ $resource->id }}_name" >
				{!! $resource->name() !!}
			</div>
			
			<!-- resource author -->
			<div class="col-xs-3 top-padding-small" id="resource_{{ $resource->id }}_author">
				{{ $resource->author }}	
			</div>
			
			<!-- resource year -->
			<div class="col-xs-1 top-padding-small"  id="resource_{{ $resource->id }}_year" >
				{{ $resource->year }}
			</div>

			<!-- resource manage buttons -->
			<div class="col-xs-3 text-right">
				<!-- edit resource -->

				<div class="btn bck-course modal_url" data-url="/resources/edit_modal/{{ $resource->id }}" ><i class="fas fa-pencil-alt" aria-hidden="true"></i></div>
				<!-- delete resource -->
				<button data-model='resources' data-delete-id="resource_row_{{ $resource->id }}" data-id={{ $resource->id }} data-text='{{ trans("text.question_delete_resource") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
				<!-- publish / unpublish resource -->
				<?php $icon = ($resource->published == 1) ? "far fa-eye" : 'far fa-eye-slash'; ?>
				<div class="btn bck-course publish_resource" data-url="/resources/publish" data-id="{!! $resource->id !!}" data-delete-id="resource_row_{!! $resource->id !!}">
					<i class="{!! $icon !!}" aria-hidden="true"></i>
				</div>		
			</div>

		</div>

	@endforeach

	<hr>

	<div class="row">
		<div class="col-xs-1">
			<input type="checkbox" class="check_all_resources" title="{{ trans('courses.select_all_resources') }}"  data-lecture-id="{!! $lecture->id !!}">
		</div>
		<div class="col-xs-11 text-right">
				<!-- publish / unpublish selected resource -->
				<button class="btn bck-course publish_selected_resources" type="button" title="{{ trans('courses.publish_selected_resources') }}">
					<i class="far fa-eye" aria-hidden="true"></i>
				</button>		
				<button class="btn bck-course unpublish_selected_resources" type="button" title="{{ trans('courses.unpublish_selected_resources') }}">
					<i class="far fa-eye-slash" aria-hidden="true"></i>
				</button>
				<!-- delete selected resources -->
				<button class="btn btn-danger delete_selected_resources" type="button" title="{{ trans('courses.delete_selected_resources') }}">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>
		</div>
	</div>	



	<script type="text/javascript">
		var current_lecture_id = "{!! $lecture->id !!}";
	</script>

	</div>
</div>
