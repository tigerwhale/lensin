<div class="image_container_inner">
	<img src="{{ $course->image }}" style="width:100%;">
	<!-- bottom popup -->
	<div class="image_change_bottom_cover bck-course">
		<!-- remove image button -->
		<button class="btn btn-danger remove_course_image_button" data-id="1" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
	</div>			
</div>