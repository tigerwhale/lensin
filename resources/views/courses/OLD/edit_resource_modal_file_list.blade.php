<!-- FILE LIST -->
<?php 
	$resource_files = json_decode($resource->path);
?>

@if( $resource_files )
	@foreach ($resource_files as $resource_file)
		
		<div class="row" style="padding-top: 5px; padding-bottom:5px; border-bottom: 1px dashed #CCC;">
			<div class="col-xs-10" style="padding-top: 10px;">
				{!! $resource_file !!}
			</div>
			<div class="col-xs-1">
				<div class="round_button btn-danger delete_resource_file" data-resource-id="{!! $resource->id !!}" data-file-index="{!! $loop->index !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
			</div>
			<div class="col-xs-1">
				<div class="round_button btn-info make_preview_link" data-link="{{ url('/') }}/{!! $resource_file !!}" title="{{ trans('resources.make_this_file_link') }}" data-resource-id="{!! $resource->id !!}" data-file-index="{!! $loop->index !!}"><i class="fa fa-link" aria-hidden="true"></i></div>
			</div>
		</div>
		
	@endforeach
@endif

<script type="text/javascript">

	$('body').on('click', '.make_preview_link', function(){
		$('#preview').val( $(this).data('link') );
		$('#preview').trigger("change");
	});

</script>
		
