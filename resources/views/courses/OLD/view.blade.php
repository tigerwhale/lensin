@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['active_switch' => "courses"])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-8">

                <!-- content header -->
                <div class="row content-header">
                    
                    <!-- left button -->
                    <div class="col-xs-2">
                    </div>
                    
                    <!-- middle -->
                    <div class="col-xs-8 text-center">
						<ul id="content_tabs">
						  	<li class="btn nav-course active">
						  		<a data-toggle="tab" href="#course_info">
						  			{{ strtoupper(trans('text.info')) }}
						  		</a>
						  	</li>
						  	<li class="btn nav-course">
								<a data-toggle="tab" href="#course_courseware">
									{{ strtoupper(trans('text.courseware')) }}
								</a>
							</li>
						</ul>
						<script>
							$('#content_tabs a').click(function (e) {
							  	e.preventDefault()
							  	$(this).tab('show')
							})
						</script>
					</div>
                    
                    <!-- right -->
                    <div class="col-xs-2">
                        <!-- manage button -->
                        @include('home_manage_button')
                    </div>

                </div>


				<div class="row tab-content">
					<!-- COURSE INFO -->
					<div role="tabpanel" class="tab-pane active" id="course_info">
						<!-- title -->
		                <div class="row" style="margin-bottom: 10px;">
		                	<div class="col-xs-12">
		                		<img src="/images/resource/course.png" class="table_view_image"> <span class="pill bck-course">{{ $course['name'] }}</span>
		                	</div>
		                </div>

		                <!-- info -->
		                <div class="row">
		                	<div class="col-xs-4">
		                		<!-- image --> 
		                		@if (isset($course['image']))
		                			<span class="title">
		                				{!! strtoupper(trans('text.cover_image')) !!}
		                			</span>
		                			<img src='{!! $course["image"] !!}' class="image" style="margin-bottom: 10px;">
		                		@endif

		                		<!-- <!-- description -->
		                		<div class="title">
		                			{{ strtoupper(trans('text.description')) }}
		                		</div>

		                		<table style="width:100%;">
		                			<tr>
		                				<td>
		                					<!-- school -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.school')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
											{{ echo_index($course, 'school') }}
		                				</td>
		                			</tr>
		                			<tr>
		                				<td>
		                					<!-- year -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.year')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ echo_index($course, 'year') }}
		                				</td>
									</tr>
									<tr>
		                				<td>
		                					<!-- country -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.country')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ isset($course['country']['name']) ? $course['country']['name'] : "" }}
		                				</td>
		                			</tr>
									<tr>
		                				<td>
		                					<!-- type -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.type')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ echo_index($course, 'type') }}
		                				</td>
		                			</tr>
		                			<tr>
		                				<td>
		                					<!-- language -->
		                					<span class="title">
		                						{{ strtoupper(trans('text.language')) }}
		                					</span>
		                				</td>
		                				<td class="text-right">
		                					{{ isset($course['language']['name']) ? $course['language']['name'] : "" }}
		                				</td>
		                			</tr>
		                		</table>	                		
		                	</div>

		                	<div class="col-xs-6">
		                		<!-- syllabus -->
		                		<div class="title">
		                			{{ strtoupper(trans('text.syllabus')) }}
		                		</div>
		                		{{ echo_index($course, 'description') }}
		                	</div>
		                	<div class="col-xs-2">
		                		<!-- teachers -->
		                		<div class="title">
		                			{{ strtoupper(trans('text.authors')) }}
		                		</div>
		                		@if(isset($course['teachers']))
									@foreach($course['teachers'] as $teacher)
										<div class="text-course title remove_teacher">
											<img src="{!! user_image($teacher['id']) !!} " class="image">
											{{ echo_index($teacher, 'name') }} {{ echo_index($teacher, 'last_name') }}
										</div>
									@endforeach
								@endif
		                	</div>
		                </div>
					</div>

					<div role="tabpanel" class="tab-pane" id="course_courseware">
					</div>
					
				</div>

			</div>

			<!-- MENU RIGHT -->
            <div class="col-xs-2">
                @include('right_menu_filter', ['home_page' => 1])
            </div>
            
		</div>
	</div>


<script type="text/javascript">
	
	$( document ).ready(function() {

		// load the courseware grid view by default
		courseware_view('grid');

		// Grid view loader
		$('body').on('click', '#grid_view_selector', function(){
			// load the courseware grid view
			courseware_view('grid');
		});

		// Grid view loader
		$('body').on('click', '#list_view_selector', function(){
			// load the courseware list view
			courseware_view('list');
		});

	});


	function courseware_view(view_type, obj) {
	
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.get({
			url: '{!! $server_url !!}/api/courses/courseware/view/' + view_type + '/{!! $course['id'] !!}/{!! $course['server_id'] !!}',
			data: {
				'_token': token,
		      },
			success: function(data, status) {
				$("#course_courseware").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		})		
	}

</script>

@endsection