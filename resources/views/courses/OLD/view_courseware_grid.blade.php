<!-- courseware -->
<div class="row">
	<div class="col-xs-12 text-center">
		<i id="grid_view_selector" class="fa fa-th-large resource_view_selector  text-course" data-id="{!! $course['id'] !!}" aria-hidden="true"></i>
		<i id="list_view_selector" class="fa fa-bars resource_view_selector"  data-id="{!! $course['id'] !!}"  aria-hidden="true"></i>
	</div>
</div>

<!-- COURSE TITLE -->
<div class="row" style="margin-bottom: 10px;">
	<div class="col-xs-12">
		<img src="/images/resource/course.png" class="table_view_image"> <span class="pill bck-course">{{ $course['name'] }}</span>
	</div>
</div>

<!-- COURSEWARE -->
<div class="row">

		<!-- SUBJECTS -->
		@if (isset($course['subjects_published']))
			<!-- if no published subjects -->
			@if (count($course['subjects_published']) == 0)
				{{ trans('text.no_subjects') }}
			@endif

			@foreach ($course['subjects_published'] as $subject)

				<div class="panel-group " id="accordion{!! $loop->index !!}">
					<div class="panel panel-default in">
						<!-- subject heading -->
						<a data-toggle="collapse" data-parent="#accordion{!! $loop->index !!}" href="#collapse{!! $loop->index !!}">
							<div class="panel-heading">
								{{ $subject['name'] }}
							</div>
						</a>
						
						<!-- LECTURES -->
						<div id="collapse{!! $loop->index !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $loop->index !!}">
					        <div class="panel-body" style="background-color: #f2f2f2; padding: 0px;">
				        		<div class="row">
				        			<div class="col-xs-12">
				        			
				      					@if (isset($subject['lectures_published']))
				      						
				      						<!-- if no published lectures -->
											@if (count($subject['lectures_published']) == 0)
											
												{{ trans('text.no_lectures') }}
											
											@else 

											<!-- if published lectures -->
												<ul class="sortable_ul ui-sortable">

										      		@foreach ($subject['lectures_published'] as $lecture)

										      			<li class="col-xs-3 lecture_grid_li" style="margin-top: 0px;">

										      				<div class="lecture_grid_image"
																@if ($lecture['image'])
																	style="background-image: url('{{ $lecture['image'] }}');" ></div>
																@else 
																	>
																	<img src="/images/resource/lecture_big.png"/></div>
																@endif


															<div class="row">
																<div class="col-xs-12 lecture_grid">

																	<!-- TEXT DIV -->
																	<div class="lecture_grid_text">
																		<div class="row">
																			<!-- lecture name -->
																			<div class="col-xs-8">
																				{{ $lecture['name'] }}
																			</div>
																			<!-- lecture icon -->
																			<div class="col-xs-4 text-right">
																				<span class="btn-course-small pull-left">&nbsp;</span>
																			</div>
																		</div>
																		<!-- platform icon -->
																		<div style="position: absolute; right: 25px; bottom: 10px;">
																			<div class="platform_icon" style="background-image: url({!! url("/images/server/logo.png") !!}); "></div>
																		</div>
																	</div>

																	<!-- HOVER DIV -->
																	<div class="row lecture_bottom_cover" style="color: white;">
																			
												      					<!-- RESOURCES -->
												      					@if (isset($lecture['resources_published']))
												      					
												      						<div class="col-xs-12" style="margin-top:40px;">
												      							<div class="row">
													      							
														      						<!-- if no published resources -->
																					@if (count($lecture['resources_published']) == 0)
																						{{ trans('text.no_resources') }}
																					@else 
																				
																						<?php $resource_icons = []; ?>

															      						@foreach ($lecture['resources_published'] as $resource)
															      							
															      							<!-- resource type icon -->
															      							@if ( !in_array($resource['resource_type']['name'], $resource_icons) )
																      							<?php 
																      								// add to array
																      								array_push($resource_icons, $resource['resource_type']['name']);

																      								if (!server_property('central_server')){
																      									$server_url = url('/');
																      								} else {
																      									$server_url = server_url($course['server_id']);
																      								}
																      								
																      							?>
																      							<div class="col-xs-3">
																      								<div class="round_button bck-course modal_url" style="margin-left: auto; margin-right: auto;" data-url="/resources/view_modal/{!! $resource['id'] !!}/{!! $lecture['id'] !!}?server_url={!! $server_url !!}" data-data="">{!! $resource['resource_type']['icon'] !!}</div>
																      							</div>
															      							@endif

															      						@endforeach

															      						<div class="col-xs-3">
															      							<div class="round_button modal_url text-course text-center" style="margin-left: auto; margin-right: auto; background-color: white;" data-url="/lectures/view_modal_download/{!! $lecture['id'] !!}?server_url={!! $server_url !!}">
															      								<i class="fa fa-download" aria-hidden="true"></i>
															      							</div>
															      						</div>
															      						
															      					@endif
													      						</div>
																			</div>

												      					@else 
												      						{{ trans('text.no_resources') }}
												      					@endif

																	</div>
																</div>
															</div>															

										      			</li>
										      		@endforeach

										      	</ul>

											@endif 

										@else 
								      		{{ trans('text.no_lectures') }}					      		
								      	@endif 
							      	</div>
							    </div>
		      				</div>
		    			</div>
		    		</div>
		    	</div>

			@endforeach 
		
		@else 
			{{ trans('text.no_subjects') }}
		@endif

	</div>
</div>