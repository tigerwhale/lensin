<?php 
	$resource_count = count($resources);
?>

@foreach($resources as $resource)

	<?php 
		// files for this resource
		if (isset($resource->path)) {
			$resources_files = json_decode($resource->path);
		}

		$hidden_resource = "block";
		$arrows_hidden = "hidden";
		// is there one or more 
		
		$previous_modal = 0;
		$next_modal = 0;

		if ( $resource_count > 1 ) {

			// is modal hidden or not
			if (!$loop->first) {
				$hidden_resource = "none";
			} 

			// unhide the arrows
			$arrows_hidden = "";

			$previous_modal = ($loop->index == 0) ? $resource_count - 1 : $loop->index - 1;
			$next_modal = ($loop->index + 1 == $resource_count) ? 0 : $loop->index + 1;
			
		}


		
	?>
	<div class="modal-dialog modal-lg" style="display:{!! $hidden_resource !!}" id="modal-{!! $loop->index !!}">

		<!-- Modal content-->
		<div class="modal-content" style="font-size: 13pt;">
			<div class="modal-body">

				<!-- header -->
				<div class="row" style="margin-bottom: 20px;">
					<!-- resource icon -->
					<div class="col-xs-1 col-sm-offset-1 text-resource">
						{!! ($resource->resource_type) ? $resource->resource_type->icon : "" !!}
					</div>
					<!-- resource name -->
					<div class="col-xs-8 text-center resource_text text-lecture">
						<div class="round_button resouce-button bck-resource" style="display: inline-block;">&nbsp;</div> {{ $resource->resourcable->name }}
					</div>
					<!-- close button-->
					<div class="col-xs-2 text-right">
						<button type="button" class="btn round_button text-course" style="background-color: #f2f2f2" data-dismiss="modal">X</button>
					</div>
				</div>

				<!-- content -->
				<div class="row">
					<!-- left -->
					<div class="col-xs-1">
						<div class="{!! $arrows_hidden !!} round_button arrow_modal text-course" data-this-modal-id="{!! $loop->index !!}" data-open-modal-id="{!! $previous_modal !!}"><strong><i class="fa fa-angle-left" aria-hidden="true"></i></strong></div>
					</div>
					<!-- content -->
					<div class="col-xs-4">
						<table style="width: 100%">
							<!-- name -->
							<tr>
								<td class="text-lecture">{{ trans('text.name') }}</td>
								<td>{{ $resource->name }}</td>
							</tr>
							<!-- author -->
							<tr>
								<td class="text-lecture">{{ trans('text.author') }}</td>
								<td>{{ $resource->author }}</td>
							</tr>
							<!-- year -->
							<tr>
								<td class="text-lecture">{{ trans('text.year') }}</td>
								<td>{{ $resource->year }}</td>
							</tr>
							<!-- licence -->
							<tr>
								<td class="text-lecture">{{ trans('text.licence') }}</td>
								<td>
									@if ($resource->licences)
										@foreach($resource->licences as $licence)
											<img src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
										@endforeach
									@endif
								</td>
							</tr>
							<!-- length -->
							<tr>
								<td class="text-lecture">{{ trans('text.length') }}</td>
								<td>{{ $resource->length }}</td>
							</tr>
						</table>
					</div>
					<!-- file preview -->
					<div class="col-xs-6">
						@foreach ($resources_files as $resources_file)
							<div class="row">
								<div class="col-xs-12">
									
									<!-- VIDEO --> 
									@if ($resource->resource_type->name == "video")
										<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
										    <source src="{{ $resources_file }}?{{ time() }}" type='video/mp4'>
										    <p class="vjs-no-js">
												To view this video please enable JavaScript, and consider upgrading to a web browser that
												<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
										    </p>
										</video>
									
									<!-- IMAGE -->
									@elseif($resource->resource_type->name == "image")
										<img src="{{ $resources_file }}?{{ time() }}" style="max-width: 100%;">
									@endif

									<!-- file name and download -->
									<div class="no-preview">
										<div class="row">
											<div class="col-xs-10" style="padding-top: 10px">
												{{ basename($resources_file) }} 
											</div>
											<div class="col-xs-2">
												<div class="round_button text-course text-center" style="margin-left: auto; margin-right: auto; background-color: white;" >
													<a href="{{ url($resources_file) }}" download ><i class="fa fa-download" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>

									</div>
								 

								</div>
							</div>
						@endforeach
					</div>
					<!-- right -->
					<div class="col-xs-1">
						<div class="{!! $arrows_hidden !!} round_button arrow_modal text-course" data-this-modal-id="{!! $loop->index !!}" data-open-modal-id="{!! $next_modal !!}"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					</div>
				</div>

				<!-- download footer -->
				<div class="row">
					<div class="col-xs-5 text-center">
					</div>
					<div class="col-xs-1 text-center">
						<div class="round_button modal_url text-course text-center" title="{{ trans('resources.download_all') }}" style="margin-left: auto; margin-right: auto; background-color: white;" data-url="/lectures/view_modal_download/{!! $lecture_id !!}?server_url={!! $server_url !!}">
							<i class="fa fa-download" aria-hidden="true"></i>
						</div>
					</div>
					<!-- if there is a preview -->
					@if ($resource->preview)
						<div class="col-xs-1 text-center">
							<div class="round_button text-course text-center" style="margin-left: auto; margin-right: auto; background-color: white;">
								<a href="{{ url($resource->preview) }}" title="{{ trans('resources.preview') }}"><i class="fa fa-film" aria-hidden="true"></i></a>
							</div>
						</div>
					@endif
				</div>

			</div>
		</div>
		

	</div>

@endforeach

<script type="text/javascript">
	
	$('body').on('click', '.arrow_modal', function(){

		$('#modal-' + $(this).data('this-modal-id') ).slideToggle("fast");
		$('#modal-' + $(this).data('open-modal-id') ).slideToggle("fast");

	});



</script>