<div id="author_{{ $user->id }}" class="remove_teacher">
	
	<!-- author image -->
	<div style="background-image: url('{!! user_image($user->id) !!}')" class="course_author_image">
		<div class="bottom_cover bck-course">
			<!-- remove author button -->
			<button class="btn btn-danger remove_author_button" data-id="{!! $user->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
		</div>
	</div>
	<!-- author name -->
	<div class="text-course title">
		{{ $user->name }} {{ $user->last_name }}
	</div>
	
</div>