@extends('layouts.resource')

@section('content')


<!-- CREATE MODE -->
@if (isset($mode)) 
	<div class="row bck-course resource_header">
		<!-- left text -->
		<div class="col-xs-4 text-left resource_header_title">
			<table>
				<tr>
					<td style="height: 70px; vertical-align: middle; font-size: 38pt">
						<p style="line-height: 38pt; margin-bottom: 6px;">+</p>
					</td>
					<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
						{{ strtoupper(trans('text.create_mode')) }}
					</td>
				</tr>
			</table>
			
		</div>

		<!-- middle buttons -->
		<div class="col-xs-4">
			<table style="margin-left: auto; margin-right: auto;">
				<tr>
					<!-- back -->
						<td style="padding: 5px;">
							<a href="/courses/modify/">
								<button class="btn btn-danger" type="button"><i class="fas fa-arrow-left" aria-hidden="true"></i></button>
							</a>
							<br>
							{{ strtoupper(trans('text.back')) }}
						</td>
					<!-- confirm -->
						<td style="padding: 5px;">
							<a href="/courses/edit/{!! $course->id !!}">
							<button class="btn btn-success" type="button"><i class="fa fa-check" aria-hidden="true"></i></button>
							</a>
							<br>
							{{ strtoupper(trans('text.confirm')) }}
						</td>
					<!-- cancel -->
						<td style="padding: 5px;">
							<a href="/courses/delete/{!! $course->id !!}?cancel=1">
							<button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							</a>
							<br>
							{{ strtoupper(trans('text.cancel')) }}
						</td>
				</tr>
			</table>			
		</div>
		
		<!-- right text -->
		<div class="col-xs-4 text-right resource_header_title">
			<table style="float: right;">
				<tr>
					<td style="vertical-align: middle; height: 70px;">
						{{ strtoupper(trans('text.courses')) }}
					</td>
					<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
						<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>							
					</td>
				</tr>
			</table>
			
		</div>			
	</div>


@else 
<!-- MODIFY MODE -->

	<div class="row bck-course resource_header">
		<!-- left text -->
		<div class="col-xs-4 text-left resource_header_title">
			<table>
				<tr>
					<td style="height: 70px; vertical-align: middle; font-size: 38pt">
						<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fas fa-pencil-alt" aria-hidden="true"></i></p>
					</td>
					<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
						{{ strtoupper(trans('text.modify_mode')) }}
					</td>
				</tr>
			</table>
			
		</div>

		<!-- middle buttons -->
		<div class="col-xs-4">
			<table style="margin-left: auto; margin-right: auto;">
				<tr>
					<td style="padding: 5px;">
						<a href="{!! url('/courses/modify') !!}">
							<button class="btn btn-danger" type="button"><i class="fas fa-arrow-left" aria-hidden="true"></i></button>
						</a>
						<br>
						{{ strtoupper(trans('text.back')) }}
					</td>
				</tr>
			</table>			
		</div>
		
		<!-- right text -->
		<div class="col-xs-4 text-right resource_header_title">
			<table style="float: right;">
				<tr>
					<td style="vertical-align: middle; height: 70px;">
						{{ strtoupper(trans('text.courses')) }}
					</td>
					<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
						<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>							
					</td>
				</tr>
			</table>
			
		</div>			
	</div>

@endif



<div class="container">
	<!-- HEADER BUTTONS -->
	<div class="row">
		<div class="col-xs-12 text-center">
			<ul id="content_tabs">
				<li class="btn nav-course active"><a data-toggle="tab" href="#course_info">{{ strtoupper(trans('text.info')) }}</a></li>
			    <li class="btn nav-course"><a data-toggle="tab" href="#course_courseware">{{ strtoupper(trans('text.courseware')) }}</a></li>
			</ul>
			<script>
				$('#content_tabs a').click(function (e) {
				  	e.preventDefault()
				  	$(this).tab('show')
				})
			</script>		
		</div>
	</div>
	
	<hr class="resource_hr">

	<!-- COURSE TITLE  -->
	<div class="row" style="margin-top: 20px">
		<div class="col-xs-2">
			<img src="/images/resource/course.png" class="table_view_image"> {{ strtoupper(trans('text.resource_title')) }}
		</div>
		<div class="col-xs-9">
			<input type="text" name="name" data-model='courses' data-id={{ $course->id }} value="{{ $course->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
		</div>
	</div>


	<div class="row tab-content">
	
		<!-- COURSE INFO -->
		<div role="tabpanel" class="tab-pane active" id="course_info">

			<div class="row" style="margin-top: 20px;">
				
				<div class="col-xs-4 text-center">

					<!-- COVER IMAGE -->
					{{ strtoupper(trans('text.cover_image')) }}
					<div class="image_container" id='image_container'></div>
					
					<div class="dropzeon dropzone_div" id="upload_cover_image">
						{{ trans('text.click_to_select_image') }}
					</div>


					<!-- SCHOOL -->
					{{ trans('text.school') }}
					<input list="schools" type="text" id="school" name="school" data-model='courses' data-id={{ $course->id }} value="{{ $course->school }}" class="form-control update_input" required>
					<datalist id="schools">
						@foreach (list_schools() as $school) 
							<option value="{{ $school->school }}">
						@endforeach
					</datalist>

					<!-- YEAR -->
					{{ trans('text.year') }}
					<input list="years" type="text" id="year" name="year" data-model='courses' data-id={{ $course->id }} value="{{ $course->year }}" class="form-control update_input" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year) 
							<option value="{{ $year }}">
						@endforeach
					</datalist>

					<!-- COUNTRY -->
					{{ trans('text.country') }}
					{!! Form::select('country_id', $countries, $course->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'courses', 'data-id'=> "$course->id" ]) !!}

					<!-- COUNTRY -->
					{{ trans('text.language') }}
					{!! Form::select('language_id', $languages, $course->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'courses', 'data-id'=> "$course->id" ]) !!}

					<!-- TYPE -->
					{{ trans('text.type') }}
					<input type="text" id="type" name="type" data-model='courses' data-id={{ $course->id }} value="{{ $course->type }}" class="form-control update_input" required>
					<datalist id="types">
						@foreach (DB::table('courses')->select('type')->distinct()->get() as $type) 
							<option value="{{ $type->type }}" />
						@endforeach
					</datalist>				
				</div>

				<!-- SYLLABUS -->
				<div class="col-xs-5 text-center">
					{{ strtoupper(trans('text.syllabus')) }}<br>
					<textarea name="description" class="form-control update_input"  data-model='courses' data-id={{ $course->id }} placeholder="{{ trans('text.insert_description') }}" style="height: 300px;">{{ $course->description }}</textarea>
				</div>

				<!-- AUTHORS -->
				<div class="col-xs-3 text-center">
					{{ strtoupper(trans('text.authors')) }}
					<br>
					<!-- add author button -->
					<button type="button" title="{{ trans('course.add_teacher') }}" class="btn bck-course" data-toggle="modal" data-target="#authorsModal"><i class="fa fa-plus" aria-hidden="true"></i></button>
					
					<div class="text-center" style="margin-top: 10px;">
						<!-- list of authors -->
						<div id="authors_list"></div>
					</div>

				</div>
			</div>
		</div>

		<!-- COURSEWARE -->
		<div role="tabpanel" style="margin-top: 10px;" class="tab-pane" id="course_courseware">

			<!-- VIEW TYPE SELECTOR --> 
			<div class="row">
				<div class="col-xs-12 text-center">
					<i id="grid_view_selector" class="fa fa-th-large resource_view_selector" aria-hidden="true"></i>
					<i id="list_view_selector" class="fa fa-bars resource_view_selector" aria-hidden="true"></i>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12" id="courseware_data">
				</div>
			</div>

			<div class="row" id="wait">
				<div class="col-xs-12">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
				</div>
			</div>	
			
		</div>				

	</div>
</div>


<!-- Authors Modal -->
<div id="authorsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
			</div>
			<div class="modal-body">
				<table class="display" id="datatable" style="width: 100%">
					<thead>
						<th>{{ trans('text.name') }}</th>
						<th>{{ trans('text.last_name') }}</th>
						<th>{{ trans('text.school') }}</th>
						<th></th>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>


<!-- Resource Modal -->
<div id="resourceModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ strtoupper(trans('text.add_resource_information')) }}</h4>
			</div>
			<div class="modal-body" id="resource_modal_div">
			</div>
		</div>

	</div>
</div>


<!-- Resource single/multiple file question -->
<div id="dialog-confirm" title="{{ trans('text.what_type_of_file_are_you_uploading') }}">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span></p>
</div>


<script type="text/javascript">

$( document ).ready(function() {

	loadCourseImageForEdit(); 
	loadCourseAuthorsForEdit();


	function loadCourseAuthorsForEdit() {

		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/courses/load_course_authors_for_edit/{{ $course->id }}',
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data) {
				$("#authors_list").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}

	// DROPZONE - upload project image
	$("#upload_cover_image").dropzone({ 

		url: "/courses/upload_cover_image/{{ $course->id }}",
		paramName: "image",
		previewsContainer: false,
		uploadMultiple: false,
		parallelUploads: 1,
		acceptedFiles: "image/*",
				
		sending: function(file, xhr, formData) {
			formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		},

		uploadprogress: function(file, progress, bytesSent) {
			// upload progress percentage
		    $(".image_container").html(parseInt(progress) + "%");
		},

		success: function(file, responce) {

			// change image
			if (responce == "OK") {
				loadCourseImageForEdit({{ $course->id }});
			} else {
				alert("{{ trans('text.upload_error') }}");
			}
		}
	});



	// REMOVE COURSE COVER IMAGE
	$('body').on('click', '.remove_course_image_button', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var course_id = $(this).data('id');
		var button = $(this);

		$.ajax({
			url: '/courses/delete_cover_image/' + course_id,
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data, status) {
				loadCourseImageForEdit();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	})

	// LIST VIEW SELECTOR
	function loadCourseImageForEdit() 
	{
		var token = $("meta[name='csrf-token']").attr("content"); 
		$.ajax({
			url: '/courses/load_cover_image_for_edit/{!! $course->id !!}',
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data) {
				$('#image_container').html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	};




	// LIST VIEW SELECTOR
	$('body').on('click', '#list_view_selector', function(){
		var token = $("meta[name='csrf-token']").attr("content"); 
		display_style = "list";

		$.ajax({
			url: '/courses/edit/view/list/{!! $course->id !!}',
			type: 'POST',
			data: {
				'_token': token,
		      },
		    beforeSend: function() { $('#wait').show(); $("#courseware_data").html("");},
			success: function(data) {
				$('#wait').hide();
				$("#courseware_data").html(data);
				$("#list_view_selector").addClass('text-course');
				$("#grid_view_selector").removeClass('text-course');

			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});

	// GRID VIEW SELECTOR
	$('body').on('click', '#grid_view_selector', function(){
		var token = $("meta[name='csrf-token']").attr("content"); 
		display_style = "grid";

		$.ajax({
			url: '/courses/edit/view/grid/{!! $course->id !!}',
			type: 'POST',
			data: {
				'_token': token,
		      },
		    beforeSend: function() { $('#wait').show(); $("#courseware_data").html("");},
			success: function(data) {
				$('#wait').hide();
				$("#courseware_data").html(data);
				$("#list_view_selector").removeClass('text-course');
				$("#grid_view_selector").addClass('text-course');

			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});

	// GRID VIEW DEFAUT
	$("#grid_view_selector").trigger('click');


	// RESOURCE MODAL CLOSE
	$('body').on('hidden.bs.modal', '#resourceModal', function(){	
		var resource_id = $('#resource_modal_id').val();
		$('#resource_' + resource_id + '_name').html( $('#resource_modal_name').val() );
		$('#resource_' + resource_id + '_author').html( $('#resource_modal_author').val() );
		$('#resource_' + resource_id + '_year').html( $('#resource_modal_year').val() );

		var resource_type_checkbox = $('.resource_type:checked:first');
		var label = $("label[for='"+ resource_type_checkbox.attr('id') + "']");
		$('#resource_' + resource_id + '_file_type').html( $(label).html() );
	});

	
	// SORT COURSES
	sort_courses();

	
	// ADD SUBJECT
    $('body').on('click', '#add_subject', function(){
		$.ajax({
			url: '/course_subjects/create/{!! $course->id !!}' ,
			type: 'GET',
			success: function(data, status) {
				$('#sortable_courseware').append(data);
				// Add lecture
				addLecture($("#sortable_courseware li").last().data('subject-id'));
				sort_courses();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});


    // ADD A LECTURE TO COURSE SUBJECT
    $('body').on('click', '.add_lecture', function() {
    	var course_subject_id = $(this).data('course-subject-id');
		addLecture(course_subject_id);
	});

    function addLecture(_course_subject_id) {

		$.ajax({
			url: '/lectures/create/' + _course_subject_id ,
			type: 'GET',
			success: function(data, status) {
				$('#sortable_lectures_' + _course_subject_id).append(data);
				window["sort_course_subject_" + _course_subject_id]();

			},
			error: function(xhr, desc, err) {
				bootbox.alert (desc);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
    }



	// EDIT RESOURCE
	$('body').on('click', '.edit_resource', function(){	
		// console.log($(this).data('id'));
		var resource_id = $(this).data('id');
		edit_resource(resource_id);
	});


	// DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache": true,
            "url": '/users/list',
            "rowId": 'id',
        },
        "columns": [
            { "data": "name" },
            { "data": "last_name" },
            { "data": "school" },
            { "data": "id" },
        ],
		"aoColumnDefs": [
        {
            "mRender": function ( data, type, row ) {
                return "<button class='btn btn-success' type='button' id='add_teacher'><i class='fa fa-plus' aria-hidden='true'></i></button>";
            },
            "aTargets": [ 3 ]
        },
     	]
    }); 


    // ADD TEACHERS
	$('#datatable tbody').on( 'click', '#add_teacher', function () {

		// token
		var token = $("meta[name='csrf-token']").attr("content"); 

		var row = table.row( $(this).parents('tr') );
		var author_id = row.data()['id'];

		$.ajax({
			url: '/courses/add_teacher',
			type: 'POST',
			data: {
				'_token': token,
				'course_id': {{ $course->id }},
				'author_id': author_id,
		      },

			success: function(data, status) {
				$("#authors_list").append(data);
				  row.remove().draw();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});


	// REMOVE TEACHER
	$('body').on('click', '.remove_author_button', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var author_id = $(this).data('id');
		var button = $(this);

		$.ajax({
			url: '/courses/remove_teacher',
			type: 'POST',
			data: {
				'_token': token,
				'course_id': {{ $course->id }},
				'author_id': author_id,
		      },

			success: function(data, status) {
				$("#author_" + author_id).remove();
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	})


	// ADD LECTURE GRID VIEW
	$('body').on('click', '.add_lecture_grid', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var course_subject_id = $(this).data('course-subject-id');

		$.ajax({
			url: '/lectures/create/' + course_subject_id,
			type: 'GET',
			data: {
				'_token': token,
				'grid': '1',
		    },

			success: function(data, status) {
				$("#sortable_lectures_" + course_subject_id).find(' > li:nth-last-child(1)').before(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	})


	// TABS
	$('#content_tabs a').click(function (e) {
	  	e.preventDefault()
	  	$(this).tab('show')
	})				


	
	$('body').on('mouseover', '.lecture_grid_text', function(){
		
	});


	// DELETE RESUORCE FILE
	$('body').on('click', '.delete_resource_file', function(){
		var token = $("meta[name='csrf-token']").attr("content"); 
		var resource_id = $(this).data('resource-id');
		var file_index = $(this).data('file-index');

		$.ajax({
			url: '/courses/resource/delete_file/' + resource_id,
			type: 'POST',
			data: {
				'_token': token,
				'file_index': file_index
		      },
			success: function(data, status) {
				$("#filename").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});



	// ADD LECTURE CONTENT
	$('body').on('click', '.add_lecture_content', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var lecture_id = $(this).data('lecture-id');

		$.post({
			url: '/lectures/content/create/' + lecture_id,
				data: {
					'_token': token,
					'lecture_id': lecture_id,
				},
				success: function(data, status) {
					lecture_content_load(lecture_id);
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
		})

	});


	// DELETE LECTURE CONTENT
	$('body').on('click', '.delete_lecture_content', function(){

		var token = $("meta[name='csrf-token']").attr("content"); 
		var content_id = $(this).data('content-id');
		var lecture_id = $(this).data('lecture-id');

		// ASK THE QUESTION 
		bootbox.confirm({
		    message: "{{ trans('courses.delete_content_question') }}",
		    buttons: {
		        confirm: {
		            label: '{{ trans('text.delete') }}',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: '{{ trans('text.cancel') }}',
		            className: 'btn-danger'
		        }
		    },
		    callback: function (result) {
		        if (result) {

		        	// If OK, delete the content
		        	$.post({
						url: '/lectures/content/delete/' + content_id,
							data: {
								'_token': token,
							},
							success: function(data, status) {
								lecture_content_load(lecture_id);
							},
							error: function(xhr, desc, err) {
								console.log(xhr);
								console.log("Details: " + desc + "\nError:" + err);
							}
					});
		        }
		    }
		});


	});

});

	// EDIT RESOURCE MODAL
	function edit_resource(resource_id) {
		$.ajax({
			url: '/resources/edit_modal/' + resource_id,
			type: 'GET',
			success: function(data, status) {
				$('#resource_modal_div').html(data);
				$('#resourceModal').modal('show');
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}

	function sort_courses() {
		$( "#sortable_courseware" ).sortable({
			
			stop: function( event, ui ) {
				// get the token
				var token = $("meta[name='csrf-token']").attr("content"); 

				// make subject id-s		
				var subjects = [];

				$(this).children('li').each(function(i){
					//alert ($(this).data('subject-id'));
					subjects.push($(this).data('subject-id'));

				});

				// send to server
				$.post({
					url: '/course_subjects/update_sort',
					data: {
						'_token': token,
						'subjects': subjects,
					},
					success: function(data, status) {
						//alert(data);
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
		  	}
		});
	}

	// LOAD LECTURE CONTENT
	function lecture_content_load(lecture_id) {
		
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.post({
			url: '/lectures/content/load_edit/' + lecture_id,
				data: {
					'_token': token,
				},
				success: function(data, status) {
					$('body #lecture_' + lecture_id + '_content').html(data);
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
		})
	}

	// LOAD LECTURE RESOURCES
	function  lecture_resources_load(lecture_id){
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.post({
			url: '/lectures/content/load_resources_list/' + lecture_id,
			method: "POST",
			data: {
				'_token': token,
			},
			success: function(data, status) {
				$('body #lecture_' + lecture_id + '_resources').html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		})
	}

	// Create a new resource
	$('body').on('click', '.add_resource', function(){
		
		var lecture_id = $(this).data('lecture-id');
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			method:"POST",
			url:"/resources/create/" + lecture_id,
			success: function(responce){
				openModalFromUrl('/resources/edit_modal/' + responce['id']);
				lecture_resources_load(lecture_id);
			},
			data: {
				'_token': token,
		    },
		});
	});

	// check all resources
	$('body').on('click', '.check_all_resources', function(){
	    $('input:checkbox.resource_checkbox_' + $(this).data('lecture-id') ).not(this).prop('checked', this.checked);
	});

	// publish all selected resources 
	$('body').on('click', '.publish_selected_resources', function(){
	    publish_selected_resources(1);
	});

	// unpublish all selected resources 
	$('body').on('click', '.unpublish_selected_resources', function(){
	    publish_selected_resources(0);
	});

	
	function publish_selected_resources(publish_state) 
	{
		var token = $("meta[name='csrf-token']").attr("content"); 
		var resources = [];

		$.each($(".resource_checkbox:checked"), function() {
		  resources.push( $(this).data('id') );
		});

		$.ajax({
			method: "POST",
			url:"/resources/publish_multiple",
			data: {
				'_token': token,
				'resources': resources,
				'publish_state': publish_state
		    },
			success: function(responce){
				if (display_style == "grid") {
					lecture_resources_load(current_lecture_id);	
				} else {
					responce.forEach( function(element, index, array) {
						lecture_resources_load(element);
					});
					
				}
				
			},
			
		});		
	}


	// delete all selected resources 
	$('body').on('click', '.delete_selected_resources', function(){
	    delete_selected_resources();
	});

	
	function delete_selected_resources() 
	{
		var text = "{{ trans('resources.delete_multiple_question') }}";
		
        bootbox.confirm(text, function(result){ 		
		
			var resources = [];
        	var token = $("meta[name='csrf-token']").attr("content"); 

			$.each($(".resource_checkbox:checked"), function() {
			  resources.push( $(this).data('id') );
			});

			$.ajax({
				method: "POST",
				url:"/resources/delete_multiple",
				data: {
					'_token': token,
					'resources': resources
			    },
				success: function(responce){
					if (display_style == "grid") {
						lecture_resources_load(current_lecture_id);	
					} else {
						responce.forEach( function(element, index, array) {
							lecture_resources_load(element);
						});
						
					}
				},
				
			});
		});
	}

</script>

@endsection


