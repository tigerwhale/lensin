@extends('layouts.resource')

@section('content')


<div class="row bck-course resource_header">
    <!-- left text -->
    <div class="col-xs-4 text-left resource_header_title">
        <table>
            <tr>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fas fa-pencil-alt" aria-hidden="true"></i></p>
                </td>
                <td style="vertical-align: middle; height: 70px; padding-left: 10px;">
                    {{ strtoupper(trans('text.modify_mode')) }}
                </td>
            </tr>
        </table>
        
    </div>

    <!-- middle buttons -->
    <div class="col-xs-4">
        <table style="margin-left: auto; margin-right: auto;">
            <tr>
                <td style="padding: 5px;">
                    <a href="{!! url('/') !!}"><button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></a>
                    <br>
                    {{ strtoupper(trans('text.close')) }}
                </td>
            </tr>
        </table>            
    </div>
    
    <!-- right text -->
    <div class="col-xs-4 text-right resource_header_title">
        <table style="float: right;">
            <tr>
                <td style="vertical-align: middle; height: 70px;">
                    {{ strtoupper(trans('text.courses')) }}
                </td>
                <td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
                    <p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>                           
                </td>
            </tr>
        </table>
        
    </div>          
</div>



<div class="container">
    <div class="row text-center">
        <!-- CREATE COURSE -->
        <a href="/courses/create/">
            <button type="button" class="btn bck-course">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </a>
    </div>
	<div class="row">
		<div class="col-xs-12 text-center">		
	        <!-- DATATABLE SEARCH -->
		    <table class="display" id="datatable">
		        <thead>
		            <tr>
		                <th></th>
		                <th>{{ trans('text.title') }}</th>
		                <th>{{ trans('text.authors') }}</th>
		                <th>{{ trans('text.country') }}</th>
		                <th>{{ trans('text.year') }}</th>
		                <th>{{ trans('text.language') }}</th>
		                <th></th>
		            </tr>
		        </thead>
		    </table>
		</div>
	</div>
</div>



<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/courses/datatable_list',
            rowId: 'id',
        },
		
		"aoColumnDefs": [
        {
            // `data` refers to the data for the cell (defined by `mData`, which
            // defaults to the column being worked with, in this case is the first
            // Using `row[0]` is equivalent.
            "mRender": function ( data, type, row ) {
                return "<div class='text-center' style='width:100%'><img src='/images/resource/" + row[6] + ".png' class='table_view_image'><br><img src='/images/platform/" + row[7] + ".png' class='table_view_image platform'></div>";
            },
            "aTargets": [ 0 ], 

            "mRender": function ( data, type, row ) {

            	var modify = '<a title="{{ trans('text.edit_course') }}" href="/courses/edit/' + row[0] + '"><div class="btn bck-course"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';

            	if (row[9] == 1) {
            		// if published
            		var publish = '<a title="{{ trans('text.tooltip_unpublish_course') }}" href="/courses/unpublish/' + row[0] + '"><div class="btn bck-course"><i class="far fa-eye" aria-hidden="true"></i></div></a> ';
            	} else {
            		// if unpublished
            		var publish = '<a title="{{ trans('text.tooltip_publish_course') }}" href="/courses/publish/' + row[0] + '"><div class="btn bck-course-unpublished"><i class="far fa-eye-slash" aria-hidden="true"></i></div></a> ';
            	}

            	var delete_button = '<div class="btn btn-danger delete_course" title="{{ trans('text.tooltip_delete_course') }}" data-course-id="' +  row[0] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

            	return modify + publish + delete_button;
            },
            "aTargets": [ 6 ]
        }]
    });


    // 
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });


    $('body').on('click', '.delete_course', function() {

        var course_id = $(this).data('course-id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_course') }}", function(result){ 
            // if OK, delete the model
            if (result) {
                window.location.href = '/courses/delete/' + course_id;
            }

        });

    });


</script>


@endsection