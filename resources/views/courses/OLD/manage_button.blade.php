 @if (Auth::check())

    <div class="round_button bck-course pull-right" id="manage_courses">

        <!-- MANAGE COURSES -->
        @if (Auth::user()->isRole('managecourses'))
            <a href="/courses/modify/">
                <button class="btn round_button course" id="edit_courses">
                    <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                </button>
            </a> 
            {{ strtoupper(trans('text.courses')) }} 
            <a href="/courses/create/">
                <button class="btn round_button course" id="edit_courses">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </a>
            <br>
        @endif


        <!-- MANAGE CASES -->
        @if (Auth::user()->isRole('managestudycases'))
            <a href="/study_cases/modify/">
                <button class="btn round_button course" id="edit_courses">
                    <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                </button>
            </a> 
            {{ strtoupper(trans('text.courses')) }} 
            <a href="/study_cases/create/">
                <button class="btn round_button course" id="edit_courses">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </a>
            <br>
        @endif

    </div>
   
@endif 