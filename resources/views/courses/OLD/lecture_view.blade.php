@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['active_switch' => "lectures"])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-8" style="margin-top:20px;">

				<div class="row">
					
					<!-- LECTURE INFO -->
						<div class="col-xs-6">
							<!-- title, author -->
							<h4 class="text-lecture"> {!! $lecture->name !!}</h4>

							<!-- course -->
							<div class="row">
								<div class="col-xs-4">
									{{ trans('text.course') }}
								</div>
								<div class="col-xs-8">
									<a href="/courses/view/{!! $lecture->course_subject->course->id !!}">{{ $lecture->course_subject->course->name }}</a>
								</div>
							</div>

							<!-- lecture subject -->
							<div class="row">
								<div class="col-xs-4">
									{{ trans('text.course_subject') }}
								</div>
								<div class="col-xs-8">
									{{ $lecture->course_subject->name }}
								</div>
							</div>

							<!-- lecture description -->
							<div class="row">
								<div class="col-xs-4">
									{{ trans('text.description') }}
								</div>
								<div class="col-xs-8">
									{{ $lecture->description }}
								</div>
							</div>


							<!-- lecture content -->
							<div class="row">
								<div class="col-xs-4">
									{{ trans('text.contents') }}
								</div>
								<div class="col-xs-8">
									@if (isset($lecture->contents))
										@foreach($lecture->contents as $content)
											{{ $content->text }} <br>
										@endforeach
									@endif
								</div>
							</div>

							<!-- files / resources -->
							<div class="row">
								<div class="col-xs-4">
									{{ trans('text.resources') }}
								</div>
								<div class="col-xs-8">
									@foreach ($lecture->resources_published as $resource)
		      							<div class="round_button bck-course pull-left modal_url" data-url="/resources/view_modal/{!! $resource->id !!}/{!! $lecture->id !!}{!! ($server_id) ? '?server_id=' . $server_id : "" !!} ">{!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}</div>
		      						@endforeach
								</div>
							</div>

						</div>
							
					
					<!-- IMAGE -->
					@if ($lecture->image)
						<div class="col-xs-5">
							<img src="{{ $lecture->image }}" class="image">
						</div>
					@endif
				</div>

			</div>

			<!-- MENU RIGHT -->
            <div class="col-xs-2">
                @include('right_menu_filter', ['home_page' => 1])
            </div>
            
		</div>
	</div>

@endsection