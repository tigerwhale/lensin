<li class="col-xs-3 lecture_grid_li" data-lecture-id="{!! $lecture->id !!}">
	<!-- lecture image -->
	<div class="lecture_grid_image"
	@if ($lecture->image)
		style="background-image: url('{{ $lecture->image }}');" ></div>
	@else 
		>
		<img src="/images/resource/lecture_big.png"/></div>
		<!-- <i class="fa fa-graduation-cap" aria-hidden="true"></i></div> -->
	@endif

	<div class="row">
		<div class="col-xs-12 lecture_grid">

			<!-- TEXT DIV -->
			<div class="lecture_grid_text" data-id="{!! $lecture->id !!}">
				<div class="row">
					<!-- lecture name -->
					<div class="col-xs-9" id="lecture_{!! $lecture->id !!}_name">
						{{ $lecture->name }}
					</div>
					<!-- lecture icon -->
					<div class="col-xs-3 text-left">
						<img src="/images/resource/lecture.png" class="table_view_image">
					</div>
				</div>
				<!-- Date and published -->
				<div style="position: absolute; left: 25px; bottom: 10px; height: 20px; font-size: 10pt;">
					{!! $lecture->created_at->format("d M") !!} 
					<span style="font-size: 12pt;" class="lecture_{!! $lecture->id !!}_published">{!! ($lecture->published) ? '<i class="far fa-eye" aria-hidden="true"></i>' : '<i class="far fa-eye-slash" aria-hidden="true"></i>' !!}</span>
				</div>
				<!-- platform icon -->
				<div style="position: absolute; right: 25px; bottom: 10px;">
					<img src='{!! url("/images/server/logo.png") !!}' style="width: 30px;" class='table_view_image'>
				</div>
			</div>

			<!-- HOVER DIV -->
			<div class="row lecture_bottom_cover" style="color: white;">

				<!-- modify lecture -->
				<div class="col-xs-4 text-center" style="padding-top: 50px;">
					<button type="button" class="btn bck-course shadow modal_url" data-url="/lectures/edit_modal/{!! $lecture->id !!}"><i class="fas fa-pencil-alt" aria-hidden="true"></i></button>
					<br>
					{{ strtoupper(trans('text.modify')) }}
				</div>

				<!-- TODO publish lecture -->
				<?php $published_icon = ($lecture->published == 1) ? "far fa-eye-slash" : "far fa-eye"; ?>
				<?php $published_text = ($lecture->published == 1) ? "unpublish" : "publish"; ?>
				<div class="col-xs-4 text-center" style="padding-top: 50px;">
					<button type="button" class="btn bck-course publish_resource shadow" data-url="/lectures/publish" data-id="{!! $lecture->id !!}"><i class="{!! $published_icon !!}" aria-hidden="true"></i></button>
					<br>
					{{ strtoupper(trans('text.' . $published_text)) }}
				</div>

				<!-- delete lecture -->
				<div class="col-xs-4 text-center" style="padding-top: 50px;">
					<button data-model='lectures' data-delete-id="sortable_lectures_{{ $lecture->id }}" data-id={{ $lecture->id }} data-text='{{ trans("text.question_delete_lecture") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
					<br>
					{{ strtoupper(trans('text.delete')) }}
				</div>

			</div>

		</div>
	</div>
	<!-- sorting handle -->
	<span class="movable_arrow_lecture_grid "><i class="fas fa-arrows-alt" aria-hidden="true"></i></span>
</li>