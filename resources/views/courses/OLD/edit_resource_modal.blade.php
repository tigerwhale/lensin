<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="text-course modal-title">{{ trans('courses.edit_resource') }}</h4>
		</div>

		<div class="modal-body">
			<input type="hidden" id="resource_modal_id" value="{{ $resource->id }}">
			<!-- NAME -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.name') }}
				</div>
				<div class="col-xs-8">
					<input id="resource_modal_name" type="text" name="name" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->name }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- author -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.author') }}
				</div>
				<div class="col-xs-8">
					<input id="resource_modal_author" type="text" name="author" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->author }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- YEAR -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.year') }}
				</div>
				<div class="col-xs-8">
					<input list="years" type="text" id="resource_modal_year" name="year" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->year }}" class="form-control update_input" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year) 
							<option value="{{ $year }}">
						@endforeach
					</datalist>
				</div>
			</div>

			<!-- LICENCES -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.licence') }}
				</div>
				<div class="col-xs-8">
					@foreach (\App\Licence::all() as $licence)
						<?php $checked = $licence->resources->find($resource->id) ? "checked" : "";?>
						<input type="checkbox" id="licence_{!! $licence->id !!}" name="licence" data-url='/resources/licence_update' data-id={!! $resource->id !!} data-id-belongs-to="{!! $licence->id !!}" value="{{ $licence->resources->find($resource->id) }}" class="update_checkbox radio_course" style="display: none" {!! $checked !!} >
						<label class="round_button" for="licence_{{ $licence->id }}">
							<img id="licence_image_{{ $licence->id }}" src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
						</label>
					@endforeach
				</div>
			</div>

			<!-- FILE TYPE -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.file_type') }}
				</div>
				<div class="col-xs-8">
					@foreach (\App\ResourceType::all() as $resource_type)
						<?php $checked = ($resource->resource_type_id == $resource_type->id) ? "checked" : "";?>
						<input {!! $checked !!} type="radio" value="{{ $resource_type->id }}" list="years" type="text" id="resource_type_{{ $resource_type->id }}" name="resource_type_id" data-model='resources' data-id={{ $resource->id }} class="radio_course update_input resource_type" style="display: none;">
						<label class="round_button lens" for="resource_type_{{ $resource_type->id }}">{!! $resource_type->icon !!}</label>
					@endforeach
				</div>
			</div>

			<!-- LENGTH -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('text.length') }}
				</div>
				<div class="col-xs-8">
					<input type="text" name="length" data-model='resources' data-id={!! $resource->id !!} value="{{ $resource->length }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- PREVIEW -->
			<div class="row">
				<div class="col-xs-4">
					{{ trans('resources.preview') }}
				</div>
				<div class="col-xs-8">
					<input type="text" id="preview" name="preview" data-model='resources' data-id={!! $resource->id !!} value="{{ $resource->preview }}" class="form-control update_input" >
				</div>
			</div>

			<!-- FILES -->
			<div class="row" style="padding-bottom: 10px;">
				<div class="col-xs-4">
					{{ trans('text.file') }}
				</div>
				<div class="col-xs-8">
					<div id="filename" style="width:100%; word-wrap: break-word">
						@include('courses.edit_resource_modal_file_list', ['resource' => $resource])
					</div>
				</div>
			</div>	
			<!-- FILE UPLOAD -->
			<div class="row">
				<div class="col-xs-4">	
				</div>
				<div class="col-xs-8">
					<!-- DROPZONE -->
					<div class="dropzeon dropzone_div" id="resource_upload_{{ $resource->id }}">{{ trans('text.click_to_select_file') }}</div>
				</div>

			</div>


			<script type="text/javascript">

				var token = $("meta[name='csrf-token']").attr("content"); 
			  	var progressBar_resource_{{ $resource->id }} = document.getElementById('progressBar_resource_{{ $resource->id }}');
			  	var progressOuter_resource_{{ $resource->id }} = document.getElementById('progressOuter_resource_{{ $resource->id }}');



				// DROPZONE - upload resouce and refresh file list
				$("div#resource_upload_{{ $resource->id }}").dropzone({ 

					url: "/resources/upload_file/{{ $resource->id }}",
					paramName: "resource_file",
					previewsContainer: false,
					uploadMultiple: false,
					parallelUploads: 1,

					sending: function(file, xhr, formData) {
						// add the token to request
						formData.append( "_token", $("meta[name='csrf-token']").attr('content') );

						// if extension is zip, ask if the upload is camtasia
						var extension = file.name.substring(file.name.length -3);

						if (extension == "zip") {

							if (confirm( "{{ trans('resources.question_is_this_camtasia') }}" )) {
					    		formData.append("zip", "1");
					        	console.log("zip");	
					    	}
						}
						
					},

					uploadprogress: function(file, progress, bytesSent) {
						// upload progress percentage
					    $("#resource_upload_{{ $resource->id }}").html(parseInt(progress) + "%");
					},

					success: function(file, responce) {

						// id of the parrent lecture
						var lecture_id = {{ $resource->resourcable->id }};

						// add the row and open the modal
						if (responce != "upload_error") {
							var token = $("meta[name='csrf-token']").attr("content"); 

							$.post({
								url: "/resources/lecture_modal_file_list/{{ $resource->id }}",
								data: {
									'_token': token,
				      			},
								success: function(data) {
									$('#filename').html(data);
								}
							});

						} else {

							alert ("{{ trans('text.upload_error') }}");
						}

						// Return text to "click here..."
						$("#resource_upload_{{ $resource->id }}").html("{{ trans('text.click_to_select_file') }}");
						
					},

					// error return function
					error: function (file, responce){
						$("#resource_upload_{{ $resource->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
					},

					init: function (){
						
						myDropzone = this;
					},

				});



				// delete resource file
				$('body').on('click', '#delete_resource_file', function(){

					$.ajax({
						url: '/resources/remove_file/{{ $resource->id }}',
						type: 'POST',
						data: {
							'_token': token,
					      },
						success: function(data, status) {
							if (data == "OK") {
								$("#filename").html("");
							} else {
								alert(data);
							}
						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});

				});

			</script>
		
		</div>
	</div>
</div>