	<!-- subjects -->
	<div class="courseware">
		<ul id="sortable_courseware" class="sortable_ul">
			@foreach ($course->subjects as $subject)
				@include ('courses.course_subject_edit', ['courseSubject' => $subject])
			@endforeach
		</ul>
	</div>

	<!-- add subject button -->
	<div class="row">
		<div class="col-xs-1 text-center">
			<button type="button" class="btn bck-course" id="add_subject"><i class="fa fa-plus" aria-hidden="true"></i></button>
		</div>
		<div class="col-xs-11" style="padding-top: 6px; padding-left: 0px;">
			<span class="text-course">{{ strtoupper(trans('text.add_subject')) }}</span>
		</div>
	</div>