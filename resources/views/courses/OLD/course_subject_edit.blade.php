<li data-subject-id='{!! $courseSubject->id !!}' class="sortable_li">
	<div class="row" style="margin-bottom: 5px; margin-top: 10px; " id="course_subject_{{ $courseSubject->id }}">
		<div class="col-xs-1 text-center" style="padding: 10px;">

			<!-- delete button -->
			<button data-model='course_subjects' data-delete-id="course_subject_{{ $courseSubject->id }}" data-id={{ $courseSubject->id }} data-text='{{ trans("text.question_delete_course_subject") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
			<br>

			<!-- publish button -->
			<?php $icon = ($courseSubject->published == 1) ? "far fa-eye" : 'far fa-eye-slash'; ?>
			<div class="btn
				<?php if ($courseSubject->published == 1)
						echo 'bck-course';
					else echo 'bck-course-unpublished'; ?>
				publish_resource" data-url="/course_subjects/publish" data-id="{!! $courseSubject->id !!}" style="margin-top: 10px;">
				<i class="{!! $icon !!}" aria-hidden="true"></i>
			</div>
			<br>

			<!-- order -->
			{{ $courseSubject->order }}
				
		</div>
		<div class="col-xs-11" style="padding: 0px; padding-bottom: 10px;" id="course_subject_{{ $courseSubject->id }}_collapse">

			<div class="panel-group" id="accordion_{{ $courseSubject->id }}">
				<div class="panel panel-default" style="background-color: #f2f2f2; padding-left:10px; padding-right: 10px;">

					<div class="panel-heading">
						<!-- course subject name-->
						<input style="width: 90%;" type="text" name="name" data-model='course_subjects' data-id={{ $courseSubject->id }} value="{{ $courseSubject->name }}" placeholder="{{ strtoupper(trans('text.insert_subject_title')) }}" class="form-control update_input">

						<!-- collapse header icon-->							
						<a data-toggle="collapse" data-parent="#accordion_{{ $courseSubject->id }}" href="#collapse{!! $courseSubject->id !!}">
							<span class="collapsable_arrow" href="#collapse{!! $courseSubject->id !!}"><i class="fa fa-sort-asc" aria-hidden="true"></i></span>
						</a>							

						<!-- sorting handle -->
						<span class="movable_arrow"><i class="fas fa-arrows-alt-v" aria-hidden="true"></i></span>
					</div>

					
					<!-- collapse content -->
					<div id="collapse{!! $courseSubject->id !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $courseSubject->index !!}" style="background-color: #f2f2f2;">
						<!-- course subject -->
						<div id="course_subject_{!! $courseSubject->id !!}_lectures" style="margin-bottom: 10px; margin-top: 10px;">
							<ul id="sortable_lectures_{!! $courseSubject->id !!}" class="sortable_ul">
								@foreach ($courseSubject->lectures as $lecture)
									@include ('courses.lecture_edit', ['lecture' => $lecture])
								@endforeach
							</ul>
						</div>
						<!-- add lecture button -->
						<button type="button" class="btn bck-lecture add_lecture" data-course-subject-id="{{ $courseSubject->id }}"><i class="fa fa-plus" aria-hidden="true"></i></button>
						<span class="text-lecture">{{ strtoupper(trans('text.add_lecture')) }}</span>
						<div class="clearfix">&nbsp;</div>
					</div>

				</div>	
			</div>

		</div>
	</div>

	<script type="text/javascript">

		sort_course_subject_{!! $courseSubject->id !!}();

		function sort_course_subject_{!! $courseSubject->id !!}() {
			$( "#course_subject_{!! $courseSubject->id !!}_collapse" ).accordion({
		    	header: "> span",
		    	handle: ".movable_arrow",
		    	collapsible: true,
		    	heightStyle: "content",
		    	active: false
		  	})

			$( "#sortable_lectures_{!! $courseSubject->id !!}" ).sortable({
				stop: function( event, ui ) {
					// get the token
					var token = $("meta[name='csrf-token']").attr("content"); 

					// make subject id-s		
					var lectures = [];

					$(this).children('li').each(function(i){
						//alert ($(this).data('lecture-id'));
						lectures.push($(this).data('lecture-id'));
					});

					// send to server
					$.post({
						url: '/lectures/update_sort',
						data: {
							'_token': token,
							'lectures': lectures
						},
						success: function(data, status) {
							//alert(data);
						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});
			  	}
			});
		}

		sort_courses();

	</script>
</li>