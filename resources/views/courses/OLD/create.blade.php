@extends('layouts.resource')

@section('content')

	<form method="POST" action="/courses/create" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row bck-course resource_header">
		
			<!-- left text -->
			<div class="col-xs-4 text-left resource_header_title">
				<table>
					<tr>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt">
							<p style="line-height: 38pt; margin-bottom: 6px;">+</p>
						</td>
						<td style="vertical-align: middle; height: 70px; padding-left: 10px;">
							{{ strtoupper(trans('text.create_mode')) }}
						</td>
					</tr>
				</table>
				
			</div>

			<!-- middle buttons -->
			<div class="col-xs-4">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						<!-- back -->
						<td style="padding: 5px;">
							<a href="/courses/modify/">
								<button class="btn btn-danger" type="button"><i class="fas fa-arrow-left" aria-hidden="true"></i></button>
							</a>
							<br>
							{{ strtoupper(trans('text.back')) }}
						</td>
						<!-- confirm -->
						<td style="padding: 5px;">
							<button class="btn btn-success" type="submit"><i class="fa fa-check" aria-hidden="true"></i></button>
							<br>
							{{ strtoupper(trans('text.confirm')) }}
						</td>
						<!-- cancel -->
						<td style="padding: 5px;">
							<button class="btn btn-danger back-button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							<br>
							{{ strtoupper(trans('text.cancel')) }}
						</td>
					</tr>
				</table>			
			</div>
			
			<!-- right text -->
			<div class="col-xs-4 text-right resource_header_title">
				<table style="float: right;">
					<tr>
						<td style="vertical-align: middle; height: 70px;">
							{{ strtoupper(trans('text.courses')) }}
						</td>
						<td style="height: 70px; vertical-align: middle; font-size: 38pt; padding-left: 10px;">
							<p style="line-height: 38pt; margin-bottom: 6px;"><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>							
						</td>
					</tr>
				</table>
				
			</div>			
		</div>
		

		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<button type="button" class="btn bck-course">{{ trans('text.info') }}</button>
					<button type="button" class="btn bck-course" disabled="disabled">{{ trans('text.courseware') }}</button>
				</div>
			</div>

			<div class="row" style="margin-top: 20px">
				<div class="col-xs-3">
					<img src="/images/resource/course.png" class="table_view_image"> {{ strtoupper(trans('text.resource_title')) }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="name" class="form-control" placeholder="{{ strtoupper(trans('text.insert_title')) }}" required>
				</div>
			</div>

			<div class="row" style="margin-top: 20px">
				
				<div class="col-xs-4 text-center">
					<!-- IMAGE -->
					{{ strtoupper(trans('text.cover_image')) }}

					<div class="image_container" id='image_container' name='image'>
						
						<label for="fileselect">Files to upload:</label>
						<input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
						<div id="filedrag">or drop files here</div>


						<!-- show upload button -->
						<div id="upload_button"><br>
							<input type="file" name="image" style="display: none">
							<label for="image">{{ trans('text.drop_or_click_to_select_image') }}</label>
						</div>
					</div>					

					<!-- SCHOOL -->
					{{ trans('text.school') }}
					<input name="school" id="school" list="schools" class="form-control" required value="{{ Auth::user()->school }}">
					<datalist id="schools">
						@foreach (list_schools() as $school) 
							<option value="{{ $school->school }}">
						@endforeach
					</datalist>

					<!-- YEAR -->
					{{ trans('text.year') }}
					<input name="year" id="year" list="years" class="form-control" required value=" {{ date('Y') }} " >
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year)
							@if ($year === {{ date('Y') }})
								<option selected value="{{ $year }}">
							@else
								<option value="{{ $year }}">
							@endif
						@endforeach
					</datalist>

					<!-- COUNTRY -->
					{{ trans('text.country') }}
					{!! Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'required' => 'required']) !!}

					<!-- COUNTRY -->
					{{ trans('text.language') }}
					{!! Form::select('language_id', $languages, null,  ['class' => 'form-control', 'required' => 'required']) !!}

					<!-- TYPE -->
					{{ trans('text.type') }}
					<input name="type" id="type" list="types" class="form-control" required>
					<datalist id="types">
						@foreach (DB::table('courses')->select('type')->distinct()->get() as $type) 
							<option value="{{ $type->type }}">
						@endforeach
					</datalist>				


				</div>

				<!-- SYLLABUS -->
				<div class="col-xs-5">
					{{ strtoupper(trans('text.syllabus')) }}<br>
					<textarea name="description" class="form-control" placeholder="{{ trans('text.insert_description') }}" style="height: 300px;"></textarea>
				</div>

				<!-- AUTHORS -->
				<div class="col-xs-3">
					{{ strtoupper(trans('text.authors')) }}
					<div class="text-center">
						<div id="authors_list">
							
							<!-- default author -->
							<div id="author_{!! Auth::user()->id !!}">
								<input type="hidden" name="authors[]" value="{!! Auth::user()->id !!}">
								{{ Auth::user()->name }} {{ Auth::user()->last_name }} 
								<button class="btn btn-danger remove_teacher_button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
							</div>

						</div>
						<button type="button" class="btn bck-course" data-toggle="modal" data-target="#authorsModal"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>
		</div>

	</form>


<!-- Modal -->
<div id="authorsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
			</div>
			<div class="modal-body">
				<table class="display" id="datatable" style="width: 100%">
					<thead>
						<th>{{ trans('text.name') }}</th>
						<th>{{ trans('text.last_name') }}</th>
						<th>{{ trans('text.school') }}</th>
						<th></th>
					</thead>
				</table>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	
	// DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache": true,
            "url": '/users/list',
            "rowId": 'id',
        },
        "columns": [
            { "data": "name" },
            { "data": "last_name" },
            { "data": "school" },
            { "data": "id" },

        ],
		"aoColumnDefs": [
        {
            "mRender": function ( data, type, row ) {
                return "<button class='btn btn-success' type='button' id='add_teacher'><i class='fa fa-plus' aria-hidden='true'></i></button>";
            },
            "aTargets": [ 3 ]
        },
     	]
    });

    // ALTER TEACHERS
	$('#datatable tbody').on( 'click', '#add_teacher', function () {

		var row_data = table.row( $(this).parents('tr') ).data();

		var author_id = row_data['id'];
		var author_full_name = row_data['name'] + " " + row_data["last_name"];

		var author_input_field = '<div id="author_' + author_id + '">';
		author_input_field += '<input type="hidden" name="authors[]" value="' + author_id + '">';
		author_input_field += author_full_name;
		author_input_field += '<button class="btn btn-danger remove_teacher_button" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>';
		author_input_field += '</div>';

		$("#authors_list").append(author_input_field);

	});


    $('.back-button').click(function(){
        parent.history.back();
        return false;
    }); 


</script>

@endsection
