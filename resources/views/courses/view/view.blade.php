@extends('layouts.app')

@section('content')
	
	<!-- COURSES -->
	<div class="container">
		<div class="row">

			<!-- MENU LEFT -->
			<div class="col-xs-2">
				@include('left_menu_filter', ['active_switch' => "courses"])
			</div>

			<!-- CONTENT -->
			<div class="col-xs-8" id="course_content" style="margin-top: 20px;" >
    			<div class="text-center" style="width: 100%;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
			</div>

			<!-- MENU RIGHT -->
            <div class="col-xs-2">
                @include('right_menu_filter', ['home_page' => 1])
            </div>
            
		</div>
	</div>
	
	<script type="text/javascript">
		var token = $("meta[name='csrf-token']").attr("content"); 

        $.post({
            url: "{!! $viewApiUrl !!}",
            data: {
                '_token': token,
                'user_id': user_id,
              },
            success: function(data, status) {
                $('#course_content').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
	</script>

@endsection