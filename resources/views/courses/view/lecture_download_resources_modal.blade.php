<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content">

		<!-- TITLE -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ (isset($modal_data->name) ? $modal_data->name : "") }}</h4>
		</div>

		<!-- FILES -->
		<div class="modal-body">
			
			@foreach($modal_data->resources_published as $resource)
				@if ($resource->path)
					<div class="row">
						<div class="col-xs-12">
							<input type="checkbox" data-resource="{!! $resource->id !!}" class="download_file" id="download_file_{!! $resource->id !!}" data-path="{!! url($resource->path) !!}" value="{!! $resource->path !!}"> <label for="download_file_{!! $resource->id !!}">{{ $resource->name }}</label>
						</div>
					</div>
				@endif 
			@endforeach

			<button type="button" class="btn btn-success" id="download_selected_files">{{ trans('courses.download_selected_files') }}</button>

		</div>
	</div>
</div>

<script type="text/javascript">
	
	$( document ).ready(function() {

		$("#download_selected_files").click( function(){
			var files = [];
			$('.download_file:checked').each(function(){
				var url_path = $(this).data('path');
				var filename = url_path.substring(url_path.lastIndexOf('/')+1);
				var resource = $(this).data('resource');
				var server_url = "{!! url('/api/resources/increment_download/') !!}";
				 files.push({ 
				 	'download' : url_path, 
				 	'filename' : filename,
				 	'resource' : resource,
				 	'server_url' : server_url
				 });
			});
			download_files(files);

		});

	});


</script>