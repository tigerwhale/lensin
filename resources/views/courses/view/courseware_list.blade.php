<!-- VIEW SELETOR grid/list  -->
<div class="row">
	<div class="col-xs-12 text-center">
		<i id="grid_view_selector" class="fa fa-th-large resource_view_selector" data-id="{!! $course->id !!}" aria-hidden="true" ></i>
		<i id="list_view_selector" class="fa fa-bars resource_view_selector  text-course"  data-id="{!! $course->id !!}"  aria-hidden="true"></i>
	</div>
</div>

<!-- COURSE TITLE -->
<div class="row" style="margin-bottom: 10px;">
	<div class="col-xs-12">
		<table>
			<tr>
				<td style="padding-right: 10px;">
					<div class="btn-course-small pull-left">&nbsp;</div>&nbsp;
				</td>
				<td>
					<div class="resource-title text-course">{{ $course->name }}</span>
				</td>
			</tr>
		</table>	
	</div>
</div>

<!-- COURSEWARE -->
<div class="row">

	<!-- SUBJECTS -->
	@if (isset($course->subjects_published))
		<!-- if no published subjects -->
		@if (count($course->subjects_published) == 0)
			{{ trans('text.no_subjects') }}
		@endif

		@foreach ($course->subjects_published as $subject)
			<div class="panel-group" id="accordion{!! $loop->index !!}">
				<div class="panel panel-default in">
					<!-- subject heading -->
					<a class="collapse_arrow" data-toggle="collapse" data-parent="#accordion{!! $loop->index !!}" href="#collapse{!! $loop->index !!}">
						<div class="panel-heading">
							{{ $subject['name'] }}
							<!-- collapse header icon-->							
							<div class="pull-right">
								<i class="fa fa-caret-up pull-right fa-lg" aria-hidden="true"></i>
							</div>	
						</div>
					</a>
					
					<!-- LECTURES -->
					<div id="collapse{!! $loop->index !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $loop->index !!}">
				        <div class="panel-body">
	      					@if (isset($subject['lectures_published']))
	      						
	      						<!-- if no published lectures -->
								@if (count($subject['lectures_published']) == 0)
									{{ trans('text.no_lectures') }}
								@endif

					      		@foreach ($subject['lectures_published'] as $lecture)
					      			<div class="row hover-lecture">
					      				<div class="col-xs-1">
					      					<span class="btn-lecture-small shadow-small pull-right">&nbsp;</span>
					      				</div>
					      				<div class="col-xs-4">
					      					{{ $loop->parent->iteration }}.{{ $loop->iteration }} {{ $lecture['name'] }}
					      				</div>
					      				<div class="col-xs-2">
					      					
					      				</div>
					      				<div class="col-xs-3">
					      					{{ $lecture['description'] }}
					      				</div>
					      				<div class="col-xs-2">

					      					<!-- RESOURCES -->
					      					@if (isset($lecture['resources_published']))
					      						<!-- if no published resources -->
												@if (count($lecture['resources_published']) == 0)
													{{ trans('text.no_resources') }}
												@endif
												
												<?php $resource_icons = []; ?>

					      						@foreach ($lecture['resources_published'] as $resource)
					      							<!-- resource type icon -->
					      							@if ( !in_array($resource['resource_type']['name'], $resource_icons) )
						      							<?php 
						      								// add to array
						      								array_push($resource_icons, $resource['resource_type']['name']);

						      								if (!server_property('central_server')){
						      									$server_url = url('/');
						      								} else {
						      									$server_url = server_url($course->server_id);
						      								}
						      								
						      							?>
						      							<div class="col-xs-3">
						      								<div class="round_button bck-course modal_url lens" style="margin-left: auto; margin-right: auto;" data-url="{!! $server_url !!}/api/resources/view_modal/{!! $resource['id'] !!}/{!! $lecture['id'] !!}" data-data="" title="{!! trans('resource_types.' . $resource['resource_type']['name']) !!}">{!! $resource['resource_type']['icon'] !!}</div>
						      							</div>
					      							@endif
					      						@endforeach

					      					@else 
					      						{{ trans('text.no_resources') }}
					      					@endif

					      				</div>
					      			</div>
					      			
					      		@endforeach
							
							@else 
					      		{{ trans('text.no_lectures') }}					      		
					      	@endif 
	      				</div>
	    			</div>
	    		</div>
			</div>

		@endforeach 
	
	@else 
		{{ trans('text.no_subjects') }}
	@endif
	
</div>