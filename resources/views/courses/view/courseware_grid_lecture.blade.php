<?php 
	$background_image = $lecture['image'] ? url($lecture['image']) : '/images/resource/lecture_big.png';
	$lecture_grid_image_class = $lecture['image'] ? 'lecture_grid_image' : 'lecture_grid_no_image';
?>
<script type="text/javascript">
	var lecture_files_{!! $lecture['id'] !!} = [];
</script>

<li class="col-xs-3 datatable_grid_div" style="margin-top: 0px;">
	<div class="{!! $lecture_grid_image_class !!}"	style="background-image: url('{{ $background_image }}');" ></div>

	<!-- HOVER DIV -->
	<div class="row datatable_grid_div_bottom_cover" style="color: white;">
			
			<!-- RESOURCES -->
			@if (isset($lecture['resources_published']))
			
				<div class="col-xs-12" style="margin-top:40px;">
					<div class="row">
						
						<!-- if no published resources -->
					@if (count($lecture['resources_published']) == 0)
						{{ trans('text.no_resources') }}
					@else 
				
						<?php 
							$resource_icons = []; 
							$icons_order = \App\ResourceType::orderBy('order')->get(); 
						?>
						<!-- SORT ICONS -->
						@foreach ($icons_order as $icon_order)

	  						@foreach ($lecture['resources_published'] as $resource)
	  							
	  							@if ($resource['resource_type']['id'] ==  $icon_order->id)

		  							<!-- resource type icon -->
		  							@if ( !in_array($resource['resource_type']['name'], $resource_icons) )
		      							<?php 
		      								// add to array
		      								array_push($resource_icons, $resource['resource_type']['name']);

		      								if (!server_property('central_server')){
		      									$server_url = url('/');
		      								} else {
		      									$server_url = server_url($course['server_id']);
		      								}
		      								
		      							?>
		      							<div class="col-xs-4">
		      								@if ($user_id)
		      									<div class="round_button bck-course modal_url lens" data-user-id="{!! $user_id ? $user_id : '' !!}" style="margin-left: auto; margin-right: auto;" data-url="{!! $server_url !!}/api/resources/view_modal/{!! $resource['id'] !!}/{!! $lecture['id'] !!}" data-data="" title="{!! trans('resource_types.' . $resource['resource_type']['name']) !!}">{!! $resource['resource_type']['icon'] !!}</div>
		      								@else
		      									<div class="round_button bck-course lens" title="{!! trans('resource_types.' . $resource['resource_type']['name']) !!}. {{ trans('text.you_have_to_login_to_download_this_resource') }}" style="margin-left: auto; margin-right: auto;" >{!! $resource['resource_type']['icon'] !!}</div>
		      								@endif
		      							</div>
		  							@endif

		  							@if (isset($resource['path']))
		  								<script type="text/javascript">
		  									lecture_files_{!! $lecture['id'] !!}.push("{!! $resource['path'] !!}");
		  								</script>
		  							@endif

		  						@endif

	  						@endforeach
						@endforeach	  						

  						<div class="col-xs-3">
  							@if ($user_id)
	  							<div class="round_button modal_url text-course text-center" style="margin-left: auto; margin-right: auto; background-color: white;" title="{{ trans('text.download') }}" data-url="{!! $server_url !!}/api/lectures/view_modal_download/{!! $lecture['id'] !!}">
	  								<i class="fa fa-download" aria-hidden="true"></i>
	  							</div>
	  						@else
	  							<div class="round_button text-course text-center" style="margin-left: auto; margin-right: auto; background-color: white;" title="{{ trans('text.you_have_to_login_to_download_this_resource') }}">
	  								<i class="fa fa-download" aria-hidden="true"></i>
	  							</div>
	  						@endif
	  							
  						</div>
  						
  					@endif
					</div>
			</div>

			@else 
				{{ trans('text.no_resources') }}
			@endif

	</div>


	<div class="row">
		<div class="col-xs-12 lecture_grid">

			<!-- TEXT DIV -->
			<div class="lecture_grid_text">

				<div class="row">
					<!-- lecture name -->
					<div class="col-xs-12 line-limit" style="height: 110px;">
						{{ $lecture['name'] }}
					</div>
				</div>
				<div class="row">
					<!-- icon -->
					<div class="col-xs-3 text-left">
						<div class="btn-lecture-small btn-smaller shadow-small"></div>
					</div>
					<!-- date -->
					<div class="col-xs-6 text-center grid_date">
						{!! strtoupper(Carbon\Carbon::parse($lecture['created_at'])->format('M y')) !!}
					</div>
					<!-- platform icon -->
					<div class="col-xs-3">
						<div class="platform_icon btn-smaller pull-right" style="background-image: url({!! url("/images/server/logo.png") !!}); "></div>
					</div>
				</div>		
				
			</div>

		
		</div>
	</div>															

</li>