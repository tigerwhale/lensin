<!-- COURSE -->

<!-- content header -->
<div class="row" style="padding-bottom: 20px; margin-left: -40px; margin-right: -40px;">
    <div class="col-xs-12 text-center">
        <ul id="content_tabs" class="nav nav-tabs">
            {!! left_back_button('courses') !!}
		  	<li class="active">
		  		<a data-toggle="tab" href="#course_info">
		  			{{ strtoupper(trans('text.info')) }}
		  		</a>
		  	</li>
		  	<li>
				<a data-toggle="tab" href="#course_courseware" id="open_courseware">
					{{ strtoupper(trans('text.courseware')) }}
				</a>
			</li>
            <li style="position: absolute; right: 15px;">
                {!! view_counter_icons($course) !!}
            </li>
		</ul>
	</div>
</div>

<div class="row tab-content">
	
    <!-- COURSE INFO -->
	<div role="tabpanel" class="tab-pane active" id="course_info" style="padding-left: 15px; padding-right: 15px;">
		<!-- title -->
        <div class="row" style="margin-bottom: 10px; margin-left:">
        	<div class="col-xs-12">
        		<span class="title">{{ strtoupper(trans('text.title')) }}</span>
        		<table style="margin-top: 5px;">
        			<tr>
        				<td style="padding-right: 10px;">
        					<div class="btn-course-small pull-left">&nbsp;</div>&nbsp;
        				</td>
        				<td>
        					<div class="resource-title text-course">{{ $course->name }}</div>
        				</td>
        			</tr>
        		</table>
        	</div>
        </div>

        <!-- info -->
        <div class="row">
        	<div class="col-xs-4">
        		<!-- image --> 
        		{!! coverImage($course, "course_big.png") !!}

        		<!-- description -->
        		<div class="title">
        			{{ strtoupper(trans('text.description')) }}
        		</div>

        		<table style="width:100%;" class="table borderless">
        			<tr>
        				<td>
        					<!-- school -->
        					<span class="title">
        						{{ strtoupper(trans('text.school')) }}
        					</span>
        				</td>
        				<td class="text-right">
							{{ $course->school }}
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<!-- year -->
        					<span class="title">
        						{{ strtoupper(trans('text.year')) }}
        					</span>
        				</td>
        				<td class="text-right">
        					{{ $course->year }}
        				</td>
					</tr>
					<tr>
        				<td>
        					<!-- country -->
        					<span class="title">
        						{{ strtoupper(trans('text.country')) }}
        					</span>
        				</td>
        				<td class="text-right">
        					{{ isset($course->country->name) ? $course->country->name : "" }}
        				</td>
        			</tr>
					<tr>
        				<td>
        					<!-- type -->
        					<span class="title">
        						{{ strtoupper(trans('text.type')) }}
        					</span>
        				</td>
        				<td class="text-right">
        					{{ $course->type }}
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<!-- language -->
        					<span class="title">
        						{{ strtoupper(trans('text.language')) }}
        					</span>
        				</td>
        				<td class="text-right">
        					{{ isset($course->language->name) ? $course->language->name : "" }}
        				</td>
        			</tr>
                    <tr>
                        <td>
                            <!-- duration -->
                            <span class="title">
                                {{ strtoupper(trans('text.duration')) }}
                            </span>
                        </td>
                        <td class="text-right">
                            {{ $course->duration }}
                        </td>
                    </tr>                    
        		</table>	                		
        	</div>

        	<div class="col-xs-6">
        		<!-- syllabus -->
        		<div class="title">
        			{{ strtoupper(trans('text.syllabus')) }}
        		</div>
        		{{ $course->description }}
        	</div>

        	<div class="col-xs-2">
        		<!-- teachers -->
        		<div class="title">
        			{{ strtoupper(trans('text.authors')) }}
        		</div>
        		@if(isset($course->teachers))
					@foreach($course->teachers as $teacher)
						<div class="text-course title remove_teacher">
							<img src="{!! $teacher->image !!} " class="image">
							{{ $teacher->name }} {{ $teacher->last_name }}
						</div>
					@endforeach
				@endif
        	</div>
        </div>
	</div>

    <!-- COURSEWARE -->
	<div role="tabpanel" class="tab-pane" id="course_courseware">
        <!-- Populated through ajax -->
        <div class="text-center" style="width: 100%;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
	</div>
	
</div>

<script type="text/javascript">
	
	$(document).ready(function() {

        coursewareView('grid');
        $("#open_courseware").tab('show');

		// GRID VIEW LOADER
		$('body').on('click', '#open_courseware', function() {
			coursewareView('grid');
		});

		// GRID VIEW LOADER
		$('body').on('click', '#grid_view_selector', function() {
			coursewareView('grid');
		});

		// LIST VIEW LOADER
		$('body').on('click', '#list_view_selector', function() {
			coursewareView('list');
		});

		// PREVENT DEFAULT ON TABS
		$('#content_tabs a').click(function (e) {
		  	e.preventDefault();
		  	$(this).tab('show');
		});

	});

	// LOAD COURSEWARE VIEW
	function coursewareView(view_type, obj) {

		var token = $("meta[name='csrf-token']").attr("content"); 
		var view_url = '{!! url("/") !!}/api/courses/view/courseware/' + view_type + '/{!! $course->id !!}/{!! $course->server_id !!}';

		$.get({
			url: view_url,
			data: {
				'_token': token,
				'user_id': user_id,
		      },
			success: function(data, status) {
				$("#course_courseware").html(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});	
	}

</script>

