<?php 
	$resource_count = count($resources);
?>
@foreach($resources as $resource)

	<?php 
		$hidden_resource = "block";
		$arrows_hidden = "hidden";
		// is there one or more 
		$previous_modal = 0;
		$next_modal = 0;

		if ( $resource_count > 1 ) 
		{
			$hidden_resource = ( $opened_resource_id == $resource->id ) ? "block" : "none";
			// unhide the arrows
			$arrows_hidden = "";
			$previous_modal = ($loop->index == 0) ? $resource_count - 1 : $loop->index - 1;
			$next_modal = ($loop->index + 1 == $resource_count) ? 0 : $loop->index + 1;
		}
	?>

	<div class="modal-dialog modal-lg" style="display:{!! $hidden_resource !!}" id="modal-{!! $loop->index !!}">

		<!-- Modal content-->
		<div class="modal-content" style="font-size: 13pt;">
			<div class="modal-body">

				<!-- header -->
				<div class="row" style="margin-bottom: 20px;">
					<div class="col-xs-1 col-sm-offset-1 text-resource">
					</div>
					<!-- resource name -->
					<div class="col-xs-8 text-center resource_text text-lecture">
						<div class="round_button resouce-button bck-resource lens" style="display: inline-block;">{!! ($resource->resource_type) ? $resource->resource_type->icon : "" !!}</div> {{ $resource->resourcable->name }}
					</div>
					<!-- close button-->
					<div class="col-xs-2 text-right">
						<button type="button" class="btn round_button text-course" style="background-color: #f2f2f2" data-dismiss="modal">X</button>
					</div>
				</div>

				<!-- content -->
				<div class="row">

					<!-- left arrow -->
					<div class="col-xs-1">
						<div class="{!! $arrows_hidden !!} round_button arrow_modal text-course" data-this-modal-id="{!! $loop->index !!}" data-open-modal-id="{!! $previous_modal !!}"><strong><i class="fa fa-angle-left" aria-hidden="true"></i></strong></div>
					</div>

					<!-- resource left side -->
					<div class="col-md-4">
						<table style="width: 100%" class="table borderless">
							<!-- name -->
							<tr>
								<td class="text-lecture">{{ trans('text.name') }}</td>
								<td>{{ $resource->name }}</td>
							</tr>
							<!-- author -->
							<tr>
								<td class="text-lecture">{{ trans('text.author') }}</td>
								<td>{{ $resource->author }}</td>
							</tr>
							<!-- year -->
							<tr>
								<td class="text-lecture">{{ trans('text.year') }}</td>
								<td>{{ $resource->year }}</td>
							</tr>
							<!-- licence -->
							<tr>
								<td class="text-lecture">{{ trans('text.licence') }}</td>
								<td>
									@if ($resource->licences)
										@foreach($resource->licences as $licence)
											<img src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
										@endforeach
									@endif
								</td>
							</tr>
							<!-- length -->
							<tr>
								<td class="text-lecture">{{ trans('text.length') }}</td>
								<td>{{ $resource->length }} {{ isset($resource->unit) ? trans('text.' . $resource->unit) : ""}}</td>
							</tr>
							<!-- download -->
							<tr>
								<td class="text-lecture">{{ trans('text.download') }}</td>
								<td>{!! downloadLinkAndIncrementAlignLeft("api/resources/increment_download/" . $resource->id, $request) !!}</td>
							</tr>
						</table>
					</div>
					
					<!-- resource right side -->
					<div class="col-md-6" style="height: 300px;">
						@if ($resource->path && !$resource->preview)
							<!-- SINGLE FILE -->
							<div class="row">
								<div class="col-xs-12">
									
									
									@if ($resource->resource_type->name == "video") 
									<!-- VIDEO --> 
										<video id="my-video" class="video-js" controls preload="none" data-setup="{}">
										    <source src="{{ url($resource->path) }}?{{ time() }}" type='video/mp4'>
										    <p class="vjs-no-js"> 
												To view this video please enable JavaScript, and consider upgrading to a web browser that
												<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
										    </p>
										</video>
									
									@elseif ($resource->resource_type->name == "image")
									<!-- IMAGE -->
										<!-- icon and name -->
										<a href="{!! url($resource->path) !!}?{{ time() }}" target="_blank">
											<div class="row" style="height: 300px; line-height: 250px; background-color: #CCC;">
												<div class="col-xs-12 text-center">
													<!-- resource file type -->
													<div class="resource_grid_icon lens">
															@if ($resource->resource_type)
																{!! $resource->resource_type->icon !!}
															@else 
																<i class="fa fa-ellipsis-h fa-3x" aria-hidden="true"></i>
															@endif
														
														<!-- resource name -->
														<div class="resource_grid_name line-limit">
															{!! $resource->name() !!}
														</div>
													</div>
													
												</div>
											</div>
										</a>
								
							        @else
								        <div class="row" style="height: 300px; padding-top: 20px;">
											<div class="col-xs-12 text-center">
												<!-- resource file type -->
												<div class="resource_grid_icon lens" style="font-size:150px; color: #CCC">
													@if ($resource->resource_type)
														{!! $resource->resource_type->icon !!}
													@else 
														<i class="fa fa-ellipsis-h" aria-hidden="true"></i>
													@endif
												</div>
												<!-- resource name 
												<div class="resource_grid_name line-limit" style="margin-top: 20px;">
													{!! $resource->name() !!}
												</div>-->
											</div>
										</div>		
									@endif

								</div>
							</div>

						@else ($resource->preview && $resource->path)
							<!-- FILE AND PREVIEW (CAMTASIA) -->
							@if( pathinfo($resource->preview, PATHINFO_EXTENSION) == "mp4" )
								<!-- mp4 -->
								<video id="my-video" class="video-js" controls preload="none" data-setup="{}">
								    <source src="{{ url($resource->preview) }}?{{ time() }}" type='video/mp4'>
								    <p class="vjs-no-js"> 
										To view this video please enable JavaScript, and consider upgrading to a web browser that
										<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
								    </p>
								</video>
							@else 
								<!-- IFRAME -->
								<iframe id="preview_iframe_{!! $resource->id !!}" src="{!! $resource->preview !!}?autostart=0" style="width:100%; height:100%; max-height: 260px; border: 1px #CCC solid;" allowfullscreen></iframe>
								<div class="btn btn-info full_screen_btn" data-iframe-id="preview_iframe_{!! $resource->id !!}">
									<i class="fa fa-television" aria-hidden="true"></i> {{ trans('text.fullscreen') }}
								</div>
							@endif
						@endif

					</div>

					<!-- right arrow -->
					<div class="col-xs-1">
						<div class="{!! $arrows_hidden !!} round_button arrow_modal text-course" data-this-modal-id="{!! $loop->index !!}" data-open-modal-id="{!! $next_modal !!}"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					</div>
				</div>
			
			</div>
		</div>
		

	</div>

@endforeach