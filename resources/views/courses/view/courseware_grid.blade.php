<!-- VIEW SELECTORS grid/list -->
<div class="row">
	<div class="col-xs-12 text-center">
		<i id="grid_view_selector" class="fa fa-th-large resource_view_selector text-course" data-id="{!! $course->id !!}" aria-hidden="true"></i>
		<i id="list_view_selector" class="fa fa-bars resource_view_selector"  data-id="{!! $course->id !!}"  aria-hidden="true"></i>
	</div>
</div>

<!-- COURSE TITLE -->
<div class="row" style="margin-bottom: 10px;">
	<div class="col-xs-12">
		<table>
			<tr>
				<td style="padding-right: 10px;">
					<div class="btn-course-small pull-left">&nbsp;</div>&nbsp;
				</td>
				<td>
					<div class="resource-title text-course">{{ $course->name }}</span>
				</td>
			</tr>
		</table>	
	</div>
</div>

<!-- COURSEWARE -->
<div class="row">

	<!-- SUBJECTS -->
	@if (isset($course->subjects_published))
		<!-- if no published subjects -->
		@if (count($course->subjects_published) == 0)
			{{ trans('text.no_subjects') }}
		@endif

		@foreach ($course->subjects_published as $subject)

			<div class="panel-group" id="accordion{!! $loop->index !!}">
				<div class="panel panel-default in">
					<!-- subject heading -->
					<a class="collapse_arrow" data-toggle="collapse" data-parent="#accordion{!! $loop->index !!}" href="#collapse{!! $loop->index !!}">
						<div class="panel-heading">
							{{ $subject->name }}
							<i class="fa fa-caret-up pull-right fa-lg" aria-hidden="true"></i>
						</div>
					</a>
					
					<!-- LECTURES -->
					<div id="collapse{!! $loop->index !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $loop->index !!}">
				        <div class="panel-body" style="padding: 0px; padding-top: 15px;">
			        		<div class="row">
			        			<div class="col-xs-12">
			        			
			      					@if (isset($subject->lectures_published))
			      						
			      						<!-- if no published lectures -->
										@if (count($subject->lectures_published) == 0)
											{{ trans('text.no_lectures') }}
										@else 
											<ul class="sortable_ul ui-sortable">
									      		@foreach ($subject->lectures_published as $lecture)
													@include('courses.view.courseware_grid_lecture', ['lecture' => $lecture, 'user_id' => $user_id])
									      		@endforeach
									      	</ul>
										@endif 
									@else 
							      		{{ trans('text.no_lectures') }}					      		
							      	@endif 
						      	</div>
						    </div>
	      				</div>
	    			</div>
	    		</div>
	    	</div>

		@endforeach 
	
	@else 
		{{ trans('text.no_subjects') }}
	@endif

</div>

<script type="text/javascript">
	$(document).ready(function(){
		lineLimit();
	});
</script>