
<!-- LECTURE HEADER -->
	<div class="row content-header">

		<!-- left button -->
	    <div class="col-xs-3">
	    	{!! left_back_button('lectures') !!}
	    </div>
	    
	    <!-- middle -->
	    <div class="col-xs-6 text-center">
	    	<h2 style="margin-bottom: 0px;">{{ trans('text.lecture') }}</h2>
	    </div>

	    <!-- right -->
	    <div class="col-xs-3">
	    	{!! view_counter_icons($lecture) !!}
	    </div>

	</div>

<!-- LECTURE TITLE -->
	<div class="row">
		<!-- title, author -->
		<div class="col-xs-12">
	        <div class="row" style="margin-bottom: 10px;">
	        	<div class="col-xs-12">
	        		<span class="title">{{ strtoupper(trans('text.title')) }}</span>
					<table style="margin-top: 5px;">
						<tr>
							<td style="padding-right: 10px;">
								<div class="btn-lecture-small pull-left">&nbsp;</div>
							</td>
							<td>
								<div class="resource-title text-lecture">{{ $lecture->name }}</span>
							</td>
						</tr>
	        		</table>
	        	</div>
	        </div>
	    </div>
	</div>

<!-- LECTURE INFO -->
	<div class="row">	

		<!-- LEFT -->
		<div class="col-xs-4">

			<!-- Cover image--> 
			{!! coverImage($lecture, "lecture_big.png") !!}

			<!-- View link --> 
			@if ($lecture->resources_published)
				<table style="width: 100%;">
					<tr>
						<td style="vertical-align: middle;">
							{{ strtoupper(trans('text.resources')) }}
						</td>
						<td>
							@foreach ($lecture->resources_published as $resource)
								@if ($resource->published)
									@if($request->user_id)
			  							<div class="round_button btn-smallest bck-course pull-right modal_url lens" style="font-size: 12px;" data-user-id={!! isset($request->user_id) ? $request->user_id : '""' !!} data-url="{!! url( '/api/resources/view_modal/' . $resource->id . '/' . $lecture->id ) !!} ">
			  								{!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
			  							</div>
			  						@else 
			  							<div class="round_button bck-course btn-smallest pull-right lens" style="font-size: 12px;" title="{{ trans('text.you_have_to_login_to_download_this_resource') }}">
			  								{!! (isset($resource->resource_type)) ? $resource->resource_type->icon : "" !!}
			  							</div>
			  						@endif
								@endif
							@endforeach
						</td>
					</tr>
				</table>
			@else 
				{{ trans('text.no_resources') }}
			@endif

			<!-- Language -->
        	{!! isset($lecture->language_id) ? titleAndTextRow('text.language', $lecture->language->name) : "" !!}

		</div>

		<!-- MIDDLE -->
		<div class="col-xs-8">
			<!-- course -->
			<div class="row padding-small">
				<div class="col-xs-12 col-md-4">
					{{ strtoupper(trans('text.course')) }}
				</div>
				<div class="col-xs-12 col-md-8">
					<a href="/courses/view/{!! $lecture->course_subject->course->id !!}">{{ $lecture->course_subject->course->name }}</a>
				</div>
			</div>

			<!-- lecture subject -->
			<div class="row padding-small">
				<div class="col-xs-12 col-md-4">
					{{ strtoupper(trans('text.course_subject')) }}
				</div>
				<div class="col-xs-12 col-md-8">
					{{ $lecture->course_subject->name }}
				</div>
			</div>

			<!-- lecture description -->
			<div class="row padding-small">
				<div class="col-xs-12 col-md-4">
					{{ strtoupper(trans('text.description')) }}
				</div>
				<div class="col-xs-12 col-md-8">
					{{ $lecture->description }}
				</div>
			</div>

			<!-- lecture content -->
			<div class="row padding-small">
				<div class="col-xs-12 col-md-4">
					{{ strtoupper(trans('text.contents')) }}
				</div>
				<div class="col-xs-12 col-md-8">
					@if (isset($lecture->contents))
						@foreach($lecture->contents as $content)
							{{ $content->text }} <br>
						@endforeach
					@endif
				</div>
			</div>

		</div>	
	</div>
