<?php
$transparent = "";
if (!$courseSubject->published) {
	$transparent = "transparent";
};
?>
<li data-subject-id='{!! $courseSubject->id !!}' class="sortable_li ul_lectures {!! $transparent !!}" id="course_subject_{!! $courseSubject->id !!}">
	<div class="row" style="margin-bottom: 5px; margin-top: 10px; " >
		<div class="col-xs-1 text-center" style="padding: 10px;">

			<!-- delete button -->
			<button data-model='course_subjects' title="{{ trans('text.delete') }}"  data-delete-id="course_subject_{!! $courseSubject->id !!}" data-id={!! $courseSubject->id !!} data-text='{{ trans("text.question_delete_course_subject") }}' class="btn btn-danger delete-resource" type="button" >
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
			<br><br>
			<!-- publish button -->
			{!! publishButton($courseSubject, "data-return-function='toggleTransparentClass' data-return-function-arguments='course_subject_" . $courseSubject->id . "'") !!}
			<br>
			<!-- order -->
			{{ $courseSubject->order }}
				
		</div>
		<div class="col-xs-11" style="padding: 0px; padding-bottom: 10px;" id="course_subject_{{ $courseSubject->id }}_collapse">

			<div class="panel-group" id="accordion_{{ $courseSubject->id }}">
				<div class="panel panel-default" style="background-color: #f2f2f2; padding-left:10px; padding-right: 10px;">

					<div class="panel-heading">
						<!-- course subject name-->
						<input style="width: 90%;" type="text" name="name" data-model='course_subjects' data-id={{ $courseSubject->id }} value="{{ $courseSubject->name }}" placeholder="{{ strtoupper(trans('text.insert_subject_title')) }}" class="form-control update_input">

						<!-- collapse header icon-->							
						<a class="collapse_arrow" data-toggle="collapse" data-parent="#accordion_{{ $courseSubject->id }}" href="#collapse{!! $courseSubject->id !!}">
							<i class="fa fa-caret-up" aria-hidden="true"></i>
						</a>							

						<!-- sorting handle -->
						<span class="movable_arrow"><i class="fas fa-arrows-alt-v" aria-hidden="true"></i></span>
					</div>

					
					<!-- collapse content -->
					<div id="collapse{!! $courseSubject->id !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $courseSubject->index !!}" style="background-color: #f2f2f2;">
						<!-- course subject -->
						<div id="course_subject_{!! $courseSubject->id !!}_lectures" style="margin-bottom: 10px; margin-top: 10px;">
							<ul id="sortable_lectures_{!! $courseSubject->id !!}" data-subject-id="{!! $courseSubject->id !!}" class="sortable_ul">
								@foreach ($courseSubject->lectures as $lecture)
									@include ('courses.edit.courseware.lecture_list_view', ['lecture' => $lecture, 'languages' => $languages])
								@endforeach
							</ul>
						</div>
						<!-- add lecture button -->
						<button type="button" class="btn bck-lecture add_lecture" data-course-subject-id="{{ $courseSubject->id }}"><i class="fa fa-plus" aria-hidden="true"></i></button>
						<span class="text-lecture">{{ strtoupper(trans('text.add_lecture')) }}</span>
						<div class="clearfix">&nbsp;</div>
					</div>

				</div>	
			</div>

		</div>
	</div>

	<script type="text/javascript">

		sort_course_subject({!! $courseSubject->id !!});
		sort_courses();

	</script>

</li>