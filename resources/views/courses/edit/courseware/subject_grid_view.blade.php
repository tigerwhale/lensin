<?php
$transparent = "";
if (!$courseSubject->published) {
	$transparent = "transparent";
};
?>

<li data-subject-id='{!! $courseSubject->id !!}' class="sortable_li {!! $transparent !!}" id="course_subject_{{ $courseSubject->id }}" style="margin-bottom: 20px;">
	<div class="row">
		<!-- LEFT SIDE -->
		<div class="col-xs-1 text-center" style="padding: 10px;">

			<!-- delete button -->
			<button data-model='course_subjects' title="{{ trans('text.delete') }}"  data-delete-id="course_subject_{{ $courseSubject->id }}" data-id={{ $courseSubject->id }} data-text='{{ trans("text.question_delete_course_subject") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
			<br><br>
			{!! publishButton($courseSubject, "data-return-function='toggleTransparentClass' data-return-function-arguments='course_subject_" . $courseSubject->id . "'") !!}

			<br>
		</div>

		<!-- RIGHT SIDE -->
		<div class="col-xs-11" style=" padding: 0px; padding-bottom: 10px;" id="course_subject_{{ $courseSubject->id }}_collapse">

			<div class="panel-group" id="accordion_{{ $courseSubject->id }}">
				<div class="panel panel-default" >

					<div class="panel-heading">
						<!-- course subject name-->
						<input style="width: 90%;" type="text" name="name" data-model='course_subjects' data-id={{ $courseSubject->id }} value="{{ $courseSubject->name }}" placeholder="{{ strtoupper(trans('text.insert_subject_title')) }}" class="form-control update_input">

						<!-- collapse header icon-->
						<a class="collapse_arrow" data-toggle="collapse" data-parent="#accordion_{{ $courseSubject->id }}" href="#collapse{!! $courseSubject->id !!}">
							<span class="collapsable_arrow" href="#collapse{!! $courseSubject->id !!}">
								<i class="fa fa-caret-up" aria-hidden="true"></i>
							</span>
						</a>

						<!-- sorting handle -->
						<span class="movable_arrow"><i class="fas fa-arrows-alt-v" aria-hidden="true"></i></span>
					</div>

					<!-- collapse content -->
					<div id="collapse{!! $courseSubject->id !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $courseSubject->index !!}" style="background-color: #f2f2f2;">

						<div class="row">
							<div class="col-xs-12">

								<div id="sortable_lectures_{!! $courseSubject->id !!}" data-subject-id="{!! $courseSubject->id !!}" class="row sortable_ul ul_lectures" style="padding-left: 15px; padding-right: 15px;">
									@foreach ($courseSubject->lectures as $lecture)
										@include ('courses.edit.courseware.lecture_grid_view', ['lecture' => $lecture])
									@endforeach

									<div class="col-xs-3 datatable_grid_div">
										<!-- lecture image -->
										<div class="lecture_grid_image_add">
											<img src="/images/resource/lecture_big.png">
											<br>
											<button type="button" class="btn bck-lecture shadow add_lecture_grid" data-course-subject-id="{!! $courseSubject->id !!}"><i class="fa fa-plus" aria-hidden="true"></i></button>
											<br>
										 	<span class="text-lecture">{{ strtoupper(trans('text.add_lecture')) }}</span>
										</div>
									</div>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript">

		function sort_course_subject_{!! $courseSubject->id !!}() {

			$( "#course_subject_{!! $courseSubject->id !!}_collapse" ).accordion({
		    	header: "> span",
		    	handle: ".movable_arrow",
		    	collapsible: true,
		    	heightStyle: "content",
		    	active: false
		  	})

			$( "#sortable_lectures_{!! $courseSubject->id !!}" ).sortable({
				connectWith: ".ul_lectures",

				receive: function( event, ui ){

					var recieverObject = $(this);
					var token = $("meta[name='csrf-token']").attr("content");
					var lecture_id = $(ui.item).data('lecture-id');
					var value = $(this).data('subject-id');

					// send to server
					$.post({
						url: '/lectures/update_data',
						data: {
							'_token': token,
							'value': value,
							'id': lecture_id,
							'name': 'course_subject_id',
						},
						success: function(data, status) {

							// make subject id-s
							var lectures = [];

							recieverObject.children('div.datatable_grid_div').each(function(i){
								// console.log($(this).data('lecture-id'))
								lectures.push($(this).data('lecture-id'));
							});

							// send to server
							$.post({
								url: '/lectures/update_sort',
								data: {
									'_token': token,
									'lectures': lectures
								},
								success: function(data, status) {
									console.log(lectures);
								},
								error: function(xhr, desc, err) {
									console.log(xhr);
									console.log("Details: " + desc + "\nError:" + err);
								}
							});


						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});
				},
				stop: function( event, ui ) {
					// get the token
					var token = $("meta[name='csrf-token']").attr("content");
					// make subject id-s
					var lectures = [];

					$(this).children('div.datatable_grid_div').each(function(i){
						// console.log($(this).data('lecture-id'))
						lectures.push($(this).data('lecture-id'));
					});

					// send to server
					$.post({
						url: '/lectures/update_sort',
						data: {
							'_token': token,
							'lectures': lectures
						},
						success: function(data, status) {
							console.log(lectures);
						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});
			  	}
			});
		};

		sort_course_subject_{!! $courseSubject->id !!}();
	</script>

</li>


