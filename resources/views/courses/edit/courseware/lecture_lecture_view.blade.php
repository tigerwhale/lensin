<li class="ui-state-default sortable_li" data-lecture-id='{!! $lecture->id !!}'>

	<div class="row lecture_div"  id="lecture_{{ $lecture->id }}">
		<div class="col-xs-1 text-center" style="padding: 10px;">

			<!-- lecture img -->
			<img src="/images/resource/lecture_big.png"/>

			<!-- delete button -->
			<button data-model='lectures' data-delete-id="lecture_{{ $lecture->id }}" data-id={{ $lecture->id }} data-text='{{ trans("text.question_delete_lecture") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
			<br>
			
			<!-- publish button -->
			<?php $icon = ($lecture->published == 1) ? "far fa-eye" : 'far fa-eye-slash'; ?>
			<div class="btn
				<?php if ($lecture->published == 1)
						echo 'bck-lens';
					else echo 'bck-lens-unpublished'; ?>
				publish_resource" data-url="/lectures/publish" data-id="{!! $lecture->id !!}" style="margin-top: 10px;">
				<i class="{!! $icon !!}" aria-hidden="true"></i>
			</div>
			<br>
			
			<!-- order -->
			{{ $lecture->order }}
				
		</div>

		<div class="col-xs-11" padding: 10px">
			<span class="movable_arrow_lecture"><i class="fas fa-arrows-alt-v" aria-hidden="true"></i></span>
			<!-- lecture name-->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.title') }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="name" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->name }}" placeholder="{{ strtoupper(trans('text.insert_lecture_title')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture author -->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.authors') }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="author" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->author }}" placeholder="{{ strtoupper(trans('text.author')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture year -->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.year') }}
				</div>
				<div class="col-xs-9">
					<input type="text" name="year" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->year }}" placeholder="{{ strtoupper(trans('text.year')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture content -->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.contents') }}
				</div>
				<div class="col-xs-9" id="lecture_{{ $lecture->id }}_content">
					<script>lecture_content_load({{ $lecture->id }}); </script>
				</div>
			</div>		


			<!-- cover image -->
			<div class="row padding-small hover">
				<div class="col-xs-3">
					{{ trans('text.cover_image') }}
				</div>
				<div class="col-xs-5">
					<div class="dropzeon dropzone_div" id="lecture_upload_image_{{ $lecture->id }}" class="image" >
						{{ trans('text.click_to_select_file') }}
					</div>
				</div>
				<div class="col-xs-4 text-right" id="lecture_image_{{ $lecture->id }}">
					@if ($lecture->image)
						<img src="{{ $lecture->image }}" style="max-height: 150px; max-width: 150px;">
					@endif
				</div>				
			</div>	


			<!-- resources -->
			<div class="row">
				<div class="col-xs-3">
					{{ trans('text.resources') }}
				</div>
				<div class="col-xs-9">
					<div id="lecture_{{ $lecture->id }}_resources" style="width: 100%;">
						@include('courses.resource_edit', ['lecture' => $lecture])
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>


<script type="text/javascript">
	$( document ).ready(function() {
		Dropzone.autoDiscover = false;

		// UPLOAD COVER IMAGE
		$("#lecture_upload_image_{{ $lecture->id }}").dropzone({
			url: "/lectures/upload_cover_image/{{ $lecture->id }}",
			paramName: "image",
			previewsContainer: false,
			uploadMultiple: false,
			parallelUploads: 1,		
			
			sending: function(file, xhr, formData) {
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
		
			},

			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#lecture_upload_image_{{ $lecture->id }}").html(parseInt(progress) + "%");
			},

			success: function(file, responce) {

				// change image
				if (responce != "upload_error") {
					$('#lecture_upload_image_{{ $lecture->id }}').html('{{ trans('text.click_to_select_file') }}');
					$('#lecture_image_{{ $lecture->id }}').html('<img src=' + responce +' style="max-height: 150px; max-width: 150px;">');

				} else {
					alert ("{{ trans('text.upload_error') }}");
				}
			}
		});


		// DROPZONE - upload resouce and open resource modal
		$("div#resource_upload_{{ $lecture->id }}").dropzone({ 

			url: "/resources/create/{{ $lecture->id }}",
			paramName: "resource_file",
			sending: function(file, xhr, formData) {
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
				
				// if extension is zip, ask if the upload is camtasia
				var extension = file.name.substring(file.name.length -3);
				// console.log(file);
				if (extension == "zip") {

					if (confirm( "{{ trans('resources.question_is_this_camtasia') }}" )) {
			    		formData.append("zip", "1");
			        	// console.log("zip");	
			    	}
				}
			},
			parallelUploads: 1,
			previewsContainer: false,
			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#resource_upload_{{ $lecture->id }}").html(parseInt(progress) + "%");
			},
			success: function(file, responce) {
				// id of the parrent lecture
				var lecture_id = {{ $lecture->id }};

				//add the row and open the modal
				if (responce != "upload_error") {

					$('#lecture_' + lecture_id + "_resources").append(responce[1]);
					edit_resource(responce['id']);

				} else {

					alert ("{{ trans('text.upload_error') }}");
				}

				// Return text to "click here..."
				$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.click_to_select_file') }}");
				
			},
			error: function (file, responce){
				$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			}

		});



	// COVER IMAGE
	var token = $("meta[name='csrf-token']").attr("content"); 
  	var progressBar{{ $lecture->id }} = document.getElementById('progressBar_{{ $lecture->id }}');
  	var progressOuter{{ $lecture->id }} = document.getElementById('progressOuter_{{ $lecture->id }}');


	});

</script>

	</div>
</li>	