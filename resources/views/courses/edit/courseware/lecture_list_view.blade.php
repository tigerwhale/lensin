<?php
$transparent = "";
if (!$lecture->published) {
	$transparent = "transparent";
};
?>
<li class="ui-state-default sortable_li {!! $transparent !!}" id="lecture_{!! $lecture->id !!}" data-lecture-id='{!! $lecture->id !!}'>
	<div class="panel-group">
		<div class="panel panel-default">
		    
		    <div class="panel-heading" style="background-color: #CCCCCC">
		    	<div class="row">
		    		<div class="col-xs-9">
		    			<table>
		    				<tr>
			    				<td>
			    					<span class="btn-lecture-small pull-left" style="margin-right: 10px;"></span>
			    				</td>
			    				<td style="width:100%;">
			    					<input type="text" name="name" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->name }}" placeholder="{{ strtoupper(trans('text.insert_lecture_title')) }}" class="form-control update_input">
			    				</td>
			    			</tr>
		    			</table>
		    		</div>
		    		<div class="col-xs-2">
						{!! publishButton($lecture, "data-return-function='toggleTransparentClass' data-return-function-arguments='lecture_" . $lecture->id . "'") !!}
		    			<button data-model='lectures' title="{{ trans('text.delete') }}"  data-delete-id="lecture_{{ $lecture->id }}" data-id={{ $lecture->id }} data-text='{{ trans("text.question_delete_lecture") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
					
		    		</div>
		    		<div class="col-xs-1 text-right">
						<a href="#lecture_colapse_{!! $lecture->id !!}" class="collapse_arrow" data-toggle="collapse" style="padding-right: 10px;">
							<span class="text-course" style="width:30px;"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
						</a>
		    			<span class="movable_arrow_lecture"><i class="fas fa-arrows-alt-v" aria-hidden="true"></i></span>
		    		</div>
		    	</div>
		    </div>
			
			<div class="panel-collapse collapse in" id="lecture_colapse_{{ $lecture->id }}">
			
				<div class="row lecture_div  text-center" role="tabpanel" id="lecture_{{ $lecture->id }}" style="margin:0px;">
					<div class="col-xs-4" >

						<!-- cover image --> 
						{{ strtoupper(trans('text.cover_image')) }}<br>
						<div class="row padding-small">
							<div class="col-xs-12" id="cover_image_container_{{ $lecture->id }}">
								{!! coverImageEdit($lecture) !!}
							</div>
						</div>	

						<!-- lecture author -->
						{{ strtoupper(trans('text.authors')) }}<br>
						<input type="text" name="author" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->author }}" placeholder="{{ strtoupper(trans('text.author')) }}" class="form-control update_input">

						<!-- lecture language -->
						{{ strtoupper(trans('text.language')) }}<br>
						{!! Form::select('language_id', $languages, $lecture->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'lectures', 'data-id'=> "$lecture->id", 'placeholder' => trans('text.select_language') ]) !!}

						<!-- lecture year -->
						{{ strtoupper(trans('text.year')) }}<br>
						<input type="text" name="year" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->year }}" placeholder="{{ strtoupper(trans('text.year')) }}" class="form-control update_input">

						<!-- lecture content -->
						{{ strtoupper(trans('text.contents')) }}<br>
						<div class="row padding-small hover">
							<div class="col-xs-12" id="lecture_{{ $lecture->id }}_content">
								<script>lecture_content_load({{ $lecture->id }}); </script>
							</div>
						</div>		

					</div>

					<div class="col-xs-8">
						<!-- resources -->
						{{ strtoupper(trans('text.resources')) }}<br>
						
						<div class="row">
							<div class="col-xs-12">
								<div id="lecture_{{ $lecture->id }}_resources" style="width: 100%;">
									@include('courses.edit.modals.resource_list_for_lecture_modal', ['lecture' => $lecture])
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>	

<script type="text/javascript">
	$( document ).ready(function() {
		Dropzone.autoDiscover = false;

			// DROPZONE - upload resouce and open resource modal
		$("div#resource_upload_{{ $lecture->id }}").dropzone({ 
			url: "/resources/create_and_upload_files/{{ $lecture->id }}",
			paramName: "resource_file",
			previewsContainer: false,
			uploadMultiple: true,
			parallelUploads: 100,

			sendingmultiple: function(files, xhr, formData){
			
				var paths = [];
				files.forEach( function(item, index){
					paths.push(item.fullPath);
				})
				// console.log('----' + paths);
				formData.append( "paths", paths);
				formData.append( "_token", $("meta[name='csrf-token']").attr('content'));
				
			},

			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#resource_upload_{{ $lecture->id }}").html(parseInt(progress) + "%");
			},

			successmultiple: function(file, responce) {

				var responce_json = JSON.parse(responce);

				// If multiple
				if (responce_json[0] == "multiple") {

					var files = responce_json[2].split(",");
					var file_prompt = [];
					files.forEach( function(item, index){
						file_prompt.push({text: item, value: item, className: "select_preview_file"});
					})
					// open file choise
					bootbox.prompt({
					    title: "Select file for preview:",
					    inputType: 'select',
					    inputOptions: file_prompt,
					    closeButton: false,
					    callback: function (result) {
			        		$.ajax({
								data: { 
								    '_token': $("meta[name='csrf-token']").attr("content"),
								    'preview': result,
								    'id' : responce_json[1],
								    'name': 'preview',
								    'value': result
								},
								url: '/resources/update_data',
								type: 'POST',
								success: function(response) {
								    openModalFromUrl('/resources/course_edit_modal/' + responce_json[1]);
								},
								error: function(response){
								    bootbox.alert (response);
								}
							});

					    }
					});

				} 
				// If single
				else if (responce_json[0] == "single")			
				{
					// Open resource edit modal
					lecture_resources_load_{!! $lecture->id !!}();
					openModalFromUrl('/resources/course_edit_modal/' + responce_json[1], );

				} else 
				{
					bootbox.alert ("{{ trans('text.upload_error') }}");
				}	

				// Return text to "click here..."
				$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.click_to_select_file') }}");
				
			},

			// error return function
			error: function (file, responce){
				bootbox.alert ("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
				//$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			},

			init: function (){
				myDropzone = this;
			}
		});
		// COVER IMAGE
		var token = $("meta[name='csrf-token']").attr("content"); 
	  	var progressBar{{ $lecture->id }} = document.getElementById('progressBar_{{ $lecture->id }}');
	  	var progressOuter{{ $lecture->id }} = document.getElementById('progressOuter_{{ $lecture->id }}');

	});

</script>