<?php
    $transparent = "";
    if (!$lecture->published) {
        $transparent = "transparent crossed-eye-bck";
    };
?>
<div class="col-xs-3 datatable_grid_div {!! $transparent !!}"
	 data-lecture-id="{!! $lecture->id !!}"
	 id="lecture_{!! $lecture->id !!}"
	 style="border-top: 1px solid transparent;">
	<!-- HOVER DIV -->
	<div class="row datatable_grid_div_bottom_cover" style="color: white;">

		<!-- modify lecture -->
		<div class="col-xs-12 text-center" style="padding-top: 50px;">

			<button type="button"
					title="{{ trans('text.modify') }}"
					class="btn bck-lens shadow modal_url"
					data-close-function="lecture_{!! $lecture->id !!}"
					data-url="/lectures/edit_modal/{!! $lecture->id !!}">
				<i class="fas fa-pencil-alt" aria-hidden="true"></i>
			</button>

			{!! publishButton($lecture, "data-return-function='toggleTransparentAndEyeClass' data-return-function-arguments='lecture_" . $lecture->id . "'") !!}

			<button data-model='lectures'
					title="{{ trans('text.delete') }}"
					data-delete-id="lecture_{!! $lecture->id !!}"
					data-id="{!! $lecture->id !!}"
					data-text='{{ trans("text.question_delete_lecture") }}'
					class="btn btn-danger delete-resource"
					type="button" >
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>

		</div>
	</div>

	<?php
        $background_image = $lecture->image ? $lecture->image : '/images/resource/lecture_big.png';
        $lecture_grid_image_class = $lecture->image ? 'lecture_grid_image' : 'lecture_grid_no_image';
    ?>
	<div class="{!! $lecture_grid_image_class !!}"	style="background-image: url('{{ $background_image }}');" ></div>

	<div class="row">
		<div class="col-xs-12 lecture_grid">

			<!-- TEXT DIV -->
			<div class="lecture_grid_text" data-id="{!! $lecture->id !!}">
				<div class="row">
					<!-- lecture name -->
					<div class="col-xs-12 line-limit" id="lecture_{!! $lecture->id !!}_name" style="height: 100px;">
						{{ $lecture->getName() }}
					</div>
				</div>
				<div class="row">
					<!-- lecture icon -->
					<div class="col-xs-3 text-left">
						<span class="btn-lecture-small pull-left"></span>
					</div>
					<!-- Date and published -->
					<div class="col-xs-6 text-center grid_date" style="margin-top: 10px;">
						{!! strtoupper($lecture->created_at->format("d M")) !!}
					</div>
					<div class="col-xs-3">
						<img src='{!! url("/images/server/logo.png") !!}' style="width: 30px;" class='table_view_image'>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- sorting handle -->
	<span class="movable_arrow_lecture_grid "><i class="fas fa-arrows-alt" aria-hidden="true"></i></span>

	<script type="text/javascript">
		function lecture_grid_{!! $lecture->id !!}()
		{
			lecture_grid_load({!! $lecture->id !!});
		}

		$(document).ready(function(){
			lineLimit();
		});
	</script>
</div>