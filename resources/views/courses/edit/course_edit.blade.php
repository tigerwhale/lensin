@extends('layouts.resource')

@section('content')

	<div class="container">

        {!! backendHeader('course', 'courses', strtoupper(trans('text.course')), 'course')  !!}

		<!-- HEADER BUTTONS -->
		<div class="row">
			<div class="col-xs-12 text-center">
				<ul id="content_tabs" class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#course_info">{{ strtoupper(trans('text.info')) }}</a></li>
				    <li class=""><a data-toggle="tab" href="#course_courseware" id="open_courseware">{{ strtoupper(trans('text.courseware')) }}</a></li>
				</ul>
				<script>
					$('#content_tabs a').click(function (e) {
					  	e.preventDefault()
					  	$(this).tab('show')
					})
				</script>		
			</div>
		</div>
		
		<!-- COURSE TITLE  -->
		<div class="row" style="margin-top: 10px">
			<div class="col-xs-2">
				<span class="btn-course-small pull-left" style="margin-right: 10px;"></span> 
				<span style="line-height: 30px;">{{ strtoupper(trans('courses.course_title')) }}</span>
			</div>
			<div class="col-xs-10">
				<input type="text" name="name" data-model='courses' data-id={{ $course->id }} value="{{ $course->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
			</div>
		</div>


		<div class="row tab-content">
			<!-- COURSE INFO -->
			<div role="tabpanel" class="tab-pane active col-xs-12" id="course_info">

				<div class="row" style="margin-top: 20px;">
					
					<div class="col-xs-4 text-center">
					
						<!-- cover image -->
						<div class="row padding-small">
							<div class="col-xs-12" id="cover_image_container_{!! $course->id !!}">
								{!! coverImageEdit($course) !!}
							</div>
						</div>							
						
						<!-- SCHOOL -->
						{{ trans('text.school') }}
						<input list="schools" type="text" id="school" name="school" data-model='courses' data-id={{ $course->id }} value="{{ $course->school }}" class="form-control update_input" required>
						<datalist id="schools">
							@foreach (list_schools() as $school) 
								<option value="{{ $school->school }}">
							@endforeach
						</datalist>

						<!-- YEAR -->
						{{ trans('text.year') }}
						<input list="years" type="text" id="year" name="year" data-model='courses' data-id={{ $course->id }} value="{{ $course->year }}" class="form-control update_input" required>
						<datalist id="years">
							@foreach (range(date('Y'), 1970) as $year) 
								<option value="{{ $year }}">
							@endforeach
						</datalist>

						<!-- COUNTRY -->
						{{ trans('text.country') }}
						{!! Form::select('country_id', $countries, $course->country_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'courses', 'data-id'=> "$course->id" ]) !!}

						<!-- LANGUAGE -->
						{{ trans('text.language') }}
						{!! Form::select('language_id', $languages, $course->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'courses', 'data-id'=> "$course->id", 'placeholder' => trans('text.select_language')]) !!}

						<!-- TYPE -->
						{{ trans('text.type') }}
						<input type="text" id="type" name="type" data-model='courses' data-id={{ $course->id }} value="{{ $course->type }}" class="form-control update_input">
						<datalist id="types">
							@foreach (DB::table('courses')->select('type')->distinct()->get() as $type) 
								<option value="{{ $type->type }}" />
							@endforeach
						</datalist>				

						<!-- DURATION -->
						{{ trans('text.duration') }}
						<input type="text" id="duration" name="duration" data-model='courses' data-id={{ $course->id }} value="{{ $course->duration }}" class="form-control update_input">
						
					</div>

					<!-- SYLLABUS -->
					<div class="col-xs-5 text-center">
						{{ strtoupper(trans('text.syllabus')) }}<br>
						<textarea name="description" class="form-control update_input"  data-model='courses' data-id={{ $course->id }} placeholder="{{ trans('text.insert_description') }}" style="height: 300px;">{{ $course->description }}</textarea>
					</div>

					<!-- AUTHORS -->
					<div class="col-xs-3 text-center">
						{{ strtoupper(trans('text.authors')) }}
						<br>
						<!-- add author button -->
						<button type="button" title="{{ trans('course.add_teacher') }}" class="btn bck-course modal_url" data-url="/courses/add_author_modal"><i class="fa fa-plus" aria-hidden="true"></i></button>
						
						<div class="text-center" style="margin-top: 10px;">
							<!-- list of authors -->
							<div id="authors_list"></div>
						</div>

					</div>
				</div>
			</div>

			<!-- COURSEWARE -->
			<div role="tabpanel" style="margin-top: 10px;" class="tab-pane col-xs-12" id="course_courseware">

				<!-- VIEW TYPE SELECTOR --> 
				<div class="row">
					<div class="col-xs-12 text-center">
						<i id="grid_view_selector" class="fa fa-th-large resource_view_selector" aria-hidden="true"></i>
						<i id="list_view_selector" class="fa fa-bars resource_view_selector" aria-hidden="true"></i>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12" id="courseware_data">
					</div>
				</div>

				<div class="row" id="wait">
					<div class="col-xs-12">
						<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
					</div>
				</div>	
				
			</div>	

		</div>
	</div>

	@include('courses.edit.course_edit_js')

@endsection