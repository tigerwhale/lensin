<!-- CREATE MODE -->
<div class="row bck-course resource_header">
	<!-- left text -->
	<div class="col-xs-4 text-left resource_header_title">
		<table>
			<tr>
				<td class="edit-mode-header-tr">
					+
				</td>
				<td style="vertical-align: middle; padding-left: 10px;">
					{{ strtoupper(trans('text.create_mode')) }}
				</td>
			</tr>
		</table>
	</div>

	<!-- middle buttons -->
	<div class="col-xs-4">
		<table style="margin-left: auto; margin-right: auto; margin-top: 5px;">
			<tr>
				<!-- back -->
				<td style="padding: 5px;">
					<a href="/courses/modify/">
						<button class="btn btn-danger" type="button">
							<i class="fas fa-arrow-left" aria-hidden="true"></i>
							{{ strtoupper(trans('text.back')) }}	
						</button>
						
					</a>
				</td>
				<!-- confirm -->
				<!-- <td style="padding: 5px;">
					<a href="/courses/edit/{!! $course->id !!}">
						<button class="btn btn-success" type="button">
							<i class="fa fa-check" aria-hidden="true"></i>
							{{ strtoupper(trans('text.confirm')) }}
						</button>
						
					</a>
				</td> -->
				<!-- cancel -->
				<td style="padding: 5px;">
					<a href="/courses/delete/{!! $course->id !!}?cancel=1">
						<button class="btn btn-danger back-button" type="button">
							<i class="fa fa-times" aria-hidden="true"></i> 
							{{ strtoupper(trans('text.cancel')) }}
						</button>
						
					</a>
				</td>
			</tr>
		</table>			
	</div>
	
	<!-- right text -->
	<div class="col-xs-4 text-right resource_header_title">
		<table style="float: right;">
			<tr>
				<td style="vertical-align: middle; padding-right: 10px;">
					{{ strtoupper(trans('text.courses')) }}
				</td>
				<td class="edit-mode-header-tr">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				</td>
			</tr>
		</table>
		
	</div>			
</div>