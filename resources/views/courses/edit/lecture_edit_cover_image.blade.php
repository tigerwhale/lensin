@if ($lecture->image)
	<div class="image_container">						
		<div class="image_container_inner">
			<img src="{{ $lecture->image }}" style="width:100%;">
			<!-- bottom popup -->
			<div class="bottom_cover bck-lecture">
				<!-- remove image button -->
				<button class="btn btn-danger remove_lecture_image_button" data-lecture-id="{!! $lecture->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
			</div>
		</div>
	</div>

@else 
	<div class="image_container">	
		<div class="dropzone_div lecture_upload_cover_{!! $lecture->id !!}" id="lecture_upload_cover_{!! $lecture->id !!}" data-id="{{ $lecture->id }}">
			{{ trans('text.click_to_select_image') }}
		</div>
	</div>

	<script>
		$(".lecture_upload_cover_" + {!! $lecture->id !!}).dropzone({
			url: "/lectures/upload_cover_image/{!! $lecture->id !!}",
			paramName: "image",
			previewsContainer: false,
			uploadMultiple: false,
			parallelUploads: 1,		
			sending: function(file, xhr, formData) {
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') )
				// console.log("upload slike za {!! $lecture->id !!}");
			},
			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#lecture_upload_image_" + $(this).data("id")).html(parseInt(progress) + "%");
			},
			success: function(file, responce) {
				// change image
				if (responce != "upload_error") {
					loadLectureImageForEdit({!! $lecture->id !!});
				} else {
					alert ("{{ trans('text.upload_error') }}");
				}
			}
		});
	</script>
@endif