<!-- MODIFY MODE -->
<div class="row bck-course resource_header">
	<!-- left text -->
	<div class="col-xs-4 text-left resource_header_title">
		<table>
			<tr>
				<td class="edit-mode-header-tr">
					<i class="fas fa-pencil-alt" aria-hidden="true"></i>
				</td>
				<td style="vertical-align: middle; padding-left: 10px;">
					{{ strtoupper(trans('text.modify_mode')) }}
				</td>
			</tr>
		</table>		
	</div>

	<!-- middle buttons -->
	<div class="col-xs-4">
		<table style="margin-left: auto; margin-right: auto; margin-top: 5px;">
			<tr>
				<td style="padding: 5px;">
					<a href="{!! url('/courses/modify') !!}">
						<button class="btn btn-danger" type="button" title="{{ strtoupper(trans('text.back')) }}">
							<i class="fas fa-arrow-left" aria-hidden="true"></i>
						</button>
					</a>
				</td>
			</tr>
		</table>			
	</div>
	
	<!-- right text -->
	<div class="col-xs-4 text-right resource_header_title">
		<table style="float: right;">
			<tr>
				<td style="vertical-align: middle; padding-right: 10px;">
					{{ strtoupper(trans('text.courses')) }}
				</td>
				<td class="edit-mode-header-tr">
					<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				</td>
			</tr>
		</table>		
	</div>			
</div>
