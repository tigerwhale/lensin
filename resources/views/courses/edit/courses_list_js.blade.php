<script type="text/javascript">
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    // DATATABLE
    var table = $('#datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            cache:true,
            data: {
                '_token': token
            },
            url : '/courses/datatable_list',
            rowId: 'id',
        },

        "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "author" },
            { "data": "country" },
            { "data": "year" },
            { "data": "language" },
            { "data": "published" }
        ],         
        
        "aoColumnDefs": [
        {
            "mRender": function ( data, type, row ) {
                return "<div class='text-center' style='width:100%'><img src='/images/resource/" + row[6] + ".png' class='table_view_image'><br><img src='/images/platform/" + row[7] + ".png' class='table_view_image platform'></div>";
            },
            "aTargets": [ 0 ], 

            "mRender": function ( data, type, row ) {
                
                var modify = '<a title="{{ trans('text.modify') }}" href="/courses/edit/' + row['id'] + '"><div class="btn bck-lens"><i class="fas fa-pencil-alt" aria-hidden="true"></i></div></a> ';
                var publish = publishButton(row['published'], 'course', row['id'], "{{ trans('text.published_click_to_unpublish') }}", "{{ trans('text.not_published_click_to_publish') }}")
                var delete_button = '<div class="btn btn-danger delete_course" title="{{ trans('text.delete') }}" data-id="' +  row['id'] + '"><i class="fa fa-times" aria-hidden="true"></i></div> ';

                return modify + publish + delete_button;
            },
            "aTargets": [ 6 ]
        }]
    });


    // ROW HOVER
    $("table tbody tr").hover(function(event) {
        $(".drawer").show().appendTo($(this).find("td:first"));
    }, function() {
        $(this).find(".drawer").hide();
    });

    // DELETE COURSE BUTTON
    $('body').on('click', '.delete_course', function() {
        var id = $(this).data('id');
        // confirm the delete
        bootbox.confirm("{{ trans('text.question_delete_course') }}", function(result){ 
            // if OK, delete the course
            if (result) {

                 $.ajax({
                    data: { 
                        '_token': token,
                    },
                    url: '/courses/delete/' + id,
                    type: 'GET',
                    success: function(response) {
                        table.ajax.reload();
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
               });
            }
        });
    });


</script>