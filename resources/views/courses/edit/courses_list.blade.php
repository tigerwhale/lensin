@extends('layouts.resource')

@section('content')

    <div class="container">

        {!! backendHeader('course', 'courses', strtoupper(trans('text.course')), 'course', true)  !!}
      
    	<div class="row">
    		<div class="col-xs-12 text-center">		
    	        <!-- DATATABLE SEARCH -->
    		    <table class="display" id="datatable">
    		        <thead>
    		            <tr>
    		                <th></th>
    		                <th>{{ trans('text.title') }}</th>
    		                <th>{{ trans('text.authors') }}</th>
    		                <th>{{ trans('text.country') }}</th>
    		                <th>{{ trans('text.year') }}</th>
    		                <th>{{ trans('text.language') }}</th>
    		                <th style="min-width: 120px;"></th>
    		            </tr>
    		        </thead>
    		    </table>
    		</div>
    	</div>
    </div>

@include('courses.edit.courses_list_js')

@endsection