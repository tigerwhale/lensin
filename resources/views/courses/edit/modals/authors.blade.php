<div class="modal-dialog modal-lg">
	<div class="modal-content">
		
		<!-- Modal content-->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('text.select_author') }}</h4>
		</div>

		<div class="modal-body">
			<!-- Modal content-->
			<table class="display" id="authors_list_datatable" style="width: 100%">
				<thead>
					<th>{{ trans('text.name') }}</th>
					<th>{{ trans('text.last_name') }}</th>
					<th>{{ trans('text.school') }}</th>
					<th></th>
				</thead>
			</table>
		</div>

	</div>
</div>

<script type="text/javascript">

	// DATATABLE
    var authors_list_datatable = $('#authors_list_datatable').DataTable({
        "processing": true,
        "stateSave": true, 
        "dom": '<"top"lf>rt<"bottom"ip><"clear">',
        "ajax": {
            "cache": true,
            "url": '/users/list',
            "rowId": 'id',
        },
        "columns": [
            { "data": "name" },
            { "data": "last_name" },
            { "data": "school" },
            { "data": "id" },
        ],
		"aoColumnDefs": [
        {
            "mRender": function ( data, type, row ) {
                return "<button class='btn btn-success add_teacher' type='button' data-author-id='" + row['id'] + "'><i class='fa fa-plus' aria-hidden='true'></i></button>";
            },
            "aTargets": [ 3 ]
        },
     	]
    }); 

</script>