<div class="modal-dialog modal-lg" id="apiModal">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title pull-left">{{ trans('text.lecture') }}</h4>
			<button class="btn btn-success pull-right setPublishedStatus" style="margin-left: 10px" data-status="1" data-dismiss="modal">{{ trans('text.save_and_publish') }}</button>
			<button class="btn btn-danger pull-right setPublishedStatus" data-status="0" data-dismiss="modal">{{ trans('text.save_and_unpublish') }}</button>
		</div>
		<div class="modal-body">

			<!-- lecture name-->
			<div class="row padding-small  hover">
				<div class="col-xs-2">
					{{ trans('text.title') }}
				</div>
				<div class="col-xs-10">
					<input id="lecture_name_modal" type="text" name="name" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->name }}" placeholder="{{ strtoupper(trans('text.insert_lecture_title')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture author -->
			<div class="row padding-small hover">
				<div class="col-xs-2">
					{{ trans('text.authors') }}
				</div>
				<div class="col-xs-10">
					{!! inputCombobox("author", "lectures", $lecture->id, $lecture->author, \App\User::all('id', 'name', 'last_name')->pluck('full_name'), trans('text.author')) !!}
				</div>
			</div>

			<!-- lecture year -->
			<div class="row padding-small hover">
				<div class="col-xs-2">
					{{ trans('text.year') }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="year" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->year }}" placeholder="{{ strtoupper(trans('text.year')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture length -->
			<div class="row padding-small hover">
				<div class="col-xs-2">
					{{ trans('text.length') }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="length" data-model='lectures' data-id={{ $lecture->id }} value="{{ $lecture->length }}" placeholder="{{ strtoupper(trans('text.length')) }}" class="form-control update_input">
				</div>
			</div>

			<!-- lecture language -->
			<div class="row padding-small hover">
				<div class="col-xs-2">
					{{ trans('text.language') }}
				</div>
				<div class="col-xs-10">
					{!! Form::select('language_id', $languages, $lecture->language_id, ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'lectures', 'data-id'=> $lecture->id, 'placeholder' => trans('text.select_language') ]) !!}
				</div>
			</div>


			<!-- lecture contents -->
			<div class="row padding-small  hover">
				<div class="col-xs-2">
					{{ trans('text.contents') }}
				</div>
				<div class="col-xs-10" id="lecture_{{ $lecture->id }}_content">
					<script> lecture_content_load({{ $lecture->id }}); </script>
				</div>
			</div>

			<!-- cover image -->
			<div class="row padding-small">
				<div class="col-xs-2">
					{{ trans('text.cover_image') }}
				</div>
				<div class="col-xs-4" id="cover_image_container_{!! $lecture->id !!}">
					{!! coverImageEdit($lecture) !!}
				</div>
			</div>

			<!-- resources -->
			<div class="row padding-small">
				<div class="col-xs-2">
					{{ trans('text.resources') }}
				</div>
				<div class="col-xs-10">
					<!-- list of resources -->
					<div id="lecture_{!! $lecture->id !!}_resources" style="width: 100%;"></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>§
</div>

<script type="text/javascript">
	lecture_resources_load({!! $lecture->id !!});

	$('.setPublishedStatus').click(function () {
		let status = $(this).data('status');
		$.post({
			data: {
				'_token': token,
				'status': status,
			},
			url: '/lecture/setPublishedStatus/{{ $lecture->id }}',
			success: function(data) {
				console.log(data);
				//lecture_resources_load({!! $lecture->id !!});
				loadGridView();
			}
		});
	});


	$('.closeModal').click(function () {
		reloadTable();
	});

	// On modal close
	$('body').on('hidden.bs.modal', '#modal', function(){
		var token = $("meta[name='csrf-token']").attr("content");
		$.post({
			data: {
				'_token': token,
			},
			url: '/lectures/grid_view_edit/{{ $lecture->id }}',
			success: function(data) {
				$('[data-lecture-id="{{ $lecture->id }}"]').replaceWith(data);
			}
		});
	});
</script>