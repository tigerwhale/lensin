<div class="modal-dialog modal-lg resource_modal_{{ $resource->id }}">

	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="text-course modal-title">{{ trans('courses.edit_resource') }}</h4>
		</div>

		<div class="modal-body">
			<input type="hidden" id="resource_modal_id" value="{{ $resource->id }}">
			<!-- NAME -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.name') }}
				</div>
				<div class="col-xs-10">
					<input id="resource_modal_name" type="text" name="name" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->name }}" class="form-control update_input" required>
				</div>
			</div>

			<!-- author -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.author') }}
				</div>
				<div class="col-xs-10">
					{!! inputCombobox("author", "resources", $resource->id, $resource->author, \App\User::all('id', 'name', 'last_name')->pluck('full_name'), "") !!}
				</div>
			</div>

			<!-- YEAR -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.year') }}
				</div>
				<div class="col-xs-10">
					<input list="years" type="text" id="resource_modal_year" name="year" data-model='resources' data-id={{ $resource->id }} value="{{ $resource->year }}" class="form-control update_input" required>
					<datalist id="years">
						@foreach (range(date('Y'), 1970) as $year)
							<option value="{{ $year }}">
						@endforeach
					</datalist>
				</div>
			</div>


			<!-- LICENCES -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.licence') }}
				</div>
				<div class="col-xs-10">
					@foreach (\App\Licence::all() as $licence)
						<?php $checked = $licence->resources->find($resource->id) ? "checked" : "";?>
						<input type="checkbox" id="licence_{!! $licence->id !!}" name="licence" data-url='/resources/licence_update' data-id={!! $resource->id !!} data-id-belongs-to="{!! $licence->id !!}" value="{{ $licence->resources->find($resource->id) }}" class="update_checkbox radio_course" style="display: none" {!! $checked !!} >
						<label class="round_button" for="licence_{{ $licence->id }}" title="{!! $licence->name !!} || {!! $licence->description !!}">
							<img id="licence_image_{{ $licence->id }}" src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
						</label>
					@endforeach
				</div>
			</div>

			<!-- TYPE -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.file_type') }}
				</div>
				<div class="col-xs-10">
					@foreach (\App\ResourceType::get()->sortBy('order') as $resource_type)
						<?php $checked = ($resource->resource_type_id == $resource_type->id) ? "checked" : "";?>
						<input {!! $checked !!} type="radio" value="{{ $resource_type->id }}" list="years" type="text" id="resource_type_{{ $resource_type->id }}" name="resource_type_id" data-model='resources' data-id={{ $resource->id }} class="radio_course update_input resource_type" style="display: none;">
						<label class="round_button lens" for="resource_type_{{ $resource_type->id }}" title="{{ trans('resource_types.' . $resource_type->name) }}">{!! $resource_type->icon !!}</label>
					@endforeach
				</div>
			</div>

			<!-- LENGTH -->
			<div class="row">
				<div class="col-xs-2">
					{{ trans('text.length') }}
				</div>
				<div class="col-xs-5">
					<input type="text" name="length" data-model='resources' data-id={!! $resource->id !!} value="{{ $resource->length }}" class="form-control update_input" required>
				</div>
				<div class="col-xs-5">
					{!! Form::select('unit', ['minutes' => trans('text.minutes'), 'hours' => trans('text.hours'), 'pages'=> trans('text.pages'), 'slides'=> trans('text.slides')], $resource->unit, ['class' => 'form-control update_input', 'data-model' => 'resources', 'data-id' => $resource->id, 'placeholder' => trans('text.select_unit') ]) !!}
				</div>
			</div>

			<div class="row" style="margin-top: 10px;">
				<div class="col-xs-2"></div>
				<div class="col-xs-10" id="resource_file_and_preview">
					@include('courses.edit.modals.resource_edit_modal_file_list', ['resource' => $resource])
				</div>
			</div>

		</div>
	</div>
</div>


<script type="text/javascript">
	// Delete resource file
	$('body').on('click', '#delete_resource_file', function(){

		$.ajax({
			url: '/resources/remove_file/{{ $resource->id }}',
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data, status) {
				if (data == "OK") {
					resourceModalFilePreviewUpdate({{ $resource->id }});
				} else {
					alert(data);
				}
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});


</script>

