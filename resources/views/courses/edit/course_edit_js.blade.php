<script type="text/javascript">
	
	var current_course_id = {{ $course->id }};

	$( document ).ready(function() {

		loadCourseImageForEdit(); 
		loadCourseAuthorsForEdit();
		make_courses_sortable();

		// GRID VIEW LOADER
		$('body').on('click', '#open_courseware', function(){
			loadGridView();
		});

		// Create a new resource
		$('body').on('click', '.add_resource', function(){
			
			var lecture_id = $(this).data('lecture-id');
			var preview = $('#add_resource_preview_' + lecture_id).val();
			var token = $("meta[name='csrf-token']").attr("content"); 
			
	        var return_data = convertMedia(preview);
	        if ( return_data ) {
	            $('#add_resource_preview_' + lecture_id).val(return_data); 
	            preview = return_data;
	        }	

			$.ajax({
				method:"POST",
				url:"/resources/create_with_preview/" + lecture_id,
				success: function(responce){
					openModalFromUrl('/resources/course_edit_modal/' + responce['id']);
					lecture_resources_load(lecture_id);
				},
				data: {
					'preview': preview,
					'_token': token,
			    },
			});
		});


		// ---- AUTHORS ----//
		

			// ADD AUTHORS
			$('body').on( 'click', '.add_teacher', function () {

				// token
				var token = $("meta[name='csrf-token']").attr("content"); 

				var row = authors_list_datatable.row( $(this).parents('tr') );
				var author_id = $(this).data('author-id');
				// console.log(author_id);
				$.ajax({
					url: '/courses/add_author',
					type: 'POST',
					data: {
						'_token': token,
						'course_id': current_course_id,
						'author_id': author_id,
				      },

					success: function(data, status) {
						loadCourseAuthorsForEdit();
						row.remove().draw();
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
			});


			// REMOVE AUTHOR
			$('body').on('click', '.remove_author_button', function(){

				var token = $("meta[name='csrf-token']").attr("content"); 
				var author_id = $(this).data('id');
				var button = $(this);

				$.ajax({
					url: '/courses/remove_author',
					type: 'POST',
					data: {
						'_token': token,
						'course_id': {{ $course->id }},
						'author_id': author_id,
				      },

					success: function(data, status) {
						loadCourseAuthorsForEdit();
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
			})

	
		// ---- VIEW SELECTORS ----//
		
			// LIST VIEW SELECTOR
			$('body').on('click', '#list_view_selector', function(){
				loadListView();
			});



			// GRID VIEW SELECTOR
			$('body').on('click', '#grid_view_selector', function(){
				loadGridView();				
			});


		// ---- SUBJECTS ---- //
		
			// CREATE SUBJECT
		    $('body').on('click', '#add_subject', function(){
				$.ajax({
					url: '/course_subjects/create_grid_view/{!! $course->id !!}' ,
					type: 'GET',
					success: function(data, status) {
						$('#sortable_courseware').append(data);
						// Add lecture
						addLectureListView($("#sortable_courseware li").last().data('subject-id'));
						make_courses_sortable();
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
			});

			// CREATE SUBJECT GRID VIEW
		    $('body').on('click', '#add_subject_grid_view', function(){
				$.ajax({
					url: '/course_subjects/create_grid_view/{!! $course->id !!}' ,
					type: 'GET',
					success: function(data, status) {
						$('#sortable_courseware').append(data);
						// Add lecture
						make_courses_sortable();
						addLectureGridView($("#sortable_courseware").children().last().data('subject-id'));
						
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
			});



		// ---- LECTURES ---- //

		    // ADD A LECTURE LIST VIEW 
		    $('body').on('click', '.add_lecture', function() {
		    	var course_subject_id = $(this).data('course-subject-id');
				addLectureListView(course_subject_id);
				
			});

		   
			// BUTTON CREATE LECTURE GRID VIEW
			$('body').on('click', '.add_lecture_grid', function(){

				var course_subject_id = $(this).data('course-subject-id');
				console.log(course_subject_id);
				addLectureGridView(course_subject_id);
				
			})

			
		// RESOURCE MODAL CLOSE
		$('body').on('hidden.bs.modal', '#resourceModal', function(){	
			var resource_id = $('#resource_modal_id').val();
			$('#resource_' + resource_id + '_name').html( $('#resource_modal_name').val() );
			$('#resource_' + resource_id + '_author').html( $('#resource_modal_author').val() );
			$('#resource_' + resource_id + '_year').html( $('#resource_modal_year').val() );

			var resource_type_checkbox = $('.resource_type:checked:first');
			var label = $("label[for='"+ resource_type_checkbox.attr('id') + "']");
			$('#resource_' + resource_id + '_file_type').html( $(label).html() );
		});

	

    // DELETE LECTURE CONTENT
    $('body').on('click', '.delete_lecture_content', function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var content_id = $(this).data('content-id');
        var lecture_id = $(this).data('lecture-id');

        // ASK THE QUESTION 
        bootbox.confirm({
            message: "{{ trans('courses.delete_content_question') }}",
            buttons: {
                confirm: {
                    label: '{{ trans('text.delete') }}',
                    className: 'btn-success'
                },
                cancel: {
                    label: '{{ trans('text.cancel') }}',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {

                    // If OK, delete the content
                    $.post({
                        url: '/lectures/content/delete/' + content_id,
                            data: {
                                '_token': token,
                            },
                            success: function(data, status) {
                                lecture_content_load(lecture_id);
                            },
                            error: function(xhr, desc, err) {
                                console.log(xhr);
                                console.log("Details: " + desc + "\nError:" + err);
                            }
                    });
                }
            }
        });
    });


		// EDIT RESOURCE
		$('body').on('click', '.edit_resource', function(){	
			//console.log($(this).data('id'));
			var resource_id = $(this).data('id');
			edit_resource(resource_id);
		});

		// TABS
		$('#content_tabs a').click(function (e) {
		  	e.preventDefault()
		  	$(this).tab('show')
		})				
		
		$('body').on('mouseover', '.lecture_grid_text', function(){
			
		});



		$('body').on('click', '.resource_add_preview', function(){

			var token = $("meta[name='csrf-token']").attr("content"); 
			var resource_id = $(this).data('resource-id');
			var preview = $(".resource_preview").val();

	        var return_data = convertMedia(preview);
	        if ( return_data ) {
	            $(".resource_preview").val(return_data); 
	            preview = return_data;
	        }			

			$.ajax({
				url: '/resources/add_preview/' + resource_id,
				type: 'POST',
				data: {
					'_token': token,
					'preview': preview
			      },
				success: function(data, status) {
					resourceModalFilePreviewUpdate(resource_id);
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
			});	
		});

	});

	// Remove lecture cover image
	$('body').on('click', '.remove_lecture_image_button', function(){
		var lecture_id = $(this).data("lecture-id");
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/lectures/remove_cover_image/' + lecture_id,
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data) {
				loadLectureImageForEdit(lecture_id)
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	});	

	// EDIT RESOURCE MODAL
	function edit_resource(resource_id) {
		$.ajax({
			url: '/resources/edit_modal/' + resource_id,
			type: 'GET',
			success: function(data, status) {
				$('#resource_modal_div').html(data);
				$('#resourceModal').modal('show');
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}

	function make_courses_sortable() {
		$( "#sortable_courseware" ).sortable({
			
			stop: function( event, ui ) {
				// get the token
				var token = $("meta[name='csrf-token']").attr("content"); 

				// make subject id-s		
				var subjects = [];

				$(this).children('li').each(function(i){
					subjects.push($(this).data('subject-id'));
				});

				// send to server
				$.post({
					url: '/course_subjects/update_sort',
					data: {
						'_token': token,
						'subjects': subjects,
					},
					success: function(data, status) {
						//alert(data);
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
		  	}
		});
	}

	// LOAD LECTURE CONTENT
	function lecture_content_load(lecture_id) {
		
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.post({
			url: '/lectures/content/list_for_lecture_modal/' + lecture_id,
				data: {
					'_token': token,
				},
				success: function(data, status) {
					$('body #lecture_' + lecture_id + '_content').html(data);
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
		})
	}



	// ---- LECTURE RESOURCES ---- //


		// check all resources
		$('body').on('click', '.check_all_resources', function(){
		    $('input:checkbox.resource_checkbox_' + $(this).data('lecture-id') ).not(this).prop('checked', this.checked);
		});

		// publish all selected resources 
		$('body').on('click', '.publish_selected_resources', function(){
		    publish_selected_resources(1);
		});

		// unpublish all selected resources 
		$('body').on('click', '.unpublish_selected_resources', function(){
		    publish_selected_resources(0);
		});

		
		function publish_selected_resources(publish_state) 
		{
			var token = $("meta[name='csrf-token']").attr("content"); 
			var resources = [];

			$.each($(".resource_checkbox:checked"), function() {
			  resources.push( $(this).data('id') );
			});

			// check if there are resources
			if (resources.length > 0){

				$.ajax({
					method: "POST",
					url:"/resources/publish_multiple",
					data: {
						'_token': token,
						'resources': resources,
						'publish_state': publish_state
				    },
					success: function(responce){
						if (display_style == "grid") {
							lecture_resources_load(current_lecture_id);	
						} else {
							responce.forEach( function(element, index, array) {
								lecture_resources_load(element);
							});
						}
					},
				});	

			} else {
				bootbox.alert("{{ trans('resources.select_resources_you_want_to_publish') }}");
			}
		}


		// delete all selected resources 
		$('body').on('click', '.delete_selected_resources', function(){
		    delete_selected_resources();
		});

		
		function delete_selected_resources() 
		{
			var text = "{{ trans('resources.delete_multiple_question') }}";
			var resources = [];
        	var token = $("meta[name='csrf-token']").attr("content"); 

			$.each($(".resource_checkbox:checked"), function() {
			  resources.push( $(this).data('id') );
			});

			// check if there are resources
			if (resources.length > 0){
			
		        bootbox.confirm(text, function(result){ 		
			
					$.ajax({
						method: "POST",
						url:"/resources/delete_multiple",
						data: {
							'_token': token,
							'resources': resources
					    },
						success: function(responce){
							if (display_style == "grid") {
								lecture_resources_load(current_lecture_id);	
							} else {
								responce.forEach( function(element, index, array) {
									lecture_resources_load(element);
								});
							}
						},
					});
				});

			} else {
				bootbox.alert("{{ trans('resources.select_resources_you_want_to_delete') }}");
			}	

		}


		function sort_course_subject(subject_id) {
			$( "#course_subject_" + subject_id + "_collapse" ).accordion({
		    	header: "> span",
		    	handle: ".movable_arrow",
		    	collapsible: true,
		    	heightStyle: "content",
		    	active: false
		  	})

			$( "#sortable_lectures_" + subject_id ).sortable({
				connectWith: ".sortable_ul",
				stop: function( event, ui ) {
					// get the token
					var token = $("meta[name='csrf-token']").attr("content"); 

					// make subject id-s		
					var lectures = [];

					$(this).children('li').each(function(i){
						//alert ($(this).data('lecture-id'));
						lectures.push($(this).data('lecture-id'));
					});

					// send to server
					$.post({
						url: '/lectures/update_sort',
						data: {
							'_token': token,
							'lectures': lectures
						},
						success: function(data, status) {
							//alert(data);
						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});
			  	},
				receive: function( event, ui ){

					var recieverObject = $(this);
					var token = $("meta[name='csrf-token']").attr("content"); 
					var lecture_id = $(ui.item).data('lecture-id');
					var value = $(this).data('subject-id');
								console.log('lecture_id ' + lecture_id);
								console.log('subject-id ' + value);
					// send to server
					$.post({
						url: '/lectures/update_data',
						data: {
							'_token': token,
							'value': value,
							'id': lecture_id,
							'name': 'course_subject_id',
						},
						success: function(data, status) {
							// make subject id-s		
							var lectures = [];
							recieverObject.children('li').each(function(i){
								lectures.push($(this).data('lecture-id'));
							});
							// send to server
							$.post({
								url: '/lectures/update_sort',
								data: {
									'_token': token,
									'lectures': lectures
								},
								success: function(data, status) {
									console.log(lectures);
								},
								error: function(xhr, desc, err) {
									console.log(xhr);
									console.log("Details: " + desc + "\nError:" + err);
								}
							});

						},
						error: function(xhr, desc, err) {
							console.log(xhr);
							console.log("Details: " + desc + "\nError:" + err);
						}
					});				

							
				},			  	
			});
		}


	function sort_courses() {
		$( "#sortable_courseware" ).sortable({
			
			stop: function( event, ui ) {
				// get the token
				var token = $("meta[name='csrf-token']").attr("content"); 

				// make subject id-s		
				var subjects = [];

				$(this).children('li').each(function(i){
					subjects.push($(this).data('subject-id'));
				});

				// send to server
				$.post({
					url: '/course_subjects/update_sort',
					data: {
						'_token': token,
						'subjects': subjects,
					},
					success: function(data, status) {
						//alert(data);
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				});
		  	}
		});
	}


	// LOAD LECTURE GRID VIEW
	function lecture_grid_load(lecture_id) {
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/lectures/grid_view_edit/' + lecture_id,
			type: 'POST',
			data: {
				'_token': token,
		    },

			success: function(data, status) {
				$("#lecture_grid_" + lecture_id).replaceWith(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}


	// LOAD RESOURCE COVER IMAGE 
	function loadLectureImageForEdit(lecture_id) 
	{
		var token = $("meta[name='csrf-token']").attr("content"); 
		$.ajax({
			url: '/lectures/load_cover_image_for_edit/' + lecture_id,
			type: 'POST',
			data: {
				'_token': token,
		      },
			success: function(data) {
				$('#lecture_image_container_' + lecture_id).html(data);
				console.log('lecture imge ' + lecture_id + data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	};


	function update_resource_preview_file(resource_id, preview_file){
		var token = $("meta[name='csrf-token']").attr("content");
		$.ajax({
			data: { 
			    '_token': token,
			    'preview': preview_file
			},
			url: '/resources/update_preview_file/' + resource_id,
			type: 'POST',

			success: function(response) {
			    if (response == "OK") {
			        object.animate({
			            backgroundColor: "#90EE90"
			        }, 300);
			        object.animate({
			            backgroundColor: "white"
			        }, 400).delay(400);

			    } else {
			        //bootbox.alert (response);
			    }
			    
			},
			error: function(response){
			    bootbox.alert (response);
			}
			});
	}

	function addLectureListView(_course_subject_id) {

		$.ajax({
			url: '/lectures/create_grid_view/' + _course_subject_id ,
			type: 'GET',
			success: function(data, status) {
				$('#sortable_lectures_' + _course_subject_id).append(data);
				window["sort_course_subject_" + _course_subject_id]();

			},
			error: function(xhr, desc, err) {
				bootbox.alert (desc);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
    }

	// CREATE LECTURE GRID VIEW
	function addLectureGridView(course_subject_id) {
		
		var token = $("meta[name='csrf-token']").attr("content"); 

		$.ajax({
			url: '/lectures/create_grid_view/' + course_subject_id,
			type: 'GET',
			data: {
				'_token': token,
		    },

			success: function(data, status) {
				$("#sortable_lectures_" + course_subject_id).find(' > .datatable_grid_div:nth-last-child(1)').before(data);
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
		});
	}

    // LOAD AUTHORS
    function loadCourseAuthorsForEdit()
    {
        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            url: '/courses/load_course_authors_for_edit/{{ $course->id }}',
            type: 'POST',
            data: {
                '_token': token,
            },
            success: function(data) {
                $("#authors_list").html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    }


    function loadListView() {
        // Hide no resources div (if exists)
        $(".noResourcesDiv").hide();
        // Get token
        var token = $("meta[name='csrf-token']").attr("content");
        // Set display style
        display_style = "list";

        $.ajax({
            url: '/courses/edit/view/list/{!! $course->id !!}',
            type: 'POST',
            data: {
                '_token': token,
            },
            beforeSend: function() { $('#wait').show(); $("#courseware_data").html("");},
            success: function(data) {
                $('#wait').hide();
                $("#courseware_data").html(data);
                $("#list_view_selector").addClass('text-course');
                $("#grid_view_selector").removeClass('text-course');

            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    }

    function loadGridView() {
        // Show no resources div (if exists)
        $(".noResourcesDiv").show();

        var token = $("meta[name='csrf-token']").attr("content");
        display_style = "grid";

        $.ajax({
            url: '/courses/edit/view/grid/{!! $course->id !!}',
            type: 'POST',
            data: {
                '_token': token,
            },
            beforeSend: function() { $('#wait').show(); $("#courseware_data").html("");},
            success: function(data) {
                $('#wait').hide();
                $("#courseware_data").html(data);
                $("#list_view_selector").removeClass('text-course');
                $("#grid_view_selector").addClass('text-course');

            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    }


</script>