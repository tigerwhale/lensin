<div class="panel-group text-center">
  <button class="advanced_search_confirm_button btn">{{ trans('text.confrm') }}</button>
  <button class="advanced_search_close_button btn"><i class="fa fa-times" aria-hidden="true"></i></button>
  <!-- LANGUAGES -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#advanced_search_languages">{{ strtoupper(trans('text.language')) }} <span class="caret"></span></a>
      </h4>
    </div>
    <div id="advanced_search_languages" class="panel-collapse collapse" style="padding: 3px;">
      @foreach ($languages as $language)
        {{ Form::checkbox($language->name, $language->id, ['name'=>'language_'.$language->id]) }} <label for="{!! 'language_'.$language->id !!}">{!! $language->name !!}</label><br>
      @endforeach
    </div>
  </div>

  <!-- LANGUAGES -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#advanced_search_type">{{ strtoupper(trans('text.types')) }} <span class="caret"></span></a>
      </h4>
    </div>
    <div id="advanced_search_type" class="panel-collapse collapse" style="padding: 3px;">
      @foreach ($languages as $language)
        {{ Form::checkbox($language->name, $language->id, ['name'=>'language_'.$language->id]) }} <label for="{!! 'language_'.$language->id !!}">{!! $language->name !!}</label><br>
      @endforeach
    </div>
  </div>

  <!-- AUTHORS -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#authors_search_input">{{ strtoupper(trans('text.authors')) }} <span class="caret"></span></a>
      </h4>
    </div>
    <div id="authors_search_input" class="panel-collapse collapse" style="padding: 3px;">
      <input type="text" name="authors" class="form-control" placeholder="{{ trans('text.authors') }}">
    </div>
  </div>

  <!-- YEAR -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#year_search_input">{{ strtoupper(trans('text.year')) }} <span class="caret"></span></a>
      </h4>
    </div>
    <div id="year_search_input" class="panel-collapse collapse" style="padding: 3px;">
      {{ Form::selectRange('number', 2000, date("Y"), null ,['class'=>'form-control']) }}
      &nbsp;
    </div>
  </div>

</div>


<script type="text/javascript">

    $('.header-advanced-search, .advanced_search_close_button, .advanced_search_confirm_button').on('click', function (event) {
        $('.advanced_search_div').toggle();
    });
    
</script>