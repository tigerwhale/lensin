@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-xs-2" style="padding-top: 20px;"></div>
        <div class="col-xs-8" style="padding-top: 20px;">
            <h2 style="text-align: center;">{{ trans('text.profile') }}</h2>
            <ul class="nav nav-tabs" style="border-bottom: 1px solid #ddd;">
                <li id="profile_tab" class="active"><a data-toggle="tab" href="#profile">{{ trans('text.participant') }}</a></li>
                <li id="members_tab"><a data-toggle="tab" href="#create_member_tab">{{ trans('text.join_as_member') }}</a></li>
                <li id="partners_tab" ><a data-toggle="tab" href="#partners">{{ trans('text.join_as_partner') }}</a></li>
                <li id="new_network_tab" ><a data-toggle="tab" href="#new_network">{{ trans('text.register_a_new_network') }}</a></li>
            </ul>

            <div class="tab-content">
                <div id="profile" class="tab-pane fade in active">
                    @include('users.tab_profile', ['user' => $user, 'editableProfile' => $editableProfile])
                </div>

                <div id="create_member_tab" class="tab-pane fade in">
                    @include('users.tab_profile_members', ['user' => $user])
                </div>

                <div id="partners" class="tab-pane fade in">
                    @include('users.tab_profile_partners', ['user' => $user])
                </div>

               <div id="new_network" class="tab-pane fade in">
                    @include('users.tab_new_network', ['user' => $user])
                </div>
            </div>
        </div>
    </div>
</div>

@include('users.profile_js')

@endsection
