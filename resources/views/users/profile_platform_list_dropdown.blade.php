<div class="col-12">
	<label>{{ trans('text.platform') }}</label>
	<select name="server_id" class="form-control update_input" id="test" data-id="{{ $request->user_id }}" data-model="backend/users"> 
			<option value="{!! server_property('server_id') !!}">{{ server_property('platform_title') }}</option>
	    @foreach ($server_list as $server)
	    	<?php 
	    		$selected = ($server->id == $request->platform_id) ? "selected" : "";
	    	?>
	    	<option {!! $selected !!} value="{!! $server->id !!}">{{ $server->name }}</option>
    	@endforeach
	</select>
</div>

