<?php 
    $memberGroups = \App\PartnersGroup::where('published', 1)->get()->pluck('title', 'id');
    $countries = \App\Country::all()->pluck('name', 'id');
    $memberGroupTypes = [
        'university/school' => 'University/School',
        'research institute' => 'Research institute'
    ];
?>

<!-- PARTNERS GROUP LIST-->
@if (count($user->partners_groups))
    <br>
    <h4>Current partners</h4>
    {{ trans('users.you_are_a_part_of') }}:<br>
    @foreach ($user->partners_groups as $partners_group)

        <div class="row hover padding-small" id="partner_row_{!! $partners_group->id !!}">
            <div class="col-xs-10" style="padding-top: 7px;">
                {!! $partners_group->title !!}
                @if(!$partners_group->published)
                    <span class="text-danger small"> - {{ trans('users.pending_administrator_approval') }}</span><br>
                @endif 
            </div>
            <div class="col-xs-2 text-right">
                <div class="btn btn-danger leave_partner" title="{{ trans('users.leave_partner') }}" data-partner-id="{!! $partners_group->id !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
            </div>
        </div>

    @endforeach
    <br>
@endif


<!-- JOIN PARTNERS GROUP -->
<br>
<hr style="border-top: 1px solid lightgray !important;">

<h4>Join existing partners</h4>
{{ trans('users.join_partner_text') }}
<br>

@if (isset($partners_group))

    <form method="POST" action="/partnersgroup/join/{!! Auth::user()->id !!}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-10">
                {{ Form::select('partner_group_id', $memberGroups , null, ['class' => 'form-control']) }}
            </div>
            <div class="col-xs-2 text-right">
                <button class="btn btn-success" type="submit">{{ trans('text.join') }}</button>
            </div>
        </div>
    </form>

@else
    {{ trans('users.no_partner_groups') }}
@endif 

<br>
<hr style="border-top: 1px solid lightgray !important;">


<!-- CREATE PARTNERS GROUP -->
<h4>Create a new partner</h4>
{!! trans('users.register_partner_text') !!}
<br>
<br>
<form method="POST" action="/partnersgroup/create" enctype="multipart/form-data">
    {{ csrf_field() }}
    
    <div class="row">

        <!-- LEFT -->
        <div class="col-md-8">

            <div class="row form-group platform_dropdown">
                <i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>
            </div>        
            <!-- Type --> 
            {!! updateInput('text.type', Form::select('type', $memberGroupTypes , null, ['class' => 'form-control', 'id'=> 'member_type']) ) !!}
            <!-- Name --> 
            {!! updateInput('text.name', Form::text('title', null, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Description --> 
            {!! updateInput('text.description', Form::text('description', null, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Reference Person --> 
            {!! updateInput('text.reference_person', Form::text('reference_person', Auth::user()->fullName(), ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Reference Person Email --> 
            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', Auth::user()->email, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Country --> 
            {!! updateInput('text.country', Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'id'=> 'country_id', 'placeholder' => trans('text.select_country')]) ) !!}
            <!-- Address --> 
            {!! updateInput('text.address', Form::text('address', null, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Website --> 
            {!! updateInput('text.web_site', Form::text('web_site', null, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Email --> 
            {!! updateInput('text.email', Form::email('email', null, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Submit button --> 
            <button class="btn btn-success" id="create_member_group">{{ trans('text.register') }}</button>
        </div>

        <!-- RIGHT -->
        <div class="col-md-4">
            <div class="row top-padding-small">
                Partners logo
            </div>
            <div class="row">
                <label for="partner_cover_image" class="dropzone_div" id="partner_cover_image_label">{{ trans('text.click_to_select_image') }}</label>
                <input type="file" name="image" id="partner_cover_image" style="display: none">
            </div>
        </div>

    </div>
</form>


<!-- FUNCTIONS -->
<script type="text/javascript">

    $(document).ready(function(){

        $('#partner_cover_image_label').on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            })
        .on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            $('#partner_cover_image_label').html( e.originalEvent.dataTransfer.files[0].name);
            $("#partner_cover_image").prop("files", e.originalEvent.dataTransfer.files)
        });
    });

    $('#partner_cover_image').change(function(e) {
        $('#partner_cover_image_label').html( e.target.files[0].name );
    });


    $('body').on('click', '.leave_partner', function(){
                
        var token = $("meta[name='csrf-token']").attr("content"); 
        var partner_id = $(this).data('partner-id');
        var text = "{{ trans('users.leave_partner_question') }}"

        bootbox.confirm(text, function(result){ 
            if (result) {
               $.post({
                    url: '/partnersgroup/leave',
                    data: {
                        '_token': token,
                        'partner_id': partner_id,
                    },
                    success: function(data, status) {
                        if (data == "OK"){
                            $('#partner_row_' + partner_id).toggle();                       
                        }
                    },
                    error: function(xhr, desc, err) {
                        console.log(xhr);
                        console.log("Details: " + desc + "\nError:" + err);
                    }
                })
            }
        });

    });
  
</script>
