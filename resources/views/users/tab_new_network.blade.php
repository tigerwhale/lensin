<?php 
	$countries = \App\Country::all()->pluck('name', 'id');
?>
<div class="row">
	<div class="col-xs-12">
		<br>
		<h4>{{ trans('text.launch_a_new_lens_platform') }}</h4>
		{!! trans('text.launch_a_new_lens_platform_text') !!}
		<br><br>

		<span style="line-height: 45px;">{{ strtoupper(trans('text.download')) }}:</span>
		<span class="round_button text-tools text-center pull-right" style="background-color: white;">
			<a href="/download_files/LeNS_PLATFORM_letter_of_intent.docx"><i class="fa fa-download" aria-hidden="true"></i></a>
		</span>

		<br>
		<br>
		<hr style="border-top: 1px solid lightgray !important;">

		<form action="/backend/register_new_network" method="POST" enctype="multipart/form-data" >
			{{ csrf_field() }}
			<!-- Name --> 
            {!! updateInput('text.name', Form::text('name', null, ['class' => 'form-control', 'required'=>""]) ) !!}
			<!-- Description --> 
            {!! updateInput('text.description', Form::textarea('description', null, ['class' => 'form-control', 'required' => "", 'style' => 'height: 300px;']) ) !!}
			<!-- reference_person --> 
            {!! updateInput('text.reference_person', Form::text('reference_person', Auth::user()->fullName(), ['class' => 'form-control', 'required'=>""]) ) !!}
			<!-- reference_person_email --> 
            {!! updateInput('text.reference_person_email', Form::email('reference_person_email', Auth::user()->email, ['class' => 'form-control', 'required'=>""]) ) !!}
            <!-- Country --> 
            {!! updateInput('text.country', Form::select('country_id', $countries, Auth::user()->country_id, ['class' => 'form-control', 'id'=> 'country_id', 'placeholder' => trans('text.select_country')]) ) !!}
            <div class="row form-group">
            	<div class="col-md-4">
            		{{ trans('text.upload_filled_document') }}
            	</div>
            	<div class="col-md-8">
            		<label for="document_upload" class="dropzone_div" id="document_upload_label">Upload signed document</label>
                	<input type="file" name="document" id="document_upload" style="display: none">	
            	</div>
            </div>
			<br>
			<button class="btn btn-success">{{ trans('text.register') }}</button>
		</form>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function(){

	    $('#document_upload_label').on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
	            e.preventDefault();
	            e.stopPropagation();
	        })
	    .on('drop', function(e) {
	        droppedFiles = e.originalEvent.dataTransfer.files;
	        $('#document_upload_label').html( e.originalEvent.dataTransfer.files[0].name);
	        $("#document_upload").prop("files", e.originalEvent.dataTransfer.files)
	    });
	});

	$('#document_upload').change(function(e) {
	    $('#document_upload_label').html( e.target.files[0].name );
	});

</script>