<script type="text/javascript">

    // LOAD CREATE MEMEBER GROUP TAB
    function loadMembersTab(){
        var token = $("meta[name='csrf-token']").attr("content"); 

        $.post({
            url: '/member_group/view_create_member_tab',
            data: {
                '_token': token,
            },
            success: function(data, status) {
                $('#create_member_tab').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })
    }

    // LOAD CREATE MEMEBER GROUP TAB
    function loadMembersTab(){
        var token = $("meta[name='csrf-token']").attr("content"); 

        $.post({
            url: '/partnersgroup/view_create_partners_tab',
            data: {
                '_token': token,
            },
            success: function(data, status) {
                $('#partners').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })
    }

    $.get({
        url: '{!! server_property('central_server_address') !!}/api/platform_list_dropdown',
        data: {
            "platform_id": "{!! server_property('server_id') !!}",
        },
        success: function(data, status) {
            $('.platform_dropdown').each(function() {
                $(this).html(data);
            });
            $('.js_platform_list_spinner').hide();
        },
        error: function(xhr, desc, err) {
            $('.platform_dropdown').html('<div class="col-12">Platform dropdown error</div>');
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });

    $.get({
        url: '{!! server_property('central_server_address') !!}/api/profile_platform_list_dropdown',
        data: {
            "platform_id": "{!! server_property('server_id') !!}",
            "user_id": {!! Auth::user()->id !!}
        },
        success: function(data, status) {
            $('.user_platform_dropdown').html(data);
        },
        error: function(xhr, desc, err) {
            $('.user_platform_dropdown').html('<div class="col-12">Platform dropdown error</div>');
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });

    $('body').on('change', '.platform_select', function(){
        var url = document.location.toString();
        var address = $(this).val() + "/profile";
        if (url.match('#')) {
            address = address + "#" +url.split('#')[1];
        } 
        window.location.href = address;
    });  

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
    

</script>