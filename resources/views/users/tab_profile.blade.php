<div class="row">
    <div class="col-md-8" style="padding-top: 20px;">

        <h4>Information</h4>

        @if (!$editableProfile)
            <div class="row form-group user_platform_dropdown">
                <i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i>
            </div>
        @else
            Your profile information is editable only on your original platform.
            <br>
            <a href="{!! Auth::user()->server_url !!}/profile?token={!! Auth::user()->login_token !!}&email={!! Auth::user()->email !!}&server_id={!! Auth::user()->server_id ?: 0 !!}" class="btn btn-success">Edit your profile</a>
            <br><br>
        @endif

        <!-- NAME --> 
        {!! updateTextInput($user, 'name', 'backend/users', [$editableProfile => $editableProfile] ) !!}
        <!-- LAST_NAME --> 
        {!! updateTextInput($user, 'last_name', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- USER_TYPE --> 
        {!! updateInput('text.user_type', Form::select('user_type_id', $user_types, $user->user_type_id, [$editableProfile => $editableProfile, 'class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) ) !!}
        <!-- EMAIL --> 
        {!! updateTextInput($user, 'email', 'backend/users', [$editableProfile => $editableProfile, 'type'=>'email']) !!}
        <!-- SCHOOL --> 
        {!! updateTextInput($user, 'school', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- DEPARTMENT --> 
        {!! updateTextInput($user, 'departement', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- POSITION --> 
        {!! updateTextInput($user, 'position', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- COUNTRY --> 
        {!! updateInput('text.country', Form::select('country_id', $countries, $user->country_id, [$editableProfile => $editableProfile, 'class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'backend/users', 'data-id'=> "$user->id" ]) ) !!}
        <!-- ADDRESS --> 
        {!! updateTextInput($user, 'address', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- INTEREST --> 
        {!! updateTextInput($user, 'interest', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- PHONE --> 
        {!! updateTextInput($user, 'phone', 'backend/users', [$editableProfile => $editableProfile]) !!}
        <!-- WEB --> 
        {!! updateTextInput($user, 'web', 'backend/users', [$editableProfile => $editableProfile]) !!}

        <!-- PASSWORD -->
        <h4>{{ trans('users.change_password') }}</h4>
        {!! updateInput('users.old_password', Form::password('old_password', ['class' => 'form-control', $editableProfile => $editableProfile]) ) !!}
        {!! updateInput('users.new_password', Form::password('new_password', ['class' => 'form-control', $editableProfile => $editableProfile]) ) !!}
        {!! updateInput('users.verify_new_password', Form::password('new_password_verify', ['class' => 'form-control', $editableProfile => $editableProfile]) ) !!}
        <button class="btn btn-success" {!! $editableProfile !!}>{{ trans('users.update_password') }}</button>
    </div>

    @if (!$editableProfile)
        <div class="col-md-4" style="padding-top: 20px;">
            <h4>&nbsp;</h4>
            User image
            {!! coverImageEdit($user) !!}
        </div>
    @endif
</div>