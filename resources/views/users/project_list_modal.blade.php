<div class="modal-dialog modal-lg">
	<div class="modal-content">

		<!-- Modal content-->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('users.my_projects') }}</h4>
		</div>

		<div class="modal-body">
			@foreach($user->challenge_groups as $group)

				<div class="row hover">
					<div class="col-xs-12">
						<a href="/projects/view/{!! $group->id !!}">{{ $group->name }}</a>
					</div>
				</div>

			@endforeach
		</div>

	</div>
</div>
