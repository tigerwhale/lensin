<!-- LIST OF RESOURCES -->
<div class="row" style="margin-bottom: 20px;">
	<div class="col-xs-12">
		<ul class="sortable_ul ui-sortable">

			@foreach ($lecture->resources as $resource)
				<li class="col-xs-3 resource_grid_li" data-id="{!! $resource->id !!}" id="resource_row_{!! $resource->id !!}">
					<?php $resourceRowClass = $resource->published ? 'resourceRowPublished' : 'resourceRowUnpublished'; ?>
					<!-- icon and name -->
					<div class="row {!! $resourceRowClass !!}">
						<div class="col-xs-12 text-center">
							<!-- resource file type -->
							<div class="resource_grid_icon">
								@if ($resource->resource_type)
									{!! $resource->resource_type->icon !!}
								@else
									<i class="fa fa-ellipsis-h" aria-hidden="true"></i>
								@endif
							</div>
							<!-- resource name -->
							<div class="resource_grid_name line-limit">
								{!! $resource->name() !!}
							</div>
						</div>
					</div>

					<!-- cover block -->
					<div class="resource_cover">
						<!-- edit resource -->
						<div class="btn bck-lens modal_url" data-close-function="lecture_resources_load_{!! $lecture->id !!}" data-url="/resources/course_edit_modal/{{ $resource->id }}" ><i class="fas fa-pencil-alt" aria-hidden="true"></i></div>
						<!-- publish / unpublish resource -->
						{!! publishButton($resource) !!}
						<!-- delete resource -->
						<button data-model='resources' data-delete-id="resource_row_{{ $resource->id }}" data-id={{ $resource->id }} data-text='{{ trans("text.question_delete_resource") }}' class="btn btn-danger delete-resource" type="button" ><i class="fa fa-times" aria-hidden="true"></i></button>
					</div>

				</li>
			@endforeach

			<!-- NEW RESOURCE -->
			<li class="col-xs-6" style="padding: 5px; list-style: none; height: 110px;">
				<div style="width: 100%; background-color: #EEE; height: 100px; padding: 5px;">
					<div class="row">
						<div class="col-xs-12">
							<!-- DROPZONE -->
							<div class="dropzeon dropzone_div" style="padding-top: 5px; padding-bottom: 5px;" id="resource_upload_{{ $lecture->id }}" >{{ trans('text.click_to_select_file') }}</div>
							<span id="tmp-path"></span>
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div class="col-xs-12">
							<input type="text" id="add_resource_preview_{!! $lecture->id !!}" name="resource_link" class="form-control inline_for_button" placeholder="{{ trans('text.or_insert_link') }} ">
							<button type="button" class="btn btn-success add_resource" data-lecture-id="{!! $lecture->id !!}">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>

</div>
<div class="clear-fix"></div>

<script type="text/javascript">
	// Limit the lines
	lineLimit();

	// Set the current lecture id
	var current_lecture_id = "{!! $lecture->id !!}";

	// Set the function to load resources
	window["lecture_resources_load_{!! $lecture->id !!}"] = function lecture_resources_load_{!! $lecture->id !!}() {
		lecture_resources_load({!! $lecture->id !!});
	}

	$( document ).ready(function(){
		var file_paths = [];

		// DROPZONE - upload resouce and refresh file list
		$("div#resource_upload_{{ $lecture->id }}").dropzone({
			url: "/resources/create_and_upload_files/{{ $lecture->id }}",
			paramName: "resource_file",
			previewsContainer: false,
			uploadMultiple: true,
			parallelUploads: 100,
			autoProcessQueue: false,

			drop: function()
			{
				setTimeout(function() {
					var myDropzone = Dropzone.forElement("div#resource_upload_{{ $lecture->id }}");
					myDropzone.processQueue();
				}, 1000);
			},
			addedfile: function()
			{
				setTimeout(function() {
					var myDropzone = Dropzone.forElement("div#resource_upload_{{ $lecture->id }}");
					myDropzone.processQueue();
				}, 1000);
			},
			processing: function(file, xhr, formData)
			{
				if (file.fullPath) {
					file_paths.push(file.fullPath);
				} else {
					file_paths.push(file.name);
				}
			},
			sending: function(file, xhr, formData)
			{
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') );
		        formData.append("file_paths", file_paths);

				// if extension is zip, ask if the upload is camtasia
				if (file.name.substring(file.name.length -3) == "zip")
				{
					if (confirm( "{{ trans('resources.question_is_this_camtasia') }}" )) {
			    		formData.append("zip", "1");
			    	}
				}
			},
			uploadprogress: function(file, progress, bytesSent)
			{
			    $("#resource_upload_{{ $lecture->id }}").html(parseInt(progress) + "%");
			},
			successmultiple: function(file, responce)
			{
				// add the row and open the modal
				if (responce != "upload_error")
				{
					resourceModalFilePreviewUpdate({{ $lecture->id }});
					var responce_array = JSON.parse(responce);
					if (responce_array['type'] == "multiple") {
						select_resource_preview_file(responce_array['resource_id'], responce_array['file_list']);
					}
				} else
				{
					alert ("{{ trans('text.upload_error') }}");
				}
				// Return text to "click here..."
				$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.click_to_select_file') }}");
				lecture_resources_load({!! $lecture->id !!})
			},
			// error return function
			error: function (file, responce){
				$("#resource_upload_{{ $lecture->id }}").html("{{ trans('text.upload_error') }}" + "<br>" + "{{ trans('text.click_to_select_file') }}");
			},

			init: function (){
				var file_paths = [];
				myDropzone = this;
			},
		});
	});
</script>
