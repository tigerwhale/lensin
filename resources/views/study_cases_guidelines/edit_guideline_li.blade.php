	<li id='guideline_{{ $guideline->id }}' class="sortable_li">
		<div class="row">
			<!-- OFFSET 1 -->
			@if($level > 0)
				<div class="col-xs-{!! $level * 2 !!}">
				</div>
			@endif		

			<!-- TEXT -->
			<div class="col-xs-6">
				<input type="text" name="name" class="form-control">
			</div>
		</div>
		
		@if ($guideline->children)
			@foreach($guideline->children as $child)
				@include('study_cases_guidelines.edit', ['guidelines' => $child, 'level' => $level + 1, 'parent' => $guideline->id]);
			@endforeach
		@endif
	</li>	