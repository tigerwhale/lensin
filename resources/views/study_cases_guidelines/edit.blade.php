<?php
switch ($level)
{
	case (-1):
		$create_button_title = trans('criteria.create_dimension');
		$delete_button_title = trans('criteria.delete_dimension');
		break;
	case 0:
		$create_button_title = trans('criteria.create_criteria');
		$delete_button_title = trans('criteria.delete_dimension');
		break;
	case 1:
		$create_button_title = trans('criteria.create_guideline');
		$delete_button_title = trans('criteria.delete_criteria');
		break;
	case 2:
		$create_button_title = trans('criteria.create_subguideline');
		$delete_button_title = trans('criteria.delete_guideline');
		break;
	case 3:
		$create_button_title = trans('criteria.create_subguideline');
		$delete_button_title = trans('criteria.delete_subguideline');
		break;
}

$panelClass = $level != 3 ? "panel-collapse-toggle collapsed" : "";
?>

<li id="guideline_li_{!! $guideline->id !!}" class="criteria_li" style="padding-top: 10px;">
	<div class="row">

		@if($level == 0)
			<div class="panel-group" style="margin-bottom: 5px;">
		@else
			<div class="panel-group" style="padding-left: 10px; margin-bottom: 5px;">
		@endif

			<div class="panel panel-default">
				<div class="panel-heading criteria-bck-{!! $level !!}">
					<h4 class="panel-title">
						<a data-toggle="collapse" class="{!! $panelClass !!}" href="#collapse_{!! $guideline->id !!}"><small>{{ trans('text.' . $level_texts[$level]) }}</small></a>
						<div style="height: 40px;">
							<input type="text"
								   name="name"
								   data-model="study_case_guideline"
								   data-id="{{ $guideline->id }}"
								   value="{{ $guideline->name }}"
								   placeholder="{{ strtoupper(trans('text.insert_title')) }}"
								   class="form-control update_input"
								   style="width: calc(100% - 50px); float: left;"
								   maxlength="490" required>
							<div class="btn round_button btn-danger delete_guideline pull-right"
								 title="{{ $delete_button_title }}"
								 data-guideline-id = '{!! $guideline->id !!}'
								 data-level="{!! $level !!}"><i class="fa fa-times" aria-hidden="true"></i></div>
						</div>

					</h4>
				</div>

				<div id="collapse_{!! $guideline->id !!}" class="panel-collapse collapse criteria-bck-{!! $level !!}">
					@if($level == 0)
						<div class="col-md-9 col-xs-12">
					@else
						<div class="col-xs-12">
					@endif
							<ul data-level="{!! $level !!}" data-guideline-id="{!! $guideline->id !!}" id="guideline_ul_{!! $guideline->id !!}" class="sortable_ul">
								<!-- load children -->
								@if ($guideline->children)
									@foreach($guideline->children as $child)
										@include('study_cases_guidelines.edit', [ 'guideline' => $child, 'level' => $level + 1, 'languages' => $languages ])
									@endforeach
								@endif
							</ul>
						</div>

					@if($level == 0)
						<div class="col-md-3 col-xs-12">
							<small>{{ trans('text.author') }}</small>
							<input type="text" name="author" data-model='study_case_guideline' data-id={{ $guideline->id }} value="{{ $guideline->author }}" placeholder="{{ strtoupper(trans('text.author')) }}" class="form-control update_input input-sm">

							<small>{{ trans('text.language') }}</small>
							{{ Form::select('language_id', $languages, $guideline->language_id, ['class' => 'form-control input-sm update_input', 'data-model' => "study_case_guideline", 'data-id' => $guideline->id, 'placeholder' => trans('text.language')]) }}

							<small>{{ trans('text.year') }}</small>
							<input type="text" name="year" data-model='study_case_guideline' data-id={{ $guideline->id }} value="{{ $guideline->year }}" placeholder="{{ strtoupper(trans('text.year')) }}" class="form-control update_input input-sm">

							<small>{{ trans('text.institution') }}</small>
							<input type="text" name="institution" data-model='study_case_guideline' data-id={{ $guideline->id }} value="{{ $guideline->institution }}" placeholder="{{ strtoupper(trans('text.institution')) }}" class="form-control update_input input-sm">

							<small>{{ trans('text.licence') }}</small><br>
							@foreach ($licences as $licence)
								<?php $checked = $licence->guidelines->find($guideline->id) ? "checked" : ""; ?>
								<input type="checkbox" id="licence_{!! $licence->id !!}" name="licence" data-url='/study_case_guideline/licence_update' data-id={!! $guideline->id !!} data-id-belongs-to="{!! $licence->id !!}" value="{{ $guideline->id }}" class="update_checkbox radio_course" style="display: none" {!! $checked !!} >
								<label class="round_button" for="licence_{{ $licence->id }}">
									<img id="licence_image_{{ $licence->id }}" src="/images/licences/{!! $licence->id !!}.png" style="height: 20px;">
								</label>
							@endforeach

						</div>
					@endif
					<!-- BUTTON + -->
					<div class="row">
						<div class="col-xs-12">
							@if ($level < 3)
								<div class="btn round_button btn-success create_guideline" style="margin-left: 10px;" title="{{ $create_button_title }}" data-guideline-id = '{!! $guideline->id !!}' data-level="{!! $level !!}"><i class="fa fa-plus" aria-hidden="true"></i></div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</li>
