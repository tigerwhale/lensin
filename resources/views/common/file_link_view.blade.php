<?php $fileType = fileExtensionType($filename); ?>

<div class="row" style="margin-bottom: 10px;">
	<div class="col-xs-12 col-12">
		@if (!isset($noLink))
			{{ strtoupper(trans('text.download')) }}: <a href="{{ $filename }}" download>{{ basename($filename) }}</a>
		@endif 
		
		@if ($fileType == 'image')	
			
			<br>
			<img src="{{ $filename }}" class="image">
			
		@elseif ($fileType == 'video')
			
			<video id="my-video" class="video-js" controls preload="auto" data-setup="{}">
			    <source src="{{ $filename }}?{{ time() }}" type='video/mp4'>
			    <p class="vjs-no-js">
					To view this video please enable JavaScript, and consider upgrading to a web browser that
					<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			    </p>
			</video>

		@elseif ($fileType == 'p11df')
			
			<object data="{{ $filename }}" type="application/pdf" style="width: 100%; height: 600px;">
			    <embed src="{{ $filename }}">
			        This browser does not support view of PDFs.
			    </embed>
			</object>

		@endif

	</div>
</div>