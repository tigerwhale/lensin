  <!-- HEADER -->
    <div class="row bck-{!! $class !!} resource_header">
        <!-- left text -->
        <div class="col-xs-4 text-left resource_header_title edit-mode-header-tr">
            <i class="fas fa-pencil-alt" aria-hidden="true"></i>
        </div>

        <!-- middle buttons -->
        <div class="col-xs-4">
            <table style="margin-left: auto; margin-right: auto; margin-top:5px;">
                <tr>
                    <td style="padding: 5px;">
                        @if ($model == "lectures")
                            <!-- Close button -->
                            <a href="/"><button class="btn btn-danger" type="button" title="{{ trans('text.back') }}"><i class="fas fa-arrow-left" aria-hidden="true"></i></button></a>
                        @elseif ($createButton)
                            <!-- Close button -->
                            <a href="/"><button class="btn btn-danger" type="button" title="{{ trans('text.back') }}"><i class="fas fa-arrow-left" aria-hidden="true"></i></button></a>
                            <!-- Create button -->
                            <a href="/{!! $model !!}/create"><button type="button" class="btn btn-success" title="{{ strtoupper(trans('text.create')) }}"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
                        @else 
                            <!-- Close button -->
                            <a href="/{!! $model !!}/modify"><button class="btn btn-danger" type="button" title="{{ trans('text.back') }}"><i class="fas fa-arrow-left" aria-hidden="true"></i></button></a>
                        @endif

                    </td>
                </tr>
            </table>          
        </div>
        
        <!-- right text -->
        <div class="col-xs-4 text-right resource_header_title">
            <table style="float: right;">
                <tr>
                    <td style="vertical-align: middle; padding-right: 10px;">
                        {!! $text !!}
                    </td>
                    <td class="edit-mode-header-tr">
                        <img src="/images/resource/{!! $image !!}_off.png">
                    </td>
                </tr>
            </table>    
        </div>
    </div>
