<?php
	$modelClass = substr(get_class($resource), 4);
?>

<div id="cover_image_container_{!! $modelClass !!}_{!! $resource->id !!}">
	@if ($resource->image)
		<div class="image_container_inner text-center">
			<img src="{{ url($resource->image) }}"  style="max-width:100%; max-height: 200px;">
			<!-- bottom popup -->
			<div class="image_change_bottom_cover bck-{{ strtolower($modelClass) }}">
				<!-- remove image button -->
				<button class="btn btn-danger round_button remove_cover_image_button" data-model="{!! $modelClass !!}" data-id="{!! $resource->id !!}" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
			</div>			
		</div>
	@else 
		<div class="dropzeon dropzone_div" id="upload_cover_image_{!! $modelClass !!}_{!! $resource->id !!}" data-id="{{ $resource->id }}">
			{{ trans('text.click_to_select_image') }}
		</div>
	@endif

	<script type="text/javascript">
		// DROPZONE - upload cover image
		$("#upload_cover_image_{!! $modelClass !!}_{!! $resource->id !!}").dropzone(
		{ 
			url: "/upload_cover_image/{!! $modelClass !!}/{!! $resource->id !!}",
			paramName: "image",
			previewsContainer: false,
			uploadMultiple: false,
			parallelUploads: 1,
			acceptedFiles: "image/*",
					
			sending: function(file, xhr, formData) {
				formData.append( "_token", $("meta[name='csrf-token']").attr('content') );
			},
			uploadprogress: function(file, progress, bytesSent) {
				// upload progress percentage
			    $("#upload_cover_image_{!! $resource->id !!}").html(parseInt(progress) + "%");
			},

			success: function(file, responce) {
				// change image
				loadCourseImageForEdit('{!! $modelClass !!}', '{!! $resource->id !!}');
			}
		});
	</script>
</div>