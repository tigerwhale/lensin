<div class="{!! $class !!}" style="color: gray; padding-top: 7px;">
	<span title="{{ trans('text.views') }}"><i class="far fa-eye" aria-hidden="true" ></i> {!! isset($model->view_count) ? $model->view_count : 0 !!}</span> |
	<span title="{{ trans('text.downloads') }}"><i class="fa fa-download" aria-hidden="true" title="{{ trans('text.downloads') }}"></i> {!! $download_count !!}</span>
</div>