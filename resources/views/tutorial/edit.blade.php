@extends('layouts.resource')

@section('content')

	<div class="container">
	    {!! backendHeader('tutorials', 'tutorials', strtoupper(trans('text.tutorials')), 'tutorial')  !!}

		<!-- LEFT PART - IMAGE -->
		<div class="col-xs-3">
			<div class="row padding-small text-center">
				{{ strtoupper(trans('text.cover_image')) }}
				<div class="col-xs-12" id="cover_image_container_{!! $tutorial->id !!}">
					{!! coverImageEdit($tutorial) !!}
				</div>
			</div>
		</div>

		<div class="col-xs-9">

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-12">
					<!-- TITLE -->
					<label>{{ strtoupper(trans('text.title')) }}</label>
					<input type="text" name="title" data-model='tutorials' data-id={{ $tutorial->id }} value="{{ $tutorial->title }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
					<!-- AUTHOR -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.author')) }}</label>
					<input type="text" name="author" data-model='tutorials' data-id={{ $tutorial->id }} value="{{ $tutorial->author }}" placeholder="{{ strtoupper(trans('text.insert_author')) }}" class="form-control update_input" required>
					<!-- INSTITUTION -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.institution')) }}</label>
					<input type="text" name="institution" data-model='tutorials' data-id={{ $tutorial->id }} value="{{ $tutorial->institution }}" placeholder="{{ strtoupper(trans('text.insert_institution')) }}" class="form-control update_input" required>
					<!-- LANGUAGE -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.language')) }}</label>
					{!! Form::select('language_id', $languages, $tutorial->language_id,  ['class' => 'form-control update_input', 'required' => 'required', 'data-model' => 'tutorials', 'data-id'=> "$tutorial->id", 'placeholder' => trans('text.select_language') ]) !!}
					<!-- CATEGORY -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.category')) }}</label>
					<input type="text" id="category_input" name="category" data-model='tutorials' data-id={{ $tutorial->id }} value="{{ $tutorial->category }}" placeholder="{{ strtoupper(trans('text.insert_category')) }}" class="form-control update_input basicAutoComplete" autocomplete="off" required>
					<!-- DESCRIPTION -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.description')) }}</label>
					<textarea name="description" class="form-control update_input basicAutoComplete"  placeholder="{{ strtoupper(trans('text.description')) }}" data-model='tutorials' data-id={{ $tutorial->id }}>{{ $tutorial->description }}</textarea>
					<!-- RESOURCES -->
					<label style="margin-top: 20px">{{ strtoupper(trans('text.resources')) }}</label>
					<div id="lecture_{!! $tutorial->id !!}_resources" style="width: 100%;"></div>
				</div>
			</div>


		</div>
	</div>

	<script>
		$(document).ready(function () {
			var availableTags = [
				@foreach($uniqueCategories as $category)
					"{!! $category !!}",
				@endforeach
			];
			$( ".basicAutoComplete" ).autocomplete({
				source: availableTags
			});

			$(document).on('hide.bs.modal',function () {
				lecture_resources_load({!! $tutorial->id !!}, 'Tutorial');
			});

			lecture_resources_load({!! $tutorial->id !!}, 'Tutorial');
		});
	</script>

@endsection
