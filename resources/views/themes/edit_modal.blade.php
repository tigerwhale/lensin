<div class="modal-dialog modal-lg">
	<div class="modal-content">

		<!-- HEADER -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('text.theme') }}</h4>
		</div>

		<!-- CONTENT -->
		<div class="modal-body">

			<div class="row">

				<!-- CONTENT -->
				<div class="col-xs-12">

					<!-- NAME -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.theme_name')) }}
						</div>
						<div class="col-xs-10">
							<input type="text" name="name" data-model='projects/themes' data-id={{ $theme->id }} value="{{ $theme->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
						</div>
					</div>


					<!-- DESCRIPTION -->
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-2">
							{{ strtoupper(trans('text.description')) }}
						</div>
						<div class="col-xs-10">
							<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/themes' data-id={{ $theme->id }}>{{ $theme->description }}</textarea>
						</div>
					</div>
					
				</div>

			</div>

		</div>
	</div>
</div>