<div class="modal-dialog modal-lg">

	<!-- Modal content-->
	<div class="modal-content" style="font-size: 13pt;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">{{ trans('challenges.create_new_theme') }}</h4>
		</div>

		<div class="modal-body">

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.theme_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="name" class="form-control" placeholder="{{ strtoupper(trans('text.insert_name')) }}" required>
				</div>
			</div>

			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.description')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control" placeholder="{{ strtoupper(trans('text.description')) }}"></textarea>
				</div>
			</div>

			<!-- BUTTONS -->
			<div class="row" style="padding-top: 20px;">
				<div class="col-xs-12 text-right">
					<button class="btn btn-success" id="create_theme_btn">
						<i class="fa fa-check" aria-hidden="true"></i>
					</button>
					<button class="btn btn-danger" class="close" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>
			</div>	
			
		</div>
	</div>
</div>