<div class="container">

	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>{{ trans('text.edit_theme') }}</h2>
		</div>
	</div>

	<div class="row">

		<!-- CONTENT -->
		<div class="col-xs-12">

			<!-- NAME -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('text.theme_name')) }}
				</div>
				<div class="col-xs-10">
					<input type="text" name="name" data-model='projects/themes' data-id={{ $theme->id }} value="{{ $theme->name }}" placeholder="{{ strtoupper(trans('text.insert_title')) }}" class="form-control update_input" required>
				</div>
			</div>


			<!-- DESCRIPTION -->
			<div class="row" style="margin-top: 20px">
				<div class="col-xs-2">
					{{ strtoupper(trans('themes.theme_title')) }}
				</div>
				<div class="col-xs-10">
					<textarea name="description" class="form-control update_input" placeholder="{{ strtoupper(trans('text.description')) }}" data-model='projects/themes' data-id={{ $theme->id }}>{{ $theme->description }}</textarea>
				</div>
			</div>
			
		</div>

	</div>
</div>


<script type="text/javascript">

    $('.back-button').click(function(){
        window.location = '{!! url("/") !!}/projects/themes/modify';
        return false;
    }); 

</script>
