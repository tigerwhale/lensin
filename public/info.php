<span style="font-size: 15pt; font-family: Verdana; line-height: 25pt;">
<?php 

$version_larger  = version_compare(phpversion(), '5.6.4', ">=") ? "green" : "red";
echo "PHP version: <span style='color:$version_larger'>" . phpversion() . "</span><br>";
echo "tokenizer: " . (extension_loaded('tokenizer') ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "xml: " . (extension_loaded('xml') ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "Mbstring: " . (extension_loaded('Mbstring') ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "PDO: " . (extension_loaded('pdo') ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "OpenSSL: " . (extension_loaded('OpenSSL') ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "GD library: " . (isset(gd_info()['GD Version']) ? " <span style='color:green;'>Yes " . gd_info()['GD Version'] . "</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "cURL: " . (function_exists('curl_version') ? " <span style='color:green;'>Yes " . implode(curl_version()['protocols'], " - ") . "</span>" : "<span style='color:red;'>No</span>") . "<br>";

$color = (intval(substr(ini_get('post_max_size'), 0, -1)) >= 350) ? "green" : "red";
echo "post_max_size: <span style='color:$color;'>" . ini_get('post_max_size') . "</span><br>";

$color = (intval(substr(ini_get('upload_max_filesize'), 0, -1)) >= 350) ? "green" : "red";
echo "Upload_max_filesize: <span style='color:$color;'>" . ini_get('upload_max_filesize') . "</span><br>";

$color = (intval(substr(ini_get('memory_limit'), 0, -1)) >= 500) ? "green" : "red";
echo "memory_limit: <span style='color:$color;'>" . ini_get('memory_limit') . "</span><br>";

$color = (intval(ini_get('max_execution_time')) >= 60) ? "green" : "red";
echo "max_execution_time: <span style='color:$color;'>" . ini_get('max_execution_time') . "</span><br>";


if (function_exists('apache_get_modules')) {
  $modules = apache_get_modules();
  $mod_rewrite = in_array('mod_rewrite', $modules);
} else {
  $mod_rewrite =  getenv('HTTP_MOD_REWRITE')=='On' ? true : false ;
}
echo "mod_rewrite: " . ($mod_rewrite ? " <span style='color:green;'>Yes</span>" : "<span style='color:red;'>No</span>") . "<br>";
echo "sendmail path: " . ini_get('sendmail_path') . "<br>";

?>
</span>

<?php phpinfo(); ?>