var timer;

$(document).ready(function(){
   
      $(document).tooltip({
          content: function () {
              return $(this).prop('title');
          }
      });

    if (location.hash != null && location.hash != ""){
        $('.collapse').removeClass('in');
        $(location.hash + '.collapse').collapse('show');
        var hash = location.hash;
        
        setTimeout(function(){
            console.log(location.hash);
            $('html, body').animate({
                scrollTop: $( hash ).offset().top
            }, 500);
        } ,500);
        
    }

    $('body').on('change', '#preview', function(){
        var return_data = convertMedia($(this).val());
        if ( return_data )  {
            $(this).val(return_data)
        }
    });
  
   var maxed = false;

    $('body').on('click', '.full_screen_btn', function(){
        var element = document.getElementById( $(this).data('iframe-id') );

        if(maxed){
            if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen(); 
                maxed = false;
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
                maxed = false;
            }
        }else{
            if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
                maxed = true;
            } else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
                maxed = true;
            }
        }
    })


    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'albumLabel': ""
    });


    $('body').on('click', '.criteria-name', function(){
        $(".criteria-name").removeClass("criteria-selected");
        $(".criteria-name").removeClass("criteria-parent-selected");
        $(this).addClass("criteria-selected");
        var criteria_tab_url = $(this).data("server-url") + "/api/study_cases/criteria_tab";
        var data = $(this).data('criteria-id');
        selectCriteriaParents( $(this).data('parent') );
        ajax_post_to_div(criteria_tab_url, 'criteria_tab', data, realign_load);
    }); 

    function selectCriteriaParents(parent_id) {
        var parent = $('#' + parent_id);
        parent.addClass("criteria-parent-selected");
        if (parent.data('parent')) {
            selectCriteriaParents(parent.data('parent'));
        }
    }


    var realign_load = function() {
        var top_position = $(".criteria-selected:first").offset().top - 173;
        $(".select_by_criteria_tab").css({ top: top_position + 'px' });

        // Set the height of the mai div
        var tab_pane_height = $(".select_by_criteria_tab_pane").first().height();
        if ( tab_pane_height > $("#select_by_criteria").height() ) 
        {
            $("#select_by_criteria").height(tab_pane_height + 150);
        }
    }   

    // RESOURCE MODAL ARROWS
    $('body').on('click', '.arrow_modal', function(){
        $('#modal-' + $(this).data('this-modal-id') ).hide();
        $('#modal-' + $(this).data('open-modal-id') ).delay(100).show();
    });

    // ADD LECTURE CONTENT
    $('body').on('click', '.add_lecture_content', function(){
        var lecture_id = $(this).data('lecture-id');
        var token = $("meta[name='csrf-token']").attr("content"); 
        
        $.post({
            url: '/lectures/content/create/' + lecture_id,
            data: {
                '_token': token,
                'lecture_id': lecture_id,
            },
            success: function(data, status) {
                lecture_content_load(lecture_id);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        })

    });


    // REMOVE COVER IMAGE
    $('body').on('click', '.remove_cover_image_button', function(){

        var token = $("meta[name='csrf-token']").attr("content"); 
        var button = $(this);
        var model = $(this).data('model');
        var id = $(this).data('id');

        $.ajax({
            url: "/remove_cover_image/" + model + "/" + id,
            type: 'get',
            data: {
                '_token': token,
              },
            success: function(data, status) {
                loadCourseImageForEdit(model, id);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    })



    // MAKE TOOLTIPS
    $( document ).tooltip();

    // CREATE MODEL POPUP
    $('body').on('click', '.create_model', function(){
        var model_name = $(this).data('model-name');
        var token = $("meta[name='csrf-token']").attr("content");
        var title = $(this).data('title');
        var placeholder = $(this).data('placeholder');

        bootbox.prompt({
            title: title,
            placeholder: placeholder,
            callback: function(result){
                var create_url = "/" + model_name + "/create";
                if (result) {
                    $.ajax({
                        data: {
                            'name': result,
                            '_token': token,
                        },
                        url: create_url,
                        type: 'POST',
                        success: function(response) {
                            window.location.href = "/" + model_name + "/edit/" + response;
                        },
                        error: function(response){
                            bootbox.alert (response);
                        }
                    });
                }
            }
        });
    });

    $('body').on('click', '.no_propagation', function(){
        event.stopPropagation();
    });

    // UPDATE INPUT FIELD
    $('body').on('input', '.update_input', function(){
        updateInput(this);
    });

    // UPDATE INPUT FIELD
    $('body').on('change', '.update_input:radio', function(){
        updateInput(this);
    });

    function updateInput(object)
    {
        var delay = 600; // 0.3 seconds delay after last input
        var object = $(object);
        var value = $(object).val();
        var update_url = '/' + $(object).data('model') + '/update_data';
        var id = $(object).data('id');
        var name = $(object).attr('name');
        var token = $("meta[name='csrf-token']").attr("content");

        window.clearTimeout(timer);

        // Check text length
        var length = 190;
        if ( $(object).attr('maxlength') ) {
            length = $(object).attr('maxlength');
        }
        if ($(object).is("input") && value.length > length) {
            bootbox.alert('Input too long');
        } else {

            timer = window.setTimeout(function(){

                $.ajax({
                    data: {
                        'id': id,
                        'value': value,
                        'name': name,
                        '_token': token,
                    },
                    url: update_url,
                    type: 'POST',

                    success: function(response) {
                        if (response == "OK") {
                            object.animate({
                                backgroundColor: "#90EE90"
                            }, 300);
                            object.animate({
                                backgroundColor: "white"
                            }, 400).delay(400);

                        }
                        console.log(object);
                        if (object.data('callback')) {
                            eval(object.data('callback'))();
                        }
                    },
                    error: function(response){
                        bootbox.alert (response);
                    }
               });
            }, delay);
        }
    }


    // UPDATE CHECKBOX
    $('body').on('change', '.update_checkbox', function(){

        var object = $(this);
        var id = $(this).data('id');
        var id_belongs_to = $(this).data('id-belongs-to');
        var update_url = $(this).data('url');
        var name = $(this).attr('name');
        var token = $("meta[name='csrf-token']").attr("content");

        if ($(this).is(":checked")) {
            var value = 1;
        } else {
            var value = 0;
        }

        $.ajax({
            data: {
                'id': id,
                'value': value,
                'name': name,
                'id_belongs_to': id_belongs_to,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(response) {
                //alert(response);
            },
            error: function(response){
                bootbox.alert (response);
            }
       });
    });


    // DELETE RESOURCE
    $('body').on('click', '.delete-resource', function(){

        var update_url = '/' + $(this).data('model') + '/delete';
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content");
        var delete_id =  $(this).data('delete-id');
        var text =  $(this).data('text');

        bootbox.confirm(text, function(result){
            // if OK, delete the model
            if (result) {
                $.ajax({
                    data: {
                        'id': id,
                        '_token': token,
                    },
                    url: update_url,
                    type: 'POST',

                    success: function(html) {
                        // remove the div
                        $("#" + delete_id).fadeOut(500, function() { $(this).remove(); });
                    },

                    error: function(response) {
                        bootbox.alert("err" + response);
                    }

                });
            }
        });

    });


    // BUTTON AJAX
    $('body').on('click', '.ajax_btn', function(){

        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var data = $(this).data('data');
        var token = $("meta[name='csrf-token']").attr("content");
        var return_fn =  $(this).data('return_fn');
        var confirm_text =  $(this).data('confirm_text');

        if (confirm_text) {

            bootbox.confirm(confirm_text, function(result){
                // if OK, delete the model
                if (result) {

                    $.ajax({
                        data: {
                            'id': id,
                            'data': data,
                            '_token': token,
                        },
                        url: update_url,
                        type: 'POST',

                        success: function(return_data) {
                            // if there is a return function execute it with return data
                            if (return_fn) {
                                window[return_fn](return_data);
                            }
                        },

                        error: function(response) {
                            bootbox.alert("err" + response);
                        }

                    });
                }
            });
        }
    });


   // PUBLISH RESOURCE
    $('body').on('click', '.publish_resource', function(){

        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content");
        var object = $(this);

        $.ajax({
            data: {
                'id': id,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(html) {
                // update the icon
                object.html(html);
                $(".lecture_" + id + "_published").html(html);
                if (returnFunction) {
                    window[returnFunction]();
                }
            },
            error: function() {
                bootbox.alert('Error');
            }
        });
    });


   // PUBLISH RESOURCE OBJECT
    $('body').on('click', '.publish_object', function(){
        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content");
        var object = $(this);
        var returnFunction = $(this).data('return-function');
        var returnFunctionArgs = $(this).data('return-function-arguments');

        $.ajax({
            data: {
                'id': id,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(html) {
                // update the button
                object.replaceWith(html);

                if (returnFunction) {
                    window[returnFunction](returnFunctionArgs);
                }
            },
            error: function() {
                bootbox.alert('Error');
            }
        });
    });


   // PUBLISH RESOURCE OBJECT
    $('body').on('click', '.global_object', function(){

        var update_url = $(this).data('url');
        var id = $(this).data('id');
        var token = $("meta[name='csrf-token']").attr("content");
        var object = $(this);

        $.ajax({
            data: {
                'id': id,
                '_token': token,
            },
            url: update_url,
            type: 'POST',

            success: function(html) {
                // update the button
                object.replaceWith(html);
            },
            error: function() {
                bootbox.alert('Error');
            }
        });
    });


    // make modal on top
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });


    $(document).on('hide.bs.modal', '.modal', function () {
        $(this).remove();
    });

    // OPEN MODAL AFTER URL
    $('body').on('click', '.modal_url', function(){
        var close_function = $(this).data('close-function');
        var user_id = $(this).data('user-id');

        if ( close_function ){
            openModalFromUrl( $(this).data('url'), close_function, user_id );
        } else {
            openModalFromUrl( $(this).data('url'), "", user_id );
        }

    });

    $('body').on('click', '.collapse_arrow', function(){
        console.log($(this));
        $(this).find('.fa-caret-down,.fa-caret-up').first().toggleClass("fa-caret-down").toggleClass("fa-caret-up");
    });

    /**
     * Download resource and increment
     */
    $('body').on('click', '.download_resource', function(){
        console.log('radi');
        var incement_url = $(this).data('download-url');
        var resource_id = $(this).data('resource-id');

        $.ajax({
            type: 'POST',
            url: incement_url,
            data: {
                "resource_id": resource_id
            },
            success: function(file_url) {
                // Construct the <a> element
                var link = document.createElement("a");
                link.download = "";
                link.href = file_url;

                document.body.appendChild(link);
                link.click();

                // Cleanup the DOM
                document.body.removeChild(link);
                delete link;
            },
            error: function(response) {
                bootbox.alert("err" + response);
            }

        });
    });

    $('body').on('click', '.colapse_arrow', function(){
        $(this).children(":first").toggleClass("fa-caret-down").toggleClass("fa-caret-up");
    });


});

/**
 * Open Modal From URL
 * @param  {String} modal_url
 * @param  {String} close_function [description]
 */
function openModalFromUrl(modal_url, close_function, user_id) {

    if (close_function === undefined) {
            close_function = "";
    }
    if (user_id === undefined) {
            user_id = "";
    }

    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        data: {
            '_token': token,
            'user_id': user_id,
        },
        url: modal_url,
        type: 'POST',
        success: function(html) {
            // create a new modal div
            var modal = document.createElement("div");
            $(modal).addClass('modal fade');
            $(modal).attr( "role", "dialog" );

            // Add the modal div
            $('body').append(modal);
            // Place responce in the modal
            $(modal).html(html)
            // Open the modal
            $(modal).modal('show');
        },

        error: function() {
            // Display the error
            bootbox.alert('Error');  
        }
        
    });

}

/**
 * Line Limiter
 */
function lineLimit() {
    $(".line-limit").dotdotdot({
        ellipsis    : '... ',
        watch       : 'window'
    });
}


function download_files(files) {

    var token = $("meta[name='csrf-token']").attr("content"); 

    function download_next(i) {
        if(i >= files.length) {
            return;
        }

        $.get({
            data: { 
                '_token': token,
            },
            url: files[i].server_url + "/" + files[i].resource,
            type: 'POST',
            success: function(html) {

                var a = document.createElement('a');
                a.href = files[i].download;
                a.target = '_parent';
                // Use a.download if available, it prevents plugins from opening.
                if ('download' in a) {
                    a.download = files[i].filename;
                }
                // Add a to the doc for click to work.
                (document.body || document.documentElement).appendChild(a);
                if (a.click) {
                    a.click(); // The click method is supported by most browsers.
                } else {
                    $(a).click(); // Backup using jquery
                }
                // Delete the temporary link.
                a.parentNode.removeChild(a);
                // Download the next file with a small timeout. The timeout is necessary
                // for IE, which will otherwise only download the first file.
                setTimeout(function () { download_next(i + 1); }, 500);
            },            
            error: function() {
                // Display the error
                bootbox.alert('Error');  
            }
            
        });
    }

    // Initiate the first download.
    download_next(0);
}

// Study Case Image slideshow
function studyCaseImageSlideshow(study_case_id, server_url) 
{
    // check server_url
    if (server_url === undefined) { server_url = ""; }

    $("#studyCaseImageSlideshow").html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>');
    var token = $("meta[name='csrf-token']").attr("content"); 
    $.ajax({
        data: { 
            '_token': token,
        },
        url: server_url + '/study_cases/image_slideshow/' + study_case_id,
        type: 'POST',
        success: function(html) {
            $('#studyCaseImageSlideshow').html(html)
        },

        error: function() {
            // Display the error
            bootbox.alert('Error');  
        }
    });   
}

function select_resource_preview_file(resource_id, file_list)
{
    var token = $("meta[name='csrf-token']").attr("content");
    bootbox.prompt({
        title: "{{ trans('resources.select_resource_preview_file') }}",
        inputType: 'select',
        inputOptions: JSON.parse(file_list),
        callback: function (result) {
            $.ajax({
                url: '/resources/update_data',
                type: 'POST',
                data: {
                    '_token': token,
                    'name': 'preview',
                    'value': result,
                    'id': resource_id,
                },
                success: function(data, status) {
                    resourceModalFilePreviewUpdate(resource_id);
                },
                error: function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });     
        }
    });
}


function resourceModalFilePreviewUpdate(resource_id) {
    var token = $("meta[name='csrf-token']").attr("content"); 

    $.ajax({
        url: '/resources/file_preview_list/' + resource_id,
        type: 'GET',
        data: {
            '_token': token,
        },
        success: function(data, status) {
            $("#resource_file_and_preview").html(data);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });     
}


function publishButton(published, model, id, titleTextPublished, titleTextUnpublished) {

    if (published) {
        var transparent = "";
        var icon = "far fa-eye";
        var titleText = titleTextPublished;
    } else {
        var transparent = "transparent";
        var icon = "far fa-eye-slash";
        var titleText = titleTextUnpublished;
    }
    
    returnData = '<button type="button"';
    returnData += " class='btn bck-lens publish_object shaddow " + transparent + "'";
    returnData += " data-url='/" + model + "/publish/" + id + "'";
    returnData += " data-id='" + id + "' title='" + titleText + "'>";
    returnData += "<i class='";
    returnData += icon;
    returnData += "' aria-hidden='true'></i>";
    returnData += "</button> ";

    return returnData;
}

function globalButton(global, model, id, titleTextGlobal, titleTextLocal) {

    if (global) {
        var transparent = "";
        var icon = "fa fa-globe";
        var titleText = titleTextGlobal;
    } else {
        var transparent = "transparent";
        var icon = "far fa-circle";
        var titleText = titleTextLocal;
    }
    
    returnData = '<button type="button"';
    returnData += " class='btn bck-lens global_object shaddow " + transparent + "'";
    returnData += " data-url='/" + model + "/global/" + id + "'";
    returnData += " data-id='" + id + "' title='" + titleText + "'>";
    returnData += "<i class='";
    returnData += icon;
    returnData += "' aria-hidden='true'></i>";
    returnData += "</button> ";

    return returnData;
}


// LOAD COVER IMAGE 
function loadCourseImageForEdit(model, id) 
{
    var token = $("meta[name='csrf-token']").attr("content"); 
    $.ajax({
        url: '/cover_image/'+ model + '/' + id,
        type: 'POST',
        data: {
            '_token': token,
          },
        success: function(data) {
            $('body #cover_image_container_' + model + '_' + id).replaceWith(data);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });
};


// LOAD TUTORIAL RESOURCES
function  tutorial_resources_load(tutorial_id){
    console.log("A1");
    var token = $("meta[name='csrf-token']").attr("content");

    $.post({
        url: '/tutorials/resources_list_backend/' + tutorial_id,
        method: "POST",
        data: {
            '_token': token,
        },
        success: function(data, status) {
            $('body #tutorial_' + tutorial_id + '_resources').html(data);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    })
}


// LOAD LECTURE RESOURCES
function  lecture_resources_load(model_id, model_name = "Lecture"){
    var token = $("meta[name='csrf-token']").attr("content"); 
    $.post({
        url: '/reosurces/backend_resource_list',
        method: "POST",
        data: {
            '_token': token,
            'model_name': model_name,
            'model_id': model_id
        },
        success: function(data, status) {
            $('body #lecture_' + model_id + '_resources').html(data);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    })
}

// LOAD LECTURE CONTENT
function lecture_content_load(lecture_id) {
    
    var token = $("meta[name='csrf-token']").attr("content"); 

    $.post({
        url: '/lectures/content/list_for_lecture_modal/' + lecture_id,
            data: {
                '_token': token,
            },
            success: function(data, status) {
                $('body #lecture_' + lecture_id + '_content').html(data);
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
    });
}

function ajaxApiCall(url, container, data) {

    data._token = $("meta[name='csrf-token']").attr("content"); 

    $.post({
        url: url,
        data: data,
        success: function(returnData, status) {
            $(container).html(returnData);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });
}


function populate_div_with_request(api_url, view_url, container)
{
    var token = $("meta[name='csrf-token']").attr("content"); 

    $.post({
        url: api_url,
        data: {
            '_token': token,
        },
        success: function(api_return, status) {

            $.post({
                url: view_url,
                data: {
                    '_token': token,
                    'data' : api_return
                },
                success: function(view_return, status) {
                    $(container).html(view_return);
                },
                error: function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });                

        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });    
}



function postCall(url, return_function, data) {
    // check data var
    if (data = undefined) { data = []; }

    // get token
    data._token = $("meta[name='csrf-token']").attr("content"); 
    
    // POST Request
    $.post({
        url: url,
        data: data,
        success: function(returnData, status) {
            return return_function(returnData);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });
}

function getCall(url, return_function, data) {
    // check data var
    if (data = undefined) { data = []; }

    // get token
    data._token = $("meta[name='csrf-token']").attr("content"); 

    // GET Request
    $.get({
        url: url,
        data: data,
        success: function(returnData, status) {
            return_function(returnData);
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
        }
    });
}

function convertMedia(input_data){
    
    var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
    var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;
    var pattern3 = /([-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?(?:jpg|jpeg|gif|png))/gi;
    var is_link = false;
    var html = input_data; 

    if (pattern1.test(html)) {
       var pattern = pattern1;
       var replacement = 'player.vimeo.com/video/$1';
    }
    
    if (pattern2.test(html)) {
        var pattern = pattern2;
        var replacement = 'http://www.youtube.com/embed/$1';
    } 
    
    if (pattern3.test(html)) {
        var pattern = pattern3;
        var replacement = '<a href="$1" target="_blank"><img class="sml" src="$1" /></a><br />';
    } 

    if (pattern && replacement) {
        var r = confirm("Would you like this to be converted to a embed link?");
        if (r == true) {
            var return_var = html.replace(pattern, replacement);  
            return return_var;
        } 
    }      
       
    return false;
}

        
function getParams() {
    
    url = decodeURIComponent(window.location.href);

    var regex = /([^=&?]+)=([^&#]*)/g, params = {}, parts, key, value;

    while((parts = regex.exec(url)) != null) {

        key = parts[1], value = parts[2];
        var isArray = /\[\]$/.test(key);

        if(isArray) {
            params[key.slice(0,-2)] = params[key.slice(0,-2)] || [];
            params[key.slice(0,-2)].push(value);
        }
        else {
            params[key] = value;
        }
    }
    return params;
}

function truncateString(string, maxLength){
   if (string.length > maxLength)
      return string.substring(0, maxLength) + '...';
   else
      return string;
};

function toggleTransparentAndEyeClass(objectID) {
    $('#' + objectID).toggleClass('transparent crossed-eye-bck');
}

function toggleTransparentClass(objectID) {
    $('#' + objectID).toggleClass('transparent');
}