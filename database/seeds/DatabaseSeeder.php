<?php

use Illuminate\Database\Seeder;
use HttpOz\Roles\Models\Role;
use App\User;
use App\Course;
use App\CourseSubject;
use App\Lecture;
use App\Tool;
use App\Theme;
use App\Challenge;
use App\ChallengeGroup;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
		User::truncate();
		Course::truncate();
		CourseSubject::truncate();
		Lecture::truncate();
		Tool::truncate();
		Theme::truncate();
		Challenge::truncate();
		ChallengeGroup::truncate();


    	User::create([
	        'server_id'     => server_property("server_id"),
	        'language'      => 'en',
	        'country_id'    => App\Country::inRandomOrder()->first()->id,
	        'email'         => "administrator@lens-international.org",
	        'password'      => bcrypt('administrator'),
	        'name'          => "administrator",
	        'last_name'     => "administrator",
	        'phone'         => $faker->e164PhoneNumber,
	        'web'           => $faker->domainName,
	        'user_type_id'  => App\UserType::inRandomOrder()->first()->id,
	        'school'        => $faker->company,
	        'address'       => $faker->streetAddress,
	        'departement'   => "administrator",
	        'position'      => "administrator",
	        'interest'      => $faker->sentence,
	        'gps'           => rand(0,50) . ", " . rand(0,50),
	        'facebook_id'   => rand(100,50000),
	        'twitter_id'    => rand(100,50000),
	        'google_id'     => rand(100,50000),
	        'linkedin_id'   => rand(100,50000),
	        'validated'     => 1,
	        'newsletter'    => rand(0,1),
	        'public'        => 0,
	        'birthday'      => $faker->date(),
	        'note'          => $faker->sentence()

		]);
		
		factory(User::class, 20)->create();
		factory(Tool::class, 20)->create();
	
		factory(Course::class, 20)->create()->each(function ($course) 
		{
			factory(CourseSubject::class, rand(2,8))->create()->each( function ($courseCubject) use ($course) 
			{
				$courseCubject->created_by = $course->user_id;
				$courseCubject->course_id = $course->id;
				$courseCubject->save();

				factory(Lecture::class, rand(1,15))->create()->each( function ($lecture) use ($courseCubject)  
				{
					$lecture->created_by = $courseCubject->user_id;
					$lecture->course_subject_id = $courseCubject->id;
					$lecture->save();
				});

			});
		});

		factory(Theme::class, 5)->create()->each( function ($theme) 
		{
			factory(Challenge::class, 5)->create()->each( function ($challenge) use ($theme) 
			{
				$challenge->created_by = $theme->created_by;
				$challenge->theme_id = $theme->id;
				$challenge->save();
				factory(ChallengeGroup::class, 5)->create()->each( function ($challengeGroup) use ($challenge) 
				{
					$challengeGroup->challenge_id = $challenge->id;
					$challengeGroup->save();
					$challengeGroup->users()->attach(User::all()->random(rand(3,10)));
					$challengeGroup->teachers()->attach(User::all()->random(rand(1,5)));

				});

			});
				
		});
		
    }
}

