<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts18 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "users_assigned_to_group", "text" => "Users assigned to group"],
            ["locale" => "en" , "group" => "challenges", "item" => "detach_user_from_group", "text" => "Detach user from group"],
            ["locale" => "en" , "group" => "challenges", "item" => "user_already_assigned", "text" => "User is already in the group."],
            ["locale" => "en" , "group" => "challenges", "item" => "detach_user_from_group_question", "text" => "Detach user from group?"]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
