<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyCaseGuidelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_case_guidelines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('study_case_guideline_id')->nullable();
            $table->integer('created_by');
            $table->string('name')->nullable();
            $table->integer('order');
            $table->integer('level');
            $table->string('reference')->nullable();
            $table->tinyinteger('published')->nullable();
            $table->timestamps();
            $table->softDeletes();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_case_guidelines');
    }
}
