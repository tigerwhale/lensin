<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('licences', function (Blueprint $table) {
            $table->string('icon')->nullable();
        });

        $licence = \App\Licence::where('name', "Attribution")->first();
        $licence->icon = "&#x2550;";
        $licence->save();
        
        $licence = \App\Licence::where('name', "Share-alike")->first();
        $licence->icon = "&#x2551;";
        $licence->save();

        $licence = \App\Licence::where('name', "Non-commercial")->first();
        $licence->icon = "&#x2552;";
        $licence->save();

        $licence = \App\Licence::where('name', "Non-derivative")->first();
        $licence->icon = "&#x2553;";
        $licence->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
