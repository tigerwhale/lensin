<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts49 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "reference_person", "text" => "Reference person"],
            ["locale" => "en" , "group" => "text", "item" => "reference_person_email", "text" => "Reference person email"],
            ["locale" => "en" , "group" => "text", "item" => "web_site", "text" => "Website"],
        ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
