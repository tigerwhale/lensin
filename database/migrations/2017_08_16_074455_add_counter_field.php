<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCounterField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenges', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('courses', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('lectures', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('news', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('pages', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('resources', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('study_cases', function (Blueprint $table) { $table->integer('view_count')->default(0); });
        Schema::table('tools', function (Blueprint $table) { $table->integer('view_count')->default(0); });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
