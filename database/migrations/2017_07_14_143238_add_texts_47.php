<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts47 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "members", "text" => "Members"],
            ["locale" => "en" , "group" => "text", "item" => "partners", "text" => "Partners"]
        ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
