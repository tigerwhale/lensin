<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLargeIconsResourceTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource_types', function (Blueprint $table) { 
            $table->string('icon_2')->nullable(); 
            $table->string('icon_3')->nullable(); 
            $table->string('icon_4')->nullable(); 
            $table->string('icon_5')->nullable(); 
        });

        $resource_type = \App\ResourceType::where('name', 'slideshow')->first();
        $resource_type->icon_2 = '<i class="fa fa-window-restore fa-2x" aria-hidden="true"></i>';
        $resource_type->icon_3 = '<i class="fa fa-window-restore fa-3x" aria-hidden="true"></i>';
        $resource_type->icon_4 = '<i class="fa fa-window-restore fa-4x" aria-hidden="true"></i>';
        $resource_type->icon_5 = '<i class="fa fa-window-restore fa-5x" aria-hidden="true"></i>';
        $resource_type->save();

        $resource_type = \App\ResourceType::where('name', 'video')->first();
        $resource_type->icon_2 = '<i class="fa fa-file-audio-o fa-2x" aria-hidden="true"></i>';
        $resource_type->icon_3 = '<i class="fa fa-file-audio-o fa-3x" aria-hidden="true"></i>';
        $resource_type->icon_4 = '<i class="fa fa-file-audio-o fa-4x" aria-hidden="true"></i>';
        $resource_type->icon_5 = '<i class="fa fa-file-audio-o fa-5x" aria-hidden="true"></i>';
        $resource_type->save();

        $resource_type = \App\ResourceType::where('name', 'document')->first();
        $resource_type->icon_2 = '<i class="fa fa-file-o fa-2x" aria-hidden="true"></i>';
        $resource_type->icon_3 = '<i class="fa fa-file-o fa-3x" aria-hidden="true"></i>';
        $resource_type->icon_4 = '<i class="fa fa-file-o fa-4x" aria-hidden="true"></i>';
        $resource_type->icon_5 = '<i class="fa fa-file-o fa-5x" aria-hidden="true"></i>';
        $resource_type->save();        

        $resource_type = \App\ResourceType::where('name', 'audio')->first();
        $resource_type->icon_2 = '<i class="fa fa-volume-up fa-2x" aria-hidden="true"></i>';
        $resource_type->icon_3 = '<i class="fa fa-volume-up fa-3x" aria-hidden="true"></i>';
        $resource_type->icon_4 = '<i class="fa fa-volume-up fa-4x" aria-hidden="true"></i>';
        $resource_type->icon_5 = '<i class="fa fa-volume-up fa-5x" aria-hidden="true"></i>';
        $resource_type->save();        

        $resource_type = \App\ResourceType::where('name', 'other')->first();
        $resource_type->icon_2 = '<i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i>';
        $resource_type->icon_3 = '<i class="fa fa-ellipsis-h fa-3x" aria-hidden="true"></i>';
        $resource_type->icon_4 = '<i class="fa fa-ellipsis-h fa-4x" aria-hidden="true"></i>';
        $resource_type->icon_5 = '<i class="fa fa-ellipsis-h fa-5x" aria-hidden="true"></i>';
        $resource_type->save();             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
