<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts180 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('text', 'download_document', 'Download document');
        add_migration_text('text', 'close_and_mark_as_unresolved', 'Cloase and mark as unresolved');
        add_migration_text('text', 'close_and_mark_as_resolved', 'Cloase and mark as resolved');
        add_migration_text('text', 'new_network_requests', 'New network requests');
        add_migration_text('text', 'resloved', 'Resolved');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
