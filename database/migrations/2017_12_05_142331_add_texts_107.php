<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts107 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("text", "home_page_video_thumbnail", "Home page video thumbnail");
        add_migration_text("text", "home_video_poster_uploaded", "Home page video thumbnail uploaded");
        add_migration_text("text", "home_video_poster_deleted", "Home page video thumbnail deleted");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
