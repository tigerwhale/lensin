<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts60 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "launch_a_new_lens_platform", "text" => "Launch a new LeNS platform"],
            ["locale" => "en" , "group" => "text", "item" => "launch_a_new_lens_platform_text", "text" => "
                    Any research and/or educational institutions (in design), belonging to a region where a LeNS network doesn’t exist yet, if interested and committed, can decide to incubate and then launch a new regional LeNS. A new regional LeNS can download free a LeNS web platform and customize it on a local server, i.e. managing the same platform as local administrator, in particular enabling local partners to upload learning resources on design for sustainability. As far as the new local platform will be connected to all others (via a central platform), all is uploaded will be visible and downloadable by all.
                    <br>
                    <b>How to launch a new regional LeNS network</b>
                    <br>
                    Please fill in the record aside and press register. The LeNS international staff will contact you when your request has been accepted, i.e. you will appear on the LeNS regional list with all Partners you committed to join you that far.
            "],

        ]);  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
