<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts54 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "to_become_a_member_please_register_as_participant", "text" => "In order to register a as a member, please register as participant first."],
            ["locale" => "en" , "group" => "users", "item" => "to_become_a_partner_please_register_as_participant", "text" => "In order to register a as a partner, please register as participant first."],
            ["locale" => "en" , "group" => "users", "item" => "to_regiter_a_new_network_please_register_as_participant", "text" => "In order to register a new LeNS network, please register as participant first."],
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
