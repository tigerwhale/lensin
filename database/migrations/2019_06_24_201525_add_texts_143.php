<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts143 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('users', "downgrade_partner_to_member", 'Downgrade partner to member');
        add_migration_text('users', "member_group_created", 'Member group created');
        add_migration_text('users', "you_are_now_a_part_of", 'You are now a part of ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
