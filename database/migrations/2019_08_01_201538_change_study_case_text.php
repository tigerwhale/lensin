<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStudyCaseText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $texts = \App\LanguageTranslation::where('text', 'like', 'Study cases')->get();
        foreach ($texts as $text) {
            $text->text = 'Case studies';
            $text->save();
        }

        $texts = \App\LanguageTranslation::where('text', 'like', 'Study case')->get();
        foreach ($texts as $text) {
            $text->text = 'Case study';
            $text->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
