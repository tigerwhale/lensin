<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts141 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('text', "logo", 'Logo');
        add_migration_text('text', "click_to_select_document", 'Click here to select a document for upload');
        add_migration_text('users', "register_participant_text", 'You can register as a participant here.');
        add_migration_text('users', "to_become_a_member_please_register_as_participant", 'To become a member please register as a participant first.');
        add_migration_text('users', "to_become_a_partner_please_register_as_participant", 'To become a partner please register as a participant first.');
        add_migration_text('users', "to_regiter_a_new_network_please_register_as_participant", 'To register a new network please register as a participant first.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
