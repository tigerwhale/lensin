<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTextStudyCases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //study_cases
        $translations = \App\LanguageTranslation::where('group', 'LIKE', 'text')->where('item', 'LIKE', 'study_cases')->get();
        foreach ($translations as $translation) {
            $translation->text = 'Cases/Criteria';
            $translation->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
