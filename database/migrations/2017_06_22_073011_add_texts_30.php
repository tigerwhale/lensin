<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts30 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "courses", "item" => "create_new_course", "text" => "Create a new course"],
            ["locale" => "en" , "group" => "lectures", "item" => "delete_lecture", "text" => "Delete lecture"],
            ["locale" => "en" , "group" => "text", "item" => "published_click_to_unpublish", "text" => "Published. Click to unpublish."],
            ["locale" => "en" , "group" => "text", "item" => "not_published_click_to_publish", "text" => "Unpublished. Click to publish."],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
