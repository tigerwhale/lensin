<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText126 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('text', "platform_version", 'Platform Version: ');
        add_migration_text('text', "networks", 'Networks');
        add_migration_text('network', "member_name", 'Member name');
        add_migration_text('network', "contacts", 'Contacts');
        add_migration_text('network', "create_new_contact", 'Create a new contact');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
