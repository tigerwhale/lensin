<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('data');
            $table->timestamps();
        });

        DB::table('server_properties')->insert([
            ["name" => "server_name", "data" => "Lens"], 
            ["name" => "server_id", "data" => ""], 
            ["name" => "default_locale", "data" => "en"], 
            ["name" => "google_recapcha_key", "data" => "en"], 
            ["name" => "social_icons", "data" => '{"facebook":"http://facebook.com", "linkedin":"http://linkedin.com", "google_plus":"http://google.com",  "instagram":"http://instagram.com"}'], 
            ["name" => "central_server", "data" => '0'], 
            ["name" => "central_server_address", "data" => "http://www.lens-international.org"], 
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_properties');
    }
}
