<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIconsResourceTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $r = \App\ResourceType::where('name', 'slideshow')->first();
        $r->icon = "a";
        $r->save();
        $r = \App\ResourceType::where('name', 'video')->first();
        $r->icon = "b";
        $r->save();
        $r = \App\ResourceType::where('name', 'audio')->first();
        $r->icon = "c";
        $r->save();
        $r = \App\ResourceType::where('name', 'document')->first();
        $r->icon = "d";
        $r->save();
        $r = \App\ResourceType::where('name', 'other')->first();
        $r->icon = "f";
        $r->save();
        $r = \App\ResourceType::where('name', 'ppt-audio')->first();
        $r->icon = "e";
        $r->save();        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
