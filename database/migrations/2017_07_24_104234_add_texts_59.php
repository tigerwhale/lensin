<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts59 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "leave_member", "text" => "Leave member group"],
            ["locale" => "en" , "group" => "users", "item" => "leave_member_question", "text" => "Are you sure you want to leave this member group?"],
            ["locale" => "en" , "group" => "users", "item" => "leave_partner", "text" => "Leave partner group"],
            ["locale" => "en" , "group" => "users", "item" => "leave_partner_question", "text" => "Are you sure you want to leave this partner group?"],
        ]);  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
