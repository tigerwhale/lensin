<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts51 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "no_member_groups", "text" => "There are currently no active Members"],
            ["locale" => "en" , "group" => "users", "item" => "join_member_group", "text" => "Join a member"],
            ["locale" => "en" , "group" => "users", "item" => "join_member_text", "text" => "Join a member text description"],
            ["locale" => "en" , "group" => "users", "item" => "create_member_group", "text" => "Create a Member"],
            ["locale" => "en" , "group" => "users", "item" => "create_member_group_text", "text" => "Create a member description"],
            ["locale" => "en" , "group" => "text", "item" => "join", "text" => "Join"],
            ["locale" => "en" , "group" => "users", "item" => "university_type", "text" => "University"],
            ["locale" => "en" , "group" => "users", "item" => "school_type", "text" => "School"],
            ["locale" => "en" , "group" => "users", "item" => "company_type", "text" => "Company"],
            ["locale" => "en" , "group" => "users", "item" => "ngo_type", "text" => "NGO"],
            ["locale" => "en" , "group" => "users", "item" => "goverment_institution_type", "text" => "Goverment institution"],
            ["locale" => "en" , "group" => "users", "item" => "others_type", "text" => "Others"],
            ["locale" => "en" , "group" => "users", "item" => "member_group_created", "text" => "Member created. Administrator will be notified."],
        ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
