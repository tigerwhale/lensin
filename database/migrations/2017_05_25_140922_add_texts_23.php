<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts23 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "update_password", "text" => "Update password"],
            ["locale" => "en" , "group" => "users", "item" => "new_password", "text" => "New password"],
            ["locale" => "en" , "group" => "users", "item" => "old_password", "text" => "Old password"],
            ["locale" => "en" , "group" => "users", "item" => "change_password", "text" => "Change password"],
            ["locale" => "en" , "group" => "text", "item" => "search_results", "text" => "Search results"],
            ["locale" => "en" , "group" => "text", "item" => "sort_by", "text" => "Sort by"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
