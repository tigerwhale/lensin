<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts66 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'platform')->first();
        $translation->text = "LeNS Platform";
        $translation->save();
        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'join_as_participant')->first();
        $translation->text = "Join as Participant";
        $translation->save();

        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "are_you_sure_you_want_to_leave_this_page", "text" => "Are you sure you want to leave this page?"],
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
