<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts37 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "resource_types", "item" => "video", "text" => "Video"],
            ["locale" => "en" , "group" => "resource_types", "item" => "slideshow", "text" => "Slideshow"],
            ["locale" => "en" , "group" => "resource_types", "item" => "document", "text" => "Document"],
            ["locale" => "en" , "group" => "resource_types", "item" => "audio", "text" => "Audio"],
            ["locale" => "en" , "group" => "resource_types", "item" => "image", "text" => "Image"],
            ["locale" => "en" , "group" => "resource_types", "item" => "other", "text" => "Other"]
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
