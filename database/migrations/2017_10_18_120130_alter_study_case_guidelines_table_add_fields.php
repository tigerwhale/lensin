<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStudyCaseGuidelinesTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Year, Institution, Creative Commons license, Language
        Schema::table('study_case_guidelines', function (Blueprint $table) { 
            $table->string('year', 4)->nullable(); 
            $table->string('institution')->nullable(); 
            $table->integer('licence_id')->nullable();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
