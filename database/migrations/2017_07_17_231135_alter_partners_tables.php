<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPartnersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners_groups', function (Blueprint $table) {
            $table->integer('created_by_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('image')->nullable();
            $table->string('type')->nullable();
            $table->string('reference_person')->nullable();
            $table->string('reference_person_email')->nullable();
            $table->string('address')->nullable();
            $table->string('web_site')->nullable();
            $table->string('email')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->softDeletes();
        });


        Schema::create('partners_group_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partners_group_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
