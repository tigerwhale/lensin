<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText123 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("users", "join_existing_partner_group", "Jopin an existing partner group");
        add_migration_text("users", "partners_logo", "Partner logo");

        add_migration_text("text", "launch_a_new_lens_platform", "Launch a new LeNS platform");
        add_migration_text("text", "upload_filled_document", "Upload filled document");
        add_migration_text("text", "launch_a_new_lens_platform_text", "\n                    Any research and/or educational institutions (in design), belonging to a region where a LeNS network doesn’t exist yet, if interested and committed, can decide to incubate and then launch a new regional LeNS. A new regional LeNS can download free a LeNS web platform and customize it on a local server, i.e. managing the same platform as local administrator, in particular enabling local partners to upload learning resources on design for sustainability. As far as the new local platform will be connected to all others (via a central platform), all is uploaded will be visible and downloadable by all.\n                    <br>\n                    <b>How to launch a new regional LeNS network</b>\n                    <br>\n                    Please fill in the record aside and press register. The LeNS international staff will contact you when your request has been accepted, i.e. you will appear on the LeNS regional list with all Partners you committed to join you that far.\n");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
