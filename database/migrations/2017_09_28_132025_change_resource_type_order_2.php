<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeResourceTypeOrder2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $r = \App\ResourceType::where('name', 'video')->first();
        $r->order = 1;
        $r->save();
        $r = \App\ResourceType::where('name', 'slideshow')->first();
        $r->order = 2;
        $r->save();  
        $r = \App\ResourceType::where('name', 'ppt-audio')->first();
        $r->order = 3;
        $r->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
