<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLanguagesInModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::table('challenges')->update(['language_id' => "en"]);
        DB::table('courses')->update(['language_id' => "en"]);
        DB::table('lectures')->update(['language_id' => "en"]);
        DB::table('news')->update(['language_id' => "en"]);
        DB::table('study_cases')->update(['language_id' => "en"]);
        DB::table('tools')->update(['language_id' => "en"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
