<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts98 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("criteria", "create_dimension", "Create a new dimension");
        add_migration_text("criteria", "create_criteria", "Create a new criteria ");
        add_migration_text("criteria", "create_guideline", "Create a new guideline");
        add_migration_text("criteria", "create_subguideline", "Create a new subguideline");
        add_migration_text("criteria", "delete_dimension", "Delete dimension");
        add_migration_text("criteria", "delete_criteria", "Delete criteria");
        add_migration_text("criteria", "delete_guideline", "Delete guideline");
        add_migration_text("criteria", "delete_subguideline", "Delete subguideline");
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
