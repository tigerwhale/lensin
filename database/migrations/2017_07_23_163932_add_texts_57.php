<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts57 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "partners_group_created", "text" => "Partners group creater"],
            ["locale" => "en" , "group" => "users", "item" => "join_partner_group", "text" => "Join partner group"],
            ["locale" => "en" , "group" => "users", "item" => "create_partner_group", "text" => "Create partner group"],
            ["locale" => "en" , "group" => "users", "item" => "no_partner_groups", "text" => "No partners"],
            ["locale" => "en" , "group" => "users", "item" => "join_partner_text", "text" => "Here you can join a partner group."],
        ]);       
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
