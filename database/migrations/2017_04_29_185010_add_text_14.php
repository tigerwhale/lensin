<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText14 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "resources", "item" => "download_all", "text" => "Download all"],
            ["locale" => "en" , "group" => "resources", "item" => "add_resource", "text" => "Create a new resource"],
            ["locale" => "en" , "group" => "text", "item" => "or", "text" => "or"]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
