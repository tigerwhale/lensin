<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTextHomepage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = \App\PageText::where('page_id', 1)->first();
        $text->text = '<p style="margin-top: 20px;">View videos, slideshows, tools, projects and study cases<br /> to learn designing for sustainability for all<br /> Download and adapt them to your needs and desires<br /> to support preparing your own course</p>';
        $text->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
