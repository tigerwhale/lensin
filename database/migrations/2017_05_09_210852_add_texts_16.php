<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts16 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "project_name", "text" => "Project name"],
            ["locale" => "en" , "group" => "challenges", "item" => "group_name", "text" => "Group name"],
            ["locale" => "en" , "group" => "challenges", "item" => "enter_group_and_project_name", "text" => "Please enter group and project name."],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
