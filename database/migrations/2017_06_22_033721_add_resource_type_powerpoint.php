<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResourceTypePowerpoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('resource_types')->truncate();
        
        DB::table('resource_types')->insert([
            ["name" => "video", "icon" => '<i class="fa fa-play" aria-hidden="true"></i>'], 
            ["name" => "document", "icon" => '<i class="fa fa-file-o" aria-hidden="true"></i>'], 
            ["name" => "audio", "icon" => '<i class="fa fa-volume-up" aria-hidden="true"></i>'], 
            ["name" => "image", "icon" => '<i class="fa fa-picture-o" aria-hidden="true"></i>'], 
            ["name" => "slideshow", "icon" => '<i class="fa fa-clone" aria-hidden="true"></i>'],
            ["name" => "other", "icon" => '<i class="fa fa-ellipsis-h" aria-hidden="true"></i>']            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
