<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts25 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "courses", "item" => "edit_resource", "text" => "Edit resource"],
            ["locale" => "en" , "group" => "resources", "item" => "select_resources_you_want_to_publish", "text" => "Select resource you want to publish"],
            ["locale" => "en" , "group" => "resources", "item" => "select_resources_you_want_to_delete", "text" => "Select resource you want to delete"],
            ["locale" => "en" , "group" => "resources", "item" => "delete_multiple_question", "text" => "Are you sure you want to delete selected resources?"],
            ["locale" => "en" , "group" => "courses", "item" => "course_title", "text" => "Course title"],
            ["locale" => "en" , "group" => "challanges", "item" => "challenge_title", "text" => "Challange title"],
            ["locale" => "en" , "group" => "study_cases", "item" => "study_case_title", "text" => "Study case title"],
            ["locale" => "en" , "group" => "themes", "item" => "theme_title", "text" => "theme title"],
            ["locale" => "en" , "group" => "tools", "item" => "tool_name", "text" => "Tool title"]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
