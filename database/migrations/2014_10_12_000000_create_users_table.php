<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id');
            $table->string('language', 2);
            $table->integer('country_id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('last_name');
            $table->string('phone')->nullable();
            $table->string('web')->nullable();
            $table->integer('user_type_id');
            $table->string('school')->nullable();
            $table->string('address')->nullable();
            $table->string('departement')->nullable();
            $table->string('position')->nullable();
            $table->string('interest')->nullable();
            $table->string('gps')->nullable();
            $table->string('image')->nullable();            
            $table->string('facebook_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('linkedin_id')->nullable();
            $table->tinyInteger('validated')->nullable();
            $table->tinyInteger('newsletter')->nullable();
            $table->tinyInteger('public')->nullable();
            $table->text('note')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'birthday',
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
