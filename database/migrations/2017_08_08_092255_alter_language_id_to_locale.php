<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLanguageIdToLocale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenges', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\Challenge::where("language_id")->update(['language_id' => 'en']);

        Schema::table('courses', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\Course::where("language_id")->update(['language_id' => 'en']);

        Schema::table('lectures', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\Lecture::where("language_id")->update(['language_id' => 'en']);

        Schema::table('news', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\News::where("language_id")->update(['language_id' => 'en']);

        Schema::table('study_cases', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\StudyCase::where("language_id")->update(['language_id' => 'en']);

        Schema::table('tools', function (Blueprint $table) {
            $table->string('language_id', 2)->change();
        }); 
        \App\Tool::where("language_id")->update(['language_id' => 'en']);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
