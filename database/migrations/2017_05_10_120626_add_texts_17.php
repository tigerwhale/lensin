<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts17 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "edit_group", "text" => "Edit group"],
            ["locale" => "en" , "group" => "challenges", "item" => "group_teachers", "text" => "Group teachers"],
            ["locale" => "en" , "group" => "challenges", "item" => "assign_teacher", "text" => "Assign teacher"],
            ["locale" => "en" , "group" => "challenges", "item" => "detach_teacher_from_group", "text" => "Detach teacher from group"],
            ["locale" => "en" , "group" => "challenges", "item" => "detach_teacher_from_group_question", "text" => "Are you sure you want to detach teacher from group"],
            ["locale" => "en" , "group" => "challenges", "item" => "teacher_already_assigned", "text" => "This teacher is already assigned to the group."],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
