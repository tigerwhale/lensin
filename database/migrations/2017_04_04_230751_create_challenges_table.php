<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{  
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->integer('server_id')->nullable();
            $table->integer('theme_id');
            $table->string('name');
            $table->string('course');
            $table->string('year');
            $table->text('description')->nullable();
            $table->tinyInteger('comments_enabled')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->tinyInteger('archived')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
