<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts19 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "delete_project_question", "text" => "Are you sure you want to delete this project?"],
            ["locale" => "en" , "group" => "challenges", "item" => "delete_project", "text" => "Delete project"],
            ["locale" => "en" , "group" => "challenges", "item" => "modify_project", "text" => "Modify project"],
            ["locale" => "en" , "group" => "challenges", "item" => "challenge", "text" => "Challenge"],
            ["locale" => "en" , "group" => "challenges", "item" => "create_new_project", "text" => "Create a new project"],
            ["locale" => "en" , "group" => "challenges", "item" => "create_new_challange", "text" => "Create a new challenge"],
            ["locale" => "en" , "group" => "challenges", "item" => "create_new_theme", "text" => "Create a new theme"],
            ["locale" => "en" , "group" => "challenges", "item" => "question_delete_theme", "text" => "Are you sure you want to delete this theme?"],
            ["locale" => "en" , "group" => "challenges", "item" => "no_attachments", "text" => "No attachments"],
            ["locale" => "en" , "group" => "challenges", "item" => "attachments", "text" => "Attachments"],
            ["locale" => "en" , "group" => "challenges", "item" => "no_students_assigned", "text" => "No students assigned"],
            ["locale" => "en" , "group" => "text", "item" => "students", "text" => "Students"],
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
