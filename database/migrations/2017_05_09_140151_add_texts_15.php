<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts15 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "groups_assigned_to_challenge", "text" => "Groups assigned to challenge"],
            ["locale" => "en" , "group" => "challenges", "item" => "assign_group_to_challenge", "text" => "Assign group to challenge"],
            ["locale" => "en" , "group" => "challenges", "item" => "no_groups_assigned_to_challenge", "text" => "No group assigned to challenge"],
            ["locale" => "en" , "group" => "challenges", "item" => "create_new_group", "text" => "Create a new group"],
            ["locale" => "en" , "group" => "challenges", "item" => "no_groups", "text" => "No groups."],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
