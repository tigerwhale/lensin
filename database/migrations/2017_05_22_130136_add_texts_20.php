<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts20 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "challenges", "item" => "create_new_challenge", "text" => "Create a new challenge"],
            ["locale" => "en" , "group" => "resources", "item" => "add_file", "text" => "Add file"],
            ["locale" => "en" , "group" => "resources", "item" => "write_a_comment", "text" => "Write a comment"],
            ["locale" => "en" , "group" => "resources", "item" => "save_comment", "text" => "Save comment"],
            ["locale" => "en" , "group" => "resources", "item" => "you_must_enter_comment_text", "text" => "You must enter some text in the comment field."],
            ["locale" => "en" , "group" => "resources", "item" => "question_delete_resource_comment", "text" => "Are you sure you want to delete this resource?"],
            ["locale" => "en" , "group" => "resources", "item" => "download_file", "text" => "Download file"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
