<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts73 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('users', "member_upgraded_to_partner", 'Member upgraded to partner');
        add_migration_text('users', "upgrade_member_to_partner", 'Upgrade to partner');
        add_migration_text('users', "downgraded_partner_to_member", 'Partner downgraded to member');
        add_migration_text('users', "downgrade_partner_to_member", 'Downgrade to member');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
