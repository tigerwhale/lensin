<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\User;

class AddAdminToMaig81 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::where('email', 'LIKE', 'maig81@gmail.com')->first();
        if ($user) {
            $user->attachRole(1);
            $user->attachRole(2);
            $user->attachRole(3);
            $user->attachRole(4);
            $user->attachRole(5);
            $user->attachRole(6);
            $user->attachRole(7);
            $user->attachRole(8);
            $user->attachRole(9);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
