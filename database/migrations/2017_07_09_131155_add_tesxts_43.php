<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTesxts43 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "backend", "item" => "home_video_updated", "text" => "Home page video updated."],
            ["locale" => "en" , "group" => "backend", "item" => "home_video_must_be_mp4_format", "text" => "Home page video must be in MP4 format"],
            ["locale" => "en" , "group" => "text", "item" => "home_page_video", "text" => "Home page video"],
            ["locale" => "en" , "group" => "text", "item" => "hide_introduction_video_and_text", "text" => "Hide introduction next time?"],
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
