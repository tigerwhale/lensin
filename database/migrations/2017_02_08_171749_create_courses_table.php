<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('school')->nullable();
            $table->string('type')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->integer('year')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
