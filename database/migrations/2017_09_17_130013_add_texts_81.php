<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts81 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("backend", "home_video_set_to_default", "Home video set to default.");
        add_migration_text("backend", "home_video_deleted", "Home video deleted");
        add_migration_text("text", "return_to_default", "Return to defaut");
        add_migration_text("text", "upload", "Upload");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
