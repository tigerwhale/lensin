<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Network;
use \App\NetworkMember;

class AddNetwork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ns = Network::all();
        foreach ($ns as $n) {
            $n->forceDelete();
        }
        
        $ns = NetworkMember::all();
        foreach ($ns as $n) {
            $n->forceDelete();
        }

        $network = Network::create([ 'name' => 'LeNS_Africa', 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => 'Cape Peninsula University of Technology (CPUT), South Africa', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Retha De La Harpe', 'email' => 'delaharper@cput.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ephias Ruhode', 'email' => 'RuhodeE@cput.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Johan Van Niekerk', 'email' => 'VanNiekerkJ@cput.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mugendi M’Rithaa', 'email' => 'mugendim@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Munira Alliem', 'email' => 'alliem@cput.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Stellenbosch University (SUN), South Africa', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Elmarie Costandius', 'email' => 'elmarie@sun.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Karolien Perold-Bull', 'email' => 'karolien@sun.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Marjorie Naidoo', 'email' => 'marjorie.naidoo@telkomsa.net' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Andrea Broom', 'email' => 'andrea.broom@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Farm and Garden National Trust', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Elmarie Costandius', 'email' => 'elmarie@sun.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Cape Craft and Design Institute NPC', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Karolien Perold-Bull', 'email' => 'karolien@sun.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Tshwane University of Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Marjorie Naidoo', 'email' => 'marjorie.naidoo@telkomsa.net' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University of Johannesburg', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Angus Donald Campbell', 'email' => 'acampbell@uj.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Martin Bolton', 'email' => 'mbolton@uj.ac.za' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University of Botswana', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Richie Moalosi', 'email' => 'MOALOSI@mopipi.ub.bw' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Paulson Letsholo', 'email' => 'letshopm@mopipi.ub.bw' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Yaone Rapitsenyane', 'email' => 'yaone.rapitsenyane@mopipi.ub.bw' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Botswana International University of Science and Technology (BIUST)', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Tunde Oladiran', 'email' => 'oladirant@biust.ac.bw' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University of Nairobi', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lilac Osanjo', 'email' => 'lilac.osanjo@uonbi.ac.ke' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lorraine Amollo', 'email' => 'amollo.lorraine@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Machakos University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sophia Njeru', 'email' => 'sophianjeru2010@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Makerere University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Venny Nakazibwe', 'email' => 'vnakazibwe@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mackay Okure', 'email' => 'mokure@tech.mak.ac.ug' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mary Suzan Abbo', 'email' => 'msabbo@creec.or.ug' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Kyambogo University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Emmanuel Mutungi', 'email' => 'mutungiemmanuel@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Kwame Nkurumah University of Science and Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Eddie Appiah ', 'email' => 'eddappiah@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'The Federal University of Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sunday Roberts Ogunduyile', 'email' => 'sunny_duyile@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Zimbabwe institute of Vigital Arts', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Saki Mafundikwa', 'email' => 'sakimaf@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Namibia University of Science and Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Shilumbe Chivuno Kuria', 'email' => 'schivuno@nust.na' , 'network_member_id' => $member->id, 'created_by' => 1 ]);


        $network = Network::create([ 'name' => 'LeNS_Argentina', 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => 'INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Edurne Battista', 'email' => 'edurnebattista@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'UNLP Universidad Nacional de La Plata', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ana Bocos', 'email' => 'anabocos@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Alejandra Sivila Soza', 'email' => 'alejandrasivilasoza@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        
        $member = NetworkMember::create([ 'name' => 'INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sergio Justianovich', 'email' => 'justianovich.sergio@inta.gob.ar' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'IHAM Instituto del Habitat y del Ambiente, FAUD-Facultad de Arquitectura Urbanismo y Diseño UNMdP-Universidad Nacional de Mar del Plata', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mariana Gonzalez Insua', 'email' => 'gonzalezinsuamariana@conicet.gov.ar' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Universidad Nacional de Lanús, Centro Internacional de Diseño del Conocimiento Tomás Maldonado (MinCyT), UBA-Universidad de Buenos Aires', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Roxana Garbarini', 'email' => 'rgarbarini@mincyt.gob.ar' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Clara Tapia', 'email' => 'claritapia@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        
        $network = Network::create([ 'name' => 'LeNS_Brazil', 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => 'Universidade Federal do Paraná (UFPR)', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Aguinaldo Dos Santos', 'email' => 'asantos@ufpr.br' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Adriane Santos', 'email' => 'adriane.shibata@univille.net' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Klarissa Saes', 'email' => 'ufpr.internacional@ufpr.br' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Isadora Dickie', 'email' => 'isadora.dickie@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Alexandre Oliveira', 'email' => 'aleantoli@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Naotake Fukushima', 'email' => 'profnaotake@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'João Caccere', 'email' => 'jcaccere@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ana Cristina Broega', 'email' => 'cbroega@det.uminho.pt' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Camila Hérnandez', 'email' => 'camilahernandez.sta@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'xxxxxxx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'xxxxxxx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Universidade Federal de Pernambuco (UFPE)', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Leonardo Gomez Castillo', 'email' => 'leonardo.a.gomez@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Londrina State University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Suzana Barreto', 'email' => 'suzanabarreto@onda.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Claudio Pereira', 'email' => 'qddesign@hotmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Fluminense Federal University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Liliane Item Chaves', 'email' => 'chaves.liliane@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        
        $member = NetworkMember::create([ 'name' => 'Federal University of Alagoas', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Priscilla Ramalho Lepre', 'email' => 'cillaramalho@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Federal University of Uberlândia', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Viviane Nunes', 'email' => 'vivianenunes.br@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Federal University of Santa Catarina', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Eugenio Merino', 'email' => 'eadmerino@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);


        $member = NetworkMember::create([ 'name' => 'Universidade do Estado de Minas Gerais - UEMG', 'network_id' => $network->id, 'decription' => 'Escola de Design' ,'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Dijon De Moraes', 'email' => 'dijon.moraes@uemg.br' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Rita Engler', 'email' => 'rcengler@uol.com.br' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Rosemary Bom Conselho', 'email' => 'rosebcs@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);


        $network = Network::create([ 'name' => 'LeNS_China', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'Tsinghua University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Liu Xin', 'email' => 'xinl@tsinghua.edu.cn' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Xian Nan', 'email' => 'squallxn@hotmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Zhong Fang', 'email' => 'zhongfangpoli@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lv Mingue', 'email' => 'lvmoon@163.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Hunan University (HNU)', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Jun Zhang', 'email' => 'zhangjun@hnu.edu.cn' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Eun Jin Cho', 'email' => 'taowood@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Renke He', 'email' => 'Renke8@163.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Jiing Ou', 'email' => 'jingou@hnu.edu.cn' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Huang Yisong', 'email' => 'hys_id@126.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Liu Yen', 'email' => '1542892325@qq.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Jia Yanyang', 'email' => '574664242@qq.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Wuhan University of Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lv Jiefeng', 'email' => 'whutljf@126.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Shaohua Han', 'email' => 'hanshaohua@msn.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Jiangnan University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Zhang Linghao', 'email' => 'wowo.zlh@163.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Qian Xiaobo', 'email' => 'qianxiaobo0118@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'The University of Science and Technology Beijing', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Qin Jingyan', 'email' => 'qinjingyan@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Beijing Information Science and Technology University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Li Honghai', 'email' => 'lihonghai@vip.sina.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'The Hong Kong Polytechnic University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Benny Leong', 'email' => 'sdbenny@polyu.edu.hk' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Brian Lee', 'email' => 'brian.yh.lee@polyu.edu.hk' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
 
        $member = NetworkMember::create([ 'name' => 'Guangzhou academy of fine arts', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Feng Shu', 'email' => 'jumoi2001@qq.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Wang LiuZhuang', 'email' => 'wangliuzhuang@163.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'xxxxxxx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'xxxxxxx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Tongji University', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Gao Bo', 'email' => 'gaobotj@163.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        
        $network = Network::create([ 'name' => 'LeNS_Colombia', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'University El Bosque', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carolina Montoya Rodriguez', 'email' => 'montoyacarolina@unbosque.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mariana Buraglia Osorio', 'email' => 'buragliamarianao@unbosque.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University Javeriana', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lucas Ivorra', 'email' => 'ivorra@javeriana.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ricardo Rugeles', 'email' => 'rugeles-w@javeriana.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Pedro Sanchez', 'email' => 'pedrosanchez@javeriana.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University UDI', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Adolfo Vargas', 'email' => 'avargas2@udi.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University De Pamplona', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sandra Forero', 'email' => 'sforero@unipamplona.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carlos Luna', 'email' => 'cmluna@unipamplona.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University de Medellín', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lina María Agudelo', 'email' => 'lmagudelo@udem.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University de Los Andes', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carolina Obregón', 'email' => 'c.obregon101@uniandes.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University Colegio Mayor de Cundinamarca', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sandra Uribe Perez', 'email' => 'sauripez@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sergio A. Ballén Z.', 'email' => 'sergio.ballen@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University Jorge Tadeo Lozano', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Fernando Álvarez', 'email' => 'fernando.alvarez@utadeo.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $member = NetworkMember::create([ 'name' => 'University San Buenaventura de Medellín', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Diomar Elena Calderón Riaño', 'email' => 'diomar.calderon@usbmed.edu.co' , 'network_member_id' => $member->id, 'created_by' => 1 ]);


        $network = Network::create([ 'name' => 'LeNS_Europe', 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => 'Politecnico di Milano (POLIMI), Design Dept., Italy', 'network_id' => $network->id, 'created_by' => 1 ]);        
        $contact = NetworkMember::create([ 'name' => 'Carlo Vezzoli', 'email' => 'carlo.vezzoli@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Emanuel Delfino', 'email' => 'emanuela.delfino@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Elisa Bacchetti', 'email' => 'elisa.bacchetti@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Fiammetta Costa', 'email' => 'fiammetta.costa@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Technische Universiteit Delft (TU Delft), Netherlands', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Jan Carel Diehl', 'email' => 'jc_diehl@me.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Aalto University (Aalto ARTS), Finland', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Cindy Kohtala', 'email' => 'cindy.kohtala@aalto.fi' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Tatu Marttila', 'email' => 'tatu.marttila@aalto.fi' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Idil Gaziulusoy', 'email' => 'idil.gaziulusoy@aalto.fi' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Brunel University London (UBRUN), UK', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Fabrizio Ceschin', 'email' => 'fabrizio.ceschin@brunel.ac.uk' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Silvia Emili', 'email' => 'silvia.emili@brunel.ac.uk' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Aine Petrulaityte', 'email' => 'aine.petrulaityte@brunel.ac.uk' , 'network_member_id' => $member->id, 'created_by' => 1 ]);


        $network = Network::create([ 'name' => 'LeNS_EU German speaking languages', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'Institute of Design Research Vienna, Vienna, Austria', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Harald Gruendl', 'email' => 'hg@idrv.org' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ulrike Haele', 'email' => 'uh@idrv.org' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Marco Kellhammer', 'email' => 'mk@idrv.org' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University of Applied Arts, Vienna, Austria', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Peter Knobloch', 'email' => 'peter.knobloch@uni-ak.ac.at' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Technical University Vienna, Vienna, Austria', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Anton Kottbauer', 'email' => 'anton.kottbauer@tuwien.ac.at' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'FH Salzburg, Salzburg, Austria', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Michael Leube', 'email' => 'michael.leube@fh-salzburg.ac.at' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'HfG Karlsruhe, Karlsruhe, Germany', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Volker Albus', 'email' => 'va@hfg-karlsruhe.de' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Technical University Munich, Munich, Germany', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Fritz Frenkler', 'email' => 'f.frenkler@lrz.tum.de' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'University of Applied Sciences and Arts Northwestern Switzerland FHNW, Basel, Switzerland', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Heinz Wagner', 'email' => 'heinz.wagner@fhnw.ch' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $network = Network::create([ 'name' => 'LeNS_India', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'Srishti Institute of Art, Design and Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Geetha Narayanan', 'email' => 'g_narayanan@srishti.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Mary Jacob', 'email' => 'maryjacob@srishti.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ranjani Balasubramanian', 'email' => 'ranjani.b@srishti.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Naveen Bagalkot', 'email' => 'naveen@srishti.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Indian Institute of Technology Guwahati (IIT Guwahati)', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ravi Mokashi', 'email' => 'mokashi@iitg.ernet.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sharmistha Banerjee', 'email' => 'banerjee.sharmistha@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Pankaj Upadhyay', 'email' => 'pankaj.nitsil@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'C.A.R.E. School of Architecture', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Godwin Emmanuel', 'email' => 'godwinarchitect@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Pandit Dwarka Prasad Mishra Indian Institute of Information Technology', 'network_id' => $network->id, 'created_by' => 1 ]);
        $member = NetworkMember::create([ 'name' => 'Indian Institute Of Technology Gandhinagar', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Achal Mehra', 'email' => 'achal@iitgn.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Harish M. P.', 'email' => 'harish@iitgn.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Franklin Kristi', 'email' => 'franklin@iitgn.ac.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Goa College of Architecture', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ashish K. Rege', 'email' => 'gca.principal@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Vishvesh Kandolkar', 'email' => 'wishvesh@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Hunnarshala Foundation for Building Technology & Innovations', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Neelkanth Chhaya', 'email' => 'niluchhaya@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sandeep Virmani', 'email' => 'hunnarshala@yahoo.co.in' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Vastu Shilpa Foundation', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Piyas Choudhuri', 'email' => 'piyas@sangath.org' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Joseph Varughese', 'email' => 'vsf@sangath.org' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        
        $network = Network::create([ 'name' => 'LeNS_Italia', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'Politecnico di Milano', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carlo Vezzoli', 'email' => 'carlo.vezzoli@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carlo Proserpio', 'email' => 'carlo.proserpio@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Elisa Bacchetti', 'email' => 'elisa.bacchetti@polimi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Politecnico di Torino', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Luigi Bistagnino', 'email' => 'bistagnino@polito.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Silvia Barbero', 'email' => 'silvia.barbero@polito.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Paolo Tamborrini', 'email' => 'paolo.tamborrini@polito.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
 
        $member = NetworkMember::create([ 'name' => 'Università degli studi di Firenze', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Giuseppe Lotti', 'email' => 'giuseppe.lotti@unifi.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Maria Rosanna Fossati', 'email' => 'maria_fossati@yahoo.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Università di Chieti', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Antonio Marano', 'email' => 'a.marano@unich.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Università Federico II di Napoli', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Rosanna Veneziano', 'email' => 'rosanna.veneziano@unina2.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Patrizia Ranzo', 'email' => 'direzione.di@unina2.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Carla Langella', 'email' => 'carla.langella@unina2.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Roberto Liberti', 'email' => 'robertoliberti2@virgilio.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Francesca Larocca', 'email' => 'f.larocca@mclink.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Università di Camerino', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Lucia Pietroni', 'email' => 'lucia.pietroni@unicam.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Università di Genova', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Paola Gambaro', 'email' => 'gambaro@leonardo.arch.unige.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Adriano Magliocco', 'email' => '' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Università IUAV di Venezia', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Medardo Chiapponi', 'email' => 'medardo.chiapponi@iuav.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Laura Badalucco', 'email' => 'laurabada@tiscali.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        
        $member = NetworkMember::create([ 'name' => 'Università di Palermo', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Anna Catania', 'email' => 'annacatania16@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        
        $member = NetworkMember::create([ 'name' => 'Sapienza università di Roma', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Cecilia Cecchini', 'email' => 'cecilia.cecchini@uniroma1.it' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $network = Network::create([ 'name' => 'LeNS_Mexico', 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => 'Univesidad Autonoma Metropolitana', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Alejandro Ramirez Lozano', 'email' => 'ramloz@correo.azc.uam.mx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Sandra Molina Mata', 'email' => 'unasandra@yahoo.com.mx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Brenda Garcia Parra', 'email' => 'brendagparra@gmail.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Universidad dela Valle de México', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'José Eduardo Camacho G', 'email' => 'protoccolo@yahoo.com' , 'network_member_id' => $member->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Rodrigo Lepez Vela', 'email' => 'rodrigo.lepezv@uvmnet.edu' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Univesidad National Autonoma Metropolitana', 'network_id' => $network->id, 'created_by' => 1 ]);
        $contact = NetworkMember::create([ 'name' => 'Ana María Losada Alfaro', 'email' => 'pdi_sacad@posgrado.unam.mx' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

        $member = NetworkMember::create([ 'name' => 'Instituto Tecnológico de Monterrey Campus Ciudad de México', 'network_id' => $network->id, 'created_by' => 1 ]);

        // $network = Network::create([ 'name' => 'LeNS_Mexico', 'created_by' => 1]);
        // $member = NetworkMember::create([ 'name' => 'XXXXXX', 'network_id' => $network->id, 'created_by' => 1 ]);
        // $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'XXXXXX' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
