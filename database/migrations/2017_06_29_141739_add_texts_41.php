<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts41 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "pages", "item" => "permalink", "text" => "Permalink"],
            ["locale" => "en" , "group" => "text", "item" => "link", "text" => "Link"],
            ["locale" => "en" , "group" => "text", "item" => "download", "text" => "Download"],
            ["locale" => "en" , "group" => "backend", "item" => "platform_description", "text" => "Platform description"]
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}