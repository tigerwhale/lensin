<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = \App\User::create([
                    "server_id" => "0",
                    "language" => "en",
                    "country_id" => "264",
                    "email" => "administrator@lens-international.org",
                    'password' => bcrypt("administrator"),
                    "password" => '$2y$10$FhLT6ow0YTaH3ALssYUHk.5Q.1EYNh4S2vEpndIWGFxrxnreyt.Pi',
                    "name" => 'Administrator',
                    "last_name" => 'Administrator',
                    "school" => 'Lens',
                    "address" => 'The World',
                    "user_type_id" => 1
                ]);

        $user->attachRole(role('managecourses'));
        $user->attachRole(role('managesite'));
        $user->attachRole(role('manageusers'));
        $user->attachRole(role('managestudycases'));
        $user->attachRole(role('managetools'));
        $user->attachRole(role('manageservers'));
        $user->attachRole(role('manageprojects'));
        $user->attachRole(role('managelanguages'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
