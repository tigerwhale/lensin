<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts94 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("network", "no_members", "No members yet.");
        add_migration_text("network", "contacts", "No contacts yet.");
        add_migration_text("network", "create_new_member", "Create a new member");
        add_migration_text("network", "create_new_contact", "Create a new contact");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
