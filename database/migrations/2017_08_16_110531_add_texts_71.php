<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts71 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("text", "logo", "Logo");
        add_migration_text("text", "views", "Views");
        add_migration_text("text", "downloads", "Downloads");
        add_migration_text("text", "right_menu_title_platforms", "Platforms");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
