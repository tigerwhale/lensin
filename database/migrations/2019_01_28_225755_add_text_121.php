<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText121 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("backend", "platform_properties", "Platform properties");
        add_migration_text("backend", "central_server_properties", "Central server properties");
        add_migration_text("backend", "changelog", "Changelog");
        add_migration_text("text", "home_page_video", "Home page video");
        add_migration_text("text", "home_page_video_thumbnail", "Home page video thumbnail");
        add_migration_text("text", "upload", "Upload");
        add_migration_text("text", "return_to_default", "Return to default");
        add_migration_text("pages", "page_name", "Page name");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
