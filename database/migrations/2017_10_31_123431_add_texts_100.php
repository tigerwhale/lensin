<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("text", "sort", "Sort");
        add_migration_text("advanced_research", "any_language", "Any language");
        add_migration_text("advanced_research", "any_author", "Any author");
        add_migration_text("advanced_research", "any_type", "Any type");
        add_migration_text("advanced_research", "any_year", "Any year");
        add_migration_text("advanced_research", "any_length", "Any length");
        add_migration_text("advanced_research", "any_institution", "Any institution");
        add_migration_text("advanced_research", "any_sort", "Any sort");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
