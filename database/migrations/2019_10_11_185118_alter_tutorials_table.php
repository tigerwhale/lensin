<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tutorials');

        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('institution')->nullable();
            $table->string('language_id')->nullable();
            $table->text('category')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
