<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        fixText('Univeristy/Institution/Company', 'Univeristy / Institution / Company');
        fixText('You have registered. You will recieve an email soon.', 
                'You have registered. You will receive an email soon.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
