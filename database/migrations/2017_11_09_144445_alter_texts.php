<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //study_cases
        $translations = \App\LanguageTranslation::where('group', 'LIKE', 'challanges')->get();
        foreach ($translations as $translation) {
            $translation->group = 'challenges';
            $translation->save();
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
