<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts63 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "insert_email", "text" => "Insert email"],
            ["locale" => "en" , "group" => "text", "item" => "click_to_select_document", "text" => "Click here to select a document"],
            ["locale" => "en" , "group" => "text", "item" => "new_network_request_created", "text" => "New network request created"],
            ["locale" => "en" , "group" => "text", "item" => "new_networks", "text" => "New networks"],
            ["locale" => "en" , "group" => "text", "item" => "document", "text" => "Document"],
            ["locale" => "en" , "group" => "text", "item" => "close_and_mark_as_resolved", "text" => "Close and mark as resolved"],
            ["locale" => "en" , "group" => "text", "item" => "close_and_mark_as_unresolved", "text" => "Close and mark as unresolved"],
        ]);   
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
