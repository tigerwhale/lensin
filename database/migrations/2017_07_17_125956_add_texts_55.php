<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts55 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'join_as_participant')->first();
        $translation->text = "Join as single user";
        $translation->save();

        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "join_as_memeber", "text" => "Join as member"],
            ["locale" => "en" , "group" => "text", "item" => "join_as_partner", "text" => "Join as partner"],
            ["locale" => "en" , "group" => "text", "item" => "register_a_new_network", "text" => "Register a new LeNS network"],
        ]);           

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
