<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeResourceTypeOrder3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $r = \App\ResourceType::where('name', 'other')->first();
        $r->order = 7;
        $r->save();

        $r = \App\ResourceType::where('name', 'image')->first();
        $r->order = 6;
        $r->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
