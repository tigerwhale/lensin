<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "delete_user", "text" => "This completely deletes the user and all his resources!"],
            ["locale" => "en" , "group" => "users", "item" => "suspend_user", "text" => "This blocks the user from the system and his resources!"],
            ["locale" => "en" , "group" => "users", "item" => "user_suspended", "text" => "User blocked!"],
            ["locale" => "en" , "group" => "users", "item" => "user_deleted", "text" => "User deleted!"]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
