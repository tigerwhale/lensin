<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText122 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("backend", "welcome_text", "Welcome to LeNS backend administration.");
        add_migration_text("users", "my_projects", "My projects");
        add_migration_text("users", "change_password", "Change password");
        add_migration_text("users", "old_password", "Old password");
        add_migration_text("users", "new_password", "New password");
        add_migration_text("users", "verify_new_password", "Verify new password");
        add_migration_text("users", "update_password", "Update password");
        add_migration_text("users", "join_as_member", "Join as member");
        add_migration_text('users', "join_existing_member_group", 'Join existing member group');
        add_migration_text('users', "join_member_text", 'Join a member text description');
        add_migration_text('users', "no_member_groups", 'There are currently no active Members');
        add_migration_text('users', "create_a_new_member", 'Create a new Member');
        add_migration_text('users', "members_logo", 'Member logo');
        add_migration_text('users', "university_type", 'University');
        add_migration_text('users', "school_type", 'School');
        add_migration_text('users', "company_type", 'Company');
        add_migration_text('users', "ngo_type", 'NGO');
        add_migration_text('users', "goverment_institution_type", 'Goverment Institution');
        add_migration_text('users', "others", 'Others');
        add_migration_text('users', "join_partners_text", 'You can join an existing partner group.');
        add_migration_text('users', "no_partners_groups", 'There are no partner groups yet.');
        add_migration_text('users', "create_a_new_partner", 'Create a new partner');


        add_migration_text('users', "register_member_text", 'Any Design research and/or educational institutions, design companies, companies, NGOs, category association, governmental institutions, etc. can become a LeNS Member. By becoming a LeNS Member you will be and recognized as being part of an international community active in designing and diffusing sustainability. Your institutional and (desired) personal data will be posted on the LeNS Member page and you have the opportunity to publish news, request for information, partners for a project, etc.  
                <br><br>
                <b>How to become a member of the LeNS network</b> <br>
                Please fill in the record aside and press register. <br>
                LeNS staff will contact you when the your request has been accepted, i.e. you will appear on the LeNS member page.
            ');


        add_migration_text("text", "admin", "Administration");
        add_migration_text("text", "participant", "Participant");
        add_migration_text("text", "join_as_member", "Join as member");
        add_migration_text("text", "join_as_partner", "Join as partner");
        add_migration_text("text", "register_a_new_network", "Register a new network");
        add_migration_text("text", "logo_jpg_disclaimer", "For best results please use JPG or non-transparent PNG files.");
        add_migration_text("text", "reference_person_email", "Reference person e-mail");
        add_migration_text("text", "web_site", "Website");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
