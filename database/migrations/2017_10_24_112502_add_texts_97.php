<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts97 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translations = \App\LanguageTranslation::where('group', 'LIKE', 'network')->where('item', 'LIKE', 'contacts')->get();
        foreach ($translations as $translation) {
            $translation->text = 'Contacts';
            $translation->save();
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
