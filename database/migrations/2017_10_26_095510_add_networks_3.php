<?php

use \App\Network;
use \App\NetworkMember;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNetworks3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ns = Network::all();
        foreach ($ns as $n) {
            $n->forceDelete();
        }
        
        $ns = NetworkMember::all();
        foreach ($ns as $n) {
            $n->forceDelete();
        }

        $network = Network::create([ 'name' => "LeNS_Brazil", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Universidad Federal do Parana (UFPR)", 'network_id' => $network->id, 'city' => "Curitiba", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Emanuela Lima Silveira", "email" => "manuhsilveira@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Aguinaldo Dos Santos", "email" => "asantos@ufpr.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Michele Cuccu", "email" => "cuccu.am@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Adriane Santos", "email" => "adriane.shibata@univille.net", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Klarissa Saes", "email" => "ufpr.internacional@ufpr.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Isadora Dickie", "email" => "isadora.dickie@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Alexandre Oliveira", "email" => "aleantoli@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Naotake Fukushima", "email" => "profnaotake@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "João Caccere", "email" => "jcaccere@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Milena Carneiro Alves", "email" => "mcarneiroalves@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marc Rul", "email" => "marcjrul@yahoo.com.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Iana Uliana Perez", "email" => "iana.uli@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Aline Müller", "email" => "eilan.muller@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ana Cristina Broega", "email" => "cbroega@det.uminho.pt", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Camila Hérnandez", "email" => "camilahernandez.sta@gmail.com ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Universidad Federal de Pernambuco", 'network_id' => $network->id, 'city' => "Recife", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carla Pasa Gómez", "email" => "carlapasagomez@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Leonardo Castillo Gomez", "email" => "leonardo.a.gomez@gmail.com  ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Universidade do Estado do Parà (UEPA)", 'network_id' => $network->id, 'city' => "Belem", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Antonio Erlindo Braga Júnior ", "email" => "erlindo@uepa.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Universidade do Vale do Rio dos Sinos- Campus São Leopoldo (Unisinos)", 'network_id' => $network->id, 'city' => "São Leopoldo", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Andre Canal Marques", "email" => "ANDRECM@unisinos.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Chiara Del Gaudio", "email" => "CHIARADG@unisinos.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Universidade Feevale (FEEVALE)", 'network_id' => $network->id, 'city' => "Novo Hamburgo", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Fabiano André Trein", "email" => "fabianotrein@feevale.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "The Centro Universitário Ritter dos Reis (UNIRITTER)", 'network_id' => $network->id, 'city' => "Porto Alegre", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Diego Pacheco", "email" => "profdajp@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Fluminense Federal University", 'network_id' => $network->id, 'city' => "Niteroi", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Thomàs Cavalcanti", "email" => "thomascavalcanti@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Liliane Item Chaves", "email" => "chaves.liliane@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Federal University of Alagoas (UFAL)", 'network_id' => $network->id, 'city' => "Maceio Alagoas", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Priscilla Ramalho Lepre", "email" => "cillaramalho@yahoo.com.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Federal University of Uberlândia (UFU)", 'network_id' => $network->id, 'city' => "Uberlândia", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Aline Teixeira de Souza", "email" => "designuem.aline@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Andressa Bernardo", "email" => "andressa.bernardo@hotmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Viviane Nunes", "email" => "vivianenunes.br@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidade da Regiao de Joinville (UNIVILLE)", 'network_id' => $network->id, 'city' => "Santa Catarina", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Anna Cavalcanti", "email" => "anna.cavalcanti08@gmail.com ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marli Everling", "email" => "marli.everling@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Luis Fernando Figueiredo", "email" => "lff@cce.ufsc.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ana Veronica", "email" => "anaverpw@gmail.com ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Eugenio Merino", "email" => "eadmerino@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => " Londirna State University", 'network_id' => $network->id, 'city' => " Londirna", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ana Boa Vista", "email" => "anaboavista@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Suzana Barreto", "email" => "suzanabarreto@onda.com.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Claudio Pereira", "email" => "qddesign@hotmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidade FUMEC", 'network_id' => $network->id, 'city' => "Belo Horizonte", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Adriana Tonani Mazzieiro", "email" => "adrianat@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Andrea de Paula Xavier Vilela", "email" => "avilela@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Euclides Guimaraes Neto", "email" => "egneto@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Flavio Fabrino Negrao Azevedo", "email" => "ffna@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Flavio Lucio Nunes de Lima", "email" => "flnlima@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Juliana Pontes Ribeiro", "email" => "jpontes@fumec.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Universidade do Estado de Minas Gerais (UEMG) Escola de Design", 'network_id' => $network->id, 'city' => "Belo Horizonte", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Dijon De Moraes ", "email" => "dijon.moraes@uemg.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => " Rita Engler ", "email" => "rcengler@uol.com.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Rosemary Bom Conselho", "email" => "rosebcs@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Rosangela Miriam Mendonca", "email" => "Romiriam@arcquicad.com.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidade Federal de São Carlos (UFSCAR)", 'network_id' => $network->id, 'city' => " São Paulo ", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lilian Azevedo", "email" => "liliansaazevedo@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidade de São Paulo - campus of São Carlos", 'network_id' => $network->id, 'city' => "Universidade de São Paulo - campus of São Carlos", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Maria Cecília Loschiavo dos Santos", "email" => "closchia@usp.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Rosana Vasques ", "email" => "ravasques@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidade de São Paulo - campus of São Paulo", 'network_id' => $network->id, 'city' => "Universidade de São Paulo - campus of São Paulo", 'country' => "Brasil", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marília Rosendo", "email" => "mariliac.rosendo@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Henrique Rozenfeld", "email" => "roz@usp.br", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Caio Marques  ", "email" => "caio.nunes.marques@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carolina Queiroz ", "email" => "queiroz.carolina02@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Maira Rosa ", "email" => "MS.maiararosa@gmai.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Renato Carriao ", "email" => "rcarriao@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Renato Nunes Moraes", "email" => "renato.nmoraes@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sania Fernandes", "email" => "sania.scf@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lucas Gomes ", "email" => "lucaspcgomes@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);


        $network = Network::create([ 'name' => "LeNS_Mexico", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Univesidad Autonoma Metropolitana", 'network_id' => $network->id, 'city' => "Mexico Distrito Federal", 'country' => "Mexico", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Alejandro Ramirez Lozano", "email" => "ramloz@correo.azc.uam.mx", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sandra Molina Mata", "email" => "unasandra@yahoo.com.mx ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Brenda Garcia Parra", "email" => "brendagparra@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidad dela Valle de México", 'network_id' => $network->id, 'city' => "Cità de México", 'country' => "Mexico", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "José Eduardo Camacho G", "email" => "protoccolo@yahoo.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Rodrigo Lepez Vela ", "email" => "rodrigo.lepezv@uvmnet.edu", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Univesidad National Autonoma Metropolitana", 'network_id' => $network->id, 'city' => "Mexico Distrito Federal", 'country' => "Mexico", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ana María Losada Alfaro", "email" => "pdi_sacad@posgrado.unam.mx", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Instituto Tecnológico de Monterrey Campus Ciudad de México", 'network_id' => $network->id, 'city' => "Nuevo Leon", 'country' => "Mexico", 'created_by' => 1 ]);


        $network = Network::create([ 'name' => "LeNS_China", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Tsinghua University ", 'network_id' => $network->id, 'city' => "Beijing", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Liu Xin", "email" => "xinl@tsinghua.edu.cn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Tsinghua University", 'network_id' => $network->id, 'city' => "Beijing", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Xian Nan", "email" => "squallxn@hotmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Zhong Fang", "email" => "zhongfangpoli@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lv Mingue", "email" => "lvmoon@163.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Hunan University", 'network_id' => $network->id, 'city' => "Changsha", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Jun Zhang", "email" => "zhangjuncoco@gmail.com; zhangjun@hnu.edu.cn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Eun Jin Cho", "email" => "taowood@gmail.com ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Renke He", "email" => "Renke8@163.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Jiing Ou", "email" => "jingou@hnu.edu.cn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Huang Yisong", "email" => "hys_id@126.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Liu Yen", "email" => "1542892325@qq.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Jia Yanyang", "email" => "574664242@qq.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Wuhan University of Technology", 'network_id' => $network->id, 'city' => "Wuhan", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lv Jiefeng", "email" => "whutljf@126.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Shaohua Han", "email" => "hanshaohua@msn.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Jiangnan University ", 'network_id' => $network->id, 'city' => "Wuxi", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Zhang Linghao", "email" => "wowo.zlh@163.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Qian Xiaobo", "email" => "qianxiaobo0118@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "The University of Science and Technology Beijing", 'network_id' => $network->id, 'city' => "Beijing", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Qin Jingyan", "email" => "qinjingyan@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Beijing Information Science and Technology University", 'network_id' => $network->id, 'city' => "0", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Li Honghai", "email" => "lihonghai@vip.sina.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "The Hong Kong Polytechnic University", 'network_id' => $network->id, 'city' => "Hong Kong", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Benny Leong", "email" => "benny.leong@polyu.edu.hk", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "0", "email" => "sdbenny@polyu.edu.hk", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Brian Lee", "email" => "brian.yh.lee@polyu.edu.hk", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Guangzhou academy of fine arts", 'network_id' => $network->id, 'city' => "Guangzhou", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Feng Shu", "email" => "jumoi2001@qq.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Wang LiuZhuang", "email" => "wangliuzhuang@163.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Tongji University", 'network_id' => $network->id, 'city' => "Shanghai", 'country' => "China", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Gao Bo", "email" => "gaobotj@163.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);



        $network = Network::create([ 'name' => "LeNS_India", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Srishti School of Art Design and Technology", 'network_id' => $network->id, 'city' => "Bangalore", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mary Jacob", "email" => "maryjacob@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ranjani Balasubramanian", "email" => "ranjani.b@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ravi Mani", "email" => "ravi.mani@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Jacob Mathew", "email" => "jacob.mathew@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ritu Sonalika", "email" => "ritu.sonalika@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Aishwarya Iyengar", "email" => "aishwarya.i@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Urvashi Jalali", "email" => "urvashi@srishti.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Indian Institute of technology Guwahati", 'network_id' => $network->id, 'city' => "Guwahati", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ravi Mokashi", "email" => "mokashi@iitg.ernet.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sharmistha Banerjee", "email" => "banerjee.sharmistha@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Pankaj Upadhyay", "email" => " pankaj.nitsil@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "C.A.R.E. School of Architecture", 'network_id' => $network->id, 'city' => "Trichy", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Godwin emmanuel", "email" => "godwinarchitect@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Indian Institute of Information technology Design and Manufacturing Jabalpur (IIITDMJ)", 'network_id' => $network->id, 'city' => "Jabalpur", 'country' => "India", 'created_by' => 1 ]);
        $member = NetworkMember::create([ 'name' => "Indian Institute Of Technology Gandhinagar", 'network_id' => $network->id, 'city' => "Ahmedabad", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Achal Mehra", "email" => "achal@iitgn.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Harish M. P.", "email" => "harish@iitgn.ac.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Franklin Kristi", "email" => "franklin@iitgn.ac.in ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Goa College of Architecture", 'network_id' => $network->id, 'city' => "Panjim", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ashish K. Rege", "email" => "gca.principal@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Vishvesh Kandolkar", "email" => "wishvesh@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Hunnarshala Foundation for Building Technology & Innovations", 'network_id' => $network->id, 'city' => "Bhuj", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Neelkanth Chhaya", "email" => "niluchhaya@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sandeep Virmani", "email" => "hunnarshala@yahoo.co.in", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Vastu Shilpa Foundation", 'network_id' => $network->id, 'city' => "Ahmedabad", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Piyas Choudhuri", "email" => "piyas@sangath.org", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Joseph Varughese", "email" => "vsf@sangath.org", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Cape Peninsula University of Technology", 'network_id' => $network->id, 'city' => "Cape Town", 'country' => "India", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Retha De La Harpe ", "email" => "delaharper @cput.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ephias Ruhode ", "email" => "RuhodeE@cput.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Johan Van Niekerk", "email" => "VanNiekerkJ@cput.ac.za ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mugendi M'Rithaa", "email" => "mugendim@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Munira Alliem", "email" => "alliem@cput.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);


        $network = Network::create([ 'name' => "LeNS_Africa", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Stellenbosch University", 'network_id' => $network->id, 'city' => "Cape Town", 'country' => "South Africa", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Elmarie Costandius", "email" => "sysadmin@sun.ac.za", "email2" => "elmarie@sun.ac.za" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Karolien Perold-Bull", "email" => "karolien@sun.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marjorie Naidoo", "email" => "marjorie.naidoo@telkomsa.net", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Andrea Broom", "email" => "andrea.broom@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Farm and Garden National Trust", 'network_id' => $network->id, 'city' => "Cape Town", 'country' => "South Africa", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Rob Small", "email" => "rsmall@xsinet.co.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Cape Craft and Design Institute NPC", 'network_id' => $network->id, 'city' => "Cape Town", 'country' => "South Africa", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Erica Elk", "email" => "erica.elk@ccdi.org.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Tshwane University of Technology", 'network_id' => $network->id, 'city' => "Pretoria West", 'country' => "South Africa", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marcel Mare", "email" => " marem@tut.ac.za  ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Johannesburg", 'network_id' => $network->id, 'city' => "Johannesburg", 'country' => "South Africa", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Angus Donald Campbell ", "email" => "acampbell@uj.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Martin Bolton", "email" => " mbolton@uj.ac.za", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Botswana", 'network_id' => $network->id, 'city' => "Gaborone", 'country' => "Botswana", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Richie Moalosi", "email" => "MOALOSI@mopipi.ub.bw", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Paulson Letsholo", "email" => "LETSHOPM@mopipi.ub.bw", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Yaone Rapitsenyane", "email" => " yaone.rapitsenyane@mopipi.ub.bw", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Botswana International University of Science and Technology (BIUST)", 'network_id' => $network->id, 'city' => "Palapye", 'country' => "Botswana", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Tunde Oladiran", "email" => "oladirant@biust.ac.bw", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Nairobi", 'network_id' => $network->id, 'city' => "Nairobi", 'country' => "Kenya", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lilac Osanjo", "email" => "lilac.osanjo@uonbi.ac.ke", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lorraine Amollo", "email" => "amollo.lorraine@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "James Wafula", "email" => "james.wafula@uonbi.ac.ke", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Machakos University", 'network_id' => $network->id, 'city' => "Nairobi", 'country' => "Kenya", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sophia Njeru", "email" => "sophianjeru2010@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Makerere University", 'network_id' => $network->id, 'city' => "Kampala", 'country' => "Uganda", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Venny Nakazibwe", "email" => "vnakazibwe@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mackay Okure", "email" => "mokure@tech.mak.ac.ug", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mary Suzan Abbo", "email" => "msabbo@creec.or.ug", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Kyambogo University", 'network_id' => $network->id, 'city' => "Kampala", 'country' => "Uganda", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Emmanuel Mutungi", "email" => "mutungiemmanuel@yahoo.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Kwame Nkurumah University of Science and Technology", 'network_id' => $network->id, 'city' => "Kumasi", 'country' => "Ghana", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Eddie Appiah", "email" => "eddappiah@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "The Federal University of Technology", 'network_id' => $network->id, 'city' => "Ihiagwa", 'country' => "Nigeria", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sunday Roberts Ogunduyile", "email" => "sunny_duyile@yahoo.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Zimbabwe institute of Vigital Arts", 'network_id' => $network->id, 'city' => "Harare", 'country' => "Zimbabwe", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Saki Mafundikwa ", "email" => "sakimaf@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Namibia University of Science and Technology", 'network_id' => $network->id, 'city' => "Windhoek", 'country' => "Namibia", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Shilumbe Chivuno Kuria", "email" => "schivuno@nust.na", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);




        $network = Network::create([ 'name' => "LeNS_Europe", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "Politecnico di Milano", 'network_id' => $network->id, 'city' => "Milano", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Carlo Vezzoli", "email" => "carlo.vezzoli@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Emanuel Delfino", "email" => "emanuela.delfino@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Elisa Bacchetti", "email" => "elisa.bacchetti@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Fiammetta Costa", "email" => "fiammetta.costa@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Tu Delft", 'network_id' => $network->id, 'city' => "Delft", 'country' => "The Netherland", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Jan Carel Diehl", "email" => "jc_diehl@me.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Jan Carel Diehl", "email" => "j.c.diehl@tudelft.nl", "email2" => "jc_diehl@me.com" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Aalto University", 'network_id' => $network->id, 'city' => "Helsinki", 'country' => "Finland", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Cindy Kohtala", "email" => "cindy.kohtala@aalto.fi", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Tatu Marttila", "email" => "tatu.marttila@aalto.fi", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Idil Gaziulusoy", "email" => "idil.gaziulusoy@aalto.fi", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Brunel University", 'network_id' => $network->id, 'city' => "London", 'country' => "Uganda", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Fabrizio Ceschin", "email" => "fabrizio.ceschin@brunel.ac.uk", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Silvia Emili", "email" => "silvia.emili@brunel.ac.uk", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Aine Petrulaityte", "email" => "aine.petrulaityte@brunel.ac.uk ", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);




        $network = Network::create([ 'name' => "LeNS_EU German speaking languages", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "Institute of Design Research Vienna", 'network_id' => $network->id, 'city' => "Vienna", 'country' => "Austria", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Harald Gruendl", "email" => "hg@idrv.org", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ulrike Haele", "email" => "uh@idrv.org", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Marco Kellhammer", "email" => "mk@idrv.org", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Applied Arts", 'network_id' => $network->id, 'city' => "Vienna", 'country' => "Zimbabwe", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Peter Knobloch", "email" => "peter.knobloch@uni-ak.ac.at", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Technical University Vienna", 'network_id' => $network->id, 'city' => "Vienna", 'country' => "Namibia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Anton Kottbauer", "email" => " anton.kottbauer@tuwien.ac.at", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "FH Salzburg", 'network_id' => $network->id, 'city' => "Salzburg", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Michael Leube", "email" => "michael.leube@fh-salzburg.ac.at", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "HfG Karlsruhe", 'network_id' => $network->id, 'city' => "Karlsruhe", 'country' => "Germany", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Volker Albus", "email" => "va@hfg-karlsruhe.de", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Technical University  Munich", 'network_id' => $network->id, 'city' => "Munich", 'country' => "Germany", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Fritz Frenkler", "email" => "fritz.frenkler@tum.de", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Applied Sciences and Arts Northwestern Switzerland FHNW", 'network_id' => $network->id, 'city' => "Basel", 'country' => "Switzerland", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Heinz Wagner", "email" => "heinz.wagner@fhnw.ch", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);



        $network = Network::create([ 'name' => "LeNS_Oceania", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "University of New South Wales", 'network_id' => $network->id, 'city' => "Sydney", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Mariano Ramirez", "email" => "m.ramirez@unsw.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Western Sydney", 'network_id' => $network->id, 'city' => "Sydney", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Abby Mellick Lopes", "email" => "A.Lopes@uws.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "RMIT University", 'network_id' => $network->id, 'city' => "Melbourne", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Liam Fennessy", "email" => "liam.fennessy@rmit.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Soumitri Varadarajan", "email" => "soumitri.varadarajan@rmit.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Simon Lockrey", "email" => "simon.lockrey@rmit.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Swinburne University of Technology", 'network_id' => $network->id, 'city' => "Melbourne", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Katherine Bissett Johnson", "email" => "kbissettjohnson@swin.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mark Strachan", "email" => "mstrachan@swin.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Monash University", 'network_id' => $network->id, 'city' => "Melbourne", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Mark Richardson", "email" => "Mark.Richardson@monash.edu", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Canberra", 'network_id' => $network->id, 'city' => "Canberra", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Carlos Montana Hoyos", "email" => "Carlos.Montana.Hoyos@canberra.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Livio Bonollo", "email" => "Livio.Bonollo@canberra.edu.au", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Auckland University of Technology", 'network_id' => $network->id, 'city' => "Auckland", 'country' => "Australia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Andrew Withell", "email" => "andrew.withell@aut.ac.nz", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Stephen Reay", "email" => "stephen.reay@aut.ac.nz", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Canterbury", 'network_id' => $network->id, 'city' => "Christchurch", 'country' => "New Zealand", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Conan Fee ", "email" => "conan.fee@canterbury.ac.nz", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);



        $network = Network::create([ 'name' => "LeNS_Italia", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "Politecnico di Milano", 'network_id' => $network->id, 'city' => "Milano", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Carlo Vezzoli", "email" => "carlo.vezzoli@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carlo Proserpio", "email" => "carlo.proserpio@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Politecnico di Torino", 'network_id' => $network->id, 'city' => "Torino", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Luigi Bistagnino", "email" => "bistagnino@polito.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "silvia barbero", "email" => "silvia.barbero@POLITO.IT", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Paolo Tamborrini", "email" => "paolo.tamborrini@polito.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università degli studi di Firenze", 'network_id' => $network->id, 'city' => "Firenze", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Giuseppe Lotti", "email" => "giuseppe.lotti@unifi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Maria Rosanna Fossati", "email" => "maria_fossati@yahoo.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università di Chieti", 'network_id' => $network->id, 'city' => "Chieti", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Antonio Marano", "email" => "a.marano@unich.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università Federico II di Napoli", 'network_id' => $network->id, 'city' => "Napoli", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "rosanna veneziano", "email" => "rosanna.veneziano@unina2.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Patrizia Ranzo ", "email" => "direzione.di@unina2.it; cappellieranzo@usa.net", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carla Langella", "email" => "carla.langella@unina2.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => " Roberto Liberti", "email" => "robertoliberti2@virgilio.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => " Francesca Larocca", "email" => "f.larocca@mclink.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università di Camerino", 'network_id' => $network->id, 'city' => "Camerino", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Lucia Pietroni", "email" => "lucia.pietroni@unicam.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università di Genova", 'network_id' => $network->id, 'city' => "Genova", 'country' => "New Zealand", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Paola Gambaro", "email" => "gambaro@leonardo.arch.unige.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Adriano Magliocco", "email" => "0", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università IUAV di Venezia", 'network_id' => $network->id, 'city' => "Venezia", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Medardo Chiapponi", "email" => "medardo.chiapponi@iuav.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Laura Badalucco", "email" => "laurabada@tiscali.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Università di Palermo", 'network_id' => $network->id, 'city' => "Palermo", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Anna Catania", "email" => "annacatania16@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Libera Università di Bolzano", 'network_id' => $network->id, 'city' => "Bolzano", 'country' => "Italy", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Claudio De Luca ", "email" => "", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Sapienza università di Roma", 'network_id' => $network->id, 'city' => "Roma", 'country' => "Italy", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Cecilia Cecchini", "email" => "cecilia.cecchini@uniroma1.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $network = Network::create([ 'name' => "LeNS_Colombia", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "University de Los Andes", 'network_id' => $network->id, 'city' => "Bogotá", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Carolina Obregón ", "email" => "c.obregon101@uniandes.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University Jorge Tadeo Lozano", 'network_id' => $network->id, 'city' => "Bogotá", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Fernando Álvarez", "email" => "fernando.alvarez@utadeo.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University Colegio Mayor de Cundinamarca", 'network_id' => $network->id, 'city' => "Bogotá", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Sandra Uribe Perez", "email" => "sauripez@yahoo.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sergio A. Ballén Z.", "email" => "sergio.ballen@yahoo.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "University El Bosque", 'network_id' => $network->id, 'city' => "Bogotá", 'country' => "Colombia", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carolina Montoya Rodriguez", "email" => "montoyacarolina@unbosque.edu.co", "email2" => "carolina.montoya.rodriguez@gmail.com" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Mariana Buraglia Osorio", "email" => "mburaglia@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "University Javeriana", 'network_id' => $network->id, 'city' => "Bogotá", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Lucas Ivorra", "email" => " ivorra@javeriana.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ricardo Rugeles ", "email" => "rugeles-w@javeriana.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Pedro Sanchez", "email" => " pedrosanchez@javeriana.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University UDI", 'network_id' => $network->id, 'city' => "Bucaramanga, Santander", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Adolfo Vargas ", "email" => " avargas2@udi.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University De Pamplona", 'network_id' => $network->id, 'city' => "Pamplona, Norte de Santander", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Carlos Luna", "email" => "cmluna@unipamplona.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Sandra Forero", "email" => "sforero@unipamplona.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lina María Agudelo", "email" => "lmagudelo@udem.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University San Buenaventura de Medellín", 'network_id' => $network->id, 'city' => "University San Buenaventura de Medellín", 'country' => "Colombia", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Diomar Elena  Calderón Riaño", "email" => "diomar.calderon@usbmed.edu.co", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);


        $network = Network::create([ 'name' => "LeNS_Argentina", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata", 'network_id' => $network->id, 'city' => "La Plata", 'country' => "Argentina", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Edurne Battista", "email" => "edurnebattista@gmail.com", "email2" => "battista.edurne@inta.gob.ar" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "UNLP Universidad Nacional de La Plata", 'network_id' => $network->id, 'city' => "UNLP Universidad Nacional de La Plata", 'country' => "Argentina", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Ana Bocos", "email" => "anabocos@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Alejandra Sivila Soza", "email" => "alejandrasivilasoza@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata", 'network_id' => $network->id, 'city' => "INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata", 'country' => "Argentina", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Sergio Justianovich ", "email" => "justianovich.sergio@inta.gob.ar", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "IHAM Instituto del Habitat y del Ambiente, FAUD-Facultad de Arquitectura Urbanismo y Diseño UNMdP-Universidad Nacional de Mar del Plata", 'network_id' => $network->id, 'city' => "Mar de La Plata", 'country' => "Argentina", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Mariana Gonzalez Insua", "email" => "gonzalezinsuamariana@hotmail.com    ", "email2" => "gonzalezinsuamariana@conicet.gov.ar" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Universidad Nacional de Lanús-,  Centro Internacional de Diseño del Conocimiento Tomás Maldonado (MinCyT), UBA -Universidad de Buenos Aires", 'network_id' => $network->id, 'city' => "Buenos Aires y Lanús", 'country' => "Argentina", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Roxana Garbarini", "email" => "rgarbarini@mincyt.gob.ar", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Clara Tapia", "email" => "claritapia@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);


        $network = Network::create([ 'name' => "LeNS_Vietnam", 'created_by' => 1]);

        $member = NetworkMember::create([ 'name' => "FPT University", 'network_id' => $network->id, 'city' => "Hanoi", 'country' => "Vietnam", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Tran Thi Le Quyen ", "email" => "quyenttl2@fe.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nguyen Kim Anh", "email" => "kimanh@fe.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nguyen Ha Thanh", "email" => "thanhnh29@fe.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Industrial Fine Arts", 'network_id' => $network->id, 'city' => "Hanoi", 'country' => "Vietnam", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Vu Chi Cong ", "email" => "chicongvu63@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Bui Mai Trinh", "email" => "maitrinh.bui@polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "National University of Civil Engineering", 'network_id' => $network->id, 'city' => "Hanoi", 'country' => "Vietnam", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Pham Hung Cuong", "email" => "phcuong39@gmail.com", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nguyen Quang Minh", "email" => "minhnq@nuce.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ta Quynh Hoa", "email" => "hoatq@nuce.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Doan The Trung", "email" => "trungdt@nuce.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Tran Minh Tung", "email" => "tungtm@nuce.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Ton Duc Thang University", 'network_id' => $network->id, 'city' => "Ho Chi Minh city", 'country' => "Vietnam", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Trinh Tu Anh", "email" => "trinhtuanh@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Ngo Le Minh", "email" => "ngoleminh@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Tran Thi Quynh Mai", "email" => "tranthiquynhmai@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Le To Quyen", "email" => "letoquyen@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Hoang Thi Phuong Thao", "email" => "hoangthiphuongthao@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Le Hoang Nam", "email" => "lehoangnam@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nguyen Dinh Nam", "email" => "nguyendinhnam@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nguyen Thi Ngoc Giang", "email" => "nguyenthingocgiang@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Dang The Hien", "email" => "dangthehien@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Lam Quy Thuong", "email" => "lamquythuong@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Le Tan Hanh", "email" => "letanhanh@tdt.edu.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "University of Science and Technology - The University of Danang", 'network_id' => $network->id, 'city' => "Danang", 'country' => "Vietnam", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Le Dinh Duong", "email" => "ldduong@dut.udn.vn", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $network = Network::create([ 'name' => "LeNS_Peru", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Universidad de Ingenieria Y Tecnologia (UTEC)", 'network_id' => $network->id, 'city' => "Lima", 'country' => "Peru", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Gabriela Pella Fernandez", "email" => "Gpella@utec.edu.pe", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $member = NetworkMember::create([ 'name' => "Pontificia Universidad Católica del Perú", 'network_id' => $network->id, 'city' => "Lima", 'country' => "Peru", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Juan Guisepe Montalván Lume", "email" => "jgmontalvan@pucp.pe", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Milagros Diez Canseco", "email" => "mdiez@pucp.pe", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Carla Fiorella Flores Guardia", "email" => "carlafiorella.flores@mail.polimi.it", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $network = Network::create([ 'name' => "LeNS_Iran", 'created_by' => 1]);
        $member = NetworkMember::create([ 'name' => "Tehran University of Art", 'network_id' => $network->id, 'city' => "Tehran", 'country' => "Iran", 'created_by' => 1 ]);$contact = NetworkMember::create([ "name" => "Mehdi A. Fallah", "email" => "M.fallah@art.ac.ir", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);
        $contact = NetworkMember::create([ "name" => "Media Hosseini", "email" => "Media.hosseini@mail.polimi.itg", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "University of Tehran", 'network_id' => $network->id, 'city' => "Tehran", 'country' => "Iran", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Alireza Ajdari", "email" => "alireza.ajdari@ut.ac.ir", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "Iran University of Science and Technology", 'network_id' => $network->id, 'city' => "Tehran", 'country' => "Iran", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Nasser Koleini Mamaghani", "email" => "koleini@iust.ac.ir", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);

        $member = NetworkMember::create([ 'name' => "University of Tehran, Kish International Campus", 'network_id' => $network->id, 'city' => "Kish Island", 'country' => "Iran", 'created_by' => 1 ]);
        $contact = NetworkMember::create([ "name" => "Yasaman Khodadade", "email" => "khodadade@ut.ac.ir", "email2" => "" , "network_member_id" => $member->id, "created_by" => 1 ]);






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
