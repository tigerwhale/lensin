<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPages1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('pages')->insert([
            ["created_by_id" => "1" , "name" => "Home page", "permalink" => "home"],
            ["created_by_id" => "1" , "name" => "About", "permalink" => "about"],
            ["created_by_id" => "1" , "name" => "Tutorial", "permalink" => "tutorial"],
            ["created_by_id" => "1" , "name" => "Network", "permalink" => "network"],
            ["created_by_id" => "1" , "name" => "Labs", "permalink" => "labs"],
            ["created_by_id" => "1" , "name" => "Contact", "permalink" => "contact"]
        ]);   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
