<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Lecture;
use App\Language;

class FixLectureLng2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get all lectures
        $lectures = Lecture::all();

        foreach ($lectures as $lecture) 
        {
            // get lecture langugae id
            if ($lecture->language_id) 
            {
                // get and check if language exists 
                if ($language = Language::find($lecture->language_id)) 
                {
                    // change lecture_id to locale
                    $lecture->language_id = $language->locale;
                    // save lecture
                    $lecture->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
