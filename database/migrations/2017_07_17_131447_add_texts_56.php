<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts56 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'ngo_type')->first();
        $translation->text = "Non Government Organisation";
        $translation->save();

        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'school_type')->first();
        $translation->text = "University/Schools";
        $translation->save();

        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'school_type')->first();
        $translation->text = "University/Schools";
        $translation->save();

        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'company_type')->first();
        $translation->text = "Organisation/Company";
        $translation->save();

        $translation = \App\LanguageTranslation::where('item', 'LIKE', 'research_interest')->first();
        $translation->text = "Interests in sustainability";
        $translation->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
