<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('page_texts')->insert([
            // home
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 1, "title" => 'Learn sustainability for all', "text" => '
                            <p style="margin-top:20px;">
                                View videos, slideshows, tools, projects and study cases<br>
                                to learn designing for sustainability for all<br>
                                Download and adapt them to your needs and desires<br>
                                to support preparing your own course 
                            </p>
                            
                            <!-- VIDEO -->                        
                            <div style="width: 100%; text-align: center;">
                                <video src="/images/home.mp4" controls="" style="height:300px; text-align: center"></video>

                                <!-- COOKIE -->
                                <div class="close-div">
                                  <a href="javascript:void(0)"><i class="close-button">×</i>
                                  Hide introduction next time?</a>
                                </div>

                            </div>
            '], 
            // about
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 2, "title" => 'LeNS International', "text" => '
                <p style="margin-top:20px;">LeNSin, the International Learning Network of networks on Sustainability (2015-2018), is an EU-supported (ERASMUS+) project involving 36 universities from Europe, Asia, Africa, South America and Central America, aiming at the promotion of a new generation of designers (and design educators) capable to effectively contribute to the transition towards a sustainable society for all.</p><p>    LeNSin ambitions to improve the internationalisation, intercultural cross-fertilisation and accessibility of higher education on Design for Sustainability (DfS). The project focuses on Sustainable Product-Service Systems (S.PSS) and Distributed Economies (DE) – considering both as promising models to couple environmental protection with social equity, cohesion and economic prosperity – applied in different contexts around the world. LeNSin connects a multi-polar network of Higher Education Institutions adopting and promoting a learning-by-sharing knowledge generation and dissemination, with an open and copyleft ethos.</p><p>    During the three years of operation, LeNSin project activities involve five seminars, ten pilot courses, the setting up of ten regional LeNS Labs, and of a (decentralised) open web platform, any students/designers and any teachers can access to download, modify/remix and reuse an articulated set of open and copyleft learning resources, i.e. courses/lectures, tools, cases, criteria, projects.</p><p>    LeNSin will also promote a series of diffusion activities targeting the design community worldwide. The final event will be a decentralised conference in 2018, based simultaneously in six partner universities, organised together by the 36 project partners form four continents.</p><p>    Join the project network by subscribing to our <a href="http://goo.gl/forms/9IvcDK5WuFpJOz5k1" target="_blank"><strong>newsletter!</strong></a><br>
                    <div class="row">
                        <div class="col-xs-12" style="text-align: left;">

                            <h2>LeNS Partners</h2><br>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#LeNS_Brazil">
                                                <h4>LeNS_Brazil</h4>
                                                
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="LeNS_Brazil" class="panel-collapse collapse">
                                        <div class="panel-body">                     

                                            <strong>PARTNERS:<br></strong>
                                            Universidade Federal do Paraná (UFPR)<br>
                                            Universidade Federal de Pernambuco (UFPE)<br><br>

                                            <strong>ASSOCIATES:<br></strong>
                                            Londrina State University<br>
                                            Fluminense Federal University<br>
                                            Federal University of Alagoas<br>
                                            Federal University of Uberlândia<br>
                                            Federal University of Santa Catarina<br><br>

                                            <strong>CONTACT:<br></strong>
                                            Aguinaldo Dos Santos <a href="mailto:asantos@ufpr.br"> | email </a><br>
                                            Leonardo Gomez Castillo <a href="mailto:leonardo.a.gomez@gmail.com"> | email </a><br><br>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#LeNS_India_1">
                                                <h4>LeNS_India</h4>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="LeNS_India_1" class="panel-collapse collapse">
                                        <div class="panel-body">                     
                                            <strong>PARTNERS:<br></strong>
                                            Srishti Institute of Art, Design and Technology<br>
                                            Indian Institute of Technology Guwahati (IIT Guwahati)<br><br>

                                            <strong>ASSOCIATES:<br></strong>
                                            C.A.R.E. School of Architecture<br>
                                            Pandit Dwarka Prasad Mishra Indian Institute of Information Technology<br>
                                            Indian Institute Of Technology Gandhinagar<br>
                                            Goa College of Architecture<br>
                                            Hunnarshala Foundation for Building Technology & Innovations<br>
                                            Vastu Shilpa Foundation<br><br>

                                            <strong>CONTACT:<br></strong>
                                            Mary Jacob <a href="mailto:maryjacob@srishti.ac.in"> | email </a><br>
                                            Ravi Mokashi <a href="mailto:mokashi@iitg.ernet.in"> | email </a><br><br>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#china_1">
                                                <h4>LeNS_China</h4>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="china_1" class="panel-collapse collapse">
                                        <div class="panel-body">                     
                                            <strong>PARTNERS:<br></strong>
                                            Tsinghua University<br>
                                            Hunan University (HNU)<br><br>

                                            <strong>ASSOCIATES:<br></strong>
                                            Wuhan University of Technology<br>
                                            Jiangnan University<br>
                                            The University of Science and Technology Beijing<br>
                                            Beijing Information Science and Technology University<br>
                                            The Hong Kong Polytechnic University<br>
                                            Guangzhou academy of fine arts<br>
                                            Tongji University<br><br>

                                            <strong>CONTACT:<br></strong>
                                            Liu Xin <a href="mailto:xinl@tsinghua.edu.cn"> | email </a><br>
                                            Jun Zhang <a href="mailto:zhangjun@hnu.edu.cn"> | email </a><br><br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#sa">
                                                <h4>LeNS_South Africa</h4>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="sa" class="panel-collapse collapse">
                                        <div class="panel-body">                     
                                            <strong>PARTNERS:<br></strong>
                                            Cape Peninsula University of Technology (CPUT), South Africa<br>
                                            Stellenbosch University (SUN), South Africa<br><br>

                                            <strong>ASSOCIATES:<br></strong>
                                            Farm and Garden National Trust<br>
                                            Cape Craft and Design Institute NPC<br><br>

                                            <strong>CONTACT:<br></strong>
                                            Ephias Ruhode  <a href="mailto:ruhodee@cput.ac.za"> | email </a><br>
                                            Marjorie Naidoo <a href="mailto:marjorie.naidoo@telkomsa.net"> | email </a><br><br>                                     
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#mexico_1">
                                                <h4>LeNS_Mexico</h4>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="mexico_1" class="panel-collapse collapse">
                                        <div class="panel-body">                     
                                            <strong>PARTNERS:<br></strong>
                                            Universidad Autónoma Metropolitana (UAM)<br>
                                            Universidad del Valle de México (UVM)<br><br>

                                            <strong>ASSOCIATES:<br></strong>
                                            Univesidad National Autónoma Metropolitana<br>
                                            Instituto Tecnológico de Monterrey Campus Ciudad de México<br><br>

                                            <strong>CONTACT:<br></strong>
                                            Alejandro Ramírez Lozano<a href="mailto:ramloz@correo.azc.uam.mx"> | email </a><br>
                                            Rodrigo Lépez Vela <a href="mailto:rodrigo.lepezv@uvmnet.edu"> | email </a><br>
                                            Sandra Molina Mata <a href="mailto:unasandra@yahoo.com.mx"> | email </a><br><br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#eu_1">
                                                <h4>LeNS_Europe</h4>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="eu_1" class="panel-collapse collapse">
                                        <div class="panel-body">                     
                                            PARTNERS AND CONTACTS:<br><br>

                                            Politecnico di Milano (POLIMI), Design Dept., Italy<br>
                                            Carlo Vezzoli (project coordinator)<br>
                                            <a href="mailto:carlo.vezzoli@polimi.it"> | email </a><br><br>

                                            Aalto University (Aalto ARTS), Finland<br>
                                            Cindy Kohtala <a href="mailto:cindy.kohtala@aalto.fi"> | email </a><br><br>

                                            Brunel University London (UBRUN), UK<br>
                                            Fabrizio Ceschin <a href="mailto:fabrizio.ceschin@brunel.ac.uk"> | email </a><br><br>

                                            Technische Universiteit Delft (TU Delft), Netherlands<br>
                                            Jan Carel Diehl <a href="mailto:jc_diehl@me.com"> | email </a><br><br>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <br> Previous newsletters:<br> <a href="http://eepurl.com/cvMmtv" target="_blank"><strong>LeNSin project newsletter #3</strong> (1/2017)</a><br>        <a href=http://eepurl.com/ckruST" target="_blank"><strong>LeNSin project newsletter #2</strong> (10/2016)</a><br>        <a href=http://eepurl.com/b60LT9" target="_blank"><strong>LeNSin project newsletter #1</strong> (6/2016)</a></p>
                '], 
            // tutorial
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 3, "title" => 'LeNS Tutorials', "text" => 'Work in progress'], 
            // network
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 4, "title" => 'LeNS Network', "text" => '
                       
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#africa">
                                            <h4>LeNS_Africa</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="africa" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>
                                        Cape Peninsula University of Technology (CPUT), South Africa<br>
                                        Retha De La Harpe <a href="mailto:delaharper@cput.ac.za"> | email </a><br>
                                        Ephias Ruhode <a href="mailto:RuhodeE@cput.ac.za"> | email </a><br>
                                        Johan Van Niekerk <a href="mailto:VanNiekerkJ@cput.ac.za"> | email </a><br>
                                        Mugendi M’Rithaa <a href="mailto:mugendim@gmail.com"> | email </a><br>
                                        Munira Alliem <a href="mailto:alliem@cput.ac.za"> | email </a><br><br>

                                        Stellenbosch University (SUN), South Africa<br>
                                        Elmarie Costandius <a href="mailto:elmarie@sun.ac.za"> | email </a><br>
                                        Karolien Perold-Bull <a href="mailto:karolien@sun.ac.za"> | email </a><br>
                                        Marjorie Naidoo <a href="mailto:marjorie.naidoo@telkomsa.net"> | email </a><br>
                                        Andrea Broom <a href="mailto:andrea.broom@gmail.com"> | email </a><br><br>

                                        Farm and Garden National Trust<br>
                                        Elmarie Costandius <a href="mailto:elmarie@sun.ac.za"> | email </a><br><br>

                                        Cape Craft and Design Institute NPC<br>
                                        Karolien Perold-Bull <a href="mailto:karolien@sun.ac.za"> | email </a><br><br>

                                        Tshwane University of Technology<br>
                                        Marjorie Naidoo <a href="mailto:marjorie.naidoo@telkomsa.net"> | email </a><br><br>

                                        University of Johannesburg<br>
                                        Angus Donald Campbell <a href="mailto:acampbell@uj.ac.za"> | email </a><br>
                                        Martin Bolton <a href="mailto:mbolton@uj.ac.za"> | email </a><br><br>

                                        University of Botswana<br>
                                        Richie Moalosi <a href="mailto:MOALOSI@mopipi.ub.bw"> | email </a><br>
                                        Paulson Letsholo <a href="mailto:letshopm@mopipi.ub.bw"> | email </a><br>
                                        Yaone Rapitsenyane <a href="mailto:yaone.rapitsenyane@mopipi.ub.bw"> | email </a><br><br>

                                        Botswana International University of Science and Technology (BIUST)<br>
                                        Tunde Oladiran <a href="mailto:oladirant@biust.ac.bw"> | email </a><br><br>

                                        University of Nairobi<br>
                                        Lilac Osanjo <a href="mailto:lilac.osanjo@uonbi.ac.ke"> | email </a><br>
                                        Lorraine Amollo <a href="mailto:amollo.lorraine@gmail.com"> | email </a><br>

                                        Machakos University<br>
                                        Sophia Njeru <a href="mailto:sophianjeru2010@gmail.com"> | email </a><br><br>

                                        Makerere University<br>
                                        Venny Nakazibwe <a href="mailto:vnakazibwe@gmail.com"> | email </a><br>
                                        Mackay Okure <a href="mailto:mokure@tech.mak.ac.ug"> | email </a><br>
                                        Mary Suzan Abbo <a href="mailto:msabbo@creec.or.ug"> | email </a><br><br>

                                        Kyambogo University<br>
                                        Emmanuel Mutungi <a href="mailto:mutungiemmanuel@yahoo.com"> | email </a><br><br>

                                        Kwame Nkurumah University of Science and Technology<br>
                                        Eddie Appiah <a href="mailto:eddappiah@gmail.com"> | email </a><br><br>

                                        The Federal University of Technology<br>
                                        Sunday Roberts Ogunduyile <a href="mailto:sunny_duyile@yahoo.com"> | email </a><br><br>

                                        Zimbabwe institute of Vigital Arts<br>
                                        Saki Mafundikwa <a href="mailto:sakimaf@gmail.com"> | email </a><br><br>

                                        Namibia University of Science and Technology<br>
                                        Shilumbe Chivuno Kuria <a href="mailto:schivuno@nust.na"> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#argentina">
                                            <h4>LeNS_Argentina</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="argentina" class="panel-collapse collapse">
                                    <div class="panel-body">    
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>                 
                                        INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata<br>
                                        Edurne Battista<br>
                                        <a href="mailto:edurnebattista@gmail.com"> | email1</a>, <a href="mailto:battista.edurne@inta.gob.ar"> | email2 </a><br><br>

                                        UNLP Universidad Nacional de La Plata<br>
                                        Ana Bocos <a href="mailto:anabocos@gmail.com"> | email </a><br>
                                        Alejandra Sivila Soza <a href="mailto:alejandrasivilasoza@gmail.com"> | email </a><br><br>

                                        INTA Instituto Nacional de Tecnología Agropecuaria y UNLP Universidad Nacional de La Plata<br>
                                        Sergio Justianovich <a href="mailto:justianovich.sergio@inta.gob.ar"> | email </a><br><br>

                                        IHAM Instituto del Habitat y del Ambiente, FAUD-Facultad de Arquitectura Urbanismo y Diseño UNMdP-Universidad Nacional de Mar del Plata<br>
                                        Mariana Gonzalez Insua<br><br>
                                        <a href="mailto:gonzalezinsuamariana@conicet.gov.ar"> | email1 </a>, <a href="mailto:gonzalezinsuamariana@hotmail.com"> | email </a><br><br>

                                        Universidad Nacional de Lanús, Centro Internacional de Diseño del Conocimiento Tomás Maldonado (MinCyT), UBA-Universidad de Buenos Aires<br>
                                        Roxana Garbarini <a href="mailto:rgarbarini@mincyt.gob.ar"> | email </a><br>
                                        Clara Tapia <a href="mailto:claritapia@gmail.com"> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#brasil">
                                            <h4>LeNS_Brazil</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="brasil" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>
                                        Universidade Federal do Paraná (UFPR)<br>
                                        Aguinaldo Dos Santos <a href="mailto:asantos@ufpr.br"> | email </a><br>
                                        Adriane Santos <a href="mailto:adriane.shibata@univille.net"> | email </a><br>
                                        Klarissa Saes <a href="mailto:ufpr.internacional@ufpr.br"> | email </a><br>
                                        Isadora Dickie <a href="mailto:isadora.dickie@gmail.com"> | email </a><br>
                                        Alexandre Oliveira <a href="mailto:aleantoli@gmail.com"> | email </a><br>
                                        Naotake Fukushima <a href="mailto:profnaotake@gmail.com"> | email </a><br>
                                        João Caccere <a href="mailto:jcaccere@gmail.com"> | email </a><br>
                                        Ana Cristina Broega <a href="mailto:cbroega@det.uminho.pt"> | email </a><br>
                                        Camila Hérnandez <a href="mailto:camilahernandez.sta@gmail.com"> | email </a><br><br>

                                        Universidade Federal de Pernambuco (UFPE)<br>
                                        Leonardo Gomez Castillo <a href="mailto:leonardo.a.gomez@gmail.com"> | email </a><br><br>

                                        Londrina State University<br>
                                        Suzana Barreto <a href="mailto:suzanabarreto@onda.com"> | email </a><br>
                                        Claudio Pereira <a href="mailto:qddesign@hotmail.com"> | email </a><br><br>

                                        Fluminense Federal University<br>
                                        Liliane Item Chaves <a href="mailto:chaves.liliane@gmail.com"> | email </a><br>

                                        Federal University of Alagoas<br>
                                        Priscilla Ramalho Lepre <a href="mailto:cillaramalho@yahoo.com"> | email </a><br><br>

                                        Federal University of Uberlândia<br>
                                        Viviane Nunes <a href="mailto:vivianenunes.br@gmail.com"> | email </a><br><br>

                                        Federal University of Santa Catarina<br>
                                        Eugenio Merino <a href="mailto:eadmerino@gmail.com"> | email </a><br><br>

                                        Universidade do Estado de Minas Gerais -UEMG<br>
                                        Escola de Design<br>
                                        Dijon De Moraes <a href="mailto:dijon.moraes@uemg.br"> | email </a><br>
                                        Rita Engler <a href="mailto:rcengler@uol.com.br"> | email </a><br>
                                        Rosemary Bom Conselho <a href="mailto:rosebcs@gmail.com"> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_China">
                                            <h4>LeNS_China</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_China" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>
                                        Tsinghua University<br>
                                        Liu Xin <a href="mailto:xinl@tsinghua.edu.cn"> | email </a><br>
                                        Xian Nan <a href="mailto:squallxn@hotmail.com"> | email </a><br>
                                        Zhong Fang <a href="mailto:zhongfangpoli@gmail.com"> | email </a><br>
                                        Lv Mingue <a href="mailto:lvmoon@163.com"> | email </a><br><br>

                                        Hunan University (HNU)<br>
                                        Jun Zhang <a href="mailto:zhangjun@hnu.edu.cn"> | email </a><br>
                                        <a href="mailto:zhangjuncoco@gmail.com"> | email </a><br>
                                        Eun Jin Cho <a href="mailto:taowood@gmail.com"> | email </a><br>
                                        Renke He <a href="mailto:Renke8@163.com"> | email </a><br>
                                        Jiing Ou <a href="mailto:jingou@hnu.edu.cn"> | email </a><br>
                                        Huang Yisong <a href="mailto:hys_id@126.com"> | email </a><br>
                                        Liu Yen <a href="mailto:1542892325@qq.com"> | email </a><br>
                                        Jia Yanyang <a href="mailto:574664242@qq.com"> | email </a><br><br>

                                        Wuhan University of Technology<br>
                                        Lv Jiefeng <a href="mailto:whutljf@126.com"> | email </a><br>
                                        Shaohua Han <a href="mailto:hanshaohua@msn.com"> | email </a><br><br>

                                        Jiangnan University<br>
                                        Zhang Linghao <a href="mailto:wowo.zlh@163.com"> | email </a><br>
                                        Qian Xiaobo <a href="mailto:qianxiaobo0118@gmail.com"> | email </a><br><br>

                                        The University of Science and Technology Beijing<br>
                                        Qin Jingyan <a href="mailto:qinjingyan@gmail.com"> | email </a><br><br>

                                        Beijing Information Science and Technology University<br>
                                        Li Honghai <a href="mailto:lihonghai@vip.sina.com"> | email </a><br><br>

                                        The Hong Kong Polytechnic University<br>
                                        Benny Leong <a href="mailto:sdbenny@polyu.edu.hk"> | email1 </a>, <a href="mailto:benny.leong@polyu.edu.hk"> | email2 </a><br>
                                        Brian Lee <a href="mailto:brian.yh.lee@polyu.edu.hk"> | email </a><br><br>

                                        Guangzhou academy of fine arts<br>
                                        Feng Shu <a href="mailto:jumoi2001@qq.com"> | email </a><br>
                                        Wang LiuZhuang <a href="mailto:wangliuzhuang@163.com"> | email </a><br>

                                        Tongji University<br>
                                        Gao Bo <a href="mailto:gaobotj@163.com"> | email </a><br><br>                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_Colombia">
                                            <h4>LeNS_Colombia</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_Colombia" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    <strong>MEMBERS AND CONTACTS:<br><br></strong>                   
                                    University El Bosque<br>
                                    Carolina Montoya Rodriguez<br>
                                    <a href="mailto:montoyacarolina@unbosque.edu.co"> | email </a><br>
                                    <a href="mailto:carolina.montoya.rodriguez@gmail.com"> | email </a><br>
                                    Mariana Buraglia Osorio<br>
                                    <a href="mailto:buragliamarianao@unbosque.edu.co"> | email </a><br>
                                    <a href="mailto:mburaglia@gmail.com"> | email </a><br>

                                    University Javeriana<br>
                                    Lucas Ivorra <a href="mailto:ivorra@javeriana.edu.co"> | email </a><br>
                                    Ricardo Rugeles <a href="mailto:rugeles-w@javeriana.edu.co"> | email </a><br>
                                    Pedro Sanchez <a href="mailto: pedrosanchez@javeriana.edu.co"> | email </a><br><br>

                                    University UDI<br>
                                    Adolfo Vargas  <a href="mailto:avargas2@udi.edu.co"> | email </a><br><br>

                                    University De Pamplona<br>
                                    Sandra Forero <a href="mailto:sforero@unipamplona.edu.co"> | email </a><br>
                                    Carlos Luna <a href="mailto: cmluna@unipamplona.edu.co"> | email </a><br><br>

                                    University de Medellín<br>
                                    Lina María Agudelo <a href="mailto:lmagudelo@udem.edu.co"> | email </a><br><br>

                                    University de Los Andes<br>
                                    Carolina Obregón <a href="mailto:c.obregon101@uniandes.edu.co"> | email </a><br><br>

                                    University Colegio Mayor de Cundinamarca<br>
                                    Sandra Uribe Perez <a href="mailto:sauripez@yahoo.com"> | email </a><br>
                                    Sergio A. Ballén Z. <a href="mailto:sergio.ballen@yahoo.com"> | email </a><br><br>

                                    University Jorge Tadeo Lozano<br>
                                    Fernando Álvarez <a href="mailto: fernando.alvarez@utadeo.edu.co"> | email </a><br><br>

                                    University San Buenaventura de Medellín<br>
                                    Diomar Elena  Calderón Riaño <a href="mailto: diomar.calderon@usbmed.edu.co"> | email </a><br><br>                                  </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_Europe">
                                            <h4>LeNS_Europe</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_Europe" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>
                                        Politecnico di Milano (POLIMI), Design Dept., Italy<br>
                                        Carlo Vezzoli <a href="mailto:carlo.vezzoli@polimi.it"> | email </a><br>
                                        Emanuel Delfino <a href="mailto:emanuela.delfino@polimi.it"> | email </a><br>
                                        Elisa Bacchetti <a href="mailto:elisa.bacchetti@polimi.it"> | email </a><br>
                                        Fiammetta Costa <a href="mailto:fiammetta.costa@polimi.it"> | email </a><br><br>

                                        Technische Universiteit Delft (TU Delft), Netherlands<br>
                                        Jan Carel Diehl <a href="mailto:jc_diehl@me.com"> | email </a><br><br>

                                        Aalto University (Aalto ARTS), Finland<br>
                                        Cindy Kohtala <a href="mailto:cindy.kohtala@aalto.fi"> | email </a><br>
                                        Tatu Marttila <a href="mailto:tatu.marttila@aalto.fi"> | email </a><br>
                                        Idil Gaziulusoy idil.gaziulusoy@aalto.fi<a href="mailto:"> | email </a><br><br>

                                        Brunel University London (UBRUN), UK<br>
                                        Fabrizio Ceschin <a href="mailto:fabrizio.ceschin@brunel.ac.uk"> | email </a><br>
                                        Silvia Emili <a href="mailto:silvia.emili@brunel.ac.uk"> | email </a><br>
                                        Aine Petrulaityte <a href="mailto:aine.petrulaityte@brunel.ac.uk"> | email </a><br><br>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_EU">
                                            <h4>LeNS_EU German speaking languages</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_EU" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>

                                        Institute of Design Research Vienna, Vienna, Austria<br>
                                        Harald Gruendl <a href="mailto:hg@idrv.org"> | email </a><br>
                                        Ulrike Haele <a href="mailto:uh@idrv.org"> | email </a><br>
                                        Marco Kellhammer <a href="mailto:mk@idrv.org"> | email </a><br><br>

                                        University of Applied Arts, Vienna, Austria<br>
                                        Peter Knobloch <a href="mailto:peter.knobloch@uni-ak.ac.at"> | email </a><br><br>

                                        Technical University Vienna, Vienna, Austria<br>
                                        Anton Kottbauer <a href="mailto:anton.kottbauer@tuwien.ac.at"> | email </a><br><br>

                                        FH Salzburg, Salzburg, Austria<br>
                                        Michael Leube <a href="mailto:michael.leube@fh-salzburg.ac.at"> | email </a><br><br>

                                        HfG Karlsruhe, Karlsruhe, Germany<br>
                                        Volker Albus <a href="mailto:va@hfg-karlsruhe.de"> | email </a><br><br>

                                        Technical University Munich, Munich, Germany<br>
                                        Fritz Frenkler <a href="mailto:f.frenkler@lrz.tum.de"> | email </a><br><br>

                                        University of Applied Sciences and Arts Northwestern Switzerland FHNW, Basel, Switzerland<br>
                                        Heinz Wagner<a href="mailto: heinz.wagner@fhnw.ch "> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_India">
                                            <h4>LeNS_India</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_India" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>

                                        Srishti Institute of Art, Design and Technology<br>
                                        Geetha Narayanan <a href="mailto: g_narayanan@srishti.ac.in"> | email </a><br>
                                        Mary Jacob <a href="mailto:maryjacob@srishti.ac.in"> | email </a><br>
                                        Ranjani Balasubramanian <a href="mailto:ranjani.b@srishti.ac.in"> | email </a><br>
                                        Naveen Bagalkot <a href="mailto:naveen@srishti.ac.in"> | email </a><br><br>

                                        Indian Institute of Technology Guwahati (IIT Guwahati)<br>
                                        Ravi Mokashi <a href="mailto:mokashi@iitg.ernet.in"> | email </a><br>
                                        Sharmistha Banerjee <a href="mailto:banerjee.sharmistha@gmail.com"> | email </a><br>
                                        Pankaj Upadhyay <a href="mailto:pankaj.nitsil@gmail.com"> | email </a><br><br>

                                        C.A.R.E. School of Architecture<br>
                                        Godwin Emmanuel <a href="mailto:godwinarchitect@gmail.com"> | email </a><br><br>

                                        Pandit Dwarka Prasad Mishra Indian Institute of Information Technology<br><br>

                                        Indian Institute Of Technology Gandhinagar<br>
                                        Achal Mehra <a href="mailto:achal@iitgn.ac.in"> | email </a><br>
                                        Harish M. P. <a href="mailto:harish@iitgn.ac.in"> | email </a><br>
                                        Franklin Kristi <a href="mailto:franklin@iitgn.ac.in"> | email </a><br>

                                        Goa College of Architecture<br>
                                        Ashish K. Rege <a href="mailto:gca.principal@gmail.com"> | email </a><br>
                                        Vishvesh Kandolkar <a href="mailto:wishvesh@gmail.com"> | email </a><br><br>

                                        Hunnarshala Foundation for Building Technology & Innovations<br>
                                        Neelkanth Chhaya <a href="mailto:niluchhaya@gmail.com"> | email </a><br>
                                        Sandeep Virmani <a href="mailto:hunnarshala@yahoo.co.in"> | email </a><br><br>

                                        Vastu Shilpa Foundation<br>
                                        Piyas Choudhuri <a href="mailto:piyas@sangath.org"> | email </a><br>
                                        Joseph Varughese <a href="mailto:vsf@sangath.org"> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#LeNS_Italia">
                                            <h4>LeNS_Italia</h4>
                                        </a>
                                    </h4>
                                </div>

                                <div id="LeNS_Italia" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>

                                        Politecnico di Milano<br>
                                        Carlo Vezzoli <a href="mailto:carlo.vezzoli@polimi.it"> | email </a><br>
                                        Carlo Proserpio <a href="mailto:carlo.proserpio@polimi.it"> | email </a><br>
                                        Elisa Bacchetti <a href="mailto:elisa.bacchetti@polimi.it"> | email </a><br><br>

                                        Politecnico di Torino<br>
                                        Luigi Bistagnino <a href="mailto:bistagnino@polito.it"> | email </a><br>
                                        Silvia Barbero <a href="mailto:silvia.barbero@polito.it"> | email </a><br>
                                        Paolo Tamborrini <a href="mailto:paolo.tamborrini@polito.it"> | email </a><br><br>

                                        Università degli studi di Firenze<br>
                                        Giuseppe Lotti <a href="mailto:giuseppe.lotti@unifi.it"> | email </a><br>
                                        Maria Rosanna Fossati <a href="mailto:maria_fossati@yahoo.it"> | email </a><br><br>

                                        Università di Chieti<br>
                                        Antonio Marano <a href="mailto:a.marano@unich.it"> | email </a><br><br>

                                        Università Federico II di Napoli<br>
                                        Rosanna Veneziano <a href="mailto:rosanna.veneziano@unina2.it"> | email </a><br>
                                        Patrizia Ranzo <a href="mailto:direzione.di@unina2.it"> | email </a>; <a href="mailto:cappellieranzo@usa.net"> | email </a><br>
                                        Carla Langella <a href="mailto:carla.langella@unina2.it"> | email </a><br>
                                        Roberto Liberti <a href="mailto:robertoliberti2@virgilio.it"> | email </a><br>
                                        Francesca Larocca <a href="mailto:f.larocca@mclink.it"> | email </a><br><br>

                                        Università di Camerino<br>
                                        Lucia Pietroni <a href="mailto:lucia.pietroni@unicam.it"> | email </a><br><br>

                                        Università di Genova<br>
                                        Paola Gambaro <a href="mailto:gambaro@leonardo.arch.unige.it"> | email </a><br>
                                        Adriano Magliocco<br><br>

                                        Università IUAV di Venezia<br>
                                        Medardo Chiapponi <a href="mailto:medardo.chiapponi@iuav.it"> | email </a><br>
                                        Laura Badalucco <a href="mailto:laurabada@tiscali.it"> | email </a><br><br>

                                        Università di Palermo<br>
                                        Anna Catania <a href="mailto:annacatania16@gmail.com"> | email </a><br><br>

                                        Sapienza università di Roma<br>
                                        Cecilia Cecchini <a href="mailto:cecilia.cecchini@uniroma1.it"> | email </a><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <i class="fa fa-caret-down pull-right fa-lg" style="line-height:2.2em" aria-hidden="true"></i>
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#mexico">
                                            <h4>LeNS_Mexico</h4><br>
                                        </a>
                                    </h4>
                                </div>

                                <div id="mexico" class="panel-collapse collapse">
                                    <div class="panel-body">                     
                                        <strong>MEMBERS AND CONTACTS:<br><br></strong>
                                        Univesidad Autonoma Metropolitana<br>
                                        Alejandro Ramirez Lozano <a href="mailto:ramloz@correo.azc.uam.mx"> | email </a><br>
                                        Sandra Molina Mata <a href="mailto:unasandra@yahoo.com.mx"> | email </a><br>
                                        Brenda Garcia Parra <a href="mailto:brendagparra@gmail.com"> | email </a><br>

                                        Universidad dela Valle de México<br>
                                        José Eduardo Camacho G <a href="mailto:protoccolo@yahoo.com"> | email </a><br>
                                        Rodrigo Lepez Vela <a href="mailto:rodrigo.lepezv@uvmnet.edu"> | email </a><br><br>

                                        Univesidad National Autonoma Metropolitana<br>
                                        Ana María Losada Alfaro <a href="mailto:pdi_sacad@posgrado.unam.mx"> | email </a><br><br>

                                        Instituto Tecnológico de Monterrey Campus Ciudad de México<br><br>
                                    </div>
                                </div>
                            </div>
                        </div>



                        </div>
                    </div>
                      
                '], 
            // labs
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 5, "title" => 'LeNS Labs', "text" => '

                        <p style="margin-top:20px;">
                            A network of LeNS Labs is in-progress to be established in Partner Countries (Brazil, Africa, Mexico, China, India) as equipped spaces accessible to students, teachers, researchers as well as local interested stakeholders to use a set of tools, resources and facilities to support Design for Sustainability (DfS) research, education and practice. Other LeNS Labs worldwide have been opened and are listed below (though not funded by EU under the Erasmus+ project).
                        </p>
                        <p>
                            LeNS Labs are aimed at:<br>
                            1. the dissemination, the sharing and the development of knowledge-base and know-how on Design for Sustainability, supported by this Platform of platforms;<br>
                            2. promoting research activities, teaching and internationalization, being connected to the multipolar worldwide scheme of LeNS_Labs, as well as with local and global HEIs, by adopting an intercultural approach to favour knowledge cross-fertilisation;<br>
                            3. strengthening the link with the local productive sectors, acting as a hub.
                        </p><br>
                        <p>
                            LeNS India:<br>
                            LeNS Lab Srishti Institute of Art, Design and Technology, Bangalore, India<br>
                            Contact: <a href="mailto:maryjacob@srishti.ac.in">Mary Jacob</a><br><br>

                            LeNS Lab/Sustainability and Social Innovation Lab, Department of Design, IIT Guwahati,
                            Guwahati, India<br>
                            Contact: <a href="mailto:sharmistha@iitg.ernet.in">Sharmistha Banerjee</a><br>
                            Website: SSi Lab<br><br>

                            LeNS  Africa:<br>
                            LeNS Lab Design Garage at Cape Peninsula University of Technology, Cape Town, South Africa<br>
                            Contact: <a href="mailto:RuhodeE@cput.ac.za">Ephias Ruhode</a><br><br>

                            LeNS Lab Department of Visual Arts at Stellenbosch University, Stellenbosch, South Africa<br>
                            Contact:  <a href="mailto:elmarie@sun.ac.za">Elmarie Constandius</a><br><br>

                            LeNS Mexico:<br>
                            LeNS Lab Universidad Autónoma Metropolitana (UAM) Azcapotzalco, Mexico City, Mexico<br>
                            Contact: <a href="mailto:unasandra@yahoo.com.mx">Sandra Molina</a><br><br>

                            LeNS Lab Universidad Autónoma Metropolitana (UAM) Cuajimalpa, Mexico City, Mexico<br>
                            Contact: <a href="mailto:brendagparra@gmail.com">Brenda Garcia Parra</a><br><br>

                            LeNS China:<br>
                            LeNS Lab Tsinghua University, Academy of Arts & Design, Blk B, RM464., Beijing, China<br>
                            Contact: <a href="mailto:xinl@tsinghua.edu.cn">Liu Xin</a><br><br>

                            LeNS Lab School of Design, N.319, Hunan University, Lushan South Road 1#, 410082, Changsha, China<br>
                            Contact: <a href="mailto:zhangjun@hnu.edu.cn">Jun Zhang</a><br><br>

                            LeNS Brazil:<br>
                            LeNS Lab Universidade Federal do Paraná (UFPR), Curitiba, Brazil<br>
                            Contact: <a href="mailto:asantos@ufpr.br">Aguinaldo Dos Santos</a><br><br>

                            LeNS Lab Inovation, Design and Sustainability Lab, Federal University of Pernambuco, UFPE<br>
                            Contact: Leonardo Gómez Castillo <leonardo.a.gomez@gmail.com><br><br>

                            LeNS Europe:<br>
                            LeNS Lab Politecnico di Milano, Milan, Italy<br>
                            Contact: <a href="mailto:carlo.vezzoli@polimi.it">Carlo Vezzoli</a><br>
                            Website: LeNSlab_POLIMI<br><br>

                            Other LeNS Labs worldwide will open soon!
                        </p>
            '], 
            // contact
            ["locale" => "en" , "created_by_id" => 1, "page_id" => 6, "title" => 'LeNS Contact', "text" => 'Work in progress'], 

        ]);   
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
