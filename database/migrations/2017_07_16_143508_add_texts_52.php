<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts52 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "users", "item" => "register_participant_text", "text" => "
                Anyone can join the LeNS network, either a teacher, a researcher, a student, a designer or anyone interested to learn about design for sustainability. Joining the LeNS Network allows free view and download of all the Learning Resources: courses, single lectures, tools, case studies, criteria and guidelines, projects, etc. In addition, you will receive newsletters, updates about the project latest developments, information about new available resources, communication about LeNS events, information, publications, etc. 
                <br><br>
                <b>How to join the LeNSes network</b><br>
                Please fill in the record aside and press register. 
                You will receive an e-mail of confirmation and your personal username and password.
            "],
            ["locale" => "en" , "group" => "users", "item" => "register_member_text", "text" => "

                Any Design research and/or educational institutions, design companies, companies, NGOs, category association, governmental institutions, etc. can become a LeNS Member. By becoming a LeNS Member you will be and recognized as being part of an international community active in designing and diffusing sustainability. Your institutional and (desired) personal data will be posted on the LeNS Member page and you have the opportunity to publish news, request for information, partners for a project, etc.  
                <br><br>
                <b>How to become a member of the LeNS network</b> <br>
                Please fill in the record aside and press register. <br>
                LeNS staff will contact you when the your request has been accepted, i.e. you will appear on the LeNS member page.
            "],
            ["locale" => "en" , "group" => "users", "item" => "register_partner_text", "text" => "

                Any research and/or educational institutions (in design), can become a LeNS Partner joining an existing LeNS regional network. By becoming a LeNS Partner you will be part of an international community enabled to upload your own learning resources on design for sustainability, either courses, single lectures, tools, case studies, criteria, your students projects, etc. to make them available to all the LeNS community in open and copyleft ethos. So forth empowering the opportunities and the impact the same network can have to contribute to a more sustainable world for all. Your institutional and (desired) personal data will be posted on the LeNS Partner page and you have the opportunity to publish news, request for information, partners for a project, etc.  
                <br><br>
                <b>How to become a Partners of the LeNS network</b> <br>
                Please fill in the record aside and press register. <br>
                The LeNS regional network you selected will contact you when your request has been accepted, i.e. you will appear on the LeNS Partners page.

            "],
            ["locale" => "en" , "group" => "users", "item" => "register_new_lens_text", "text" => "
                Any research and/or educational institutions (in design), belonging to a region where a LeNS network doesn’t exist yet, if interested and committed, can decide to incubate and then launch a new regional LeNS. A new regional LeNS can download free a LeNS web platform and customize it on a local server, i.e. managing the same platform as local administrator, in particular enabling local partners to upload learning resources on design for sustainability. As far as the new local platform will be connected to all others (via a central platform), all is uploaded will be visible and downloadable by all.
                <br><br>
                <b>How to launch a new regional LeNS network</b><br>
                Please fill in the record aside and press register. <br>
                The LeNS international staff will contact you when your request has been accepted, i.e. you will appear on the LeNS regional list with all Partners you committed to join you that far.
            "],
        ]); 

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
