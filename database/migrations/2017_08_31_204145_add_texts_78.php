<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts78 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text("languages", "add_new_language_directions", "If there is no language in the list, here you can create a new language. Enter the locale (two letter ISO Alpha-2 code) and language name, and the language will be created and put in the list with english text. You can then proceed to translate text. When you finish, you can publish the language and distribute it to other platforms.");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
