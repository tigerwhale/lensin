<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts151 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('courses', "edit_resource", 'Edit resource');
        add_migration_text('resource_types', "video", 'Video');
        add_migration_text('resource_types', "slideshow", 'Slideshow');
        add_migration_text('resource_types', "ppt-audio", 'PPT / Audio');
        add_migration_text('resource_types', "audio", 'Audio');
        add_migration_text('resource_types', "document", 'Document');
        add_migration_text('resource_types', "other", 'Other');
        add_migration_text('resource_types', "image", 'Image');

        add_migration_text('text', "select_unit", 'Select unit');
        add_migration_text('text', "minutes", 'Minutes');
        add_migration_text('text', "hours", 'Hours');
        add_migration_text('text', "pages", 'Pages');
        add_migration_text('text', "slides", 'Slides');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
