<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts28 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "study_cases", "item" => "create_a_new_study_case", "text" => "Create a new study case"],
            ["locale" => "en" , "group" => "study_cases", "item" => "case_study", "text" => "Case study"]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
