<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = \App\PageText::where('title', "LeNS Labs")->first();
        $text->text = '
                        <p>A network of <strong>LeNS Labs</strong> is in-progress to be established in Partner Countries (Brazil, Africa, Mexico, China, India) as&nbsp;equipped spaces accessible to students, teachers, researchers as well as local interested stakeholders to use a set of tools, resources and facilities to support Design for Sustainability (DfS) research, education and practice.&nbsp;Other LeNS Labs worldwide have been opened and are listed below (though not funded by EU under the Erasmus+ project).</p>
                        <p>LeNS Labs are aimed at: 
                        <br>
                        <strong>.&nbsp;</strong>the dissemination, the sharing and the development of knowledge-base and know-how on Design for Sustainability, supported by this Platform of platforms; 
                        <br>
                        <strong>.&nbsp;</strong>promoting research activities, teaching and internationalization, being connected to the multipolar worldwide scheme of LeNS_Labs, as well as with local and global HEIs, by adopting an intercultural approach to favour knowledge cross-fertilisation; 
                        <br>
                        <strong>.&nbsp;</strong>strengthening the link with the local productive sectors, acting as a hub.</p>
                        <p><strong>LeNS Labs:</strong></p>
                        <p><span style="text-decoration: underline;">LeNS India:<br>
                        </span>LeNS Lab Srishti Institute of Art, Design and Technology, Bangalore, India<br>
                        <em>Contact:</em> Mary Jacob &lt;<a href="mailto:maryjacob@srishti.ac.in">maryjacob@srishti.ac.in</a>&gt;</p>
                        <div id="AppleMailSignature">LeNS Lab/Sustainability and Social Innovation Lab, Department of Design, IIT Guwahati,</div>
                        <div>Guwahati, India</div>
                        <p><em>Contact:</em>&nbsp;Sharmistha Banerjee&nbsp;&lt;<a href="mailto:sharmistha@iitg.ernet.in">sharmistha@iitg.ernet.in</a>&gt;<br>
                        <em>Website:</em>&nbsp;<a href="http://www.sustainability-and-social-innovation.com/">SSi Lab</a></p>
                        <p><span style="text-decoration: underline;">LeNS &nbsp;Africa:</span><strong><br>
                        </strong>LeNS Lab Design Garage at Cape Peninsula University of Technology, Cape Town, South Africa<br>
                        <em>Contact:</em>&nbsp;Ephias Ruhode&nbsp; &lt;<a href="mailto:RuhodeE@cput.ac.za">RuhodeE@cput.ac.za</a>&gt;</p>
                        <p>LeNS Lab Department of Visual Arts at Stellenbosch University, Stellenbosch, South Africa <em>Contact:</em>&nbsp;Elmarie Constandius &lt;<a href="mailto:andrea.broom@gmail.com">elmarie@sun.ac.za</a>&gt;</p>
                        <p><span style="text-decoration: underline;">LeNS Mexico:<br>
                        </span>LeNS Lab Universidad Autónoma Metropolitana (UAM) Azcapotzalco, Mexico City, Mexico<br>
                        <em>Contact:</em>&nbsp;Sandra Molina &lt;<a href="mailto:unasandra@yahoo.com.mx">unasandra@yahoo.com.mx</a><em>&gt;</em></p>
                        <p>LeNS Lab Universidad Autónoma Metropolitana (UAM) Cuajimalpa, Mexico City, Mexico<br>
                        <em>Contact:</em>&nbsp;Brenda Garcia Parra&nbsp;&lt;<a href="mailto:unasandra@yahoo.com.mx">brendagparra@gmail.com</a><em>&gt;</em></p>
                        <p><span style="text-decoration: underline;">LeNS China:<br>
                        </span>LeNS Lab Tsinghua University, Academy of Arts &amp; Design, Blk B, RM464., Beijing, China &nbsp; &nbsp; &nbsp; &nbsp; <em>Contact:</em> Liu xin &lt;<a href="mailto:xinl@tsinghua.edu.cn">xinl@tsinghua.edu.cn</a>&gt;</p>
                        <p>LeNS Lab School of Design, N.319, Hunan University, Lushan South Road 1#, 410082, Changsha, China<br>
                        <em>Contact:</em>&nbsp;Jun Zhang &lt;<a href="mailto:zhangjun@hnu.edu.cn">zhangjun@hnu.edu.cn</a>&gt;</p>
                        <p>LeNS Lab Wuhan University of Technology, Wuhan, China<br>
                        <em>Contact:</em>&nbsp;Shaohua Han &lt;<a href="mailto:hanshaohua@msn.com">hanshaohua@msn.com</a>&gt;</p>
                        <p><span style="text-decoration: underline;">LeNS Brazil:</span><strong><br>
                        </strong>LeNS Lab Universidade Federal do Paraná (UFPR), Curitiba, Brazil<br>
                        <em>Contact:&nbsp;</em>Aguinaldo Dos Santos &lt;<a href="mailto:asantos@ufpr.br">asantos@ufpr.br</a>&gt;</p>
                        <div>LeNS Lab Inovation, Design and Sustainability Lab, Federal University of Pernambuco, UFPE</div>
                        <p><em>Contact: </em>Leonardo Gómez Castillo&nbsp;&lt;<a href="mailto:leonardo.a.gomez@gmail.com">leonardo.a.gomez@gmail.com</a>&gt;</p>
                        <p><span style="text-decoration: underline;">LeNS Europe:</span><strong><br>
                        </strong>LeNS Lab Politecnico di Milano, Milan,&nbsp;Italy<br>
                        <em>Contact:</em>&nbsp;Carlo Vezzoli &lt;<a href="mailto:carlo.vezzoli@polimi.it">carlo.vezzoli@polimi.it</a>&gt;<br>
                        <em>Website:</em>&nbsp;<a href="http://www.lenslab.polimi.it">LeNSlab_POLIMI</a></p>
                        <p><strong>Other LeNS Labs&nbsp;worldwide will open soon!</strong></p>
        ';
        $text->save();    

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
