<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakedatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fakedatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author');
            $table->string('country');
            $table->string('year');
            $table->string('language');
            $table->string('type');
            $table->string('platform');
            $table->timestamps();
        });

        // insert default country list
        DB::table('fakedatas')->insert([
            ["title" => "Course 1", "author" => "POLIMI LENSes Team", "country" => "USA", "year" => 2016, "language" => "English", "type" => "course", "platform" => "lenses"], 
            ["title" => "Course 2", "author" => "Fabrizio Ceschin, Silvia Emili, James Wafula, Lorraine Amollo", "country" => "Italy", "year" => 2015, "language" => "English", "type" => "course", "platform" => "lenses"],
            ["title" => "Course 3", "author" => "Mackay Okure", "country" => "USA", "year" => 2014, "language" => "English", "type" => "course", "platform" => "lenses"],
            ["title" => "Corso 1", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2016, "language" => "Italiano", "type" => "course", "platform" => "lensit"], 
            ["title" => "Corso 2", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "course", "platform" => "lensit"],
            ["title" => "Corso 3", "author" => "Silvia Barbero", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "course", "platform" => "lensit"],
            ["title" => "Kors 1", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2016, "language" => "Hindi", "type" => "course", "platform" => "lensin"], 
            ["title" => "Kors 2", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2015, "language" => "Hindi", "type" => "course", "platform" => "lensin"],
            ["title" => "Kors 3", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2014, "language" => "Hindi", "type" => "course", "platform" => "lensin"],

            ["title" => "Lecture 1", "author" => "POLIMI LENSes Team", "country" => "USA", "year" => 2016, "language" => "English", "type" => "lecture", "platform" => "lenses"], 
            ["title" => "Lecture 2", "author" => "Fabrizio Ceschin, Silvia Emili, James Wafula, Lorraine Amollo", "country" => "Italy", "year" => 2015, "language" => "English", "type" => "lecture", "platform" => "lenses"],
            ["title" => "Lecture 3", "author" => "Mackay Okure", "country" => "USA", "year" => 2014, "language" => "English", "type" => "lecture", "platform" => "lenses"],
            ["title" => "Lezione 1", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2016, "language" => "Italiano", "type" => "lecture", "platform" => "lensit"], 
            ["title" => "Lezione 2", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "lecture", "platform" => "lensit"],
            ["title" => "Lezione 3", "author" => "Silvia Barbero", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "lecture", "platform" => "lensit"],
            ["title" => "Bhaashan 1", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2016, "language" => "Hindi", "type" => "lecture", "platform" => "lensin"], 
            ["title" => "Bhaashan 2", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2015, "language" => "Hindi", "type" => "lecture", "platform" => "lensin"],
            ["title" => "Bhaashan 3", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2014, "language" => "Hindi", "type" => "lecture", "platform" => "lensin"],

            ["title" => "Tool 1", "author" => "POLIMI LENSes Team", "country" => "USA", "year" => 2016, "language" => "English", "type" => "tool", "platform" => "lenses"], 
            ["title" => "Tool 2", "author" => "Fabrizio Ceschin, Silvia Emili, James Wafula, Lorraine Amollo", "country" => "Italy", "year" => 2015, "language" => "English", "type" => "tool", "platform" => "lenses"],
            ["title" => "Tool 3", "author" => "Mackay Okure", "country" => "USA", "year" => 2014, "language" => "English", "type" => "tool", "platform" => "lenses"],
            ["title" => "Strumento 1", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2016, "language" => "Italiano", "type" => "tool", "platform" => "lensit"], 
            ["title" => "Strumento 2", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "tool", "platform" => "lensit"],
            ["title" => "Strumento 3", "author" => "Silvia Barbero", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "tool", "platform" => "lensit"],
            ["title" => "Saadhan 1", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2016, "language" => "Hindi", "type" => "tool", "platform" => "lensin"], 
            ["title" => "Saadhan 2", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2015, "language" => "Hindi", "type" => "tool", "platform" => "lensin"],
            ["title" => "Saadhan 3", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2014, "language" => "Hindi", "type" => "tool", "platform" => "lensin"],

            ["title" => "Case Study 1", "author" => "POLIMI LENSes Team", "country" => "USA", "year" => 2016, "language" => "English", "type" => "case", "platform" => "lenses"], 
            ["title" => "Case Study 2", "author" => "Fabrizio Ceschin, Silvia Emili, James Wafula, Lorraine Amollo", "country" => "Italy", "year" => 2015, "language" => "English", "type" => "case", "platform" => "lenses"],
            ["title" => "Case Study 3", "author" => "Mackay Okure", "country" => "USA", "year" => 2014, "language" => "English", "type" => "case", "platform" => "lenses"],
            ["title" => "Caso di Studio 1", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2016, "language" => "Italiano", "type" => "case", "platform" => "lensit"], 
            ["title" => "Caso di Studio 2", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "case", "platform" => "lensit"],
            ["title" => "Caso di Studio 3", "author" => "Silvia Barbero", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "case", "platform" => "lensit"],
            ["title" => "Maamale ka adhyayan 1", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2016, "language" => "Hindi", "type" => "case", "platform" => "lensin"], 
            ["title" => "Maamale ka adhyayan 2", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2015, "language" => "Hindi", "type" => "case", "platform" => "lensin"],
            ["title" => "Maamale ka adhyayan 3", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2014, "language" => "Hindi", "type" => "case", "platform" => "lensin"],

            ["title" => "Project 1", "author" => "POLIMI LENSes Team", "country" => "USA", "year" => 2016, "language" => "English", "type" => "project", "platform" => "lenses"], 
            ["title" => "Project 2", "author" => "Fabrizio Ceschin, Silvia Emili, James Wafula, Lorraine Amollo", "country" => "Italy", "year" => 2015, "language" => "English", "type" => "project", "platform" => "lenses"],
            ["title" => "Project 3", "author" => "Mackay Okure", "country" => "USA", "year" => 2014, "language" => "English", "type" => "project", "platform" => "lenses"],
            ["title" => "Progetto 1", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2016, "language" => "Italiano", "type" => "project", "platform" => "lensit"], 
            ["title" => "Progetto 2", "author" => "Carlo Vezzoli", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "project", "platform" => "lensit"],
            ["title" => "Progetto 3", "author" => "Silvia Barbero", "country" => "Italy", "year" => 2015, "language" => "Italiano", "type" => "project", "platform" => "lensit"],
            ["title" => "Pariyojana 1", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2016, "language" => "Hindi", "type" => "project", "platform" => "lensin"], 
            ["title" => "Pariyojana 2", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2015, "language" => "Hindi", "type" => "project", "platform" => "lensin"],
            ["title" => "Pariyojana 3", "author" => "Mahatma Gandhi", "country" => "India", "year" => 2014, "language" => "Hindi", "type" => "project", "platform" => "lensin"]
        ]);           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fakedatas');
    }
}
