<?php

use \App\Network;
use \App\NetworkMember;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNetwork2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $networks = NetworkMember::where('name', 'LIKE', "xxxxxx")->get();
        foreach ($networks as $network) {
            $network->forceDelete();
        }

        // $network = Network::create([ 'name' => 'LeNS_Mexico', 'created_by' => 1]);
        // $member = NetworkMember::create([ 'name' => 'XXXXXX', 'network_id' => $network->id, 'created_by' => 1 ]);
        // $contact = NetworkMember::create([ 'name' => 'xxxxxx', 'email' => 'XXXXXX' , 'network_member_id' => $member->id, 'created_by' => 1 ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
