<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText130 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('text', "challenge", 'Challenge');
        add_migration_text('text', "students", 'Students');
        add_migration_text('text', "published_click_to_unpublish", 'Published - click to unpublish');
        add_migration_text('text', "published_click_to_publish", 'Not published - click to publish');

        add_migration_text('challenges', "enter_group_and_project_name", 'Enter group and project name');
        add_migration_text('challenges', "no_groups_assigned_to_challenge", 'No groups assigned to challenge');
        add_migration_text('challenges', "edit_group", 'Edit group');
        add_migration_text('challenges', "attachments", 'Attachments');
        add_migration_text('challenges', "group_teachers", 'Group teachers');
        add_migration_text('challenges', "users_assigned_to_group", 'Users assigned to group');
        add_migration_text('challenges', "assign_user", 'Assign to group');
        add_migration_text('challenges', "assign_teacher", 'Assign to teacher');
        add_migration_text('challenges', "detach_teacher_from_group", 'Detach teacher from group');
        add_migration_text('challenges', "detach_user_from_group", 'Detach user from group');
        add_migration_text('challenges', "teacher_already_assigned", 'Teacher already assigned');
        add_migration_text('challenges', "user_already_assigned", 'User already assigned');
        add_migration_text('challenges', "question_delete_theme", 'Are you sure you want to delete theme?');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
