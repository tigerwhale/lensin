<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts24 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "date", "text" => "Date"],
            ["locale" => "en" , "group" => "text", "item" => "no_name", "text" => "(no name)"],
            ["locale" => "en" , "group" => "courses", "item" => "publish_selected_resources", "text" => "Publish selected resources"],
            ["locale" => "en" , "group" => "courses", "item" => "unpublish_selected_resources", "text" => "Unpublish selected resources"],
            ["locale" => "en" , "group" => "courses", "item" => "select_all_resources", "text" => "Select all resources"],
            ["locale" => "en" , "group" => "courses", "item" => "delete_selected_resources", "text" => "Delete selected resources"],
            ["locale" => "en" , "group" => "courses", "item" => "delete_selected_resources_question", "text" => "Are you sure you want to delete selected resources?"],
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
