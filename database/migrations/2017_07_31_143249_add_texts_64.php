<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts64 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "upload_filled_document", "text" => "Upload filled document"],
            ["locale" => "en" , "group" => "text", "item" => "participants", "text" => "Participants"],
            ["locale" => "en" , "group" => "text", "item" => "platforms", "text" => "Platforms"],
            ["locale" => "en" , "group" => "text", "item" => "members_and_contacts", "text" => "Members and contacts"]
        ]);  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
