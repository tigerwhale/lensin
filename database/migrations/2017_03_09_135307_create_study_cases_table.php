<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id')->unsigned();
            $table->integer('created_by');
            $table->string('name');
            $table->string('category')->nullable();
            $table->string('image')->nullable();
            $table->string('source')->nullable();
            $table->string('producer')->nullable();
            $table->string('author')->nullable();
            $table->string('designer')->nullable();
            $table->string('location')->nullable();
            $table->string('main_contact')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('status')->nullable();
            $table->string('start_year')->nullable();
            $table->text('description')->nullable();
            $table->text('benefits')->nullable();
            $table->tinyinteger('published')->nullable();
            $table->string('gps')->nullable();
            $table->string('other_notes')->nullable();
            $table->string('language')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_cases');
    }
}
