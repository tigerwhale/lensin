<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPptText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translation = \App\LanguageTranslation::where('group', 'LIKE', 'resource_types')
            ->where('item', 'LIKE', 'ppt-audio')
            ->where('locale', 'LIKE', 'en')
            ->first();
        $translation->text = 'Slideshow / Audio';
        $translation->save();

        //add_migration_text('study_cases', 'case_study', 'Case Study');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
