<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAdminCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $administrator = \App\User::where('email', 'LIKE', 'administrator@lens-international.org')->first();
        $global_county = \App\Country::where('name', 'LIKE', 'Global')->first();
        if ($administrator && $global_county) {
            $administrator->country_id = $global_county->id;
            $administrator->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
