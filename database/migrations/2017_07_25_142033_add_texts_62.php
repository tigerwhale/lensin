<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts62 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = \App\LanguageTranslation::where('item', 'LIKE', 'register_participant_text')->first();
        $text->text = '
            Anyone can join the LeNS network, either a teacher, a researcher, a student, a designer or anyone interested to learn about design for sustainability. Joining the LeNS Network allows free view and download of all the Learning Resources: courses, single lectures, tools, case studies, criteria and guidelines, projects, etc. In addition, you will receive newsletters, updates about the project latest developments, information about new available resources, communication about LeNS events, information, publications, etc. 
            <br><br>
            <b>How to join the LeNSes network</b><br>
            Please fill in the record aside and press register. 
            You will receive an e-mail of confirmation and your personal username and password.
            ';
        $text->save();       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
