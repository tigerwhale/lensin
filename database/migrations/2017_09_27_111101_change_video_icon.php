<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVideoIcon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $image_resource_type = \App\ResourceType::where('name', 'image')->first();
        $document_resource_type = \App\ResourceType::where('name', 'document')->first();
        if ($image_resource_type){
            $resource_images = \App\Resource::where('resource_type_id', $image_resource_type->id)->withTrashed()->update(['resource_type_id' => $document_resource_type->id ]);
            $image_resource_type->delete();
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
