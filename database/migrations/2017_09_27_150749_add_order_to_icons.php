<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToIcons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('resource_types', function (Blueprint $table) { 
            $table->integer('order')->nullable(); 
        });

        $r = \App\ResourceType::where('name', 'slideshow')->first();
        $r->order = 1;
        $r->save();
        $r = \App\ResourceType::where('name', 'video')->first();
        $r->order = 3;
        $r->name = "ppt-audio";
        $r->save();
        $r = \App\ResourceType::where('name', 'audio')->first();
        $r->order = 4;
        $r->save();
        $r = \App\ResourceType::where('name', 'document')->first();
        $r->order = 5;
        $r->save();
        $r = \App\ResourceType::where('name', 'other')->first();
        $r->order = 6;
        $r->save();

        DB::table('resource_types')->insert([
            ["name" => "video", 
            "icon" => '<i class="fa fa-video-camera" aria-hidden="true"></i>',
            "icon_2" => '<i class="fa fa-video-camera fa-2x" aria-hidden="true"></i>',
            "icon_3" => '<i class="fa fa-video-camera fa-3x" aria-hidden="true"></i>',
            "icon_4" => '<i class="fa fa-video-camera fa-4x" aria-hidden="true"></i>',
            "icon_5" => '<i class="fa fa-video-camera fa-5x" aria-hidden="true"></i>',
            "order" => 2
            ]
        ]);         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
