<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAboutText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = \App\PageText::where('locale', 'LIKE', 'en')->where('page_id', 2)->first();
        $text->text = '
            <p style="margin-top: 20px;">LeNSin, the International Learning Network of networks on Sustainability (2015-2018), is an EU-supported (ERASMUS+) project involving 36 universities from Europe, Asia, Africa, South America and Central America, aiming at the promotion of a new generation of designers (and design educators) capable to effectively contribute to the transition towards a sustainable society for all.</p>
            <p>LeNSin ambitions to improve the internationalisation, intercultural cross-fertilisation and accessibility of higher education on Design for Sustainability (DfS). The project focuses on Sustainable Product-Service Systems (S.PSS) and Distributed Economies (DE) &ndash; considering both as promising models to couple environmental protection with social equity, cohesion and economic prosperity &ndash; applied in different contexts around the world. LeNSin connects a multi-polar network of Higher Education Institutions adopting and promoting a learning-by-sharing knowledge generation and dissemination, with an open and copyleft ethos.</p>
            <p>During the three years of operation, LeNSin project activities involve five seminars, ten pilot courses, the setting up of ten regional LeNS Labs, and of a (decentralised) open web platform, any students/designers and any teachers can access to download, modify/remix and reuse an articulated set of open and copyleft learning resources, i.e. courses/lectures, tools, cases, criteria, projects.</p>
            <p>LeNSin will also promote a series of diffusion activities targeting the design community worldwide. The final event will be a decentralised conference in 2018, based simultaneously in six partner universities, organised together by the 36 project partners form four continents.</p>
            <p>Join the project network by subscribing to our <a href="http://goo.gl/forms/9IvcDK5WuFpJOz5k1" target="_blank" rel="noopener"><strong>newsletter!</strong></a><br /><br /> Previous newsletters:<br /> <a href="http://eepurl.com/cvMmtv" target="_blank" rel="noopener"><strong>LeNSin project newsletter #3</strong> (1/2017)</a><br /> <a href="http://eepurl.com/ckruST" target="_blank" rel="noopener"><strong>LeNSin project newsletter #2</strong> (10/2016)</a><br /> <a href="http://eepurl.com/b60LT9" target="_blank" rel="noopener"><strong>LeNSin project newsletter #1</strong> (6/2016)</a></p>
        ';
        $text->save();  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
