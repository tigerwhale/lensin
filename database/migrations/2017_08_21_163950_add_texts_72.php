<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts72 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        add_migration_text('users', "join_existing_member_group", 'Join existing member group');
        add_migration_text('users', "join_existing_partner_group", 'Join existing partner group');
        add_migration_text('users', "current_members", 'Current members');
        add_migration_text('users', "create_a_new_member", 'Create a new member');
        add_migration_text('users', "create_a_new_partner", 'Create a new partner');
        add_migration_text('users', "members_logo", 'Members logo');
        add_migration_text('users', "partners_logo", 'Partners logo');
        add_migration_text('users', "current_partners", 'Current partners');
        add_migration_text('users', "no_partners_groups", 'No partner groups');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
