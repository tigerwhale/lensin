<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChallengeGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id');
            $table->integer('created_by');
            $table->string('name');
            $table->string('project_name')->nullable();
            $table->tinyInteger('published')->nullable();
            $table->tinyInteger('archived')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_groups');
    }
}
