<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddText71 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add_migration_text('text', "new_networks", 'New networks');
        add_migration_text('text', "backend_welcome_text", 'Please use upper tabs to explore and modify the users and platform settings.');
        add_migration_text('text', "modify_page", 'Modify page');

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
