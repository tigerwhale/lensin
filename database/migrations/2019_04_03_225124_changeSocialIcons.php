<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSocialIcons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $serverProperty = \App\ServerProperties::where('name', "social_icons")->first();
        $serverProperty->data = '{"twitter":"https://twitter.com/LeNS_Network",
            "facebook":"https://www.facebook.com/Lens-Learning-Network-on-Sustainability-270096843638098", 
            "instagram":"http://instagram.com/lensnetwork"
            }';
        $serverProperty->save();
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
