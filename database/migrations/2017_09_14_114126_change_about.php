<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = \App\PageText::where('title', "LeNS International")->first();
        $text->text = '<p style="margin-top: 20px;">LeNSin, the International Learning Network of networks on Sustainability (2015-2018), is an EU-supported (ERASMUS+) project involving 36 universities from Europe, Asia, Africa, South America and Central America, aiming at the promotion of a new generation of designers (and design educators) capable to effectively contribute to the transition towards a sustainable society for all.</p>
                        <p>LeNSin ambitions to improve the internationalisation, intercultural cross-fertilisation and accessibility of higher education on Design for Sustainability (DfS). The project focuses on Sustainable Product-Service Systems (S.PSS) and Distributed Economies (DE) &ndash; considering both as promising models to couple environmental protection with social equity, cohesion and economic prosperity &ndash; applied in different contexts around the world. LeNSin connects a multi-polar network of Higher Education Institutions adopting and promoting a learning-by-sharing knowledge generation and dissemination, with an open and copyleft ethos.</p>
                        <p>During the three years of operation, LeNSin project activities involve five seminars, ten pilot courses, the setting up of ten regional LeNS Labs, and of a (decentralised) open web platform, any students/designers and any teachers can access to download, modify/remix and reuse an articulated set of open and copyleft learning resources, i.e. courses/lectures, tools, cases, criteria, projects.</p>
                        <p>LeNSin will also promote a series of diffusion activities targeting the design community worldwide. The final event will be a decentralised conference in 2018, based simultaneously in six partner universities, organised together by the 36 project partners form four continents.</p>
                        <p>Join the project network by subscribing to our <a href="http://goo.gl/forms/9IvcDK5WuFpJOz5k1" target="_blank" rel="noopener"><strong>newsletter!</strong></a><br /><br /> Previous newsletters:<br /> <a href="http://eepurl.com/cvMmtv" target="_blank" rel="noopener"><strong>LeNSin project newsletter #3</strong> (1/2017)</a><br /> <a href="http://eepurl.com/ckruST" target="_blank" rel="noopener"><strong>LeNSin project newsletter #2</strong> (10/2016)</a><br /> <a href="http://eepurl.com/b60LT9" target="_blank" rel="noopener"><strong>LeNSin project newsletter #1</strong> (6/2016)</a></p>
                        <br><br>
                        <p><strong>LeNS_Brazil</strong></p>
                        <p><strong>PARTNERS:<br>
                        </strong>Universidade Federal do Paraná (UFPR)<br>
                        Universidade Federal de Pernambuco (UFPE)</p>
                        <p><strong>ASSOCIATES:<br>
                        </strong>Londrina State University<br>
                        Fluminense Federal University<br>
                        Federal University of Alagoas<br>
                        Federal University of Uberlândia<br>
                        Federal University of Santa Catarina</p>
                        <p><strong>CONTACT:</strong><br>
                        Aguinaldo Dos Santos &lt;<a href="mailto:asantos@ufpr.br">asantos@ufpr.br</a>&gt;<br>
                        Leonardo Gomez Castillo &lt;<a href="mailto:leonardo.a.gomez@gmail.com">leonardo.a.gomez@gmail.com</a>&gt;</p>
                        <hr>
                        <p><strong>LeNS_India</strong></p>
                        <p><strong>PARTNERS:<br>
                        </strong>Srishti Institute of Art, Design and Technology<br>
                        Indian Institute of Technology Guwahati (IIT Guwahati)</p>
                        <p><strong>ASSOCIATES:<br>
                        </strong>C.A.R.E. School of Architecture<br>
                        Pandit Dwarka Prasad Mishra Indian Institute of Information Technology<br>
                        Indian Institute Of Technology Gandhinagar<br>
                        Goa College of Architecture<br>
                        Hunnarshala&nbsp;Foundation for Building Technology &amp; Innovations<br>
                        Vastu Shilpa Foundation</p>
                        <p><strong>CONTACT:</strong><br>
                        Mary Jacob &lt;<a href="mailto:maryjacob@srishti.ac.in">maryjacob@srishti.ac.in</a>&gt;<br>
                        Ravi Mokashi &lt;<a href="mailto:mokashi@iitg.ernet.in">mokashi@iitg.ernet.in</a>&gt;</p>
                        <hr>
                        <p><strong>LeNS_China</strong></p>
                        <p><strong>PARTNERS:<br>
                        </strong>Tsinghua University<br>
                        Hunan University (HNU)</p>
                        <p><strong>ASSOCIATES:<br>
                        </strong>Wuhan University of Technology<br>
                        Jiangnan University<br>
                        The University of Science and Technology Beijing<br>
                        Beijing Information Science and Technology University<br>
                        The Hong Kong Polytechnic University<br>
                        Guangzhou academy of fine arts<br>
                        Tongji University</p>
                        <p><strong>CONTACT:</strong><br>
                        Liu Xin &lt;<a href="mailto:xinl@tsinghua.edu.cn">xinl@tsinghua.edu.cn</a>&gt;<br>
                        Jun Zhang &lt;<a href="mailto:zhangjun@hnu.edu.cn">zhangjun@hnu.edu.cn</a>&gt;</p>
                        <hr>
                        <p><strong>LeNS_South Africa</strong></p>
                        <p><strong>PARTNERS:<br>
                        </strong>Cape Peninsula University of Technology (CPUT), South Africa<br>
                        Stellenbosch University (SUN), South Africa</p>
                        <p><strong>ASSOCIATES:<br>
                        </strong>Farm and Garden National Trust<br>
                        Cape Craft and Design Institute NPC</p>
                        <p><strong>CONTACT:</strong><br>
                        Ephias Ruhode &nbsp;&lt;<a href="mailto:ruhodee@cput.ac.za">ruhodee@cput.ac.za</a>&gt;<br>
                        Marjorie Naidoo &lt;<a href="mailto:marjorie.naidoo@telkomsa.net">marjorie.naidoo@telkomsa.net</a>&gt;</p>
                        <hr>
                        <p><strong>LeNS_Mexico</strong></p>
                        <p><strong>PARTNERS:<br>
                        </strong>Universidad Autónoma Metropolitana (UAM)<br>
                        Universidad del Valle de México (UVM)</p>
                        <p><strong>ASSOCIATES:<br>
                        </strong>Univesidad National Autónoma Metropolitana<br>
                        Instituto Tecnológico de Monterrey Campus Ciudad de México</p>
                        <p><strong>CONTACT:</strong><br>
                        Alejandro Ramírez Lozano&lt;<a href="mailto:ramloz@correo.azc.uam.mx">ramloz@correo.azc.uam.mx</a>&gt;<br>
                        Rodrigo Lépez Vela &lt;<a href="mailto:rodrigo.lepezv@uvmnet.edu">rodrigo.lepezv@uvmnet.edu</a>&gt;<br>
                        Sandra Molina Mata &lt;<a href="mailto:unasandra@yahoo.com.mx">unasandra@yahoo.com.mx</a>&gt;</p>
                        <hr>
                        <p><strong>LeNS_Europe</strong></p>
                        <p><strong>PARTNERS AND CONTACTS:</strong></p>
                        <p>Politecnico di Milano (POLIMI), Design Dept., Italy<br>
                        Carlo Vezzoli&nbsp;<em>(project coordinator)<br>
                        </em>&lt;<a href="mailto:carlo.vezzoli@polimi.it">carlo.vezzoli@polimi.it</a>&gt;</p>
                        <p>Aalto University (Aalto ARTS), Finland<br>
                        Cindy Kohtala &lt;<a href="mailto:cindy.kohtala@aalto.fi">cindy.kohtala@aalto.fi</a>&gt;</p>
                        <p>Brunel University London (UBRUN), UK<br>
                        Fabrizio Ceschin &lt;<a href="mailto:fabrizio.ceschin@brunel.ac.uk">fabrizio.ceschin@brunel.ac.uk</a>&gt;</p>
                        <p>Technische Universiteit Delft (TU Delft), Netherlands<br>
                        Jan Carel Diehl &lt;<a href="mailto:jc_diehl@me.com">jc_diehl@me.com</a>&gt;</p>
                        <hr>
                        <p><img class="alignnone size-large wp-image-32" src="images/logobar-768x108.png" alt="logobar" width="604" height="85" ></a></p>
                    ';
        $text->save();    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
