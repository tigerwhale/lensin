<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTexts26 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translator_translations')->insert([
            ["locale" => "en" , "group" => "text", "item" => "select_unit", "text" => "Select unit"],
            ["locale" => "en" , "group" => "text", "item" => "minutes", "text" => "minutes"],
            ["locale" => "en" , "group" => "text", "item" => "hours", "text" => "hours"],
            ["locale" => "en" , "group" => "text", "item" => "pages", "text" => "pages"],
            ["locale" => "en" , "group" => "text", "item" => "slides", "text" => "slides"]
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
