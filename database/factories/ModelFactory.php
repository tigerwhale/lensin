<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * USERS
 */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'server_id'     => server_property("server_id"),
        'language'      => 'en',
        'country_id'    => App\Country::inRandomOrder()->first()->id,
        'email'         => $faker->unique()->safeEmail,
        'password'      => $password ?: $password = bcrypt('secret'),
        'name'          => $faker->firstName,
        'last_name'     => $faker->lastName,
        'phone'         => $faker->e164PhoneNumber,
        'web'           => $faker->domainName,
        'user_type_id'  => App\UserType::inRandomOrder()->first()->id,
        'school'        => $faker->company,
        'address'       => $faker->streetAddress,
        'departement'   => $faker->word,
        'position'      => $faker->jobTitle,
        'interest'      => $faker->sentence,
        'gps'           => rand(0,50) . ", " . rand(0,50),
        'facebook_id'   => rand(100,50000),
        'twitter_id'    => rand(100,50000),
        'google_id'     => rand(100,50000),
        'linkedin_id'   => rand(100,50000),
        'validated'     => rand(0,1),
        'newsletter'    => rand(0,1),
        'public'        => rand(0,1),
        'birthday'      => $faker->date(),
        'note'          => $faker->sentence()
    ];
});


/**
 * NETWORKS
*/
$factory->define(App\Network::class, function (Faker\Generator $faker) {
    return [
        'created_by'    => 1,
        'name'          => "LeNS_" . $faker->country,
        'desctiption'   => $faker->text(180)
    ];
});

// NETWORK Memebers
$factory->define(App\NetworkMember::class, function (Faker\Generator $faker) {
    $network_id = ( \App\Network::all()->count() > 0 ) ? \App\Network::all()->random()->id : "";
    return [
        'created_by'    => \App\User::all()->random()->id,
        'name'          => $faker->name,
        'email'         => $faker->unique()->safeEmail,
        'network_id'    => $network_id,
        'desctiption'   => $faker->text(180)
    ];
});


// COURSES
$factory->define(App\Course::class, function (Faker\Generator $faker) {
    return [
        'server_id'         => server_property("server_id"),
        'country_id'        => \App\Country::all()->random()->id,
        'language_id'       => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'created_by'        => \App\User::all()->random()->id,
        'name'              => $faker->sentence(rand(1,6)),
        'image'             => "",
        'school'            => $faker->company,
        'type'              => $faker->sentence(rand(1,6)),
        'description'       => $faker->text(180),
        'published'         => rand(0,1),
        'year'              => $faker->year(),
        'view_count'        => rand(1,10000),
        'download_count'    => rand(1,10000),
        'duration'          => $faker->sentence(rand(1,3))
    ];
});

// COURSE SUBJECTS
$factory->define(App\CourseSubject::class, function (Faker\Generator $faker) {
    return [
        'course_id'     => \App\Course::all()->random()->id,
        'created_by'    => \App\User::all()->random()->id,
        'name'          => $faker->sentence(rand(1,6)),
        'published'     => rand(0,1)
    ];
});

// LECTURE
$factory->define(App\Lecture::class, function (Faker\Generator $faker) {
    return [
        'created_by'        => \App\User::all()->random()->id,
        'course_subject_id' => \App\CourseSubject::all()->random()->id,
        'name'              => $faker->sentence(rand(1,6)),
        'description'       => $faker->text(180),
        'image'             => "",
        'published'         => rand(0,1),
        'order'             => 1,
        'author'            => $faker->name(),
        'year'              => $faker->year(),
        'length'            => rand(1, 100),
        'unit'              => $faker->word,
        'language_id'       => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'view_count'        => rand(1,10000),
        'download_count'    => rand(1,10000)
    ];
});

// LECTURE CONTENT
$factory->define(App\LectureContent::class, function (Faker\Generator $faker) {
    return [
        'created_by'        => \App\User::all()->random()->id,
        'lecture_id'        => \App\Lecture::all()->random()->id,
        'text'              => $faker->sentence(rand(1,6)),
    ];
});

// TOOLS
$factory->define(App\Tool::class, function (Faker\Generator $faker) {
    return [
        'created_by'        => \App\User::all()->random()->id,
        'title'             => $faker->sentence(rand(1,6)),
        'category'          => $faker->word,
        'description'       => $faker->text(180),
        'order'             => 1,
        'filename'          => "",
        'link'              => "",
        'published'         => rand(0,1),
        'image'             => "",
        'language_id'       => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'author'            => $faker->name(),
        'institution'       => $faker->company,
        'view_count'        => rand(1,10000),
        'download_count'    => rand(1,10000)
    ];
});

// STUDY CASES 
$factory->define(App\StudyCase::class, function (Faker\Generator $faker) {
    return [
        'name'          => $faker->sentence(rand(1,6)),
        'created_by'    => \App\User::all()->random()->id,
        'description'   => $faker->text(180),
        'server_id'     => server_property("server_id"),
        'year'          => $faker->year(),
        'category'      => $faker->word,
        'country_id'    => \App\Country::all()->random()->id,
        'source'        => $faker->word,
        'producer'      => $faker->name(),
        'author'        => $faker->name(),
        'designer'      => $faker->name(),
        'location'      => $faker->name(),
        'main_contact'  => $faker->name(),
        'email'         => $faker->safeEmail,
        'website'       => $faker->url,
        'status'        => $faker->word,
        'start_year'    => $faker->year(),
        'description'   => $faker->sentence(rand(1,6)),
        'benefits'      => $faker->sentence(rand(1,6)),
        'gps'           => rand(1000,100000),
        'other_notes'   => $faker->sentence(rand(1,6)),
        'language_id'   => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'institution'   => $faker->company
    ];
});


// THEMES 
$factory->define(App\Theme::class, function (Faker\Generator $faker) {
    return [
        'name'          => $faker->sentence(rand(1,6)),
        'created_by'    => \App\User::all()->random()->id,
        'description'   => $faker->text(180),
        'server_id'     => server_property("server_id")
    ];
});

// CHALLENGES 
$factory->define(App\Challenge::class, function (Faker\Generator $faker) {
    return [
        'created_by'        => \App\User::all()->random()->id,
        'server_id'         => server_property("server_id"),
        'theme_id'          => \App\Theme::all()->random()->id,
        'name'              => $faker->sentence(rand(1,6)),
        'course'            => $faker->sentence(rand(1,6)),
        'year'              => $faker->year(),
        'description'       => $faker->text(180),
        'comments_enabled'  => rand(0,1),
        'published'         => rand(0,1),
        'archived'          => rand(0,1),
        'language_id'       => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'view_count'        => rand(1,10000),
        'download_count'    => rand(1,10000)
    ];
});

// CHALLENGE GROUPS
$factory->define(App\ChallengeGroup::class, function (Faker\Generator $faker) {
    return [
        'server_id'     => server_property("server_id"),
        'created_by_id' => \App\User::all()->random()->id,
        'name'          => $faker->sentence(rand(1,6)),
        'project_name'  => $faker->sentence(rand(1,6)),
        'published'     => rand(0,1),
        'archived'      => rand(0,1),
        'description'   => $faker->text(180),
        'challenge_id'  => \App\Challenge::all()->random()->id,
        'image'         => "",
        'view_count'    => rand(1,10000),
        'download_count' => rand(1,10000)        
    ];
});


$factory->define(App\News::class, function (Faker\Generator $faker) {
    return [
        'created_by_id' => \App\User::all()->random()->id,
        'title'         => $faker->sentence(),
        'text'          => $faker->paragraph(),
        'image'         => 'https://www.amazonfilters.com/uploads/files/test-image.png',
        'server_id'     => server_property("server_id"),
        'published'     => 1,
        'language_id'   => \App\Language::where('published', 'LIKE', 1)->get()->random()->locale,
        'global'        => 1
    ];
});
