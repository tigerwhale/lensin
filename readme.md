## LeNSin Project

LeNSin, the International Learning Network of networks on Sustainability (2015-2018), is an EU-supported (ERASMUS+) project involving 36 universities from Europe, Asia, Africa, South America and Central America, aiming at the promotion of a new generation of designers (and design educators) capable to effectively contribute to the transition towards a sustainable society for all.

LeNSin ambitions to improve the internationalisation, intercultural cross-fertilisation and accessibility of higher education on Design for Sustainability (DfS). The project focuses on Sustainable Product-Service Systems (S.PSS) and Distributed Economies (DE) – considering both as promising models to couple environmental protection with social equity, cohesion and economic prosperity – applied in different contexts around the world.

LeNSin connects a multi-polar network of Higher Education Institutions adopting and promoting a learning-by-sharing knowledge generation and dissemination, with an open and copyleft ethos.

During the three years of operation, LeNSin project activities involve five seminars, ten pilot courses, the setting up of ten regional LeNS Labs, and of a (decentralised) open web platform, any students/designers and any teachers can access to download, modify/remix and reuse an articulated set of open and copyleft learning resources, i.e. courses/lectures, tools, cases, criteria, projects.

LeNSin will also promote a series of diffusion activities targeting the design community worldwide. The final event will be a decentralised conference in 2018, based simultaneously in six partner universities, organised together by the 36 project partners form four continents.