<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// SERVER
	Route::get('/server_list', 'ServerListController@server_list')->middleware('cors');
	Route::post('/server_list', 'ServerListController@server_list')->middleware('cors');
	Route::get('/server_url/{server_id}', 'ServerListController@server_url_api')->middleware('cors');
	Route::post('/backend/register_platform', 'BackendController@register_platform')->middleware('cors');
	Route::get('/backend/register_platform', 'BackendController@register_platform')->middleware('cors');
	Route::post('/backend/synchronize', 'BackendController@synchronize')->middleware('cors');

//LANGUAGE
	Route::get('/language_list', 'LanguageController@language_list');
	
// USERS
	Route::get('/user_details/{user}', 'UserController@user_details')->middleware('cors');
	Route::get('/user_on_system/{email}', 'UserController@user_on_system_api')->middleware('cors');
	Route::get('/check_user_exists', 'UserController@check_user_exists')->middleware('cors');
	Route::post('/check_user_exists', 'UserController@check_user_exists')->middleware('cors');
	Route::get('/all_users', 'UserController@all_users_api')->middleware('cors');
	Route::get('/all_members', 'MembersGroupController@all_members_api')->middleware('cors');
	Route::get('/all_platforms_view', 'DataController@all_platforms_view')->middleware('cors');
	Route::get('/data_all_servers', 'DataController@data_all_servers')->middleware('cors');
	Route::get('/partners_list', 'MembersGroupController@partners_list')->middleware('cors');
	Route::get('/members_list', 'MembersGroupController@members_list')->middleware('cors');
	Route::get('/platform_list_dropdown', 'ServerListController@platform_list_dropdown')->middleware('cors');
	Route::get('/profile_platform_list_dropdown', 'ServerListController@profile_platform_list_dropdown')->middleware('cors');
	Route::post('/update_user_data', 'UserController@update_user_data_from_server')->middleware('cors');
	Route::get('/update_user_image', 'UserController@update_user_image')->middleware('cors');
	Route::get('/confirm_login/{token}/{email}', 'UserController@confirm_login')->middleware('cors');


// COURSES
	Route::post('/courses/{course}', 'CourseController@course_data')->middleware('cors'); 
	Route::post('/courses/view/{course}', 'CourseController@viewApi')->middleware('cors'); 
	
	Route::get('/courses/view/courseware/grid/{course}/{server_id}', 'CourseController@coursewareViewGridApi')->middleware('cors');
	Route::get('/courses/view/courseware/list/{course}/{server_id}', 'CourseController@coursewareViewListApi')->middleware('cors');

// STUDY CASES
	Route::get('/study_cases/view/{study_case}', 'StudyCaseController@viewApi')->middleware('cors'); 
	Route::post('/study_cases/view/{study_case}', 'StudyCaseController@viewApi')->middleware('cors'); 
	Route::get('/study_cases/criteria_tab_view', 'StudyCaseController@viewCriteriaTab')->middleware('cors');
	Route::post('/study_cases/criteria_tab_view', 'StudyCaseController@viewCriteriaTab')->middleware('cors');

	Route::get('/study_cases/select_by_criteria_data', 'StudyCaseController@select_by_criteria_data')->middleware('cors');
	Route::post('/study_cases/select_by_criteria_data', 'StudyCaseController@select_by_criteria_data')->middleware('cors');
	Route::post('/study_cases/criteria_tab', 'StudyCaseController@criteria_tab')->middleware('cors');
	Route::get('/study_cases/criteria_tab', 'StudyCaseController@criteria_tab')->middleware('cors');

// TOOLS
	Route::post('/tools/view/{tool}', 'ToolController@viewApi')->middleware('cors'); 
	Route::get('/tools/view/{tool}', 'ToolController@viewApi')->middleware('cors'); 
	Route::post('/tool/view/{tool}', 'ToolController@viewApi')->middleware('cors'); 
	Route::get('/tool/view/{tool}', 'ToolController@viewApi')->middleware('cors'); 


// LECTURES 
	Route::post('/lectures/view/{lecture}', 'LectureController@viewApi')->middleware('cors'); 
	Route::get('/lectures/view_modal_download/{lecture}', 'LectureController@download_resources_modal')->middleware('cors');
	Route::post('/lectures/view_modal_download/{lecture}', 'LectureController@download_resources_modal')->middleware('cors');

// RESOURCES
	Route::get('/all_resources', 'DataController@all_resources_array')->middleware('cors');
	Route::post('/all_resources', 'DataController@all_resources_array')->middleware('cors');
	Route::post('/all_resources_datatable', 'DataController@all_resources_array');
	Route::post('/all_resources_datatable', 'DataController@all_resources_array');
	Route::get('/home_page_resources_datatable', 'DataController@home_page_resources_datatable')->middleware('cors');

	Route::get('/resources/zip_files', 'ResourceController@zip_files');
	Route::post('/resources/zip_files', 'ResourceController@zip_files')->middleware('cors');
	Route::get('/resources/load_project_resources/{project}', 'ResourceController@load_project_resources')->middleware('cors');
	Route::post('/resources/load_project_resources/{project}', 'ResourceController@load_project_resources')->middleware('cors');
	Route::get('/resources/load_project_resources/{project}/{edit}', 'ResourceController@load_project_resources')->middleware('cors');
	Route::post('/resources/load_project_resources/{project}/{edit}', 'ResourceController@load_project_resources')->middleware('cors');
	Route::get('/resources/preview/{resource}', 'ResourceController@preview')->middleware('cors');
	Route::post('/resources/preview/{resource}', 'ResourceController@preview')->middleware('cors');
	Route::post('/resources/increment_download/{resource}', 'ResourceController@increment_download')->middleware('cors');
	Route::get('/resources/view_modal/{resource_id}/{lecture_id}', 'ResourceController@view_modal')->middleware('cors');
	Route::post('/resources/view_modal/{resource_id}/{lecture_id}', 'ResourceController@view_modal')->middleware('cors'); 

	Route::post('/resources/modal/{resource_id}/{lecture_id}', 'ResourceController@resourceModal')->middleware('cors'); 
	Route::post('/resources/tutorialModal/{resource_id}/{tutorial_id}', 'ResourceController@tutorialResourceModal')->middleware('cors');

	Route::get('/resources/comments/list/{resource}', 'ResourceCommentsController@comments_list')->middleware('cors');
	Route::post('/resources/comments/create', 'ResourceCommentsController@create')->middleware('cors');	

// PROJECTS
	Route::post('/projects/challenges/load_project_image/{project}', 'ChallengeGroupController@loadProjectImage')->middleware('cors');
	Route::get('/projects/view/{challenge_group}', 'ChallengeGroupController@viewApi')->middleware('cors'); 
	Route::post('/projects/view/{challenge_group}', 'ChallengeGroupController@viewApi')->middleware('cors');

// DONWLOADS AND INCREMENTS
	Route::post('/tools/increment_download/{tool}', 'ToolController@increment_download')->middleware('cors');
	Route::post('/study_cases/increment_download/{study_case}', 'StudyCaseController@increment_download')->middleware('cors');
	Route::post('/resources/increment_download/{resource}', 'ResourceController@increment_download')->middleware('cors');
	Route::get('/resources/increment_download/{resource}', 'ResourceController@increment_download')->middleware('cors');

// NEWS
	Route::get('/news/latest', 'NewsController@latest_api')->middleware('cors');
	Route::post('/news/latest', 'NewsController@latest_api')->middleware('cors');
	Route::post('/backend/news_list', 'NewsController@api_backend_news_list')->middleware('cors');
	Route::get('/backend/news_list', 'NewsController@api_backend_news_list')->middleware('cors');
	Route::get('/news/forbidden_news_list', 'NewsController@forbidden_news_list')->middleware('cors');

	Route::post('/backend/publish_news_central/{news_id}/{server_id}', 'NewsController@api_publish_news_central')->middleware('cors');
