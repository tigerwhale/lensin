<?php
// AUTH
	Auth::routes();
	Route::get('/logout', 'Auth\LoginController@logout');
	Route::get('/login', 'Auth\LoginController@showLoginForm');
	Route::get('/changelog', 'HomeController@changelog');
	$router->get('/logs', ['as' => '/logs', 'middleware' => 'role:managesite', 'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index' ]);

// UPDATES AND MIGRATION
	Route::get('/laravel/migrate', 'UpdateController@migrate');
	Route::get('/laravel/update', 'UpdateController@update');
	Route::get('/laravel/updateDev', 'UpdateController@updateDev');
	Route::get('/laravel/createTestData', 'UpdateController@createTestData')->middleware('auth');


// LOCALE
	Route::get('/locale/{locale}', 'LanguageController@change_locale');

// HOME
	Route::get('/', 'HomeController@newHomePage');
	Route::get('/search', 'HomeController@searchPage');


// RESOURCES
	Route::get('/data_all_resources', 'DataController@all_resources_datatable');
	Route::get('/all_resources', 'DataController@all_resources_array');


// COVER IMAGE
	Route::post('/upload_cover_image/{model}/{id}', 'DataController@uploadCoverImage');
	Route::post('/cover_image/{model}/{id}', 'DataController@coverImage');
	Route::post('/remove_cover_image/{model}/{id}', 'DataController@removeCoverImage');
	Route::get('/remove_cover_image/{model}/{id}', 'DataController@removeCoverImage');

// NEWS
	$router->get('/news/create', ['as' => '/news/create', 'middleware' => 'role:managenews', 'uses' => 'NewsController@createPage' ]);
	$router->get('/news/edit/{news}', ['as' => '/news/edit/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@edit' ]);
	$router->get('/news/modify', ['as' => '/news/modify', 'middleware' => 'role:managenews', 'uses' => 'NewsController@modify' ]);
	$router->get('/news/delete/{news}', ['as' => '/news/delete/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@delete' ]);
	$router->post('/news/update_data', ['as' => '/news/update_data', 'middleware' => 'role:managenews', 'uses' => 'NewsController@update_data' ]);
	$router->post('/news/publish/{news}', ['as' => '/news/publish/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@publish' ]);
	$router->post('/news/global/{news}', ['as' => '/news/global/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@global_change' ]);
	$router->get('/news/datatable_list', ['as' => '/news/datatable_list', 'middleware' => 'role:managenews', 'uses' => 'NewsController@datatableList' ]);

	$router->get('/backend/news', ['as' => '/backend/news', 'middleware' => 'role:managenews', 'uses' => 'NewsController@backend_index' ]);
	$router->get('/backend/news/edit_modal/{news}', ['as' => '/backend/news/edit/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@edit_modal' ]);
	$router->post('/backend/news/edit_modal/{news}', ['as' => '/backend/news/edit/{news}', 'middleware' => 'role:managenews', 'uses' => 'NewsController@edit_modal' ]);
	$router->post('/backend/news/create_modal', ['as' => '/backend/news/create_modal', 'middleware' => 'role:managenews', 'uses' => 'NewsController@create_modal' ]);

 	Route::get('/news/{news}', 'NewsController@view');
 	Route::get('/news_old/{news}', 'NewsController@view_old');
 	Route::post('/news/home_page_view', 'NewsController@home_page_view');


// COURSES
	// Views
	Route::get('/courses/view/{course_id}', 'CourseController@view');
	Route::get('/courses/datatable_list', 'CourseController@datatable_list');
	// Data
	$router->get('/courses/edit/{course}', ['as' => '/courses/edit/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@edit' ]);
	$router->post('/courses/edit/view/list/{course}', ['as' => '/courses/edit/view/list/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@editListView' ]);
	$router->post('/courses/edit/view/grid/{course}', ['as' => '/courses/edit/view/grid/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@editGridView' ]);
	$router->get('/courses/modify', ['as' => '/courses/modify', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@modify' ]);
	$router->get('/courses/delete/{course}', ['as' => '/courses/delete/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@delete' ]);
	$router->post('/course/publish/{course}', ['as' => '/course/publish/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@publish' ]);
	$router->get('/courses/create', ['as' => '/courses/create', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@createPage' ]);
	$router->post('/courses/update_data', ['as' => '/courses/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@update_data' ]);
	$router->get('/courses/update_data', ['as' => '/courses/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@update_data' ]);
	$router->get('/courses/unpublish/{course}', ['as' => '/courses/unpublish/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@unpublish' ]);
	// Cover Image
	$router->post('/courses/load_cover_image_for_edit/{course}', ['as' => '/courses/load_cover_image_for_edit/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@loadCoverImageForEdit' ]);
	$router->post('/courses/upload_cover_image/{course}', ['as' => '/courses/upload_cover_image/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@uploadCoverImage' ]);
	$router->get('/courses/remove_cover_image/{course}', ['as' => '/courses/remove_cover_image/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@removeCoverImage' ]);
	$router->post('/courses/remove_cover_image/{course}', ['as' => '/courses/remove_cover_image/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@removeCoverImage' ]);
	// Teachers
	$router->post('/courses/load_course_authors_for_edit/{course}', ['as' => '/courses/load_course_authors_for_edit/{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@loadCourseAuthorsForEdit']);
	$router->post('/courses/add_author_modal', ['as' => '/courses/add_author_modal', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@addAuthorModal']);
	$router->post('/courses/add_author', ['as' => '/courses/add_teacher', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@addAuthor' ]);
	$router->post('/courses/remove_author', ['as' => '/courses/remove_teacher', 'middleware' => 'role:managecourses', 'uses' => 'CourseController@removeAuthor']);


// COURSE SUBJECTS
	$router->get('/course_subjects/create_grid_view/{course}', ['as' => '/course_subjects/create_grid_view{course}', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@createGridView' ]);
	$router->post('/course_subjects/delete', ['as' => '/course_subjects/delete', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@delete' ]);
	$router->post('/course_subjects/update_data', ['as' => '/course_subjects/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_data' ]);
	$router->get('/course_subjects/update_data', ['as' => '/course_subjects/update_data', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_data' ]);
	$router->post('/coursesubject/publish/{coursesubject}', ['as' => '/coursesubject/publish/{coursesubject}', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@publish' ]);
	$router->post('/course_subjects/publish', ['as' => '/course_subjects/publish', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@publish' ]);
	$router->post('/course_subjects/update_sort', ['as' => '/course_subjects/update_sort', 'middleware' => 'role:managecourses', 'uses' => 'CourseSubjectController@update_sort' ]);

// LECTURES
	// Views
	Route::get('/lectures/view/{lecture_id}', 'LectureController@view');
	Route::get('/lectures/view/api/{lecture}', 'LectureController@view_api');
	Route::get('/lectures/view_modal_download/{lecture}', 'LectureController@download_resources_modal');
	Route::post('/lectures/view_modal_download/{lecture}', 'LectureController@download_resources_modal');
	$router->get('/lectures/create_grid_view/{course_subject}', ['as' => '/lectures/create_grid_view/{course_subject}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@createFromGridView' ]);
	$router->get('/lectures/create_list_view/{course_subject}', ['as' => '/lectures/create_list_view/{course_subject}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@createFromListView' ]);
	$router->post('/lectures/edit_modal/{lecture}', ['as' => '/lectures/edit_modal/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@edit_modal' ]);
	$router->get('/lectures/edit_modal/{lecture}', ['as' => '/lectures/edit_modal/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@edit_modal' ]);
	$router->post('/lectures/grid_view_edit/{lecture}', ['as' => '/lectures/grid_view/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@grid_view_edit' ]);
	$router->get('/lectures/grid_view_edit/{lecture}', ['as' => '/lectures/grid_view_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@grid_view_edit' ]);
	// Data
	$router->post('/lectures/update_data', ['as' => '/lectures/update_data', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@update_data' ]);
	$router->post('/lectures/delete', ['as' => '/lectures/delete', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@delete' ]);
	$router->post('/lectures/update_sort', ['as' => '/lectures/update_sort', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@update_sort' ]);
	$router->post('/lecture/publish/{lecture}', ['as' => '/lecture/publish/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@publish' ]);
	$router->post('/lecture/setPublishedStatus/{lecture}', ['as' => '/lecture/setPublishedStatus/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@setPublishedStatus' ]);
	$router->get('/lectures/modify', ['as' => '/lectures/modify', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@modify' ]);
	$router->get('/lectures/datatable_list', ['as' => '/lectures/datatable_list', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@datatableList' ]);
	// Cover image
	$router->post('/lectures/upload_cover_image/{lecture}', ['as' => '/lectures/upload_cover_image/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@upload_cover_image' ]);
	$router->post('/lectures/load_cover_image_for_edit/{lecture}', ['as' => '/lectures/load_cover_image_for_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@loadCoverImageForEdit' ]);
	$router->post('/lectures/remove_cover_image/{lecture}', ['as' => '/lectures/remove_cover_image/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureController@removeCoverImage' ]);
	// Lecture content
	$router->post('/lectures/content/create/{lecture}', ['as' => '/lectures/content/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@create' ]);
	$router->post('/lectures/content/delete/{lectureContent}', ['as' => '/lectures/content/delete/{lectureContent}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@delete' ]);
	$router->post('/lectures/content/update_data', ['as' => '/lectures/content/update_data', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@update_data' ]);
	$router->post('/lectures/content/list_for_lecture_modal/{lecture}', ['as' => '/lectures/content/load_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@listForLectureModal' ]);
	$router->get('/lectures/content/list_for_lecture_modal/{lecture}', ['as' => '/lectures/content/load_edit/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@listForLectureModal' ]);
	$router->post('/lectures/content/resources_list_for_lecture_modal/{lecture}', ['as' => '/lectures/content/resources_list_for_lecture_modal/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@resourcesListForLectureModal' ]);
	$router->post('/lectures/content/resources_list_for_lecture_modal', ['as' => '/lectures/content/resources_list_for_lecture_modal', 'middleware' => 'role:managecourses', 'uses' => 'LectureContentController@resourcesListForLectureModal' ]);


// TOOLS
	// Views
 	Route::get('/tools/datatable_list', 'ToolController@datatable_list');
	Route::get('/tools/view/{tool_id}', 'ToolController@view');
	//Route::get('/tools/view/api/{tool}', 'ToolController@viewApi');

	// Data
	$router->get('/tools/create', ['as' => '/tools/create', 'middleware' => 'role:managetools', 'uses' => 'ToolController@createPage' ]);
	$router->get('/tools/delete/{tool}', ['as' => '/tools/delete/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@delete' ]);
	$router->get('/tools/modify', ['as' => '/tools/modify', 'middleware' => 'role:managetools', 'uses' => 'ToolController@modify' ]);
	$router->get('/tools/edit/{tool}', ['as' => '/tools/edit/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@edit' ]);
	$router->post('/tool/publish/{tool}', ['as' => '/tool/publish/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@publish' ]);
	$router->post('/tools/update_data', ['as' => '/tools/update_data', 'middleware' => 'role:managetools', 'uses' => 'ToolController@update_data' ]);
	// Files
	$router->post('/tools/upload_file/{tool}', ['as' => '/tools/upload_file/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@upload_file' ]);
	$router->post('/tools/file_list/{tool}', ['as' => '/tools/file_list/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@file_list' ]);
	$router->post('/tools/delete_file/{tool}', ['as' => '/tools/delete_file/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@deleteFile' ]);
	// Cover image
	$router->post('/tools/upload_cover_image/{tool}', ['as' => '/tools/upload_cover_image/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@upload_cover_image' ]);
	$router->post('/tools/remove_cover_image/{tool}', ['as' => '/tools/remove_cover_image/{tool}', 'middleware' => 'role:managetools', 'uses' => 'ToolController@remove_cover_image' ]);


// TUTORIALS
    Route::get('/tutorials/datatable_list', 'TutorialController@datatable_list');

    Route::get('/tutorials', 'TutorialController@viewPage');

    $router->get('/tutorials/create/', ['as' => '/tutorials/create/', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@createPage' ]);
    $router->get('/tutorials/edit/{tutorial}', ['as' => '/tutorials/edit/', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@modifyPage' ]);
    $router->get('/tutorials/category_list', ['as' => '/tutorials/category_list/', 'uses' => 'TutorialController@categoryList' ]);
    $router->get('/tutorials/delete/{tutorial}', ['as' => '/tutorials/delete/{tutorial}', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@delete' ]);
    $router->post('/tutorials/publish/{tutorial}', ['as' => '/tutorials/publish/{tutorial}', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@publish' ]);
    $router->post('/tutorial/publish/{tutorial}', ['as' => '/tutorial/publish/{tutorial}', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@publish' ]);
    $router->post('/tutorials/update_data', ['as' => '/tutorials/update_data', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@update_data' ]);

    $router->get('/tutorials/modify/', ['as' => '/tutorials/modify/', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@backendView' ]);
    $router->get('/tutorial/{tutorial}', ['as' => '/tutorials/', 'uses' => 'TutorialController@view' ]);

    $router->post('/tutorials/resources_list_backend/{tutorial}', ['as' => '/lectures/resources_list_backend/{tutorial}', 'middleware' => 'role:managetutorials', 'uses' => 'LectureContentController@resourcesListForLectureModal' ]);

// Tutorial API
    $router->get('/api/tutorials/datatable/all', ['as' => '/api/tutorials/datatable/all', 'middleware' => 'role:managetutorials', 'uses' => 'TutorialController@datatable_all' ]);

// RESOURCES
    $router->post('/reosurces/backend_resource_list', ['as' => '/reosurces/backend_resource_list', 'middleware' => 'role:managetutorials', 'uses' => 'LectureContentController@backendResourceList' ]);



// STUDY CASES
	// Views
	Route::get('/study_cases/view/{study_case}', 'StudyCaseController@view');
	Route::get('/study_cases/view/api/{study_case}', 'StudyCaseController@view_api');
	Route::post('/study_cases/criteria_tab_view', 'StudyCaseController@viewCriteriaTab');

	Route::get('/study_cases/select_by_criteria', 'StudyCaseController@select_by_criteria_view');

	Route::get('/study_cases/select_by_criteria_view', 'StudyCaseController@select_by_criteria_view');
	Route::post('/study_cases/select_by_criteria_view', 'StudyCaseController@select_by_criteria_view');

	Route::get('/study_cases/exportToJsonForOffline', 'StudyCaseController@collectZipFilesForOffline');
	// Data
	$router->get('/study_cases/create/', ['as' => '/study_cases/create/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@createPage' ]);
	$router->post('/study_cases/create/', ['as' => '/study_cases/create/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@create' ]);
	$router->get('/study_cases/modify/', ['as' => '/study_cases/modify/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@modify' ]);
	$router->get('/study_cases/datatable_list', ['as' => '/study_cases/datatable_list/', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@datatable_list' ]);
	$router->post('/studycase/publish/{study_case}', ['as' => '/studycase/publish/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@publish' ]);
	$router->get('/study_cases/delete/{study_case}', ['as' => '/study_cases/delete/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@delete' ]);
	$router->get('/study_cases/edit/{study_case}', ['as' => '/study_cases/edit/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@edit' ]);
	$router->post('/study_cases/update_data', ['as' => '/study_cases/update_data', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@update_data' ]);
	$router->post('/study_cases/update_data', ['as' => '/study_cases/update_data', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@update_data' ]);
	// Images
	$router->post('/study_cases/edit_image_list/{study_case}', ['as' => '/study_cases/edit_image_list/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@imageListEdit' ]);
	$router->post('/study_cases/upload_image/{study_case}', ['as' => '/study_cases/upload_image/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@createResourceWithImage' ]);
	$router->post('/study_cases/update_image_sort/{study_case}', ['as' => '/study_cases/update_image_sort/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'ResourceController@updateImageSort' ]);
	$router->post('/study_cases/image_slideshow/{study_case}', ['as' => '/study_cases/image_slideshow/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@imageSlideshow' ]);
	$router->post('/study_cases/delete_image', ['as' => '/study_cases/delete_image', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@deleteImage' ]);

	// Reports
	$router->post('/study_cases/upload_report/{study_case}', ['as' => '/study_cases/upload_report/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@uploadReport' ]);
	$router->post('/study_cases/delete_report_file/{study_case}', ['as' => '/study_cases/delete_report_file/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@deleteReportFile' ]);
	$router->post('/study_cases/delete_report_preview/{study_case}', ['as' => '/study_cases/delete_report_preview/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@delteReportPreview' ]);
	$router->post('/study_cases/file_list/{study_case}', ['as' => '/study_cases/file_list/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseController@fileList' ]);
	//guidelines
	$router->post('/study_case_guideline/load_guidelines', ['as' => '/study_case_guideline/load_guidelines', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@load_guidelines' ]);
	$router->get('/study_case_guideline/load_guidelines', ['as' => '/study_case_guideline/load_guidelines', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@load_guidelines' ]);
	$router->post('/study_case_guideline/create', ['as' => '/study_case_guideline/create', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@create' ]);
	$router->post('/study_case_guideline/update_data', ['as' => '/study_case_guideline/update_data', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@update_data' ]);
	$router->post('/study_case_guideline/delete/{study_case_guideline}', ['as' => '/study_case_guideline/delete/{study_case_guideline}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@delete' ]);
	$router->post('/study_case_guideline/assign/{study_case}', ['as' => '/study_case_guideline/assign/{study_case}', 'middleware' => 'role:managestudycases', 'uses' => 'StudyCaseGuidelineController@assign' ]);
	$router->post('/study_case_guideline/licence_update', ['as' => '/study_case_guideline/licence_update', 'middleware' => 'role:managecourses', 'uses' => 'StudyCaseGuidelineController@licence_update' ]);


// RESOURCES

	// Project
	Route::get('/resources/create_project_resource/{project}', 'ResourceController@create_project_resource');
	Route::post('/resources/create_project_resource/{project}', 'ResourceController@create_project_resource');
	//$router->get('/resources/create_project_resource/{project}', ['as' => '/resource/create_project_resource/{project}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create_project_resource' ]);
	//$router->post('/resources/create_project_resource/{project}', ['as' => '/resource/create_project_resource/{project}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create_project_resource' ]);
	$router->post('/resources/edit_project_resource/{resource}', ['as' => '/resources/edit_project_resource/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@edit_project_resource' ]);
	Route::get('/resources/load_project_resources/{project}', 'ResourceController@load_project_resources');
	Route::get('/resources/load_project_resources/{project}/{edit}', 'ResourceController@load_project_resources')->middleware('auth');
	Route::post('/resources/update_sort', 'ResourceController@update_sort')->middleware('auth');
	Route::get('/resources/preview/{resource}', 'ResourceController@preview');
	Route::get('/resources/comments/list/{resource}', 'ResourceCommentsController@comments_list');
	Route::get('/resources/comments/create', 'ResourceCommentsController@create')->middleware('auth');
	Route::get('/resources/comments/delete/{resource}', 'ResourceCommentsController@delete')->middleware('auth');
	Route::get('/resources/comments/delete/{resource}', 'ResourceCommentsController@delete')->middleware('auth');

	// Lecture
	$router->get('/resources/create/{lecture}', ['as' => '/resource/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create_lecture' ]);
	$router->post('/resources/create/{lecture}', ['as' => '/resource/create/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@create_lecture' ]);
	$router->post('/resources/create_with_preview/{lecture}', ['as' => '/resource/create_with_preview/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@createWithPreview' ]);
	$router->post('/resources/upload_file/{resource}', ['as' => '/resource/upload_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@upload_file' ]);
	$router->post('/resources/create_and_upload_files/{lecture}', ['as' => '/resource/create_and_upload_files/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@createAndUploadFile' ]);
	$router->post('/resources/update_preview_file/{lecture}', ['as' => '/resource/update_preview_file/{lecture}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@updatePreviewFile' ]);

	Route::post('/resources/delete', 'ResourceController@delete');
	Route::get('/resources/delete', 'ResourceController@delete');
	Route::get('/resources/delete_multiple', 'ResourceController@deleteMultiple');

	$router->post('/resources/remove_file/{resource}', ['as' => '/resources/remove_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@remove_file' ]);
	Route::post('/resources/edit_modal/{resource}', 'ResourceController@edit_modal');
	$router->post('/resources/course_edit_modal/{resource}', ['as' => '/resources/course_edit_modal/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@courseEditModal' ]);
	$router->post('/resources/lecture_modal_file_list/{resource}', ['as' => '/resources/lecture_modal_file_list/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@courseEditModalFileList' ]);

	$router->post('/resources/update_data', ['as' => '/resources/update_data', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@update_data' ]);
	$router->post('/resources/licence_update', ['as' => '/resources/licence_update', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@licence_update' ]);
	$router->post('/resources/upload_file/{resource}', ['as' => '/resources/upload_file/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@upload_file' ]);
	$router->post('/resources/publish', ['as' => '/resources/publish', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@publish' ]);
	$router->post('/resource/publish/{resource}', ['as' => '/resource/publish/{resource}', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@publish' ]);
	$router->post('/resources/publish_multiple', ['as' => '/resources/publish_multiple', 'middleware' => 'role:managecourses', 'uses' => 'ResourceController@publishMultiple' ]);
	Route::post('/resources/view_modal/{resource_id}/{lecture_id}', 'ResourceController@view_modal');
	Route::get('/resources/view_modal/{resource_id}/{lecture_id}', 'ResourceController@view_modal');
	Route::get('/resources/view_modal/api/{resource_type_id}/{lecture_id}', 'ResourceController@view_modal_api_lecture');
	Route::post('/resources/view_modal/api/{resource_type_id}/{lecture_id}', 'ResourceController@view_modal_api_lecture');
	Route::post('/courses/resource/delete_file/{resource}', 'ResourceController@delete_file');
	Route::post('/resources/file_preview_list/{resource}', 'ResourceController@filePreviewList');
	Route::get('/resources/file_preview_list/{resource}', 'ResourceController@filePreviewList');
	Route::get('/resources/update_preview_file/{resource}', 'ResourceController@update_preview_file');
	Route::post('/resources/add_preview/{resource}', 'ResourceController@addPreview');


/***** PROJECTS ****/
// THEMES
	$router->post('/projects/themes/edit_page', ['as' => '/projects/themes/edit_page', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@edit_page' ]);
	Route::get('/projects/themes/data_table_modify', 'ThemesController@data_table_modify');
	$router->post('/projects/themes/create_modal', ['as' => '/projects/themes/create_modal', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@create_view' ]);
	$router->post('/projects/themes/create', ['as' => '/projects/themes/create', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@create' ]);
	$router->post('/projects/themes/edit/{theme}', ['as' => '/projects/themes/edit/{theme}', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@edit' ]);
	$router->post('/projects/themes/edit_modal/{theme}', ['as' => '/projects/themes/edit_modal/{theme}', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@edit_modal' ]);
	$router->post('/projects/themes/delete/{theme}', ['as' => '/projects/themes/delete/{theme}', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@delete' ]);
	$router->get('/projects/themes/modify', ['as' => '/projects/themes/modify', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@modify' ]);
	$router->post('/projects/themes/update_data', ['as' => '/projects/themes/update_data', 'middleware' => 'role:manageprojects', 'uses' => 'ThemesController@update_data' ]);


// CHALANGES
	Route::get('/projects/challenges/data_table_modify', 'ChallengesController@data_table_modify');
	$router->post('/projects/challenges/modify', ['as' => '/projects/challenges/modify', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@modify' ]);

	$router->post('/projects/challenges/create_modal', ['as' => '/projects/challenges/create_modal', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create_modal' ]);
	$router->post('/projects/challenges/create', ['as' => '/projects/challenges/create', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create' ]);
	//$router->post('/projects/challenges/create', ['as' => '/projects/challenges/create', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create' ]);
	$router->get('/projects/challenges/delete/{challenge}', ['as' => '/projects/challenges/delete/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@delete' ]);
	$router->post('/projects/challenges/update_data', ['as' => '/projects/challenges/update_data', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@update_data' ]);

	$router->post('/projects/challenges/edit/load_assign_group_to_challenge', ['as' => '/projects/challenges/edit/load_assign_group_to_challenge', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@load_assign_group_to_challenge' ]);
	//$router->post('/projects/challenges/edit/create_new_group_modal/{challenge}', ['as' => '/projects/challenges/edit/create_new_group_modal/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create_new_group_modal' ]);
	$router->post('/projects/challenges/edit/create_new_group_modal', ['as' => '/projects/challenges/edit/create_new_group_modal', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@create_new_group_modal' ]);

	// challange groups
	$router->post('/projects/challenges/groups/create', ['as' => '/projects/challenges/groups/create', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@create' ]);
	$router->post('/projects/challenges/groups/delete/{group}', ['as' => '/projects/challenges/groups/delete/{group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@delete' ]);
	$router->post('/projects/challenges/groups/edit/{challenge_group}', ['as' => '/projects/challenges/groups/edit/{challenge_group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@edit_modal' ]);
	$router->post('/projects/challenges/groups/update_data', ['as' => '/projects/challenges/groups/update_data', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@update_data' ]);
	$router->post('/projects/challenges/groups/upload_cover_image/{group}', ['as' => '/projects/challenges/groups/upload_cover_image/{group}', 'uses' => 'ChallengeGroupController@upload_image' ]);
	$router->post('/projects/challenges/groups/modify', ['as' => '/projects/challenges/groups/modify', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@modify' ]);
	$router->post('/projects/challenges/groups/publish', ['as' => '/projects/challenges/groups/publish', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@publish' ]);

		// group users
		$router->post('/projects/challenges/groups/load_group_users/{challenge_group}', ['as' => '/projects/challenges/groups/load_group_users/{challenge_group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@load_group_users' ]);
		$router->post('/projects/challenges/groups/assign_user/{challenge_group}/{user}', ['as' => '/projects/challenges/groups/assign_user/{challenge_group}/{user}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@assign_user' ]);
		$router->post('/projects/challenges/groups/detach_user/{challenge_group}/{user}', ['as' => '/projects/challenges/groups/detach_user/{challenge_group}/{user}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@detach_user' ]);

		// group teachers
		$router->post('/projects/challenges/groups/load_group_teachers/{challenge_group}', ['as' => '/projects/challenges/groups/load_group_teachers/{challenge_group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@load_group_teachers' ]);
		$router->post('/projects/challenges/groups/assign_teacher/{challenge_group}/{teacher}', ['as' => '/projects/challenges/groups/assign_teacher/{challenge_group}/{teacher}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@assign_teacher' ]);
		$router->post('/projects/challenges/groups/detach_teacher/{challenge_group}/{teacher}', ['as' => '/projects/challenges/groups/detach_teacher/{challenge_group}/{teacher}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@detach_teacher' ]);

	$router->post('/projects/challenges/edit/{challenge}/groups_assigned_to_challenge', ['as' => '/projects/challenges/edit/groups_assigned_to_challenge', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@groups_assigned_to_challenge' ]);
	$router->get('/projects/challenges/edit/{challenge}', ['as' => '/projects/challenges/edit/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@edit' ]);
	$router->post('/projects/challenges/edit_modal/{challenge}', ['as' => '/projects/challenges/edit_modal/{challenge}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@edit_modal' ]);

// PROJECTS/GROUPS
	Route::post('/projects/resource_upload/{group}', 'ChallengeGroupController@resource_upload_modal');
	Route::get('/projects/view/{project_id}', 'ChallengeGroupController@view');
	Route::get('/projects/view/api/{project_id}', 'ChallengeGroupController@view_api');
	$router->post('/projects/delete/{group}', ['as' => '/projects/delete/{group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@delete']);
	$router->post('/challengegroup/publish/{group}', ['as' => '/challengegroup/publish/{group}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@publish']);
	$router->get('/projects/data_table_modify', ['as' => '/projects/data_table_modify', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengeGroupController@data_table_modify']);
	$router->get('/projects/{create}', ['as' => '/projects/{create}', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@main_view' ]);
	$router->get('/projects', ['as' => '/projects', 'middleware' => 'role:manageprojects', 'uses' => 'ChallengesController@main_view' ]);



/**** END PROJECTS ****/


// USERS
	Route::get('/users/list', 'UserController@datatable_list');
	$router->post('/backend/user_roles/update_data', ['as' => '/backend/users/update_data', 'middleware' => 'role:manageusers', 'uses' => 'UserController@update_roles']);
	$router->post('/backend/user_roles/update_data', ['as' => '/backend/users/update_data', 'middleware' => 'role:manageusers', 'uses' => 'UserController@update_roles']);
	$router->get('/backend/users/delete/{user}', ['as' => '/backend/users/delete{user}', 'middleware' => 'role:manageusers', 'uses' => 'UserController@delete_user' ]);
	$router->get('/backend/users/suspend/{user}', ['as' => '/backend/users/suspend{user}', 'middleware' => 'role:manageusers', 'uses' => 'UserController@suspend_user' ]);

	// PROFILE
	Route::get('/profile', 'UserController@profile')->middleware('auth');
	Route::post('/profile/groups_list/{user}', 'UserController@groupsList')->middleware('auth');
	Route::post('/profile/upload_image/{user}', 'UserController@upload_image')->middleware('auth');
	Route::post('/profile/remove_image/{user}', 'UserController@remove_image')->middleware('auth');


	// MEMBER GROUPS
	Route::post('/member_group/view_create_member_tab', 'MembersGroupController@create_member_tab')->middleware('auth');
	Route::post('/member_group/create', 'MembersGroupController@create')->middleware('auth');
	Route::post('/member_group/join/{user}', 'MembersGroupController@join')->middleware('auth');
	Route::get('/member_group/datatable_list', 'MembersGroupController@datatable_list')->middleware('auth');
	Route::post('/member_group/delete/{members_group}', 'MembersGroupController@delete')->middleware('auth');
	Route::post('/member_group/update_data', 'MembersGroupController@update_data')->middleware('auth');
	Route::post('/membersgroup/publish/{members_group}', 'MembersGroupController@publish')->middleware('auth');
	Route::post('/membersgroup/leave', 'MembersGroupController@leave')->middleware('auth');

	$router->get('/backend/members', ['as' => '/backend/members', 'middleware' => 'role:manageusers', 'uses' => 'MembersGroupController@backend' ]);
    Route::post('/backend/members/update/{member_group}', 'MembersGroupController@update_member')->middleware('role:manageusers');
    $router->get('/backend/upgrade_member_to_partner/{member_group}', ['as' => '/backend/upgrade_member_to_partner/{member_group}', 'middleware' => 'role:manageusers', 'uses' => 'MembersGroupController@upgrade_member_to_partner' ]);
	$router->get('/backend/downgrade_partner_to_member/{member_group}', ['as' => '/backend/downgrade_partner_to_member/{member_group}', 'middleware' => 'role:manageusers', 'uses' => 'MembersGroupController@downgrade_partner_to_member' ]);
	$router->post('/backend/members/edit_modal/{member_group}', ['as' => '/backend/members/edit_modal/{members_group}', 'middleware' => 'role:manageusers', 'uses' => 'MembersGroupController@edit_modal' ]);

	// PARTNERS
	Route::post('/partnersgroup/view_create_partner_tab', 'PartnersGroupController@create_partners_tab')->middleware('auth');
	Route::post('/partnersgroup/create', 'PartnersGroupController@create')->middleware('auth');
	Route::get('/partnersgroup/datatable_list', 'PartnersGroupController@datatable_list')->middleware('auth');
	Route::post('/partnersgroup/publish/{partnersgroup}', 'PartnersGroupController@publish')->middleware('auth');
	Route::post('/partnersgroup/delete/{partnersgroup}', 'PartnersGroupController@delete')->middleware('auth');
	Route::post('/partnersgroup/leave', 'PartnersGroupController@leave')->middleware('auth');
	Route::post('/partnersgroup/update_data', 'PartnersGroupController@update_data')->middleware('auth');

	$router->get('/backend/partners', ['as' => '/backend/patners', 'middleware' => 'role:manageusers', 'uses' => 'PartnersGroupController@backend' ]);
	$router->post('/backend/partners/edit_modal/{partnersgroup}', ['as' => '/backend/partners/edit_modal/{partnersgroup}', 'middleware' => 'role:manageusers', 'uses' => 'PartnersGroupController@edit_modal' ]);

	// NEW NETWORKS
	Route::get('/newnetwork/datatable_list', 'NewNetworkController@datatable_list')->middleware('auth');
	Route::post('/newnetworks/delete/{newnetwork}', 'NewNetworkController@delete')->middleware('auth');
	Route::post('/newnetworks/publish/{newnetwork}', 'NewNetworkController@publish')->middleware('auth');
	Route::post('/newnetworks/resolve/{newnetwork}', 'NewNetworkController@resolve')->middleware('auth');

	$router->get('/backend/newnetworks', ['as' => '/backend/newnetworks', 'middleware' => 'role:manageusers', 'uses' => 'NewNetworkController@backend' ]);
	$router->post('/backend/newnetwork/view_modal/{newnetwork}', ['as' => '/backend/newnetwork/view_modal/{newnetwork}', 'middleware' => 'role:manageusers', 'uses' => 'NewNetworkController@view_modal' ]);

// BACKEND
	Route::get('/backend_old', 'BackendController@backend_old')->middleware('auth');

	Route::get('/backend', 'BackendController@backend')->middleware('auth');
	Route::post('/backend/register_new_network', 'NewNetworkController@create')->middleware('auth');
	Route::post('/backend/newnetwork/list', 'NewNetworkController@list')->middleware('auth');

	// REGISTER A PLATFORM
	Route::post('/backend/register_platform', 'BackendController@register_platform')->middleware('cors');
	Route::get('/backend/register_platform', 'BackendController@register_platform')->middleware('cors');
	// SEND REGISTRATION TO CENTRAL SERVER
	$router->get('/backend/register_platform_request', ['as' => '/backend/register_platform_request', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@register_platform_request' ]);

	// USERS
	$router->get('/backend/users', ['as' => '/backend/users', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@users_list' ]);
	$router->get('/backend/users/edit/{user}', ['as' => '/backend/users/edit/{user}', 'middleware' => 'role:manageusers', 'uses' => 'BackendController@user_edit' ]);
	$router->post('/backend/users/update_data', ['as' => '/backend/users/update_data', 'uses' => 'UserController@update_data_backend']);
	$router->post('/backend/user_roles/update/{user}', ['as' => '/backend/user_roles/update/{user}',  'middleware' => 'role:manageusers','uses' => 'UserController@update_roles' ]);
	//Route::post('/backend/users/update_data', 'BackendController@update_data_backend');

	// SERVER PROPERTIES
	$router->get('/backend/general', ['as' => '/backend/general', 'middleware' => 'role:managesite', 'uses' => 'BackendController@general' ]);
	$router->post('/backend/general', ['as' => '/backend/general', 'middleware' => 'role:managesite', 'uses' => 'BackendController@general_update' ]);
	$router->post('/backend/upload/{path}', ['as' => '/backend/upload/{path}', 'middleware' => 'role:managesite', 'uses' => 'BackendController@upload_logo' ]);

	// HOME VIDEO
	$router->post('/backend/upload_home_video', ['as' => '/backend/upload_home_video', 'middleware' => 'role:managesite', 'uses' => 'BackendController@uploadHomeVideo' ]);
	$router->post('/backend/defaut_home_video', ['as' => '/backend/defaut_home_video', 'middleware' => 'role:managesite', 'uses' => 'BackendController@defaut_home_video' ]);
	$router->post('/backend/delete_home_video', ['as' => '/backend/delete_home_video', 'middleware' => 'role:managesite', 'uses' => 'BackendController@delete_home_video' ]);
	$router->post('/backend/upload_home_video_poster', ['as' => '/backend/upload_home_video_poster', 'middleware' => 'role:managesite', 'uses' => 'BackendController@uploadHomeVideoPoster' ]);
	$router->post('/backend/delete_home_video_poster', ['as' => '/backend/delete_home_video_poster', 'middleware' => 'role:managesite', 'uses' => 'BackendController@deleteHomeVideoPoster' ]);


	// SERVER LIST
	$router->get('/backend/servers', ['as' => '/backend/servers', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@index' ]);
	$router->post('/backend/servers/update/{server_list}', ['as' => '/backend/servers/update/{server_list}', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@update' ]);
	$router->post('/backend/servers/create', ['as' => '/backend/servers/create', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@create' ]);
	$router->get('/backend/servers/delete/{server_list}', ['as' => '/backend/servers/delete/{server_list}', 'middleware' => 'role:manageservers', 'uses' => 'ServerListController@delete' ]);

	// LANGUAGES
	$router->get('/backend/languages', ['as' => '/backend/languages', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@index' ]);
	$router->post('/backend/languages/create', ['as' => '/backend/languages/create', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@create' ]);
	$router->post('/backend/languages/update/{language}', ['as' => '/backend/languages/update/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@update' ]);
	$router->get('/backend/languages/delete/{language}', ['as' => '/backend//delete/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageController@delete' ]);
	// TRANSLATIONS
	$router->get('/backend/translations/{language}', ['as' => '/backend/translations/{language}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageTranslationsController@index' ]);
	$router->post('/backend/translations/update/{translation}', ['as' => '/backend/translations/update/{translation}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageTranslationsController@update' ]);
	$router->post('/backend/langauge/translation_array/{locale}', ['as' => '/backend/langauge/translation_array/{locale}', 'middleware' => 'role:managelanguages', 'uses' => 'LanguageTranslationsController@translation_array']);

	Route::get('/backend/synchronize', 'BackendController@synchronize');

	// PAGES
	$router->get('/backend/pages', ['as' => '/backend/pages', 'middleware' => 'role:managesite', 'uses' => 'PageController@modify' ]);
	$router->get('/backend/pages/edit/{page}', ['as' => '/backend/pages/edit/{page}', 'middleware' => 'role:managesite', 'uses' => 'PageController@edit' ]);
	$router->get('/pages/datatable_list', ['as' => '/pages/datatable_list', 'middleware' => 'role:managesite', 'uses' => 'PageController@datatableList' ]);
	$router->post('/pages/update/{page}', ['as' => '/pages/update/{page}', 'middleware' => 'role:managesite', 'uses' => 'PageController@update' ]);
	// PAGE TEXT
	$router->post('/page_texts/update_data', ['as' => '/page_texts/update_data', 'middleware' => 'role:managesite', 'uses' => 'PageTextsController@update' ]);

	// NETWORKS
    $router->get('/backend/networks_view', ['as' => '/backend/networks_view', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@backend_view' ]);
	$router->post('/backend/networks_view', ['as' => '/backend/networks_view', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@backend_view' ]);
	$router->post('/backend/networks/create', ['as' => '/backend/networks/create', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@create' ]);
	Route::post('/networks/update_data', 'NetworkController@update_data')->middleware('auth');
	// NETWORK MEMBERS
	Route::post('/network_member/update_data', 'NetworkMemberController@update_data')->middleware('auth');
	Route::post('/network_member/create', 'NetworkMemberController@create')->middleware('auth');
	Route::post('/network_member/delete/{member}', 'NetworkMemberController@delete')->middleware('auth');
	Route::get('/network', 'NetworkController@view');
	Route::get('/new_network', 'NetworkController@new_page_view');

	// BACKEND NETWORKS
    $router->get('/backend/edit_networks', ['as' => '/backend/networks', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@networks_edit_page' ]);
    $router->get('/backend/networks_datatableList', ['as' => '/backend/networks_datatableList', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@datatableList' ]);
    $router->get('/backend/networks', ['as' => '/backend/networks', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@backend_networks_page' ]);
    $router->post('/backend/edit_network_modal/{network}', ['as' => 'backend.edit_network_modal', 'middleware' => 'role:managesite', 'uses' => 'NetworkController@edit_network_modal' ]);
    $router->post('/network/publish/{network}', ['as' => '/networks/publish/{network}', 'middleware' => 'role:managenews', 'uses' => 'NetworkController@publish' ]);


// FIX
	Route::get('/fix_courses', 'CourseController@fix_courses');
// Test EMAIL
	Route::get('/test_email/{email}', 'TestController@test_email');
	Route::get('/clear_cache', 'TestController@clear_cache');


	//---- TO REMOVE -----//
	//Route::get('/', 'HomeController@homePageOld');
	Route::post('/home_grid_view', 'DataController@home_grid_view');

	Route::post('/all_resources', 'DataController@all_resources_array');
	Route::get('/all_resources_all_servers', 'DataController@all_resources_all_servers');
	Route::post('/all_resources_all_servers', 'DataController@all_resources_all_servers');
	Route::get('/all_resources_all_servers_datatables', 'DataController@all_resources_all_servers_datatables');


// PAGES
	Route::get('/{permalink}', 'PageController@view');